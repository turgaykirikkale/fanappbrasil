module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          '@Components': './src/Components',
          '@Commons': './src/Commons',
          '@Navigation': './src/Navigation',
          '@Screens': './src/Screens',
          '@Services': './src/Services',
          '@Stream': './src/Stream',
          '@ValidationEngine': './src/ValidationEngine',
          '@GlobalStore': './src/GlobalStore',
          '@Helpers': './src/Helpers',
          '@Localization': './src/Localization',
          '@Utils': './src/Utils',
        },
      },
    ],
    ['@babel/plugin-proposal-decorators', {legacy: true}],
  ],
};
