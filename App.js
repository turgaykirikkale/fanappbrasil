import 'react-native-gesture-handler';
import React from 'react';
import Entrypoint from '@Navigation/Entrypoint';
import Toast from 'react-native-toast-message';
import {LogBox} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import store from '@GlobalStore/store';
import codePush from 'react-native-code-push';
import Analytics from 'appcenter-analytics';
// import ReactNativeAN from 'react-native-alarm-notification';
import {NativeEventEmitter, NativeModules} from 'react-native';

// const {RNAlarmNotification} = NativeModules;
// const RNAlarmEmitter = new NativeEventEmitter(RNAlarmNotification);

LogBox.ignoreLogs(['Warning: ...', 'Error: console.error']); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notifications

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};

class App extends React.Component {
  // // requestPermissions() {
  // //   ReactNativeAN.requestPermissions({
  // //     alert: true,
  // //     badge: true,
  // //     sound: true,
  // //     lockScreen: true,
  // //     notificationCenter: true,
  // //   });
  // // }

  // _alarmSubscription = null;

  async componentDidMount() {
    // this.requestPermissions();
    await Analytics.trackEvent('Initialize');
  }

  //   this._alarmSubscription = RNAlarmEmitter.addListener(
  //     'OnNotificationOpened',
  //     data => console.log(JSON.parse(data)),
  //   );
  // }

  // componentWillUnmount() {
  //   if (this._alarmSubscription !== null) {
  //     this._alarmSubscription.remove();
  //   }
  // }

  render() {
    return (
      <Provider store={store}>
        <SafeAreaProvider>
          <Entrypoint />
          <Toast ref={ref => Toast.setRef(ref)} />
        </SafeAreaProvider>
      </Provider>
    );
  }
}

App = codePush(codePushOptions)(App);
export default App;
