import store from '@GlobalStore/store';
import {useEffect, useState} from 'react';

export const useMarketInformation = () => {
  const [marketQueueName, setMArketQueueName] = useState(null);
  const globalStore = store.getState();
  const MarketState = globalStore?.MarketState;

  useEffect(() => {
    if (MarketState) {
      setMArketQueueName(MarketState?.MarketQueueName);
    }
  }, [MarketState]);
  return {
    MarketQueueName: marketQueueName,
  };
};
