import React from 'react';
import {getDataFromStorage, setDataToStorage} from '@Commons/FAStorage';
import themes from './theme.json';
import _ from 'lodash';

const defaultTheme = themes[0];
export const ThemeContext = React.createContext();

export class ThemeProvider extends React.Component {
  state = {
    theme: defaultTheme,
    updateTheme: theme => {
      const themeObj = _.find(themes, {name: theme});
      this.setState({theme: themeObj}, async () => {
        await setDataToStorage('Theme', theme);
      });
    },
  };

  async componentWillMount() {
    const themeNameFromStorage = await getDataFromStorage('Theme');
    let themeObj = defaultTheme;
    if (themeNameFromStorage) {
      themeObj = _.find(themes, {name: themeNameFromStorage});
    }
    this.setState({
      theme: themeObj,
    });
  }

  render() {
    const {theme} = this.state;
    return (
      <ThemeContext.Provider value={this.state} theme={theme}>
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}
