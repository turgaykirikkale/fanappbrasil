import * as EventsActionsConstants from '@GlobalStore/Actions/EventActions/constants';

const initialState = {
  ActiveEvents: [],
  PassedEvents: [],
  JoinedEvents: [],
};

export default function (state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case EventsActionsConstants.SetEvents:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
}
