import * as UserActionConstants from '@GlobalStore/Actions/UserActions/constants';
import * as FlowActionConstants from '@GlobalStore/Actions/FlowActions/constants';
const initialState = {
  UserInfo: null,
  PusherPrivateKey: null,
  FinanceInfo: {
    isLoadingCoinAssetList: true,
  },
  Language: 'en',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case UserActionConstants.setUserInformation:
      return {
        ...state,
        UserInfo: action.payload,
      };
    case UserActionConstants.setUserFinanceState:
      return {
        ...state,
        FinanceInfo: {
          ...state.Finance,
          CashMoneyBalanceList: action.payload.CashMoneyBalanceList,
          TotalBalanceValueList: action.payload.TotalBalanceValueList,
          CustomerCoinBalanceDetailList:
            action.payload.CustomerCoinBalanceDetailList,
          CompletedProfitLoss: action.payload.CompletedProfitLoss,
          UnCompletedProfitLoss: action.payload.UnCompletedProfitLoss,
        },
        isLoadingCoinAssetList: false,
      };

    case UserActionConstants.setPusherPrivateKey: {
      return {
        ...state,
        PusherPrivateKey: action.payload,
      };
    }

    case UserActionConstants.changeLanguage:
      return {
        ...state,
        Language: action.payload,
      };
    case FlowActionConstants.resetState:
      return {
        ...state,
        UserInfo: null,
        PusherPrivateKey: null,
        FinanceInfo: {
          isLoadingCoinAssetList: true,
        },
      };

    default:
      return state;
  }
}
