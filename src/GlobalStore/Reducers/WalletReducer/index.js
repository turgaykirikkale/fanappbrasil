import * as WalletActionConstants from '@GlobalStore/Actions/WalletActions/constants';
import * as FlowActionConstants from '@GlobalStore/Actions/FlowActions/constants';

const initialState = {
  PaymentChannelList: [],
  UserWallets: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case WalletActionConstants.setPaymentChannelList: {
      return {
        ...state,
        PaymentChannelList: action.payload,
      };
    }

    case WalletActionConstants.setUserWallets: {
      return {
        ...state,
        UserWallets: action.payload,
      };
    }
    case FlowActionConstants.resetState:
      return {
        state: initialState,
      };
    default:
      return state;
  }
}
