import {combineReducers} from 'redux';
import MarketState from './MarketReducer';
import UserState from './UserReducer';
import WalletState from './WalletReducer';
import EventState from './EventReducer';
import SurveyState from './SurveyReducer';
import NewsState from './NewsReducer';

export default combineReducers({
  MarketState,
  UserState,
  WalletState,
  EventState,
  SurveyState,
  NewsState,
});
