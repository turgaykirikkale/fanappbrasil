import * as SurveyActionConstants from '@GlobalStore/Actions/SurveyActions/constants';

const initialState = {
  ActiveSurveys: [],
  PassedSurveys: [],
  VotedSurveys: [],
};

export default function (state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case SurveyActionConstants.SetSurveys:
      return {
        ...state,
        ...payload,
      };

    default:
      return state;
  }
}
