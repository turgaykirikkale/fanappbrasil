import * as MarketActionConstants from '@GlobalStore/Actions/MarketActions/constants';
import {StockMode} from '@Commons/FAEnums';

const initialState = {
  SelectedCoinCode: 'BITCI',
  SelectedCurrencyCode: 'TRY',
  isAnonymousFlow: true,
  SelectedPair: {},
  StockMode: StockMode.Advanced,
  CommissionRate: {},
  MarketQueueName: null,
  CoinRates: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MarketActionConstants.setSelectedCoinAndCurrency:
      return {
        ...state,
        ...action.payload,
      };

    case MarketActionConstants.setMarketInformation:
      return {
        ...state,
        ...action.payload,
      };

    case MarketActionConstants.setIsAnonymousFlow:
      return {
        ...state,
        isAnonymousFlow: action.payload,
      };

    case MarketActionConstants.setStockMode:
      return {
        ...state,
        StockMode: action.payload,
      };
    case MarketActionConstants.setCoinRates:
      return {
        ...state,
        CoinRates: action.payload,
      };
    case MarketActionConstants.setSelectedPair:
      return {
        ...state,
        SelectedPair: action.payload,
      };
    default:
      return state;
  }
}
