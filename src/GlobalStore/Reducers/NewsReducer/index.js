import * as NewsActionsConstants from '@GlobalStore/Actions/NewsActions/constants';

const initialState = {};
export default function (state = initialState, action) {
  switch (action.type) {
    case NewsActionsConstants.SetNews:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
}
