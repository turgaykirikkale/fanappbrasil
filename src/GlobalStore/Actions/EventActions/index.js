import * as EventsActionsConstants from './constants';

export const GetEventsAction = () => {
  return {
    type: EventsActionsConstants.GetEvents,
  };
};

export const SetEventsAction = payload => {
  return {
    type: EventsActionsConstants.SetEvents,
    payload,
  };
};

export default {
  GetEventsAction,
  SetEventsAction,
};
