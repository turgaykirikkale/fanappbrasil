export const GetEvents = 'GET_EVENTS';
export const SetEvents = 'SET_EVENTS';

export default {
  GetEvents,
  SetEvents,
};
