export const setSelectedCoinAndCurrency = 'SET_SELECTED_COIN_AND_CURRENCY';
export const setIsAnonymousFlow = 'SET_IS_ANONYMOUS_FLOW';
export const setMarketInformation = 'SET_MARKET_INFORMATION';
export const getMarketInformation = 'GET_MARKET_INFORMATION';
export const setStockMode = 'SET_STOCK_MODE';
export const setCoinRates = 'GET_MARKET_COIN_RATES';
export const setSelectedPair = 'SET_SELECTED_PAİR';

export default {
  setSelectedCoinAndCurrency,
  setIsAnonymousFlow,
  getMarketInformation,
  setMarketInformation,
  setStockMode,
  setCoinRates,
  setSelectedPair,
};
