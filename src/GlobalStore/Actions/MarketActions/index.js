import * as MarketActionConstants from './constants';

export const setSelectedCoinAndCurrencyCode = data => {
  return {
    type: MarketActionConstants.setSelectedCoinAndCurrency,
    payload: data,
  };
};

export const setIsAnonymousFlow = data => {
  return {
    type: MarketActionConstants.setIsAnonymousFlow,
    payload: data,
  };
};

export const setSelectedStockMode = selectedMode => {
  return {
    type: MarketActionConstants.setSelectedStockMode,
    payload: selectedMode,
  };
};

export const getMarketInformationAction = payload => {
  return {
    type: MarketActionConstants.getMarketInformation,
    payload,
  };
};

export const setMarketInformationAction = payload => {
  return {
    type: MarketActionConstants.setMarketInformation,
    payload,
  };
};

export const setStockModeAction = payload => {
  return {
    type: MarketActionConstants.setStockMode,
    payload,
  };
};

export const setCoinRates = payload => {
  return {
    type: MarketActionConstants.setCoinRates,
    payload,
  };
};
export const setSelectedPair = payload => {
  return {
    type: MarketActionConstants.setSelectedPair,
    payload,
  };
};

export default {
  setSelectedCoinAndCurrencyCode,
  setSelectedStockMode,
  setIsAnonymousFlow,
  setMarketInformationAction,
  setStockModeAction,
  setCoinRates,
  setSelectedPair,
};
