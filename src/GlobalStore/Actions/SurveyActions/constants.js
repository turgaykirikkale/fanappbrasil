export const GetSurveys = 'GET_SURVEYS';
export const SetSurveys = 'SET_SURVEYS';

export default {
  GetSurveys,
  SetSurveys,
};
