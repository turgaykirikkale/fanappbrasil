import * as SurveyActionConstants from './constants';

export const getSurveyAction = () => {
  return {
    type: SurveyActionConstants.GetSurveys,
  };
};

export const setSurveyAction = payload => {
  return {
    type: SurveyActionConstants.SetSurveys,
    payload,
  };
};

export default {
  getSurveyAction,
  setSurveyAction,
};
