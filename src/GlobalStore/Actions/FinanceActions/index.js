import * as FinanceActionsConstants from './constants';

export const getCustomerFinance = incomingCurrencyId => {
  return {
    type: FinanceActionsConstants.getCustomerFinance,
    payload: incomingCurrencyId,
  };
};

export const setCustomerFinance = data => {
  return {
    type: FinanceActionsConstants.setCustomerFinance,
    payload: data,
  };
};
export default {
  getCustomerFinance,
  setCustomerFinance,
};
