import * as NewsActionsConstants from './constants';

export const setNewsAction = payload => {
  return {
    type: NewsActionsConstants.SetNews,
    payload,
  };
};

export const getNewsAction = () => {
  return {
    type: NewsActionsConstants.GetNews,
  };
};

export default {
  setNewsAction,
  getNewsAction,
};
