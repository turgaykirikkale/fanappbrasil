export const GetNews = 'GET_NEWS';
export const SetNews = 'SET_NEWS';

export default {
  GetNews,
  SetNews,
};
