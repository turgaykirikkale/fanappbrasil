export const preLoginInitializeDataCall = 'PRE_LOGIN_INITIALIZE_DATA_CALL';
export const initializeDataCall = 'INITIALIZE_DATA_CALL';
export const resetState = 'RESET_STATE';

export default {
  preLoginInitializeDataCall,
  initializeDataCall,
  resetState,
};
