import * as FlowActionConstants from './constants';

export const initializeDataCallAction = () => {
  return {
    type: FlowActionConstants.initializeDataCall,
  };
};
export const preLoginInitializeDataCallAction = () => {
  return {
    type: FlowActionConstants.preLoginInitializeDataCall,
  };
};
export const resetState = () => {
  return {
    type: FlowActionConstants.resetState,
  };
};

export default {
  preLoginInitializeDataCallAction,
  initializeDataCallAction,
  resetState,
};
