import * as UserActionConstants from './constants';

export const getUserInformationAction = () => {
  return {
    type: UserActionConstants.getUserInformation,
    payload: null,
  };
};

export const setUserInformationAction = data => {
  return {
    type: UserActionConstants.setUserInformation,
    payload: data,
  };
};

export const getUserFinanceStateAction = () => {
  return {
    type: UserActionConstants.getUserFinanceState,
    payload: null,
  };
};

export const setUserFinanceStateAction = data => {
  return {
    type: UserActionConstants.setUserFinanceState,
    payload: data,
  };
};
export const changeLanguage = data => {
  return {
    type: UserActionConstants.changeLanguage,
    payload: data,
  };
};

export const setUserPusherPrivateKey = data => {
  return {
    type: UserActionConstants.setPusherPrivateKey,
    payload: data,
  };
};

export default {
  getUserInformationAction,
  setUserInformationAction,
  getUserFinanceStateAction,
  setUserFinanceStateAction,
  changeLanguage,
  setUserPusherPrivateKey,
};
