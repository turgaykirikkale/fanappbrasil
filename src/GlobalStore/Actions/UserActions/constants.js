export const getUserInformation = 'GET_USER_INFORMATION_ACTION';
export const setUserInformation = 'SET_USER_INFORMATION_ACTION';
export const getUserInformationError = 'GET_USER_INFORMATION_ACTION_ERROR';
export const setUserFinanceState = 'SET_USER_FINANCE_STATE_ACTION';
export const getUserFinanceState = 'GET_USER_FINANCE_STATE_ACTION';
export const changeLanguage = 'CHANGE_LANGUAGE';
export const setPusherPrivateKey = 'SET_PUSHER_PRIVATE_KEY';
export default {
  getUserInformation,
  setUserInformation,
  getUserInformationError,
  setUserFinanceState,
  getUserFinanceState,
  changeLanguage,
  setPusherPrivateKey,
};
