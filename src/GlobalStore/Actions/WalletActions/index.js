import * as WalletActionConstants from './constants';

export const getPaymentChannelListAction = incomingCurrencyId => {
  return {
    type: WalletActionConstants.getPaymentChannelList,
    payload: incomingCurrencyId,
  };
};

export const setPaymentChannelListAction = data => {
  return {
    type: WalletActionConstants.setPaymentChannelList,
    payload: data,
  };
};

export const getUserWalletsAction = () => {
  return {
    type: WalletActionConstants.getUserWallets,
    payload: null,
  };
};

export const setUserWalletAction = payload => {
  return {
    type: WalletActionConstants.setUserWallets,
    payload,
  };
};

export default {
  getPaymentChannelListAction,
  setPaymentChannelListAction,
  getUserWalletsAction,
  setUserWalletAction,
};
