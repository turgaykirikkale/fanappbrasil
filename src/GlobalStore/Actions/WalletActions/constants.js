export const getPaymentChannelList = 'GET_PAYMENT_CHANNEL_LIST';
export const setPaymentChannelList = 'SET_PAYMENT_CHANNEL_LIST';
export const getUserWallets = 'GET_USER_WALLETS';
export const setUserWallets = 'SET_USER_WALLETS';

export default {
  getPaymentChannelList,
  setPaymentChannelList,
  getUserWallets,
  setUserWallets,
};
