import {call, put, takeLatest, fork} from 'redux-saga/effects';
import _ from 'lodash';
import FAService from '@Services';
import {FAToFixed} from '@Commons/FAMath';

import * as UserActionConstants from '@GlobalStore/Actions/UserActions/constants';
import * as UserActions from '@GlobalStore/Actions/UserActions';
import {toFixedNoRounding} from '@Commons/FAMath';

import * as WalletActionConstants from '@GlobalStore/Actions/WalletActions/constants';
import * as WalletActions from '@GlobalStore/Actions/WalletActions';
import * as FlowActionConstants from '@GlobalStore/Actions/FlowActions/constants';

import * as MarketActions from '@GlobalStore/Actions/MarketActions';
import * as MarketActionConstants from '@GlobalStore/Actions/MarketActions/constants';

import * as EventsActionConstants from '@GlobalStore/Actions/EventActions/constants';
import * as EventActions from '@GlobalStore/Actions/EventActions';

import * as SurveyActionConstans from '@GlobalStore/Actions/SurveyActions/constants';
import * as SurveyActions from '@GlobalStore/Actions/SurveyActions';

import * as NewsActionConstants from '@GlobalStore/Actions/NewsActions/constants';
import * as NewsActions from '@GlobalStore/Actions/NewsActions';

import {AppConstants} from '@Commons/Contants';

const filterFreeEvents = incomingEventList => {
  return _.filter(incomingEventList, event => event.Price <= 0 && event.Title);
};
const filterFreeSurveys = incomingSurveyList => {
  return _.filter(
    incomingSurveyList,
    survey => survey.Price <= 0 && survey.Title,
  );
};

const filterListByJoined = (list, isJoined) => {
  return _.filter(list, item => item.Joined === isJoined && item.Title);
};

const filterListByVotedAndActive = (list, isVoted) => {
  return _.filter(
    list,
    item => item.Voted === isVoted && item.DaysLeft !== 0 && item.Title,
  );
};
const filterListByVoted = (list, isVoted) => {
  return _.filter(list, item => item.Voted === isVoted && item.Title);
};

const filterListByDaysLeft = (list, isVoted) => {
  return _.filter(
    list,
    item => item.Voted === isVoted && item.DaysLeft === 0 && item.Title,
  );
};

function* initializeDataCall() {
  try {
    yield fork(getPusherPrivateKeyAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getPusherPrivateKeyAndSetToStore error');
  }

  try {
    yield fork(getMarketInformationAndSetToStore, {
      payload: {
        CurrencyId: AppConstants.CurrencyId,
        CoinId: AppConstants.CoinId,
      },
    });
  } catch (e) {
    console.log('initializeDataCall getMarketInformationAndSetToStore error');
  }

  try {
    yield fork(getUserInformationAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getUserInformationAndSetToStore error');
  }
  try {
    yield fork(getCoinRatesAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getCoinRatesAndSetToStore error');
  }

  try {
    yield fork(getUserWalletAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getUserWalletAndSetToStore error');
  }

  try {
    yield fork(initializePairDetailCall);
  } catch (e) {
    console.log('initializeDataCall initializePairDetailCall error');
  }
  try {
    yield fork(getUserFinanceState);
  } catch (error) {
    console.log('initializeDataCall getUserFinanceState error');
  }
  try {
    yield fork(getNewsAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getNewsAndSetToStore error');
  }
  try {
    yield fork(getAllEventsAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getAllEventsAndSetToStore error');
  }
  try {
    yield fork(getAllSurveysAndSetToStore);
  } catch (e) {
    console.log('initializeDataCall getActiveSurveysAndSetToStore error');
  }
}

function* preLoginInitializeDataCall() {
  try {
    yield fork(initializePairDetailCall);
  } catch (e) {
    console.log('preLoginInitializeDataCall initializePairDetailCall error');
  }
  try {
    yield fork(getCoinRatesAndSetToStore);
  } catch (e) {
    console.log('preLoginInitializeDataCall getCoinRatesAndSetToStore error');
  }

  try {
    yield fork(getMarketInformationAndSetToStore, {
      payload: {
        CurrencyId: AppConstants.CurrencyId,
        CoinId: AppConstants.CoinId,
      },
    });
  } catch (e) {
    console.log(
      'preLoginInitializeDataCall getMarketInformationAndSetToStore error',
    );
  }

  try {
    yield fork(getNewsAndSetToStore);
  } catch (e) {
    console.log('preLoginInitializeDataCall getNewsAndSetToStore error');
  }
}

function* getMarketInformationAndSetToStore({payload}) {
  try {
    const requestBody = payload || undefined;
    const marketInformationResponse = yield call(
      FAService.GetMarketInformation,
      requestBody,
    );
    if (marketInformationResponse?.data?.CommissionRate) {
      yield put(
        MarketActions.setMarketInformationAction(
          marketInformationResponse.data,
        ),
      );
    }
  } catch (e) {
    console.log('Saga getMarketInformationAndSetToStore error', e);
  }
}

function* getPusherPrivateKeyAndSetToStore() {
  try {
    const incomingPusherPrivateKeyResponse = yield call(
      FAService.GetPusherPrivateKey,
    );

    if (
      incomingPusherPrivateKeyResponse &&
      incomingPusherPrivateKeyResponse.data &&
      incomingPusherPrivateKeyResponse.data.AuthCode
    ) {
      yield put(
        UserActions.setUserPusherPrivateKey(
          incomingPusherPrivateKeyResponse.data.AuthCode,
        ),
      );
    }
  } catch (e) {
    console.log(e);
  }
}

function* getAllEventsAndSetToStore() {
  try {
    const params = {
      // coinId: 0,
      // coinId: 92,
    };
    const activeResponse = yield call(FAService.GetActiveEvents, params);
    const passedResponse = yield call(FAService.GetPassedEvents, params);

    debugger;
    let object = {};
    if (activeResponse?.data) {
      const incomingEvents = activeResponse.data;
      const notJoinedActiveEvents = filterListByJoined(incomingEvents, false);
      const joinedActiveEvents = filterListByJoined(incomingEvents, true);
      object.ActiveEvents = {
        All: notJoinedActiveEvents,
        Free: filterFreeEvents(notJoinedActiveEvents),
      };
      object.JoinedEvents = {
        All: joinedActiveEvents,
        Free: filterFreeEvents(joinedActiveEvents),
      };
    }

    if (passedResponse?.data) {
      const incomingEvents = passedResponse.data;
      const notJoinedPassedEvents = filterListByJoined(incomingEvents, false);
      const joinedPassedEvents = filterListByJoined(incomingEvents, true);
      object.PassedEvents = {
        All: notJoinedPassedEvents,
        Free: filterFreeEvents(notJoinedPassedEvents),
      };
      object.JoinedEvents = {
        All: [...object.JoinedEvents.All, ...joinedPassedEvents],
        Free: [
          ...object.JoinedEvents.Free,
          ...filterFreeEvents(joinedPassedEvents),
        ],
      };
    }

    yield put(EventActions.SetEventsAction(object));
  } catch (e) {
    console.log('Saga getAllEventsAndSetToStore error');
  }
}

function* getAllSurveysAndSetToStore() {
  try {
    const params = {
      // coinId: 92,
      // coinId: 0,
    };
    const activeSurveysResponse = yield call(FAService.GetActiveSurvey, {
      params: params,
    });
    const passedSurveysResponse = yield call(FAService.GetPassedSurvey, params);

    let object = {};
    debugger;
    if (activeSurveysResponse?.data) {
      const incomingSurveys = activeSurveysResponse.data;

      const notVotedAndActiveSurveys = filterListByVotedAndActive(
        incomingSurveys,
        false,
      );

      const notVotedAndPassedSurveys = filterListByDaysLeft(
        incomingSurveys,
        false,
      );

      const votedSurveys = filterListByVoted(incomingSurveys, true);

      object.ActiveSurveys = {
        All: notVotedAndActiveSurveys,
        Free: filterFreeSurveys(notVotedAndActiveSurveys),
      };
      object.VotedSurveys = {
        All: votedSurveys,
        Free: filterFreeSurveys(votedSurveys),
      };
      object.PassedSurveys = {
        All: notVotedAndPassedSurveys,
        Free: filterFreeSurveys(notVotedAndPassedSurveys),
      };
    }
    if (passedSurveysResponse?.data) {
      const incomingSurveys = passedSurveysResponse.data;
      const notVotedPassedSurveys = filterListByVoted(incomingSurveys, false);
      const votedPassedSurveys = filterListByVoted(incomingSurveys, true);
      object.PassedSurveys = {
        All: [...object.PassedSurveys.All, ...notVotedPassedSurveys],
        Free: [
          ...object.PassedSurveys.Free,
          ...filterFreeSurveys(notVotedPassedSurveys),
        ],
      };
      object.VotedSurveys = {
        All: [...object.VotedSurveys.All, ...votedPassedSurveys],
        Free: [
          ...object.VotedSurveys.Free,
          ...filterFreeSurveys(votedPassedSurveys),
        ],
      };
    }

    yield put(SurveyActions.setSurveyAction(object));
  } catch (error) {
    console.log('Saga getActiveSurveysAndSetToStore error', error);
  }
}

function* getNewsAndSetToStore() {
  try {
    const serviceResponse = yield call(FAService.GetNewsByBranch);
    debugger;
    if (serviceResponse.status === 200 && serviceResponse?.data?.Contents) {
      const contents = _.filter(serviceResponse.data.Contents, item => {
        return item.Title;
      });
      yield put(NewsActions.setNewsAction(contents));
    }
  } catch (error) {
    console.log('Saga getNewsAndSetToStore error', error);
  }
}

function* initializePairDetailCall() {
  try {
    const payload = {
      SelectedCoinCode: AppConstants.CoinCode,
      SelectedCurrencyCode: AppConstants.CurrencyCode,
      SelectedPair: {
        CoinId: AppConstants.CoinId,
        CoinCode: AppConstants.CoinCode,
        CoinName: AppConstants.CoinName,
        CurrencyId: AppConstants.CurrencyId,
        CurrencyCode: AppConstants.CurrencyCode,
      },
    };
    yield put(MarketActions.setSelectedCoinAndCurrencyCode(payload));
  } catch (e) {
    console.log('initializePairDetailCall error');
  }
}

function* getCoinRatesAndSetToStore() {
  try {
    const serviceResponse = yield call(FAService.GetCoinRates);
    console.log('initializeGetCOinRates', serviceResponse);
    let filteredMarketList = _.filter(serviceResponse?.data, {CoinCode: 'BFT'});
    console.log('filteredMarketList', filteredMarketList);

    yield put(MarketActions.setCoinRates(filteredMarketList));
  } catch (error) {
    console.log('Saga getCoinRatesAndSetToStore error', error);
  }
}

function* getUserWalletAndSetToStore() {
  try {
    const incomingResponse = yield call(FAService.GetUserWallet);
    if (
      incomingResponse &&
      incomingResponse.data &&
      incomingResponse.status === 200
    ) {
      let walletData = [];
      if (incomingResponse.data.length && incomingResponse.data.length > 0) {
        _.each(incomingResponse.data, item => {
          if (item && item.Balance) {
            item.Balance = FAToFixed(item.Balance);
          }
          walletData.push(item);
        });
      }
      walletData = _.sortBy(walletData, 'Name');
      yield put(WalletActions.setUserWalletAction(walletData));
    }
  } catch (error) {
    console.log('Saga getUserWalletAndSetToStore() error');
    console.log(error);
  }
}

function* getUserInformationAndSetToStore() {
  try {
    const customerInfoResponse = yield call(FAService.GetCustomerInfo);

    if (
      customerInfoResponse &&
      customerInfoResponse.data &&
      customerInfoResponse.status === 200
    ) {
      yield put(
        UserActions.setUserInformationAction(customerInfoResponse.data),
      );
    }
  } catch (e) {
    console.log('getUserInformationAndSetToStore exception');
    console.log(e);
  }
}

function* getUserFinanceState() {
  try {
    const requestBody = {
      CurrencyId: 1,
    };
    const serviceResponse = yield call(
      FAService.GetUserFinanceState,
      requestBody,
    );
    let incomingResponse = [];
    if (
      serviceResponse &&
      serviceResponse.status === 200 &&
      serviceResponse.data
    ) {
      const {CustomerCoinBalanceDetailList} =
        serviceResponse?.data?.CustomerCoinBalanceDetailList;
      if (
        CustomerCoinBalanceDetailList &&
        CustomerCoinBalanceDetailList.length &&
        CustomerCoinBalanceDetailList.length > 0
      ) {
        const generatedCoinBalanceDetailList = _.filter(
          CustomerCoinBalanceDetailList,
          item => {
            return (
              item.TotalBalance && toFixedNoRounding(item.TotalBalance, 8) > 0.0
            );
          },
        );
        incomingResponse = {
          ...incomingResponse,
          CustomerCoinBalanceDetailList: generatedCoinBalanceDetailList,
        };
      } else {
        incomingResponse = serviceResponse.data;
      }
    }
    yield put(UserActions.setUserFinanceStateAction(incomingResponse));
  } catch (error) {}
}

export default function* rootSaga() {
  yield takeLatest(FlowActionConstants.initializeDataCall, initializeDataCall);
  yield takeLatest(
    FlowActionConstants.preLoginInitializeDataCall,
    preLoginInitializeDataCall,
  );
  yield takeLatest(
    UserActionConstants.getUserInformation,
    getUserInformationAndSetToStore,
  );
  yield takeLatest(
    UserActionConstants.getUserFinanceState,
    getUserFinanceState,
  );
  yield takeLatest(
    MarketActionConstants.getMarketInformation,
    getMarketInformationAndSetToStore,
  );
  yield takeLatest(
    WalletActionConstants.getUserWallets,
    getUserWalletAndSetToStore,
  );
  yield takeLatest(EventsActionConstants.GetEvents, getAllEventsAndSetToStore);
  yield takeLatest(SurveyActionConstans.GetSurveys, getAllSurveysAndSetToStore);
  yield takeLatest(NewsActionConstants.GetNews, getNewsAndSetToStore);
}
