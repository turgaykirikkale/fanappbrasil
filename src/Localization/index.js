import i18n from 'i18next';
import {getLocales} from 'react-native-localize';
import tr from './tr';
import en from './en';
import pt from './pt';
import {initReactI18next} from 'react-i18next';

i18n.use(initReactI18next).init({
  lng: getLocales()[0].languageCode,
  fallbackLng: 'en',
  resources: {
    en: {translation: en},
    tr: {translation: tr},
    pt: {translation: pt},
  },
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
