import {StyleSheet, Dimensions} from 'react-native';

const screenWidth = {width: Dimensions.get('window').width}.width;

export const styles = StyleSheet.create({
  headerContainer: {
    marginHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 4,
  },
  FlatListMainContainer: {
    marginVertical: 8,
    marginHorizontal: 10,
  },
  absouluteViewContainer: {
    width: '100%',
    height: 20,
    position: 'absolute',
    top: 0,
    zIndex: 99,
    backgroundColor: 'black',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  priceTextStyle: {
    color: 'white',
    letterSpacing: 0.6,
    fontWeight: '500',
  },
  imageContainerStyle: {
    borderRadius: 4,
    width: screenWidth / 3,
    height: screenWidth / 2.1,
  },
});
