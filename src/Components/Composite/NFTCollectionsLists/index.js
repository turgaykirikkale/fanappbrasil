import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
} from 'react-native';
import FAListHeader from '@Components/Composite/FAListHeader';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {styles} from './assets/styles';

const NFTCollectionsLists = props => {
  const {onPressNavigate, data, title} = props;
  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <View>
            <View
              style={[
                styles.headerContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <FAListHeader
                theme={theme}
                fontColor={generateRandomColor()}
                resetPadding
                title={title}
                iconName={'plus-square'}
                onPress={() => onPressNavigate && onPressNavigate()}
              />
            </View>
            <View style={styles.FlatListMainContainer}>
              <FlatList
                syle={{marginLeft: 10}}
                ItemSeparatorComponent={() => (
                  <View style={{marginRight: 10}} />
                )}
                keyExtractor={(item, index) => index}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => {
                  return (
                    <TouchableOpacity>
                      <View style={styles.absouluteViewContainer}>
                        <Text style={styles.priceTextStyle}>Fiyat</Text>
                      </View>
                      {/* <Text
                        numberOfLines={2}
                        style={{
                          position: 'absolute',
                          bottom: 0,
                          zIndex: 99,
                          color: 'black',
                          marginHorizontal: 4,
                          letterSpacing: 0.6,
                          fontWeight: '500',
                        }}>
                        Lorem Ipsum dolor sit amet,consetetur
                      </Text> */}
                      <Image
                        resizeMode={'cover'}
                        style={styles.imageContainerStyle}
                        source={item.image}
                      />
                    </TouchableOpacity>
                  );
                }}
                data={data}
                horizontal
              />
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};
export default NFTCollectionsLists;
