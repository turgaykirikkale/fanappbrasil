import {FlatList, View} from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';
import FAProgressItem from '../FAProgressItem';

const FAActiveOrderList = props => {
  const {data, onOrderCancelSuccess, showCodes} = props;
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      nestedScrollEnabled
      keyExtractor={(item, index) => index}
      data={data}
      renderItem={({item, index}) => {
        const orderPercent =
          ((item.StartCoinValue - item.CoinValue) * 100) / item.StartCoinValue;
        return (
          <FAProgressItem
            currencyCode={showCodes ? item.CurrencyCode : null}
            coinCode={showCodes ? item.CoinCode : null}
            onOrderCancelSuccess={() =>
              onOrderCancelSuccess && onOrderCancelSuccess()
            }
            item={item}
            type={item.Type}
            occuredAmount={item.StartCoinValue - item.CoinValue}
            totalAmount={item.StartCoinValue}
            priceLeft={item.StartTotalValue - item.Total}
            priceRight={item.StartTotalValue}
            price={item.Price}
            percent={orderPercent}
            date={item.CreateDate}
          />
        );
      }}
    />
  );
};

FAActiveOrderList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      Type: PropTypes.number.isRequired,
      StartCoinValue: PropTypes.number.isRequired,
      CoinValue: PropTypes.number.isRequired,
      StartTotalValue: PropTypes.number.isRequired,
      Total: PropTypes.number.isRequired,
      CreateDate: PropTypes.string.isRequired,
      Price: PropTypes.number.isRequired,
    }),
  ).isRequired,
  onOrderCancelSuccess: PropTypes.func,
  showCodes: PropTypes.bool,
};

FAActiveOrderList.defaultProps = {
  showCodes: false,
};

export default FAActiveOrderList;
