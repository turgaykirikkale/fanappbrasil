import React from 'react';
import {View, Text} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import Localization from '@Localization';
import FACurrencyAndCoinFormatter from '../../../Commons/FACurrencyFormat';

const FAWalletHeader = props => {
  const {
    currencyName,
    currencyCode,
    balance,
    onPressDepositButton,
    onPressWithdrawButton,
    onPressSwapButton,
    decimalCount,
    theme,
  } = props;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.flex1}>
        <Text
          style={[
            styles.headerCurrencyNameTextStyle,
            {color: theme?.colors?.lightGray},
          ]}>
          {`${currencyName} (${currencyCode})`}
        </Text>
        <Text
          style={[
            styles.headerBalanceTextStyle,
            {color: theme?.colors?.lightGray},
          ]}>
          {FACurrencyAndCoinFormatter(balance, decimalCount || 4)}
        </Text>
      </View>
      <View style={styles.buttonContainerStyle}>
        {onPressDepositButton && (
          <FAButton
            disabled={false}
            onPress={() => onPressDepositButton()}
            containerStyle={[
              styles.depositButtonContainer,
              {borderColor: theme?.colors?.green},
            ]}
            textStyle={[
              styles.depositButtonTextStyle,
              {color: theme?.colors?.green},
            ]}
            text={Localization.t('WalletFix.Deposit')}
          />
        )}
        {onPressWithdrawButton && (
          <FAButton
            disabled={false}
            onPress={() => onPressWithdrawButton()}
            containerStyle={[
              styles.withdrawButtonContainer,
              {borderColor: theme?.colors?.red},
            ]}
            textStyle={[
              styles.withdrawButtonTextStyle,
              {color: theme?.colors?.red},
            ]}
            text={Localization.t('WalletFix.Withdraw')}
          />
        )}
        {onPressSwapButton && (
          <FAButton
            disabled={false}
            onPress={() => onPressSwapButton()}
            containerStyle={[
              styles.withdrawButtonContainer,
              {borderColor: theme?.colors?.red},
            ]}
            textStyle={[
              styles.withdrawButtonTextStyle,
              {color: theme?.colors?.red},
            ]}
            text={Localization.t('Swap')}
          />
        )}
      </View>
    </View>
  );
};

export default FAWalletHeader;
