import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  flex1: {flex: 1},
  headerCurrencyNameTextStyle: {color: '#9B9B9B', fontSize: 14},
  headerBalanceTextStyle: {color: '#333333', fontSize: 12, marginTop: 3},
  buttonContainerStyle: {flex: 1, flexDirection: 'row'},
  depositButtonContainer: {
    borderWidth: 1,
    borderColor: '#03A66D',
    borderRadius: 4,
    flex: 1,
    marginRight: 4,
  },
  depositButtonTextStyle: {
    marginVertical: 6,
    color: '#03A66D',
    fontSize: 12,
  },
  withdrawButtonContainer: {
    borderWidth: 1,
    borderColor: '#C90950',
    borderRadius: 4,
    flex: 1,
    marginLeft: 4,
  },
  withdrawButtonTextStyle: {
    marginVertical: 6,
    color: '#C90950',
    fontSize: 12,
  },
});
