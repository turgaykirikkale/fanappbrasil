import React from 'react';
import {View, TouchableOpacity, Text, Image} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FANewsShower = props => {
  const {
    headerText,
    explainedText,
    video,
    onPressShare,
    image,
    imageInLocal,
    publishDate,
    content,
    onPress,
    language,
  } = props;

  const generatedImage = image ? (imageInLocal ? image : {uri: image}) : null;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={styles.mainContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => onPress && onPress()}>
            <View>
              <Image style={styles.imageContainer} source={generatedImage} />
            </View>
            <View style={styles.rightTopIconContainer}>
              <View
                style={[
                  styles.rightTopIconChildContainer,
                  {backgroundColor: theme?.colors?.black2},
                ]}>
                {video ? (
                  <View style={styles.insideIconContainer}>
                    <FAIcon
                      iconName={'FAVideo'}
                      color={theme?.colors?.white2}
                      size={20}
                    />
                  </View>
                ) : (
                  <View style={styles.insideIconContainer}>
                    <FAIcon
                      iconName={'FAPages'}
                      color={theme?.colors?.white2}
                      size={20}
                    />
                  </View>
                )}
              </View>
            </View>
            {headerText || explainedText ? (
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 0, y: 0.5}}
                colors={['rgba(107,107,107,0)', 'rgba(0,0,0, .7)']}
                style={styles.textsMainContainer}>
                {headerText && (
                  <View style={styles.textsChildContainer}>
                    <Text
                      style={[
                        styles.headerTextStyle,
                        {color: theme?.colors?.white2},
                      ]}>{`${headerText}`}</Text>
                  </View>
                )}
                {explainedText && (
                  <Text
                    style={[
                      styles.explainedTextStyle,
                      {color: theme?.colors?.white2},
                    ]}>{`${explainedText}`}</Text>
                )}
              </LinearGradient>
            ) : null}
          </TouchableOpacity>
          <View
            style={[
              styles.bottomMainContainerStyle,
              {backgroundColor: theme?.colors?.white},
            ]}>
            {content && (
              <Text
                style={[
                  styles.bottomTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {content}
              </Text>
            )}
            <View
              style={[
                styles.buttonsContainerStyle,
                {marginTop: !content ? 12 : null},
              ]}>
              <Text
                style={[
                  styles.timeTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {moment(publishDate).locale(language).fromNow() || ''}
              </Text>
              <TouchableOpacity
                onPress={() => onPressShare && onPressShare()}
                activeOpacity={0.7}
                style={styles.shareButtonContainer}>
                <FAIcon iconName={'FAShare'} color={'#747474'} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FANewsShower;
