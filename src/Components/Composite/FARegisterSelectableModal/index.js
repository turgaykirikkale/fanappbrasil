import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, Text, ScrollView} from 'react-native';
import _ from 'lodash';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FASearchBar from '../../Composite/FASearchBar';
import FAIcon from '../../UI/FAIcon';

const FARegisterSelectableModal = props => {
  const [data, setData] = useState([]);
  useEffect(() => {
    setData(props.data);
  }, [props.data]);
  const searchEngineOnChange = value => {
    if (value.length > 0) {
      let newSearchedArr = [];
      _.filter(props.data, item => {
        if (
          value &&
          typeof value === 'string' &&
          item &&
          item.Name &&
          typeof item.Name === 'string' &&
          item.Name.toLowerCase().indexOf(value.toLowerCase()) > -1
        ) {
          newSearchedArr.push(item);
        }
      });
      setData(newSearchedArr);
    } else {
      setData(props.data);
    }
  };
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            // borderTopLeftRadius: 50,
            // borderTopRightRadius: 50,
            position: 'absolute',
            bottom: 0,
            height: '70%',
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)',
            // alignItems: 'center',
            // justifyContent: 'center'
            paddingHorizontal: 20,
            zIndex: 999,
          }}>
          <View style={{marginTop: 15, flex: 1}}>
            <TouchableOpacity
              onPress={() => props.closeModal && props.closeModal()}
              style={{
                marginBottom: 15,
                alignSelf: 'flex-end',
                height: 25,
                width: 25,
                backgroundColor: 'white',
                borderRadius: 12.5,
              }}>
              <FAIcon iconName={'FACross'} color={theme?.colors?.black} />
            </TouchableOpacity>
            <View>
              <FASearchBar
                type
                onChangeText={value => searchEngineOnChange(value)}
              />
            </View>
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{
                backgroundColor: theme?.colors?.white,
                // width: '90%',
                marginBottom: 40,
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
                // alignContent: 'center',
                flex: 1,
              }}>
              {_.map(data, item => {
                return (
                  <View
                    style={{
                      // borderWidth:
                      //   props.selectedContryCode === item.code ? null : 0.5,
                      borderColor: '#bcc6d1',
                      marginTop: 5,
                      marginHorizontal: 10,
                      borderRadius: 4,
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        props.controlSelectedItem &&
                        props.controlSelectedItem(item)
                      }
                      style={{
                        //   backgroundColor:
                        //     props.selectedContryCode === item.code ? '#c2edcf' : null,

                        // alignItems: 'center',
                        flexDirection: 'row',
                        paddingVertical: 10,
                        borderRadius: 4,
                        paddingHorizontal: 10,
                      }}>
                      <Text
                        style={{
                          // marginLeft: 20,
                          // flex: 2,
                          color: theme?.colors?.black,
                          marginVertical: 1,
                        }}>
                        {item.Name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
              <View style={{marginBottom: 5}} />
            </ScrollView>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FARegisterSelectableModal;
