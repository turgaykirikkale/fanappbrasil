import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';

const FALiveFixturHeader = props => {
  const {date} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: FAColor.VBlueGray,
        paddingVertical: 10,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View style={{width: 10, height: 10, marginRight: 5}}>
        <FAIcon iconName={'liveCamera'} color={FAColor.Green} />
      </View>
      <Text style={{fontSize: 12, color: FAColor.Green}}>Canlı Anlatım </Text>
      <Text
        style={{
          color: FAColor.Green,
          flex: 1,
          textAlign: 'center',
          marginLeft: 3,
        }}>
        88'
      </Text>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View style={{width: 10, height: 10, marginRight: 5}}>
          <FAIcon iconName={'FACalendar'} color={FAColor.Gray} />
        </View>
        <Text style={{color: FAColor.Gray, fontSize: 10}}>{date}</Text>
      </View>
    </View>
  );
};

export default FALiveFixturHeader;
