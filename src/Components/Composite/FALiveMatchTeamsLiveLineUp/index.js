import React from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
import FAIcon from '@Components/UI/FAIcon';

const FALiveMatchTeamsLiveLineUp = props => {
  const {left, item} = props;
  return (
    <View style={{alignItems: left ? null : 'flex-end'}}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {left ? (
          <Text style={{fontSize: 12, color: FAColor.DarkGray}}>
            {item.time}
          </Text>
        ) : (
          <Text style={{fontSize: 12, color: FAColor.DarkGray}}>
            {item.name}
          </Text>
        )}
        <View style={{width: 10, height: 10, marginHorizontal: 6}}>
          <FAIcon iconName={'FABall'} color={FAColor.DarkGray} />
        </View>
        {left ? (
          <Text style={{fontSize: 12, color: FAColor.DarkGray}}>
            {item.name}
          </Text>
        ) : (
          <Text
            style={{
              fontSize: 12,
              color: FAColor.DarkGray,
            }}>
            {item.time}
          </Text>
        )}
      </View>
    </View>
  );
};
export default FALiveMatchTeamsLiveLineUp;
