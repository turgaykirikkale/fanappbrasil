import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: 'black',
    flexDirection: 'row',
    paddingVertical: 9,
    alignContent: 'center',
  },
  fanTokenTextStyle: {
    color: 'white',
    marginLeft: 20,
    fontWeight: '500',
  },
  currencyTextStyle: {
    color: 'white',
    marginLeft: 20,
    flex: 1,
  },
  marginRight18: {
    marginRight: 18,
  },
});
