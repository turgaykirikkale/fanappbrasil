import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {styles} from './assets/styles';
import {AppConstants} from '@Commons/Contants';
import {connect} from 'react-redux';
import Localization from '@Localization';
import FAButton from '@Components/Composite/FAButton';
import _ from 'lodash';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';

const FAAssetShower = props => {
  const {UserState} = props;
  let type = props.MarketState?.isAnonymousFlow ? 1 : 2;

  const {onPress} = props;

  let fanTokenBalance = '0.00';
  let currencyBalance = '0.00';
  let userCoinBalanceDetail =
    UserState?.FinanceInfo?.CustomerCoinBalanceDetailList;
  if (_.find(userCoinBalanceDetail, {CoinCode: AppConstants.CoinCode})) {
    const fanToken = _.find(userCoinBalanceDetail, {
      CoinCode: AppConstants.CoinCode,
    });
    fanTokenBalance = FACurrencyAndCoinFormatter(
      Number(fanToken?.CoinBalance).toFixed(9),
      2,
    );
  }

  if (_.find(userCoinBalanceDetail, {CoinId: 29})) {
    const currency = _.find(userCoinBalanceDetail, {CoinId: 29});
    // TODO Appconstants'da currencyId =2007 olduğu için response undifened dönüyor.
    currencyBalance = FACurrencyAndCoinFormatter(
      Number(currency?.CoinBalance).toFixed(9),
      2,
    );
  }
  return type === 1 ? (
    <View
      style={[
        styles.mainContainer,
        {flexDirection: 'row', justifyContent: 'center'},
      ]}>
      <FAButton
        disabled={false}
        onPress={() => onPress && onPress(1)}
        containerStyle={{marginRight: 4}}
        textStyle={{color: 'white'}}
        text={Localization.t('FAAccountScreenIsAnonymousFlow.Login')}
      />
      <Text style={{color: 'white'}}>/</Text>
      <FAButton
        disabled={false}
        onPress={() => onPress && onPress(2)}
        containerStyle={{marginLeft: 4}}
        textStyle={{color: 'white'}}
        text={Localization.t('FAAccountScreenIsAnonymousFlow.Register')}
      />
    </View>
  ) : (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => onPress && onPress(3)}
      style={styles.mainContainer}>
      {AppConstants.CoinCode ? (
        <Text
          style={
            styles.fanTokenTextStyle
          }>{`${fanTokenBalance} ${AppConstants.CoinCode}`}</Text>
      ) : null}

      <Text style={styles.currencyTextStyle}>{`${currencyBalance} BITCI`}</Text>

      <View style={styles.marginRight18}>
        <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={18}
          height={18}
          viewBox="0 0 18 18"
          {...props}>
          <Path d="M0 0h18v18H0z" fill="none" />
          <Path
            d="M9.75 7.5h3.75L9 12 4.5 7.5h3.75V2.25h1.5zM3 14.25h12V9h1.5v6a.75.75 0 01-.75.75H2.25A.75.75 0 011.5 15V9H3z"
            fill="#fff"
          />
        </Svg>
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
    UserState: state.UserState,
  };
};
export default connect(mapStateToProps)(FAAssetShower);
