import React from 'react';
import {View, Text, Image} from 'react-native';
import {styles} from './assets/styles';
import Localization from '@Localization';
import LinearGradient from 'react-native-linear-gradient';

const FADrawImage = props => {
  const {date, explainedText, time, imageUrl, price, CoinCode, type} = props;
  return (
    <View>
      <View>
        <Image
          style={styles.imageContainer}
          // source={require('../../../Assets/images/korfez.jpg')}
          source={{
            uri: imageUrl,
          }}
          //propstan alıncak
        />
      </View>
      <View style={styles.rightTokencontainer}>
        <View style={styles.leftTimeChilfContainer}>
          <Text style={styles.leftTimeTextStyle}>
            {price && price !== '0'
              ? price
              : Localization.t('EventDetailScreen.Free')}
          </Text>
          {price && price !== '0' && (
            <Text style={[styles.leftTimeTextStyle, {marginLeft: 3}]}>
              {CoinCode}
            </Text>
          )}
        </View>
      </View>
      <View style={styles.leftTimeMainContainer}>
        <View style={styles.leftTimeChilfContainer}>
          <Text style={styles.leftTimeTextStyle}>{`${date} ${
            type === 0 ? Localization.t('SurveyScreenDetail.daysLeft') : ''
          }`}</Text>
        </View>
        <Text style={styles.timeTextStyle}>{time}</Text>
      </View>
      <LinearGradient
        style={styles.explainedTextLinear}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 0.4}}
        colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.8)']}>
        <Text style={styles.explainedTextStyle}>{explainedText}</Text>
      </LinearGradient>
    </View>
  );
};

export default FADrawImage;
