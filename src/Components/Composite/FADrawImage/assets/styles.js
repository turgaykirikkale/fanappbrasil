import {Dimensions, StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';
import {FAOrange} from '@Commons/FAColor';

const screenWidth = Dimensions.get('screen').width;
export const styles = StyleSheet.create({
  imageContainer: {width: '100%', height: 200, backgroundColor: FAOrange},
  rightTokencontainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    marginLeft: 12,
    marginTop: 20,
  },
  leftTimeMainContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    marginRight: 12,
    marginTop: 20,
  },
  leftTimeChilfContainer: {
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 9,
    borderRadius: 4,
    backgroundColor: '#000000B2',
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftTimeTextStyle: {
    color: FAColor.White,
    fontSize: 14,
    fontWeight: '700',
  },
  timeTextStyle: {
    color: FAColor.White,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
    marginTop: 3,
  },
  explainedTextLinear: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 18,
    borderRadius: 5,
    paddingHorizontal: 24,
  },
  explainedTextContainer: {
    position: 'absolute',
    bottom: 0,
    marginHorizontal: 24,
    marginBottom: 14,
  },
  explainedTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    letterSpacing: -0.8,
    paddingTop: 38,
  },
});
