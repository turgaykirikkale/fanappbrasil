import {generateRandomColor} from '@Helpers/RandomColorHelper';
import {Dimensions, ScrollView, View} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {DarkBlue, White} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import React from 'react';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FACustomTabView = props => {
  const {
    onIndexChange,
    currentIndex,
    routes,
    customLabelStyle,
    customContainerStyle,
    customIndicatorStyle,
    activeTextColor,
    passiveTextColor,
    backgroundColor,
    itemCountPerPage,
    scrollEnabled,
  } = props;
  const initialLayout = Dimensions.get('window').width;
  const tabItemWidth =
    initialLayout / (itemCountPerPage || routes?.length || 3);
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <ScrollView
          scrollEnabled={scrollEnabled}
          showsHorizontalScrollIndicator={false}
          horizontal
          snapToInterval={tabItemWidth}
          decelerationRate={0.1}
          style={{backgroundColor: theme?.colors?.white || backgroundColor}}>
          {routes?.map((route, index) => {
            const randomColor = generateRandomColor();
            return (
              <View
                key={route.key}
                style={{width: tabItemWidth, position: 'relative'}}>
                <FAButton
                  onPress={() => {
                    if (currentIndex !== index) {
                      onIndexChange && onIndexChange(index);
                    }
                  }}
                  text={route.title}
                  containerStyle={[
                    {
                      paddingVertical: 10,
                    },
                    {...customContainerStyle},
                  ]}
                  textStyle={{
                    fontSize: customLabelStyle?.fontSize || 12,
                    color:
                      currentIndex === index
                        ? activeTextColor ||
                          theme?.colors?.faOrange ||
                          randomColor
                        : passiveTextColor || theme?.colors?.darkBlue,
                    textAlign: 'center',
                  }}
                />
                {currentIndex === index && (
                  <View
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      height: customIndicatorStyle.height || 1,
                      backgroundColor:
                        customIndicatorStyle?.backgroundColor ||
                        theme?.colors?.faOrange ||
                        randomColor,
                      width: tabItemWidth,
                    }}
                  />
                )}
              </View>
            );
          })}
        </ScrollView>
      )}
    </ThemeContext.Consumer>
  );
};

FACustomTabView.propTypes = {
  onIndexChange: PropTypes.func.isRequired,
  currentIndex: PropTypes.number.isRequired,
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
  ).isRequired,
  customLabelStyle: PropTypes.object,
  customIndicatorStyle: PropTypes.object,
  customContainerStyle: PropTypes.object,
  activeTextColor: PropTypes.string,
  passiveTextColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  itemCountPerPage: PropTypes.number,
  scrollEnabled: PropTypes.bool,
  theme: PropTypes.object.isRequired,
};

FACustomTabView.defaultProps = {
  scrollEnabled: true,
  customIndicatorStyle: {
    height: 1,
  },
  customLabelStyle: {
    fontSize: 12,
  },
  backgroundColor: White,
};

export default FACustomTabView;
