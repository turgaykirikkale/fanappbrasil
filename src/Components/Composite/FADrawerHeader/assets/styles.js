import {Platform, StyleSheet} from 'react-native';
import {Black} from '@Commons/FAColor';

const menuButtonMarginLeft = 14;
const platformOs = Platform.OS;
const isAndroid = platformOs === 'android';
export const styles = StyleSheet.create({
  generalContainer: {flex: 1, backgroundColor: '#FFFFFF'},
  sideMenuMainContainer: {
    backgroundColor: 'white',
    flex: 1,
  },
  sideMenuContentContainer: {
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {width: 2, height: 0},
    shadowOpacity: 0.1,
    shadowRadius: 15,
    elevation: 1,
  },
  sideMenuContentItemContainer: {marginHorizontal: 14, marginTop: 10},
  sideMenuContentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ededed',
  },
  sideMenuContentItemTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#000000',
    flex: 1,
    marginLeft: 8,
  },
  sideMenuContentItemArrow: {
    color: '#9d9d9d',
    fontSize: 14,
  },
  sideMenuHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#000000',
    paddingHorizontal: 17,
    paddingVertical: 15,
  },
  sideMenuHeaderUserAvatar: {
    width: 45,
    height: 45,
    borderRadius: 45,
    borderWidth: 1,
  },
  sideMenuHeaderUserInformationContainer: {
    flex: 1,
    marginLeft: 12,
  },
  sideMenuHeaderUserFirstNameLastName: {
    fontSize: 12,
    color: '#FFFFFF',
    flex: 1,
  },
  sideMenuHeaderUserCoinBalanceContainer: {
    marginTop: 0,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  sideMenuHeaderUserCoinBalance: {
    fontSize: 12,
    fontWeight: '500',
    color: '#FFFFFF',
  },
  sideMenuHeaderUserCurrencyValue: {
    fontSize: 12,
    fontWeight: '500',
    color: 'rgba(255,255,255,0.8)',
    marginLeft: 9,
  },
  sideMenuFooterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 13,
    paddingVertical: 14,
  },
  sideMenuFooterLogoContainer: {flexDirection: 'row', alignItems: 'center'},
  sideMenuFooterLogoText: {fontSize: 7, color: '#454545'},
  sideMenuFooterLogoImage: {
    width: 65,
    height: 18,
    marginLeft: 4,
  },
  sideMenuFooterLogoutButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  sideMenuFooterLogoutButtonText: {
    marginLeft: 4,
    fontSize: 14,
    color: '#C90950',
  },
  screenSafeArea: {flex: 1},
  headerMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  },
  sideMenuButton: {
    marginLeft: menuButtonMarginLeft,
    height: '100%',
  },
  headerTitle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 12,
    color: Black,
    fontWeight: isAndroid ? '600' : '500',
  },
  emptyView: {
    width: menuButtonMarginLeft + 24,
  },
});
