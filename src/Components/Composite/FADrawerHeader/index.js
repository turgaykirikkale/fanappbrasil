import {
  Image,
  Platform,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Drawer from 'react-native-drawer';
import {styles} from './assets/styles';
import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import FAIcon from '@Components/UI/FAIcon';
import {resetState} from '@GlobalStore/Actions/FlowActions/index';
import {connect} from 'react-redux';
import {toFixedNoRounding} from '@Commons/FAMath';
import {AppConstants} from '@Commons/Contants';
import _ from 'lodash';
import FAButton from '@Components/Composite/FAButton';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import Localization from '@Localization';
import {SideMenuRoute} from '@Commons/Enums/SideMenuRoute';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {getDataFromStorage} from '@Commons/FAStorage';

const FADrawerHeader = props => {
  const selectedLanguage = props.UserState.Language;
  const SideMenuRouteList = [
    // {
    //   title: 'BITCICOIN',
    //   stack: null,
    //   screen: 'WebviewScreen',
    //   imageName: 'BITCI',
    //   showAnonymous: true,
    //   webviewUrl: 'https://bitci-fantokens.vercel.app/projects/BITCI/',
    //   // https://www.bitci.com/bitcicoin
    //   //Token web sayfasına
    //   //Coin icon koyulacak
    // },
    {
      title: Localization.t('Drawer.BFTTokenInfo'),
      // screen: 'FanTokenInfoScreen',
      showAnonymous: true,
      webviewUrl:
        selectedLanguage === 'pt'
          ? `https://www.bitcibrasil.com/projects/BFT/?custom=fanapp&lang=pt&theme=dark`
          : `https://www.bitcibrasil.com/en/projects/BFT/?custom=fanapp&lang=en&theme=dark`,
      imageName: 'BFT',
    },

    {
      title: Localization.t('Drawer.Wallet'),
      stack: 'WalletTabStackNavigator',
      // screen: SideMenuRoute.WalletScreen,
      icon: 'coins',
      showAnonymous: false,
    },

    {
      title: Localization.t('NFT'),
      stack: 'NFTTabStackNavigator',
      screen: SideMenuRoute.SurveysScreen,
      icon: 'FACooperCoinLine',
      showAnonymous: true,
    },
    {
      title: Localization.t('Drawer.Polls'),
      // stack: 'SurveyStack',
      screen: 'SurveysScreen',
      icon: 'FAChatPollLine',
      showAnonymous: true,
    },
    {
      title: Localization.t('Drawer.Events'),
      // stack: 'EventStack',
      screen: 'EventsScreen',
      icon: 'FACalendarEventLine',
      showAnonymous: true,
    },

    // {
    //   title: Localization.t('Drawer.Settings'),
    //   stack: 'SettingStack',
    //   // screen: SideMenuRoute.SettingsScreen,
    //   icon: 'FASettingLine',
    //   showAnonymous: true,
    //   // Güvenlik, dil ayarları, tema, alarm, kripto çevirici, yardım, referans, komisyon
    // },
  ];

  const {MarketState, UserState} = props;
  const isAnonymousFlow = MarketState?.isAnonymousFlow;

  const [currencyBalance, setCurrencyBalance] = useState(0.0);
  const [CurrencyCode, setCurrencyCode] = useState(0.0);

  useEffect(() => {
    if (UserState?.FinanceInfo) {
      const {CustomerCoinBalanceDetailList} = UserState?.FinanceInfo;
      const {CashMoneyBalanceList} = UserState?.FinanceInfo;

      const currencyBalanceDetail = _.find(CashMoneyBalanceList, {
        CurrencyId: 6, // TODO Appconstants'da currencyId =2007 olduğu için response undifened dönüyor.
      });

      console.log(toFixedNoRounding(currencyBalanceDetail?.CoinBalance, 4));

      setCurrencyBalance(toFixedNoRounding(currencyBalanceDetail?.Amount, 4));
      setCurrencyCode(currencyBalanceDetail?.CurrencyCode);
    }
  }, [UserState]);

  let _drawerRef = null;
  const {title, children, navigation} = props;
  const insets = useSafeAreaInsets();

  const openSideMenu = () => {
    if (_drawerRef) {
      _drawerRef.open();
    }
  };

  const navigateToScreen = (menuItem, theme) => {
    if (_drawerRef) {
      _drawerRef.close();
    }
    if (menuItem.webviewUrl !== undefined) {
      props.navigation.navigate('WebviewScreen', {
        url: `${menuItem.webviewUrl}?mode=${theme.name || null}`,
        title: menuItem.title,
      });
    } else {
      if (menuItem && menuItem.stack) {
        navigation.navigate(menuItem.stack, {
          screen: menuItem.screen,
          params: {
            routeMain: props.routeMain,
          },
        });
      } else if (menuItem) {
        navigation.navigate(menuItem.screen, {
          routeMain: props.routeMain,
        });
      }
    }
  };

  const menuButtonSize = 20;
  const menuButtonMarginLeft = 10;
  let paddingVertical = 15;
  if (Platform.OS === 'android') {
    paddingVertical = 20;
  }

  const Approved = UserState?.UserInfo?.Approved;

  console.log('Props', props);

  return (
    <ThemeContext.Consumer>
      {({theme, updateTheme}) => (
        <View
          style={[
            styles.generalContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <Drawer
            type={'overlay'}
            panCloseMask={0.2}
            closedDrawerOffset={0}
            tapToClose={true}
            openDrawerOffset={0.1}
            ref={ref => (_drawerRef = ref)}
            tweenHandler={ratio => ({
              main: {opacity: (2 - ratio) / 0.5},
            })}
            content={
              <View
                style={[
                  styles.sideMenuContentContainer,
                  {
                    paddingTop: insets.top,
                  },
                ]}>
                <View
                  style={[
                    styles.sideMenuHeaderContainer,
                    {backgroundColor: theme?.colors?.blueGray},
                  ]}>
                  <Image
                    width={45}
                    height={45}
                    style={[
                      styles.sideMenuHeaderUserAvatar,
                      {borderColor: theme?.colors?.black},
                    ]}
                    // source={{
                    //   uri: 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
                    // }}
                    source={require('./assets/CBF.png')}
                  />
                  <View style={styles.sideMenuHeaderUserInformationContainer}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        flex: 1,
                      }}>
                      <Text
                        style={[
                          styles.sideMenuHeaderUserFirstNameLastName,
                          {color: theme?.colors?.black},
                        ]}>
                        {isAnonymousFlow
                          ? Localization.t('Drawer.Hello')
                          : `${UserState?.UserInfo?.NameSurname}`}
                      </Text>
                      {isAnonymousFlow ? null : Approved ? (
                        <Text
                          style={{
                            fontSize: 10,
                            color: theme?.colors?.green,
                            alignSelf: 'center',
                            fontWeight: 'bold',
                          }}>
                          {Localization.t('Drawer.ApprovedAccount')}
                        </Text>
                      ) : (
                        <FAButton
                          disabled={false}
                          onPress={() => navigation.navigate('AccountSettings')}
                          textStyle={{
                            fontSize: 10,
                            color: theme?.colors?.red,
                            alignSelf: 'center',
                          }}
                          text={Localization.t('Drawer.ApproveMyAccount')}
                        />
                      )}
                    </View>
                    <View style={styles.sideMenuHeaderUserCoinBalanceContainer}>
                      {isAnonymousFlow ? (
                        <View style={{flexDirection: 'row'}}>
                          <FAButton
                            disabled={false}
                            onPress={() => navigation.navigate('LoginScreen')}
                            containerStyle={{marginRight: 4}}
                            textStyle={[
                              styles.sideMenuHeaderUserCoinBalance,
                              {color: theme?.colors?.black},
                            ]}
                            text={Localization.t('Drawer.Login')}
                          />
                          <Text
                            style={[
                              styles.sideMenuHeaderUserCoinBalance,
                              {color: theme?.colors?.black},
                            ]}>
                            /
                          </Text>
                          <FAButton
                            disabled={false}
                            onPress={() =>
                              navigation.navigate('RegisterStepOne')
                            }
                            containerStyle={{marginLeft: 4}}
                            textStyle={[
                              styles.sideMenuHeaderUserCoinBalance,
                              {color: theme?.colors?.black},
                            ]}
                            text={Localization.t('Drawer.Register')}
                          />
                        </View>
                      ) : (
                        <View>
                          <Text
                            style={[
                              styles.sideMenuHeaderUserCoinBalance,
                              {color: theme?.colors?.black},
                            ]}>
                            {`${FACurrencyAndCoinFormatter(
                              currencyBalance || '0.0',
                              AppConstants.CurrencyDecimalCount,
                            )} ${CurrencyCode}`}
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                </View>
                <View
                  style={[
                    styles.sideMenuMainContainer,
                    {backgroundColor: theme?.colors?.white},
                  ]}>
                  <ScrollView>
                    <View style={styles.sideMenuContentItemContainer}>
                      {SideMenuRouteList.map((menu, index) => {
                        if (!menu.showAnonymous && isAnonymousFlow) {
                          return null;
                        }
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[
                              styles.sideMenuContentItem,
                              {borderBottomColor: theme?.colors?.darkWhite},
                            ]}
                            activeOpacity={0.7}
                            onPress={() => navigateToScreen(menu, theme)}>
                            {menu.imageName ? (
                              <Image
                                style={{
                                  width: 15,
                                  height: 15,
                                  borderRadius: 7.5,
                                }}
                                source={require('./assets/CBF.png')}
                                // source={{
                                //   uri: `https://bitci.com/page/assets/coin?code=${menu.imageName}`,
                                //   headers: {
                                //     'User-Agent': AppConstants.UserAgent,
                                //   },
                                // }}
                              />
                            ) : (
                              <View style={{width: 15, height: 15}}>
                                <FAIcon
                                  iconName={menu.icon}
                                  color={theme?.colors?.black}
                                />
                              </View>
                            )}

                            <Text
                              style={[
                                styles.sideMenuContentItemTitle,
                                {color: theme?.colors?.black},
                              ]}>
                              {menu.title}
                            </Text>
                            <FAVectorIcon
                              iconName={'chevron-right'}
                              color={'#9d9d9d'}
                              size={12}
                            />
                          </TouchableOpacity>
                        );
                      })}
                    </View>

                    <View style={styles.sideMenuFooterContainer}>
                      <View style={styles.sideMenuFooterLogoContainer}>
                        {/* <Text
                          style={[
                            styles.sideMenuFooterLogoText,
                            {color: theme?.colors?.black},
                          ]}>
                          Powered By
                        </Text>
                        <Image
                          width={65}
                          height={18}
                          style={styles.sideMenuFooterLogoImage}
                          source={{
                            uri: 'https://www.bitci.com/assets/img/brand/bitci-original-black-logo.png',
                            headers: {
                              'User-Agent': AppConstants.UserAgent,
                            },
                          }}
                        /> */}
                      </View>
                    </View>
                  </ScrollView>
                </View>
              </View>
            }>
            <>
              <View style={{backgroundColor: theme?.colors?.white}}>
                <StatusBar barStyle={theme.statusBar} />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingVertical: paddingVertical,
                    backgroundColor: theme?.colors?.white,
                    borderBottomWidth: 0.5,
                    borderBottomColor: theme?.colors?.softGray,
                    marginTop: insets.top,
                  }}>
                  <TouchableOpacity
                    hitSlop={{left: 10, right: 20, top: 10, bottom: 10}}
                    activeOpacity={0.7}
                    onPress={() => openSideMenu()}
                    style={{marginLeft: menuButtonMarginLeft}}>
                    <FAVectorIcon
                      iconName={'bars'}
                      color={theme?.colors?.black}
                      size={menuButtonSize}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      marginLeft: menuButtonMarginLeft + menuButtonSize - 20,
                    }}
                  />
                  <Text
                    style={[styles.headerTitle, {color: theme?.colors?.black}]}>
                    {title || ''}
                  </Text>

                  <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() =>
                      updateTheme(theme.name === 'dark' ? 'light' : 'dark')
                    }
                    style={{marginRight: menuButtonMarginLeft}}>
                    {theme?.name === 'dark' ? (
                      <FAIcon iconName={'sun'} size={20} color={'#e3cb27'} />
                    ) : (
                      <FAIcon iconName={'moon'} size={20} color={'#23272A'} />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
              {children}
            </>
          </Drawer>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FADrawerHeader.propTypes = {
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {resetState})(FADrawerHeader);
