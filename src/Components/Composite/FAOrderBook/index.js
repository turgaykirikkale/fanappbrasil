import React from 'react';
import {FlatList} from 'react-native';
import FAOrderLine from '@Components/UI/FAOrderLine';
import PropTypes from 'prop-types';

const FAOrderBook = props => {
  const {data, volumeBarDirection, reverse, onPress, size, theme} = props;
  if (!data) {
    return null;
  }
  return (
    <FlatList
      data={data}
      renderItem={({item, index}) => (
        <FAOrderLine
          theme={theme}
          size={size}
          amount={item?.CoinValue}
          linePercent={item?.DifferencePercent}
          flowType={item?.Type}
          price={item?.Price}
          volumeBarDirection={volumeBarDirection}
          reverse={reverse}
          onPress={() => onPress && onPress(item?.Price)}
        />
      )}
    />
  );
};

FAOrderBook.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      CoinValue: PropTypes.number.isRequired,
      Type: PropTypes.number.isRequired,
      Price: PropTypes.number.isRequired,
      DifferencePercent: PropTypes.number.isRequired,
    }),
  ).isRequired,
  volumeBarDirection: PropTypes.oneOf(['left', 'right']),
  reverse: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  size: PropTypes.number,
  theme: PropTypes.object.isRequired,
};

export default FAOrderBook;
