import {StyleSheet} from 'react-native';
import {Black, VGray} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  inputIOS: {
    fontSize: 12,
    borderWidth: 1,
    borderColor: VGray,
    borderRadius: 4,
    color: Black,
    paddingVertical: 6,
    textAlign: 'center',
  },
  inputAndroid: {
    textAlign: 'center',
    fontSize: 12,
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: VGray,
    borderRadius: 4,
    color: Black,
  },

  placeholder: {
    color: Black,
    fontSize: 12,
  },
  inputNonBordered: {
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: Black,
    fontSize: 14,
  },
});
