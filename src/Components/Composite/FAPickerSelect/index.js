import FAVectorIcon from '@Components/UI/FAVectorIcon';
import RNPickerSelect from 'react-native-picker-select';
import * as PropTypes from 'prop-types';
import {styles} from './assets/styles';
import React from 'react';
import Localization from '@Localization';

export const FAPickerSelect = props => {
  const {
    list,
    value,
    onValueChange,
    nonBorder,
    paddingVertical,
    paddingHorizontal,
    useNativeAndroidPickerStyle,
    placeholder,
    theme,
  } = props;
  return (
    <>
      {list ? (
        <RNPickerSelect
          doneText={Localization.t('CommonsFix.Okay')}
          style={{
            inputIOS: [
              styles.inputIOS,
              {
                paddingHorizontal: paddingHorizontal || 15,
                borderColor: theme?.colors?.darkBlack,
                color: theme?.colors?.black,
              },
              nonBorder
                ? {...styles.inputNonBordered, color: theme?.colors?.black}
                : null,
            ],
            inputAndroid: [
              styles.inputAndroid,
              {
                paddingHorizontal: paddingHorizontal || 15,
                borderWidth: nonBorder ? 0 : 1,
                borderColor: theme?.colors?.darkBlack,
                color: theme?.colors?.black,
              },
            ],
            iconContainer: {
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 12,
            },
            placeholder: {
              ...styles.placeholder,
              color: theme?.colors?.black,
            },
          }}
          useNativeAndroidPickerStyle={useNativeAndroidPickerStyle || false}
          placeholder={placeholder || {}}
          onValueChange={value => onValueChange && onValueChange(value)}
          items={list}
          value={value}
          Icon={() => (
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'angle-down'}
              color={theme?.colors?.black}
              size={16}
            />
          )}
        />
      ) : null}
    </>
  );
};

FAPickerSelect.propTypes = {
  onValueChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
  nonBorder: PropTypes.bool,
  paddingVertical: PropTypes.number,
  paddingHorizontal: PropTypes.number,
  useNativeAndroidPickerStyle: PropTypes.bool,
  placeholder: PropTypes.string,
  theme: PropTypes.object.isRequired,
};

export default FAPickerSelect;
