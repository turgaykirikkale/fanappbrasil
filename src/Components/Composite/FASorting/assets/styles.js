import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.VGray,
    flexDirection: 'row',
  },
  textStyle: {color: FAColor.DarkGray, fontSize: 12, letterSpacing: -0.5},
  iconsContainer: {marginLeft: 6, alignItems: 'center'},
});
