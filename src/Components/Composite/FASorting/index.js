import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import {DarkGray, SoftGray} from '@Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

class FASorting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      angleUpFlow: props.value || false,
      angleDownFlow: props.value !== undefined ? !props.value : false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value === undefined) {
      this.setState({
        angleUpFlow: false,
        angleDownFlow: false,
      });
    }
  }

  setSitutation() {
    const {onPress} = this.props;
    const {angleUpFlow, angleDownFlow} = this.state;
    if (!angleUpFlow && !angleDownFlow) {
      this.setState({angleUpFlow: true, angleDownFlow: false}, () => {
        onPress && onPress(true);
      });
    } else if (angleUpFlow && !angleDownFlow) {
      this.setState({angleUpFlow: false, angleDownFlow: true}, () => {
        onPress && onPress(false);
      });
    } else if (!angleUpFlow && angleDownFlow) {
      this.setState(
        {
          angleUpFlow: false,
          angleDownFlow: false,
        },
        () => {
          onPress && onPress(undefined);
        },
      );
    }
  }

  render() {
    const {text, textStyle, containerStyle, type} = this.props;
    const {angleUpFlow, angleDownFlow} = this.state;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={containerStyle}>
            <TouchableOpacity
              style={[
                styles.mainContainer,
                {
                  backgroundColor:
                    type === 5 ? theme?.colors?.white : theme?.colors?.blueGray,
                },
              ]}
              activeOpacity={0.7}
              onPress={() => this.setSitutation()}>
              <Text
                style={[
                  styles.textStyle,
                  {color: theme?.colors?.darkGray},
                  textStyle,
                ]}
                numOflines={1}>
                {text}
              </Text>
              <View style={styles.iconsContainer}>
                <FAVectorIcon
                  group={'FontAwesome'}
                  iconName={'angle-up'}
                  size={8}
                  color={
                    angleUpFlow
                      ? theme?.colors?.darkGray
                      : theme?.colors?.softGray
                  }
                />
                <FAVectorIcon
                  group={'FontAwesome'}
                  iconName={'angle-down'}
                  size={8}
                  color={
                    angleDownFlow
                      ? theme?.colors?.darkGray
                      : theme?.colors?.softGray
                  }
                />
              </View>
            </TouchableOpacity>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

FASorting.propTypes = {
  onPress: PropTypes.func.isRequired,
  value: PropTypes.any.isRequired,
  text: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
};

export default FASorting;
