import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  Text,
  Platform,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import autobind from 'autobind-decorator';
import {launchImageLibrary} from 'react-native-image-picker';
import {styles} from './assets/styles';
import Localization from '@Localization';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFrontCamera: false,
      showCamera: true,
      flashOpen: false,
    };
  }
  notAuthorizedView() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Text>{Localization.t('DocumenationPageFix.CameraPermission')}</Text>
      </SafeAreaView>
    );
  }

  takePicture = async () => {
    const {onShotCamera} = this.props;
    if (this.camera) {
      const options = {quality: 0.3, base64: true, width: 1080};
      const data = await this.camera.takePictureAsync(options);
      onShotCamera(data);
    }
  };
  openGallery() {
    const {onShotCamera} = this.props;
    launchImageLibrary(
      {
        mediaType: 'photo',
        quality: 0.3,
        includeBase64: true,
        width: 1080,
        height: 768,
      },
      data => {
        if (data && !data.didCancel) {
          if (
            data &&
            data.assets &&
            data.assets.length &&
            data.assets.length > 0
          ) {
            onShotCamera(data.assets[0]);
          }
        }
      },
    );
  }
  @autobind
  onChangeFlashMode() {
    this.setState({
      flashOpen: !this.state.flashOpen,
    });
  }

  render() {
    const {flashOpen, isFrontCamera} = this.state;
    const {showCamera, closeCamera} = this.props;
    return (
      <Modal
        style={{
          flex: 1,
          backgroundColor: 'black',
        }}
        visible={showCamera}>
        <View
          style={{
            flex: 1,
          }}>
          <RNCamera
            notAuthorizedView={this.notAuthorizedView()}
            ref={ref => {
              this.camera = ref;
            }}
            style={{
              flex: 1,
            }}
            type={
              isFrontCamera
                ? RNCamera.Constants.Type.front
                : RNCamera.Constants.Type.back
            }
            flashMode={
              flashOpen
                ? RNCamera.Constants.FlashMode.on
                : RNCamera.Constants.FlashMode.off
            }
            captureAudio={false}
          />

          <TouchableOpacity
            onPress={() =>
              this.setState({isFrontCamera: !this.state.isFrontCamera})
            }
            style={{position: 'absolute', bottom: 40, right: 20}}>
            <MaterialIcons
              name={
                Platform.OS === 'ios'
                  ? 'flip-camera-ios'
                  : 'flip-camera-android'
              }
              size={30}
              color={'white'}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => closeCamera && closeCamera()}
            style={{position: 'absolute', top: 50, left: 20}}>
            <MaterialIcons
              name={'keyboard-backspace'}
              size={30}
              color={'white'}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.openGallery()}
            style={{position: 'absolute', bottom: 40, left: 20}}>
            <MaterialIcons name={'image'} size={30} color={'white'} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.onChangeFlashMode()}
            style={{position: 'absolute', top: 50, right: 20}}>
            <MaterialIcons
              name={flashOpen ? 'flash-on' : 'flash-off'}
              size={30}
              color={'white'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={{
              width: 75,
              height: 75,
              borderRadius: 37.5,
              position: 'absolute',
              alignSelf: 'center',
              bottom: 30,
              borderWidth: 5,
              borderColor: 'white',
              justifyContent: 'center',
            }}>
            <View
              style={{
                alignSelf: 'center',
                width: 50,
                height: 50,
                borderRadius: 25,
                backgroundColor: 'white',
              }}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
