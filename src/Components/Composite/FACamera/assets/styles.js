import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: FAColor.Black,
  },
  mainContainer: {
    flex: 1,
  },
  iconContainer: {position: 'absolute', bottom: 40, right: 20},
  closeCameraContainer: {position: 'absolute', top: 50, left: 20},
  openGalleryContainer: {position: 'absolute', bottom: 40, left: 20},
  onChangeFlashModeContainer: {position: 'absolute', top: 50, right: 20},
  takePictureButtonContainer: {
    width: 75,
    height: 75,
    borderRadius: 37.5,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 30,
    borderWidth: 5,
    borderColor: FAColor.White,
    justifyContent: 'center',
  },
  takePictureButtonChildContainer: {
    alignSelf: 'center',
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: FAColor.White,
  },
});
