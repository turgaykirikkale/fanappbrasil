import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.Black,
    flex: 1,
    opacity: 0.7,
    justifyContent: 'center',
  },
  childContainer: {
    backgroundColor: FAColor.White,
    marginHorizontal: 10,
    borderRadius: 4,
    alignItems: 'center',
    paddingVertical: 25,
  },
  QRCodeMainContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: FAColor.Green,
    alignItems: 'center',
    justifyContent: 'center',
  },
  QRCodeChildContainer: {width: 70, height: 70, marginRight: 15},
  textStyle: {
    color: FAColor.Black,
    marginVertical: 16,
    fontSize: 12,
    textAlign: 'center',
  },
  buttonContainerStyle: {backgroundColor: FAColor.DarkGray, borderRadius: 4},
  buttonTextStyle: {
    color: FAColor.White,
    marginVertical: 8,
    marginHorizontal: 110,
  },
});
