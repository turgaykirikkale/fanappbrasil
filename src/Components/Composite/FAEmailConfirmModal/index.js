import React from 'react';
import {View, Text, Modal} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import Localization from '@Localization';
import FAVectorIcon from '../../UI/FAVectorIcon';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FAColor from '@Commons/FAColor';

const FAEmailConfirmModal = props => {
  const {visible, onPress} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <Modal visible={visible}>
          <View style={styles.mainContainer}>
            <View
              style={[
                styles.childContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <View style={styles.QRCodeMainContainer}>
                <FAVectorIcon
                  group={'FontAwesome'}
                  iconName={'check'}
                  size={60}
                  color={'white'} //TO DO COLOR
                />
              </View>
              <Text style={[styles.textStyle, {color: theme?.colors?.black}]}>
                {Localization.t('EmailConfirmationScreenFix.sendEMail')}
              </Text>
              <View>
                <FAButton
                  disabled={true}
                  containerStyle={[
                    styles.buttonContainerStyle,
                    {backgroundColor: FAColor.VBlueGray},
                  ]}
                  textStyle={[styles.buttonTextStyle, {color: 'gray'}]}
                  text={Localization.t('WalletFix.WaitingforApproval')}
                  onPress={() => onPress && onPress()}
                />
                <View style={{marginTop: 10}} />
                <FAButton
                  disabled={false}
                  containerStyle={[
                    styles.buttonContainerStyle,
                    {backgroundColor: FAColor.Green},
                  ]}
                  textStyle={[styles.buttonTextStyle]}
                  text={Localization.t('CommonsFix.Home')}
                  onPress={() => onPress && onPress()}
                />
              </View>
            </View>
          </View>
        </Modal>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAEmailConfirmModal;
