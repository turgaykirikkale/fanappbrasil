import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import PropTypes from 'prop-types';
import Localization from '@Localization';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';

const FAShareBox = props => {
  const {share} = props;
  const randomColor = generateRandomColor();
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          onPress={() => share && share()}
          activeOpacity={0.8}
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <View style={styles.childContainer}>
            <View style={styles.iconContainer}>
              <FAIcon iconName={'FAUSers'} color={randomColor} />
            </View>
            <Text style={[styles.headerTextStyle, {color: randomColor}]}>
              {Localization.t('FAShareBox.ShareWithFriend')}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAShareBox.propTypes = {
  share: PropTypes.func.isRequired,
};

export default FAShareBox;
