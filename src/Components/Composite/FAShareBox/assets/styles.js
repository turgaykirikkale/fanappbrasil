import {StyleSheet} from 'react-native';
import {FAOrange, VGray, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: White,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  childContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {width: 16, height: 16, marginRight: 6},
  headerTextStyle: {fontSize: 12, color: FAOrange},
  textStyle: {fontSize: 12, textAlign: 'center'},
  buttonContainerStyle: {
    backgroundColor: VGray,
    borderRadius: 5,
    marginTop: 15,
  },
  buttonTextStyle: {fontSize: 12, color: FAOrange, marginVertical: 12},
});
