import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';

const FAFixtureHeader = props => {
  const {date, onPressAlarm} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: FAColor.VBlueGray,
        paddingBottom: 10,
        marginBottom: 10,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
        <View style={{width: 14, height: 14, marginRight: 5}}>
          <FAIcon iconName={'FACalendar'} color={FAColor.Gray} />
        </View>
        <Text style={{color: FAColor.Gray}}>{date}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TouchableOpacity style={{width: 15, height: 14, marginRight: 5}}>
          <FAIcon iconName={'regularStar'} color={FAColor.Gray} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{width: 14, height: 14, marginRight: 5}}
          onPress={() => onPressAlarm && onPressAlarm()}>
          <FAIcon iconName={'FAAlarm'} color={FAColor.Gray} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FAFixtureHeader;
