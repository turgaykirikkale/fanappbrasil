import React, {Component} from 'react';
import Localization from '@Localization';
import {RNCamera} from 'react-native-camera';
import {
  ActivityIndicator,
  Text,
  TouchableOpacity,
  View,
  Modal,
  Dimensions,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FAColor from '@Commons/FAColor';
import BarcodeMask from 'react-native-barcode-mask';

export default class FAQrScanner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  pendingView() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size={'large'} color={'black'} />
        <Text style={{color: 'black', marginTop: 5}}>
          {Localization.t('QrScannerScreen.CameraWaitingText')}
        </Text>
      </View>
    );
  }

  onBarcodeRead(scanResult) {
    const {onQrReaded} = this.props;

    let Adressdata = '';
    if (
      scanResult &&
      scanResult.data !== null &&
      scanResult.data.length &&
      scanResult.data.length > 0
    ) {
      let indefOfRef = scanResult.data.indexOf('ref=');
      if (indefOfRef > 0) {
        Adressdata = scanResult.data.slice(scanResult.data.indexOf('ref=') + 4);
      } else {
        Adressdata = scanResult.data;
      }
    }
    onQrReaded && onQrReaded(Adressdata);
  }

  render() {
    const initialLayoutWdith = Dimensions.get('window').width;

    const {showCamera, closeCamera} = this.props;

    return (
      <Modal
        style={{
          flex: 1,
          backgroundColor: 'black',
        }}
        visible={showCamera}>
        <View
          style={{
            flex: 1,
          }}>
          <RNCamera
            captureAudio={false}
            ref={ref => (this.camera = ref)}
            style={{flex: 1, width: '100%'}}
            onBarCodeRead={this.onBarcodeRead.bind(this)}>
            {({camera, status, recordAudioPermissionStatus}) => {
              if (status !== 'READY') {
                return this.pendingView();
              }
            }}

            <BarcodeMask
              outerMaskOpacity={0.3}
              width={initialLayoutWdith / 1.5}
              height={initialLayoutWdith / 1.5}
              edgeWidth={initialLayoutWdith / 10}
              edgeHeight={initialLayoutWdith / 10}
              edgeColor={FAColor.Orange}
              animatedLineColor={FAColor.Orange}
              edgeBorderWidth={5}
              useNativeDriver={true}
            />
          </RNCamera>

          <TouchableOpacity
            onPress={() => closeCamera && closeCamera()}
            style={{position: 'absolute', top: 20, left: 20, marginTop: 20}}>
            <MaterialIcons
              name={'keyboard-backspace'}
              size={30}
              color={'white'}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
