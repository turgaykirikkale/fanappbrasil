import React from 'react';
import {View} from 'react-native';
import FAColor from '@Commons/FAColor';
import RNPickerSelect from 'react-native-picker-select';
import Localization from '@Localization';

const FAMatchCenterHeaderSelect = props => {
  const {useNativeAndroidPickerStyle, onValueChange, value, list, placeholder} =
    props;
  return (
    <View
      style={{
        backgroundColor: FAColor.White,
        borderRadius: 4,
        marginVertical: 8,
        marginLeft: 8,
        marginRight: 4,
        justifyContent: 'center',
        paddingHorizontal: 8,
      }}>
      <RNPickerSelect
        doneText={Localization.t('Commons.Okay')}
        style={{
          inputIOS: [
            {
              marginVertical: 5,
            },
          ],
          inputAndroid: [{color: 'black', marginVertical: -8}],
        }}
        useNativeAndroidPickerStyle={useNativeAndroidPickerStyle || false}
        placeholder={{}}
        onValueChange={data => onValueChange && onValueChange(data)}
        items={list}
        value={value}
      />
    </View>
  );
};

export default FAMatchCenterHeaderSelect;
