import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    // paddingVertical: 23,
    // backgroundColor: 'red',
    paddingHorizontal: 10,
    paddingTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  PriceTextStyle: {fontSize: 23},
  PriceWithSymbolTextStyle: {
    fontSize: 16,
    color: FAColor.Gray,
  },
  change24HContainerStyle: {marginRight: 25, marginTop: 8},
  marginHorizontal18: {marginHorizontal: 19},
  highestTextStyle: {textAlign: 'right', color: FAColor.DarkGray},
  lowestTextStyle: {
    textAlign: 'right',
    color: '#717171',
    marginTop: 7,
    marginBottom: 7,
  },
  BTCVolumeTextStyle: {
    textAlign: 'right',
    color: FAColor.DarkGray,
    marginBottom: 7,
  },
  TRYVolumeTextStyle: {textAlign: 'right', color: FAColor.DarkGray},
  highesPriceTextStyle: {textAlign: 'right', color: FAColor.Black},
  lowestPriceTextStyle: {
    textAlign: 'right',
    color: FAColor.Black,
    marginTop: 7,
    marginBottom: 7,
  },
  BTCVolumePriceTextStyle: {
    textAlign: 'right',
    color: FAColor.Black,
    marginBottom: 7,
  },
  TRYVolumePriceTextStyle: {textAlign: 'right', color: FAColor.Black},
  informationAreaContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  informationValuesContainer: {marginLeft: 5},
});
