import FAPercentStatus from '@Components/UI/FAPercentStatus';
import {toFixedWithoutRounding} from '@Commons/FAMath';
import Localization from '@Localization';
import {View, Text, ActivityIndicator} from 'react-native';
import {styles} from './assets/styles';
import React from 'react';
import {Orange} from '../../../Commons/FAColor';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';

const FACoinHeadDetail = props => {
  const {
    isLoading,
    Price,
    PriceWithSymbol,
    CurrencySymbol,
    Change24H,
    Highest,
    Lowest,
    BTCVolume,
    TRYVolume,
    currencyCode,
    coinCode,
    currencyDecimalCount,
    theme,
  } = props;
  if (isLoading) {
    return (
      <View
        style={[styles.mainContainer, {backgroundColor: theme?.colors?.white}]}>
        <ActivityIndicator size={'small'} color={theme?.colors?.orange} />
      </View>
    );
  }
  return (
    <View
      style={[styles.mainContainer, {backgroundColor: theme?.colors?.white}]}>
      <View>
        <Text style={[styles.PriceTextStyle, {color: theme?.colors?.black}]}>
          {FACurrencyAndCoinFormatter(
            toFixedWithoutRounding(Price, currencyDecimalCount),
            currencyDecimalCount,
          )}
        </Text>
        {PriceWithSymbol && CurrencySymbol ? (
          <Text
            style={[
              styles.PriceWithSymbolTextStyle,
              {color: theme?.colors?.black},
            ]}>{`${PriceWithSymbol} ${CurrencySymbol}`}</Text>
        ) : null}
        <View style={styles.change24HContainerStyle}>
          <FAPercentStatus
            theme={theme}
            value={Change24H === 0 ? 0.0 : Change24H}
          />
        </View>
      </View>
      <View style={styles.informationAreaContainer}>
        <View>
          <Text
            style={[styles.highestTextStyle, {color: theme?.colors?.darkGray}]}>
            {Localization.t('MarketDetail.High')}
          </Text>
          <Text
            style={[styles.lowestTextStyle, {color: theme?.colors?.darkGray}]}>
            {Localization.t('MarketDetail.Low')}
          </Text>
          {coinCode ? (
            <Text
              style={[
                styles.BTCVolumeTextStyle,
                {color: theme?.colors?.darkGray},
              ]}>
              {Localization.t('MarketDetail.Volume', {
                currencyOrCoinCode: coinCode || '',
              })}
            </Text>
          ) : null}

          {/* {currencyCode ? (
            <Text style={styles.TRYVolumeTextStyle}>
              {currencyCode || ''} Hacim
            </Text>
          ) : null} */}
        </View>
        <View style={styles.informationValuesContainer}>
          <Text
            style={[
              styles.highesPriceTextStyle,
              {color: theme?.colors?.black},
            ]}>
            {FACurrencyAndCoinFormatter(
              toFixedWithoutRounding(Highest, currencyDecimalCount),
              currencyDecimalCount,
            )}
          </Text>
          <Text
            style={[
              styles.lowestPriceTextStyle,
              {color: theme?.colors?.black},
            ]}>
            {FACurrencyAndCoinFormatter(
              toFixedWithoutRounding(Lowest, currencyDecimalCount),
              currencyDecimalCount,
            )}
          </Text>
          {BTCVolume ? (
            <Text
              style={[
                styles.BTCVolumePriceTextStyle,
                {color: theme?.colors?.black},
              ]}>
              {FACurrencyAndCoinFormatter(
                toFixedWithoutRounding(BTCVolume, 2),
                2,
              )}
            </Text>
          ) : null}
          {TRYVolume ? (
            <Text
              style={[
                styles.TRYVolumePriceTextStyle,
                {color: theme?.colors?.black},
              ]}>
              {toFixedWithoutRounding(TRYVolume, 2)}
            </Text>
          ) : null}
        </View>
      </View>
    </View>
  );
};

FACoinHeadDetail.defaultProps = {
  currencyDecimalCount: 4,
};

export default FACoinHeadDetail;
