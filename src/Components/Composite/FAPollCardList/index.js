import React from 'react';
import {FlatList, View} from 'react-native';
import FAPollCardItem from '@Components/UI/FAPollCardItem';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {MockTokenList} from '@Commons/MockDatas/TokenList';

const FAPollCardList = props => {
  const {data, spaceBetweenItems, onPress, tokenList, WalletState} = props;

  let coinList = [];
  if (WalletState?.UserWallets) {
    const userWallets = WalletState.UserWallets;
    userWallets.map(item => {
      if (MockTokenList.includes(item.Code)) {
        coinList.push(item);
      }
    });
  }

  if (data?.length > 0) {
    const lastIndex = data.length - 1;
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index}
        data={data}
        renderItem={({item, index}) => (
          <View
            style={{
              marginTop:
                index !== 0 && spaceBetweenItems ? spaceBetweenItems : null,
              paddingBottom: index === lastIndex ? 75 : null,
            }}>
            <FAPollCardItem
              text={item.Title}
              onPress={() => onPress && onPress(item)}
              image={item.ImageUrl}
              price={item.Price}
              tokenList={tokenList}
              coinId={item.CoinId}
              coinList={coinList}
            />
          </View>
        )}
      />
    );
  }
  return null;
};

FAPollCardList.propTypes = {
  data: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
  spaceBetweenItems: PropTypes.number,
};

const mapStateToProps = state => {
  return {
    WalletState: state.WalletState,
  };
};

export default connect(mapStateToProps, {})(FAPollCardList);
