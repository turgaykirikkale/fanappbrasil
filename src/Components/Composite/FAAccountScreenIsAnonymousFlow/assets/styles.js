import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  isAnonymousFlowMainContainer: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 10,
  },
  iconContainer: {width: 50, height: 50},
  isAnonymousFlowButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1,
    marginLeft: 30,
  },
  isAnonymousFlowSignButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    borderRadius: 4,
    flex: 1,
    marginRight: 8,
  },
  isAnonymousFlowSignButtonTextStyle: {
    marginVertical: 15,
    color: FAColor.DarkBlue,
  },
  isAnonymousFlowRegisterButtonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    flex: 1,
    marginLeft: 8,
  },
  isAnonymousFlowRegisterButtonTextStyle: {
    marginVertical: 15,
    color: FAColor.White,
  },
});
