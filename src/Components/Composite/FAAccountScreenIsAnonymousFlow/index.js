import React from 'react';
import {View} from 'react-native';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import FAIcon from '@Components/UI/FAIcon';
import FAButton from '@Components/Composite/FAButton';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAAccountScreenIsAnonymousFlow = props => {
  const {onPressLogin, onPressSignIn} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={styles.isAnonymousFlowMainContainer}>
          <View style={styles.iconContainer}>
            <FAIcon iconName={'BMAUserAlt'} color={theme?.colors?.faOrange} />
          </View>
          <View style={styles.isAnonymousFlowButtonContainer}>
            <FAButton
              onPress={() => onPressLogin && onPressLogin()}
              containerStyle={[
                styles.isAnonymousFlowSignButtonContainer,
                {backgroundColor: theme?.colors?.gray},
              ]}
              textStyle={[
                styles.isAnonymousFlowSignButtonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={Localization.t('CommonsFix.Login')}
            />
            <FAButton
              onPress={() => onPressSignIn && onPressSignIn()}
              containerStyle={[
                styles.isAnonymousFlowRegisterButtonContainer,
                {backgroundColor: theme?.colors?.green},
              ]}
              textStyle={[
                styles.isAnonymousFlowRegisterButtonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={Localization.t('CommonsFix.Register')}
            />
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAAccountScreenIsAnonymousFlow;
