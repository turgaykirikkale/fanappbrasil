import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: 6,
  },
  BMAMarketItemPriceAndVolumeContainer: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: 16,
  },
  BMAPercentStatusContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  BMAVectorIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 18,
    height: 22,
  },
});
