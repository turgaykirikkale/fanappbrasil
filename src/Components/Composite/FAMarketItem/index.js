import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {styles} from './assets/styles';
import FAMarketItemLogoAndName from '@Components/UI/FAMarketItemLogoAndName';
import FAMarketItemPriceAndVolume from '@Components/UI/FAMarketItemPriceAndVolume';
import FAPercentStatus from '@Components/UI/FAPercentStatus';
import FAColor from '@Commons/FAColor';
import {sentenceShortener} from '@Helpers/StringHelper';
import FAIcon from '@Components/UI/FAIcon';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAMarketItem = props => {
  const {
    status,
    imageName,
    coinName,
    coinLongName,
    coinPrice,
    coinVolume,
    currencySymbol,
    change24H,
    onPress,
    isDisabled,
    currencyDecimalCount,
    isOpen,
    currencyCode,
    imageUri,
  } = props;
  const generatedCoinLongName = sentenceShortener(coinLongName, 10);
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}
          activeOpacity={0.7}
          onPress={() => onPress && onPress()}>
          <FAMarketItemLogoAndName
            theme={theme}
            imageUri={imageUri}
            imageName={imageName}
            name={coinName}
            currencyCode={currencyCode}
            longName={generatedCoinLongName}
          />

          <View style={styles.BMAMarketItemPriceAndVolumeContainer}>
            <FAMarketItemPriceAndVolume
              theme={theme}
              isDisabled={isDisabled}
              Price={FACurrencyAndCoinFormatter(
                coinPrice,
                currencyDecimalCount,
              )}
              Volume={FACurrencyAndCoinFormatter(coinVolume, 2)}
              currencySymbol={currencySymbol}
            />
          </View>

          {status ? (
            <View style={styles.BMAPercentStatusContainer}>
              <FAPercentStatus theme={theme} value={change24H} />
            </View>
          ) : (
            <View style={styles.BMAVectorIconContainer}>
              <FAIcon
                iconName={isOpen ? 'FAAngleDown' : 'FAAngelRight'}
                color={FAColor.SoftGray}
              />
            </View>
          )}
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAMarketItem.defaultProps = {
  currencyDecimalCount: 8,
  isOpen: false,
};

export default FAMarketItem;
