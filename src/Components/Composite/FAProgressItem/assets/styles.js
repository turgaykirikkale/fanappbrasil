import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingVertical: 9,
    alignItems: 'center',
    position: 'relative',
  },
  spacer: {marginLeft: 8},
});
