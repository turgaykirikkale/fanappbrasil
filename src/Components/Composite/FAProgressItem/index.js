import FAProgressItemValues from '@Components/UI/FAProgressItemValues';
import FACircleProgress from '@Components/UI/FACircleProgress';
import {AppConstants} from '@Commons/Contants';
import {styles} from './assets/styles';
import React, {useState} from 'react';
import {ActivityIndicator, View} from 'react-native';
import FAService from '@Services';
import moment from 'moment';
import {FAOrange} from '../../../Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAProgressItem = props => {
  const {
    type,
    percent,
    occuredAmount,
    totalAmount,
    priceLeft,
    priceRight,
    date,
    price,
    item,
    onOrderCancelSuccess,
    currencyCode,
    coinCode,
  } = props;

  const [isLoading, setIsLoading] = useState(false);

  const onPressCancel = () => {
    const generatedOrderId = item.OrderId || item.Id;
    const requestBody = {
      OrderId: generatedOrderId,
    };
    FAService.CancelCustomerActiveOrder(requestBody)
      .then(response => {
        if (response && response.data && response.data.IsSuccess) {
          setIsLoading(false);
          onOrderCancelSuccess && onOrderCancelSuccess();
        } else {
          setIsLoading(false);
        }
      })
      .catch(error => {
        setIsLoading(false);
      });
  };

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[styles.container, {backgroundColor: theme?.colors?.white}]}>
          {isLoading ? (
            <View
              style={{
                position: 'absolute',
                justifyContent: 'center',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                zIndex: 99,
                opacity: 0.5,
                backgroundColor: theme?.colors?.black,
              }}>
              <ActivityIndicator
                size={'small'}
                color={theme?.colors?.faOrange}
              />
            </View>
          ) : null}
          <FACircleProgress
            hasPairCodes={coinCode && currencyCode}
            percent={percent}
            type={item.Type}
          />
          <View style={styles.spacer} />
          <FAProgressItemValues
            currencyCode={currencyCode}
            coinCode={coinCode}
            currencyDecimalCount={AppConstants.CurrencyDecimalCount}
            coinDecimalCount={AppConstants.CoinDecimalCount}
            onPressCancel={() => {
              setIsLoading(true);
              onPressCancel();
            }}
            item={item}
            type={type}
            occuredAmount={occuredAmount}
            totalAmount={totalAmount}
            priceLeft={priceLeft}
            priceRight={priceRight}
            date={moment(date).format('DD-MM-YYYY HH:mm')}
            price={price}
          />
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAProgressItem;
