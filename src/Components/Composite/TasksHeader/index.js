import React from 'react';
import {View, Text} from 'react-native';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import FAIcon from '@Components/UI/FAIcon';

const TasksHeader = props => {
  const {headerUp, headerBottom} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            backgroundColor: theme?.colors?.white,
            paddingVertical: 10,
            paddingHorizontal: 10,
            marginVertical: 10,
            marginHorizontal: 10,
            borderRadius: 4,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <FAIcon
              iconName={'FAPages'}
              color={theme?.colors?.darkBlue}
              size={16}
            />
            <Text style={{color: theme?.colors?.darkBlue, marginLeft: 5}}>
              Genel Durum
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 16,
            }}>
            <View style={{flexDirection: 'row'}}>
              <FAIcon
                iconName={'FACheck'}
                color={theme?.colors?.black}
                size={16}
              />
              <Text
                style={{
                  color: theme?.colors?.black,
                  marginLeft: 7,
                  fontWeight: '500',
                }}>
                {headerUp}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  marginLeft: 5,
                  fontWeight: '500',
                }}>
                {`${0} / ${0}`}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 8,
            }}>
            <View style={{flexDirection: 'row'}}>
              <FAIcon
                iconName={'FAHoldingMoney'}
                color={theme?.colors?.black}
                size={16}
              />
              <Text
                style={{
                  color: theme?.colors?.black,
                  marginLeft: 7,
                  fontWeight: '500',
                }}>
                {headerBottom}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  marginLeft: 5,
                  fontWeight: '500',
                }}>
                {`${0} / ${0}`}
              </Text>
            </View>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default TasksHeader;
