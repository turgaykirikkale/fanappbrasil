import React from 'react';
import {View, Text, TouchableOpacity, Modal} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {Green} from '@Commons/FAColor';
import {BlurView} from '@react-native-community/blur';
import Localization from '@Localization';
import PropTypes from 'prop-types';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAAlertModal = props => {
  const {
    onPress,
    iconName,
    header,
    text,
    visible,
    iconColor,
    FAIconName,
    modalControl,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <Modal
          animationType="none"
          transparent={true}
          visible={visible}
          style={{zIndex: 100}}>
          <BlurView
            style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}
            blurType="dark"
            blurAmount={3}
            reducedTransparencyFallbackColor={theme?.colors?.white}
          />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}>
            <View
              style={{
                paddingVertical: 24,
                paddingHorizontal: 12,
                backgroundColor: theme?.colors?.white,
                alignContent: 'center',
                justifyContent: 'center',
              }}>
              <View style={{flexDirection: 'row'}}>
                <View style={{width: 30, height: 30}}>
                  {FAIconName ? (
                    <View>
                      <FAIcon iconName={FAIconName} />
                    </View>
                  ) : (
                    <FAVectorIcon
                      iconName={iconName}
                      size={30}
                      color={iconColor}
                    />
                  )}
                </View>
                <View
                  style={{
                    marginLeft: 8,
                  }}>
                  <Text
                    style={{
                      color: theme?.colors?.black,
                      fontSize: 16,
                    }}>
                    {header}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: theme?.colors?.darkGray,
                      paddingRight: 25,
                      marginTop: 6,
                    }}>
                    {text}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{
                  alignItems: 'center',
                  backgroundColor: modalControl
                    ? theme?.colors?.green
                    : theme?.colors?.orange,
                  paddingVertical: 12,
                  marginTop: 12,
                  borderRadius: 4,
                  zIndex: 1001,
                }}
                onPress={() => {
                  onPress && onPress();
                }}>
                <Text
                  style={{
                    // fontFamily: 'Roboto-Medium',
                    fontSize: 16,
                    color: theme?.colors?.white2,
                  }}>
                  {Localization.t('Commons.Okay')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      )}
    </ThemeContext.Consumer>
  );
};

FAAlertModal.propTypes = {
  onPress: PropTypes.func.isRequired,
  iconName: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  iconColor: PropTypes.string,
  FAIconName: PropTypes.string,
  modalControl: PropTypes.bool,
};

export default FAAlertModal;
