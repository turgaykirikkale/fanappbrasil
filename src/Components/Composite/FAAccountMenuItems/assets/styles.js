import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  BMAAccountMenuItemsMainContainer: {
    flexDirection: 'row',
    backgroundColor: FAColor.White,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  flex1: {flex: 1},
  BMAAccountMenuItemsWithCountryCodeContainer: {
    backgroundColor: FAColor.White,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  BMAAccountMenuItemsWithSwitchContainer: {
    backgroundColor: FAColor.White,
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderColor: FAColor.VBlueGray,
  },
  BMAAccountMenuItemsWithSwitchContainerTextStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  BMAAccountMenuItemsAccountParentItemsContainer: {
    backgroundColor: FAColor.White,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderColor: FAColor.VBlueGray,
  },
  BMAAccountMenuItemsAccountParentItemsLeftIconContainer: {
    width: 24,
    height: 24,
  },
  BMAAccountMenuItemsAccountParentItemsTextContainerStyle: {
    justifyContent: 'center',
    flex: 1,
  },
  fontStyles: {
    fontSize: 14,
  },
});
