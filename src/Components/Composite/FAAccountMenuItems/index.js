import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FACountry from '@Components/UI/FACountry';
import FACheckbox from '@Components/Composite/FACheckbox';
import FASwitch from '@Components/UI/FASwitch';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import {Black, DarkBlue, DarkBlueO, SoftGray} from '../../../Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAAccountMenuItems = props => {
  const {
    checked,
    fillColor,
    tickColor,
    onPress,
    iconName,
    CountryName,
    disabled,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          activeOpacity={0.7}
          style={[
            styles.BMAAccountMenuItemsMainContainer,
            {
              backgroundColor: disabled
                ? theme?.colors?.blueGray
                : theme?.colors?.white,
            },
          ]}
          onPress={() => onPress && onPress()}
          disabled={disabled}>
          <View style={[styles.flex1]}>
            <FACountry
              iconName={iconName}
              CountryName={CountryName}
              disabled={disabled}
            />
          </View>
          <FACheckbox.Circle
            disabled={disabled}
            checked={checked}
            fillColor={fillColor}
            tickColor={tickColor}
            onPress={() => onPress && onPress()}
          />
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAAccountMenuItems.WithCountryCode = props => {
  const {
    countryCode,
    checked,
    fillColor,
    tickColor,
    onPress,
    iconName,
    CountryName,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          style={[
            styles.BMAAccountMenuItemsWithCountryCodeContainer,
            {backgroundColor: theme?.colors?.white},
          ]}
          onPress={() => onPress && onPress()}>
          <View style={styles.flex1}>
            <FACountry
              countryCode={countryCode}
              iconName={iconName}
              CountryName={CountryName}
            />
          </View>
          <FACheckbox.Circle
            checked={checked}
            fillColor={fillColor}
            tickColor={tickColor}
            onPress={() => onPress && onPress()}
          />
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAAccountMenuItems.WithSwitch = props => {
  const {
    disabled,
    isOn,
    onBgColor,
    offBgColor,
    onCircleColor,
    offCircleColor,
    onToggle,
    text,
    size,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.BMAAccountMenuItemsWithSwitchContainer,
            {
              backgroundColor: theme?.colors?.white,
              borderColor: theme?.colors?.blueGray,
            },
          ]}>
          <View style={styles.BMAAccountMenuItemsWithSwitchContainerTextStyle}>
            <Text style={[styles.fontStyles, {color: theme?.colors?.black}]}>
              {text}
            </Text>
          </View>
          <FASwitch
            disabled={disabled}
            size={size}
            isOn={isOn}
            onBgColor={onBgColor}
            offBgColor={offBgColor}
            onCircleColor={onCircleColor}
            offCircleColor={offCircleColor}
            onToggle={() => onToggle && onToggle()}
          />
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAAccountMenuItems.AccountParentItems = props => {
  const {leftIconName, text, onPress, disable} = props;
  let iconColor = DarkBlue;
  let fontColor = Black;
  if (disable) {
    iconColor = DarkBlueO;
    fontColor = DarkBlueO;
  }
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          disabled={disable}
          style={[
            styles.BMAAccountMenuItemsAccountParentItemsContainer,
            {
              backgroundColor: theme?.colors?.white,
              borderColor: theme?.colors?.blueGray,
            },
          ]}
          onPress={() => onPress && onPress()}
          activeOpacity={0.7}>
          {leftIconName ? (
            <View
              style={
                styles.BMAAccountMenuItemsAccountParentItemsLeftIconContainer
              }>
              <FAIcon
                iconName={leftIconName}
                color={theme?.colors?.black || iconColor}
              />
            </View>
          ) : null}

          <View
            style={[
              styles.BMAAccountMenuItemsAccountParentItemsTextContainerStyle,
              {
                marginLeft: leftIconName ? 8 : null,
              },
            ]}>
            <Text
              style={[
                styles.fontStyles,
                {color: theme?.colors?.black || fontColor},
              ]}>
              {text}
            </Text>
          </View>
          <View
            style={{
              width: 20,
              height: 20,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <FAIcon iconName={'FAAngelRight'} color={theme?.colors?.softGray} />
          </View>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAAccountMenuItems;
