import {StyleSheet} from 'react-native';
import {VGray} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: VGray,
    borderRadius: 4,
  },
});
