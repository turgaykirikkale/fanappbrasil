import FAPickerSelect from '@Components/Composite/FAPickerSelect';
import {TouchableOpacity} from 'react-native';
import * as PropTypes from 'prop-types';
import {styles} from './assets/styles';
import React from 'react';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

export const FAExchangeOrderTypePicker = props => {
  const {flowList, selectedFlow, onValueChange} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.vGray},
          ]}>
          <FAPickerSelect
            theme={theme}
            nonBorder
            onValueChange={incomingValue =>
              onValueChange && onValueChange(incomingValue)
            }
            value={selectedFlow}
            list={flowList}
            paddingVertical={9.5}
          />
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAExchangeOrderTypePicker.propTypes = {
  onValueChange: PropTypes.func.isRequired,
  selectedFlow: PropTypes.string.isRequired,
  flowList: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default FAExchangeOrderTypePicker;
