import React, {useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native';
import FAButton from '../FAButton';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import moment from 'moment';
import _ from 'lodash';
import FAFixtureItem from '../../UI/FAFixtureItem';

const FATeamResults = props => {
  const {data} = props;

  let DateList = [];
  _.map(data, item => {
    let date = moment(item.date, 'DD-MM-YYYY').format('DD-MM-YYYY');
    if (DateList.includes(date)) {
    } else {
      DateList.push(date);
    }
  });

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <>
          {_.map(DateList, dateitem => {
            return (
              <View
                style={{
                  marginHorizontal: 10,
                  borderRadius: 4,
                  marginTop: 10,
                  borderWidth: 0.4,
                  paddingVertical: 10,
                  borderColor: theme?.colors?.softGray,
                }}>
                <View>
                  <Text
                    style={{
                      color: theme?.colors?.black,
                      marginLeft: 10,
                      fontWeight: '500',
                    }}>
                    {dateitem}
                  </Text>
                </View>
                <>
                  {_.map(data, item => {
                    if (
                      dateitem ===
                      moment(item.date, 'DD-MM-YYYY').format('DD-MM-YYYY')
                    ) {
                      return <FAFixtureItem item={item} />;
                    }
                  })}
                </>
              </View>
            );
          })}
        </>
      )}
    </ThemeContext.Consumer>
  );
};

export default FATeamResults;
