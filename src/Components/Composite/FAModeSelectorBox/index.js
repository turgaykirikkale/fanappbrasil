import React from 'react';
import FAButton from '@Components/Composite/FAButton';
import {Image, Modal, Text, TouchableOpacity, View} from 'react-native';
import {styles} from './assets/styles';
import {StockMode} from '@Commons/FAEnums';
import {DarkBlue, DarkBlueO, Green, VBlueGray, White} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import Localization from '@Localization';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {connect} from 'react-redux';

const FAModeSelectorBox = props => {
  const {onChangeMode, selectedMode, visible, onPressClose, MarketState} =
    props;
  const modeList = [
    // {
    //   id: 1,
    //   name: Localization.t('ModeScreen.EasyBuySellText'),
    //   code: StockMode.EasyBuySell,
    // },
    {
      id: 2,
      name: Localization.t('ModeScreen.StandardText'),
      code: StockMode.Standard,
    },
    {
      id: 3,
      name: Localization.t('ModeScreen.AdvancedText'),
      code: StockMode.Advanced,
    },
  ];

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <Modal animationType="none" transparent={true} visible={visible}>
          <View
            style={[
              styles.modalBackgroundContainer,
              {backgroundColor: theme?.colors?.darkBlack},
            ]}>
            <View
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              {onPressClose && (
                <TouchableOpacity
                  onPress={() => onPressClose()}
                  activeOpacity={0.7}
                  style={styles.closeButton}>
                  <FAVectorIcon
                    iconName={'times'}
                    size={22}
                    color={theme?.colors.darkBlue0}
                  />
                </TouchableOpacity>
              )}
              <View style={styles.imageContainer}>
                <Image
                  style={styles.imageStyle}
                  source={require('./assets/change-mode-icon.png')}
                />
              </View>
              <Text
                style={[styles.imageBottomText, {color: theme?.colors?.black}]}>
                {Localization.t('ModeScreen.ChooseModeText')}
              </Text>
              <View style={styles.buttonMainContainer}>
                {modeList.map(mode => (
                  <View key={mode.id} style={styles.buttonContainer}>
                    <FAButton
                      disabled={selectedMode === mode.code}
                      onPress={() => onChangeMode && onChangeMode(mode.code)}
                      text={mode.name}
                      containerStyle={[
                        styles.buttonContainerStyle,
                        {
                          backgroundColor:
                            selectedMode === mode.code
                              ? theme?.colors?.green
                              : theme?.colors?.blueGray,
                        },
                      ]}
                      textStyle={[
                        styles.buttonTextStyle,
                        {
                          color:
                            selectedMode === mode.code
                              ? theme?.colors?.white2
                              : theme?.colors?.darkBlue,
                        },
                      ]}
                    />
                  </View>
                ))}
              </View>
              <Text style={[styles.footerText, {color: theme?.colors?.gray}]}>
                {Localization.t('ModeScreen.ModeInformationText')}
              </Text>
            </View>
          </View>
        </Modal>
      )}
    </ThemeContext.Consumer>
  );
};

FAModeSelectorBox.propTypes = {
  selectedMode: PropTypes.object.isRequired,
  onChangeMode: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  onPressClose: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {})(FAModeSelectorBox);
