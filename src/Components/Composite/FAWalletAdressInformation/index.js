import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {styles} from './assets/styles';
import {DarkBlue} from '@Commons/FAColor';
import FAIcon from '@Components/UI/FAIcon';
import Localization from '@Localization';

const FAWalletAdressInformation = props => {
  const {onPressCopy, adress, coinCode, network, theme} = props;
  return (
    <View
      style={[
        styles.qrCodeContainer,
        {backgroundColor: theme?.colors?.blueGray},
      ]}>
      <View style={styles.flex1}>
        <Text
          style={[
            styles.qrCodeHeaderTextStyle,
            {color: theme?.colors?.darkBlue},
          ]}>
          {`${coinCode} ${Localization.t(
            'CommonsFix.Wallet',
          )} ${`(${Localization.t('WalletFix.Network')} : ${network})`}`}
        </Text>
        <Text
          style={[styles.coinAddressTextStyle, {color: theme?.colors?.black}]}>
          {adress}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.coppyCodeButtonStyle}
        onPress={() => onPressCopy && onPressCopy()}>
        <FAIcon iconName={'FACopy'} color={theme?.colors?.darkBlue} />
      </TouchableOpacity>
    </View>
  );
};
export default FAWalletAdressInformation;
