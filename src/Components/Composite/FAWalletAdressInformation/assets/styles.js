import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  qrCodeContainer: {
    flexDirection: 'row',
    backgroundColor: FAColor.BlueGray,
    paddingHorizontal: 32,
    paddingVertical: 12,
    marginVertical: 5,
  },
  flex1: {flex: 1},
  qrCodeHeaderTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    marginBottom: 8,
  },
  coinAddressTextStyle: {color: FAColor.Black},
  coppyCodeButtonStyle: {
    alignSelf: 'center',
    marginLeft: 30,
    marginTop: 5,
    width: 18,
    height: 18,
  },
});
