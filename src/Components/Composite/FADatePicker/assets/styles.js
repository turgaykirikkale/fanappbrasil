import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  button: {
    backgroundColor: FAColor.White,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 4,
    borderColor: FAColor.SoftGray,
    height: 50,
    flexDirection: 'row',
  },
  buttonText: {
    color: FAColor.Gray,
    marginHorizontal: 10,
  },
  OldIdentificationCardTextStyle: {
    marginLeft: 4,
    fontSize: 12,
    letterSpacing: -0.5,
    color: FAColor.DarkBlue,
  },
  checkBoxContainer: {flexDirection: 'row', alignItems: 'center'},
  overlayStyle: {
    width: '100%',
    justifyContent: 'flex-end',
    position: 'absolute',
    bottom: 0,
  },
  actionButtonsContainer: {
    backgroundColor: FAColor.White,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderColor: FAColor.SoftGray,
    borderBottomWidth: 1,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'flex-end',
  },
  cancelButton: {paddingHorizontal: 15},
  doneButton: {
    alignSelf: 'flex-end',
    marginRight: 10,
  },
  doneButtonText: {paddingHorizontal: 15, color: FAColor.Green},
  pickerStyle: {backgroundColor: FAColor.White, height: 200},
  angleDownIconContainer: {marginRight: 5, marginLeft: 15},

  labelContainer: {
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  labelText: {
    fontSize: 14,
    color: FAColor.Gray,
  },
});
