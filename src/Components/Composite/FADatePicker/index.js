import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Platform} from 'react-native';
import {styles} from './assets/styles';
import _ from 'lodash';
import moment from 'moment';
import autobind from 'autobind-decorator';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Overlay} from 'react-native-elements';
import FAIcon from '@Components/UI/FAIcon';
import FACheckbox from '@Components/Composite/FACheckbox';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

export default class FADatePicker extends Component {
  constructor(props) {
    super(props);

    this.locale = 'en-EN';
    this.state = {
      show: false,
      selectedDateDisplay: props.value,
      selectedDate: props.maximumDate,
      dayIsSelected:
        props.value && !_.isNil(props.value) && !_.isEmpty(props.value),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.selectedDateDisplay) {
      this.setState({
        selectedDateDisplay: nextProps.value,
        selectedDate: moment(nextProps.value, 'DD-MM-YYYY').toDate(),
        dayIsSelected: true,
      });
    }
  }

  componentDidMount() {
    const currentLanguage = 'en';
    if (currentLanguage === 'tr') {
      this.locale = 'tr-TR';
    } else if (currentLanguage === 'en') {
      this.locale = 'en-EN';
    }
  }

  @autobind
  onChange(event, selectedDate) {
    const {onChange} = this.props;
    const formattedDisplay = moment(selectedDate).format('DD-MM-YYYY');
    this.setState(
      prevState => {
        return {
          show: Platform.OS === 'ios' ? prevState.show : false,
          selectedDate:
            selectedDate !== undefined ? new Date(selectedDate) : new Date(),
          selectedDateDisplay: formattedDisplay,
          dayIsSelected: true,
        };
      },
      () => {
        onChange && onChange(formattedDisplay);
      },
    );
  }

  @autobind
  renderDateTimePicker(theme) {
    const {selectedDate, show} = this.state;
    const {minumumDate, maximumDate} = this.props;
    if (Platform.OS === 'ios') {
      return (
        <Overlay
          overlayStyle={styles.overlayStyle}
          fullScreen={false}
          isVisible={show}
          onBackdropPress={() => this.setState({show: false})}>
          <View>
            <View style={[styles.actionButtonsContainer]}>
              <TouchableOpacity
                style={styles.doneButton}
                onPress={() => this.setState({show: false})}>
                <Text style={styles.doneButtonText}>
                  {Localization.t('Commons.Okay')}
                </Text>
              </TouchableOpacity>
            </View>
            <DateTimePicker
              minimumDate={minumumDate}
              // maximumDate={maximumDate}
              locale={this.locale}
              textColor="black"
              testID="dateTimePicker"
              value={selectedDate}
              mode={'date'}
              is24Hour={true}
              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
              onChange={this.onChange}
            />
          </View>
        </Overlay>
      );
    }
    return (
      <DateTimePicker
        minimumDate={minumumDate}
        // maximumDate={maximumDate}
        locale={this.locale}
        textColor="black"
        testID="dateTimePicker"
        value={selectedDate}
        mode={'date'}
        is24Hour={true}
        display={Platform.OS === 'ios' ? 'spinner' : 'default'}
        onChange={this.onChange}
      />
    );
  }

  render() {
    const {
      placeHolder,
      label,
      disabled,
      withoutFlex1,
      OldIdentificationCard,
      hasOldIdentificationCard,
      onPressChecked,
      identificationNumberType,
    } = this.props;
    const {show, dayIsSelected, selectedDateDisplay} = this.state;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={[withoutFlex1 ? null : styles.flex1]}>
            {label ? (
              <View style={styles.labelContainer}>
                <Text
                  style={[styles.labelText, {color: theme?.colors?.darkBlue}]}>
                  {label}
                </Text>
                {identificationNumberType ? (
                  <View style={styles.checkBoxContainer}>
                    <FACheckbox
                      checked={hasOldIdentificationCard}
                      onPress={() => onPressChecked && onPressChecked()}
                    />
                    <Text style={styles.OldIdentificationCardTextStyle}>
                      {Localization.t(
                        'IdentityEnterScreen.HaveOldIdentityCardText',
                      )}
                    </Text>
                  </View>
                ) : null}
              </View>
            ) : null}

            <TouchableOpacity
              disabled={disabled}
              style={[
                styles.button,
                {
                  marginTop: 6,
                  backgroundColor: theme?.colors?.white,
                  borderColor: theme?.colors?.gray,
                },
              ]}
              onPress={() => this.setState({show: true})}>
              <View style={styles.flex1}>
                <Text style={[styles.buttonText]}>
                  {dayIsSelected ? selectedDateDisplay : placeHolder}
                </Text>
              </View>
              <View style={styles.angleDownIconContainer}>
                <View style={{width: 16, height: 16}}>
                  <FAIcon iconName={'FAAngleDown'} color={'#5A5A5A'} />
                </View>
              </View>
            </TouchableOpacity>
            {show ? this.renderDateTimePicker(theme) : null}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
