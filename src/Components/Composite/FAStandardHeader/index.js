import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Text, TouchableOpacity, View} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {Black} from '@Commons/FAColor';
import {styles} from './assets/styles';
import React from 'react';
import PropTypes from 'prop-types';
import {sentenceShortener} from '@Helpers/StringHelper';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FASwitch from '../../UI/FASwitch';

const FAStandardHeader = props => {
  const {title, navigation, hideBackButton, navigateToAppStack} = props;
  const insets = useSafeAreaInsets();
  return (
    <ThemeContext.Consumer>
      {({theme, updateTheme}) => (
        <View
          style={[
            styles.generalContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <View
            style={[
              styles.mainWrapper,
              {
                marginTop: insets.top,
                backgroundColor: theme?.colors?.white,
                borderBottomColor: theme?.colors.softGray,
              },
            ]}>
            {!hideBackButton && navigation.canGoBack() ? (
              <TouchableOpacity
                hitSlop={{top: 70, bottom: 70, left: 50, right: 50}}
                activeOpacity={0.8}
                onPress={() => navigation.goBack()}
                style={styles.backButtonContainer}>
                <FAIcon iconName={'leftArrow'} color={theme?.colors?.black} />
              </TouchableOpacity>
            ) : navigateToAppStack ? (
              <TouchableOpacity
                hitSlop={{top: 70, bottom: 70, left: 50, right: 50}}
                activeOpacity={0.8}
                onPress={() => navigateToAppStack && navigateToAppStack()}
                style={styles.backButtonContainer}>
                <FAIcon iconName={'leftArrow'} color={theme?.colors?.black} />
              </TouchableOpacity>
            ) : (
              <View style={styles.leftEmptyView} />
            )}
            <View style={{marginLeft: 10}} />
            <Text style={[styles.titleStyle, {color: theme?.colors?.black}]}>
              {sentenceShortener(title || '', 20)}
            </Text>
            {/*<View style={styles.rightEmptyView} />*/}
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() =>
                updateTheme(theme.name === 'dark' ? 'light' : 'dark')
              }
              style={{marginRight: 10}}>
              {theme?.name === 'dark' ? (
                <FAIcon iconName={'sun'} size={20} color={'#e3cb27'} />
              ) : (
                <FAIcon iconName={'moon'} size={20} color={'#23272A'} />
              )}
            </TouchableOpacity>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAStandardHeader.propTypes = {
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
  hideBackButton: PropTypes.bool,
};

export default FAStandardHeader;
