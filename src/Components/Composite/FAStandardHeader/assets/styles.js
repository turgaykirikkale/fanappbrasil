import {StyleSheet, Platform} from 'react-native';
import {Black, White} from '@Commons/FAColor';

const backButtonSize = 14;
const backButtonMarginLeft = 10;
let paddingVertical = 15;
const isAndroid = Platform.OS === 'android';
if (isAndroid) {
  paddingVertical = 20;
}
export const styles = StyleSheet.create({
  generalContainer: {backgroundColor: White},
  mainWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: paddingVertical,
    backgroundColor: White,
    borderBottomWidth: 0.5,
    borderBottomColor: '#ddd',
  },
  backButtonContainer: {
    marginLeft: backButtonMarginLeft,
    width: backButtonSize,
    height: backButtonSize,
  },
  leftEmptyView: {
    marginLeft: backButtonMarginLeft + backButtonSize,
  },
  titleStyle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 12,
    color: Black,
    fontWeight: isAndroid ? '600' : '500',
  },
  rightEmptyView: {
    marginRight: backButtonMarginLeft + backButtonSize,
  },
});
