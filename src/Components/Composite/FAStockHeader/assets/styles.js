import {StyleSheet} from 'react-native';
import {Black, BlueGray, VBlueGray, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: White,
    borderBottomWidth: 1,
    borderBottomColor: VBlueGray,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 12,
  },
  pairBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 4,
    backgroundColor: BlueGray,
  },
  pair: {
    marginLeft: 8,
    fontSize: 12,
    fontWeight: '500',
    color: Black,
  },
  rightIconContainer: {flexDirection: 'row', alignItems: 'center'},
  loaderContainer: {
    width: 18,
    height: 18,
    alignItems: 'center',
    marginLeft: 18,
  },
  modeIconContainer: {marginLeft: 19},
});
