import {
  ActivityIndicator,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {Black, Green, Red} from '@Commons/FAColor';
import {toFixedNoRounding} from '@Commons/FAMath';
import React, {useCallback, useEffect, useState} from 'react';
import FAIcon from '@Components/UI/FAIcon';
import {SoftBlack} from '@Commons/FAColor';
import {FAOrange} from '@Commons/FAColor';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import FAService from '@Services';
import _ from 'lodash';
import {useFocusEffect} from '@react-navigation/core';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAStockHeader = props => {
  const {
    navigation,
    pair,
    favoriteDisable,
    coinCode,
    currencyCode,
    dailyChange,
    onPressPair,
    onPressChart,
    onPressMode,
  } = props;
  const parsedDailyChange = toFixedNoRounding(dailyChange, 2);
  const [isFavorite, setIsFavorite] = useState(false);
  const [favoriteLoading, setFavoriteLoading] = useState(false);
  
  useFocusEffect(
    useCallback(() => {
      if (!favoriteDisable) {
        getFavoriteCoinList();
      }
    }, [pair, favoriteDisable]),
  );

  const dailyChangeColorGenerator = theme => {
    return parsedDailyChange < 0
      ? theme?.colors?.danger
      : parsedDailyChange > 0
      ? theme?.colors?.success
      : theme?.colors?.black;
  };

  const dailyChangeTextGenerator = () => {
    let prefix = '';
    if (parsedDailyChange > 0) {
      prefix = '+';
    }
    return `${prefix}${toFixedNoRounding(dailyChange, 2)}%`;
  };

  const getFavoriteCoinList = () => {
    setFavoriteLoading(true);
    FAService.GetFavoriteCoinList()
      .then(response => {
        if (response?.data) {
          const resData = response.data;
          const matchedCoin = _.find(resData, {CoinId: pair?.CoinId});
          if (matchedCoin) {
            setIsFavorite(true);
          } else {
            setIsFavorite(false);
          }
        }
        setFavoriteLoading(false);
      })
      .catch(error => {
        setFavoriteLoading(false);
      });
  };

  const handleFavorite = () => {
    setFavoriteLoading(true);
    const requestBody = {
      CoinId: pair.CoinId || null,
      CurrencyId: pair.CurrencyId || null,
    };
    if (isFavorite) {
      removeCoinFromFavorites(requestBody);
    } else {
      addCoinToFavorites(requestBody);
    }
  };

  const removeCoinFromFavorites = requestBody => {
    FAService.RemoveCoinFromFavorites(requestBody)
      .then(response => {
        if (response?.data?.IsSuccess) {
          setIsFavorite(false);
        }
        setFavoriteLoading(false);
      })
      .catch(error => {
        setFavoriteLoading(false);
      });
  };

  const addCoinToFavorites = requestBody => {
    FAService.AddCoinToFavorites(requestBody)
      .then(response => {
        if (response?.data?.IsSuccess) {
          setIsFavorite(true);
        }
        setFavoriteLoading(false);
      })
      .catch(error => {
        setFavoriteLoading(false);
      });
  };

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.mainContainer,
            {
              backgroundColor: theme?.colors?.white,
              borderBottomColor: theme?.colors?.vGray,
              justifyContent: 'space-between',
            },
          ]}>
          <StatusBar
            barStyle={theme.name === 'dark' ? 'light-content' : 'dark-content'}
          />
          <View>
            <TouchableOpacity
              style={{width: 20, height: 20}}
              activeOpacity={0.8}
              onPress={() => navigation.goBack()}>
              <FAIcon iconName={'leftArrow'} color={theme?.colors?.black} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => onPressPair && onPressPair()}
            style={[
              styles.pairBoxContainer,
              {backgroundColor: theme?.colors?.blueGray},
            ]}>
            <FAVectorIcon
              iconName={'exchange'}
              size={14}
              color={theme?.colors?.black}
            />
            <Text
              style={[
                styles.pair,
                {color: theme?.colors?.black},
              ]}>{`${coinCode}/${currencyCode}`}</Text>
            <Text
              style={[styles.pair, {color: dailyChangeColorGenerator(theme)}]}>
              {dailyChangeTextGenerator()}
            </Text>
          </TouchableOpacity>
          {/* <View style={styles.rightIconContainer}>
            {/* <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => onPressChart && onPressChart()}>
              <FAIcon
                iconName={'barChart'}
                size={18}
                color={theme?.colors?.black}
              />
            </TouchableOpacity> 
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.modeIconContainer}
              onPress={() => onPressMode && onPressMode()}>
              <FAVectorIcon
                iconName={'clone'}
                size={18}
                color={theme?.colors?.black}
              />
            </TouchableOpacity>
            {!favoriteDisable && !_.isNil(pair) ? (
              favoriteLoading ? (
                <View style={styles.loaderContainer}>
                  <ActivityIndicator
                    color={theme?.colors?.faOrange}
                    size={'small'}
                  />
                </View>
              ) : (
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.modeIconContainer}
                  onPress={handleFavorite}>
                  <FAVectorIcon
                    iconName={'star'}
                    size={18}
                    color={
                      isFavorite
                        ? theme?.colors?.faOrange
                        : theme?.colors?.black
                    }
                  />
                </TouchableOpacity>
              )
            ) : null}
          </View> */}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAStockHeader.propTypes = {
  pair: PropTypes.object.isRequired,
  favoriteDisable: PropTypes.bool.isRequired,
  dailyChange: PropTypes.string.isRequired,
  coinCode: PropTypes.string.isRequired,
  currencyCode: PropTypes.string.isRequired,
  onPressPair: PropTypes.func.isRequired,
  onPressChart: PropTypes.func.isRequired,
  onPressMode: PropTypes.func.isRequired,
};

FAStockHeader.defaultProps = {
  dailyChange: '0.0',
};

export default FAStockHeader;
