import React from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import Localization from '@Localization';

const FATokenLogoList = props => {
  const {data, onPress} = props;

  return (
    <View style={styles.mainContainer}>
      <View style={styles.listContainer}>
        <FlatList
          nestedScrollEnabled
          contentContainerStyle={styles.listContentContainer}
          showsHorizontalScrollIndicator={false}
          horizontal
          keyExtractor={(item, index) => index}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.itemContainer}>
              <Image
                style={styles.imageContainer}
                source={{
                  uri: `https://borsa.bitci.com/img/coin/${item.CoinCode}.png`,
                }}
              />
            </View>
          )}
        />
      </View>
      <Text style={styles.textStyle}>
        {Localization.t('FATokenLogoList.JoinTokenWorld')}
      </Text>
      <FAButton
        onPress={() => onPress && onPress()}
        text={Localization.t('FATokenLogoList.ButBitciCoin')}
        textStyle={styles.buttonTextStyle}
        containerStyle={styles.buttonContainerStyle}
      />
    </View>
  );
};

FATokenLogoList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      CoinCode: PropTypes.string.isRequired,
    }),
  ).isRequired,
  onPress: PropTypes.func.isRequired,
};
export default FATokenLogoList;
