import {StyleSheet, Platform} from 'react-native';
import {White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: 'white',
    paddingBottom: 20,
    paddingHorizontal: 5,
  },
  itemContainer: {
    marginRight: 5,
    marginLeft: 5,
    paddingTop: 20,
    paddingBottom: 15,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .4)',
        shadowOffset: {height: 1, width: 0},
        shadowOpacity: 1,
        shadowRadius: 4,
      },
      android: {
        elevation: 3,
      },
    }),
  },
  imageContainer: {
    width: 40,
    height: 40,
  },
  textStyle: {
    textAlign: 'center',
    marginBottom: 15,
    color: '#9A9999',
    fontSize: 12,
  },
  buttonTextStyle: {
    fontSize: 12,
    color: White,
    marginVertical: 7,
    fontWeight: '700',
  },
  buttonContainerStyle: {
    backgroundColor: '#9A9999',
    borderRadius: 4,
    marginHorizontal: 45,
  },
  listContainer: {alignItems: 'center'},
  listContentContainer: {
    justifyContent: 'center',
    flex: 1,
  },
});
