import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import autobind from 'autobind-decorator';
import {styles} from './assets/styles';
import moment from 'moment';
import FAIcon from '@Components/UI/FAIcon';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-community/clipboard';
import _ from 'lodash';
import FAColor from '@Commons/FAColor';
import Localization from '@Localization';

export default class FACoinTransactionHistoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTXID: false,
    };
  }

  renderTransactionStatus(item) {
    const {theme} = this.props;
    switch (item.State) {
      case 0: {
        return (
          <View style={styles.situationContainer}>
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'minus-circle'}
              size={14}
              color={
                theme.name === 'dark' ? 'rgb(201,175,97)' : 'rgb(255,216,52)'
              } //TO DO COLOR
            />
            <Text style={styles.situationTextStyle}>
              {Localization.t('CoinWithdrawHistoryScreen.WaitingForApproval')}
            </Text>
          </View>
        );
      }
      case 1: {
        return (
          <View style={styles.situationContainer}>
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'minus-circle'}
              size={14}
              color={
                theme.name === 'dark' ? 'rgb(201,175,97)' : 'rgb(255,216,52)'
              } //TO DO COLOR
            />
            <Text style={styles.situationTextStyle}>
              {Localization.t('CoinWithdrawHistoryScreen.Process')}
            </Text>
          </View>
        );
      }
      case 2: {
        return (
          <View style={styles.situationContainer}>
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'check-circle'}
              size={14}
              color={theme?.colors?.success} //TO DO COLOR
            />
            <Text style={styles.situationApprovedTextStyle}>
              {Localization.t('CoinWithdrawHistoryScreen.Approved')}
            </Text>
          </View>
        );
      }
      case 3: {
        return (
          <View style={styles.situationContainer}>
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'check-circle'}
              size={14}
              color={theme?.colors?.success} //TO DO COLOR
            />
            <Text style={styles.situationApprovedTextStyle}>
              {Localization.t('CoinWithdrawHistoryScreen.Completed')}
            </Text>
          </View>
        );
      }
      case 4: {
        return (
          <View style={styles.situationContainer}>
            <FAVectorIcon
              group={'FontAwesome'}
              iconName={'exclamation-circle'}
              size={14}
              color={theme?.colors?.danger} //TO DO COLOR
            />
            <Text style={styles.situationCancelText}>
              {Localization.t('CoinWithdrawHistoryScreen.Cancelled')}
            </Text>
          </View>
        );
      }
    }
  }

  @autobind
  copyTxId() {
    const {item} = this.props;

    Clipboard.setString(item.TxId);
    Toast.show({
      type: 'success',
      position: 'top',
      text1: Localization.t('Commons.Succes'),
      text2: Localization.t('CoinWithdrawHistoryScreen.TxIDCopy'),
      visibilityTime: 750,
      autoHide: true,
      topOffset: 50,
      bottomOffset: 40,
      onShow: () => {},
      onHide: () => {},
      onPress: () => {},
    });
  }

  render() {
    const {item, type, onPressCancel, theme} = this.props;
    if (_.isEmpty(item) || _.isNil(item)) {
      return <View />;
    } else {
      return (
        <View style={styles.mainContainer}>
          <View style={styles.flexDirectionRow}>
            <View style={styles.flex1}>
              <Text
                style={[
                  styles.transactionText,
                  {color: theme?.colors?.darkBlue0},
                ]}>
                İşlem Kodu : {`${item.RequestCode}`}
              </Text>
              <Text
                style={[styles.dateText, {color: theme?.colors?.darkBlue0}]}>
                {moment(item.CreateDate).format('DD-MM-YYYY HH:mm')}
              </Text>

              <TouchableOpacity
                style={[
                  styles.TXIDButtonContainer,
                  {backgroundColor: theme?.colors?.darkWhite},
                ]}
                onPress={() => this.setState({showTXID: !this.state.showTXID})}>
                <Text
                  style={[
                    styles.TXIDButtonText,
                    {color: theme?.colors?.black},
                  ]}>
                  {this.state.showTXID
                    ? Localization.t('CoinWithdrawHistoryScreen.TxIDHide')
                    : Localization.t('CoinWithdrawHistoryScreen.TxIDShow')}
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.depositAdressText,
                  {color: theme?.colors?.black},
                ]}>
                {item.withdrawOrDepositAdress}
              </Text>
            </View>
            <View style={styles.alignItemFlexEnd}>
              <Text style={[styles.amountText, {color: theme?.colors?.black}]}>
                {`${item.Amount + item.Fee} ${item.CoinCode}`}
              </Text>
              {this.renderTransactionStatus(item)}
              <Text
                style={[
                  styles.transactionCommissionTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {`${item.Fee} ${item.CoinCode}`}
              </Text>
            </View>
          </View>
          {this.state.showTXID ? (
            <View style={styles.TXIDContainer}>
              {item.State === 0 ||
              item.State === 4 ||
              item.State === 4 ? null : (
                <TouchableOpacity
                  style={styles.TXIDCoppyIconContainer}
                  onPress={() => this.copyTxId()}>
                  <FAIcon iconName="FACopy" color={theme?.colors?.darkBlue} />
                </TouchableOpacity>
              )}
              <Text
                style={[
                  styles.TXIDTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {item.TxId}
              </Text>
              {item.State === 0 ? (
                <TouchableOpacity>
                  <Text
                    style={[
                      styles.TXIDCancelButtonTextStyle,
                      {color: theme?.colors?.faOrange},
                    ]}
                    onPress={() => onPressCancel && onPressCancel(item.Id)}>
                    {Localization.t('CoinWithdrawHistoryScreen.Cancel')}}
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View>
          ) : null}
        </View>
      );
    }
  }
}
