import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginHorizontal: 8,
  },
  flexDirectionRow: {
    flexDirection: 'row',
  },
  flex1: {flex: 1},
  transactionText: {
    fontSize: 12,
    color: 'rgb(155,155,155)', //TO DO COLOR
  },
  dateText: {
    fontSize: 12,
    color: 'rgb(155,155,155)', //TO DO COLOR

    marginTop: 5,
    marginBottom: 5,
  },
  depositAdressText: {
    fontSize: 12,
    color: 'rgb(26,26,26)', //TO DO COLOR
  },
  TXIDButtonContainer: {
    alignItems: 'center',
    borderRadius: 4,
    width: 120,
    paddingVertical: 3,
    backgroundColor: 'rgb(245,246,250)', //TO DO COLOR
  },
  TXIDButtonText: {
    fontSize: 10,
    color: 'rgb(26,26,26)', //TO DO COLOR
  },
  alignItemFlexEnd: {alignItems: 'flex-end'},
  amountText: {
    fontSize: 16,
    color: 'rgb(26,26,26)', //TO DO COLOR
  },
  situationContainer: {flexDirection: 'row', alignItems: 'center'},
  situationTextStyle: {
    marginLeft: 4,
    fontSize: 12,
    color: 'rgb(255,216,52)', //TO DO COLOR

    marginTop: 5,
    marginBottom: 5,
  },
  situationApprovedTextStyle: {
    marginLeft: 4,
    fontSize: 12,
    color: 'rgb(120,211,53)', //TO DO COLOR

    marginTop: 5,
    marginBottom: 5,
  },
  situationCancelText: {
    marginLeft: 4,
    fontSize: 12,
    color: 'rgb(201,9,9)', //TO DO COLOR
    marginTop: 5,
    marginBottom: 5,
  },
  transactionCommissionTextStyle: {
    fontSize: 12,
    color: 'rgb(155,155,155)', //TO DO COLOR
  },
  TXIDContainer: {flexDirection: 'row', alignItems: 'center', paddingBottom: 5},
  TXIDCoppyIconContainer: {width: 16, height: 16, marginRight: 10},
  TXIDTextStyle: {
    flex: 1,
    textAlign: 'left',
    fontSize: 12,
    color: 'rgb(155,155,155)', //TO DO COLOR
    marginBottom: 3,
  },
  TXIDCancelButtonTextStyle: {
    marginTop: 5,

    fontSize: 14,
    color: 'rgb(254,80,0)', //TO DO COLOR
  },
});
