import {StyleSheet} from 'react-native';
import {DarkBlue, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    // backgroundColor: White,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerContainer: {flexDirection: 'row', alignItems: 'center'},
  headerTitle: {
    fontSize: 12,
    marginLeft: 6,
  },
  seeAllText: {fontSize: 12, color: DarkBlue},
});
