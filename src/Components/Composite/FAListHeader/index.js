import {Text, TouchableOpacity, View} from 'react-native';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import FAIcon from '@Components/UI/FAIcon';
import {DarkBlue} from '@Commons/FAColor';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import React from 'react';
import Localization from '@Localization';
import {generateRandomColor} from '../../../Helpers/RandomColorHelper';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAListHeader = props => {
  const {onPress, title, iconInLocal, iconName, resetPadding} = props;
  const randomColor = generateRandomColor();
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.mainContainer,
            {
              paddingVertical: resetPadding ? 0 : 8,
              paddingHorizontal: resetPadding ? 0 : 12,
              backgroundColor: theme?.colors?.white || 'white',
            },
          ]}>
          <View style={styles.headerContainer}>
            {iconInLocal ? (
              <View style={{width: 13, height: 13}}>
                <FAIcon
                  iconName={iconName}
                  color={randomColor || theme?.colors?.black}
                />
              </View>
            ) : (
              <FAVectorIcon
                iconName={iconName}
                color={randomColor || theme?.colors?.black}
                size={14}
              />
            )}

            <Text
              style={[
                styles.headerTitle,
                {color: randomColor || theme?.colors?.black},
              ]}>
              {title || ''}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => onPress && onPress()}
            activeOpacity={0.8}
            hitSlop={{top: 8, bottom: 8, right: 11, left: 11}}>
            <Text
              style={[
                styles.seeAllText,
                {color: randomColor || theme?.colors?.black},
              ]}>
              {Localization.t('CommonsFix.SeeAll')} >
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAListHeader.propTypes = {
  fontColor: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  iconInLocal: PropTypes.bool,
  iconName: PropTypes.string,
  buttonText: PropTypes.string,
  resetPadding: PropTypes.bool,
};

FAListHeader.defaultProps = {
  fontColor: DarkBlue,
  iconInLocal: false,
  iconName: 'tv',
};

export default FAListHeader;
