import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {
  SoftGray,
  SoftO,
  Orangray,
  White,
  Gray,
  Black,
  Danger,
  Success,
  DarkGray,
  DarkBlue,
} from '@Commons/FAColor';
import {styles} from './assets/styles';
import RNPickerSelect from 'react-native-picker-select';
import * as Rules from '@ValidationEngine/Rules';
import TextInputMask from 'react-native-text-input-mask';
import _ from 'lodash';
import FAIcon from '@Components/UI/FAIcon';
import FAButton from '@Components/Composite/FAButton';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const renderLabel = props => {
  const {label, theme} = props;
  if (label) {
    return (
      <Text style={[styles.defaultLabel, {color: theme?.colors?.darkBlue}]}>
        {label}
      </Text>
    );
  }
  return null;
};

const maskGenerator = limit => {
  if (_.isNil(limit)) {
    return null;
  }
  let mask = '[999999999]{.}';
  let generatedMask = '[';
  for (let i = 0; i < limit; i++) {
    generatedMask += '9';
  }
  generatedMask += ']';

  mask += generatedMask;
  return mask;
};

const FATextInput = props => {
  const {
    placeholder,
    disable,
    value,
    onChangeValue,
    errorMessage,
    hasSuccess,
    successMessage,
    rule,
    Wallet,
    mask,
    keyboardType,
  } = props;
  const [borderColor, setBorderColor] = useState(null);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;
  if (rule) {
    isValid = Rules[rule](value);
  }
  const hasError = !isValid && onBlur;
  return (
    <ThemeContext.Consumer>
      {({theme}) => {
        if (theme && !borderColor) {
          setBorderColor(theme?.colors?.softGray);
        }
        return (
          <View>
            {renderLabel({...props, theme})}
            {mask ? (
              <TextInputMask
                autoCapitalize={false}
                value={value}
                onChangeText={(formatted, newValue) => {
                  onChangeValue(formatted || newValue);
                }}
                onFocus={() => {
                  setBorderColor(theme?.colors?.soft0), setOnBlur(false);
                }}
                onBlur={() => {
                  setBorderColor(theme?.colors?.softGray), setOnBlur(true);
                }}
                style={[
                  styles.defaultInputStyle,
                  {
                    borderColor: hasError
                      ? theme?.colors?.danger
                      : hasSuccess
                      ? theme?.colors?.success
                      : borderColor,
                    backgroundColor: disable
                      ? Wallet
                        ? theme?.colors?.black
                        : theme?.colors?.white
                      : theme?.colors?.white,
                    color: theme?.colors?.black,
                  },
                ]}
                placeholderTextColor={theme?.colors?.gray}
                placeholder={placeholder || ''}
                editable={!disable}
                mask={mask}
                {...props}
              />
            ) : (
              <TextInput
                keyboardType={keyboardType}
                autoCapitalize={false}
                value={value}
                onChangeText={newValue =>
                  onChangeValue && onChangeValue(newValue)
                }
                onFocus={() => {
                  setBorderColor(theme?.colors?.soft0), setOnBlur(false);
                }}
                onBlur={() => {
                  setBorderColor(theme?.colors?.softGray), setOnBlur(true);
                }}
                style={[
                  styles.defaultInputStyle,
                  {
                    color: theme?.colors?.black,
                    borderColor: hasError
                      ? theme?.colors?.danger
                      : hasSuccess
                      ? theme?.colors?.success
                      : borderColor,
                    backgroundColor: disable
                      ? Wallet
                        ? theme?.colors?.white
                        : theme?.colors?.white
                      : theme?.colors?.white,
                  },
                ]}
                placeholderTextColor={theme?.colors?.gray}
                placeholder={placeholder || ''}
                editable={!disable}
              />
            )}
            {hasError && errorMessage ? (
              <Text
                style={[
                  styles.inputErrorMessage,
                  {color: theme?.colors?.danger},
                ]}>
                {errorMessage}
              </Text>
            ) : hasSuccess && successMessage ? (
              <Text
                style={[
                  styles.inputSuccessMessage,
                  {color: theme?.colors?.success},
                ]}>
                {successMessage}
              </Text>
            ) : null}
          </View>
        );
      }}
    </ThemeContext.Consumer>
  );
};

const FATextInputWithIcon = props => {
  const {
    placeholder,
    value,
    onChangeValue,
    rightIcon,
    label,
    leftIcon,
    onPressLeftIcon,
    onPressRightIcon,
    secureTextEntry,
    iconType,
    rule,
    errorMessage,
    keyboardType,
    rightIconComponent,
  } = props;
  const [borderColor, setBorderColor] = useState(null);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;
  if (rule) {
    isValid = Rules[rule](value);
  }
  const hasError = !isValid && onBlur;
  return (
    <ThemeContext.Consumer>
      {({theme}) => {
        if (theme && !borderColor) {
          setBorderColor(theme?.colors?.softGray);
        }
        return (
          <>
            {label ? (
              <Text
                style={[styles.defaultLabel, {color: theme?.colors?.darkBlue}]}>
                {label}
              </Text>
            ) : null}
            <View
              style={[
                styles.inputWithIconContainer,
                {
                  borderColor: hasError ? theme?.colors?.danger : borderColor,
                  backgroundColor: theme?.colors?.white,
                },
              ]}>
              {leftIcon && leftIcon.name ? (
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => onPressLeftIcon && onPressLeftIcon()}
                  style={{marginLeft: 16}}>
                  {iconType === 'static' ? (
                    <View style={{width: leftIcon.size, height: leftIcon.size}}>
                      <FAIcon
                        iconName={leftIcon.name}
                        color={leftIcon.color || '#F2F2F2'}
                      />
                    </View>
                  ) : (
                    <View
                      style={{
                        width: rightIcon.size || 32,
                        height: rightIcon.size || 32,
                      }}>
                      <FAIcon
                        iconName={rightIcon.name || 'FACircleFontAwesome'}
                        color={rightIcon.color || '#F2F2F2'}
                      />
                    </View>
                  )}
                </TouchableOpacity>
              ) : null}
              <TextInput
                keyboardType={keyboardType}
                secureTextEntry={secureTextEntry}
                onFocus={() => {
                  setBorderColor(theme?.colors?.soft0);
                  setOnBlur(false);
                }}
                onBlur={() => {
                  setBorderColor(theme?.colors?.softGray);
                  setOnBlur(true);
                }}
                onChangeText={newValue =>
                  onChangeValue && onChangeValue(newValue)
                }
                value={value}
                placeholder={placeholder}
                style={[
                  styles.inputWithIconInputStyle,
                  {
                    marginRight: rightIcon && rightIcon.name ? 8 : null,
                    marginLeft: leftIcon && leftIcon.name ? 8 : null,
                    color: theme?.colors?.black,
                    // textAlign:
                    //   (rightIcon && rightIcon.name && !leftIcon) || rightIconComponent
                    //     ? 'left'
                    //     : 'right',
                  },
                ]}
                placeholderTextColor={theme?.colors?.gray}
              />
              {rightIconComponent ? (
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => onPressRightIcon && onPressRightIcon()}
                  style={{
                    marginRight: 16,
                  }}>
                  {rightIconComponent}
                </TouchableOpacity>
              ) : null}

              {rightIcon && rightIcon.name ? (
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => onPressRightIcon && onPressRightIcon()}
                  style={{marginRight: 16}}>
                  {iconType === 'static' ? (
                    <View
                      style={{width: rightIcon.size, height: rightIcon.size}}>
                      <FAIcon
                        iconName={rightIcon.name}
                        color={rightIcon.color || '#F2F2F2'}
                      />
                    </View>
                  ) : (
                    <View
                      style={{
                        width: rightIcon.size || 32,
                        height: rightIcon.size || 32,
                      }}>
                      <FAIcon
                        iconName={rightIcon.name || 'FACircleFontAwesome'}
                        color={rightIcon.color || '#F2F2F2'}
                      />
                    </View>
                  )}
                </TouchableOpacity>
              ) : null}
            </View>
            {hasError && errorMessage ? (
              <Text
                style={[
                  styles.inputErrorMessage,
                  {color: theme?.colors?.danger},
                ]}>
                {errorMessage}
              </Text>
            ) : null}
          </>
        );
      }}
    </ThemeContext.Consumer>
  );
};

FATextInput.Dropdown = props => {
  const {
    selectedItem,
    placeholder,
    onChangeValue,
    disable,
    inputValue,
    onPress,
    rule,
    errorMessage,
    mask,
    keyboardType,
    label,
    isPhoneNumber,
    theme,
  } = props;
  const [borderColor, setBorderColor] = useState(theme?.colors?.softGray);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;

  if (rule) {
    isValid = Rules[rule](inputValue);
  }
  const hasError = !isValid && onBlur;
  return (
    <View>
      {renderLabel({...props, theme})}
      <View
        style={[
          styles.dropdownInputContainer,
          {
            backgroundColor: disable
              ? theme?.colors?.blueGray
              : theme?.colors?.white,
            borderColor: hasError ? theme?.colors?.danger : borderColor,
          },
        ]}>
        {selectedItem && selectedItem.phoneCode && !disable ? (
          <>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => onPress && onPress()}
              style={styles.dropdownButton}>
              <View style={styles.dropdownSelectedItemIcon}>
                <FAIcon iconName={selectedItem.flag} />
              </View>
              <Text
                style={[
                  styles.dropdownSelectedItemText,
                  {color: theme?.colors?.black},
                ]}>
                {selectedItem && selectedItem.phoneCode
                  ? selectedItem.phoneCode
                  : null}
              </Text>
              <View style={styles.dropdownArrowIcon}>
                <FAIcon iconName={'FAAngleDown'} color={theme?.colors?.black} />
              </View>
            </TouchableOpacity>
            <View
              style={[
                styles.dropdownVerticalDivider,
                {color: theme?.colors?.softGray},
              ]}
            />
          </>
        ) : null}
        {mask ? (
          <TextInputMask
            keyboardType={keyboardType}
            onFocus={() => {
              setBorderColor(theme?.colors?.soft0);
              setOnBlur(false);
            }}
            onBlur={() => {
              setBorderColor(theme?.colors?.softGray);
              setOnBlur(true);
            }}
            value={inputValue}
            onChangeText={(formatted, extracted) => {
              if (onChangeValue) {
                if (selectedItem && selectedItem.phoneCode === '+90') {
                  onChangeValue(formatted, extracted);
                } else {
                  onChangeValue(formatted, extracted);
                }
              }
            }}
            style={[styles.dropdownInputStyle, {color: theme?.colors?.black}]}
            placeholderTextColor={Gray}
            placeholder={placeholder || ''}
            editable={!disable}
            mask={mask}
          />
        ) : (
          <TextInput
            keyboardType={keyboardType}
            onFocus={() => {
              setBorderColor(theme?.colors?.soft0);
              setOnBlur(false);
            }}
            onBlur={() => {
              setBorderColor(theme?.colors?.softGray);
              setOnBlur(true);
            }}
            value={inputValue}
            onChangeText={newValue =>
              onChangeValue &&
              onChangeValue(
                selectedItem && selectedItem.phoneCode === '+90'
                  ? newValue
                  : selectedItem.phoneCode + newValue,
              )
            }
            style={[styles.dropdownInputStyle, {color: theme?.colors?.black}]}
            placeholderTextColor={Gray}
            placeholder={placeholder || ''}
            editable={!disable}
          />
        )}
      </View>
      {hasError && errorMessage ? (
        <Text
          style={[styles.inputErrorMessage, {color: theme?.colors?.danger}]}>
          {errorMessage}
        </Text>
      ) : null}
    </View>
  );
};

FATextInput.WithPicker = props => {
  const {
    placeholder,
    onChangeValue,
    disable,
    inputValue,
    list,
    PickerValue,
    onValueChangePickerValue,
    useNativeAndroidPickerStyle,
    placeholderPicker,
    keyboardType,
    rule,
    errorMessage,
    value,
  } = props;
  const [borderColor, setBorderColor] = useState(SoftGray);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;
  if (rule) {
    isValid = Rules[rule](value);
  }
  const hasError = !isValid && onBlur;
  return (
    <View>
      {renderLabel(props)}
      <View
        style={[
          styles.dropdownInputContainer,
          {
            backgroundColor: disable ? Orangray : White,
            borderColor: hasError ? Danger : borderColor,
          },
        ]}>
        {list ? (
          <RNPickerSelect
            doneText={Localization.t('Commons.Okay')}
            style={{
              inputIOS: [styles.withPickerIOSStyle],
              inputAndroid: [styles.withPickerAndroidStyle],
              iconContainer: [styles.withPickericonContainer],
            }}
            useNativeAndroidPickerStyle={useNativeAndroidPickerStyle || false}
            placeholder={placeholder || {}}
            onValueChange={value => {
              onValueChangePickerValue(value);
            }}
            items={list}
            value={PickerValue}
            Icon={() => (
              <View style={styles.dropdownArrowIcon}>
                <FAIcon iconName={'FAAngleDown'} color={'#5A5A5A'} />
              </View>
            )}
          />
        ) : null}
        <View style={styles.withPickerDivider} />
        <TextInput
          onFocus={() => {
            setBorderColor(SoftO), setOnBlur(false);
          }}
          onBlur={() => {
            setBorderColor(SoftGray), setOnBlur(true);
          }}
          keyboardType={keyboardType}
          value={inputValue}
          onChangeText={newValue => onChangeValue && onChangeValue(newValue)}
          style={styles.dropdownInputStyle}
          placeholderTextColor={Gray}
          placeholder={placeholderPicker || ''}
          editable={!disable}
          rule={rule}
          errorMessage={errorMessage}
        />
      </View>
      {hasError && errorMessage ? (
        <Text style={styles.inputErrorMessage}>{errorMessage}</Text>
      ) : null}
    </View>
  );
};

FATextInput.PhoneNumber = props => {
  const {
    selectedItem,
    placeholder,
    onChangeValue,
    disable,
    value,
    onPress,
    rule,
    errorMessage,
    phonePrefix,
    mask,
    keyboardType,
    notFormattedValue,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View>
          {renderLabel({...props, theme})}
          <FATextInput.Dropdown
            theme={theme}
            keyboardType={keyboardType}
            selectedItem={selectedItem}
            placeholder={placeholder}
            onChangeValue={(fullPhoneNumberValue, phonePrefix) => {
              onChangeValue && onChangeValue(fullPhoneNumberValue, phonePrefix);
            }}
            disable={disable}
            inputValue={value}
            onPress={() => onPress && onPress()}
            rule={rule}
            errorMessage={errorMessage}
            mask={mask}
            isPhoneNumber={true}
            notFormattedValue={notFormattedValue}
          />
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FATextInput.SelectBox = props => {
  const {value, items, placeholder, onChangeValue, disable} = props;
  const [borderColor, setBorderColor] = useState(SoftGray);
  return (
    <View>
      {renderLabel(props)}
      <TouchableOpacity
        activeOpacity={0.8}
        disabled={disable}
        onPress={() => setBorderColor(SoftO)}
        style={[
          styles.selectBoxContainer,
          {
            borderColor: borderColor,
            backgroundColor: disable ? Orangray : White,
          },
        ]}>
        <Text style={[styles.selectBoxText, {color: value ? Black : Gray}]}>
          {value || placeholder}
        </Text>
        <View style={styles.selectBoxIconContainer}>
          <FAIcon iconName={'FAAngleDown'} color={DarkGray} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

FATextInput.Money = props => {
  const {
    placeholder,
    currencyOrCoinCode,
    onPressIncrement,
    value,
    onChangeValue,
    decimalCount,
    fontSize,
  } = props;
  const [borderColor, setBorderColor] = useState(null);
  let mask = maskGenerator(decimalCount);
  return (
    <ThemeContext.Consumer>
      {({theme}) => {
        if (theme && !borderColor) {
          setBorderColor(theme?.colors?.softGray);
        }
        return (
          <View>
            {renderLabel(props)}
            <View
              style={[
                styles.moneyInputContainer,
                {
                  borderColor: borderColor,
                  backgroundColor: theme?.colors?.white,
                },
              ]}>
              <TextInputMask
                keyboardType={'decimal-pad'}
                onFocus={() => setBorderColor(theme?.colors?.soft0)}
                onBlur={() => setBorderColor(theme?.colors?.softGray)}
                value={value}
                onChangeText={newValue =>
                  onChangeValue && onChangeValue(newValue)
                }
                placeholderTextColor={theme?.colors?.gray}
                placeholder={placeholder}
                style={[
                  styles.moneyInput,
                  {color: theme?.colors?.black, fontSize: fontSize || 17},
                ]}
                mask={mask}
              />
              <View style={styles.moneyInputCodeAndIconContainer}>
                <Text
                  style={[
                    styles.moneyInputCurrencyOrCoinCodeText,
                    {
                      marginRight: onPressIncrement ? 10 : 20,
                      color: theme?.colors?.gray,
                    },
                  ]}>
                  {currencyOrCoinCode || ''}
                </Text>
                {onPressIncrement ? (
                  <TouchableOpacity
                    style={{width: 18, height: 18, marginRight: 20}}
                    onPress={() => onPressIncrement && onPressIncrement()}>
                    <FAIcon
                      iconName={'FAPlus'}
                      color={theme?.colors?.darkBlue}
                    />
                  </TouchableOpacity>
                ) : null}
              </View>
            </View>
          </View>
        );
      }}
    </ThemeContext.Consumer>
  );
};

FATextInput.ConfirmationCode = props => {
  const {
    disable,
    value,
    onChangeValue,
    onPressSendCodeButton,
    placeholder,
    rule,
    errorMessage,
  } = props;
  const [borderColor, setBorderColor] = useState(SoftGray);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;
  if (rule) {
    isValid = Rules[rule](value);
  }
  const hasError = !isValid && onBlur;
  return (
    <View>
      {renderLabel(props)}
      <View
        style={[
          styles.confirmationCodeInputContainer,
          {
            borderColor: hasError ? Danger : borderColor,
          },
        ]}>
        <TextInput
          keyboardType={'number-pad'}
          value={value}
          onChangeText={newValue => onChangeValue && onChangeValue(newValue)}
          onFocus={() => {
            setBorderColor(SoftO);
            setOnBlur(false);
          }}
          onBlur={() => {
            setBorderColor(SoftGray);
            setOnBlur(true);
          }}
          style={[
            styles.confirmationCodeInput,
            {
              backgroundColor: disable ? Orangray : White,
            },
          ]}
          editable={!disable}
          placeholder={placeholder}
        />
        <TouchableOpacity
          style={styles.confirmationCodeInputButton}
          onPress={() => onPressSendCodeButton && onPressSendCodeButton(value)}>
          <Text style={styles.confirmationCodeInputButtonText}>
            Kodu Gönder
          </Text>
        </TouchableOpacity>
      </View>
      {hasError && errorMessage ? (
        <Text style={styles.inputErrorMessage}>{errorMessage}</Text>
      ) : null}
    </View>
  );
};

FATextInput.WithRightIcon = props => {
  const {
    placeholder,
    value,
    onChangeValue,
    rightIcon,
    label,
    secureTextEntry,
    onPressRightIcon,
    iconType,
    rule,
    errorMessage,
    rightIconComponent,
    theme,
  } = props;
  return (
    <FATextInputWithIcon
      theme={theme}
      rule={rule}
      rightIconComponent={rightIconComponent}
      errorMessage={errorMessage}
      iconType={iconType}
      secureTextEntry={secureTextEntry}
      label={label}
      value={value}
      rightIcon={rightIcon}
      onChangeValue={newValue => onChangeValue && onChangeValue(newValue)}
      placeholder={placeholder || ''}
      onPressRightIcon={() => onPressRightIcon && onPressRightIcon()}
    />
  );
};

FATextInput.Password = props => {
  const {
    placeholder,
    value,
    onChangeValue,
    label,
    iconType,
    rule,
    errorMessage,
    keyboardType,
    isPasswordShowing,
    onPressRightIcon,
  } = props;

  return (
    <FATextInputWithIcon
      keyboardType={keyboardType}
      iconType={iconType}
      secureTextEntry={isPasswordShowing}
      label={label}
      value={value}
      rightIcon={{
        name: isPasswordShowing ? 'FAEyeSlash' : 'FAEye',
        size: 18,
        color: '#B1B0AF',
      }}
      onChangeValue={newValue => onChangeValue && onChangeValue(newValue)}
      placeholder={placeholder || ''}
      onPressRightIcon={() => onPressRightIcon && onPressRightIcon()}
      rule={rule}
      errorMessage={errorMessage}
    />
  );
};

FATextInput.WithLeftIcon = props => {
  const {
    placeholder,
    value,
    onChangeValue,
    leftIcon,
    label,
    secureTextEntry,
    onPressLeftIcon,
    iconType,
    rule,
    errorMessage,
  } = props;
  return (
    <FATextInputWithIcon
      rule={rule}
      errorMessage={errorMessage}
      iconType={iconType}
      secureTextEntry={secureTextEntry}
      label={label}
      value={value}
      leftIcon={leftIcon}
      onChangeValue={newValue => onChangeValue && onChangeValue(newValue)}
      placeholder={placeholder || ''}
      onPressLeftIcon={() => onPressLeftIcon && onPressLeftIcon()}
    />
  );
};

FATextInput.WithButton = props => {
  const {
    label,
    onChangeValue,
    value,
    placeholder,
    buttonText,
    onPress,
    rule,
    errorMessage,
  } = props;
  const [borderColor, setBorderColor] = useState(SoftGray);
  const [onBlur, setOnBlur] = useState(false);
  let isValid = true;
  if (rule) {
    isValid = Rules[rule](value);
  }
  const hasError = !isValid && onBlur;
  return (
    <>
      {label ? <Text style={styles.defaultLabel}>{label}</Text> : null}
      <View
        style={[
          styles.inputWithButtonContainer,
          {borderColor: hasError ? Danger : borderColor},
        ]}>
        <TextInput
          onFocus={() => {
            setBorderColor(SoftO);
            setOnBlur(false);
          }}
          onBlur={() => {
            setBorderColor(SoftGray);
            setOnBlur(true);
          }}
          onChangeText={newValue => onChangeValue && onChangeValue(newValue)}
          value={value}
          placeholder={placeholder}
          style={styles.inputWithButtonInputStyle}
          placeholderTextColor={Gray}
        />
        <FAButton
          onPress={() => onPress && onPress()}
          text={buttonText}
          containerStyle={styles.inputWithButtonContainerButton}
          textStyle={styles.inputWithButtonTextButton}
        />
      </View>
      {hasError && errorMessage ? (
        <Text style={styles.inputErrorMessage}>{errorMessage}</Text>
      ) : null}
    </>
  );
};

FATextInput.BigLine = props => {
  const {
    value,
    onChangeValue,
    placeholder,
    currencyOrCoinCode,
    decimalCurrencyCount,
    decimalCoinCount,
  } = props;
  const mask = maskGenerator(decimalCurrencyCount || decimalCoinCount);
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={styles.bigLineInputContainer}>
          <View style={styles.bigLineInputAndVerticalLineContainer}>
            <TextInputMask
              keyboardType={'decimal-pad'}
              onChangeText={newValue =>
                onChangeValue && onChangeValue(newValue)
              }
              value={value}
              placeholder={placeholder || ''}
              placeholderTextColor={theme?.colors?.darkBlue}
              style={[styles.bigLineInputStyle, {color: theme?.colors?.black}]}
              mask={mask}
            />
          </View>
          {currencyOrCoinCode ? (
            <Text
              style={[
                styles.bigLineInputCurrencyOrCoinCode,
                {color: theme?.colors?.darkBlue},
              ]}>
              {currencyOrCoinCode}
            </Text>
          ) : null}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FATextInput.ExchangeCounter = props => {
  const {
    value,
    placeholder,
    onChangeValue,
    onPressIncrement,
    onPressDecrement,
    decimalCurrencyCount,
    decimalCoinCount,
  } = props;

  let mask = maskGenerator(decimalCurrencyCount || decimalCoinCount);

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            [
              styles.exchangeCounterMainContainer,
              {backgroundColor: theme?.colors?.vGray},
            ],
          ]}>
          {onPressDecrement ? (
            <TouchableOpacity
              onPress={() => onPressDecrement && onPressDecrement()}
              style={[
                styles.exchangeCounterMinus,
                {position: 'absolute', left: 5, width: 16, height: 16},
              ]}>
              {/*TODO Renk verilmedi. İstenecek */}

              <FAIcon iconName={'FAMinus'} color={theme?.colors?.black} />
            </TouchableOpacity>
          ) : null}
          <TextInputMask
            keyboardType={'decimal-pad'}
            onChangeText={newValue => onChangeValue && onChangeValue(newValue)}
            value={value}
            placeholder={placeholder || ''}
            placeholderTextColor={theme?.colors?.gray}
            style={[
              styles.exchangeCounterInputStyle,
              {paddingVertical: 10, color: theme?.colors?.black},
            ]}
            mask={mask}
          />
          {onPressIncrement ? (
            <TouchableOpacity
              onPress={() => onPressIncrement && onPressIncrement()}
              style={[
                styles.exchangeCounterPlus,
                {position: 'absolute', right: 5, width: 16, height: 16},
              ]}>
              {/*TODO Renk verilmedi. İstenecek */}

              <FAIcon iconName={'FAPlus'} color={theme?.colors?.black} />
            </TouchableOpacity>
          ) : null}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FATextInput;
