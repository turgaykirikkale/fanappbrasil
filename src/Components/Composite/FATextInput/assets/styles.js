import {StyleSheet} from 'react-native';
import {
  SoftGray,
  DarkGray,
  Black,
  VGray,
  Gray,
  Orange,
  Danger,
  Success,
  White,
  DarkBlue,
} from '@Commons/FAColor';

const withIconOrButtonCommonContainerStyle = {
  borderWidth: 1,
  borderColor: SoftGray,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  backgroundColor: White,
};

const commonInputStyle = {
  fontSize: 14,
  color: Black,
};

export const styles = StyleSheet.create({
  defaultLabel: {
    marginBottom: 8,
    fontSize: 14,
    color: DarkGray,
  },
  defaultInputStyle: {
    paddingVertical: 15,
    paddingLeft: 16,
    borderWidth: 1,
    borderRadius: 4,
    ...commonInputStyle,
    backgroundColor: White,
  },
  dropdownInputContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4,
  },
  dropdownButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  withPickerIOSStyle: {
    paddingVertical: 15,
    letterSpacing: -0.8,
    marginRight: 16,
    marginLeft: 8,
  },
  withPickerAndroidStyle: {
    paddingVertical: 9,
    letterSpacing: -0.8,
    marginRight: 16,
    color: 'black',
    marginLeft: 8,
  },
  withPickericonContainer: {
    marginLeft: 15,
    marginVertical: 15,
  },
  withPickerDivider: {
    borderLeftWidth: 1,
    borderColor: SoftGray,
    height: '100%',
    marginLeft: 5,
  },
  dropdownSelectedItemIcon: {
    width: 29,
    height: 29,
    borderRadius: 58,
    marginLeft: 10,
  },
  dropdownSelectedItemText: {
    marginLeft: 8,
    fontSize: 14,
    color: Black,
  },
  dropdownArrowIcon: {marginLeft: 10, width: 16, height: 16},
  dropdownVerticalDivider: {
    backgroundColor: SoftGray,
    width: 1,
    height: '100%',
    marginLeft: 10,
  },
  dropdownInputStyle: {
    paddingVertical: 15,
    paddingLeft: 15,
    ...commonInputStyle,
    flex: 1,
  },
  selectBoxContainer: {
    borderWidth: 1,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectBoxText: {
    paddingLeft: 16,
    paddingVertical: 15,
  },
  selectBoxIconContainer: {
    width: 16,
    height: 16,
    paddingRight: 14,
  },
  moneyInputContainer: {
    borderWidth: 1,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: White,
  },
  moneyInput: {
    paddingVertical: 13,
    paddingLeft: 16,
    flex: 1,
    color: Black,
    fontSize: 17,
  },
  moneyInputCodeAndIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 3,
  },
  moneyInputCurrencyOrCoinCodeText: {
    fontSize: 17,
    color: Gray,
    paddingVertical: 13,
  },
  moneyInputIconContainer: {
    marginRight: 16,
    backgroundColor: SoftGray,
    borderRadius: 40,
  },
  moneyInputIcon: {
    paddingVertical: 6,
    paddingHorizontal: 8,
    width: 16,
    height: 16,
  },
  confirmationCodeInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: White,
  },
  confirmationCodeInput: {
    ...commonInputStyle,
    flex: 1,
    paddingVertical: 15,
    paddingLeft: 16,
  },
  confirmationCodeInputButton: {
    marginRight: 18,
  },
  confirmationCodeInputButtonText: {
    fontSize: 14,
    color: Orange,
  },
  inputErrorMessage: {
    fontSize: 12,
    color: Danger,
    marginTop: 4,
  },
  inputSuccessMessage: {
    fontSize: 12,
    color: Success,
    marginTop: 4,
  },
  inputWithIconContainer: {
    borderRadius: 4,
    ...withIconOrButtonCommonContainerStyle,
  },
  inputWithIconInputStyle: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 15,
    ...commonInputStyle,
  },
  inputWithButtonContainer: {
    ...withIconOrButtonCommonContainerStyle,
    borderRadius: 4,
  },
  inputWithButtonInputStyle: {
    ...commonInputStyle,
    flex: 1,
    paddingVertical: 15,
    paddingLeft: 16,
  },
  inputWithButtonContainerButton: {
    backgroundColor: SoftGray,
    marginLeft: 8,
  },
  inputWithButtonTextButton: {
    marginVertical: 0,
    fontSize: 14,
    color: DarkGray,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  bigLineInputContainer: {
    alignItems: 'center',
  },
  bigLineInputAndVerticalLineContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bigLineInputStyle: {
    fontSize: 48,
    color: Black,
    flex: 1,
    textAlign: 'center',
  },
  bigLineInputVerticalLine: {
    width: 1,
    height: '100%',
    marginLeft: 8,
    backgroundColor: '#0000004D',
  },
  bigLineInputCurrencyOrCoinCode: {
    color: DarkBlue,
    fontSize: 20,
    marginTop: 4,
  },
  exchangeCounterMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: VGray,
    borderRadius: 4,
  },
  exchangeCounterMinus: {
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
  exchangeCounterInputStyle: {
    color: Black,
    fontSize: 14,
    flex: 1,
    alignSelf: 'center',
    textAlign: 'center',
    paddingHorizontal: 6,
  },
  exchangeCounterPlus: {
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
});
