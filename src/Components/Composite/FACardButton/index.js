import {DarkBlue, Black} from '@Commons/FAColor';
import {Text, TouchableOpacity, View} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import React from 'react';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import Icons from '@Components/UI/FAIcon/assets/Icons';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FACardButton = props => {
  const {iconName, text, onPress, passive} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          onPress={() => onPress && !passive && onPress()}
          activeOpacity={onPress && !passive ? 0.7 : 1}
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <View style={styles.iconAndTextContainer}>
            {iconName ? (
              Icons[iconName] ? (
                <FAIcon
                  iconName={iconName}
                  color={
                    passive ? theme?.colors?.darkBlue : theme?.colors?.black
                  }
                  size={14}
                />
              ) : (
                <FAVectorIcon
                  iconName={iconName}
                  color={
                    passive ? theme?.colors?.darkBlue : theme?.colors?.black
                  }
                  size={14}
                />
              )
            ) : null}
            <Text
              style={[
                styles.text,
                {
                  color: passive
                    ? theme?.colors?.darkBlue
                    : theme?.colors?.black,
                },
              ]}>
              {text}
            </Text>
          </View>
          {onPress && (
            <FAVectorIcon
              iconName={'chevron-right'}
              size={8}
              color={passive ? theme?.colors?.darkBlue : theme?.colors?.black}
            />
          )}
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FACardButton.propTypes = {
  iconName: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  passive: PropTypes.bool,
};

export default FACardButton;
