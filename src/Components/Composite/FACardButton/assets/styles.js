import {StyleSheet} from 'react-native';
import {DarkBlue, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 8,
    backgroundColor: White,
    flex: 1,
  },
  iconAndTextContainer: {flexDirection: 'row', alignItems: 'center'},
  text: {marginLeft: 6, fontSize: 12},
});
