import React from 'react';
import {View, Text, Image} from 'react-native';
import {styles} from './assets/styles';
import Localization from '@Localization';
import LinearGradient from 'react-native-linear-gradient';

const FAPollImage = props => {
  const {imageName, leftTime, explainedText} = props;
  return (
    <View>
      <Image
        style={styles.imageContainer}
        source={{
          uri: imageName,
        }}
      />
      <View style={styles.leftTimeMainContainer}>
        <View style={styles.leftTimeChilfContainer}>
          <Text style={styles.leftTimeTextStyle}>
            {`${leftTime} ${Localization.t('PollScreenFix.DaysLeft')}`}
          </Text>
        </View>
      </View>
      <LinearGradient
        style={styles.explainedTextLinear}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 0.5}}
        colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.7)']}>
        <View style={styles.explainedTextContainer}>
          <Text style={styles.explainedTextStyle}>{explainedText}</Text>
        </View>
      </LinearGradient>
    </View>
  );
};

export default FAPollImage;
