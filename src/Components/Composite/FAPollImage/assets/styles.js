import {Dimensions, StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

const screenWidth = Dimensions.get('screen').width;
export const styles = StyleSheet.create({
  imageContainer: {width: '100%', height: 200},
  leftTimeMainContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    marginRight: 12,
    marginTop: 8,
  },
  leftTimeChilfContainer: {
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 9,
    backgroundColor: '#000000B2',
  },
  leftTimeTextStyle: {
    color: FAColor.White,
    fontSize: 10,
    fontWeight: '700',
    letterSpacing: -0.8,
  },
  explainedTextLinear: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 45,
    height: screenWidth / 4,
  },
  explainedTextContainer: {
    position: 'absolute',
    bottom: 0,
    marginHorizontal: 24,
    marginBottom: 14,
  },
  explainedTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    letterSpacing: -0.8,
  },
});
