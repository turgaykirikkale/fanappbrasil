import FAMarketHistoryItem from '@Components/UI/FAMarketHistoryItem';
import {FlatList, StyleSheet, View} from 'react-native';
import {VGray} from '@Commons/FAColor';
import React, {useState} from 'react';
import FASorting from '../FASorting';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAMarketHistoryList = props => {
  const {data} = props;
  const [filterLogic, setFilterLogic] = useState({
    date: true,
    amount: undefined,
    price: undefined,
  });

  if (_.isEmpty(data)) {
    return null;
  }

  const filterData = () => {
    let filteredMarketHistoryList = data;
    const {date, amount, price} = filterLogic;
    if (date !== undefined) {
      if (date) {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'CreatedOn',
        ).reverse();
      } else {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'CreatedOn',
        );
      }
    } else if (amount !== undefined) {
      if (amount) {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'CoinValue',
        ).reverse();
      } else {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'CoinValue',
        );
      }
    } else if (price !== undefined) {
      if (price) {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'Price',
        ).reverse();
      } else {
        filteredMarketHistoryList = _.sortBy(
          filteredMarketHistoryList,
          'Price',
        );
      }
    }
    return filteredMarketHistoryList;
  };
  const generatedData = filterData();
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <FlatList
          ListHeaderComponent={() => (
            <View
              style={[
                styles.filterLogicContainer,
                {backgroundColor: theme?.colors?.vGray},
              ]}>
              <FASorting
                containerStyle={[
                  styles.filterDateContainer,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                onPress={flag => {
                  setFilterLogic({
                    date: flag,
                    amount: undefined,
                    price: undefined,
                  });
                }}
                value={filterLogic.date}
                text={Localization.t('ReferenceScreenAllList.Date')}
              />
              <FASorting
                containerStyle={[
                  styles.filterAmountContainer,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                onPress={flag =>
                  setFilterLogic({
                    date: undefined,
                    amount: flag,
                    price: undefined,
                  })
                }
                value={filterLogic.amount}
                text={Localization.t('StockStandartScreen.Amount')}
              />
              <FASorting
                containerStyle={[
                  styles.filterPriceContainer,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                onPress={flag =>
                  setFilterLogic({
                    date: undefined,
                    amount: undefined,
                    price: flag,
                  })
                }
                value={filterLogic.price}
                text={Localization.t('StockStandartScreen.Price')}
              />
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
          data={generatedData}
          ItemSeparatorComponent={() => (
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: theme?.colors?.blueGray,
              }}
            />
          )}
          renderItem={({item, index}) => (
            <FAMarketHistoryItem
              createdDate={item.CreatedOn}
              coinValue={item.CoinValue}
              price={item.Price}
              type={item.MatchType}
            />
          )}
        />
      )}
    </ThemeContext.Consumer>
  );
};

const filterLogicCommonStyle = {
  flex: 1,
  backgroundColor: VGray,
  paddingVertical: 10,
};
export const styles = StyleSheet.create({
  filterLogicContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  filterDateContainer: {
    paddingLeft: 10,
    ...filterLogicCommonStyle,
  },
  filterAmountContainer: {
    ...filterLogicCommonStyle,
    alignItems: 'flex-end',
    marginLeft: 12,
  },
  filterPriceContainer: {
    ...filterLogicCommonStyle,
    alignItems: 'flex-end',
    paddingRight: 12,
  },
});

FAMarketHistoryList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      CreatedOn: PropTypes.string.isRequired,
      CoinValue: PropTypes.any.isRequired,
      Price: PropTypes.any.isRequired,
      MatchType: PropTypes.number.isRequired,
    }),
  ).isRequired,
};

export default FAMarketHistoryList;
