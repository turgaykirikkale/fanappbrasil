import React from 'react';
import FAButton from '@Components/Composite/FAButton';
import {Image, Modal, Text, TouchableOpacity, View} from 'react-native';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import Localization from '@Localization';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {connect} from 'react-redux';

const FAPairSelectedBox = props => {
  const {
    onChangeSelectedPair,
    selectedMarket,
    visible,
    onPressClose,
    MarketState,
  } = props;

  const marketList = MarketState.CoinRates;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <Modal animationType="none" transparent={true} visible={visible}>
          <View
            style={[
              styles.modalBackgroundContainer,
              {backgroundColor: theme?.colors?.darkBlack},
            ]}>
            <View
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              {onPressClose && (
                <TouchableOpacity
                  onPress={() => onPressClose()}
                  activeOpacity={0.7}
                  style={styles.closeButton}>
                  <FAVectorIcon
                    iconName={'times'}
                    size={22}
                    color={theme?.colors.darkBlue0}
                  />
                </TouchableOpacity>
              )}
              <View style={styles.imageContainer}>
                <Image
                  style={styles.imageStyle}
                  source={require('./assets/change-mode-icon.png')}
                />
              </View>
              <Text
                style={[styles.imageBottomText, {color: theme?.colors?.black}]}>
                Select the pair to be traded.
              </Text>
              <View style={styles.buttonMainContainer}>
                {marketList.map(item => (
                  <View key={item.id} style={styles.buttonContainer}>
                    <FAButton
                      disabled={selectedMarket.CurrencyId === item.CurrencyId}
                      onPress={() =>
                        onChangeSelectedPair && onChangeSelectedPair(item)
                      }
                      text={`${item.CurrencyCode}/${item.CoinCode}`}
                      containerStyle={[
                        styles.buttonContainerStyle,
                        {
                          backgroundColor:
                            selectedMarket.CurrencyId === item.CurrencyId
                              ? theme?.colors?.green
                              : theme?.colors?.blueGray,
                        },
                      ]}
                      textStyle={[
                        styles.buttonTextStyle,
                        {
                          color:
                            selectedMarket.CurrencyId === item.CurrencyId
                              ? theme?.colors?.white2
                              : theme?.colors?.darkBlue,
                        },
                      ]}
                    />
                  </View>
                ))}
              </View>
              <Text style={[styles.footerText, {color: theme?.colors?.gray}]}>
                Easily select the trading pair of your choice at any time. you
                can change
              </Text>
            </View>
          </View>
        </Modal>
      )}
    </ThemeContext.Consumer>
  );
};

FAPairSelectedBox.propTypes = {
  selectedMode: PropTypes.object.isRequired,
  onChangeMode: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  onPressClose: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {})(FAPairSelectedBox);
