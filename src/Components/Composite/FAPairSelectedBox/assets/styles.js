import {StyleSheet} from 'react-native';
import {White, Black, Gray} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  modalBackgroundContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,.6)',
  },
  mainContainer: {
    alignItems: 'center',
    marginHorizontal: 10,
    backgroundColor: White,
    borderRadius: 4,
    position: 'relative',
  },
  closeButton: {position: 'absolute', right: 10, top: 10},
  imageContainer: {
    marginTop: 32,
    alignItems: 'center',
  },
  imageStyle: {
    width: 50,
    height: 50,
  },
  imageBottomText: {
    marginTop: 16,
    fontSize: 16,
    color: Black,
  },
  buttonMainContainer: {
    marginTop: 22,
    marginHorizontal: 45,
  },
  buttonContainer: {
    marginTop: 10,
  },
  buttonContainerStyle: {
    paddingVertical: 15,
    borderRadius: 4,
  },
  buttonTextStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    marginHorizontal: 69,
    width: '100%',
  },
  footerText: {
    marginTop: 53,
    marginBottom: 32,
    marginHorizontal: 17,
    textAlign: 'center',
    color: Gray,
    fontSize: 14,
  },
  forStoryContainer: {flex: 1},
});
