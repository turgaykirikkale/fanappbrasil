import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.Black,
    flex: 1,
    opacity: 0.7,
    justifyContent: 'center',
  },
  childContainer: {
    backgroundColor: 'white',
    marginHorizontal: 10,
    borderRadius: 4,
    paddingVertical: 25,
  },
  checkIconContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: 98,
    height: 98,
    borderRadius: 49,
    backgroundColor: FAColor.Green,
    marginBottom: 15,
    letterSpacing: -0.5,
  },
  welcomeTextStyle: {fontSize: 16, color: FAColor.Black, textAlign: 'center'},
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 23,
    marginHorizontal: 25,
  },
  buttonTextStyle: {
    marginVertical: 10,
    color: FAColor.White,
  },
  VerifyTextStyle: {
    fontSize: 16,
    color: '#696969', //To Do color
    textAlign: 'center',
    marginTop: 16,
  },
  startNowButtonContainerStyle: {
    backgroundColor: FAColor.VBlueGray,
    borderRadius: 4,
    marginVertical: 23,
    marginHorizontal: 25,
  },
  startNowButtonTextStyle: {
    marginVertical: 10,
    color: FAColor.DarkBlue,
  },
});
