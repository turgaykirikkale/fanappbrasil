import React from 'react';
import {Modal, View, Text} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import FADivider from '@Components/UI/FADivider';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import FAIcon from '@Components/UI/FAIcon';
import Localization from '@Localization';

const FAIdentityEnterModal = props => {
  const {visible, onPressStartNow, onPressApproveAccount} = props;
  return (
    <Modal visible={visible}>
      <View style={styles.mainContainer}>
        <View style={styles.childContainer}>
          <View style={styles.checkIconContainer}>
            <View style={{width: 40, height: 40}}>
              <FAIcon iconName={'FACheck'} color={FAColor.White} />
            </View>
          </View>
          <Text style={styles.welcomeTextStyle}>
            {Localization.t('IdentityEnterScreen.welcomeText')}
          </Text>
          <FAButton
            disabled={false}
            onPress={() => onPressStartNow && onPressStartNow()}
            containerStyle={styles.buttonContainer}
            textStyle={styles.buttonTextStyle}
            text={Localization.t('IdentityEnterScreen.StartNow')}
          />
          <FADivider text={Localization.t('Commons.Or')} />
          <Text style={styles.VerifyTextStyle}>
            {Localization.t('IdentityEnterScreen.approvedAccountText')}
          </Text>
          <FAButton
            disabled={false}
            onPress={() => onPressApproveAccount && onPressApproveAccount()}
            containerStyle={styles.startNowButtonContainerStyle}
            textStyle={styles.startNowButtonTextStyle}
            text={Localization.t('IdentityEnterScreen.VerifyAccount')}
          />
        </View>
      </View>
    </Modal>
  );
};
export default FAIdentityEnterModal;
