import FAStockTransactionHistoryItem from '@Components/UI/FAStockTransactionHistoryItem';
import {ActivityIndicator, FlatList, View} from 'react-native';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import React from 'react';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAStockTransactionHistoryList = props => {
  const {data, onEndReached, dataLoading} = props;
  if (!data || !data.length || !data.length > 0) {
    return null;
  }
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <FlatList
          onEndReached={() => onEndReached && onEndReached()}
          ItemSeparatorComponent={() => (
            <View
              style={[
                styles.itemSeparator,
                {borderBottomColor: theme?.colors?.vGray},
              ]}
            />
          )}
          ListFooterComponent={() => {
            if (dataLoading) {
              return (
                <View style={styles.loaderContainer}>
                  <ActivityIndicator size={'large'} color={'orange'} />
                </View>
              );
            }
            return null;
          }}
          keyExtractor={(item, index) => index}
          data={data}
          renderItem={({item, index}) => {
            let generatedCommission = null;
            let generatedTotalPrice = item.Total || 0.0;
            if (item && item.CommissionRate && item.Total) {
              generatedCommission = item.Total * (item.CommissionRate / 100);
              if (item.Type === 1) {
                generatedTotalPrice = item.Total + generatedCommission;
              } else {
                generatedTotalPrice = item.Total - generatedCommission;
              }
            }
            return (
              <View style={styles.listContainer}>
                <FAStockTransactionHistoryItem
                  key={index}
                  type={item.Type}
                  coinCode={item.CoinCode || null}
                  currencyCode={item.CurrencyCode || null}
                  date={item.CreatedOn || null}
                  coinAmount={item.CoinValue || 0.0}
                  commission={generatedCommission}
                  coinPrice={item.Price || 0.0}
                  totalPrice={generatedTotalPrice}
                  state={item.State || null}
                />
              </View>
            );
          }}
        />
      )}
    </ThemeContext.Consumer>
  );
};

FAStockTransactionHistoryList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      CoinCode: PropTypes.string.isRequired,
      CurrencyCode: PropTypes.string.isRequired,
      CreatedOn: PropTypes.string.isRequired,
      CoinValue: PropTypes.number.isRequired,
      CommissionRate: PropTypes.number.isRequired,
      Price: PropTypes.number.isRequired,
      Total: PropTypes.number.isRequired,
      State: PropTypes.number.isRequired,
    }),
  ).isRequired,
  onEndReached: PropTypes.func,
  dataLoading: PropTypes.bool,
};

FAStockTransactionHistoryList.defaultProps = {
  dataLoading: false,
};

export default FAStockTransactionHistoryList;
