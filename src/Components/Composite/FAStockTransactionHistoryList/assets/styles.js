import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  listContainer: {paddingVertical: 9, paddingHorizontal: 10},
  itemSeparator: {borderWidth: 1},
  loaderContainer: {paddingVertical: 20},
});
