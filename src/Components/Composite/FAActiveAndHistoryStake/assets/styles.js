import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  headerContainer: {
    flexDirection: 'row',
    marginVertical: 12,
    marginHorizontal: 20,
  },
  headerTextStyle: {flex: 1, fontWeight: 'bold', color: '#3B3B3B'}, //TO DO COLOR
  sortingContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
    backgroundColor: FAColor.VGray,
  },
  flex1: {flex: 1},
  nameAndSurnameSortingContainer: {
    backgroundColor: FAColor.VGray,
    flex: 1,
    marginLeft: 10,
  },
  FlatListStyle: {height: 105},
  noReferenceTextStyle: {textAlign: 'center', marginTop: 5},
  renderItemContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#F7F7F7', //TO DO COLOR
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignContent: 'center',
  },
  dateTextStyle: {fontSize: 12, color: '#3B3B3B', flex: 1}, // TO DO COLOR
  nameAndSurnameTextStyle: {
    fontSize: 12,
    color: '#3B3B3B', //TO DO COLOR
    flex: 1,
  },
  WinningCommissionTextStyle: {fontSize: 12, color: '#3B3B3B'}, // TO DO COLOR
});
