import React from 'react';
import {View, FlatList, Image, Text, RefreshControl} from 'react-native';
import FAColor from '@Commons/FAColor';
import FAButton from '@Components/Composite/FAButton';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import moment from 'moment';
import {ToFixed, FAToFixed} from '@Commons/FAMath';
import Localization from '@Localization';

const FAActiveAndHistoryStake = props => {
  const {data, onPressStakeQuite, isScreenRefreshing, onRefreshControl} = props;
  return (
    <FlatList
      refreshControl={
        <RefreshControl
          tintColor={'orange'}
          refreshing={isScreenRefreshing}
          onRefresh={() => onRefreshControl && onRefreshControl()}
        />
      }
      keyExtractor={(item, index) => index}
      data={data}
      renderItem={({item, index}) => {
        let flexLeft;
        let flexRight;
        if (item.Day) {
          const date1 = new Date();
          const date2 = new Date(item.EndDate);
          const diffTime = Math.abs(date2 - date1);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          flexLeft = item.Day - diffDays;
          flexRight = item.Day - flexLeft;
        }

        return (
          <ThemeContext.Consumer>
            {({theme}) => (
              <View
                style={{
                  backgroundColor: theme?.colors?.blueGray,
                  borderBottomWidth: 0.7,
                  paddingVertical: 15,
                  borderColor: FAColor.SoftGray,
                }}>
                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 10,
                      marginHorizontal: 10,
                    }}>
                    <Image
                      width={30}
                      height={30}
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 15,
                      }}
                      source={{
                        uri: `https://borsa.bitci.com/img/coin/BITCI.png`,
                      }}
                    />
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginLeft: 12,
                        color: theme?.colors?.black,
                      }}>
                      {item.CoinCode || null}
                    </Text>

                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                      <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                        {Localization.t('StakeScreen.RateOfReturn')}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          color: theme?.colors?.faOrange,
                          fontWeight: 'bold',
                          marginTop: 4,
                        }}>
                        {`+% ${item.InterestRate}` || null}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginHorizontal: 10,
                      marginTop: 15,
                      marginBottom: 15,
                    }}>
                    <View>
                      <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                        {Localization.t('StakeScreen.LockedAmount')}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          color: theme?.colors?.black,
                          fontWeight: 'bold',
                          marginTop: 4,
                        }}>
                        {`${item.Amount.toFixed(2)} ${item.CoinCode}` || null}
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          color: theme?.colors?.black,
                          fontSize: 12,
                          textAlign: 'right',
                        }}>
                        {Localization.t('StakeScreen.CurrentStake')}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          color: theme?.colors?.black,
                          fontWeight: 'bold',
                          marginTop: 4,
                          textAlign: 'right',
                        }}>
                        {`${item.Gain.toFixed(2)} ${item.CoinCode}` || null}
                      </Text>
                    </View>
                  </View>
                </View>
                {item.InterestStakeStatusEnumId === 3 ? (
                  <View
                    style={{
                      marginHorizontal: 10,
                      flexDirection: 'row',
                      flex: 1,
                      justifyContent: 'space-between',
                    }}>
                    <FAButton
                      disabled={true}
                      containerStyle={{
                        borderWidth: 1,
                        borderColor: theme?.colors?.black,
                        borderRadius: 4,
                      }}
                      textStyle={{
                        color: theme?.colors?.black,
                        marginVertical: 8,
                        marginHorizontal: 10,
                      }}
                      text={
                        item.InterestStakeType === 'Esnek'
                          ? Localization.t('StakeScreen.Flexible')
                          : Localization.t('StakeScreen.Locked')
                      }
                    />
                    <View style={{flex: 1, marginLeft: 10}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{color: theme?.colors?.black}}>
                          {`${Localization.t('StakeScreen.StartDate')} :`}
                        </Text>
                        <Text style={{color: theme?.colors?.black}}>
                          {moment(item.StartDate).format('DD-MM-yyyy HH:mm:ss')}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{color: theme?.colors?.black}}>
                          {`${Localization.t('StakeScreen.EndDate')} :`}
                        </Text>
                        <Text style={{color: theme?.colors?.black}}>
                          {moment(item.EndDate).format('DD-MM-yyyy HH:mm:ss')}
                        </Text>
                      </View>
                    </View>
                  </View>
                ) : item.Day ? (
                  <View style={{marginHorizontal: 10, marginVertical: 10}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      <Text style={{color: theme?.colors?.black}}>
                        {`${flexLeft}/${item.Day} ${Localization.t(
                          'StakeScreen.days',
                        )}` || null}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 2}}>
                      <View
                        style={{
                          flex: flexLeft,
                          backgroundColor: FAColor.Orange,
                          height: 10,
                          borderTopLeftRadius: 10,
                          borderBottomLeftRadius: 10,
                          borderTopRightRadius:
                            flexLeft === item.Day ? 10 : null,
                          borderBottomRightRadius:
                            flexLeft === item.Day ? 10 : null,
                        }}
                      />
                      <View
                        style={{
                          flex: flexRight,
                          backgroundColor: FAColor.SoftGray,
                          borderTopRightRadius: 10,
                          borderBottomRightRadius: 10,
                          borderTopLeftRadius: flexLeft === 0 ? 10 : null,
                          borderBottomLeftRadius: flexLeft === 0 ? 10 : null,
                        }}
                      />
                    </View>
                  </View>
                ) : (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginHorizontal: 10,
                      marginVertical: 10,
                    }}>
                    <FAButton
                      disabled={true}
                      containerStyle={{
                        borderWidth: 1,
                        borderColor: theme?.colors?.black,
                        borderRadius: 4,
                        marginRight: 5,
                        flex: 1,
                      }}
                      textStyle={{
                        color: theme?.colors?.black,
                        marginVertical: 8,
                      }}
                      text={Localization.t('StakeScreen.FlexibleStake')}
                    />
                    <FAButton
                      onPress={() =>
                        onPressStakeQuite && onPressStakeQuite(item)
                      }
                      disabled={
                        item.InterestStakeStatusEnumId === 2 ? true : false
                      }
                      containerStyle={{
                        borderRadius: 4,
                        backgroundColor:
                          item.InterestStakeStatusEnumId === 2
                            ? FAColor.DarkGray
                            : FAColor.Red,
                        marginLeft: 5,
                        flex: 1,
                      }}
                      textStyle={{color: FAColor.White, marginVertical: 8}}
                      text={
                        item.InterestStakeStatusEnumId === 2
                          ? Localization.t('StakeScreen.Waiting')
                          : Localization.t('StakeScreen.StopStake')
                      }
                    />
                  </View>
                )}
              </View>
            )}
          </ThemeContext.Consumer>
        );
      }}
    />
  );
};

export default FAActiveAndHistoryStake;
