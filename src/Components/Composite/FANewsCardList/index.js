import React from 'react';
import {Dimensions, FlatList, Platform, View} from 'react-native';
import PropTypes from 'prop-types';
import FANewsCard from '@Components/UI/FANewsCard';
import {NewsContentType} from '@Commons/Enums/NewsContentType';

const FANewsCardList = props => {
  const {data, onPress} = props;
  if (data && data.length && data.length > 0) {
    const isAndroid = Platform.OS === 'android';
    const screenWidth = Dimensions.get('window').width / 3;
    return (
      <FlatList
        nestedScrollEnabled
        horizontal
        data={data}
        keyExtractor={(item, index) => index}
        showsHorizontalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={{marginLeft: 5}} />}
        renderItem={({item, index}) => {
          return (
            <FANewsCard
              cardHeight={160}
              cardWidth={screenWidth}
              content={item.Content}
              image={item.UrlPath}
              teamName={item.TeamName}
              isVideoContent={item.ContentType === NewsContentType.Video}
              onPress={() => onPress && onPress(item)}
            />
          );
        }}
      />
    );
  }
  return null;
};

FANewsCardList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      UrlPath: PropTypes.string.isRequired,
      TeamName: PropTypes.string,
      ContentType: PropTypes.number,
      Content: PropTypes.string.isRequired,
    }),
  ).isRequired,
  onPress: PropTypes.func.isRequired,
};

export default FANewsCardList;
