import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FAIcon from '../../UI/FAIcon';
const FAPickerComponent = props => {
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View>
          <Text style={{color: theme?.colors?.darkBlue}}>{props.label}</Text>
          <TouchableOpacity
            disabled={props.disabled}
            onPress={() => props.onPress && props.onPress()}
            activeOpacity={0.7}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingLeft: 18,
              borderWidth: 1,
              paddingVertical: 16,
              borderRadius: 4,
              paddingRight: 4,
              marginTop: 8,
              borderColor: theme?.colors?.gray,
            }}>
            <Text
              style={{
                color: props.colorControl
                  ? theme?.colors?.black
                  : theme?.colors?.softGray,
              }}>
              {props.placeholder}
            </Text>
            <View style={{marginLeft: 10, width: 16, height: 16}}>
              <FAIcon iconName={'FAAngleDown'} color={'#5A5A5A'} />
            </View>
          </TouchableOpacity>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};
export default FAPickerComponent;
