import React from 'react';
import {View, TouchableOpacity, Text, ActivityIndicator} from 'react-native';
import {styles} from './assets/styles';
import FAIcon from '@Components/UI/FAIcon';

const FAButton = props => {
  const {
    disabled,
    containerStyle,
    onPress,
    textStyle,
    text,
    ActivityIndicatorColor,
    rightIconName,
    rightIconColor,
    rightIconSize,
    leftIconName,
    leftIconSize,
    leftIconColor,
  } = props;
  return (
    <TouchableOpacity
      style={[
        styles.container,
        containerStyle,
        {opacity: disabled ? 0.7 : null},
        {flexDirection: rightIconName || leftIconName ? 'row' : null},
      ]}
      activeOpacity={0.7}
      disabled={disabled}
      onPress={() => onPress && onPress()}>
      {leftIconName ? (
        <View
          style={[
            styles.leftIconStyle,
            {width: leftIconSize, height: leftIconSize, alignSelf: 'center'},
          ]}>
          <FAIcon iconName={leftIconName} color={leftIconColor} />
        </View>
      ) : null}
      {text ? (
        <Text style={[styles.buttonTextStyle, textStyle]}>{text}</Text>
      ) : (
        <View style={styles.buttonActivityIndicator}>
          <ActivityIndicator color={ActivityIndicatorColor} />
        </View>
      )}
      {rightIconName ? (
        <View
          style={[
            styles.rightIconStyle,
            {
              width: rightIconSize,
              height: rightIconSize,
            },
          ]}>
          <FAIcon iconName={rightIconName} color={rightIconColor} />
        </View>
      ) : null}
    </TouchableOpacity>
  );
};

export default FAButton;
