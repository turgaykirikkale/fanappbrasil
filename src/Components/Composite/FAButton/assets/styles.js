import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  buttonTextStyle: {
    letterSpacing: -0.4,
    textAlign: 'center',
    alignSelf: 'center',
  },
  buttonActivityIndicator: {
    marginHorizontal: 42,
    marginVertical: 15,
    alignSelf: 'center',
  },
  rightIconStyle: {justifyContent: 'center', marginLeft: 4},
  leftIconStyle: {marginRight: 4},
});
