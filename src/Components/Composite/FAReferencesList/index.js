import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import FASorting from '@Components/Composite/FASorting';
import FAColor from '@Commons/FAColor';
import FAButton from '@Components/Composite/FAButton';
import _ from 'lodash';
import moment from 'moment';
import {styles} from './assets/styles';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

export default class FAReferencesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterLogic: {
        date: true,
        nameAndSurname: undefined,
        earn: undefined,
      },
    };
  }

  renderItem(item, theme) {
    const {save} = this.props;
    return (
      <View
        style={[
          styles.renderItemContainer,
          {borderColor: theme?.colors?.blueGray},
        ]}>
        <Text style={[styles.dateTextStyle, {color: theme?.colors?.darkBlack}]}>
          {moment(item.CreatedOn).format('YYYY-MM-DD')}
        </Text>
        <Text
          style={[
            styles.nameAndSurnameTextStyle,
            {textAlign: save ? null : 'right', color: theme?.colors?.darkBlack},
          ]}>
          {item.NameSurname}
        </Text>
        {save ? (
          <Text
            style={[
              styles.WinningCommissionTextStyle,
              {color: theme?.colors?.darkBlack},
            ]}>
            {item.WinningCommission}
          </Text>
        ) : null}
      </View>
    );
  }

  filterAction(List) {
    if (List && List.length > 0) {
      let filteredList = List;
      const {filterLogic} = this.state;
      const {date, nameAndSurname, earn} = filterLogic;

      if (date !== undefined) {
        if (date) {
          filteredList = _.sortBy(filteredList, 'date');
        } else {
          filteredList = _.sortBy(filteredList, 'date').reverse();
        }
      } else if (nameAndSurname !== undefined) {
        if (nameAndSurname) {
          filteredList = _.sortBy(filteredList, 'nameAndSurname').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'nameAndSurname');
        }
      } else if (earn !== undefined) {
        if (earn) {
          filteredList = _.sortBy(filteredList, 'earn').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'earn');
        }
      }

      return filteredList;
    }
    return List;
  }

  render() {
    const {filterLogic} = this.state;
    const {save, data, onPress} = this.props;
    let filteredList;
    filteredList = this.filterAction(data);
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <View style={styles.headerContainer}>
              <Text
                style={[
                  styles.headerTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {save
                  ? Localization.t('ReferenceScreenFix.MyProfit')
                  : Localization.t('ReferenceScreenFix.Myreferences')}
              </Text>
              <FAButton
                disabled={false}
                onPress={() => onPress && onPress(data, save)}
                containerStyle={{marginLeft: 8}}
                textStyle={{color: theme?.colors?.faOrange}}
                text={Localization.t('CommonsFix.All')}
              />
            </View>
            <View
              style={[
                styles.sortingContainer,
                {backgroundColor: theme?.colors?.vGray},
              ]}>
              <FASorting
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: flag,
                        nameAndSurname: undefined,
                        earn: undefined,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                initialValue={true}
                value={filterLogic.date}
                containerStyle={styles.flex1}
                text={Localization.t('CommonsFix.Date')}
              />
              <View
                style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                <FASorting
                  onPress={flag =>
                    this.setState(
                      {
                        filterLogic: {
                          date: undefined,
                          nameAndSurname: flag,
                          earn: undefined,
                        },
                      },
                      () => this.filterAction(),
                    )
                  }
                  value={filterLogic.nameAndSurname}
                  containerStyle={[
                    styles.nameAndSurnameSortingContainer,
                    {
                      alignItems: save ? null : 'flex-end',
                      backgroundColor: theme?.colors?.vGray,
                    },
                  ]}
                  text={Localization.t('CommonsFix.NameAndSurname')}
                />
                {save ? (
                  <FASorting
                    onPress={flag =>
                      this.setState(
                        {
                          filterLogic: {
                            date: undefined,
                            nameAndSurname: undefined,
                            earn: flag,
                          },
                        },
                        () => this.filterAction(),
                      )
                    }
                    containerStyle={{
                      backgroundColor: theme?.colors?.vGray,
                    }}
                    value={filterLogic.earn}
                    text={Localization.t('ReferenceScreenFix.Profit')}
                  />
                ) : null}
              </View>
            </View>
            {!_.isEmpty(filteredList) &&
            !_.isNil(filteredList) &&
            !_.isNull(filteredList) ? (
              <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal={false}
                showsVerticalScrollIndicator={false}
                data={filteredList}
                renderItem={({item}) => this.renderItem(item, theme)}
                keyExtractor={(item, index) => index}
                style={styles.FlatListStyle}
              />
            ) : (
              <Text
                style={[
                  styles.noReferenceTextStyle,
                  {color: theme?.colors?.black},
                ]}>
                {Localization.t('ReferenceScreenFix.ThereIsNoReference')}
              </Text>
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
