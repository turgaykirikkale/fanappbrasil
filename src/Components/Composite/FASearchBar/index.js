import React from 'react';
import {View, TextInput} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FASearchBar = props => {
  const {onChangeText, value, placeholder, type} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.mainContainer,
            {
              backgroundColor: type
                ? theme?.colors?.white
                : theme?.colors?.vGray,
              borderTopLeftRadius: type ? 4 : 0,
              borderTopRightRadius: type ? 4 : 0,
              borderBottomWidth: type ? 1 : 0,
              borderBottomColor: theme?.colors?.vGray,
            },
          ]}>
          <View style={{width: 18, height: 18}}>
            <FAIcon iconName={'FASearch'} color={theme?.colors?.darkBlue0} />
          </View>

          <View style={styles.textContainer}>
            <TextInput
              style={{color: theme?.colors?.black}}
              placeholderTextColor={theme?.colors?.darkBlue0}
              onChangeText={newValue => onChangeText && onChangeText(newValue)}
              value={value}
              placeholder={placeholder}
              autoCapitalize={'none'}
            />
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FASearchBar;
