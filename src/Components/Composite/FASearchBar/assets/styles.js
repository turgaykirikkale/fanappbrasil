import {StyleSheet, Platform} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.VGray,
    flexDirection: 'row',
    paddingHorizontal: 16,
    // borderRadius: 25,
    paddingVertical: Platform.OS === 'android' ? 0 : 8,
    alignItems: 'center',
  },
  textContainer: {marginLeft: 8, flex: 1},
});
