import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flexDirection: 'row', marginVertical: 16},
  iconStyle: {width: 14, height: 14, alignSelf: 'center'},
  headerContainerStyle: {marginLeft: 6, alignSelf: 'center'},
  headerTextStyle: {color: FAColor.DarkBlue},
  buttonMainContainer: {alignSelf: 'flex-end', flex: 1, alignItems: 'flex-end'},
  buttonTextStyle: {fontSize: 12, color: FAColor.DarkBlue},
});
