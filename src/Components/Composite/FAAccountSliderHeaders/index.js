import React from 'react';
import {View, Text} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import Localization from '@Localization';

const FAAccountSlidersHeaders = props => {
  const {onPress, leftIconName, headerText} = props;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.iconStyle}>
        <FAIcon iconName={leftIconName} color={FAColor.DarkBlue} />
      </View>
      <View style={styles.headerContainerStyle}>
        <Text style={styles.headerTextStyle}>{`${headerText}`}</Text>
      </View>
      <View style={styles.buttonMainContainer}>
        <FAButton
          onPress={() => onPress && onPress()}
          text={Localization.t('Commons.SeeAll')}
          textStyle={styles.buttonTextStyle}
          rightIconName={'FAAngelRight'}
          rightIconSize={12}
          rightIconColor={FAColor.DarkBlue}
        />
      </View>
    </View>
  );
};

export default FAAccountSlidersHeaders;
