import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';

const FAStaticsHeader = props => {
  const initialLayout = {width: Dimensions.get('window').width}.width;
  const {header, subTitle} = props;
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 10,
          borderBottomWidth: 1,
          borderColor: FAColor.VGray,
        }}>
        <View style={{width: 14, height: 14, marginRight: 4}}>
          <FAIcon iconName={'FABall'} color={FAColor.DarkBlue} />
        </View>
        <Text style={{marginLeft: 4, color: FAColor.DarkBlue}}>{header}</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginVertical: 8,
        }}>
        <Text style={{width: initialLayout / 12, color: FAColor.DarkBlue}}>
          Sıra
        </Text>
        <Text style={{width: initialLayout / 3, textAlign: 'center'}}>
          Oyuncu
        </Text>
        <Text
          style={{
            width: initialLayout / 3,
            textAlign: 'center',
            color: FAColor.DarkBlue,
          }}>
          Takım
        </Text>
        <Text style={{flex: 1, textAlign: 'right'}}>{subTitle}</Text>
      </View>
    </View>
  );
};

export default FAStaticsHeader;
