import React from 'react';
import {
  View,
  Modal,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {styles} from './assets/styles';
import QRCode from 'react-native-qrcode-svg';
import FAIcon from '@Components/UI/FAIcon';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import Localization from '@Localization';

const FAQRViewer = props => {
  const {visible, RefferalProgramCode, RefferalProgramLink, onPressCancel} =
    props;
  const initialLayout = Dimensions.get('window').width;
  return (
    <Modal visible={visible}>
      <View style={styles.mainContainer}>
        <View style={styles.childContainer}>
          <Image
            style={styles.imageContainer}
            source={require('../../../Assets/images/bitci.png')}
          />
          <View style={styles.innerContainer}>
            <View style={styles.flexDirectionRow}>
              <View style={styles.BitciLogoIconContainer}>
                <FAIcon iconName={'BitciLogo'} />
              </View>
              <View style={styles.flex1}>
                <TouchableOpacity
                  style={styles.closeIconContainer}
                  onPress={() => onPressCancel && onPressCancel()}>
                  <FAVectorIcon
                    group={'FontAwesome'}
                    iconName={'times-circle'}
                    size={27}
                    color={'rgba(255,255,255,0.35)'}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <Text style={styles.SignUpNowTextStyle}>
              {Localization.t('FAQRViewer.SignUpNow')}
            </Text>
            <View style={styles.QRAndReferalCodeMainContainer}>
              <View style={styles.QRContainer}>
                <QRCode
                  value={RefferalProgramLink}
                  size={initialLayout / 1.4}
                />
              </View>
              <Text style={styles.ReferansCodeHeaderStyle}>
                {Localization.t('FAQRViewer.ReferenceCode')}
              </Text>
              <Text style={styles.ReferralCodeTextStyle} numberOfLines={1}>
                {RefferalProgramCode}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default FAQRViewer;
