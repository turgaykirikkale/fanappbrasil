import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.Black,
    flex: 1,
    opacity: 0.7,
    justifyContent: 'center',
  },
  childContainer: {
    backgroundColor: '#FE5000', // TO DO COLOR
    marginVertical: 48,
    marginHorizontal: 10,
    borderRadius: 8,
    justifyContent: 'center',
  },
  imageContainer: {
    position: 'absolute',
    zIndex: -1,
    width: 120,
    height: 160,
    alignSelf: 'flex-end',
    resizeMode: 'stretch',
    right: 30,
    top: -5,
  },
  innerContainer: {marginHorizontal: 26, marginVertical: 17},
  flexDirectionRow: {flexDirection: 'row'},
  BitciLogoIconContainer: {width: 110, height: 30},
  flex1: {flex: 1},
  closeIconContainer: {alignSelf: 'flex-end', flex: 1},
  SignUpNowTextStyle: {marginTop: 16, color: FAColor.White},
  QRAndReferalCodeMainContainer: {
    backgroundColor: FAColor.White,
    borderRadius: 8,
    marginTop: 20,
  },
  QRContainer: {
    alignItems: 'center',
    marginTop: 13,
  },
  ReferansCodeHeaderStyle: {
    alignSelf: 'center',
    marginTop: 18,
    fontSize: 16,
    marginBottom: 10,
  },
  ReferralCodeTextStyle: {
    fontSize: 12,
    alignSelf: 'center',
    marginBottom: 16,
    letterSpacing: -0.5,
  },
});
