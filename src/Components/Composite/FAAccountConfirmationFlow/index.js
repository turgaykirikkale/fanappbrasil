import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAAccountConfirmationFlow = props => {
  const {
    firstText,
    secondText,
    thirdText,
    iconName,
    onPressDelete,
    openCamera,
  } = props;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.containerStyle,
            {
              backgroundColor: theme?.colors?.white,
              borderColor: theme?.colors?.darkGray,
              borderWidth: 1,
            },
          ]}>
          <View style={styles.flexDirectionRow}>
            {secondText ? (
              iconName === 'FAMan' ? (
                <View style={styles.leftIconContainer}>
                  <FAIcon iconName={iconName} />
                </View>
              ) : (
                <View style={styles.leftIconContainer}>
                  <FAIcon iconName={iconName} />
                </View>
              )
            ) : null}
            {secondText ? (
              <View style={styles.textsContainer}>
                <Text style={styles.firstTextStyle}>{firstText}</Text>
                <Text style={styles.secondTextStyle}>{secondText}</Text>
              </View>
            ) : (
              <View style={{flex: 1, justifyContent: 'center', marginLeft: 10}}>
                <Text style={styles.firstTextStyle}>{firstText}</Text>
              </View>
            )}

            {props.image ? (
              <View style={styles.imageContainer}>
                <View style={styles.deleteButtonContainer}>
                  <TouchableOpacity
                    style={styles.deleteButtonChildContainer}
                    onPress={() => onPressDelete && onPressDelete()}>
                    <FAIcon iconName="FACross" />
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => openCamera && openCamera()}>
                  <View style={styles.checkIconContainer}>
                    <View style={{width: 24, height: 24}}>
                      <FAIcon iconName="FACheck" color={FAColor.Green} />
                    </View>
                  </View>
                  <Image
                    style={styles.imageChildContainer}
                    source={{
                      isStatic: true,
                      uri: props.image.uri,
                    }}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <TouchableOpacity
                style={styles.rightIconStyle}
                onPress={() => openCamera && openCamera()}>
                <FAIcon iconName="FAUpload" />
              </TouchableOpacity>
            )}
          </View>
          {thirdText ? (
            <Text style={styles.thirdTextStyle}>{thirdText}</Text>
          ) : null}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAAccountConfirmationFlow;
