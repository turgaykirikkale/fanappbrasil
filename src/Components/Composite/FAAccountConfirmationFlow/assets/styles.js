import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: FAColor.White,
    marginTop: 16,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: FAColor.SoftGray,
    paddingHorizontal: 16,
  },
  flexDirectionRow: {
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 18,
  },
  leftIconContainer: {width: 57, height: 50},
  textsContainer: {flex: 1, marginLeft: 12},
  firstTextStyle: {
    // fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: FAColor.DarkGray,
  },
  secondTextStyle: {
    marginTop: 5,
    fontSize: 16,
    // fontFamily: 'Roboto-Medium',
    color: FAColor.Black,
  },
  rightIconStyle: {
    borderRadius: 30,
    width: 60,
    height: 60,
    alignSelf: 'center',
    marginBottom: 8,
    position: 'relative',
  },
  textShowIos: {marginTop: 3},
  imageContainer: {flexDirection: 'row', position: 'relative'},
  deleteButtonContainer: {position: 'absolute', zIndex: 999, left: -3, top: -3},
  deleteButtonChildContainer: {
    width: 18,
    height: 18,
  },
  checkIconContainer: {
    position: 'absolute',
    zIndex: 999,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageChildContainer: {
    borderRadius: 30,
    width: 60,
    height: 60,
    resizeMode: 'cover',
  },
  thirdTextStyle: {
    color: FAColor.DarkGray,
    marginBottom: 16,
  },
});
