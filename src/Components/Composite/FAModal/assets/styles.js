import {White, Black, DarkGray, Green} from '@Commons/FAColor';
import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  modalBackDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.6)',
    justifyContent: 'center',
  },
  mainContainer: {
    backgroundColor: White,
    borderRadius: 4,
    marginHorizontal: 10,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  text: {
    color: '#3B3B3B', //TODO Renk statik verildi. Değiştirilecek
    fontSize: 14,
  },
  buttonContainer: {
    marginTop: 14,
    flexDirection: 'row',
    alignItems: 'center',
  },

  alertModalContainer: {
    backgroundColor: White,
    borderRadius: 4,
    marginHorizontal: 10,
    paddingTop: 24,
    paddingBottom: 17,
    paddingHorizontal: 16,
  },
  alertModalHeadTitle: {
    color: Black,
    fontSize: 16,
  },
  alertModalContent: {
    fontSize: 14,
    color: '#696969', //TODO Renk statik verildi. Değiştirilecek
  },
  alertModalButtonWrapper: {
    marginTop: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  alertModalButtonContainer: {
    flex: 1,
  },

  imageModalMainContainer: {
    paddingBottom: 15,
    backgroundColor: White,
    borderRadius: 4,
    marginHorizontal: 10,
  },
  imageModalTextContainer: {
    marginHorizontal: 15,
    alignItems: 'center',
  },
  imageModalHeadTitle: {
    color: Black,
    fontSize: 16,
  },
  imageModalImageStyle: {
    marginTop: 12,
    marginBottom: 10,
  },
  imageModalContent: {
    textAlign: 'center',
    color: DarkGray,
    fontSize: 14,
  },
  imageModalButtonWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 9,
    marginRight: 16,
  },

  inputModalInputContainer: {
    marginTop: 16,
  },
  inputModalButtonContainer: {
    backgroundColor: Green,
    borderRadius: 4,
    marginTop: 16,
    marginBottom: 8,
    paddingVertical: 15,
  },
  inputModalButtonText: {
    fontSize: 14,
    color: White,
    fontWeight: '500',
  },
  WithSecurityInputModalContainer: {
    backgroundColor: FAColor.White,
    borderRadius: 4,
    marginHorizontal: 10,
    marginVertical: 75,
  },
  qrCodeManuelKeyAndTextContainer: {
    backgroundColor: FAColor.BlueGray,
    paddingHorizontal: 32,
    paddingVertical: 16,
  },
  headerTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
  },
  qrCodeManuelKeyTextStyle: {
    color: FAColor.Black,
    marginRight: 4,
    marginTop: 4,
    fontWeight: '500',
  },
  marginHorizontal10: {marginHorizontal: 10},
  authCodeQrContainer: {marginBottom: 15, backgroundColor: FAColor.BlueGray},
  qrImageContainer: {width: 275, height: 275},
  qrCodeManuelKeyContainer: {flexDirection: 'row', alignItems: 'center'},

  copyQrCodeContainer: {
    width: 22,
    height: 22,
  },
  cancelButtonContainer: {
    backgroundColor: FAColor.Red,
    borderRadius: 4,
    paddingVertical: 15,
    marginBottom: 12,
  },
});
