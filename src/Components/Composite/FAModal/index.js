import FATextInput from '@Components/Composite/FATextInput';
import {
  Image,
  Modal,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import React from 'react';
import FAColor from '@Commons/FAColor';
import FAIcon from '@Components/UI/FAIcon';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const RenderModalAndBackDrop = props => {
  const {visible, forStory, theme} = props;
  if (forStory) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.modalBackDrop}>{props.children}</View>
      </SafeAreaView>
    );
  }
  return (
    <Modal visible={visible} animationType={'none'}>
      <View style={styles.modalBackDrop}>{props.children}</View>
    </Modal>
  );
};

const renderButton = buttonProps => {
  const {onPress, containerStyle, textStyle, text} = buttonProps;
  return (
    <FAButton
      onPress={() => onPress && onPress()}
      containerStyle={containerStyle || null}
      textStyle={textStyle || null}
      text={text}
    />
  );
};

export const FAModal = props => {
  const {visible, text, buttons, forStory} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <RenderModalAndBackDrop visible={visible} forStory={forStory}>
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <Text style={[styles.text, {color: theme?.colors?.darkBlack}]}>
              {text}
            </Text>
            <View style={styles.buttonContainer}>
              {buttons && buttons.length
                ? buttons.map((button, index) => (
                    <View
                      key={index}
                      style={{
                        marginLeft: index !== 0 ? 10 : null,
                        display: index <= 1 ? null : 'none',
                      }}>
                      {renderButton(button)}
                    </View>
                  ))
                : null}
            </View>
          </View>
        </RenderModalAndBackDrop>
      )}
    </ThemeContext.Consumer>
  );
};

FAModal.Alert = props => {
  const {visible, text, buttons, title, forStory} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <RenderModalAndBackDrop visible={visible} forStory={forStory}>
          <View
            style={[
              styles.alertModalContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            {title ? (
              <Text
                style={[
                  styles.alertModalHeadTitle,
                  {color: theme?.colors?.black},
                ]}>
                {title}
              </Text>
            ) : null}
            <Text
              style={[
                styles.alertModalContent,
                {marginTop: title ? 16 : null, color: theme?.colors?.darkGray},
              ]}>
              {text}
            </Text>
            <View style={styles.alertModalButtonWrapper}>
              {buttons && buttons.length
                ? buttons.map((button, index) => (
                    <View
                      key={index}
                      style={[
                        styles.alertModalButtonContainer,
                        {
                          marginLeft: index !== 0 ? 24 : null,
                        },
                      ]}>
                      {renderButton(button)}
                    </View>
                  ))
                : null}
            </View>
          </View>
        </RenderModalAndBackDrop>
      )}
    </ThemeContext.Consumer>
  );
};

FAModal.WithImage = props => {
  const {title, text, image, buttons, visible, forStory} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <RenderModalAndBackDrop visible={visible} forStory={forStory}>
          <View
            style={[
              styles.imageModalMainContainer,
              {paddingTop: !image ? 12 : 26},
            ]}>
            <View style={styles.imageModalTextContainer}>
              <Text style={styles.imageModalHeadTitle}>{title || ''}</Text>
              {image ? (
                <Image source={image} style={styles.imageModalImageStyle} />
              ) : null}
              <Text style={styles.imageModalContent}>{text || ''}</Text>
            </View>
            <View style={styles.imageModalButtonWrapper}>
              {buttons && buttons.length
                ? buttons.map((button, index) => (
                    <View
                      key={index}
                      style={{marginLeft: index !== 0 ? 24 : null}}>
                      {renderButton(button)}
                    </View>
                  ))
                : null}
            </View>
          </View>
        </RenderModalAndBackDrop>
      )}
    </ThemeContext.Consumer>
  );
};

FAModal.WithSecurityInput = props => {
  const {
    smsCode,
    emailCode,
    pinCode,
    googleCode,
    passwordCode,
    onChangeSmsCode,
    onChangeEmailCode,
    onChangePinCode,
    onChangeGoogleCode,
    onChangePasswordCode,
    onPressSendButton,
    forStory,
    visible,
    qrCodeUrl,
    qrCodeManuelKey,
    onPressCancelButton,
    copyQrCodeManuelKey,
  } = props;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <RenderModalAndBackDrop visible={visible} forStory={forStory}>
          <KeyboardAwareScrollView
            showVerticalScroll={false}
            style={[
              styles.WithSecurityInputModalContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            {qrCodeManuelKey ? (
              <View
                style={[
                  styles.qrCodeManuelKeyAndTextContainer,
                  {backgroundColor: theme?.colors?.blueGray},
                ]}>
                <Text
                  style={[
                    styles.headerTextStyle,
                    {color: theme?.colors?.darkBlue},
                  ]}>
                  {Localization.t('FAModal.SetupKey')}
                </Text>
                <View style={styles.qrCodeManuelKeyContainer}>
                  <Text
                    style={[
                      styles.qrCodeManuelKeyTextStyle,
                      {color: theme?.colors?.black},
                    ]}>
                    {qrCodeManuelKey}
                  </Text>
                  <TouchableOpacity
                    onPress={() => copyQrCodeManuelKey && copyQrCodeManuelKey()}
                    style={styles.copyQrCodeContainer}>
                    <View>
                      <FAIcon
                        iconName={'FACopy'}
                        color={theme?.colors?.black}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : null}

            <View style={styles.marginHorizontal10}>
              {smsCode || forStory ? (
                <View style={styles.inputModalInputContainer}>
                  <FATextInput
                    theme={theme}
                    label={Localization.t('FAModal.SMSVerificationCode')}
                    onChangeValue={newValue => onChangeSmsCode(newValue)}
                    value={smsCode}
                    placeholder={Localization.t('FAModal.SMSVerificationCode')}
                  />
                </View>
              ) : null}

              {emailCode || forStory ? (
                <View style={styles.inputModalInputContainer}>
                  <FATextInput
                    theme={theme}
                    label={Localization.t('FAModal.EmailVerificationCode')}
                    onChangeValue={newValue => onChangeEmailCode(newValue)}
                    value={emailCode}
                    placeholder={Localization.t(
                      'FAModal.EmailVerificationCode',
                    )}
                  />
                </View>
              ) : null}

              {googleCode ? (
                <View style={styles.inputModalInputContainer}>
                  <FATextInput
                    theme={theme}
                    label={Localization.t('FAModal.GoogleAuthVerificationCode')}
                    onChangeValue={newValue => onChangeGoogleCode(newValue)}
                    value={googleCode}
                    placeholder={Localization.t(
                      'FAModal.GoogleAuthVerificationCode',
                    )}
                  />
                </View>
              ) : null}

              {pinCode || forStory ? (
                <View style={styles.inputModalInputContainer}>
                  <FATextInput
                    theme={theme}
                    label={Localization.t('FAModal.PinVerificationCode')}
                    onChangeValue={newValue => onChangePinCode(newValue)}
                    value={pinCode}
                    placeholder={Localization.t('FAModal.PinVerificationCode')}
                  />
                </View>
              ) : null}

              {passwordCode || forStory ? (
                <View style={styles.inputModalInputContainer}>
                  <FATextInput
                    theme={theme}
                    label={Localization.t('FAModal.PasswordVerificationCode')}
                    onChangeValue={newValue => onChangePasswordCode(newValue)}
                    value={passwordCode}
                    placeholder={Localization.t(
                      'FAModal.PasswordVerificationCode',
                    )}
                  />
                </View>
              ) : null}

              <FAButton
                onPress={() => onPressSendButton && onPressSendButton()}
                containerStyle={[
                  styles.inputModalButtonContainer,
                  {backgroundColor: theme?.colors?.green},
                ]}
                textStyle={[
                  styles.inputModalButtonText,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('FAModal.Verify')}
              />
              <FAButton
                onPress={() => onPressSendButton && onPressCancelButton()}
                containerStyle={[
                  styles.cancelButtonContainer,
                  {backgroundColor: theme?.colors?.red},
                ]}
                textStyle={[
                  styles.inputModalButtonText,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('FAModal.Cancel')}
              />
            </View>
          </KeyboardAwareScrollView>
        </RenderModalAndBackDrop>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAModal;
