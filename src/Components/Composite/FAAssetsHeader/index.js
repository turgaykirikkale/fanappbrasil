import React from 'react';
import {View, Text} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {DarkBlueO} from '@Commons/FAColor';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {toFixedNoRounding} from '@Commons/FAMath';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAAssetsHeader = props => {
  const {onPressWalletButton, TotalBalanceValueList, buttonText} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.assetHeadContainer,
            {
              backgroundColor: theme?.colors?.white,
              borderBottomColor: theme?.colors?.vGray,
            },
          ]}>
          <View style={styles.assetHeadValueContainer}>
            <Text style={[styles.assetHeadTitle, {color: theme?.colors?.gray}]}>
              {Localization.t('FanScreenFix.ApproximateValue')}
            </Text>
            <View style={styles.assetHeadAmountAndCurrencyCodeContainer}>
              <Text
                style={[
                  styles.assetHeadAmountValue,
                  {color: theme?.colors?.black},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(TotalBalanceValueList?.Amount, 2),
                  2,
                ) || '0.0'}
              </Text>
              <Text
                style={[
                  styles.assetHeadCurrencyCode,
                  {color: theme?.colors?.black},
                ]}>
                {TotalBalanceValueList?.CurrencyCode || ''}
              </Text>
            </View>
          </View>

          <View style={styles.assetHeadWalletButtonContainer}>
            <FAButton
              onPress={() => onPressWalletButton && onPressWalletButton()}
              text={buttonText}
              textStyle={[
                styles.assetHeadWalletButtonText,
                {color: theme?.colors?.darkBlue},
              ]}
              rightIconName={'FAAngelRight'}
              rightIconColor={DarkBlueO}
              rightIconSize={10}
            />
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAAssetsHeader.propTypes = {
  onPressWalletButton: PropTypes.func.isRequired,
  buttonText: PropTypes.string,
};

FAAssetsHeader.defaultProps = {
  buttonText: Localization.t('CommonsFix.Wallet'),
};

export default FAAssetsHeader;
