import {StyleSheet} from 'react-native';
import {Black, DarkBlue, Gray} from '@Commons/FAColor';
export const styles = StyleSheet.create({
  assetHeadContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#F5F6FA',
    backgroundColor: 'white',
  },
  assetHeadValueContainer: {paddingHorizontal: 16},
  assetHeadTitle: {color: Gray, fontSize: 12},
  assetHeadAmountAndCurrencyCodeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 6,
  },
  assetHeadAmountValue: {color: Black, fontSize: 16, fontWeight: '600'},
  assetHeadCurrencyCode: {color: Black, fontSize: 16, marginLeft: 3},
  assetHeadWalletButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  assetHeadWalletButtonText: {
    fontSize: 12,
    color: DarkBlue,
  },
});
