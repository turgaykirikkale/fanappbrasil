import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
    paddingHorizontal: 5,
  },
  imageContainer: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginHorizontal: 14,
  },
  nameTextContainer: {marginLeft: 12, flex: 1},
  iconMainContainer: {
    width: 30,
    height: 30,
    backgroundColor: '#FFFFFF9F',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {width: 20, height: 20},
  votedRatioSliderStyle: {
    position: 'absolute',
    height: '130%',
    marginLeft: 5,
  },
  votedRatioTextStyle: {fontSize: 20},
});
