import React from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import FAColor from '@Commons/FAColor';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAPollSelection = props => {
  const {
    item,
    onPressSelect,
    voted,
    selectedItems,
    selected,
    MultipleSelectionEnabled,
    isPassive,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          disabled={isPassive || voted ? true : false}
          activeOpacity={0.5}
          style={[
            styles.mainContainer,
            {
              backgroundColor: (
                MultipleSelectionEnabled
                  ? selectedItems.includes(item.QuizOptionId)
                  : selected === item.QuizOptionId
              )
                ? voted
                  ? theme?.colors?.white
                  : theme?.colors?.orange
                : theme?.colors?.darkBlue0,
              marginTop: 2,
            },
          ]}
          onPress={() => onPressSelect && onPressSelect(item.QuizOptionId)}>
          {voted || isPassive ? (
            <View
              style={[
                styles.votedRatioSliderStyle,
                {
                  backgroundColor: (
                    MultipleSelectionEnabled
                      ? selectedItems.includes(item.QuizOptionId)
                      : selected === item.QuizOptionId
                  )
                    ? theme?.colors?.orange
                    : theme?.colors?.softGray,
                  width: '100%',
                  borderRadius: 4,
                },
              ]}
            />
          ) : null}

          <Image
            style={styles.imageContainer}
            source={{
              uri: item.ImageUrl,
            }}
          />
          <Text
            style={[
              styles.nameTextContainer,
              {
                color: (
                  MultipleSelectionEnabled
                    ? selectedItems.includes(item.QuizOptionId)
                    : selected === item.QuizOptionId
                )
                  ? theme?.colors?.black
                  : theme?.colors?.black,
              },
            ]}>
            {item.Content}
          </Text>
          {(MultipleSelectionEnabled
            ? selectedItems.includes(item.QuizOptionId)
            : selected === item.QuizOptionId) && !voted ? (
            <View style={styles.iconMainContainer}>
              <View style={styles.iconContainer}>
                <FAIcon iconName="FACheck" color={theme?.colors?.orange} />
              </View>
            </View>
          ) : null}
          {/* {voted ? (
        <View>
          <Text
            style={[
              styles.votedRatioTextStyle,
              {
                color: (
                  MultipleSelectionEnabled
                    ? selectedItems.includes(item.QuizOptionId)
                    : selected === item.QuizOptionId
                )
                  ? '#EA5615'
                  : '#4E4E4E',
              },
            ]}>
            {`%${item.itemVoteRatio}`}
          </Text>
        </View>
      ) : null} */}
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAPollSelection;
