import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import {White, Success} from '@Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const SQUARE_BOX_WIDTH = 18;
const SQUARE_BOX_HEIGHT = SQUARE_BOX_WIDTH;
const CIRCLE_BOX_WIDTH = 18;
const CIRCLE_BOX_HEIGHT = CIRCLE_BOX_WIDTH;

const renderLabel = (label, theme, props) => (
  <Text style={[styles.label, {color: theme?.colors?.darkBlue, fontSize: 12}]}>
    {label}
  </Text>
);

export const FACheckbox = props => {
  const {checked, onPress, label, fillColor, tickColor} = props;
  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => onPress && onPress()}
        style={[
          styles.uncheckedMainBox,
          {
            borderWidth: checked ? null : 1,
            width: SQUARE_BOX_WIDTH,
            height: SQUARE_BOX_HEIGHT,
          },
        ]}>
        {checked ? (
          <>
            <View
              style={[
                styles.checkedFill,
                {backgroundColor: fillColor || Success},
              ]}
            />

            <View style={{width: 9, height: 9}}>
              <FAIcon iconName={'FACheck'} color={tickColor || White} />
            </View>
          </>
        ) : null}
      </TouchableOpacity>
      {label ? renderLabel(label) : null}
    </View>
  );
};

FACheckbox.Circle = props => {
  const {checked, onPress, label, fillColor, tickColor, disabled} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={styles.mainContainer}>
          <TouchableOpacity
            disabled={disabled}
            activeOpacity={0.7}
            onPress={() => onPress && onPress()}
            style={[
              styles.uncheckedMainBox,
              {
                borderWidth: checked ? null : 1,
                width: CIRCLE_BOX_WIDTH,
                height: CIRCLE_BOX_HEIGHT,
                borderRadius: CIRCLE_BOX_WIDTH * 2,
                borderColor: theme?.colors?.softGray,
              },
            ]}>
            {checked ? (
              <>
                <View
                  style={[
                    styles.checkedFill,
                    {
                      borderRadius: CIRCLE_BOX_WIDTH * 2,
                      backgroundColor: fillColor || theme?.colors?.success,
                    },
                  ]}
                />
                <View style={{width: 14, height: 14}}>
                  <FAIcon
                    iconName={'FACheck'}
                    color={tickColor || theme?.colors?.white}
                  />
                </View>
              </>
            ) : null}
          </TouchableOpacity>
          {label ? renderLabel(label, theme) : null}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FACheckbox;
