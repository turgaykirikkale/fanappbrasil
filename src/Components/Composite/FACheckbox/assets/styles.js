import {StyleSheet} from 'react-native';
import {Black, SoftGray} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  uncheckedMainBox: {
    borderColor: SoftGray,
    borderRadius: 2,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkedFill: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    borderRadius: 2,
  },
  label: {
    fontSize: 16,
    color: Black,
    marginLeft: 4,
  },
});
