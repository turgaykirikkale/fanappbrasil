import React from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';

const FALiveMatchStaticItems = props => {
  const {item} = props;

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 8,
          alignItems: 'center',
        }}>
        <Text>{item.homeTeam}</Text>
        <Text style={{color: FAColor.DarkBlue, fontSize: 13}}>
          {item.subject}
        </Text>
        <Text>{item.awayTeam}</Text>
      </View>
    </View>
  );
};

export default FALiveMatchStaticItems;
