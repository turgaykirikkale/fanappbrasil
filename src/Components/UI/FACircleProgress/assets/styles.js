import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  percentTextStyle: {
    fontSize: 12,
  },
});
