import React from 'react';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {Text} from 'react-native';
import {styles} from './assets/styles';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import {Gray, Green, Red, VBlueGray} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const generateColor = (value, theme) => {
  if (value === BUYFLOW) {
    return theme?.colors?.success;
  }
  if (value === SELLFLOW) {
    return theme?.colors?.danger;
  }
};

const FACircleProgress = props => {
  const {percent, type, hasPairCodes} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <AnimatedCircularProgress
          size={80}
          width={10}
          fill={percent}
          tintColor={
            percent === 0
              ? theme?.colors?.vBlueGray
              : generateColor(type, theme)
          }
          backgroundColor={theme?.colors?.vGray}
          rotation={(-360, -360)}>
          {percent => (
            <Text
              style={[
                styles.percentTextStyle,
                {
                  color:
                    percent === 0
                      ? theme?.colors?.gray
                      : generateColor(type, theme),
                },
              ]}>
              %{parseInt(percent, 10)}
            </Text>
          )}
        </AnimatedCircularProgress>
      )}
    </ThemeContext.Consumer>
  );
};

FACircleProgress.propTypes = {
  percent: PropTypes.number.isRequired,
  type: PropTypes.number.isRequired,
  hasPairCodes: PropTypes.bool,
};

export default FACircleProgress;
