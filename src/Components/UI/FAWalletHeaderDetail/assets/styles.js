import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  withdrawScreenContainer: {marginTop: 16, marginHorizontal: 10},
  flexDirectionRow: {flexDirection: 'row'},
  headerItemCodeTextStyle: {fontSize: 16, color: FAColor.Black},
  headerItemNameTextStyle: {
    marginLeft: 4,
    fontSize: 16,
    color: FAColor.DarkGray,
  },
  headerDividerStyle: {
    borderWidth: 2,
    marginVertical: 14,
    borderColor: FAColor.VBlueGray,
  },
});
