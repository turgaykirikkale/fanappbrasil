import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './assets/styles';

const FAWalletHeaderDetail = props => {
  const {data, theme} = props;
  return (
    <View>
      <View style={styles.withdrawScreenContainer}>
        <View style={styles.flexDirectionRow}>
          <Text
            style={[
              styles.headerItemCodeTextStyle,
              {color: theme?.colors?.black},
            ]}>
            {data.Code}
          </Text>
          <Text
            style={[
              styles.headerItemNameTextStyle,
              {color: theme?.colors?.darkGray},
            ]}>
            {data.Name}
          </Text>
        </View>
        {/* <View style={styles.headerItemBalanceContainerStyle}>
            <Text style={styles.headerItemBalanceTextStyle}>
              {data.item.Balance}
            </Text>
            <Text style={styles.headerItemBalanceCodeTextStyle}>≈ ? TRY</Text>
          </View> */}
      </View>
      <View
        style={[styles.headerDividerStyle, {borderColor: theme?.colors?.vGray}]}
      />
    </View>
  );
};

export default FAWalletHeaderDetail;
