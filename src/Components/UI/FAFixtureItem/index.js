import React from 'react';
import {View, Text, Image} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import moment from 'moment';

const FAFixtureItem = props => {
  const {item} = props;
  const scoreControl = item => {
    if (item?.status?.id === 1) {
      let date = moment(item.date, 'DD-MM-YYYY-h:mm').format('hh:mm');
      return date;
    } else if (item?.status?.id === 21) {
      return 'PT';
    } else if (item?.status?.id === 5) {
      return `${item?.homeTeam?.score?.current}-${item?.awayTeam?.score?.current}`;
    }
  };
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            backgroundColor: theme?.colors?.white,
            marginHorizontal: 10,
            marginTop: 2,
            borderRadius: 4,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 6,
              marginHorizontal: 10,
            }}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{
                  width: 15,
                  height: 15,
                  borderRadius: 7.5,
                  marginRight: 2,
                }}
                source={{
                  uri: `https://cdn.broadage.com/images-teams/soccer/256x256/${item.homeTeam.id}.png`,
                }}
              />
              <Text style={{color: theme?.colors?.black}} numberOfLines={1}>
                {item?.homeTeam?.mediumName}
              </Text>
            </View>
            <Text
              style={{
                color: theme?.colors?.black,
                flex: 1,
                textAlign: 'center',
              }}>
              {scoreControl(item)}
            </Text>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{
                  width: 15,
                  height: 15,
                  borderRadius: 7.5,
                  marginRight: 2,
                }}
                source={{
                  uri: `https://cdn.broadage.com/images-teams/soccer/256x256/${item.awayTeam.id}.png`,
                }}
              />
              <Text
                style={{
                  color: theme?.colors?.black,
                  textAlign: 'right',
                }}>
                {item?.awayTeam?.mediumName}
              </Text>
            </View>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAFixtureItem;
