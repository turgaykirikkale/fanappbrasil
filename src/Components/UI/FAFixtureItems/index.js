import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import FAColor from '@Commons/FAColor';

const FAFixtureItems = props => {
  const {item, onPress} = props;
  return (
    <TouchableOpacity
      style={{flexDirection: 'row', paddingBottom: 8}}
      disabled={item.score ? false : true}
      activeOpacity={0.7}
      onPress={() => onPress && onPress()}>
      <Text style={{fontSize: 12, flex: 1}}>{item.team1}</Text>

      <Text
        style={{
          fontSize: 12,
          textAlign: 'center',
          color: item.time ? FAColor.Gray : FAColor.Black,
        }}>
        {item.time ? item.time : item.score}
      </Text>

      <Text style={{fontSize: 12, flex: 1, textAlign: 'right'}}>
        {item.team2}
      </Text>
    </TouchableOpacity>
  );
};

export default FAFixtureItems;
