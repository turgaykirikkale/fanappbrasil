import {StyleSheet} from 'react-native';
import {Black, Gray, DarkBlue, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: White,
  },
  pairAndDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  pairText: {color: Black, fontSize: 12},
  dateText: {color: Gray, fontSize: 10},
  transactionTypeText: {fontSize: 12},
  informationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 4,
  },
  informationLabel: {color: DarkBlue, fontSize: 12},
  informationValue: {color: Black, fontSize: 12},
});
