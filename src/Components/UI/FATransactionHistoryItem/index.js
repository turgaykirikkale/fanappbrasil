import {Green, Red} from '@Commons/FAColor';
// import BMALocalization from '@Localization';
import {Text, View} from 'react-native';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import moment from 'moment';
import React from 'react';
import _ from 'lodash';
import {toFixedNoRounding} from '../../../Commons/FAMath';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import Localization from '@Localization';

const FATransactionHistoryItem = props => {
  const {
    coinCode,
    currencyCode,
    date,
    type,
    coinAmount,
    commission,
    coinPrice,
    totalPrice,
    state,
    coinBalanceDetail,
  } = props;
  let currencyDecimalCount = 8;
  let coinDecimalCount = 8;
  if (!_.isNil(coinBalanceDetail)) {
    const {PriceDecimal, CoinDecimal} = coinBalanceDetail;
    currencyDecimalCount = PriceDecimal || currencyDecimalCount;
    coinDecimalCount = CoinDecimal || coinDecimalCount;
  }
  return (
    <View style={styles.mainContainer}>
      {coinCode && currencyCode ? (
        <View style={styles.pairAndDateContainer}>
          <Text style={styles.pairText}>{`${coinCode} / ${currencyCode}`}</Text>
          {/*TODO Moment implemente edilecek*/}
          <Text style={styles.dateText}>
            {moment(date).format('DD.MM.YYYY HH:mm')}
          </Text>
        </View>
      ) : null}
      <View style={styles.informationContainer}>
        <Text
          style={[
            styles.transactionTypeText,
            {color: type === 1 ? Green : Red},
          ]}>
          {type === 1
            ? Localization.t('TransactionHistory.Buying')
            : Localization.t('TransactionHistory.Selling')}
        </Text>
        <Text
          style={[
            styles.transactionTypeText,
            {color: state === 2 ? Green : Red},
          ]}>
          {state === 2
            ? Localization.t('TransactionHistory.Completed')
            : Localization.t('TransactionHistory.Cancelled')}
        </Text>
      </View>
      {coinPrice ? (
        <View style={styles.informationContainer}>
          <Text style={styles.informationLabel}>
            {Localization.t('TransactionHistory.Price', {
              currencyCode: currencyCode || '',
            })}
          </Text>
          <Text style={styles.informationValue}>
            {FACurrencyAndCoinFormatter(
              toFixedNoRounding(coinPrice, currencyDecimalCount),
              currencyDecimalCount,
            )}
          </Text>
        </View>
      ) : null}
      {coinAmount ? (
        <View style={styles.informationContainer}>
          <Text style={styles.informationLabel}>
            {Localization.t('TransactionHistory.Amount', {
              coinCode: coinCode || '',
            })}
          </Text>
          <Text style={styles.informationValue}>
            {FACurrencyAndCoinFormatter(
              toFixedNoRounding(coinAmount, coinDecimalCount),
              coinDecimalCount,
            )}
          </Text>
        </View>
      ) : null}
      {commission ? (
        <View style={styles.informationContainer}>
          <Text style={styles.informationLabel}>
            {Localization.t('TransactionHistory.Commission', {
              currencyCode: currencyCode || '',
            })}
          </Text>
          <Text style={styles.informationValue}>
            {FACurrencyAndCoinFormatter(
              toFixedNoRounding(commission, currencyDecimalCount),
              currencyDecimalCount,
            )}
          </Text>
        </View>
      ) : null}
      {totalPrice ? (
        <View style={styles.informationContainer}>
          <Text style={styles.informationLabel}>
            {Localization.t('TransactionHistory.TotalPrice', {
              currencyCode: currencyCode || '',
            })}
          </Text>
          <Text style={styles.informationValue}>
            {FACurrencyAndCoinFormatter(
              toFixedNoRounding(totalPrice, currencyDecimalCount),
              currencyDecimalCount,
            )}
          </Text>
        </View>
      ) : null}
    </View>
  );
};

FATransactionHistoryItem.propTypes = {
  coinCode: PropTypes.string.isRequired,
  currencyCode: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
  coinAmount: PropTypes.number.isRequired,
  commission: PropTypes.number.isRequired,
  coinPrice: PropTypes.number.isRequired,
  totalPrice: PropTypes.number.isRequired,
  state: PropTypes.number.isRequired,
};

export default FATransactionHistoryItem;
