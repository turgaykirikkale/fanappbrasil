import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import {toFixedNoRounding} from '@Commons/FAFormatter/StatusFormatter';
import FAIcon from '@Components/UI/FAIcon';
import FAVectorIcon from '@Components/UI/FAVectorIcon';

const generateColor = (value, theme) => {
  let castedValue = parseFloat(value);
  if (castedValue === 0) {
    return theme?.colors?.gray;
  }
  if (castedValue > 0) {
    return theme?.colors?.green;
  }
  if (castedValue < 0) {
    return theme?.colors?.red;
  }
  return theme?.colors?.gray;
};

const generateIcon = value => {
  try {
    let castedValue = parseFloat(value);
    if (castedValue === 0) {
      return 'angle-right';
    }
    if (castedValue > 0) {
      return 'angle-up';
    }
    if (castedValue < 0) {
      return 'angle-down';
    }
    return null;
  } catch (e) {
    return null;
  }
};

const FAPercentStatus = props => {
  const {value, theme} = props;
  const castedValue = toFixedNoRounding(value, 2);
  const backgroundColor = generateColor(castedValue, theme);
  const iconName = generateIcon(castedValue);
  return (
    <View style={[styles.container, {backgroundColor: backgroundColor}]}>
      <View style={styles.iconAndPercentValueContainer}>
        {iconName ? (
          <FAVectorIcon
            iconName={iconName}
            color={theme?.colors.white2}
            size={12}
          />
        ) : null}
        <Text
          style={[
            styles.change24HTextStyle,
            {color: theme?.colors?.white2},
          ]}>{`${toFixedNoRounding(value, 2)} %`}</Text>
      </View>
    </View>
  );
};

export default FAPercentStatus;
