import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor/index';

export const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 7,
    minWidth: 80,
  },
  iconAndPercentValueContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 8,
  },
  change24HTextStyle: {
    fontSize: 12,
    color: FAColor.White,
    // marginLeft: 2,
    letterSpacing: -1,
    flex: 1,
    textAlign: 'center',
  },
});
