import React from 'react';
import {SvgXml} from 'react-native-svg';
import * as Icons from './assets/Icons';
import * as Flags from './assets/Flags';
import PropTypes from 'prop-types';
import {Black} from '@Commons/FAColor';

const FAIcon = props => {
  const {iconName, color, size} = props;
  const inComingIcon = Icons[iconName] || Flags[iconName];
  if (inComingIcon) {
    return (
      <SvgXml
        xml={inComingIcon}
        width={size || '100%'}
        height={size || '100%'}
        color={color}
      />
    );
  }
  return null;
};

FAIcon.propTypes = {
  iconName: PropTypes.string.isRequired,
  color: PropTypes.string,
  size: PropTypes.number,
};

FAIcon.defaultProps = {
  color: Black,
};

export default FAIcon;
