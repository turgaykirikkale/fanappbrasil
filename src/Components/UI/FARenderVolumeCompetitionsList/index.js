import React from 'react';
import {View, FlatList, Text, Image} from 'react-native';
import FAVolumeCompetitionListItems from '../FAVolumeCompetitionListItems';

const FARenderVolumeCompetitionsList = props => {
  const {data, onPress} = props;

  return (
    <FlatList
      nestedScrollEnabled
      data={data}
      numColumns={2}
      keyExtractor={(item, index) => index}
      showsHorizontalScrollIndicator={false}
      ItemSeparatorComponent={() => <View style={{marginLeft: 5}} />}
      renderItem={({item, index}) => {
        return (
          <>
            <FAVolumeCompetitionListItems
              type={1}
              item={item}
              onPress={inComigItem => onPress && onPress(inComigItem)}
            />
          </>
        );
      }}
    />
  );
};

export default FARenderVolumeCompetitionsList;
