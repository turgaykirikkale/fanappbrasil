import React from 'react';
import {TouchableOpacity, Dimensions, Text} from 'react-native';
import FAColor from '@Commons/FAColor';

const FAStandings = props => {
  const initialLayout = {width: Dimensions.get('window').width}.width;
  const {item, onPress} = props;
  return (
    <TouchableOpacity
      style={{flexDirection: 'row', marginBottom: 10}}
      onPress={() => onPress && onPress()}
      activeOpacity={0.6}>
      <Text style={{width: initialLayout / 3, color: FAColor.SoftBlack}}>
        {`${item.rank} ${item.team}`}
      </Text>
      <Text style={{width: initialLayout / 9, textAlign: 'right'}}>
        {item.OM}
      </Text>
      <Text style={{width: initialLayout / 9, textAlign: 'right'}}>
        {item.G}
      </Text>
      <Text style={{width: initialLayout / 9, textAlign: 'right'}}>
        {item.B}
      </Text>
      <Text style={{width: initialLayout / 9, textAlign: 'right'}}>
        {item.M}
      </Text>
      <Text style={{width: initialLayout / 9, textAlign: 'right'}}>
        {item.A}
      </Text>
      <Text style={{flex: 1, textAlign: 'right'}}>{item.P}</Text>
    </TouchableOpacity>
  );
};

export default FAStandings;
