import React from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

const FAStakeCustomerInfos = props => {
  const {
    interestStakeDay,
    startDate,
    finishDate,
    takeBackDate,
    estimatedStake,
    totalValue,
  } = props;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.StakeDate')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {startDate}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.StakingPeriod')}
            </Text>
            <Text
              style={{
                fontWeight: '400',
                color: theme?.colors?.black,
              }}>{`${interestStakeDay} Gün`}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.EndDate')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {finishDate}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.StakingPeriod')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`1  ${Localization.t('StakeScreen.day')}`}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
            {Localization.t('StakeScreen.returnBackDate')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {takeBackDate}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
            {Localization.t('StakeScreen.EstimatedStaking')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`${estimatedStake} BITCI`}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 20,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
            {Localization.t('StakeScreen.GeneralTotal')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`${totalValue} BITCI`}
            </Text>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAStakeCustomerInfos;
