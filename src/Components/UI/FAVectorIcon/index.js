import React from 'react';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import {Black} from '../../../Commons/FAColor';

const FAVectorIcon = props => {
  const {group, size, iconName, color} = props;
  switch (group) {
    case 'FontAwesome':
      return (
        <FontAwesomeIcon
          name={iconName || 'rocket'}
          size={size}
          color={color}
        />
      );

    default:
      return null;
  }
};

FAVectorIcon.propTypes = {
  group: PropTypes.oneOf(['FontAwesome']),
  size: PropTypes.number,
  iconName: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

FAVectorIcon.defaultProps = {
  group: 'FontAwesome',
  size: 14,
  color: Black,
};

export default FAVectorIcon;
