import React from 'react';
import {View, ActivityIndicator, Modal, Text} from 'react-native';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import {FAOrange} from '@Commons/FAColor';

const FAFullScreenLoader = props => {
  const {isLoading, color, text} = props;
  return (
    <Modal
      style={styles.modalContainer}
      animationType="none"
      transparent={true}
      visible={isLoading}>
      <View style={styles.indicatorContainer}>
        <ActivityIndicator size="large" color={color} />
        {text && (
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 16,
              marginTop: 18,
            }}>
            {text}
          </Text>
        )}
      </View>
    </Modal>
  );
};

FAFullScreenLoader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  color: PropTypes.string,
  text: PropTypes.string,
};

FAFullScreenLoader.defaultProps = {
  color: FAOrange,
  text: null,
  isLoading: false,
};

export default FAFullScreenLoader;
