import {StyleSheet} from 'react-native';
import * as FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: FAColor.Black,
  },

  indicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, .8)',
  },
});
