import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import FAColor from '@Commons/FAColor';

const FAStandingsHeader = props => {
  const initialLayout = {width: Dimensions.get('window').width}.width;

  return (
    <View
      style={{
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: FAColor.VBlueGray,
        marginVertical: 10,
        paddingBottom: 10,
      }}>
      <Text style={{width: initialLayout / 3, color: FAColor.FAOrange}}>
        TEAM
      </Text>
      <Text
        style={{
          width: initialLayout / 9,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        OM
      </Text>
      <Text
        style={{
          width: initialLayout / 9,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        G
      </Text>
      <Text
        style={{
          width: initialLayout / 9,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        B
      </Text>
      <Text
        style={{
          width: initialLayout / 9,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        M
      </Text>
      <Text
        style={{
          width: initialLayout / 9,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        A
      </Text>
      <Text
        style={{
          flex: 1,
          color: FAColor.FAOrange,
          textAlign: 'right',
        }}>
        P
      </Text>
    </View>
  );
};
export default FAStandingsHeader;
