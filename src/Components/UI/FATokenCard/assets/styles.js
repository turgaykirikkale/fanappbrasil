import {Dimensions, StyleSheet} from 'react-native';
import {Black, DarkGray, White} from '@Commons/FAColor';

const boldText = {
  fontSize: 16,
  fontWeight: 'bold',
  color: Black,
};
export const styles = StyleSheet.create({
  mainContainer: {
    paddingVertical: 16,
    paddingHorizontal: 10,
    backgroundColor: White,
    alignItems: 'center',
    borderRadius: 4,
    flex: 1,
  },
  tokenLogo: {width: 50, height: 50, alignSelf: 'center'},
  tokenCode: {...boldText, marginTop: 16, textAlign: 'center'},
  tokenName: {fontSize: 12, color: DarkGray, marginTop: 4, textAlign: 'center'},
  valueAndCurrencyCode: {...boldText, marginTop: 8},
  state: {fontSize: 12, marginTop: 8},
});
