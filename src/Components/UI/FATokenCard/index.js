import React from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';
import {Red, Success} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import {AppConstants} from '@Commons/Contants';
import {sentenceShortener} from '@Helpers/StringHelper';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FATokenCard = props => {
  const {tokenCode, tokenName, value, currencyCode, isCompleted, onPress} =
    props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => onPress && onPress()}
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <Image
            style={styles.tokenLogo}
            source={{
              uri: `https://borsa.bitci.com/img/coin/${tokenCode}.png?v=1`,
              headers: {
                'User-Agent': AppConstants.UserAgent,
              },
            }}
          />
          <Text style={[styles.tokenCode, {color: theme?.colors?.black}]}>
            {tokenCode}
          </Text>
          {tokenName && (
            <Text style={styles.tokenName}>
              {sentenceShortener(tokenName, 20)}
            </Text>
          )}
          {value && currencyCode && (
            <Text
              style={[
                ,
                {color: theme?.colors?.black},
              ]}>{`${value} ${currencyCode}`}</Text>
          )}
          <Text
            style={[
              styles.state,
              {
                color: isCompleted
                  ? theme?.colors?.red
                  : theme?.colors?.success,
              },
            ]}>
            {isCompleted ? 'Tamamlandı' : 'Satışa Açıldı'}
          </Text>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FATokenCard.propTypes = {
  tokenCode: PropTypes.string.isRequired,
  tokenName: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  currencyCode: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  isCompleted: PropTypes.bool,
};

FATokenCard.defaultProps = {
  currencyCode: AppConstants.CurrencyCode,
  isCompleted: false,
};

export default FATokenCard;
