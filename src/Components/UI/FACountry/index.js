import React from 'react';
import {View, Text} from 'react-native';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FACountry = props => {
  const {countryCode, iconName, CountryName, disabled} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={[styles.mainContainer]}>
          <View style={[styles.iconContainer]}>
            <FAIcon iconName={iconName} />
          </View>
          <View style={[styles.codeAndCountryNameContainer]}>
            {countryCode ? (
              <Text style={[styles.textStyles, {color: theme?.colors?.black}]}>
                {countryCode}
              </Text>
            ) : null}
            <Text
              style={[
                styles.fontStyles,
                {color: disabled ? theme?.colors?.gray : theme?.colors?.black},
              ]}>
              {CountryName}
            </Text>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FACountry;
