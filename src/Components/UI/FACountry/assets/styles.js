import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {height: 24, width: 24},
  codeAndCountryNameContainer: {
    marginLeft: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyles: {
    marginRight: 4,
    fontSize: 14,
    color: FAColor.Black,
  },
  fontStyles: {fontSize: 14, color: FAColor.Black},
});
