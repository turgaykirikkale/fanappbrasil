import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {toFixedNoRounding} from '@Commons/FAMath';
import {AppConstants} from '@Commons/Contants';
import {Green, Red} from '@Commons/FAColor';
import {BUYFLOW} from '@Commons/FAEnums';
import {Text, View} from 'react-native';
import {styles} from './assets/styles';
import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';

const FAMarketHistoryItem = props => {
  const {createdDate, coinValue, price, type} = props;
  const currencyDecimalCount = AppConstants.CurrencyDecimalCount;
  const coinDecimalCount = AppConstants.CoinDecimalCount;
  return (
    <View style={styles.marketHistoryMainContainer}>
      <View style={styles.marketHistoryDateContainer}>
        <Text style={styles.marketHistoryDateText}>
          {moment(createdDate).format('DD.MM.YYYY')}
        </Text>
        <Text style={styles.marketHistoryHourText}>
          {moment(createdDate).format('HH:mm')}
        </Text>
      </View>
      <Text style={styles.marketHistoryCoinValue}>
        {FACurrencyAndCoinFormatter(
          toFixedNoRounding(coinValue || '', coinDecimalCount),
          coinDecimalCount,
        )}
      </Text>
      <Text
        style={[
          styles.marketHistoryCoinPrice,
          {color: type === BUYFLOW ? Green : Red},
        ]}>
        {FACurrencyAndCoinFormatter(
          toFixedNoRounding(price || '', currencyDecimalCount),
          currencyDecimalCount,
        )}
      </Text>
    </View>
  );
};

FAMarketHistoryItem.propTypes = {
  createdDate: PropTypes.string.isRequired,
  coinValue: PropTypes.any.isRequired,
  price: PropTypes.any.isRequired,
  type: PropTypes.number.isRequired,
};

export default FAMarketHistoryItem;
