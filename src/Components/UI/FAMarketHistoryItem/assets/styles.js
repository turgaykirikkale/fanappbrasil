import {DarkGray} from '@Commons/FAColor';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  marketHistoryMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 12,
  },
  marketHistoryDateContainer: {flex: 1},
  marketHistoryDateText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryHourText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryCoinValue: {
    fontSize: 12,
    color: DarkGray,
    flex: 1,
    textAlign: 'right',
    marginLeft: 12,
  },
  marketHistoryCoinPrice: {
    fontSize: 12,
    flex: 1,
    textAlign: 'right',
    marginRight: 10,
  },
});
