import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {flexDirection: 'row'},
  borderContainer: {flex: 1, justifyContent: 'center'},
  borderStyle: {borderBottomWidth: 0.5, borderColor: '#CBCBCB'},
  textStyle: {fontSize: 8, marginLeft: 10, marginRight: 10, color: '#CBCBCB'},
});
