import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './assets/styles';

const FADivider = props => {
  const {text} = props;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.borderContainer}>
        <View style={styles.borderStyle} />
      </View>
      <Text style={styles.textStyle}>{text}</Text>
      <View style={styles.borderContainer}>
        <View style={styles.borderStyle} />
      </View>
    </View>
  );
};

export default FADivider;
