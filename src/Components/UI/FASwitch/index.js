import React from 'react';
import {Text, View, TouchableOpacity, Animated} from 'react-native';
import {Gray, Success, White} from '@Commons/FAColor';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

export default class FASwitch extends React.Component {
  static defaultProps = {
    isOn: false,
    size: 'medium',
    labelStyle: {},
    disabled: false,
    animationSpeed: 300,
  };
  calculateDimensions = size => {
    switch (size) {
      case 'small':
        return {
          width: 40,
          padding: 10,
          circleWidth: 15,
          circleHeight: 15,
          translateX: 22,
        };
      case 'large':
        return {
          width: 70,
          padding: 20,
          circleWidth: 30,
          circleHeight: 30,
          translateX: 38,
        };
      default:
        return {
          width: 52,
          padding: 14,
          circleWidth: 22,
          circleHeight: 22,
          translateX: 30,
        };
    }
  };

  offsetX = new Animated.Value(0);
  dimensions = this.calculateDimensions(this.props.size);

  createToggleSwitchStyle = theme => {
    const {isOn, onBgColor, offBgColor, disabled} = this.props;
    return [
      {
        justifyContent: 'center',
        width: this.dimensions.width,
        borderRadius: 20,
        padding: this.dimensions.padding,
        backgroundColor: isOn
          ? onBgColor || theme?.colors?.success
          : offBgColor || theme?.colors?.white,
        borderWidth: isOn ? 1 : 1,
        borderColor: isOn
          ? onBgColor || theme?.colors?.success
          : Gray || theme?.colors?.gray,
        opacity: disabled ? 0.45 : 1,
      },
    ];
  };

  createInsideCircleStyle = theme => {
    const {isOn, onCircleColor, offCircleColor} = this.props;
    return [
      {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 4,
        position: 'absolute',
        backgroundColor: isOn
          ? onCircleColor || theme?.colors?.white
          : offCircleColor || theme?.colors?.gray,
        transform: [{translateX: this.offsetX}],
        width: this.dimensions.circleWidth,
        height: this.dimensions.circleHeight,
        borderRadius: this.dimensions.circleWidth / 2,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2.5,
        elevation: 1.5,
      },
    ];
  };

  render() {
    const {
      animationSpeed,
      isOn,
      onToggle,
      disabled,
      labelStyle,
      rightLabel,
      leftLabel,
    } = this.props;

    let toValue;
    if (isOn) {
      toValue = this.dimensions.width - this.dimensions.translateX;
    } else {
      toValue = -1;
    }
    Animated.timing(this.offsetX, {
      toValue,
      duration: animationSpeed,
      useNativeDriver: true,
    }).start();

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={styles.container} {...this.props}>
            {leftLabel ? (
              <Text
                style={[
                  styles.labelStyle,
                  {color: theme?.colors?.black},
                  labelStyle,
                ]}>
                {leftLabel}
              </Text>
            ) : null}
            <TouchableOpacity
              disabled={disabled}
              style={this.createToggleSwitchStyle(theme)}
              activeOpacity={disabled ? 1 : 0.8}
              onPress={() => (disabled ? null : onToggle && onToggle(!isOn))}>
              <Animated.View style={this.createInsideCircleStyle(theme)} />
            </TouchableOpacity>
            {rightLabel ? (
              <Text
                style={[
                  styles.labelStyle,
                  {color: theme?.colors?.black},
                  labelStyle,
                ]}>
                {rightLabel}
              </Text>
            ) : null}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
