import {View, Text, TouchableOpacity} from 'react-native';
import {toFixedNoRounding} from '@Commons/FAMath';
import {styles} from './assets/styles';
import React from 'react';
import Localization from '@Localization';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';

const FAProgressItemValues = props => {
  const {
    occuredAmount,
    totalAmount,
    priceLeft,
    priceRight,
    date,
    price,
    currencyDecimalCount,
    coinDecimalCount,
    onPressCancel,
    currencyCode,
    coinCode,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <View>
            {currencyCode && coinCode && (
              <Text
                style={[
                  styles.fieldContainer,
                  styles.amountTextStyle,
                  {color: theme?.colors?.black},
                ]}>
                {coinCode}/{currencyCode}
              </Text>
            )}
            <View style={styles.fieldContainer}>
              <Text
                style={[
                  styles.amountTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAProgressItem.Amount')}
              </Text>
              <Text
                style={[
                  styles.occuredAmountTextStyle,
                  {color: theme?.colors?.black},
                ]}>
                {toFixedNoRounding(occuredAmount, coinDecimalCount)}
              </Text>
              <Text
                style={[
                  styles.totalAmountTextStyle,
                  {color: theme?.colors?.darkGray},
                ]}>{` / ${toFixedNoRounding(
                totalAmount,
                coinDecimalCount,
              )}`}</Text>
            </View>
            <View style={styles.fieldContainer}>
              <Text
                style={[
                  styles.priceTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAProgressItem.TotalPrice')}
              </Text>
              <Text
                style={[styles.priceLefStyle, {color: theme?.colors?.black}]}>
                {toFixedNoRounding(priceLeft, currencyDecimalCount)}
              </Text>
              <Text
                style={[
                  styles.priceRightStyle,
                  {color: theme?.colors?.darkGray},
                ]}>{` / ${toFixedNoRounding(
                priceRight,
                currencyDecimalCount,
              )}`}</Text>
            </View>
            {price ? (
              <View style={styles.priceWrapper}>
                <Text
                  style={[styles.priceLabel, {color: theme?.colors?.darkBlue}]}>
                  {Localization.t('FAProgressItem.Price')}
                </Text>
                <Text
                  style={[
                    styles.priceValue,
                    {color: theme?.colors?.darkBlack},
                  ]}>
                  {toFixedNoRounding(price, currencyDecimalCount)}
                </Text>
              </View>
            ) : null}
            <Text
              style={[styles.dateTextStyle, {color: theme?.colors?.darkGray}]}>
              {date}
            </Text>
          </View>
          <TouchableOpacity
            hitSlop={{left: 20}}
            style={styles.buttonContainer}
            onPress={() => onPressCancel && onPressCancel()}>
            <Text
              style={[
                styles.buttonTextStyle,
                {color: theme?.colors?.faOrange},
              ]}>
              {Localization.t('FAProgressItem.Cancel')}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAProgressItemValues;
