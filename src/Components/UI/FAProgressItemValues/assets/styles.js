import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.White,
    flexDirection: 'row',
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'space-between',
  },
  fieldContainer: {
    flexDirection: 'row',
    marginTop: 3,
  },
  amountTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    marginRight: 6,
  },
  occuredAmountTextStyle: {
    fontSize: 12,
    color: FAColor.Black,
  },
  totalAmountTextStyle: {
    fontSize: 12,
    color: FAColor.DarkGray,
  },
  priceWrapper: {
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  priceTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    marginRight: 5,
  },
  priceLabel: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    marginRight: 5,
  },
  priceValue: {
    fontSize: 12,
    color: FAColor.Black,
  },
  priceLefStyle: {
    fontSize: 12,

    color: FAColor.Black,
  },
  priceRightStyle: {
    fontSize: 12,

    color: FAColor.DarkGray,
  },
  dateTextStyle: {
    fontSize: 12,

    color: FAColor.Gray,
    marginTop: 6,
  },
  buttonContainer: {},
  buttonTextStyle: {
    fontSize: 12,

    color: FAColor.FAOrange,
  },
});
