import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {Black, VBlueGray} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAScrollingFilterList = props => {
  const {
    data,
    selected,
    passiveBgColor,
    activeBgColor,
    passiveTextColor,
    activeTextColor,
    onSelect,
  } = props;

  if (_.isEmpty(data)) {
    return null;
  }

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          horizontal
          data={data}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() => onSelect && onSelect(item.value)}
                activeOpacity={0.7}
                style={[
                  styles.buttonContainer,
                  {
                    backgroundColor:
                      selected === item.value
                        ? theme?.colors?.vGray
                        : passiveBgColor,
                  },
                ]}>
                <Text
                  style={[
                    styles.filterValue,
                    {
                      color: theme?.colors?.black,
                    },
                  ]}>
                  {item.label}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
      )}
    </ThemeContext.Consumer>
  );
};

FAScrollingFilterList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    }),
  ).isRequired,
  selected: PropTypes.any.isRequired,
  onSelect: PropTypes.func.isRequired,
  passiveBgColor: PropTypes.string,
  activeBgColor: PropTypes.string,
  passiveTextColor: PropTypes.string,
  activeTextColor: PropTypes.string,
};

FAScrollingFilterList.defaultProps = {
  passiveBgColor: 'transparent',
  passiveTextColor: Black,
  activeBgColor: VBlueGray,
  activeTextColor: Black,
};

export default FAScrollingFilterList;
