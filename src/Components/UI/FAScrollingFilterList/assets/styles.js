import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  separator: {marginLeft: 8},
  buttonContainer: {
    paddingVertical: 6,
    paddingHorizontal: 12,
  },
  filterValue: {
    fontSize: 12,
  },
});
