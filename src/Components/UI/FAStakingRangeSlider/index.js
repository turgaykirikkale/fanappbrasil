import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Green, Orange, Black, Gray} from '@Commons/FAColor';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

const colorExecutor = (currentPercent, limit) => {
  return currentPercent >= limit ? Orange : '#F7F7F7';
};

const fontColorExecutor = (currentPercent, limit, theme) => {
  return currentPercent >= limit ? theme?.colors?.black : Gray;
};

const FAStakingRangeSlider = props => {
  const {currentPercent, onSelectPercent, LockedListDetail} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={[styles.mainContainer]}>
          <TouchableOpacity
            onPress={() =>
              onSelectPercent && onSelectPercent(1, LockedListDetail[0])
            }
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(currentPercent, 1),
                  borderTopLeftRadius: 6,
                  borderBottomLeftRadius: 6,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {color: fontColorExecutor(currentPercent, 1, theme)},
              ]}>
              {`30 ${Localization.t('StakeScreen.days')}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              onSelectPercent && onSelectPercent(2, LockedListDetail[1])
            }
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(currentPercent, 2),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 2 ? 0 : 0,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 2 ? 0 : 0,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 2, theme),
                },
              ]}>
              {`60 ${Localization.t('StakeScreen.days')}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              onSelectPercent && onSelectPercent(3, LockedListDetail[2])
            }
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(currentPercent, 3),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 3 ? 0 : 0,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 3 ? 0 : 0,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 3, theme),
                },
              ]}>
              {`90 ${Localization.t('StakeScreen.days')}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              onSelectPercent && onSelectPercent(4, LockedListDetail[3])
            }
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(currentPercent, 4),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 4 ? 6 : 6,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 4 ? 6 : 6,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 4, theme),
                },
              ]}>
              {`180 ${Localization.t('StakeScreen.days')}`}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAStakingRangeSlider.propTypes = {
  currentPercent: PropTypes.number.isRequired,
  onSelectPercent: PropTypes.func.isRequired,
};

FAStakingRangeSlider.defaultProps = {
  flowType: 1,
};

export default FAStakingRangeSlider;
