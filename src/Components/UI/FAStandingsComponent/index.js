import React, {useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import _ from 'lodash';
import FAButton from '../../Composite/FAButton';
import Localization from '@Localization';

const FAStandingsComponent = props => {
  const {data, type} = props;
  const [buttonID, setButtonID] = useState(0);
  const [dataType, setDataType] = useState(0);

  const buttonData = [
    {
      label: Localization.t('TeamScreen.General'),
      id: 0,
    },
    {
      label: Localization.t('TeamScreen.Home'),
      id: 1,
    },
    {
      label: Localization.t('TeamScreen.Away'),
      id: 2,
    },
  ];
  const controlButton = value => {
    if (value === 0) {
      setButtonID(0);
      setDataType(0);
    } else if (value === 1) {
      setButtonID(1);
      setDataType(1);
    } else {
      setButtonID(2);
      setDataType(2);
    }
  };

  const positionStatusControl = (value, positionStatusId) => {
    if (value < 5 || positionStatusId === 5) {
      return '#27A844';
    } else if (value > 4 && value < 7 && positionStatusId === 16) {
      return '#9AE6AB';
    } else if (value > 6 && value < 13 && positionStatusId === 17) {
      return '#EA5608';
    } else if (value > 12 && value < 17) {
      return null;
    } else {
      return '#767E8D';
    }
  };

  let filteredData = [];

  if (dataType === 0) {
    filteredData =
      type === 0
        ? data[0]?.standings?.overall.slice(0, 5)
        : data[0]?.standings?.overall;
  } else if (dataType === 1) {
    filteredData =
      type === 0
        ? data[0]?.standings?.overall.slice(0, 5)
        : data[0]?.standings?.home;
  } else {
    filteredData =
      type === 0
        ? data[0]?.standings?.overall.slice(0, 5)
        : data[0]?.standings?.away;
  }

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1}}>
          <View>
            {type === 0 ? null : (
              <ScrollView horizontal style={{marginTop: 10}}>
                {_.map(buttonData, item => {
                  return (
                    <FAButton
                      onPress={() => controlButton(item.id)}
                      text={item.label}
                      textStyle={{
                        color:
                          buttonID === item.id
                            ? theme?.colors?.orange
                            : theme?.colors?.black,
                        marginHorizontal: 10,
                        letterSpacing: 0.7,
                        marginVertical: 3,
                        fontWeight: '500',
                      }}
                      containerStyle={{
                        backgroundColor:
                          buttonID === item.id
                            ? theme?.colors?.gray
                            : theme?.colors?.softGray,
                        marginLeft: 10,
                        borderRadius: 4,
                      }}
                    />
                  );
                })}
              </ScrollView>
            )}
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: type === 0 ? 5 : 10,
              borderBottomWidth: 1,
              borderColor: theme?.colors?.softGray,
              paddingBottom: 2,
              paddingHorizontal: 10,
            }}>
            <View style={{flex: 1}}>
              <Text style={{color: theme?.colors?.orange}}>
                {Localization.t('TeamScreen.Team')}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                flex: 1,
              }}>
              <Text style={{color: theme?.colors?.orange, textAlign: 'right'}}>
                P
              </Text>
              <Text style={{color: theme?.colors?.orange, textAlign: 'right'}}>
                W
              </Text>
              <Text style={{color: theme?.colors?.orange, textAlign: 'right'}}>
                D
              </Text>
              <Text style={{color: theme?.colors?.orange, textAlign: 'right'}}>
                L
              </Text>
              <Text style={{color: theme?.colors?.orange, textAlign: 'right'}}>
                Pt
              </Text>
            </View>
          </View>

          <ScrollView style={{flex: 1}}>
            {_.map(filteredData, item => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginVertical: 5,
                    paddingHorizontal: 10,
                  }}>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View
                      style={{
                        width: 4,
                        backgroundColor:
                          dataType !== 0
                            ? null
                            : positionStatusControl(
                                item.position,
                                item?.positionStatus?.id,
                              ),
                      }}
                    />
                    <Text style={{color: theme?.colors?.black, marginLeft: 6}}>
                      {item.position}
                    </Text>
                    <Text
                      style={{
                        color: theme?.colors?.black,
                        flex: 1,
                        textAlign: 'left',
                        marginLeft: 8,
                      }}>
                      {item.team.name}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      justifyContent: 'space-between',
                    }}>
                    <Text style={{color: theme?.colors?.black}}>
                      {item.played}
                    </Text>
                    <Text style={{color: theme?.colors?.black}}>
                      {item.won}
                    </Text>
                    <Text style={{color: theme?.colors?.black}}>
                      {item.draw}
                    </Text>
                    <Text style={{color: theme?.colors?.black}}>
                      {item.lost}
                    </Text>
                    <Text style={{color: theme?.colors?.black}}>
                      {item.points}
                    </Text>
                  </View>
                </View>
              );
            })}
            {type === 0 ? null : (
              <>
                <View style={{marginVertical: 20}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginHorizontal: 10,
                    }}>
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{backgroundColor: '#27A844', width: 4}} />
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          {Localization.t('TeamScreen.LibertadoreCup')}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <View style={{backgroundColor: '#9AE6AB', width: 4}} />
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          {Localization.t(
                            'TeamScreen.LibertadoresCupQualifying',
                          )}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{backgroundColor: '#EA5608', width: 4}} />
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          {Localization.t('TeamScreen.SouthAmericaCup')}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <View style={{backgroundColor: '#767E8D', width: 4}} />
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          {Localization.t('TeamScreen.LowerLeague')}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{marginBottom: 20}}>
                  <View
                    style={{
                      flexDirection: 'row',

                      marginHorizontal: 10,
                    }}>
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          P: {Localization.t('TeamScreen.Played')}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          W: {Localization.t('TeamScreen.Won')}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          D: {Localization.t('TeamScreen.Draw')}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginTop: 5,
                          marginLeft: 10,
                        }}>
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          L: {Localization.t('TeamScreen.Lost')}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <Text
                          style={{color: theme?.colors?.black, marginLeft: 6}}>
                          Pt: {Localization.t('TeamScreen.Point')}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </>
            )}
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAStandingsComponent;
