import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Green, Red, Black, Gray} from '@Commons/FAColor';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const colorExecutor = (currentPercent, limit, type, theme) => {
  return currentPercent >= limit
    ? type === 1
      ? theme?.colors?.green
      : type === 2
      ? theme?.colors?.red
      : Green
    : theme?.colors?.vGray;
};

const fontColorExecutor = (currentPercent, limit, theme) => {
  return currentPercent >= limit ? theme?.colors?.black : theme?.colors?.gray;
};

const FARangeSlider = props => {
  const {currentPercent, onSelectPercent, flowType, days} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={styles.mainContainer}>
          <TouchableOpacity
            onPress={() => onSelectPercent && onSelectPercent(0.25)}
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(
                    currentPercent,
                    0.25,
                    flowType,
                    theme,
                  ),
                  borderRadius: currentPercent === 0.25 ? 6 : null,
                  borderTopLeftRadius: currentPercent !== 0.25 ? 6 : null,
                  borderBottomLeftRadius: currentPercent !== 0.25 ? 6 : null,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 0.25, theme),
                },
              ]}>
              {days ? 30 : `%${25}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onSelectPercent && onSelectPercent(0.5)}
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(
                    currentPercent,
                    0.5,
                    flowType,
                    theme,
                  ),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 0.5 ? 6 : 0,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 0.5 ? 6 : 0,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 0.5, theme),
                },
              ]}>
              {days ? 60 : `%${50}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onSelectPercent && onSelectPercent(0.75)}
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(
                    currentPercent,
                    0.75,
                    flowType,
                    theme,
                  ),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 0.75 ? 6 : 0,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 0.75 ? 6 : 0,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 0.75, theme),
                },
              ]}>
              {days ? 90 : `%${75}`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onSelectPercent && onSelectPercent(1)}
            activeOpacity={0.8}
            style={styles.flex1}>
            <View
              style={[
                styles.percentBar,
                {
                  backgroundColor: colorExecutor(
                    currentPercent,
                    1,
                    flowType,
                    theme,
                  ),
                  borderTopRightRadius:
                    currentPercent && currentPercent === 1 ? 6 : 6,
                  borderBottomRightRadius:
                    currentPercent && currentPercent === 1 ? 6 : 6,
                  borderWidth: 0,
                },
              ]}
            />
            <Text
              style={[
                styles.percentText,
                {
                  color: fontColorExecutor(currentPercent, 1, theme),
                },
              ]}>
              {days ? 180 : `%${100}`}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FARangeSlider.propTypes = {
  currentPercent: PropTypes.number.isRequired,
  onSelectPercent: PropTypes.func.isRequired,
  flowType: PropTypes.number,
};

FARangeSlider.defaultProps = {
  flowType: 1,
};

export default FARangeSlider;
