import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  flex1: {flex: 1},
  mainContainer: {flexDirection: 'row', alignItems: 'center'},
  percentBar: {paddingVertical: 5},
  percentText: {
    fontSize: 12,
    marginTop: 5,
    textAlign: 'center',
  },
});
