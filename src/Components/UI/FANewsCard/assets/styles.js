import {StyleSheet} from 'react-native';
import {White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    position: 'relative',
    borderRadius: 4,
  },
  image: {borderRadius: 4},
  textContainer: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    position: 'absolute',
    bottom: 0,
  },
  teamNameAndIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 4,
  },
  teamName: {
    color: White,
    fontSize: 12,
    fontWeight: '600',
    marginLeft: 6,
  },
  iconContainer: {width: 14, height: 14, marginRight: 6},
  horizontalLine: {
    marginBottom: 3,
    height: 1,
    backgroundColor: 'rgba(255,255,255, 0.5)',
    marginHorizontal: 6,
  },
  contentText: {
    color: White,
    fontSize: 12,
    paddingHorizontal: 6,
    paddingVertical: 6,
    lineHeight: 16,
  },
});
