import {Image, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FAIcon from '@Components/UI/FAIcon';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import React from 'react';

const FANewsCard = props => {
  const {
    cardWidth,
    cardHeight,
    image,
    imageInLocal,
    teamName,
    isVideoContent,
    content,
    onPress,
  } = props;
  const generatedImage = imageInLocal ? image : {uri: image};
  return (
    <TouchableOpacity
      onPress={() => onPress && onPress()}
      activeOpacity={onPress ? 0.8 : 1}
      style={[
        styles.mainContainer,
        {
          height: cardHeight,
          width: cardWidth,
        },
      ]}>
      <Image
        style={[styles.image, {height: cardHeight}]}
        source={generatedImage}
      />
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 0.5}}
        colors={['rgba(0,0,0, .1)', 'rgba(0,0,0, .5)']}
        style={styles.textContainer}>
        {teamName && (
          <>
            <View style={styles.teamNameAndIconContainer}>
              <Text style={styles.teamName}>{teamName}</Text>
              <View style={styles.iconContainer}>
                <FAIcon
                  iconName={isVideoContent ? 'FAVideo' : 'FAPages'}
                  color={'white'}
                />
              </View>
            </View>
            <View style={styles.horizontalLine} />
          </>
        )}
        <Text
          numberOfLines={2}
          style={[styles.contentText, {width: cardWidth}]}>
          {content || 'Lorem ipsum dolor sit amet, consetetur'}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

FANewsCard.propTypes = {
  image: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  imageInLocal: PropTypes.bool,
  teamName: PropTypes.string,
  isVideoContent: PropTypes.bool,
  cardWidth: PropTypes.number,
  cardHeight: PropTypes.number,
};

FANewsCard.defaultProps = {
  cardWidth: 120,
  cardHeight: 180,
};

export default FANewsCard;
