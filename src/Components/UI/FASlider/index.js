import {White} from '@Commons/FAColor';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import Swiper from 'react-native-swiper';
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {styles} from './assets/styles';
import FAButton from '@Components/Composite/FAButton';
import LinearGradient from 'react-native-linear-gradient';
import {sentenceShortener} from '../../../Helpers/StringHelper';

const FASlider = props => {
  const {data, imageInLocal, height} = props;
  if (_.isEmpty(data)) {
    return null;
  }
  return (
    <Swiper
      height={Dimensions.get('window').width / 2}
      autoplay
      activeDotColor={White}
      dotColor={'rgba(255,255,255,0.2)'}
      paginationStyle={styles.pagination}
      style={styles.wrapper}>
      {data.map((item, index) => {
        const imagePath = item.ContentUrl || item.UrlPath || item.image;
        const generatedImage = imageInLocal ? imagePath : {uri: imagePath};
        return (
          <View key={index} style={styles.slide}>
            <Image source={generatedImage} style={styles.image} />
            {/*<LinearGradient*/}
            {/*  start={{x: 0, y: 0}}*/}
            {/*  end={{x: 0, y: 0.5}}*/}
            {/*  colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.7)']}*/}
            {/*  style={styles.titleAndSubTitleContainer}>*/}
            {/*  {item.Title && (*/}
            {/*    <Text style={styles.title}>*/}
            {/*      {sentenceShortener(item.Title, 30)}*/}
            {/*    </Text>*/}
            {/*  )}*/}
            {/*  {item.SubTitle && (*/}
            {/*    <Text style={styles.subTitle}>{item.SubTitle}</Text>*/}
            {/*  )}*/}
            {/*</LinearGradient>*/}
          </View>
        );
      })}
    </Swiper>
  );
};

FASlider.Alternative = props => {
  const {
    data,
    imageInLocal,
    height,
    buttonContainerStyle,
    buttonTextStyle,
    textStyle,
    buttonText,
    text,
    onPress,
  } = props;
  if (_.isEmpty(data)) {
    return null;
  }
  return (
    <Swiper
      showsPagination={false}
      height={Dimensions.get('window').width / 2}
      autoplay
      loop
      style={styles.wrapper}>
      {data.map((item, index) => {
        const generatedImage = imageInLocal
          ? item.ImageUrl
          : {uri: item.ImageUrl} || {uri: item.image};
        return (
          <View key={index} style={styles.slide}>
            <Image style={styles.image} source={generatedImage} />
            {text || buttonText ? (
              <View style={styles.textAndButtonContainer}>
                {text || (
                  <Text style={[{...textStyle}, styles.textStyle]}>
                    {text ? text : item.Title}
                  </Text>
                )}
                {buttonText && (
                  <View style={styles.buttonContainer}>
                    <FAButton
                      containerStyle={buttonContainerStyle}
                      textStyle={[{...buttonTextStyle}, styles.buttonText]}
                      text={buttonText}
                      onPress={() => onPress && onPress(item)}
                    />
                  </View>
                )}
              </View>
            ) : null}
          </View>
        );
      })}
    </Swiper>
  );
};

FASlider.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      UrlPath: PropTypes.string.isRequired,
      Title: PropTypes.string,
      SubTitle: PropTypes.string,
    }),
  ).isRequired,
  imageInLocal: PropTypes.bool,
  height: PropTypes.number,
};

FASlider.defaultProps = {
  height: 300,
};

export default FASlider;
