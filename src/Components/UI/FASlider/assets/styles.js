import {StyleSheet} from 'react-native';
import {White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  wrapper: {},
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  pagination: {
    position: 'absolute',
    bottom: 10,
  },
  image: {
    width: '100%',
    flex: 1,
    borderRadius: 5,
  },
  titleAndSubTitleContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    paddingBottom: 45,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  title: {fontSize: 20, fontWeight: 'bold', color: White},
  subTitle: {fontSize: 16, color: White},
  textAndButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 12,
    marginVertical: 10,
  },
  textStyle: {flex: 1, fontSize: 12},
  buttonContainer: {marginLeft: 8},
  buttonText: {
    color: White,
    fontSize: 12,
  },
});
