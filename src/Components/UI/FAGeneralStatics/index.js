import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import FAColor from '@Commons/FAColor';
const FAGeneralStatics = props => {
  const initialLayout = {width: Dimensions.get('window').width}.width;
  const {item} = props;

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          marginVertical: 10,
          alignItems: 'center',
        }}>
        <Text style={{width: initialLayout / 12, color: FAColor.DarkBlue}}>
          {item.rank}
        </Text>
        <Text style={{width: initialLayout / 3, textAlign: 'center'}}>
          {item.name}
        </Text>
        <Text
          style={{
            width: initialLayout / 3,
            textAlign: 'center',
            color: FAColor.DarkBlue,
          }}>
          {item.team}
        </Text>
        <Text style={{flex: 1, textAlign: 'right'}}>{item.goal}</Text>
      </View>
    </View>
  );
};

export default FAGeneralStatics;
