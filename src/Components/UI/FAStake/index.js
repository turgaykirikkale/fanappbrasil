import React from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
import FAStakingRangeSlider from '@Components/UI/FAStakingRangeSlider';
import {toFixedNoRounding} from '@Commons/FAMath';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FAButton from '@Components/Composite/FAButton';
import Localization from '@Localization';

const FAStake = props => {
  const {
    details,
    onPress,
    interestStakeRate,
    minAmount,
    currentPercent,
    onSelectPercent,
    type,
    LockedListDetail,
  } = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <>
          <TouchableOpacity
            disabled={details ? true : false}
            onPress={() => onPress && onPress()}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 20,
                marginHorizontal: 10,
              }}>
              <Image
                width={30}
                height={30}
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                }}
                source={{
                  uri: `https://borsa.bitci.com/img/coin/BITCI.png`,
                }}
              />
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 20,
                  marginLeft: 12,
                  color: theme?.colors?.black,
                }}>
                BITCI
              </Text>
              {details ? (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                  }}>
                  <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                    {Localization.t('StakeScreen.RateOfReturn')}
                  </Text>
                  <Text
                    style={{
                      fontSize: 18,
                      color: FAColor.Orange,
                      fontWeight: 'bold',
                      marginTop: 4,
                    }}>
                    {`+% ${toFixedNoRounding(interestStakeRate, 2)}`}
                  </Text>
                </View>
              ) : (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                  }}>
                  <FAButton
                    onPress={() => onPress && onPress()}
                    containerStyle={{
                      marginRight: 5,
                      borderRadius: 4,
                    }}
                    textStyle={{
                      color: theme?.colors?.faOrange,
                      fontWeight: '600',
                    }}
                    text= {Localization.t('StakeScreen.StakeOut')}
                  />
                </View>
              )}
            </View>
            {details ? null : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 10,
                  marginTop: 20,
                  marginBottom: 10,
                }}>
                <View>
                  <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                  {Localization.t('StakeScreen.RateOfReturn')}
                  </Text>
                  <Text
                    style={{
                      fontSize: 18,
                      color: FAColor.Orange,
                      fontWeight: 'bold',
                      marginTop: 4,
                    }}>
                    {`+% ${toFixedNoRounding(interestStakeRate, 2)}`}
                  </Text>
                </View>
                <View>
                  <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                    {Localization.t('StakeScreen.minLockedBitci')}
                  </Text>
                  <Text
                    style={{
                      fontSize: 18,
                      color: FAColor.Orange,
                      fontWeight: 'bold',
                      marginTop: 4,
                      textAlign: 'right',
                    }}>
                    {`${minAmount} BITCI`}
                  </Text>
                </View>
              </View>
            )}
          </TouchableOpacity>
          <View style={{marginHorizontal: 10}}>
            {type === 0 ? (
              <>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    fontSize: 12,
                    marginVertical: 10,
                  }}>
                  {Localization.t('StakeScreen.StakingTime')}
                </Text>
                <FAStakingRangeSlider
                  LockedListDetail={LockedListDetail}
                  onSelectPercent={(percentValue, LockedListDetail) =>
                    onSelectPercent &&
                    onSelectPercent(percentValue, LockedListDetail)
                  }
                  currentPercent={currentPercent}
                />
              </>
            ) : null}
          </View>
        </>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAStake;
