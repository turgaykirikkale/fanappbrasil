import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor/index';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.White,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 14,
  },
  imageStyle: {
    width: 32,
    height: 32,
    borderRadius: 16,
    marginRight: 8,
  },
  nameTextStyle: {
    fontSize: 16,
    color: FAColor.Black,
    letterSpacing: -0.6,
  },
  longNameStyle: {
    color: FAColor.Gray,
    fontSize: 14,
    letterSpacing: -0.6,
  },
});
