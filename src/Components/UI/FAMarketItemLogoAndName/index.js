import React from 'react';
import {View, Text, Image} from 'react-native';
import {styles} from './assets/styles';
import {Gray} from '@Commons/FAColor';
import {AppConstants} from '@Commons/Contants';

const FAMarketItemLogoAndName = props => {
  const {imageName, name, longName, currencyCode, imageUri, theme} = props;
  const generatedImage =
    imageUri || `https://bitci.com/page/assets/coin?code=${imageName}`;
  return (
    <View
      style={[styles.mainContainer, {backgroundColor: theme?.colors?.white}]}>
      <Image
        style={styles.imageStyle}
        source={{
          uri: generatedImage,
          headers: {
            'User-Agent': AppConstants.UserAgent,
          },
        }}
      />

      <View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[styles.nameTextStyle, {color: theme?.colors?.black}]}>
            {name}
          </Text>
          {name && currencyCode ? (
            <>
              <Text
                style={{
                  fontSize: 12,
                  marginLeft: 3,
                  color: theme?.colors?.gray,
                }}>
                /
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: theme?.colors?.gray,
                  marginLeft: 3,
                }}>
                {currencyCode}
              </Text>
            </>
          ) : null}
        </View>
        <Text style={[styles.longNameStyle, {color: theme?.colors?.gray}]}>
          {longName}
        </Text>
      </View>
    </View>
  );
};

export default FAMarketItemLogoAndName;
