import React from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
const FATeamLogos = props => {
  const {header} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        paddingBottom: 10,
        borderColor: FAColor.SoftGray,
      }}>
      <View
        style={{
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'black',
        }}></View>

      <Text style={{color: FAColor.DarkBlue}}>{header}</Text>
      <View
        style={{
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'yellow',
        }}></View>
    </View>
  );
};

export default FATeamLogos;
