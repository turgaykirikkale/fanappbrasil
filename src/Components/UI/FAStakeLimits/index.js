import React from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
import {toFixedNoRounding} from '@Commons/FAMath';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

const FAStakeLimits = props => {
  const {interestStakeData, balance} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('WalletScreenDetail.AvailableBalance')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`${toFixedNoRounding(balance, 5)} BITCI` || 0.0}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.minumumStakeAmount')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`${toFixedNoRounding(interestStakeData?.MinAmount, 2)} BITCI` ||
                0.0}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 6,
            }}>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {Localization.t('StakeScreen.maximumStakeAmount')}
            </Text>
            <Text style={{fontWeight: '400', color: theme?.colors?.black}}>
              {`${toFixedNoRounding(interestStakeData?.MaxAmount, 2)} BITCI`}
            </Text>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FAStakeLimits;
