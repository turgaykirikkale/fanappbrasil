import {StyleSheet} from 'react-native';
import {Gray, Black} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flexDirection: 'row', alignItems: 'center', marginTop: 6},
  textStyle: {flex: 1, color: Gray, fontSize: 14},
  valueStyle: {marginRight: 2, fontSize: 14, color: Black},
  coinNameStyle: {fontSize: 14, color: Black},
});
