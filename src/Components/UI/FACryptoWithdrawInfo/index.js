import React from 'react';
import {View, Text} from 'react-native';

import {styles} from './assets/styles';

const FACryptoWithdrawInfo = props => {
  const {text, value, coinName, theme} = props;
  return (
    <View style={styles.mainContainer}>
      <Text style={[styles.textStyle, {color: theme?.colors?.gray}]}>
        {text}
      </Text>
      <Text style={[styles.valueStyle, {color: theme?.colors?.black}]}>
        {value}
      </Text>
      <Text style={[styles.coinNameStyle, {color: theme?.colors?.black}]}>
        {coinName}
      </Text>
    </View>
  );
};

export default FACryptoWithdrawInfo;
