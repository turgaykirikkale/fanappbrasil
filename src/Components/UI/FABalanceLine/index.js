import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {styles} from './assets/styles';
import React from 'react';
import {Orange} from '@Commons/FAColor';
import PropTypes from 'prop-types';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FABalanceLine = props => {
  const {balance, currency, column, onPress, isLoading} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          activeOpacity={onPress ? 0.7 : 1}
          onPress={() => onPress && onPress(balance)}
          style={[
            styles.mainContainer,
            {
              flexDirection: column ? 'column' : 'row',
              justifyContent: column ? null : 'space-between',
              alignItems: column ? null : 'center',
            },
          ]}>
          {isLoading ? (
            <View style={styles.loaderContainer}>
              <ActivityIndicator size={'small'} color={theme?.colors?.orange} />
            </View>
          ) : (
            <>
              <Text
                style={[
                  styles.availableText,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('MarketScreenFix.Available')}
              </Text>
              <View
                style={[
                  styles.balanceAndCurrencyContainer,
                  {marginTop: column ? 3 : null},
                ]}>
                <Text
                  style={[styles.balanceValue, {color: theme?.colors?.black}]}>
                  {balance}
                </Text>
                <Text
                  style={[styles.currencyValue, {color: theme?.colors?.black}]}>
                  {currency}
                </Text>
                <View style={styles.iconContainer}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={16}
                    height={16}
                    viewBox="0 0 18 18"
                    {...props}>
                    <Path d="M0 0h18v18H0z" fill="none" />
                    <Path
                      d="M9.75 7.5h3.75L9 12 4.5 7.5h3.75V2.25h1.5zM3 14.25h12V9h1.5v6a.75.75 0 01-.75.75H2.25A.75.75 0 011.5 15V9H3z"
                      fill={theme?.colors?.orange}
                    />
                  </Svg>
                </View>
              </View>
            </>
          )}
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FABalanceLine.propTypes = {
  balance: PropTypes.any.isRequired,
  currency: PropTypes.string.isRequired,
  column: PropTypes.bool,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
};

FABalanceLine.defaultProps = {
  column: false,
};

export default FABalanceLine;
