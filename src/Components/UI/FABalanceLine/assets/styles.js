import {Black, DarkBlue} from '@Commons/FAColor';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  availableText: {
    color: DarkBlue,
    fontSize: 14,
  },
  balanceAndCurrencyContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  balanceValue: {
    color: Black,
    fontSize: 14,
  },
  currencyValue: {
    marginLeft: 2,
    color: Black,
    fontSize: 14,
    fontWeight: 'bold',
  },
  iconContainer: {
    marginLeft: 3,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 16,
    alignItems: 'center',
  },
});
