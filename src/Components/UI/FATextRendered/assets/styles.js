import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  rightTextStyle: {
    marginLeft: 11,
    fontSize: 12,
    color: '#696969', //TO DO COLOR
    flex: 1,
    textAlign: 'left',
    letterSpacing: -0.9,
  },
});
