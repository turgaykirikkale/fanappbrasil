import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const applyBoldStyle = (data, props, theme) => {
  const {onPressBoldSentences, stake} = props;
  let numberOfItemsAdded = 0;
  const result = data.text.split(/\{\d+\}/);
  data.boldText.forEach((boldText, i) => {
    result.splice(
      ++numberOfItemsAdded + i,
      0,
      <Text
        onPress={() => onPressBoldSentences && onPressBoldSentences[i]()}
        style={[
          styles.rightTextStyle,
          {
            fontWeight: 'bold',
            color: theme?.colors?.faOrange,
          },
        ]}>
        {boldText}
      </Text>,
    );
  });
  return <Text>{result}</Text>;
};
const FATextRendered = props => {
  const {rightText, boldSentences, checked} = props;
  let renderedText = rightText;
  return (
    <ThemeContext.Consumer>
      {({theme}) => {
        if (boldSentences && boldSentences.length && boldSentences.length > 0) {
          const data = {
            text: rightText,
            boldText: boldSentences,
          };
          renderedText = applyBoldStyle(data, props, theme);
        }
        return (
          <Text
            style={{
              marginLeft: 11,
              fontSize: 12,
              color: checked ? '#696969' : 'red',
              flex: 1,
              marginTop: 2,
              textAlign: 'left',
              letterSpacing: -0.9,
            }}>
            {renderedText}
          </Text>
        );
      }}
    </ThemeContext.Consumer>
  );
};

export default FATextRendered;
