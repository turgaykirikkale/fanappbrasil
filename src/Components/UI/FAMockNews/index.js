import React from 'react';
import {View, Text} from 'react-native';

const FAMockNews = props => {
  return (
    <View>
      <Text
        style={{
          color: '#323338',
          marginVertical: 1,
        }}>{`new}`}</Text>
    </View>
  );
};
export default FAMockNews;
