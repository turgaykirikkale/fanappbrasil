import {StyleSheet} from 'react-native';
import {Black} from '../../../../Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  textContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    paddingVertical: 12,
    paddingHorizontal: 33,
    justifyContent: 'center',
  },
  textStyle: {textAlign: 'center', fontSize: 14, color: Black},
  priceContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 20,
  },
  priceText: {
    transform: [{rotate: '-90deg'}],
    color: 'white',
    fontSize: 9,
    textAlign: 'center',
  },
});
