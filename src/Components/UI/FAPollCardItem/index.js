import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {AppConstants} from '@Commons/Contants';
import {EventPriceType} from '@Commons/Enums/EventPriceType';
import {Black, Success} from '@Commons/FAColor';
import {styles} from './assets/styles';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const CARD_SIZE = 95;
const FAPollCardItem = props => {
  const {image, text, price, type, onPress, imageInLocal, coinId, coinList} =
    props;

  let CoinCode;
  if (coinId && coinList) {
    coinList.map(item => {
      if (item.AssetId === coinId) {
        CoinCode = item.Code;
      }
    });
  }
  const generatedImage = image ? (imageInLocal ? image : {uri: image}) : null;
  const incomingPrice = String(price || '');
  const generatedPrice = incomingPrice
    ? incomingPrice + ` ${CoinCode}`
    : Localization.t('CommonsFix.Free');

  const isEventFree = !price ? EventPriceType.Free : EventPriceType.Paid;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          onPress={() => onPress && onPress()}
          activeOpacity={0.85}
          style={styles.mainContainer}>
          {generatedImage ? (
            <Image
              source={generatedImage}
              width={CARD_SIZE}
              height={CARD_SIZE}
              style={{width: CARD_SIZE, height: CARD_SIZE}}
            />
          ) : null}
          <View
            style={[
              styles.textContainer,
              {
                height: CARD_SIZE,
                backgroundColor: theme?.colors?.white,
              },
            ]}>
            <Text
              numberOfLines={5}
              style={[styles.textStyle, {color: theme?.colors?.black}]}>
              {text || ''}
            </Text>
          </View>
          <View
            style={[
              styles.priceContainer,
              {
                backgroundColor: isEventFree
                  ? theme?.colors?.success
                  : theme?.colors?.black,
                height: CARD_SIZE,
              },
            ]}>
            <Text
              style={[
                styles.priceText,
                {
                  width: CARD_SIZE,
                  color: isEventFree
                    ? theme?.colors?.black
                    : theme?.colors?.white,
                },
              ]}>
              {generatedPrice}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAPollCardItem.propTypes = {
  image: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  price: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  imageInLocal: PropTypes.bool,
};

FAPollCardItem.defaultProps = {
  type: EventPriceType.Free,
};

export default FAPollCardItem;
