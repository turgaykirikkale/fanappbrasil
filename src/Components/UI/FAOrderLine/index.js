import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {toFixedNoRounding} from '@Commons/FAMath';
import FAEnums from '@Commons/FAEnums';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {Green, Red} from '@Commons/FAColor';
import {AppConstants} from '../../../Commons/Contants';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const contentRender = props => {
  const {
    reverse,
    price,
    amount,
    flowType,
    size,
    decimalCurrencyCount,
    decimalCoinCount,
    theme,
  } = props;
  const castedCount = FACurrencyAndCoinFormatter(
    toFixedNoRounding(amount, decimalCoinCount),
    decimalCoinCount,
  );
  const castedPrice = FACurrencyAndCoinFormatter(
    toFixedNoRounding(price, decimalCurrencyCount),
    decimalCurrencyCount,
  );

  if (reverse) {
    return (
      <>
        <Text
          style={[
            styles.textLeftSection,
            {
              fontSize: size === FAEnums.SMALL ? 10 : 12,
              color: theme?.colors?.black,
            },
          ]}>
          {castedPrice}
        </Text>
        <Text
          style={[
            styles.textRightSection,
            {
              color:
                flowType === FAEnums.BUYFLOW
                  ? theme?.colors?.binaceGreen
                  : theme?.colors?.binanceRed,
              fontSize: size === FAEnums.SMALL ? 10 : 12,
            },
          ]}>
          {castedCount}
        </Text>
      </>
    );
  }
  return (
    <>
      <Text
        style={[
          styles.textLeftSection,
          {
            color:
              flowType === FAEnums.BUYFLOW
                ? theme?.colors?.green
                : theme?.colors?.red,
            fontSize: size === FAEnums.SMALL ? 10 : 12,
          },
        ]}>
        {castedCount}
      </Text>
      <Text
        style={[
          styles.textRightSection,
          {
            fontSize: size === FAEnums.SMALL ? 10 : 12,
            color: theme?.colors?.darkBlack,
          },
        ]}>
        {castedPrice}
      </Text>
    </>
  );
};

const FAOrderLine = props => {
  const {
    price,
    amount,
    linePercent,
    flowType,
    reverse,
    size,
    onPress,
    volumeBarDirection,
  } = props;
  let generatedVolumeBarDirection = reverse
    ? {
        left: null,
        right: 0,
      }
    : {
        left: 0,
        right: null,
      };
  if (volumeBarDirection) {
    generatedVolumeBarDirection = {
      left: volumeBarDirection === 'left' ? 0 : null,
      right: volumeBarDirection === 'right' ? 0 : null,
    };
  }

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <TouchableOpacity
          style={styles.mainContainer}
          onPress={() => onPress && onPress(price)}>
          <View
            style={[
              styles.orderLineContainer,
              {
                ...generatedVolumeBarDirection,
                width: linePercent ? `${linePercent}%` : null,
                backgroundColor:
                  flowType === FAEnums.BUYFLOW
                    ? theme?.colors.transparentGreen
                    : theme?.colors.transparenRed,
              },
            ]}
          />
          {contentRender({...props, theme})}
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );
};

FAOrderLine.propTypes = {
  price: PropTypes.any.isRequired,
  amount: PropTypes.number.isRequired,
  linePercent: PropTypes.number.isRequired,
  flowType: PropTypes.number.isRequired,
  reverse: PropTypes.bool,
  size: PropTypes.number,
  onPress: PropTypes.func,
  volumeBarDirection: PropTypes.oneOf(['left', 'right']),
  decimalCurrencyCount: PropTypes.number,
  decimalCoinCount: PropTypes.number,
  theme: PropTypes.object.isRequired,
};

FAOrderLine.defaultProps = {
  decimalCurrencyCount: AppConstants.CurrencyDecimalCount,
  decimalCoinCount: AppConstants.CoinDecimalCount,
  size: FAEnums.SMALL,
};

export default FAOrderLine;
