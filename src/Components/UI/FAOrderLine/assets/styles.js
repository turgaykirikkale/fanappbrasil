import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  orderLineContainer: {
    height: '100%',
    opacity: 0.2,
    position: 'absolute',
    zIndex: -1,
  },

  textLeftSection: {
    alignSelf: 'flex-start',
    paddingLeft: 4,
    marginTop: 6,
    marginBottom: 6,
    letterSpacing: -0.4,
    color: '#696969',
  },

  textRightSection: {
    letterSpacing: -0.4,
    justifyContent: 'flex-end',
    position: 'absolute',
    right: 4,
    marginTop: 6,
    marginBottom: 6,
    color: '#696969',
  },
});
