import React from 'react';
import {View, Text} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import _ from 'lodash';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
const FARenderUsersCompetitionRanks = props => {
  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={{
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              backgroundColor: theme?.colors?.white,
              marginTop: 2,
              marginBottom: 10,
            }}>
            {_.map(props.data, item => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderBottomWidth: 0.5,
                    paddingVertical: 5,
                    borderColor: theme?.colors?.gray,
                  }}>
                  <Text
                    style={{
                      flex: 0.6,
                      color: theme?.colors?.black,
                      fontSize: 14,
                    }}>
                    {item.Rank}
                  </Text>
                  <Text
                    style={{
                      flex: 1,
                      color: theme?.colors?.black,
                      fontSize: 14,
                      textAlign: 'right',
                    }}>
                    {item.Mail}
                  </Text>
                  <Text
                    style={{
                      color: theme?.colors?.black,
                      fontSize: 14,
                      flex: 1,
                      textAlign: 'right',
                    }}>
                    {FACurrencyAndCoinFormatter(item.Volume, 2)}
                  </Text>
                </View>
              );
            })}
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default FARenderUsersCompetitionRanks;
