import React from 'react';
import {View, Text, Image, Modal} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import FAColor from '@Commons/FAColor';
import moment from 'moment';
import FAButton from '@Components/Composite/FAButton';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAStopStakeModal = props => {
  const {visible, item, onPressStakeQuite, onPressCancel} = props;
  return (
    <Modal
      animationType="none"
      transparent={true}
      visible={visible}
      style={{zIndex: 100}}>
      <BlurView
        style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}
        blurType="dark"
        blurAmount={3}
        reducedTransparencyFallbackColor="white"
      />
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}>
            <View
              style={{
                paddingVertical: 24,
                paddingHorizontal: 12,
                alignContent: 'center',
                justifyContent: 'center',
                borderRadius: 4,
                backgroundColor: theme?.colors?.white,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 10,
                  marginHorizontal: 10,
                }}>
                <Image
                  width={30}
                  height={30}
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: 15,
                  }}
                  source={{
                    uri: `https://borsa.bitci.com/img/coin/BITCI.png`,
                  }}
                />
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 20,
                    marginLeft: 12,
                    color: theme?.colors?.black,
                  }}>
                  {item.CoinCode || null}
                </Text>

                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                  }}>
                  <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                    Yıllık Getiri Oranı
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: theme?.colors?.faOrange,
                      fontWeight: 'bold',
                      marginTop: 4,
                    }}>
                    {`+% ${item.InterestRate}` || null}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                  marginTop: 20,
                }}>
                <Text style={{color: theme?.colors?.black}}>
                  Başlangıç Tarihi
                </Text>
                <Text style={{color: theme?.colors?.black}}>
                  {moment(item.StartDate).format('DD.MM.YYYY HH:mm')}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                }}>
                <Text style={{color: theme?.colors?.black}}>
                  Kilitli Miktar
                </Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                  }}>{`${item.Amount} ${item.CoinCode}`}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                }}>
                <Text style={{color: theme?.colors?.black}}>Mevcut Stake</Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                  }}>{`${item.Gain} ${item.CoinCode}`}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 10,
                }}>
                <FAButton
                  onPress={() => onPressCancel && onPressCancel()}
                  containerStyle={{
                    borderWidth: 1,
                    borderColor: FAColor.SoftGray,
                    borderRadius: 4,
                    marginRight: 5,
                    flex: 1,
                  }}
                  textStyle={{color: theme?.colors?.black, marginVertical: 8}}
                  text={'Vazgeç'}
                />
                <FAButton
                  onPress={() => onPressStakeQuite && onPressStakeQuite(item)}
                  disabled={item.InterestStakeStatusEnumId === 2 ? true : false}
                  containerStyle={{
                    borderRadius: 4,
                    backgroundColor:
                      item.InterestStakeStatusEnumId === 2
                        ? theme?.colors?.darkGray
                        : theme?.colors?.red,
                    marginLeft: 5,
                    flex: 1,
                  }}
                  textStyle={{
                    color: theme?.colors?.black,
                    marginVertical: 8,
                  }}
                  text={
                    item.InterestStakeStatusEnumId === 2
                      ? 'Bekliyor'
                      : 'Stake Durdur'
                  }
                />
              </View>
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    </Modal>
  );
};

export default FAStopStakeModal;
