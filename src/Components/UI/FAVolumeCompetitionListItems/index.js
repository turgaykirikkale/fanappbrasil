import React from 'react';
import {View, Image, Text} from 'react-native';
import FAButton from '../../Composite/FAButton';
import moment from 'moment';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

const FAVolumeCompetitionsListItems = props => {
  const {item, onPress, type} = props;
  const TimeForDaysLeft = date => {
    var now = moment(new Date());
    let endDate = moment(date);
    let days = endDate.diff(now, 'days');
    let hours = endDate.diff(now, 'hours') - 24 * days;
    let minutes = endDate.diff(now, 'minutes') - 24 * 60 * days - hours * 60;
    let returnValue = `${days} ${Localization.t(
      'VolumeCompetitions.Day',
    )} ${hours}  ${Localization.t(
      'VolumeCompetitions.Hour',
    )} ${minutes}  ${Localization.t('VolumeCompetitions.Minutes')} `;

    if (days < 0) {
      return `0  ${Localization.t('VolumeCompetitions.Day')} 0 ${Localization.t(
        'VolumeCompetitions.Hour',
      )} 0 ${Localization.t('VolumeCompetitions.Minutes')}`;
    }
    return returnValue;
  };
  const totalAmounFormatte = Amount => {
    return Amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };
  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={{
              width: '44.5%',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 10,
              backgroundColor: theme?.colors?.white,
              marginHorizontal: 10,
              paddingVertical: 20,
              borderRadius: 4,
            }}>
            <Image
              style={{
                width: 80,
                height: 80,
                borderRadius: 40,
              }}
              source={{
                uri:
                  item.CoinCode === null
                    ? `https://borsa.bitci.com/img/coin/${item.CurrencyCode}.png`
                    : `https://borsa.bitci.com/img/coin/${item.CoinCode}.png`,
                headers: {
                  'User-Agent': `${item.CoinCode}`,
                },
              }}
            />
            <Text
              style={{
                color: theme?.colors?.black,
                fontWeight: 'bold',
                fontSize: 15,
                marginTop: 10,
                marginBottom: 3,
              }}>
              {item.CurrencyCode}
            </Text>
            <Text style={{color: theme?.colors?.gray, fontSize: 10}}>
              {item.CoinCode !== null
                ? `${item.CoinCode}/${item.CurrencyCode}`
                : item.CurrencyCode}
            </Text>
            <View
              style={{
                alignItems: 'center',
                marginVertical: 8,
              }}>
              <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                {Localization.t('VolumeCompetitions.Total')}
              </Text>
              <Text
                style={{
                  marginVertical: 1,
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                }}>
                {totalAmounFormatte(item.TotalAmount)}{' '}
                {item.CoinCode !== null ? item.CoinCode : item.CurrencyCode}
              </Text>
              <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                {Localization.t('VolumeCompetitions.Waiting')}
              </Text>
            </View>
            {type === 1 ? (
              <>
                <View style={{}}>
                  <Text style={{fontSize: 10, color: theme?.colors?.gray}}>
                    {TimeForDaysLeft(item.EndDate)}
                  </Text>
                </View>
                <FAButton
                  onPress={() => onPress && onPress(item)}
                  text={Localization.t('VolumeCompetitions.Discover')}
                  containerStyle={{
                    borderColor: theme?.colors.black,
                    borderWidth: 1,
                    marginTop: 10,
                    borderRadius: 4,
                    width: '80%',
                  }}
                  textStyle={{
                    color: theme.colors.black,
                    marginVertical: 6,
                  }}
                />
              </>
            ) : (
              <FAButton
                onPress={() => onPress && onPress(item)}
                text={Localization.t('VolumeCompetitions.StartTrading')}
                containerStyle={{
                  backgroundColor: theme?.colors.orange,
                  marginTop: 10,
                  borderRadius: 4,
                  //   width: '80%',
                }}
                textStyle={{
                  color: theme.colors.black,
                  marginVertical: 6,
                  marginHorizontal: 10,
                }}
              />
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default FAVolumeCompetitionsListItems;
