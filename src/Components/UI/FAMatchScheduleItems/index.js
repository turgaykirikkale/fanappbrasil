import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import FAColor from '@Commons/FAColor';

const FAMatchScheduleItems = props => {
  const {selectedTournament, onPress} = props;
  return (
    <TouchableOpacity
      style={{
        paddingHorizontal: 20,
        marginTop: 10,
        borderBottomWidth: 1,
        borderColor: FAColor.SoftGray,
      }}
      onPress={() => onPress && onPress()}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{fontSize: 12, color: FAColor.DarkBlue}}>
          04/08/2021 21:15
        </Text>
        <Text style={{fontSize: 12, color: FAColor.DarkBlue}}>
          {selectedTournament}
        </Text>
      </View>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',

            flex: 1,
          }}>
          <Text style={{flex: 1}} letterSpacing={-0.7}>
            Sporting
          </Text>
          <View
            style={{
              marginHorizontal: 10,
              height: 40,
              width: 40,
              borderRadius: 20,
              backgroundColor: FAColor.Orange,
            }}></View>
        </View>

        <View
          style={{
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text style={{fontSize: 14}}>3 -</Text>

            <Text style={{fontSize: 14}}> 0</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text style={{fontSize: 9}}>2 -</Text>

            <Text style={{fontSize: 9}}> 0</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            flex: 1,
          }}>
          <View
            style={{
              marginHorizontal: 10,
              height: 40,
              width: 40,
              borderRadius: 20,
              backgroundColor: FAColor.Red,
            }}></View>
          <Text style={{flex: 1, textAlign: 'center'}}>Ajax</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default FAMatchScheduleItems;
