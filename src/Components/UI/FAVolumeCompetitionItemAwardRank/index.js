import React from 'react';
import {View, Text, FlatList} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

const FAVolumeCompetitionItemAwardRank = props => {
  const totalAmounFormatte = Amount => {
    return Amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };
  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
            <Text style={{color: theme?.colors?.black}}>
              {Localization.t('VolumeCompetitions.AwardRanking')}
            </Text>
            <FlatList
              showsVerticalScrollIndicator={false}
              scrollEnabled={true}
              style={{height: 200}}
              data={props.data}
              keyExtractor={(item, index) => index}
              showsHorizontalScrollIndicator={false}
              ItemSeparatorComponent={() => <View style={{marginLeft: 5}} />}
              renderItem={({item, index}) => {
                return (
                  <>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 6,
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          width: 30,
                          alignItems: 'center',
                          backgroundColor: theme?.colors?.gray,
                          borderRadius: 4,
                        }}>
                        <Text
                          style={{
                            fontSize: 12,
                            color: theme?.colors?.black,
                          }}>
                          {item.Order}
                        </Text>
                      </View>
                      <Text
                        style={{
                          color: theme?.colors?.black,
                          flex: 1,
                          textAlign: 'right',
                          fontSize: 13,
                          fontWeight: 'bold',
                        }}>
                        {totalAmounFormatte(item.Amount)} {props.Code}
                      </Text>
                    </View>
                  </>
                );
              }}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default FAVolumeCompetitionItemAwardRank;
