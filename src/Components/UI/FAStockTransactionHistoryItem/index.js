import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {toFixedNoRounding} from '@Commons/FAMath';
import {AppConstants} from '@Commons/Contants';
import {Green, Red} from '@Commons/FAColor';
import {Text, View} from 'react-native';
import {styles} from './assets/styles';
import PropTypes from 'prop-types';
import moment from 'moment';
import React from 'react';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FAStockTransactionHistoryItem = props => {
  const {date, type, coinAmount, commission, coinPrice, totalPrice, state} =
    props;
  const currencyDecimalCount = AppConstants.CurrencyDecimalCount;
  const coinDecimalCount = AppConstants.CoinDecimalCount;
  const coinCode = AppConstants.CoinCode;
  const currencyCode = AppConstants.CurrencyCode;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={[
            styles.mainContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          {coinCode && currencyCode ? (
            <View style={styles.pairAndDateContainer}>
              <Text style={[styles.pairText, {color: theme?.colors?.black}]}>
                {coinCode}/{currencyCode}
              </Text>
              <Text style={[styles.dateText, {color: theme?.colors?.gray}]}>
                {moment(date).format('DD.MM.YYYY HH:mm')}
              </Text>
            </View>
          ) : null}
          <View style={styles.informationContainer}>
            <Text
              style={[
                styles.transactionTypeText,
                {color: type === 1 ? theme?.colors?.green : theme?.colors?.red},
              ]}>
              {type === 1
                ? Localization.t('FAStockTransactionHistoryItem.Buying')
                : Localization.t('FAStockTransactionHistoryItem.Selling')}
            </Text>
            <Text
              style={[
                styles.transactionTypeText,
                {
                  color:
                    state === 2 ? theme?.colors?.green : theme?.colors?.red,
                },
              ]}>
              {state === 2
                ? Localization.t('FAStockTransactionHistoryItem.Completed')
                : Localization.t('FAStockTransactionHistoryItem.Cancelled')}
            </Text>
          </View>
          {coinPrice ? (
            <View style={styles.informationContainer}>
              <Text
                style={[
                  styles.informationLabel,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAStockTransactionHistoryItem.Price')} (
                {currencyCode})
              </Text>
              <Text
                style={[
                  styles.informationValue,
                  {color: theme?.colors?.black},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(coinPrice, currencyDecimalCount),
                  currencyDecimalCount,
                )}
              </Text>
            </View>
          ) : null}
          {coinAmount ? (
            <View style={styles.informationContainer}>
              <Text
                style={[
                  styles.informationLabel,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAStockTransactionHistoryItem.Amount')} (
                {coinCode})
              </Text>
              <Text
                style={[
                  styles.informationValue,
                  {color: theme?.colors?.black},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(coinAmount, coinDecimalCount),
                  coinDecimalCount,
                )}
              </Text>
            </View>
          ) : null}
          {commission ? (
            <View style={styles.informationContainer}>
              <Text
                style={[
                  styles.informationLabel,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAStockTransactionHistoryItem.Commission')} (
                {currencyCode})
              </Text>
              <Text
                style={[
                  styles.informationValue,
                  {color: theme?.colors?.black},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(commission, currencyDecimalCount),
                  currencyDecimalCount,
                )}
              </Text>
            </View>
          ) : null}
          {totalPrice ? (
            <View style={styles.informationContainer}>
              <Text
                style={[
                  styles.informationLabel,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('FAStockTransactionHistoryItem.TotalPrice')} (
                {currencyCode})
              </Text>
              <Text
                style={[
                  styles.informationValue,
                  ,
                  {color: theme?.colors?.black},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(totalPrice, currencyDecimalCount),
                  currencyDecimalCount,
                )}
              </Text>
            </View>
          ) : null}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

FAStockTransactionHistoryItem.propTypes = {
  date: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
  coinAmount: PropTypes.number.isRequired,
  commission: PropTypes.number.isRequired,
  coinPrice: PropTypes.number.isRequired,
  totalPrice: PropTypes.number.isRequired,
  state: PropTypes.number.isRequired,
};

export default FAStockTransactionHistoryItem;
