import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import {AppConstants} from '@Commons/Contants';
import {styles} from './assets/styles';
import React from 'react';
import PropTypes from 'prop-types';
import {Success} from '@Commons/FAColor';
import LinearGradient from 'react-native-linear-gradient';

const screenWidth = Dimensions.get('window').width;
const FAFanCard = props => {
  const {
    horizontalSpace,
    priceBoxColor,
    imageInLocal,
    branchName,
    cardRatio,
    coinCode,
    onPress,
    image,
    price,
    text,
    height,
  } = props;
  const generatedImage = image ? (imageInLocal ? image : {uri: image}) : null;
  if (!generatedImage) {
    return null;
  }
  return (
    <TouchableOpacity
      onPress={() => onPress && onPress()}
      activeOpacity={0.8}
      style={{
        width: screenWidth / cardRatio - horizontalSpace * 2,
        marginHorizontal: horizontalSpace,
        height: height ? screenWidth / 1.5 : screenWidth / 2,
      }}>
      <Image
        style={[styles.imageStyle, {height: '100%'}]}
        source={generatedImage}
        resizeMode={height ? 'stretch' : null}
      />
      {price && (
        <View
          style={[
            styles.headerTextContainer,
            {backgroundColor: priceBoxColor},
          ]}>
          <Text style={styles.headerText}>
            {price} {coinCode}
          </Text>
        </View>
      )}
      {branchName && (
        <View
          style={{
            position: 'absolute',
            bottom: 40,
            backgroundColor: 'rgba(0,0,0, .7)',
            zIndex: 11,
            paddingVertical: 3,
            paddingHorizontal: 4,
            marginHorizontal: 4,
            borderRadius: 4,
          }}>
          <Text style={{fontSize: 9, fontWeight: '600', color: 'white'}}>
            {branchName}
          </Text>
        </View>
      )}
      {text && (
        <LinearGradient
          style={styles.bottomTextLinear}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 0.5}}
          colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.7)']}>
          <View style={styles.bottomTextContainer}>
            <Text numberOfLines={3} style={styles.bottomText}>
              {text}
            </Text>
          </View>
        </LinearGradient>
      )}
    </TouchableOpacity>
  );
};

FAFanCard.propTypes = {
  image: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  horizontalSpace: PropTypes.number,
  priceBoxColor: PropTypes.string,
  imageInLocal: PropTypes.bool,
  cardRatio: PropTypes.number,
  coinCode: PropTypes.string,
  branchName: PropTypes.string,
  text: PropTypes.string,
  price: PropTypes.any,
};

FAFanCard.defaultProps = {
  coinCode: AppConstants.CoinCode,
  priceBoxColor: Success,
  horizontalSpace: 0,
  cardRatio: 1,
};

export default FAFanCard;
