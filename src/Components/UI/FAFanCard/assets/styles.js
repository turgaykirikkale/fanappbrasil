import {Dimensions, StyleSheet} from 'react-native';
import {White} from '@Commons/FAColor';

const screenWidth = Dimensions.get('screen').width;
export const styles = StyleSheet.create({
  imageStyle: {borderRadius: 4},
  headerTextContainer: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
  },
  headerText: {
    fontSize: 12,
    fontWeight: '500',
    color: White,
    textAlign: 'center',
    paddingVertical: 3,
  },
  bottomTextLinear: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    paddingBottom: 45,
    borderRadius: 5,
    height: screenWidth / 4,
  },
  bottomTextContainer: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    paddingHorizontal: 6,
    paddingVertical: 7,
    width: '100%',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    height: 40,
  },
  bottomText: {fontSize: 10, color: White},
});
