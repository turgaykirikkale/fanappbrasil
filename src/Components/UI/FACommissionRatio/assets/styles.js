import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  MarketMakerCommissionContainerStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    borderColor: FAColor.BlueGray,
    paddingVertical: 9,
  },
  marketMakerTextStyle: {fontSize: 12, color: FAColor.Black},
  marketMakerRatioTextStyle: {
    fontSize: 12,
    color: FAColor.Black,
    flex: 1,
    textAlign: 'right',
  },
  MarketTakerCommissionContainerStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    borderColor: FAColor.BlueGray,
    paddingVertical: 9,
  },
  MarketTakerCommissionTextStyle: {fontSize: 12, color: FAColor.Black},
  MarketTakerRatioTextStyle: {
    fontSize: 12,
    color: FAColor.Black,
    flex: 1,
    textAlign: 'right',
  },
});
