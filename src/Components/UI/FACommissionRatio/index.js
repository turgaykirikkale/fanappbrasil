import React from 'react';
import {View, Text} from 'react-native';
import Localization from '@Localization';
import {styles} from './assets/styles';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

const FACommissionRatio = props => {
  const {makerRatio, takerRatio} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <>
          <View
            style={[
              styles.MarketMakerCommissionContainerStyle,
              {borderColor: theme?.colors?.blueGray},
            ]}>
            <Text
              style={[
                styles.marketMakerTextStyle,
                {color: theme?.colors?.black},
              ]}>
              {Localization.t('CommissionScreen.MarketMakerCommission')}
            </Text>
            <Text
              style={[
                styles.marketMakerRatioTextStyle,
                {color: theme?.colors?.black},
              ]}>{`${makerRatio}%`}</Text>
          </View>
          <View
            style={[
              styles.MarketTakerCommissionContainerStyle,
              {borderColor: theme?.colors?.blueGray},
            ]}>
            <Text
              style={[
                styles.MarketTakerCommissionTextStyle,
                {color: theme?.colors?.black},
              ]}>
              {Localization.t('CommissionScreen.MarketTakerCommission')}
            </Text>
            <Text
              style={[
                styles.MarketTakerRatioTextStyle,
                {color: theme?.colors?.black},
              ]}>{`${takerRatio}%`}</Text>
          </View>
        </>
      )}
    </ThemeContext.Consumer>
  );
};

export default FACommissionRatio;
