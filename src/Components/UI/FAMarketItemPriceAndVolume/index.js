import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './assets/styles';

const FAMarketItemPriceAndVolume = props => {
  const {Price, Volume, isDisabled, currencySymbol, theme} = props;
  return (
    <View
      style={[styles.mainContainer, {backgroundColor: theme?.colors?.white}]}>
      <Text style={[styles.priceTextStyle, {color: theme?.colors?.black}]}>
        {Price}
      </Text>
      {isDisabled ? null : (
        <Text style={[styles.volumeTextStyle, {color: theme?.colors?.gray}]}>
          {Volume}
          {currencySymbol && ` ${currencySymbol}`}
        </Text>
      )}
    </View>
  );
};

export default FAMarketItemPriceAndVolume;
