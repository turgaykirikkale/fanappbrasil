import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor/index';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: FAColor.White,
    paddingVertical: 14,
    alignItems: 'flex-end',
  },
  priceTextStyle: {
    fontSize: 16,
    color: FAColor.Black,
    letterSpacing: -0.6,
  },
  volumeTextStyle: {
    fontSize: 14,
    color: FAColor.Gray,
    letterSpacing: -0.6,
  },
});
