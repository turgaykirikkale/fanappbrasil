import React from 'react';
import {View} from 'react-native';
import {styles} from './assets/styles';
import QRCode from 'react-native-qrcode-svg';

const FAWalletQr = props => {
  const {coinAddress} = props;
  return (
    <View style={styles.qrConrtainer}>
      <QRCode value={`${coinAddress}`} size={132} />
    </View>
  );
};

export default FAWalletQr;
