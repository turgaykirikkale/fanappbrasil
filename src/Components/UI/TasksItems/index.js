import React from 'react';
import {View, Text} from 'react-native';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import FAIcon from '@Components/UI/FAIcon';

const TaskItems = props => {
  const {header, data, DaysLeft} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            backgroundColor: theme?.colors?.white,
            paddingVertical: 10,
            paddingHorizontal: 10,
            marginBottom: 10,
            marginHorizontal: 10,
            borderRadius: 4,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <FAIcon
                iconName={'FAPages'}
                color={theme?.colors?.darkBlue}
                size={16}
              />
              <Text style={{color: theme?.colors?.darkBlue, marginLeft: 5}}>
                {header}
              </Text>
            </View>
            <Text
              style={{
                color: theme?.colors?.darkBlue,
                marginLeft: 5,
              }}>{`Kalan Süre : ${DaysLeft}`}</Text>
          </View>
          {data.map((item, index) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 16,
                  marginBottom: 8,
                }}>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    fontSize: 12,
                  }}>
                  {item.header}
                </Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    fontSize: 12,
                  }}>
                  {`${item.completed} / ${item.Total}`}
                </Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    fontSize: 12,
                  }}>
                  5 BITCI
                </Text>
              </View>
            );
          })}
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default TaskItems;
