// export const baseUrl = 'https://cedtradingapi-prod.bitci.com';
export const baseUrl = 'https://cedtradingapi-staging.bitci.com';
// export const baseUrl = 'https://www.bitci.com';
// export const baseUrl = 'https://brasil-test.bitci.tech';

export const LoginUrl = '/api/Accountv2/login';
export const userRegister = '/api/Accountv2/register';
export const userRegisterOtpConfirm = '/api/Accountv2/validategsm';
export const userRegisterSendMailVerification =
  '/api/Accountv2/sendemailverificationcode';
export const passwordReset = '/api/Accountv2/PasswordReset';
export const userRegisterValidateIdentity = '/api/Account/ValidateIdentity';
export const activeOrders = '/api/order/ActiveOrders';
export const customerActiveOrdersByPair =
  '/api/Order/ActiveCustomerOrdersByPair';
export const getPusherPrivateKey = '/api/Account/getpusherauthcode';
export const customerTransactionHistory = '/api/Order/CompletedOrders';
export const getMarketHistory = '/api/Order/Trades';
export const getCoinRates = '/api/Ticker/GetCoinRates';
export const getMarketList = '/api/Order/GetMarketCurrencyCoinMatches';
export const getCustomerInfo = '/api/Accountv2/getcustomerinfo';
export const getExchangeRegisterSetting =
  '/api/Accountv2/GetExchangeRegisterSetting';
export const getCountryList = '/api/Accountv2/GetCountryList';
export const getCityList = '/api/Accountv2/GetCityList';
export const getForeignCity = '/api/Accountv2/GetStates';
export const getForeignDistrict = '/api/Accountv2/GetCityListByState';

export const getDistrictList = '/api/Accountv2/GetDistrictList';
export const validateEntity = '/api/Accountv2/ValidateIdentity';
export const getEducationalStatus = '/api/Accountv2/EducationalStatusEnum';
export const getStapleEarnerEnum = '/api/Accountv2/StapleEarnerEnum';
export const getMonetaryIncome = '/api/Accountv2/MonetaryIncomeEnum';
export const getJobs = '/api/Accountv2/JobEnum';
export const getMonthlyEarning = '/api/Accountv2/MonthlyEarningEnum';
export const getTotalAssets = '/api/Accountv2/TotalAssetsEnum';
export const getInvesmentSource = '/api/Accountv2/InvestmentSourceEnum';
export const getRiskEnum = '/api/Accountv2/RiskEnum';
export const getInvestmentPurpose = '/api/Accountv2/InvestmentPurposeEnum';
export const contents = '/api/contents';
export const setCustomerIdentificationForm =
  '/api/Accountv2/SetCustomerIdentificationForm';
export const changePassword = '/api/Accountv2/changepassword';

export const getCustomerAllActiveOrders = '/api/Order/AllActiveCustomerOrders';
export const getUserFinanceState = '/api/FinanceState/GetFinanceState';
export const getPaymentChannelList = '/api/Money/PaymentChannelList';
export const getFinanceState = '/api/FinanceState/GetFinanceState';
export const moneyWithdrawal = '/api/Money/Withdraw';
export const getCurrencyWithdrawList = '/api/Money/WithdrawList';
export const getCurrencyDepositList = '/api/Money/DepositList';
export const getCoinWithdrawList = '/api/Coin/CoinWithdrawalList';
export const cancelCustomerActiveOrder = '/api/Order/Cancel';
export const getUserWallet = '/api/Account/assets';
export const getCoinChainNetwork = '/api/Coin/GetCoinChainNetwork';
export const getCoinAdress = '/api/Coin/GetAddress';
export const coinWithdrawal = '/api/Coin/Withdrawal';
export const getMarketInformation = '/api/Order/MarketInformation';
export const getCoinBalanceDetail = '/api/Coin/CoinBalanceDetail';
export const getCurrencyBalanceDetail = '/api/Order/GetCurrencyBalance';
export const createOrder = '/api/Order/Create';
export const referralCommission = '/api/Account/referralcommissions';
export const referralProgramFriends = '/api/Account/referralprogramfriends';
export const referralProgramState = '/api/Account/referralprogramstate';
export const getCHFTList = '/api/Converter/List';
export const getCHFTHistoryList = '/api/Converter/HistoryList';
export const convertCHFTValues = '/api/Converter/Update';
export const definePin = '/api/Account/definepin';
export const setCustomerAuthenticationType =
  '/api/Account/setcustomerauthenticationtype';
export const getIeoAssetList = '/api/Coin/IeoAssetList';
export const getIeoAssetDetail = '/api/Coin/IeoAssetDetail';
export const buyIeoToken = '/api/Coin/BuyIeoToken';
export const getSliderImage = '/api/sliders';
export const getFavoriteCoinList = '/api/Ticker/GetCoinFavoriteList';
export const removeCoinFromFavorites = '/api/Ticker/DeleteFavorite';
export const addCoinToFavorites = '/api/Ticker/AddFavorite';
export const userFileUpload = '/api/Account/CustomerFileUpload';
export const getAllEvents = '/api/coin/ActivityDetails';
export const joinEvent = '/api/Coin/JoinActivity';
export const getAllSurvey = '/api/Coin/QuizDetails';
export const voteQuiz = '/api/Coin/VoteQuiz';
export const quizResults = '/api/Coin/QuizResults';
export const getNews = '/api/Coin/ContentDetails';
export const newsBranches = '/api/Coin/branches';
export const newsByBranch = '/api/Coin/BranchContents';
export const mainPageVideos = '/api/Coin/MainVideoPage';
export const mainPageVideosByCategory = '/api/Coin/Videos';
export const helpCenter = '/api/Coin/helpcenter';
export const clubInfo = '/api/Coin/clubInfo';
export const getServiceTest = '/api/Coin/ClubInfo';
export const interestStakeInfo = '/api/Coin/InterestStakeInfo';
export const interestStake = '/api/Coin/InterestStake';
export const interestStakeProcess = '/api/Coin/InterestStakeProccess';
export const InterestStakeStatusEnum = '/api/Coin/InterestStakeStatusEnum';
export const getStakeDetails = '/api/FinanceState/GetStakeDetails';
export const completeFlexibleInterestStakeTransaction =
  '/api/Coin/CompleteFlexibleInterestStakeTransaction';
export const passwordChangeDuration = '/api/Account/PasswordChangeDuration';
export const getVolumeCompetitionEvents =
  '/api/Coin/GetVolumeCompetitionEvents';
export const getVolumeCompetitionItemsDetail =
  '/api/Coin/GetVolumeCompetitionEvent';
export const getUserCompetitionsRanks = '/api/Coin/GetVolumeCompetitions';

export const getDocumentRejection = '/api/Accountv2/DocumentRejectReasonEnum';
export const getDocumentType = '/api/Documentation/GetDocumentTypes';
export const addCustomerDocumantation =
  '/api/Documentation/AddCustomerDocumentation';
export const getSocketProvider = '/api/WebSocket/GetProvider';
