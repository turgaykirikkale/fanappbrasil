import axios from 'axios';

export const DAPIService = axios.create({
  baseURL: 'https://dapi.bitcitech.com',
  headers: {},
});

const services = {
  GetFanTokens: () => {
    return DAPIService.get(
      '/items/fantokens?fields=coin_code,coin_name,token_img,contract_address',
    );
  },
};

export default services;
