import axios from 'axios';
import * as URL from './URL';

export const BroadageService = axios.create({
  baseURL: URL.baseURL,
  headers: {
    // ApiToken: API_TOKEN,
    'Content-Type': 'application/json',
    // 'Ocp-Apim-Subscription-Key': 'fe892b2e-92d3-4aa5-aade-c52971c368bd',
    // languageId: 2,
    // Accept: 'application/xml',
    // 'User-Agent': AppConstants.UserAgent,
    'Ocp-Apim-Subscription-Key': 'c2ea738f-71f1-4ba9-9c4f-4b6912704d58',
  },
});

BroadageService.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);

const services = {
  GetBasketballMatchList: params => {
    return BroadageService.get(URL.basketballMatches, {
      params: {
        params,
      },
    });
  },
  //matchList
  GetSoccerMatchList: params => {
    return BroadageService.get(URL.soccerMatches, {
      params: {
        params,
      },
    });
  },
  GetDailyMatchList: params => {
    return BroadageService.get(URL.numberOfDailyMatchesLis, {
      params: {
        params,
      },
    });
  },
  //TournamentsANDSeason
  GetTournamentsList: params => {
    return BroadageService.get(URL.soccertournaments, {
      params: {
        pageSize: 100,
      },
    });
  },
  GetSoccerSeaosonId: id => {
    const Url = `${URL.soccertournaments}/{${id}}`;
    return BroadageService.get(Url, {});
  },

  GetSoccerSeaosonTeams: id => {
    const Url = `${URL.soccerSeoasonValues}/{6c0d6d75-24ee-48f1-df9c-08d9f4789a4f}/teams`;
    return BroadageService.get(Url, {});
  },
  GetSoccerSeaosonStandings: id => {
    const Url = `${URL.soccerSeoasonValues}/{${id}}/standings`;
    return BroadageService.get(Url, {});
  },
  GetSoccerSeaosonFixture: (id, roundId) => {
    const Url = `${URL.soccerSeoasonValues}/{${id}}/fixture`;
    return BroadageService.get(Url, {
      params: {
        round: roundId,
      },
    });
  },
  GetSoccerSeaosonLeaderBoardGoal: id => {
    const Url = `${URL.soccerSeoasonValues}/{6c0d6d75-24ee-48f1-df9c-08d9f4789a4f}/leaderBoard/goal`;
    return BroadageService.get(Url, {});
  },
  GetSoccerSeaosonLeaderBoardAssist: id => {
    const Url = `${URL.soccerSeoasonValues}/{6c0d6d75-24ee-48f1-df9c-08d9f4789a4f}/leaderBoard/assist`;
    return BroadageService.get(Url, {});
  },
  GetSoccerSeaosonLeaderBoardRedCard: id => {
    const Url = `${URL.soccerSeoasonValues}/{6c0d6d75-24ee-48f1-df9c-08d9f4789a4f}/leaderBoard/red-card`;
    return BroadageService.get(Url, {});
  },
  GetSoccerSeaosonLeaderBoardYellowCard: id => {
    const Url = `${URL.soccerSeoasonValues}/{6c0d6d75-24ee-48f1-df9c-08d9f4789a4f}/leaderBoard/yellow-card`;
    return BroadageService.get(Url, {});
  },

  //teams

  GetSoccerTeams: id => {
    const Url = `${URL.soccerTeams}/{961a66de-cfc4-4b29-e4fb-08d9f47941a8}`;
    return BroadageService.get(Url, {});
  },
};

export default services;
