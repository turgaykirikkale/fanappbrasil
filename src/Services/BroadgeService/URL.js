export const baseURL = 'https://sports.bitci.com';
export const basketballMatches = '/basketball/matches';
export const soccerMatches = '/soccer/matches';
export const soccertournaments = '/soccer/tournaments';
export const numberOfDailyMatchesLis =
  '/soccer/matches/number-of-daily-match-list';
export const soccerSeoasonValues = '/soccer/season';
export const soccerTeams = '/soccer/teams';
