import axios from 'axios';

export const NewsService = axios.create({
  baseURL: 'https://rss.app/feeds/PXC393hmRVL7QDzk.xml',
  headers: {},
});

const services = {
  GetNews: () => {
    return NewsService.get('/wp/v2/posts');
  },
};

export default services;
