import axios from 'axios';
import * as URL from './URL';
import * as RootNavigation from '@Navigation/RootNavigation';
import {AppConstants} from '../Commons/Contants';
import Localization from '@Localization';

// const API_TOKEN =
//   'nkM+r4QxK0END2A9p/DpzV4dZ6uTbBcKjSBNYLv1LwVAUQkrW77FzGc3TqsO/v4Et0mVhNhD0rk2nkTumHwSrinxv3NxnXUKAy83JZ8D2zJeAv/gd6W2pyqaJYlrLuZoMOOwxuAW2GOi0Bj7jdg1MGOnpU2z2+iRiRiwttJgVJHv94BHtYMVbpWFtwcXqsQg';
const API_TOKEN =
  'HLRKhKA0ifn8RuvgjI/DLYaXpxe2TLqNvzKiyHtGCSv5LCd1UrTUa58prn1Z+rfkwnpFy2Nh+MfH9SeD0rf9ZJIjH2mjNR9m1b2umpLydvk8TJPcO11Qk05tbq9zSfsGY75F+D6NBaQBeNEUS419JKjoDJvVSA/9s2iMzuKUt5Fi3iljS2+wmb8XsDakzep0';

export const FAService = axios.create({
  baseURL: URL.baseUrl,
  headers: {
    ApiToken: API_TOKEN,
    'Content-Type': 'application/json',
    // 'Ocp-Apim-Subscription-Key': 'fe892b2e-92d3-4aa5-aade-c52971c368bd',
    // languageId: 1,
    // Accept: 'application/xml',
    'User-Agent': 'bitci',
  },
});

let selectedLanguage = 'pt';

export const setLanguage = data => {
  if (data === 'en' || data === 'pt') {
    selectedLanguage = data;
  } else {
    selectedLanguage = 'en';
  }
};

export const deleteAuthorizationHeader = () => {
  delete FAService.defaults.headers.common.Authorization;
};

export const setAuthTokenToHeader = authToken => {
  FAService.defaults.headers.common.Authorization = 'Bearer ' + authToken;
};

FAService.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);

FAService.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (
      (error &&
        error.response &&
        error.response.status &&
        error.response.status === 401) ||
      (error &&
        error.response &&
        error.response.status &&
        error.response.status === 405)
    ) {
      if (
        error &&
        error.config &&
        error.config.headers &&
        error.config.headers.Authorization
      ) {
        delete FAService.defaults.headers.common.Authorization;
      }
      RootNavigation.navigate('ResetCredentialExecutor');
    }
    return Promise.reject(error);
  },
);

const services = {
  Login: (requestBody: Object): Object => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.LoginUrl, generatedRequestBody);
  },
  UserRegister: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    console.log('userRegisterRequ', requestBody);
    return FAService.post(URL.userRegister, requestBody);
  },
  UserRegisterOtpConfirm: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.post(URL.userRegisterOtpConfirm, generatedRequestBody);
  },
  UserRegisterSendMailVerification: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.post(
      URL.userRegisterSendMailVerification,
      generatedRequestBody,
    );
  },
  PasswordReset: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.post(URL.passwordReset, generatedRequestBody);
  },
  GetActiveOrders: requestBody => {
    const {selectedCoinCode, selectedCurrencyCode} = requestBody;
    let pair = `${selectedCoinCode}_${selectedCurrencyCode}`;
    return FAService.get(URL.activeOrders, {
      params: {pair: pair},
    });
  },
  GetCustomerActiveOrders: requestBody => {
    const {selectedCoinCode, selectedCurrencyCode} = requestBody;
    const generatedPair = `${selectedCoinCode}_${selectedCurrencyCode}`;

    const generatedRequestBody = {
      Pair: generatedPair,
      Active: true,
    };
    return FAService.post(URL.customerActiveOrdersByPair, generatedRequestBody);
  },
  GetPusherPrivateKey: () => {
    return FAService.post(URL.getPusherPrivateKey);
  },
  GetCustomerTransactionHistory: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.customerTransactionHistory, generatedRequestBody);
  },
  GetMarketHistory: requestParams => {
    const {selectedCoinCode, selectedCurrencyCode} = requestParams;
    const pair = selectedCoinCode + '_' + selectedCurrencyCode;
    return FAService.get(URL.getMarketHistory, {
      params: {
        pair: pair,
      },
    });
  },
  GetCoinRates: currencyId => {
    return FAService.get(URL.getCoinRates, {
      params: {currencyId: currencyId},
    });
  },
  GetMarketList: () => {
    return FAService.post(URL.getMarketList);
  },
  GetCustomerInfo: () => {
    return FAService.get(URL.getCustomerInfo);
  },
  GetExchangeRegisterSetting: () => {
    return FAService.get(URL.getExchangeRegisterSetting);
  },
  GetCountryList: () => {
    return FAService.get(URL.getCountryList);
  },

  GetCityList: countryId => {
    return FAService.get(URL.getCityList, {
      params: {
        countryId: countryId,
      },
    });
  },
  GetForeignCityList: countryId => {
    return FAService.get(URL.getForeignCity, {
      params: {
        countryId: countryId,
      },
    });
  },
  GetDistrictList: cityId => {
    return FAService.get(URL.getDistrictList, {
      params: {
        cityId: cityId,
      },
    });
  },
  GetForeignDistrictList: cityId => {
    return FAService.get(URL.getForeignDistrict, {
      params: {
        stateId: cityId,
      },
    });
  },

  GetValidateEntity: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.validateEntity, generatedRequestBody);
  },

  GetEducationalStatus: () => {
    return FAService.get(URL.getEducationalStatus);
  },
  GetStapleEarner: () => {
    return FAService.get(URL.getStapleEarnerEnum);
  },
  GetMonetaryIncome: () => {
    return FAService.get(URL.getMonetaryIncome);
  },
  GetJobs: () => {
    return FAService.get(URL.getJobs);
  },
  GetMonthlyEarning: () => {
    return FAService.get(URL.getMonthlyEarning);
  },
  GetTotalAssets: () => {
    return FAService.get(URL.getTotalAssets);
  },
  GetIvestmentSource: () => {
    return FAService.get(URL.getInvesmentSource);
  },
  GetRiskEnum: () => {
    return FAService.get(URL.getRiskEnum);
  },
  GetInvensmentPurpose: () => {
    return FAService.get(URL.getInvestmentPurpose);
  },
  contents: () => {
    const requestBody = {
      Code: selectedLanguage,
    };
    return FAService.post(URL.contents, requestBody);
  },
  SetCustomerIdentificationForm: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(
      URL.setCustomerIdentificationForm,
      generatedRequestBody,
    );
  },

  GetCustomerAllActiveOrders: () => {
    return FAService.get(URL.getCustomerAllActiveOrders);
  },
  CancelCustomerActiveOrder: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.cancelCustomerActiveOrder, generatedRequestBody);
  },
  GetUserFinanceState: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getUserFinanceState, generatedRequestBody);
  },
  GetPaymentChannelList: requestBody => {
    return FAService.get(URL.getPaymentChannelList, {
      params: {
        currencyId: requestBody.currencyId,
      },
    });
  },
  GetCustomerFinance: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
    };
    return FAService.post(URL.getFinanceState, generatedRequestBody);
  },
  GetUserWallet: () => {
    return FAService.post(URL.getUserWallet);
  },

  GetCoinChainNetwork: requestBody => {
    return FAService.get(URL.getCoinChainNetwork, {
      params: {
        coinId: requestBody.coinId,
        resource: selectedLanguage,
      },
    });
  },
  GetCoinAdress: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getCoinAdress, generatedRequestBody);
  },
  CoinWithdraw: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
    };
    return FAService.post(URL.coinWithdrawal, generatedRequestBody);
  },
  GetCoinWithdrawList: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getCoinWithdrawList, generatedRequestBody);
  },

  UserRegisterValidateIdentity: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(
      URL.userRegisterValidateIdentity,
      generatedRequestBody,
    );
  },
  GetMarketInformation: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getMarketInformation, generatedRequestBody);
  },
  GetCoinBalanceDetail: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getCoinBalanceDetail, generatedRequestBody);
  },
  GetCurrencyBalanceDetail: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getCurrencyBalanceDetail, generatedRequestBody);
  },
  CreateOrder: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.createOrder, generatedRequestBody);
  },
  ReferralCommision: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.referralCommission, generatedRequestBody);
  },
  ReferralProgramFriends: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.referralProgramFriends, generatedRequestBody);
  },
  GetCHFTList: () => {
    return FAService.get(URL.getCHFTList);
  },
  GetCHFTHistoryList: () => {
    return FAService.get(URL.getCHFTHistoryList);
  },
  ConvertCHFTValues: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.convertCHFTValues, generatedRequestBody);
  },
  ReferralProgramState: () => {
    return FAService.get(URL.referralProgramState);
  },
  GetIeoAssetList: () => {
    return FAService.get(URL.getIeoAssetList);
  },
  GetIeoAssetDetail: coinId => {
    return FAService.get(URL.getIeoAssetDetail, {
      params: {coinId: coinId},
    });
  },
  BuyIeoToken: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.buyIeoToken, generatedRequestBody);
  },
  DefinePin: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.definePin, generatedRequestBody);
  },
  ChangePassword: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.changePassword, generatedRequestBody);
  },
  SetCustomerAuthenticationType: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(
      URL.setCustomerAuthenticationType,
      generatedRequestBody,
    );
  },
  GetSliderImage: coinId => {
    return FAService.get(URL.getSliderImage, {
      params: {
        coinId: coinId,
        take: 5,
        languageCode: selectedLanguage,
        skip: 0,
      },
    });
  },
  GetFavoriteCoinList: () => {
    return FAService.get(URL.getFavoriteCoinList);
  },
  AddCoinToFavorites: requestBody => {
    return FAService.post(URL.addCoinToFavorites, requestBody);
  },
  RemoveCoinFromFavorites: requestBody => {
    return FAService.post(URL.removeCoinFromFavorites, requestBody);
  },
  deleteActiveCoinWithdraw: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.delete(URL.coinWithdrawal, {data: generatedRequestBody});
  },
  UserFileUpload: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.userFileUpload, generatedRequestBody);
  },
  GetActiveEvents: params => {
    return FAService.get(URL.getAllEvents, {
      params: {
        ...params,
        isActive: true,
        resource: selectedLanguage,
      },
    });
  },
  GetPassedEvents: params => {
    return FAService.get(URL.getAllEvents, {
      params: {
        ...params,
        isActive: false,
        resource: selectedLanguage,
      },
    });
  },
  JoinEvent: (requestBody: Object): Promise => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.post(URL.joinEvent, generatedRequestBody);
  },
  GetActiveSurvey: params => {
    return FAService.get(URL.getAllSurvey, {
      params: {
        // coinId: 92,
        isActive: true,
        resource: selectedLanguage,
      },
    });
  },
  GetPassedSurvey: params => {
    return FAService.get(URL.getAllSurvey, {
      params: {
        ...params,
        isActive: false,
        resource: selectedLanguage,
      },
    });
  },
  VoteQuiz: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };

    return FAService.post(URL.voteQuiz, generatedRequestBody);
  },
  QuizResults: requestBody => {
    return FAService.get(URL.quizResults, requestBody);
  },

  GetNews: params => {
    return FAService.get(URL.getNews, {
      params: {
        params,
      },
    });
  },
  GetNewsBranches: () => {
    return FAService.get(URL.newsBranches, {
      params: {
        // tokenId: AppConstants.CoinId,
        // tokenId: 29,
        resource: selectedLanguage,
      },
    });
  },
  GetNewsByBranch: branchId => {
    return FAService.get(URL.newsByBranch, {
      params: {
        // TokenId: AppConstants.CoinId,
        // TokenId: 90,
        resource: selectedLanguage,
        // BranchId: branchId,
      },
    });
  },
  GetMainPageVideos: () => {
    return FAService.get(URL.mainPageVideos, {
      params: {
        // TokenId: AppConstants.CoinId,
        // tokenId: 90,
        resource: selectedLanguage,
      },
    });
  },
  GetMainPageVideosByCategory: categoryId => {
    return FAService.get(URL.mainPageVideosByCategory, {
      params: {
        // tokenId: 90,
        resource: selectedLanguage,
        categoryId: categoryId,
      },
    });
  },
  GetHelpCenterContent: tokenId => {
    return FAService.get(URL.helpCenter, {
      params: {
        TokenId: tokenId,
        Resource: selectedLanguage,
      },
    });
  },
  GetClubInfo: tokenId => {
    return FAService.get(URL.clubInfo, {
      params: {
        TokenId: tokenId,
        Resource: selectedLanguage,
      },
    });
  },
  GetInterestStakeInfo: () => {
    const generatedRequestBody = {
      LanguageCode: selectedLanguage,
    };
    return FAService.get(URL.interestStakeInfo, generatedRequestBody);
  },
  GetInterestStake: () => {
    const generatedRequestBody = {
      LanguageCode: selectedLanguage,
    };
    return FAService.get(URL.interestStake, generatedRequestBody);
  },
  InterestStakeProcess: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.interestStakeProcess, generatedRequestBody);
  },
  InterestStakeStatusEnum: requestBody => {
    const generatedRequestBody = {
      LanguageCode: selectedLanguage,
    };
    return FAService.get(URL.InterestStakeStatusEnum, generatedRequestBody);
  },
  GetStakeDetails: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.getStakeDetails, generatedRequestBody);
  },
  CompleteFlexibleInterestStakeTransaction: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(
      URL.completeFlexibleInterestStakeTransaction,
      generatedRequestBody,
    );
  },
  PasswordChangeDuration: () => {
    return FAService.get(URL.passwordChangeDuration);
  },
  getVolumeCompetitionEvents: () => {
    return FAService.get(URL.getVolumeCompetitionEvents);
  },
  getVolumeCompetitionItemDetail: requestBody => {
    return FAService.get(URL.getVolumeCompetitionItemsDetail, {
      params: requestBody,
    });
  },
  getUserCompetitionRanks: requestBody => {
    return FAService.post(URL.getUserCompetitionsRanks, requestBody);
  },

  getUserDocumentRejection: () => {
    return FAService.get(URL.getDocumentRejection);
  },

  getUserDocumentationType: () => {
    return FAService.get(URL.getDocumentType);
  },

  addCustomerDocumentation: requestBody => {
    const generatedRequestBody = {
      ...requestBody,
      LanguageCode: selectedLanguage,
    };
    return FAService.post(URL.addCustomerDocumantation, generatedRequestBody);
  },
  getSocketProvider: () => {
    return FAService.get(URL.getSocketProvider, {
      params: {
        channel: 'web',
      },
    });
  },
};

export default services;
