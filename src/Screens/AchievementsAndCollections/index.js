import React from 'react';
import {View, Image, Text} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {ScrollView} from 'react-native-gesture-handler';
import Localization from '@Localization';

const AchievementsAndCollections = props => {
  const cardValue = [
    {Name: 'Standart', Value: '1-10'},
    {Name: 'Gold', Value: '11-20'},
    {Name: 'Diamond', Value: '21-30'},
    {Name: 'Platinum', Value: '31-40'},
    {Name: 'Meteorite', Value: '41-60'},
    {Name: 'Epic', Value: '61-80'},
    {Name: 'Legendary', Value: '81-100'},
  ];
  const passive = true;
  const {navigation} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
          {passive ? (
            <View
              style={{
                marginTop: 70,
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0, 0.6)',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 99,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white'}}>
                {Localization.t('FanScreen.Soon')}
              </Text>
            </View>
          ) : null}
          <FAStandardHeader title={'Koleksiyon'} navigation={navigation} />
          <ScrollView>
            <View
              style={{
                alignItems: 'center',
                height: 250,
                justifyContent: 'center',
                borderBottomWidth: 0.3,
                borderColor: theme?.colors.black,
              }}>
              <FAVectorIcon
                iconName={'question'}
                color={theme?.colors?.black}
                size={200}
              />
            </View>

            <View
              style={{
                marginHorizontal: 10,
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                }}>
                Başarımlar Nedir?
              </Text>
              <Text style={{color: theme?.colors?.black, marginTop: 10}}>
                Bitci FANAPP uygulamasında tamamladığınız Görevler karşılığında
                standart Başarım Kartları elde edersiniz.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 10}}>
                Bu kartlar görevleri tamamlayan tüm kullanıcılara verilir.
              </Text>
            </View>

            <View
              style={{
                marginHorizontal: 10,
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                }}>
                Başarım Kartları Ne İşe Yarar?
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 10}}>
                Kazanılan Başarım Kartları BITCICOIN veya Fan Tokenlardan
                herhangi birisi ile Koleksiyon eşyasına döndürülür ve Kart
                yükseltme işlemine açık hale getirilir.
              </Text>
            </View>

            <View
              style={{
                marginHorizontal: 10,
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                  marginBottom: 20,
                }}>
                Kart Seviyeleri
              </Text>

              {cardValue.map((item, index) => {
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{color: theme?.colors?.black, marginVertical: 3}}>
                      {item.Name}
                    </Text>
                    <Text
                      style={{color: theme?.colors?.black, marginVertical: 5}}>
                      {item.Value}
                    </Text>
                  </View>
                );
              })}
            </View>

            <View
              style={{
                marginHorizontal: 10,
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                  marginBottom: 20,
                }}>
                Koleksiyon Kartım İle Ne Yapabilirim?
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Seviyesi yükseltilen kartları FANAPP Market alanında
                satabilirsiniz.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Arkadaşlarınız ile takas yapabilirsiniz.
              </Text>
            </View>
            <View
              style={{
                marginHorizontal: 10,
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              <Text
                style={{
                  color: theme?.colors?.black,
                  fontWeight: 'bold',
                  marginBottom: 20,
                }}>
                Kart Seviyesi Nasıl Yükseltilir ?
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Her koleksiyon kartının seviyesi belirlenen ücretler üzerinden
                yapılır.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Bitcicoin veya Fan Tokenler kullanılarak yükseltme işlemleri
                yapılır.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Yükseltme ücretleri BITCICOIN üzerinden belirlenir ve anlık
                olarak BITCI veya Fan Token olarak hesaplanır.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Koleksiyon kartı yükseltilmesi yapıldıktan sonra 1 hafta boyunca
                pazarda satılamaz veya takas edilemez.
              </Text>
              <Text style={{color: theme?.colors?.black, marginVertical: 3}}>
                Başarım kartları sadece 1 kex Koleksiyon eşyası olarak
                yükseltilebilir.
              </Text>
            </View>
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default AchievementsAndCollections;
