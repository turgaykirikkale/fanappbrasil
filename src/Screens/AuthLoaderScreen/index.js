import {preLoginInitializeDataCallAction} from '@GlobalStore/Actions/FlowActions';
import {initializeDataCallAction} from '@GlobalStore/Actions/FlowActions';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import React, {useCallback, useEffect, useState} from 'react';
import {CommonActions} from '@react-navigation/native';
import {getDataFromStorage} from '@Commons/FAStorage';
import {setAuthTokenToHeader} from '@Services';
import {
  setIsAnonymousFlow,
  setStockModeAction,
} from '@GlobalStore/Actions/MarketActions';
import {connect} from 'react-redux';
import moment from 'moment';
// import 'moment/locale/en';
import {changeLanguage} from '@GlobalStore/Actions/UserActions';
import {setLanguage} from '@Services';
import Localization from '@Localization';
import {getLocales} from 'react-native-localize';
import SplashScreen from 'react-native-splash-screen';
import {setDataToStorage} from '../../Commons/FAStorage';
import FAService from '../../Services';
import {setSocketconfigs} from '../../Stream/index';

const AuthLoaderScreen = props => {
  const {
    initializeDataCallAction: authDataCall,
    preLoginInitializeDataCallAction: anonymousDataCall,
    setStockModeAction: setStockMode,
    setIsAnonymousFlow: setAnonymous,
    changeLanguage: changeCurrentLanguage,
    navigation,
  } = props;
  const [isLoading, setIsLoading] = useState(true);

  const screenFlowExecutor = async () => {
    // Önceden seçilen dili ya da cihaz dilini alıp hem store'a
    // hem de servis request body'sine eklemek üzere fonksiyonu tetikliyoruz
    // moment().locale('en');
    let currentLanguage = 'en';
    try {
      const languageFromStorage = await getDataFromStorage('Language');
      if (languageFromStorage) {
        currentLanguage = languageFromStorage;
        changeCurrentLanguage(currentLanguage);
        setLanguage(currentLanguage);
        await Localization.changeLanguage(currentLanguage);
        await setDataToStorage('Language', currentLanguage);
      } else {
        //Storage'da dil yok ise kullanıcı cihazının dilini alacağız
        let deviceLanguage = getLocales()[0].languageCode;
        if (deviceLanguage === 'en' || deviceLanguage === 'en') {
          changeLanguage(deviceLanguage);
          setLanguage(deviceLanguage);
          await Localization.changeLanguage(deviceLanguage);
          await setDataToStorage('Language', deviceLanguage);
        } else {
          changeLanguage('en');
          setLanguage('en');
          await Localization.changeLanguage('en');
          await setDataToStorage('Language', 'en');
        }
      }
    } catch (e) {}

    // Daha önceden seçilen borsa modunu storage'den alıp store'a yazıyoruz
    try {
      const currentStockMode = await getDataFromStorage('StockMode');
      if (currentStockMode) {
        setStockMode(currentStockMode);
      }
    } catch (e) {
      console.log('setSelectedStockMode action exception in AuthLoader');
    }

    // Kullanıcı authorization kontrolünü yapıyoruz
    const incomingToken = await getDataFromStorage('Token');
    console.log('incomingToken', incomingToken);
    if (incomingToken) {
      setAuthTokenToHeader(incomingToken);
      setAnonymous(false);
      const initialRouteParams = {
        isAnonymous: false,
      };
      authDataCall();
      setTimeout(() => {
        setIsLoading(false);
      }, 1000);

      resetStackAndNavigate('AppStackTabNavigator', initialRouteParams);
      SplashScreen.hide();
    } else {
      setAnonymous(true);
      const initialRouteParams = {
        isAnonymous: true,
      };
      anonymousDataCall();
      setTimeout(() => {
        setIsLoading(false);
      }, 1000);
      resetStackAndNavigate('PreLoginStackTabNavigator', initialRouteParams);
      SplashScreen.hide();
    }
  };

  useEffect(() => {
    async function fetchSocketProvider() {
      debugger;
      const response = await FAService.getSocketProvider();
      await setSocketconfigs(response.data);
      console.log('RESPONSESockerProvider', response);
    }

    fetchSocketProvider();
    screenFlowExecutor();

    return () => {
      setIsLoading(false);
    };
  }, []);

  const resetStackAndNavigate = (stackName: String, params: Object): void => {
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: stackName, params: {...params}}],
      }),
    );
  };
  return <FAFullScreenLoader isLoading={isLoading} />;
};

export default connect(
  null,
  {
    initializeDataCallAction,
    preLoginInitializeDataCallAction,
    setStockModeAction,
    setIsAnonymousFlow,
    changeLanguage,
  },
  null,
)(AuthLoaderScreen);
