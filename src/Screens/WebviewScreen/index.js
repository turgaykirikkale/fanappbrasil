import React, {Component} from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import WebView from 'react-native-webview';
import FAIcon from '@Components/UI/FAIcon';
import {Black} from '@Commons/FAColor';
import {styles} from './assets/styles';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAColor from '../../Commons/FAColor';

export default class WebviewScreen extends Component {
  render() {
    const LoadingIndicatorView = () => {
      return (
        <View
          style={{
            height: '100%',
            width: '100%',
            alignSelf: 'center',
            position: 'absolute',
            justifyContent: 'center',
            backgroundColor: 'rgba(0,0,0,0.6)',
          }}>
          <ActivityIndicator color={FAColor.Orange} size="large" style={{}} />
        </View>
      );
    };
    const {props} = this;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              title={props?.route?.params?.title}
              navigation={props.navigation}
            />
            <WebView
              renderLoading={LoadingIndicatorView}
              startInLoadingState={true}
              source={{uri: `${this.props.route.params.url}`}}
              style={{
                backgroundColor: theme?.colors?.blueGray,
              }}
            />
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}
