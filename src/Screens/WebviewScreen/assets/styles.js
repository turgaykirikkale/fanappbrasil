import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  backButton: {
    justifyContent: 'center',
    width: 8,
    height: '100%',
    marginLeft: 14,
  },
  headerTitle: {alignSelf: 'center', fontSize: 12, fontWeight: '600'},
});
