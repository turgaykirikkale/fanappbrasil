import React, {useState} from 'react';
import {View, Text} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FASlider from '@Components/UI/FASlider';
import _ from 'lodash';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import NFTCollectionsLists from '@Components/Composite/NFTCollectionsLists';

const NFTCollections = props => {
  const [sliderList, setSliderList] = useState([]);

  const passive = true;

  const array = [
    {image: require('../../Assets/images/QuestionSymbol.jpeg')},
    {image: require('../../Assets/images/QuestionSymbol.jpeg')},
    {image: require('../../Assets/images/QuestionSymbol.jpeg')},
  ];
  const {navigation} = props;

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
          {passive ? (
            <View
              style={{
                marginTop: 70,
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0, 0.6)',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 99,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white'}}>Aktif değildir.</Text>
            </View>
          ) : null}

          <FAStandardHeader title={'NFT'} navigation={navigation} />
          {_.isEmpty(sliderList) ? (
            <View
              style={{
                alignItems: 'center',
                height: 250,
                justifyContent: 'center',
                borderBottomWidth: 0.2,
                borderColor: theme?.colors.black,
              }}>
              <FAVectorIcon
                iconName={'question'}
                color={theme?.colors?.black}
                size={200}
              />
            </View>
          ) : (
            <View
              style={{
                borderRadius: 10,
                marginHorizontal: 10,
                marginVertical: 10,
              }}>
              <FASlider data={sliderList} />
            </View>
          )}
          <NFTCollectionsLists data={array} title={'Başlık'} />
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default NFTCollections;
