import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FATextInput from '@Components/Composite/FATextInput';
import {CommonActions} from '@react-navigation/native';
import FAButton from '@Components/Composite/FAButton';
import {setDataToStorage} from '@Commons/FAStorage';
import FAForm from '@ValidationEngine/Form';
import autobind from 'autobind-decorator';
import {View, Text, AppState} from 'react-native';
import {styles} from './assets/styles';
import FAEnums from '@Commons/FAEnums';
import FAService from '@Services';
import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import {setAuthTokenToHeader} from '../../Services';

export default class OTPScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: null,
      countDownTime: 200,
      errorModalVisible: false,
      serviceErrorMessage: null,
      Phone:
        (props.route && props.route.params && props.route.params.phone) || '',
      flow:
        (props &&
          props.route &&
          props.route.params &&
          props.route.params.flow) ||
        FAEnums.LOGINFLOW,
      isGoogleAuth:
        (props &&
          props.route &&
          props.route.params &&
          props.route.params.isGoogleAuth) ||
        false,
      showDialogBox: false,
      FormIsValid: false,
      serviceLoading: false,
      buttonDisabled: false,
      appState: AppState.currentState,
      backgroundTime: null,
      foregroundTime: null,
      disabledResendButton: false,
      resendErrorMessage: null,
    };
  }

  timerInterval = null;

  async callOtpServiceForRegister() {
    const {otp, Phone} = this.state;
    if (
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.phone
    ) {
      const {navigation} = this.props;
      let requestBody = {
        Phone,
        SmsConfirmationCode: otp,
      };
      this.setState({serviceLoading: true});
      FAService.UserRegisterOtpConfirm(requestBody)
        .then(response => {
          if (
            response &&
            response.data &&
            response.status === 200 &&
            response.data.IsSuccess
          ) {
            this.setState({serviceLoading: false}, () => {
              this.onFinishcountDownTimer();
              clearInterval(this.timerInterval);
              navigation.navigate('EmailConfirmationScreen', {
                phone: requestBody.Phone,
              });
            });
          } else {
            this.setState({
              showDialogBox: true,
              serviceErrorMessage:
                response.data.ErrorMessage || response.data.Message,
              serviceLoading: false,
            });
          }
        })
        .catch(error => {
          console.log('error', error);
          this.setState({
            showDialogBox: true,
            serviceErrorMessage: Localization.t('CommonsFix.UnexpectedError'),
            serviceLoading: false,
          });
        });
    }
  }

  async callOtpService() {
    if (
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.loginUserCridentials
    ) {
      const {otp} = this.state;
      const {navigation} = this.props;
      const incomingUserCridentialsFromLoginScreen =
        this.props.route.params.loginUserCridentials;
      let requestBody = {
        ...incomingUserCridentialsFromLoginScreen,
        AuthenticationCode: otp,
      };
      this.setState({serviceLoading: true});
      FAService.Login(requestBody)
        .then(async response => {
          if (response?.data?.CustomerInfo?.MustPasswordChange) {
            setAuthTokenToHeader(response.data.Token);
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  {
                    name: 'PasswordGenerate',
                    params: {
                      hideBackButton: true,
                      CustomerInfo: response.data.CustomerInfo || null,
                    },
                  },
                ],
              }),
            );
            return;
          }
          if (response && response.data.Success && response.data.Token) {
            this.setState({serviceLoading: false});
            await setDataToStorage('Token', response.data.Token);
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [{name: 'AuthLoaderScreen'}],
              }),
            );
          } else if (
            response &&
            !response.data.Success &&
            !_.isNil(response.data.ErrorMessage)
          ) {
            this.setState({
              errorModalVisible: true,
              serviceErrorMessage: response.data.ErrorMessage,
              showDialogBox: true,
              serviceLoading: false,
            });
          }
        })
        .catch(error => {});
    } else {
      //LOGİN SAYFASINDAN USER CRİDENTIAL GELMEMİŞ ALERT ÇIKARTILIP Bİ ÖNCEKİ SAYFAYA DÖNMESİ GEREKMEKTEDİR.
    }
  }

  handleResendOtp() {
    const {countDownTime} = this.state;
    this.setState(
      {
        countDownTime: 200,
      },
      () => {
        this.timerInterval = setInterval(this.countDownLogic, 1000);
      },
    );

    if (
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.loginUserCridentials
    ) {
      const incomingUserCridentialsFromLoginScreen =
        this.props.route.params.loginUserCridentials;
      let requestBody = {
        ...incomingUserCridentialsFromLoginScreen,
      };

      FAService.Login(requestBody)
        .then(response => {
          if (response && response.data.Success) {
          } else {
            this.setState({
              resendErrorMessage: response.data.ErrorMessage,
            });
          }
        })
        .catch(error => {});
    }
  }

  onFinishcountDownTimer() {
    return null;
  }

  @autobind
  countDownLogic() {
    const {countDownTime} = this.state;
    if (typeof countDownTime === 'number') {
      const incomingTimeObject = this.calculateMinAndSec();
      if (
        incomingTimeObject &&
        !_.isNil(incomingTimeObject.min) &&
        !_.isNil(incomingTimeObject.sec)
      ) {
        const {min, sec} = incomingTimeObject;
        this.setState({
          countDownTime: countDownTime - 1,
          min,
          sec,
        });
        if (countDownTime === 0) {
          this.onFinishcountDownTimer();
          clearInterval(this.timerInterval);
        }
      }
    }
  }

  componentWillMount() {
    const {countDownTime} = this.state;
    if (countDownTime) {
      this.timerInterval = setInterval(this.countDownLogic, 1000);
    }
  }

  componentWillUnMount() {
    if (this.timerInterval) {
      clearInterval(this.timerInterval);
    }
  }

  componentDidMount() {
    this.appStateSubscription = AppState.addEventListener(
      'change',
      nextAppState => {
        if (
          this.state.appState.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          const {countDownTime, backgroundTime} = this.state;

          if (backgroundTime) {
            let now = moment();
            let different = moment.duration(now.diff(backgroundTime));
            let newCountDownTime = countDownTime;
            different = different.asSeconds();
            // eslint-disable-next-line no-const-assign
            newCountDownTime -= different;
            newCountDownTime = parseInt(newCountDownTime);
            if (newCountDownTime < 0) {
              newCountDownTime = 0;
            }
            this.setState({
              backgroundTime: null,
              countDownTime: newCountDownTime,
            });
          }
        } else {
          this.setState({
            backgroundTime: moment(),
          });
          console.log('App has come to the background!');
        }
        this.setState({appState: nextAppState});
      },
    );
  }

  @autobind
  calculateMinAndSec() {
    const {countDownTime} = this.state;
    if (typeof countDownTime === 'number') {
      const min = parseInt(countDownTime / 60, 10);
      const sec = countDownTime - min * 60;
      return {min, sec};
    }
  }

  @autobind
  displayTimeLogic(value) {
    if (value / 10 >= 1) {
      return String(value);
    }
    return '0' + value;
  }

  @autobind
  displayCountDownTime() {
    const {countDownTime} = this.state;
    const {min, sec} = this.state;
    if (!_.isNil(countDownTime) && !_.isNil(min) && !_.isNil(sec)) {
      return (
        <Text style={styles.countDownTimerStyle}>
          {this.displayTimeLogic(min)}:{this.displayTimeLogic(sec)}
        </Text>
      );
    }
  }

  render() {
    const {
      otp,
      serviceErrorMessage,
      flow,
      FormIsValid,
      serviceLoading,
      showDialogBox,
      isGoogleAuth,
      countDownTime,
      resendErrorMessage,
      Phone,
    } = this.state;
    const {navigation} = this.props;
    if (countDownTime === 0) {
    }

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAAlertModal
              visible={showDialogBox}
              text={serviceErrorMessage || resendErrorMessage}
              iconName={'times-circle'}
              header={Localization.t('CommonsFix.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAStandardHeader
              hideBackButton={
                this.props?.route?.params?.hideBackButton || false
              }
              title={
                this.state.isGoogleAuth
                  ? Localization.t('OTPScreenFix.goggle2FA')
                  : flow === FAEnums.LOGINFLOW
                  ? Localization.t('OTPScreenFix.HeaderTitle')
                  : Localization.t('OTPScreenFix.AccountConfirmation')
              }
              navigation={navigation}
            />
            <View style={styles.childContainer}>
              <Text
                style={[
                  styles.headerTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {isGoogleAuth
                  ? Localization.t('OTPScreenFix.GoogleInformationTitle')
                  : flow === FAEnums.LOGINFLOW
                  ? Localization.t('OTPScreenFix.SmsInformationTitle')
                  : Localization.t('OTPScreenFix.register')}
              </Text>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    FormIsValid: value,
                  })
                }>
                <FATextInput
                  keyboardType={'numeric'}
                  label={
                    isGoogleAuth
                      ? Localization.t('OTPScreenFix.TwoFAVerificationCode')
                      : flow === FAEnums.LOGINFLOW
                      ? Localization.t('OTPScreenFix.SmsVerificationCode')
                      : Localization.t('OTPScreenFix.registerLabel')
                  }
                  placeholder={
                    isGoogleAuth
                      ? Localization.t('OTPScreenFix.TwoFAVerificationCode')
                      : flow === FAEnums.LOGINFLOW
                      ? Localization.t('OTPScreenFix.SmsVerificationCode')
                      : Localization.t('OTPScreenFix.registerPlaceholder')
                  }
                  onChangeValue={newWalue =>
                    this.setState({
                      otp: newWalue,
                    })
                  }
                  value={otp}
                  rule={'isOtp'}
                  errorMessage={Localization.t('OTPScreenFix.ErrorMessage')}
                  mask={'[000000]'}
                />
              </FAForm>
              <FAButton
                disabled={!FormIsValid}
                onPress={() =>
                  flow === FAEnums.LOGINFLOW
                    ? this.callOtpService()
                    : this.callOtpServiceForRegister()
                }
                containerStyle={[
                  styles.buttonContainer,
                  {backgroundColor: theme?.colors?.green},
                ]}
                textStyle={[
                  styles.buttonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('OTPScreenFix.VerifyButtonText')}
              />
              {isGoogleAuth ? null : (
                <Text
                  style={[
                    styles.codeArrivedTextStyle,
                    {color: theme?.colors?.darkBlue},
                  ]}>
                  {Localization.t('OTPScreenFix.SubInformationTitle')}
                </Text>
              )}

              {isGoogleAuth ? null : countDownTime < 0 ? (
                <FAButton
                  onPress={() => this.handleResendOtp()}
                  containerStyle={[
                    styles.resendbuttonContainer,
                    {backgroundColor: theme?.colors?.green},
                  ]}
                  textStyle={[
                    styles.buttonTextStyle,
                    {color: theme?.colors?.white2},
                  ]}
                  text={Localization.t('OTPScreenFix.reSendSms')}
                />
              ) : (
                <Text
                  style={[styles.timerTextStyle, {color: theme?.colors?.gray}]}>
                  {this.displayCountDownTime()
                    ? this.displayCountDownTime()
                    : '03.20'}{' '}
                  {Localization.t('OTPScreenFix.SMSagain')}
                </Text>
              )}
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
