import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  childContainer: {marginVertical: 23, marginHorizontal: 10},
  headerTextStyle: {fontSize: 20, color: '#3B3B3B', marginBottom: 16}, // TO DO Color
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 12,
  },
  buttonTextStyle: {
    marginVertical: 15,
    color: FAColor.White,
  },
  codeArrivedTextStyle: {
    color: FAColor.DarkBlue,
    textAlign: 'center',
    marginTop: 16,
  },
  timerTextStyle: {
    fontSize: 12,
    color: FAColor.Gray,
    textAlign: 'center',
    marginTop: 16,
  },
  resendbuttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 8,
  },
});
