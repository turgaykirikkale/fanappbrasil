import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import FAWalletHeader from '@Components/Composite/FAWalletHeader';
import {connect} from 'react-redux';
import _ from 'lodash';
import Localization from '@Localization';
import {MockTokenList} from '@Commons/MockDatas/TokenList';
import {toFixedNoRounding} from '@Commons/FAMath';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import {ScrollView} from 'react-native-gesture-handler';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';

class WalletScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: null,
      mainWallet: {},
      secondWallet: {},
      thirdWallet: {},
      wallets: [],
      hideZeroWallets: props?.route?.params?.hideZeroWallets || false,
    };
  }

  _unsubscribeFocusListener = null;

  componentWillMount() {
    const {navigation, getUserFinanceStateAction} = this.props;
    this._unsubscribeFocusListener = navigation.addListener('focus', () => {
      getUserFinanceStateAction();
    });
    this.fillWallets();
  }

  componentWillUnmount() {
    this._unsubscribeFocusListener();
  }

  componentWillReceiveProps(nextProps) {
    const nextWalletState = nextProps?.WalletState?.UserWallets || null;
    if (
      nextWalletState &&
      nextWalletState !== this.props.WalletState?.UserWallets
    ) {
      this.fillWallets();
    }
  }

  fillWallets() {
    const {WalletState} = this.props;
    if (WalletState?.UserWallets?.length > 0) {
      const userWallets = WalletState.UserWallets;
      console.log(userWallets);
      const mainWallet = _.find(userWallets, {Code: 'BITCI'});
      const secondWallet = _.find(userWallets, {Code: 'USDT'});
      const thirdWallet = _.find(userWallets, {Code: 'BFT'});
      console.log(mainWallet, secondWallet, thirdWallet);
      let otherWallets = [];
      userWallets.map(item => {
        if (MockTokenList.includes(item.Code)) {
          otherWallets.push(item);
        }
      });
      this.setState({
        mainWallet,
        secondWallet,
        thirdWallet,
        wallets: otherWallets,
      });
    }
  }

  filterWallet(stateWallets) {
    const {searchValue} = this.state;
    let filteredWallet = [];
    if (searchValue?.length > 0) {
      _.each(stateWallets, wallet => {
        const lowerCasedSearchText = searchValue.toLowerCase();
        if (
          wallet.Code.toLowerCase().indexOf(lowerCasedSearchText) >= 0 ||
          wallet.Name.toLowerCase().indexOf(lowerCasedSearchText) >= 0
        ) {
          filteredWallet.push(wallet);
        }
      });
      return filteredWallet;
    }
    return stateWallets;
  }

  render() {
    const {navigation} = this.props;
    const {
      mainWallet,
      secondWallet,
      wallets,
      searchValue,
      hideZeroWallets,
      thirdWallet,
    } = this.state;
    let filteredWallets = this.filterWallet(wallets);
    if (hideZeroWallets) {
      filteredWallets = _.filter(
        filteredWallets,
        item => item.Balance && toFixedNoRounding(item.Balance, 8) > 0.0,
      );
    }
    const isAndroid = Platform.OS === 'android';

    console.log(mainWallet, secondWallet, thirdWallet);
    return (
      <>
        <ThemeContext.Consumer>
          {({theme}) => (
            <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
              <FAStandardHeader
                title={Localization.t('CommonsFix.Wallet')}
                navigation={navigation}
              />
              <ScrollView>
                <View
                  style={{
                    marginBottom: 12,
                    marginHorizontal: 10,
                    marginTop: 20,
                  }}>
                  <FAWalletHeader
                    theme={theme}
                    currencyName={mainWallet.Code}
                    currencyCode={mainWallet.Name}
                    balance={mainWallet.Balance}
                    onPressDepositButton={() =>
                      navigation.navigate('WalletScreenDetail', {
                        index: 0,
                        data: mainWallet,
                      })
                    }
                    onPressWithdrawButton={() =>
                      navigation.navigate('WalletScreenDetail', {
                        index: 1,
                        data: mainWallet,
                      })
                    }
                  />
                </View>

                <View
                  style={{
                    borderWidth: 2,
                    borderColor: theme?.colors?.vGray,
                  }}
                />

                {secondWallet?.DepositStatus && (
                  <>
                    <View style={{marginVertical: 12, marginHorizontal: 10}}>
                      <FAWalletHeader
                        theme={theme}
                        currencyName={secondWallet.Code}
                        currencyCode={secondWallet.Name}
                        balance={secondWallet.Balance}
                        onPressDepositButton={() =>
                          navigation.navigate('WalletScreenDetail', {
                            index: 0,
                            data: secondWallet,
                            mainWallet,
                            hideWithdraw: true,
                            showSwap: true,
                          })
                        }
                        onPressSwapButton={() => {
                          navigation.navigate('WalletScreenDetail', {
                            index: 1,
                            data: secondWallet,
                            mainWallet,
                            hideWithdraw: true,
                            showSwap: true,
                          });
                        }}
                      />
                    </View>
                    <View
                      style={{
                        borderWidth: 2,
                        borderColor: theme?.colors?.vGray,
                      }}
                    />
                  </>
                )}
                {thirdWallet?.DepositStatus && (
                  <>
                    <View style={{marginVertical: 12, marginHorizontal: 10}}>
                      <FAWalletHeader
                        theme={theme}
                        currencyName={thirdWallet.Code}
                        currencyCode={thirdWallet.Name}
                        balance={thirdWallet.Balance}
                        onPressDepositButton={() =>
                          navigation.navigate('WalletScreenDetail', {
                            index: 0,
                            data: thirdWallet,
                          })
                        }
                        onPressWithdrawButton={() =>
                          navigation.navigate('WalletScreenDetail', {
                            index: 1,
                            data: thirdWallet,
                          })
                        }
                      />
                    </View>
                    <View
                      style={{
                        borderWidth: 2,
                        borderColor: theme?.colors?.vGray,
                      }}
                    />
                  </>
                )}
              </ScrollView>
            </View>
          )}
        </ThemeContext.Consumer>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    WalletState: state.WalletState,
  };
};

export default connect(mapStateToProps, {getUserFinanceStateAction})(
  WalletScreen,
);
