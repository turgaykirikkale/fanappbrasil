import {Dimensions, Text, TouchableOpacity, View} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {GetEventsAction} from '@GlobalStore/Actions/EventActions';
import FAPollCardList from '@Components/Composite/FAPollCardList';
import FAAssetShower from '@Components/Composite/FAAssetShower';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {TabBar, TabView} from 'react-native-tab-view';
import React, {useEffect, useState} from 'react';
import {AppConstants} from '@Commons/Contants';
import FAIcon from '@Components/UI/FAIcon';
import Localization from '@Localization';
import {connect} from 'react-redux';
import {DarkGray} from '@Commons/FAColor';
import _ from 'lodash';
import {getDataFromStorage, setDataToStorage} from '../../Commons/FAStorage';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const EventsScreen = props => {
  const {navigation} = props;
  const {EventState, UserState} = props;
  const route = props?.route?.params?.routeMain;
  const [index, setIndex] = useState(0);
  //Events
  const [activeEvents, setActiveEvents] = useState([]);
  const [passedEvents, setPassedEvents] = useState([]);
  const [joinedEvents, setJoinedEvents] = useState([]);
  const [showFreeActiveEvents, setShowFreeActiveEvents] = useState(false);
  const [showFreePassedEvents, setShowFreePassedEvents] = useState(false);
  const [showFreeJoinedEvents, setShowFreeJoinedEvents] = useState(false);
  const [showInfo, setShowInfo] = useState(false);

  //Balances
  const [currencyBalance, setCurrencyBalance] = useState(0.0);
  const [coinBalance, setCoinBalance] = useState(0.0);

  const [routes, setRoutes] = useState([
    {
      key: 'active',
      title: Localization.t('CommonsFix.Active'),
    },
    {
      key: 'joined',
      title: Localization.t('CommonsFix.Joined'),
    },
    {
      key: 'passed',
      title: Localization.t('CommonsFix.Passed'),
    },
  ]);

  useEffect(() => {
    const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;
    const {GetEventsAction} = props;
    if (!isAnonymousFlow) {
      GetEventsAction();
    }
  }, []);

  // useEffect(async () => {
  //   const showInformationValue = await getDataFromStorage(
  //     'ShowEventInformation',
  //   );
  //   if (!showInformationValue) {
  //     setShowInfo(true);
  //   } else {
  //     setShowInfo(false);
  //   }
  // }, []);

  useEffect(() => {
    if (EventState) {
      setActiveEvents(EventState?.ActiveEvents);
      setPassedEvents(EventState?.PassedEvents);
      setJoinedEvents(EventState?.JoinedEvents);
    }
  }, [EventState]);

  useEffect(() => {
    if (UserState?.FinanceInfo) {
      const {CustomerCoinBalanceDetailList} = UserState?.FinanceInfo;
      const currencyBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        CoinId: 29,
      });
      const coinBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        CoinId: AppConstants.CoinId,
      });
      setCurrencyBalance(
        FACurrencyAndCoinFormatter(
          currencyBalanceDetail?.CoinBalance || 0.0,
          AppConstants.CurrencyDecimalCount,
        ),
      );
      setCoinBalance(
        FACurrencyAndCoinFormatter(
          coinBalanceDetail?.CoinBalance || 0.0,
          AppConstants.CurrencyDecimalCount,
        ),
      );
    }
  }, [UserState]);

  // const handleInformation = async () => {
  //   await setDataToStorage('ShowEventInformation', 'true');
  //   setShowInfo(false);
  // };

  const renderTabBar = (tabProps, theme) => {
    return (
      <TabBar
        {...tabProps}
        renderLabel={({route, focused, color}) => (
          <Text
            style={{
              fontSize: 12,
              color: focused
                ? theme?.colors?.faOrange
                : theme?.colors?.darkBlue,
            }}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={{
          borderColor: theme?.colors?.gray,
        }}
        indicatorStyle={{
          backgroundColor: theme?.colors?.faOrange,
        }}
        style={{
          backgroundColor: theme?.colors?.white,
        }}
      />
    );
  };

  const renderScene = ({route}, theme) => {
    switch (route.key) {
      case 'active':
        return tabRenderActiveEvents(theme);
      case 'joined':
        return tabRenderJoinedEvents(theme);
      case 'passed':
        return tabRenderPastEvents(theme);
      default:
        return null;
    }
  };

  const renderFilterButtonText = (showFreeEvents: Boolean) => {
    let buttonText = `%s ${Localization.t('EventsScreenFix.ShowEvents')}`;
    if (showFreeEvents) {
      buttonText = buttonText.replace(/%s/g, Localization.t('CommonsFix.All'));
    } else {
      buttonText = buttonText.replace(/%s/g, Localization.t('CommonsFix.Free'));
    }
    return buttonText;
  };

  const renderFilterEventListButton = (
    eventList: Array,
    eventType: 'active' | 'joined' | 'passed',
  ) => {
    let buttonText = Localization.t('EventsScreenFix.ShowFreeEvents');
    buttonText = renderFilterButtonText(
      eventType === 'active'
        ? showFreeActiveEvents
        : eventType === 'joined'
        ? showFreeJoinedEvents
        : eventType === 'passed'
        ? showFreePassedEvents
        : null,
    );
    return (
      <TouchableOpacity
        onPress={() => filterEventList(eventList, eventType)}
        activeOpacity={0.8}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 15,
          paddingHorizontal: 13,
        }}>
        <View style={{width: 14, height: 14}}>
          <FAIcon iconName={'FAEye'} color={'#AAAAAA'} />
        </View>
        <Text style={{fontSize: 12, color: '#AAAAAA', marginLeft: 9}}>
          {buttonText}
        </Text>
      </TouchableOpacity>
    );
  };

  const filterEventList = (
    eventList: Array,
    eventType: 'active' | 'joined' | 'passed',
  ) => {
    if (eventType === 'active') {
      setShowFreeActiveEvents(!showFreeActiveEvents);
    } else if (eventType === 'joined') {
      setShowFreeJoinedEvents(!showFreeJoinedEvents);
    } else if (eventType === 'passed') {
      setShowFreePassedEvents(!showFreePassedEvents);
    }
  };

  const navigateToEventDetailScreen = item => {
    const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;
    const UserBalance =
      props?.UserState?.FinanceInfo?.CustomerCoinBalanceDetailList;

    navigation.navigate('EventDetailScreen', {
      UserBalance,
      event: item,
      isAnonymousFlow: isAnonymousFlow,
      routeMain: route,
    });
  };

  const tabRenderActiveEvents = theme => {
    return (
      <>
        {renderFilterEventListButton(activeEvents, 'active')}
        <View style={{marginHorizontal: 5, flex: 1}}>
          <FAPollCardList
            data={showFreeActiveEvents ? activeEvents?.Free : activeEvents?.All}
            spaceBetweenItems={10}
            onPress={item => navigateToEventDetailScreen(item)}
          />
        </View>
      </>
    );
  };

  const tabRenderJoinedEvents = theme => {
    if (!_.isEmpty(joinedEvents) && (joinedEvents.All || joinedEvents.Free)) {
      const joinedEventData = showFreeJoinedEvents
        ? joinedEvents?.Free
        : joinedEvents?.All;
      return (
        <>
          {renderFilterEventListButton(joinedEvents, 'joined')}
          <View style={{marginHorizontal: 5, flex: 1}}>
            <FAPollCardList
              data={joinedEventData}
              spaceBetweenItems={10}
              onPress={(CoinCode, item) =>
                navigateToEventDetailScreen({CoinCode: CoinCode, item: item})
              }
            />
          </View>
        </>
      );
    }
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Text
          style={{
            textAlign: 'center',
            color: DarkGray,
            fontSize: 14,
          }}>
          {Localization.t('EventsScreenFix.noAttendEvent')}
        </Text>
      </View>
    );
  };

  const tabRenderPastEvents = theme => {
    return (
      <>
        {renderFilterEventListButton(passedEvents, 'passed')}
        <View style={{marginHorizontal: 5, flex: 1}}>
          <FAPollCardList
            data={showFreePassedEvents ? passedEvents?.Free : passedEvents?.All}
            spaceBetweenItems={10}
            onPress={(CoinCode, item) =>
              navigateToEventDetailScreen({CoinCode: CoinCode, item: item})
            }
          />
        </View>
      </>
    );
  };

  const initialLayout = {width: Dimensions.get('window').width};
  const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;

  return (
    <>
      <FAStandardHeader
        title={Localization.t('CommonsFix.Events')}
        navigation={navigation}
      />
      {/* <FAAlertModal
        iconName={'info-circle'}
        header={Localization.t('InformationTitle')}
        text={''}
        onPress={handleInformation}
        modalControl
        visible={showInfo}
      /> */}
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
            <FAAssetShower
              onPress={flow => {
                flow === 1
                  ? navigation.navigate('LoginScreen')
                  : flow === 2
                  ? navigation.navigate('RegisterStepOne')
                  : navigation.navigate('WalletTabStackNavigator');
              }}
              fanTokenBalance={coinBalance}
              currencyBalance={currencyBalance}
              props={props}
            />
            {isAnonymousFlow ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 8,
                }}>
                <Text
                  style={{
                    color: theme?.colors?.darkGray,
                    fontSize: 16,
                    textAlign: 'center',
                  }}>
                  {Localization.t('EventsScreenFix.JoinAnonymous')}
                </Text>
              </View>
            ) : (
              <TabView
                renderTabBar={tabProps => renderTabBar(tabProps, theme)}
                renderScene={sceneProps => renderScene(sceneProps, theme)}
                navigationState={{index, routes}}
                onIndexChange={indexValue => setIndex(indexValue)}
                initialLayout={initialLayout}
              />
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

const mapStateToProps = state => {
  return {
    EventState: state.EventState,
    UserState: state.UserState,
    MarketState: state.MarketState,
  };
};
export default connect(mapStateToProps, {
  GetEventsAction,
})(EventsScreen);
