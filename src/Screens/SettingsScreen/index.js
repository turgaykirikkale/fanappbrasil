import React, {useState} from 'react';
import {View, Linking, Alert, Text} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAAccountMenuItems from '@Components/Composite/FAAccountMenuItems';
import {connect} from 'react-redux';
import codePush from 'react-native-code-push';
import {DarkBlue} from '@Commons/FAColor';
import Localization from '@Localization';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const SettingScreen = props => {
  const {navigation, UserState} = props;
  const isAnonymous = props?.route?.params?.isAnonymous;
  const Language = props?.UserState?.Language;

  const [checkUpdate, setCheckUpdate] = useState(false);

  const openSupportLink = () => {
    try {
      if (Language === 'en') {
        Linking.openURL('https://www.bitci.com/en/yardim');
      } else {
        Linking.openURL('https://www.bitci.com/yardim');
      }
    } catch (error) {}
  };

  const checkAppUpdate = () => {
    setCheckUpdate(true);
    codePush.checkForUpdate().then(update => {
      if (!update) {
        setCheckUpdate(false);
        Alert.alert(
          Localization.t('SettingScreenFix.Uptade'),
          Localization.t('SettingScreenFix.AppIsAlreadyUpdate'),
        );
      } else {
        Alert.alert(
          Localization.t('SettingScreenFix.Uptade'),
          Localization.t('SettingScreenFix.UpdateAvailable'),
          [
            {
              text: Localization.t('SettingScreenFix.No'),
              onPress: () => null,
            },
            {
              text: Localization.t('SettingScreenFix.Yes'),
              onPress: () => {
                codePush.sync({
                  installMode: codePush.InstallMode.IMMEDIATE,
                });
              },
            },
          ],
        );
        setCheckUpdate(false);
      }
    });
  };

  console.log('Seeting', props);

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <FAFullScreenLoader
            isLoading={checkUpdate}
            text={Localization.t('SettingScreenFix.CheckforUpdates')}
          />

          <FAStandardHeader
            title={Localization.t('Drawer.Settings')}
            navigation={navigation}
          />

          <FAAccountMenuItems.AccountParentItems
            // leftIconName={'BMAEarthLine'}
            text={Localization.t('SettingScreenFix.SelectLanguage')}
            onPress={() => navigation.navigate('SelectLanguageScreen')}
          />

          {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={Localization.t('SettingScreenFix.AccountSetting')}
              onPress={() => navigation.navigate('AccountSettings')}
            />
          )}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={'Erişim Ayarları'}
              // onPress={() =>
              //   navigation.navigate('SecuritySettingScreen', {
              //     UserState: UserState,
              //   })
              // }
            />
          )} */}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={'Onay/Bildirim Ayarları'}
              // onPress={() =>
              //   navigation.navigate('SecuritySettingScreen', {
              //     UserState: UserState,
              //   })
              // }
            />
          )} */}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={'Belgelerim'}
              // onPress={() =>
              //   navigation.navigate('SecuritySettingScreen', {
              //     UserState: UserState,
              //   })
              // }
            />
          )} */}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={'İletişim Ayarları'}
              // onPress={() =>
              //   navigation.navigate('SecuritySettingScreen', {
              //     UserState: UserState,
              //   })
              // }
            />
          )} */}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMASetting2Line'}
              text={'Geçmiş'}
              // onPress={() =>
              //   navigation.navigate('SecuritySettingScreen', {
              //     UserState: UserState,
              //   })
              // }
            />
          )} */}
          {/* <FAAccountMenuItems.AccountParentItems
            leftIconName={'BMALifebuoyLine'}
            text={Localization.t('SettingScreen.HelpSupport')}
            onPress={() => openSupportLink()}
          />
          {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              leftIconName={'BMAInfo'}
              text={Localization.t('SettingScreen.CommissionRatios')}
              onPress={() => navigation.navigate('CommissionScreen')}
            />
          )}
          {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              leftIconName={'BMAUsers'}
              text={Localization.t('SettingScreen.References')}
              onPress={() => navigation.navigate('ReferenceScreen')}
            />
          )}
          <FAAccountMenuItems.AccountParentItems
            disable={checkUpdate}
            leftIconName={'sync'}
            text={
              checkUpdate
                ? Localization.t('SettingScreen.CheckingForUpdate')
                : Localization.t('SettingScreen.CheckForUpdates')
            }
            onPress={() => checkAppUpdate()}
          /> */}
          {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMAUsers'}
              text={Localization.t('SettingScreenFix.References')}
              onPress={() => navigation.navigate('ReferenceScreen')}
            />
          )}
          {/* {isAnonymous ? null : (
            <FAAccountMenuItems.AccountParentItems
              // leftIconName={'BMAInfo'}
              text={Localization.t('SettingScreen.CommissionRatios')}
              onPress={() => navigation.navigate('CommissionScreen')}
            />
          )} */}
          <FAAccountMenuItems.AccountParentItems
            disable={checkUpdate}
            // leftIconName={'sync'}
            text={
              checkUpdate
                ? Localization.t('SettingScreenFix.CheckingForUpdate')
                : Localization.t('SettingScreenFix.CheckforUpdates')
            }
            onPress={() => checkAppUpdate()}
          />

          <Text
            style={{
              alignSelf: 'center',
              marginTop: 20,
              color: theme?.colors?.darkBlue,
              fontSize: 12,
            }}>
            V1.1.9
          </Text>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, null)(SettingScreen);
