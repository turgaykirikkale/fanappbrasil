import {StyleSheet} from 'react-native';
import FAColor, {SoftGray, DarkGray, Green, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  headerLeftIconStyle: {
    height: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
  },
  mainContainer: {paddingHorizontal: 10, backgroundColor: White, flex: 1},
  marginHorizontal20: {marginHorizontal: 20},
  textContainer: {
    borderBottomWidth: 1,
    borderColor: SoftGray,
  },
  textStyle: {
    backgroundColor: White,
    paddingHorizontal: 10,
    color: DarkGray,
    paddingBottom: 16,
  },
  serviceCallButtonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
  },
  serviceCallButtonTextStyle: {color: FAColor.White, marginVertical: 14},
  skipButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    borderRadius: 4,
    marginVertical: 6,
  },
  skipButtonTextStyle: {
    color: FAColor.DarkBlue,
    marginVertical: 14,
  },
});
