import FAAccountConfirmationFlow from '@Components/Composite/FAAccountConfirmationFlow';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {CommonActions} from '@react-navigation/native';
import FAButton from '@Components/Composite/FAButton';
import FACamera from '@Components/Composite/FACamera';
import {View, ScrollView} from 'react-native';
import FAForm from '@ValidationEngine/Form';
import autobind from 'autobind-decorator';
import React, {Component} from 'react';
import {styles} from './assets/styles';
import FAService from '@Services';
import Localization from '@Localization';

export default class AccountConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDialogBox: false,
      serviceMessage: null,
      isLoading: false,
      showCamera: false,
      flashOpen: false,
      uploadIdentityfrontend: false,
      uploadIdentitybackend: false,
      uploadPhoto: false,
      order: null,
      ImageOne: null,
      ImageTwo: null,
      ImageThree: null,
      formIsValid: false,
    };
  }

  openCamera(incomingOrder) {
    this.setState({order: incomingOrder, showCamera: true});
  }

  customerFileUploadServiceCall() {
    const {navigation} = this.props;
    this.setState({isLoading: true});
    const requestBody = {
      ImageOne: {
        Content: this.state.ImageOne.base64,
        Name: 'front.jpg',
      },
      ImageTwo: {
        Content: this.state.ImageTwo.base64,
        Name: 'back.jpg',
      },
      ImageThree: {
        Content: this.state.ImageThree.base64,
        Name: 'selfie.jpg',
      },
    };

    FAService.UserFileUpload(requestBody)
      .then(response => {
        if (response && response.data && response.data.IsSuccess) {
          this.setState({isLoading: false}, () => {
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [{name: 'AuthLoaderScreen'}],
              }),
            );
          });
        } else {
          this.setState({
            showDialogBox: true,
            serviceMessage: response.data.ErrorMessage || response.data.Message,
            isLoading: false,
          });
        }
      })
      .catch(err => {
        this.setState({
          showDialogBox: true,
          serviceMessage: Localization.t('Commons.UnexpectedError'),
          isLoading: false,
        });
      });
  }

  resetAndNavigateToAppStack() {
    const {navigation} = this.props;
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'AuthLoaderScreen'}],
      }),
    );
  }

  @autobind
  onShotCamera(data) {
    const {order} = this.state;

    if (order === 1) {
      this.setState({
        ImageOne: data,
        showCamera: false,
      });
    } else if (order === 2) {
      this.setState({
        ImageTwo: data,
        showCamera: false,
      });
    } else if (order === 3) {
      this.setState({
        ImageThree: data,
        showCamera: false,
      });
    }
  }

  render() {
    const {
      uploadIdentityfrontend,
      uploadIdentitybackend,
      uploadPhoto,
      showCamera,
      ImageOne,
      ImageTwo,
      ImageThree,
      isLoading,
      showDialogBox,
      serviceMessage,
      formIsValid,
    } = this.state;
    const {navigation} = this.props;

    if (showCamera) {
      return (
        <FACamera
          showCamera={showCamera}
          onShotCamera={image => this.onShotCamera(image)}
          closeCamera={() => this.setState({showCamera: false})}
        />
      );
    }
    return (
      <View style={{flex: 1}}>
        <FAFullScreenLoader isLoading={isLoading} />
        <FAAlertModal
          visible={showDialogBox}
          text={serviceMessage}
          iconName={'times-circle'}
          header={Localization.t('Commons.Error')}
          onPress={() => this.setState({showDialogBox: false})}
        />
        <FAStandardHeader
          title={Localization.t('AccountConfirmationScreen.HeaderTitle')}
          navigation={navigation}
        />
        <ScrollView style={styles.mainContainer}>
          <FAForm
            formValidSituationChanged={formIsValid =>
              this.setState({
                formIsValid: formIsValid,
              })
            }>
            <FAAccountConfirmationFlow
              value={ImageOne}
              rule={'notEmptyAndNil'}
              iconName="FAfronID"
              firstText={Localization.t(
                'AccountConfirmationScreen.FirstStepTitle',
              )}
              secondText={Localization.t(
                'AccountConfirmationScreen.FirstStepContent',
              )}
              image={ImageOne}
              openCamera={() => this.openCamera(1)}
              onPress={() => this.setState({uploadIdentityfrontend: true})}
              onPressDelete={() =>
                this.setState({
                  ImageOne: null,
                })
              }
              upload={uploadIdentityfrontend}
            />
            <FAAccountConfirmationFlow
              value={ImageTwo}
              rule={'notEmptyAndNil'}
              iconName="FAfronID"
              firstText={Localization.t(
                'AccountConfirmationScreen.SecondStepTitle',
              )}
              secondText={Localization.t(
                'AccountConfirmationScreen.SecondStepContent',
              )}
              image={ImageTwo}
              openCamera={() => this.openCamera(2)}
              onPress={() => this.setState({uploadIdentitybackend: true})}
              onPressDelete={() =>
                this.setState({
                  ImageTwo: null,
                })
              }
              upload={uploadIdentitybackend}
            />
            <FAAccountConfirmationFlow
              value={ImageThree}
              rule={'notEmptyAndNil'}
              iconName="FAMan"
              firstText={Localization.t(
                'AccountConfirmationScreen.ThirdStepTitle',
              )}
              secondText={Localization.t(
                'AccountConfirmationScreen.ThirdStepContent',
              )}
              image={ImageThree}
              openCamera={() => this.openCamera(3)}
              onPress={() => this.setState({uploadPhoto: true})}
              onPressDelete={() =>
                this.setState({
                  ImageThree: null,
                })
              }
              upload={uploadPhoto}
              thirdText={Localization.t(
                'AccountConfirmationScreen.ThirdStepInformation',
              )}
            />
            <View style={{marginTop: 12}}>
              <FAButton
                disabled={!formIsValid}
                onPress={() => this.customerFileUploadServiceCall()}
                containerStyle={styles.serviceCallButtonContainer}
                textStyle={styles.serviceCallButtonTextStyle}
                text={Localization.t(
                  'AccountConfirmationScreen.SentMyAccountButtonText',
                )}
              />
              <FAButton
                disabled={false}
                onPress={() => {
                  this.resetAndNavigateToAppStack();
                }}
                containerStyle={styles.skipButtonContainer}
                textStyle={styles.skipButtonTextStyle}
                text={Localization.t(
                  'AccountConfirmationScreen.SkipThisStepText',
                )}
              />
            </View>
          </FAForm>
        </ScrollView>
      </View>
    );
  }
}
