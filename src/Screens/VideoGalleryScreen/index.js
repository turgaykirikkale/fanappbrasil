import React, {useState, useEffect} from 'react';
import FADrawerHeader from '../../Components/Composite/FADrawerHeader';
import {ScrollView, View} from 'react-native';
import Localization from '@Localization';
import FAService from '@Services';
import _ from 'lodash';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FANewsShower from '../../Components/Composite/FANewsShower';
import {sentenceShortener} from '@Helpers/StringHelper';
import {connect} from 'react-redux';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';

const VideoGalleryScreen = props => {
  const {navigation, UserState} = props;

  const videos = props?.route?.params?.category?.Videos;
  console.log(props, 'propsFrom videoGE');
  const [serviceLoading, setServiceLoading] = useState(false);
  // const [videos, setVideos] = useState([]);
  const [rssVideos, setRssVideos] = useState([]);

  // useEffect(() => {
  //   let Videos = [];
  //   let VideosDetail = [];

  //   FAService.GetMainPageVideos()
  //     .then(res => {
  //       if (res?.data) {
  //         console.log('VideeD', res);
  //         setServiceLoading(false);
  //         const resData = res.data;
  //         const categoryFiterList = _.filter(resData, category => {
  //           return !_.isEmpty(category?.Videos);
  //         });

  //         if (categoryFiterList) {
  //           _.each(categoryFiterList, item => {
  //             Videos.push(item.Videos);
  //           });
  //         }
  //         if (Videos.length > 0) {
  //           _.each(Videos, item => {
  //             if (item.length > 0) {
  //               _.each(item, Video => {
  //                 VideosDetail.push(Video);
  //               });
  //             }
  //           });
  //         }
  //         setVideos(VideosDetail);
  //       }
  //     })
  //     .catch(err => console.log('errVideo', err));
  // }, []);

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <>
          <View
            style={{
              flex: 1,
              backgroundColor: theme?.colors?.white,
            }}>
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAStandardHeader title={'Video Gallery'} navigation={navigation} />
            <ScrollView
              style={{
                flex: 1,
                paddingBottom: 10,
                marginTop: 5,
              }}
              showsVerticalScrollIndicator={false}>
              {_.map(videos, item => {
                console.log(item, 'videoİtem');
                return (
                  <>
                    <View style={{marginHorizontal: 10}}>
                      <FANewsShower
                        video={true}
                        onPress={() =>
                          navigation.navigate('VideoScreen', {
                            video: item,
                            // category: category,
                          })
                        }
                        publishDate={item.CreatedOn}
                        headerText={item.MainBranch || item.Branch || null}
                        explainedText={sentenceShortener(item.Title, 55)}
                        content={sentenceShortener(item.Description, 150)}
                        image={item.ImageUrl}
                        language={UserState?.Language || 'tr'}
                      />
                    </View>
                  </>
                );
              })}
            </ScrollView>
          </View>
        </>
      )}
    </ThemeContext.Consumer>
  );
};

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(VideoGalleryScreen);
