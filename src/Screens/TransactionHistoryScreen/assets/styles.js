import {StyleSheet} from 'react-native';
import {White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: White},
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
