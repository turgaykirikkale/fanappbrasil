import FATransactionHistoryList from '@Components/Composite/FATransactionHistoryList';
import {Orange} from '@Commons/FAColor';
import {styles} from './assets/styles';
import FAService from '@Services';
import React from 'react';
import _ from 'lodash';
import {ActivityIndicator, View} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';

class TransactionHistoryScreen extends React.Component {
  constructor(props) {
    super(props);
    const {route} = props;
    this.state = {
      data:
        route && route.params && route.params.data
          ? route.params.data
          : {CoinId: 29, CurrencyId: 1},
      coinBalanceDetail:
        route && route.params && route.params.data
          ? route.params.coinBalanceDetail
          : {},
      customerTransactionHistoryList: [],
      customerTransactionHistoryLoading: false,
      totalListLength: -1,
      currentPage: 1,
      onReachLoading: false,
    };
  }

  componentWillMount() {
    this.fillCustomerTransactionHistoryList();
  }

  onEndReached() {
    this.fillCustomerTransactionHistoryList(true);
  }

  fillCustomerTransactionHistoryList(endReached = false) {
    const {data, customerTransactionHistoryList, currentPage, totalListLength} =
      this.state;
    if (!_.isNil(data)) {
      const requestBody = {
        PagerDto: {Page: currentPage, Size: 25, Total: 0},
        coinId: data.CoinId,
        currencyId: data.CurrencyId,
      };
      if (!endReached) {
        this.setState({
          customerTransactionHistoryLoading: true,
        });
      } else if (
        endReached &&
        totalListLength !== customerTransactionHistoryList.length
      ) {
        this.setState({onReachLoading: true});
      }

      FAService.GetCustomerTransactionHistory(requestBody)
        .then(response => {
          if (response && response.data && response.status === 200) {
            const responseData = response.data;
            const responsePager = responseData.PagerDto;
            const totalData = responsePager.Total;
            let transactionHistoryList = customerTransactionHistoryList;
            if (
              responseData.Orders &&
              responsePager.PageCount >= currentPage &&
              totalListLength !== transactionHistoryList.length
            ) {
              transactionHistoryList = _.concat(
                customerTransactionHistoryList,
                responseData.Orders,
              );
            }
            this.setState({
              customerTransactionHistoryList: transactionHistoryList,
              totalListLength: totalData,
              currentPage:
                responsePager.PageCount > currentPage
                  ? currentPage + 1
                  : currentPage,
            });
          }
          if (!endReached) {
            this.setState({customerTransactionHistoryLoading: false});
          } else {
            this.setState({onReachLoading: false});
          }
        })
        .catch(error => {
          if (!endReached) {
            this.setState({customerTransactionHistoryLoading: false});
          } else {
            this.setState({onReachLoading: false});
          }
        });
    }
  }

  render() {
    const {
      customerTransactionHistoryList,
      customerTransactionHistoryLoading,
      onReachLoading,
      coinBalanceDetail,
    } = this.state;
    const {navigation} = this.props;

    return (
      <View style={styles.mainContainer}>
        <FAStandardHeader
          title={Localization.t('StockStandartScreen.TransactionHistory')}
          navigation={navigation}
        />
        {customerTransactionHistoryLoading ? (
          <View style={styles.loaderContainer}>
            <ActivityIndicator size={'large'} color={Orange} />
          </View>
        ) : (
          <FATransactionHistoryList
            coinBalanceDetail={coinBalanceDetail}
            dataLoading={onReachLoading}
            onEndReached={() => this.onEndReached()}
            data={customerTransactionHistoryList}
          />
        )}
      </View>
    );
  }
}

export default TransactionHistoryScreen;
