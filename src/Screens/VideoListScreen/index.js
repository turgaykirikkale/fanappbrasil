import React, {useEffect, useState} from 'react';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import {FlatList, RefreshControl, View} from 'react-native';
import _ from 'lodash';
import FAFanCard from '../../Components/UI/FAFanCard';
import FAService from '@Services';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const VideoListScreen = props => {
  const {navigation} = props;
  const incomingCategory = props?.route?.params?.category;

  const [videoList, setVideoList] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fillVideosByCategory();
  }, [incomingCategory]);

  const onRefresh = () => {
    fillVideosByCategory(true);
  };

  const fillVideosByCategory = (onRefresh = false) => {
    if (onRefresh) {
      setRefreshing(true);
    }
    FAService.GetMainPageVideosByCategory(incomingCategory.CategoryId)
      .then(response => {
        if (response?.data) {
          const resData = response.data;
          const filteredData = _.filter(resData, item => item.Title !== null);
          setVideoList(filteredData);
        }
        if (onRefresh) {
          setRefreshing(false);
        }
      })
      .catch(error => console.log('error', error));
  };
  return (
    <>
      <FAStandardHeader title={` Video Listesi`} navigation={navigation} />
      <ThemeContext.Consumer>
        {({theme}) => (
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            numColumns={3}
            data={videoList}
            style={{backgroundColor: theme?.colors?.blueGray}}
            renderItem={({item, index}) => {
              return (
                <View style={{marginVertical: 6}}>
                  <FAFanCard
                    cardRatio={3}
                    image={item.ImageUrl}
                    onPress={() => {
                      navigation.navigate('VideoScreen', {
                        video: item,
                        category: incomingCategory,
                      });
                    }}
                    text={item.Title}
                    horizontalSpace={4}
                  />
                </View>
              );
            }}
          />
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default VideoListScreen;
