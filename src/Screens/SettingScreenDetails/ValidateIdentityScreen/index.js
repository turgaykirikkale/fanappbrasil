import React, {Component} from 'react';
import {View, Text} from 'react-native';
import FAStandardHeader from '../../../Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';
import FATextInput from '../../../Components/Composite/FATextInput';
import autobind from 'autobind-decorator';
import FAService from '../../../Services';
import FARegisterSelectableModal from '../../../Components/Composite/FARegisterSelectableModal';
import FAPickerComponent from '../../../Components/Composite/FAPickerComponent';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import FADatePicker from '@Components/Composite/FADatePicker';
import FACheckbox from '@Components/Composite/FACheckbox';
import FAEnums from '../../../Commons/FAEnums';
import FAButton from '../../../Components/Composite/FAButton';
import {FAOrange} from '../../../Commons/FAColor';
import FAAlertModal from '../../../Components/Composite/FAAlertModal';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Toast from 'react-native-toast-message';
import _ from 'lodash';
import {connect} from 'react-redux';

class ValidateIdentity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userIdentityInformation: {
        customerId: null,
        motherName: null,
        fatherName: null,
        birthdate: null,
        birtCountry: 30,
        nationality: null,
        birthCity: null,
        birthDistrict: null,
        cbfNO: null,
        gender: null,
        address: null,
        zipCode: null,
      },
      genderFlowType: null,
      nationalityPlaceHolder: 'Nationality',
      showNationalityModal: false,
      selectedNationality: null,
      countryList: [],
      countryPlaceHolder: 'Country Of Residence',
      selectedCountry: null,
      showCountryModal: false,
      cityPlaceHolder: 'State of Residence',
      cityList: [],
      selectedCity: null,
      showCityModal: false,
      districtPlaceHolder: 'City of Residence',
      districtList: [],
      selectedDistrict: null,
      showDistrictModal: false,
      residencePermitStartDate: null,
      isLoading: false,
      showErrorMessage: false,
      errorMessage: null,
    };
  }
  componentWillMount() {
    this.getCountryList();
  }
  @autobind
  getCountryList() {
    FAService.GetCountryList()
      .then(res => {
        if (res.data && res.status === 200) {
          this.setState({
            countryList: res.data,
          });
        }
      })
      .catch(err => {
        console.log('errCountryList', err);
      });
  }
  @autobind
  getCityList(value) {
    const birtCountry = value.Id;

    FAService.GetForeignCityList(birtCountry)
      .then(res => {
        console.log('errCityList', res);
        if (res.data && res.status === 200) {
          this.setState({
            cityList: res.data,
          });
        }
      })
      .catch(err => {
        console.log('errCityList', err);
      });
  }
  @autobind
  controlSelectedCountry(value) {
    const {userIdentityInformation} = this.state;
    console.log(value);
    this.setState({
      userIdentityInformation: {
        ...userIdentityInformation,
        birtCountry: value.Id,
      },
      showCountryModal: false,
      countryPlaceHolder: value.Name,
      selectedCountry: value,
    });

    this.getCityList(value);
  }
  @autobind
  controlSelectedCity(value) {
    const {selectedCountry} = this.state;
    const {userIdentityInformation} = this.state;
    const cityId = value.Id;
    this.setState(
      {
        userIdentityInformation: {
          ...userIdentityInformation,
          birthCity: value.Id,
        },
        showCityModal: false,
        cityPlaceHolder: value.Name,
        selectedCity: value,
      },

      selectedCountry.Id === 223
        ? () =>
            FAService.GetDistrictList(cityId)
              .then(res => {
                if (res.data && res.status === 200) {
                  console.log(res.data);
                  this.setState({
                    districtList: res.data,
                  });
                }
              })
              .catch(err => {
                console.log('districtListERR', err);
              })
        : () =>
            FAService.GetForeignDistrictList(cityId)
              .then(res => {
                if (res.data && res.status === 200) {
                  console.log(res.data);
                  this.setState({
                    districtList: res.data,
                  });
                }
              })
              .catch(err => {
                console.log('districtListERR', err);
              }),
    );
  }
  @autobind
  controlSelectedDistrict(value) {
    const {userIdentityInformation} = this.state;

    this.setState({
      userIdentityInformation: {
        ...userIdentityInformation,
        birthDistrict: value.Id,
      },
      showDistrictModal: false,
      districtPlaceHolder: value.Name,
      selectedDistrict: value,
    });
  }
  @autobind
  controlIdentityModal(value) {
    const {userIdentityInformation} = this.state;

    this.setState({
      userIdentityInformation: {
        ...userIdentityInformation,
        identityType: value.Id,
      },
      showIdentityModal: false,
      identityPlaceHolder: value.Name,
    });
  }
  @autobind
  editableControl() {
    const {showCountryModal, showIdentityModal} = this.state;
    return showCountryModal || showIdentityModal;
  }

  @autobind
  valdiateServiceCall() {
    const {navigation} = this.props;
    const {userIdentityInformation} = this.state;
    const UserInfo = this?.props?.UserState?.UserInfo;

    let requestBody = {
      // Address: null,
      // BirthCityId: 80,
      // BirthDate: '1991-04-01',
      // BirthDistrictId: 962,
      // birtCountry: 223,
      // FatherName: 'FARUK',
      // Gender: 0,
      // cbfNO: '37685274069',
      // IdentitySerialNumber: 'U13101305',
      // IdentityTypeEnumId: 2,
      // IdentityValidDate: null,
      // LanguageCode: 'tr',
      // MotherName: 'MAHMUT',
      // ResidenceCityId: null,
      // ResidenceDistrictId: null,
      // ResidencePermitEndDate: null,
      // ResidencePermitStartDate: null,
      // ZipCode: null,
    };
    let birthDate = userIdentityInformation.birthdate;
    birthDate = birthDate.split('-').reverse().join('-') || null;

    requestBody.Address = userIdentityInformation.address;
    requestBody.BirthCityId = null;
    requestBody.BirthDate = birthDate;
    requestBody.BirthDistrictId = null;
    requestBody.CountryId = userIdentityInformation.nationality;
    requestBody.FatherName = userIdentityInformation.fatherName;
    requestBody.Gender = userIdentityInformation.gender;
    requestBody.IdentityNumber = userIdentityInformation.cbfNO;
    requestBody.IdentitySerialNumber = null;
    requestBody.IdentityTypeEnumId = 2;
    requestBody.IdentityValidDate = null;
    requestBody.MotherName = userIdentityInformation.motherName;
    requestBody.Name = UserInfo.Name;
    requestBody.ResidenceCityId = userIdentityInformation.birthCity;
    requestBody.ResidenceDistrictId = userIdentityInformation.birthDistrict;
    requestBody.ResidencePermitStartDate = null;
    requestBody.ResidencePermitEndDate = null;
    requestBody.Surname = UserInfo.Surname;
    requestBody.ZipCode = userIdentityInformation.zipCode;

    console.log(requestBody, 'reqBody');
    this.setState({
      isLoading: true,
    });
    FAService.GetValidateEntity(requestBody)
      .then(res => {
        console.log('responFORIDenty', res);
        if (res?.data?.IsSuccess) {
          this.setState(
            {
              isLoading: false,
            },
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('Commons.Succes'),
              text2: res.data.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => navigation.navigate('SettingsScreen'),
              onPress: () => {},
            }),
          );
          console.log('Success', res);
        } else {
          console.log('Success', res);
          this.setState({
            showErrorMessage: true,
            errorMessage: res.data.Message,
            isLoading: false,
          });
        }
      })
      .catch(err => {
        console.log(err, 'err');
        this.setState({
          showErrorMessage: true,
          errorMessage: Localization.t('Commons.UnexpectedError'),
          isLoading: false,
        });
      });
  }

  @autobind
  isReadyForServiceCall() {
    const {
      motherName,
      fatherName,
      identityType,
      birthdate,
      birtCountry,
      nationality,
      birthCity,
      birthDistrict,
      cbfNO,
      identitySerialNumber,
      identityOldSerial,
      identityOldNo,
      gender,
      address,
      zipCode,
    } = this.state.userIdentityInformation;
    console.log(
      !_.isEmpty(motherName),
      !_.isEmpty(fatherName),
      !_.isEmpty(birthdate),
      !_.isNull(birtCountry),
      !_.isNull(birthCity),
      !_.isNull(birthDistrict),
      !_.isEmpty(cbfNO),
      !_.isEmpty(identitySerialNumber),
      !_.isNull(address),
      !_.isNull(zipCode),
      !_.isNull(gender),
    );

    console.log(this.state.userIdentityInformation);

    if (
      !_.isEmpty(motherName) &&
      !_.isEmpty(fatherName) &&
      !_.isEmpty(birthdate) &&
      !_.isNull(birtCountry) &&
      !_.isNull(nationality) &&
      !_.isNull(birthCity) &&
      !_.isNull(birthDistrict) &&
      !_.isEmpty(cbfNO) &&
      !_.isNull(address) &&
      !_.isNull(zipCode) &&
      !_.isNull(gender)
    ) {
      return false;
    }
    return true;
  }

  render() {
    const {navigation} = this.props;
    const {
      userIdentityInformation,
      countryList,
      showCountryModal,
      selectedCountry,
      countryPlaceHolder,
      cityPlaceHolder,
      showCityModal,
      selectedCity,
      cityList,
      districtPlaceHolder,
      showDistrictModal,
      districtList,
      selectedDistrict,
      genderFlowType,
      showErrorMessage,
      errorMessage,
      isLoading,
      showNationalityModal,
      nationalityPlaceHolder,
      selectedNationality,
    } = this.state;

    const countryListFilteredList = _.filter(countryList, {HasState: true});

    const UserInfo = this?.props?.UserState?.UserInfo;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
            <FAFullScreenLoader isLoading={isLoading} />

            <FAAlertModal
              visible={showErrorMessage}
              text={errorMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showErrorMessage: false})}
            />

            {showCountryModal ? (
              <FARegisterSelectableModal
                data={countryListFilteredList}
                closeModal={() => this.setState({showCountryModal: false})}
                controlSelectedItem={value =>
                  this.controlSelectedCountry(value)
                }
              />
            ) : null}
            {showNationalityModal ? (
              <FARegisterSelectableModal
                data={countryList}
                closeModal={() => this.setState({showNationalityModal: false})}
                controlSelectedItem={value =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      nationality: value.Id,
                    },
                    selectedNationality: value,
                    showNationalityModal: false,
                    nationalityPlaceHolder: value.Name,
                  })
                }
              />
            ) : null}
            {showCityModal ? (
              <FARegisterSelectableModal
                data={cityList}
                closeModal={() => this.setState({showCityModal: false})}
                controlSelectedItem={value => this.controlSelectedCity(value)}
              />
            ) : null}
            {showDistrictModal ? (
              <FARegisterSelectableModal
                data={districtList}
                closeModal={() => this.setState({showDistrictModal: false})}
                controlSelectedItem={value =>
                  this.controlSelectedDistrict(value)
                }
              />
            ) : null}

            <FAStandardHeader
              title={Localization.t('Validate Identity')}
              navigation={navigation}
            />
            <KeyboardAwareScrollView
              showsVerticalScrollIndicator={false}
              style={{marginHorizontal: 10, marginTop: 10}}>
              <View style={{marginTop: 10}} />
              <FATextInput
                label={Localization.t('IdentityEnterScreen.name')}
                name={'Name'}
                placeholder={UserInfo.Name || null}
                disable={true}
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                label={Localization.t('IdentityEnterScreen.surname')}
                name={'surname'}
                placeholder={UserInfo.Surname || null}
                disable={true}
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                disable={this.editableControl()}
                rule={'notEmptyAndNil'}
                errorMessage={'Boş bırakalamaz.'}
                placeholder={'Mother Name'}
                label={'Mother Name'}
                onChangeValue={newValue =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      motherName: newValue,
                    },
                  })
                }
                value={userIdentityInformation.motherName}
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                disable={this.editableControl()}
                rule={'notEmptyAndNil'}
                errorMessage={'Boş bırakılamaz.'}
                placeholder={'Father Name'}
                label={'Father Name'}
                onChangeValue={newValue =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      fatherName: newValue,
                    },
                  })
                }
                value={userIdentityInformation.fatherName}
              />

              <View style={{marginTop: 10}} />
              <FAPickerComponent
                colorControl={selectedNationality !== null}
                label={'Nationality'}
                placeholder={nationalityPlaceHolder}
                onPress={() =>
                  this.setState({
                    showNationalityModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                keyboardType={'numeric'}
                disable={this.editableControl()}
                rule={'notEmptyAndNil'}
                errorMessage={'Boş bırakılamaz.'}
                placeholder={'CBF No'}
                label={'CBF No'}
                onChangeValue={newValue =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      cbfNO: newValue,
                    },
                  })
                }
                value={userIdentityInformation.cbfNO}
              />

              <>
                <View style={{marginTop: 10}} />
                <FAPickerComponent
                  colorControl={selectedCountry !== null}
                  label={'Country of Residence'}
                  placeholder={countryPlaceHolder}
                  onPress={() =>
                    this.setState({
                      showCountryModal: true,
                    })
                  }
                />
                <View style={{marginTop: 10}} />
                <FAPickerComponent
                  colorControl={selectedCity !== null}
                  label={'State of Residence'}
                  placeholder={cityPlaceHolder}
                  onPress={() =>
                    this.setState({
                      showCityModal: true,
                    })
                  }
                />
                <View style={{marginTop: 10}} />
                <FAPickerComponent
                  disabled={selectedCity === null ? true : false}
                  colorControl={selectedDistrict !== null}
                  label={'City of Residence'}
                  placeholder={districtPlaceHolder}
                  onPress={() =>
                    this.setState({
                      showDistrictModal: true,
                    })
                  }
                />
              </>

              <View style={{marginTop: 10}} />
              <FADatePicker
                minumumDate={new Date(moment().subtract(72, 'years').toDate())}
                maximumDate={new Date()}
                birthDate
                label={Localization.t(
                  'IdentityEnterScreen.ChooseBirthDateText',
                )}
                rule={'notEmptyAndNil'}
                value={userIdentityInformation.birthdate}
                onChange={value =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      birthdate: value,
                    },
                  })
                }
                placeHolder={Localization.t(
                  'IdentityEnterScreen.ChooseBirthDateText',
                )}
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                disable={this.editableControl()}
                rule={'notEmptyAndNil'}
                errorMessage={'Boş bırakılamaz.'}
                placeholder={'Adress'}
                label={'Adress'}
                onChangeValue={newValue =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      address: newValue,
                    },
                  })
                }
                value={userIdentityInformation.address}
              />
              <View style={{marginTop: 10}} />
              <FATextInput
                disable={this.editableControl()}
                rule={'notEmptyAndNil'}
                errorMessage={'Boş bırakılamaz.'}
                placeholder={'Post Code'}
                label={'Post Code'}
                onChangeValue={newValue =>
                  this.setState({
                    userIdentityInformation: {
                      ...userIdentityInformation,
                      zipCode: newValue,
                    },
                  })
                }
                value={userIdentityInformation.zipCode}
              />
              <View style={{marginTop: 10}} />

              <Text style={{color: theme?.colors?.darkBlue, marginBottom: 8}}>
                {Localization.t('IdentityEnterScreen.Gender')}
              </Text>
              <View style={{flexDirection: 'row'}} rule={'notEmptyAndNil'}>
                <FACheckbox.Circle
                  rule={'notEmptyAndNil'}
                  checked={genderFlowType === FAEnums.WOMAN ? true : false}
                  onPress={() =>
                    this.setState({
                      genderFlowType: FAEnums.WOMAN,
                      userIdentityInformation: {
                        ...userIdentityInformation,
                        gender: FAEnums.WOMAN,
                      },
                    })
                  }
                  label={Localization.t('IdentityEnterScreen.Woman')}
                />
                <View style={{marginLeft: 10}} />

                <FACheckbox.Circle
                  checked={genderFlowType === FAEnums.MAN ? true : false}
                  onPress={() =>
                    this.setState({
                      genderFlowType: FAEnums.MAN,
                      userIdentityInformation: {
                        ...userIdentityInformation,
                        gender: FAEnums.MAN,
                      },
                    })
                  }
                  label={Localization.t('IdentityEnterScreen.Man')}
                />
              </View>

              <View style={{marginTop: 10}} />
              <FAButton
                disabled={this.isReadyForServiceCall()}
                onPress={() => this.valdiateServiceCall()}
                containerStyle={{backgroundColor: FAOrange, borderRadius: 4}}
                textStyle={{color: 'white', marginVertical: 14}}
                text={Localization.t(
                  'IdentityEnterScreen.SentMyInformationButtonText',
                )}
              />
              <View style={{marginTop: 20}} />
            </KeyboardAwareScrollView>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, null)(ValidateIdentity);
