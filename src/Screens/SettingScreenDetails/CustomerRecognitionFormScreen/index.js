import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../../Components/Composite/FAStandardHeader';
import FAService from '../../../Services';
import {connect} from 'react-redux';
import {StatusControl} from '../../../Commons/CustomerRecognitionFormEnumsControl/index';
import _, {rest} from 'lodash';
import FARegisterSelectableModal from '../../../Components/Composite/FARegisterSelectableModal';
import FAPickerComponent from '../../../Components/Composite/FAPickerComponent';
import FACheckbox from '@Components/Composite/FACheckbox';
import FATextInput from '../../../Components/Composite/FATextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FADatePicker from '../../../Components/Composite/FADatePicker';
import moment from 'moment';
import autobind from 'autobind-decorator';
import FAButton from '../../../Components/Composite/FAButton';
import {FAOrange} from '../../../Commons/FAColor';
import FAForm from '@ValidationEngine/Form';
import Localization from '@Localization';

export default class CustomerRecognitionFormScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enumsList: null,
      educationalStatusList: null,
      stapleEarnerList: null,
      monetaryIncomeList: null,
      jobsList: null,
      monthlyEarningList: null,
      totalAssetsList: null,
      investmentSourceList: null,
      riskEnumList: null,
      invesmentPurposeList: null,
      showEducationalModal: false,
      userEducationID: null,
      userEducationStatus: Localization.t(
        'CustomerRecognitionFormFix.EducationalStatus',
      ),
      showRiskModal: false,
      userRiskID: null,
      userRiskStatus: Localization.t(
        'CustomerRecognitionFormFix.RiskPreference',
      ),
      showJobsModal: false,
      userJobID: null,
      userJobsStatus: Localization.t(
        'CustomerRecognitionFormFix.YourBusinessArea',
      ),
      showMonthlyEarningModal: false,
      userMonthlyEarnID: null,
      userMonthlyEarnStatus: Localization.t(
        'CustomerRecognitionFormFix.MonthlyIncome',
      ),
      showStapleEarnerModal: false,
      userStapleEarnerID: null,
      userStapleEarnerStatus: Localization.t(
        'CustomerRecognitionFormFix.YourIncomeType',
      ),
      showTotalAssetsModal: false,
      userTotalAssetsID: null,
      userTotalAssetsStatus: Localization.t(
        'CustomerRecognitionFormFix.YourTotalAmountofTangibleAssets',
      ),
      showInvestmentSourceModal: false,
      userInvesmentSourceID: null,
      userInvesmentSourceStatus: Localization.t(
        'CustomerRecognitionFormFix.YourInvestmentSource',
      ),
      showInvestmentPurposeModal: false,
      userInvesmentPurposeID: null,
      userInvesmentPurposeStatus: Localization.t(
        'CustomerRecognitionFormFix.YourTransactionPreference',
      ),
      showMonetaryIncomeModal: false,
      userMonetaryIncomeID: null,
      userMonetaryIncomeStatus: Localization.t(
        'CustomerRecognitionFormFix.YourIncomeType',
      ),
      ownBehalf: false,
      otherBehalf: false,
      otherBehalfName: null,
      otherBehalfSurname: null,
      otherBehalfIdentityNumber: null,
      otherBehalfFatherName: null,
      otherBehalfMotherName: null,
      otherBehalfBirthdateArea: null,
      otherBehalfBirthdate: null,
      otherBehalfEmail: null,
      phoneCode:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.code
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.countryCode
          : '+90',
      flag:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.flag
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.flag
          : 'Turkey',
      purePhoneNumber: '',
      fullPhoneNumber: '',
      formattedValue: '',
      otherBehalfAdress: null,
      FormIsValid: false,
    };
  }

  @autobind
  serviceCall() {
    const {
      userEducationID,
      userRiskID,
      userJobID,
      userMonthlyEarnID,
      userStapleEarnerID,
      userTotalAssetsID,
      userInvesmentSourceID,
      userInvesmentPurposeID,
      userMonetaryIncomeID,
      otherBehalf,
    } = this.state;

    let requestBody = {};

    requestBody.HasBeneficiaryDeclaration = otherBehalf;
    requestBody.EducationalStatusEnumId = userEducationID;
    requestBody.RiskEnumId = userRiskID;
    requestBody.MonthlyEarningEnumId = userMonthlyEarnID;
    requestBody.JobEnumId = userJobID;
    requestBody.StapleEarnerEnumId = userStapleEarnerID;
    requestBody.TotalAssetsEnumId = userTotalAssetsID;
    requestBody.InvestmentSourceEnumId = userInvesmentSourceID;
    requestBody.InvesmentPurposeEnumId = userInvesmentPurposeID;
    requestBody.MonetaryIncomeEnumId = userMonetaryIncomeID;

    FAService.SetCustomerIdentificationForm(requestBody)
      .then(res => {
        console.log('rrres', res);
      })
      .catch(err => {
        console.log('errrrr,', err);
      });
  }

  componentWillMount() {
    this.EducationalStatus();
    this.GetStapleEarner();
    this.GetMonetaryIncome();
    this.GetJobs();
    this.GetMonthlyEarning();
    this.GetTotalAssets();
    this.GetIvestmentSource();
    this.GetRiskEnum();
    this.GetInvensmentPurpose();
    this.GetContentsForEnums();
  }
  GetContentsForEnums() {
    FAService.contents()
      .then(res => {
        if (res?.data && res?.data?.length > 0) {
          console.log('contents', res);
          let enumsForm = _.filter(res.data, {Resource: 'Enum'});
          const entries = Object.entries(enumsForm[0].Contents);
          this.setState({
            enumsList: entries,
          });
        } else {
          console.log('ContentSElseError');
        }
      })
      .catch(err => {
        console.log('contents', err);
      });
  }
  EducationalStatus() {
    FAService.GetEducationalStatus()
      .then(res => {
        console.log('GetEducationalStatus', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            educationalStatusList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetEducationalStatus', err);
      });
  }
  GetStapleEarner() {
    FAService.GetStapleEarner()
      .then(res => {
        if (res?.data) {
          console.log('GetStapleEarner', res);
          const entries = Object.entries(res.data);
          this.setState({
            stapleEarnerList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetStapleEarner', err);
      });
  }
  GetMonetaryIncome() {
    FAService.GetMonetaryIncome()
      .then(res => {
        console.log('GetMonetaryIncome', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            monetaryIncomeList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetMonetaryIncome', err);
      });
  }
  GetJobs() {
    FAService.GetJobs()
      .then(res => {
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            jobsList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetJobs', err);
      });
  }
  GetMonthlyEarning() {
    FAService.GetMonthlyEarning()
      .then(res => {
        console.log('GetMonthlyEarning', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            monthlyEarningList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetMonthlyEarning', err);
      });
  }
  GetTotalAssets() {
    FAService.GetTotalAssets()
      .then(res => {
        const entries = Object.entries(res.data);
        console.log('GetTotalAssets', res);
        if (res?.data) {
          this.setState({
            totalAssetsList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetTotalAssets', err);
      });
  }
  GetIvestmentSource() {
    FAService.GetIvestmentSource()
      .then(res => {
        console.log('GetIvestmentSource', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            investmentSourceList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetIvestmentSource', err);
      });
  }
  GetRiskEnum() {
    FAService.GetRiskEnum()
      .then(res => {
        console.log('GetRiskEnum', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            riskEnumList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetRiskEnum', err);
      });
  }
  GetInvensmentPurpose() {
    FAService.GetInvensmentPurpose()
      .then(res => {
        console.log('GetInvensmentPurpose', res);
        if (res?.data) {
          const entries = Object.entries(res.data);
          this.setState({
            invesmentPurposeList: entries,
          });
        }
      })
      .catch(err => {
        console.log('GetInvensmentPurpose', err);
      });
  }
  disabledControl() {
    const {
      showInvestmentSourceModal,
      showEducationalModal,
      showRiskModal,
      showJobsModal,
      showMonthlyEarningModal,
      showStapleEarnerModal,
      showInvestmentPurposeModal,
      showMonetaryIncomeModal,
      showTotalAssetsModal,
    } = this.state;

    if (
      showInvestmentPurposeModal ||
      showStapleEarnerModal ||
      showInvestmentSourceModal ||
      showEducationalModal ||
      showRiskModal ||
      showJobsModal ||
      showMonthlyEarningModal ||
      showMonetaryIncomeModal ||
      showTotalAssetsModal
    ) {
      return true;
    }
    return false;
  }
  @autobind
  navigate() {
    this.props.navigation.navigate('CountrySelectScreen', {
      onPhoneSelect: this.onPhoneSelect,
    });
  }
  @autobind
  onPhoneSelect({code, flag}) {
    this.setState({
      phoneCode: code,
      flag: flag,
    });
  }

  @autobind
  onChangePhoneNumber(formatted, extracted) {
    const newPhonePrefix = this.state.phoneCode;
    let generatedPhoneNumberValue = extracted;
    if (extracted.indexOf(newPhonePrefix) > -1) {
      generatedPhoneNumberValue = extracted.replace(newPhonePrefix, '');
    }
    this.setState({
      purePhoneNumber: generatedPhoneNumberValue,
      fullPhoneNumber: extracted,
      phoneCode: newPhonePrefix,
      formattedValue: formatted,
    });
  }

  render() {
    const {navigation} = this.props;
    const {
      educationalStatusList,
      stapleEarnerList,
      monetaryIncomeList,
      jobsList,
      monthlyEarningList,
      enumsList,
      totalAssetsList,
      investmentSourceList,
      riskEnumList,
      invesmentPurposeList,
      showEducationalModal,
      userEducationID,
      userEducationStatus,
      showRiskModal,
      userRiskID,
      userRiskStatus,
      showJobsModal,
      userJobID,
      userJobsStatus,
      showMonthlyEarningModal,
      userMonthlyEarnID,
      userMonthlyEarnStatus,
      showStapleEarnerModal,
      userStapleEarnerID,
      userStapleEarnerStatus,
      showTotalAssetsModal,
      userTotalAssetsID,
      userTotalAssetsStatus,
      showInvestmentSourceModal,
      userInvesmentSourceID,
      userInvesmentSourceStatus,
      showInvestmentPurposeModal,
      userInvesmentPurposeID,
      userInvesmentPurposeStatus,
      showMonetaryIncomeModal,
      userMonetaryIncomeID,
      userMonetaryIncomeStatus,
      ownBehalf,
      otherBehalf,
      otherBehalfName,
      otherBehalfSurname,
      otherBehalfIdentityNumber,
      otherBehalfFatherName,
      otherBehalfMotherName,
      otherBehalfBirthdateArea,
      otherBehalfBirthdate,
      otherBehalfEmail,
      phoneCode,

      fullPhoneNumber,
      formattedValue,
      otherBehalfAdress,
      flag,
    } = this.state;

    const educationalStatusFixList = StatusControl(
      enumsList,
      educationalStatusList,
      'EducationalStatusEnum',
    );

    const stapleEarnerFixedList = StatusControl(
      enumsList,
      stapleEarnerList,
      'StapleEarnerEnum',
    );

    const monetaryIncomeFixedList = StatusControl(
      enumsList,
      monetaryIncomeList,
      'MonetaryIncomeEnum',
    );

    const jobFixedList = StatusControl(enumsList, jobsList, 'JobEnum');

    const monthlyEarningFixedList = StatusControl(
      enumsList,
      monthlyEarningList,
      'MonthlyEarningEnum',
    );

    const totalAssetsFixedList = StatusControl(
      enumsList,
      totalAssetsList,
      'TotalAssetsEnum',
    );
    const investmentSourceFixedList = StatusControl(
      enumsList,
      investmentSourceList,
      'InvestmentSourceEnum',
    );

    const riskEnumFixedList = StatusControl(
      enumsList,
      riskEnumList,
      'RiskEnum',
    );

    invesmentPurposeList;
    const invesmentPurposeFixedList = StatusControl(
      enumsList,
      invesmentPurposeList,
      'InvestmentPurposeEnum',
    );

    console.log(monetaryIncomeFixedList, 'monetaryIncomeFixedList');
    console.log(otherBehalfName);

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
            <FAStandardHeader
              title={Localization.t('SettingScreenFix.AccountSetting')}
              navigation={navigation}
            />
            <View
              style={{
                marginHorizontal: 10,
                backgroundColor: theme?.colors?.darkWhite,
                paddingHorizontal: 10,
                paddingVertical: 10,
                marginTop: 6,
                borderRadius: 4,
              }}>
              <Text style={{color: theme?.colors?.black}}>
                {Localization.t('CustomerRecognitionFormFix.AnswerQuestions')}
              </Text>
            </View>
            {showEducationalModal ? (
              <FARegisterSelectableModal
                data={educationalStatusFixList}
                closeModal={() => this.setState({showEducationalModal: false})}
                controlSelectedItem={item =>
                  this.setState({
                    showEducationalModal: false,
                    userEducationID: item.Id,
                    userEducationStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showStapleEarnerModal ? (
              <FARegisterSelectableModal
                data={stapleEarnerFixedList}
                closeModal={() => this.setState({showStapleEarnerModal: false})}
                controlSelectedItem={item =>
                  this.setState({
                    showStapleEarnerModal: false,
                    userStapleEarnerID: item.Id,
                    userStapleEarnerStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showRiskModal ? (
              <FARegisterSelectableModal
                data={riskEnumFixedList}
                closeModal={() => this.setState({showRiskModal: false})}
                controlSelectedItem={item =>
                  this.setState({
                    showRiskModal: false,
                    userRiskID: item.Id,
                    userRiskStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showJobsModal ? (
              <FARegisterSelectableModal
                data={jobFixedList}
                closeModal={() => this.setState({showJobsModal: false})}
                controlSelectedItem={item =>
                  this.setState({
                    showJobsModal: false,
                    userJobID: item.Id,
                    userJobsStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showMonthlyEarningModal ? (
              <FARegisterSelectableModal
                data={monthlyEarningFixedList}
                closeModal={() =>
                  this.setState({showMonthlyEarningModal: false})
                }
                controlSelectedItem={item =>
                  this.setState({
                    showMonthlyEarningModal: false,
                    userMonthlyEarnID: item.Id,
                    userMonthlyEarnStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showTotalAssetsModal ? (
              <FARegisterSelectableModal
                data={totalAssetsFixedList}
                closeModal={() => this.setState({showTotalAssetsModal: false})}
                controlSelectedItem={item =>
                  this.setState({
                    showTotalAssetsModal: false,
                    userTotalAssetsID: item.Id,
                    userTotalAssetsStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showInvestmentSourceModal ? (
              <FARegisterSelectableModal
                data={investmentSourceFixedList}
                closeModal={() =>
                  this.setState({showInvestmentSourceModal: false})
                }
                controlSelectedItem={item =>
                  this.setState({
                    showInvestmentSourceModal: false,
                    userInvesmentSourceID: item.Id,
                    userInvesmentSourceStatus: item.Name,
                  })
                }
              />
            ) : null}

            {showInvestmentPurposeModal ? (
              <FARegisterSelectableModal
                data={invesmentPurposeFixedList}
                closeModal={() =>
                  this.setState({showInvestmentPurposeModal: false})
                }
                controlSelectedItem={item =>
                  this.setState({
                    showInvestmentPurposeModal: false,
                    userInvesmentPurposeID: item.Id,
                    userInvesmentPurposeStatus: item.Name,
                  })
                }
              />
            ) : null}
            {showMonetaryIncomeModal ? (
              <FARegisterSelectableModal
                data={monetaryIncomeFixedList}
                closeModal={() =>
                  this.setState({showMonetaryIncomeModal: false})
                }
                controlSelectedItem={item =>
                  this.setState({
                    showMonetaryIncomeModal: false,
                    userMonetaryIncomeID: item.Id,
                    userMonetaryIncomeStatus: item.Name,
                  })
                }
              />
            ) : null}
            <KeyboardAwareScrollView
              style={{marginHorizontal: 10, marginTop: 10}}
              showsVerticalScrollIndicator={false}>
              <FAPickerComponent
                disabled={this.disabledControl()}
                colorControl={userEducationID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.EducationalStatus',
                )}
                placeholder={userEducationStatus}
                onPress={() =>
                  this.setState({
                    showEducationalModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userMonetaryIncomeID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourIncomeType',
                )}
                placeholder={userMonetaryIncomeStatus}
                onPress={() =>
                  this.setState({
                    showMonetaryIncomeModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userStapleEarnerID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourSourceofIncome',
                )}
                placeholder={userStapleEarnerStatus}
                onPress={() =>
                  this.setState({
                    showStapleEarnerModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userJobID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourBusinessArea',
                )}
                placeholder={userJobsStatus}
                onPress={() =>
                  this.setState({
                    showJobsModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userMonthlyEarnID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.MonthlyIncome',
                )}
                placeholder={userMonthlyEarnStatus}
                onPress={() =>
                  this.setState({
                    showMonthlyEarningModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userTotalAssetsID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourTotalAmountofTangibleAssets',
                )}
                placeholder={userTotalAssetsStatus}
                onPress={() =>
                  this.setState({
                    showTotalAssetsModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userInvesmentSourceID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourInvestmentSource',
                )}
                placeholder={userInvesmentSourceStatus}
                onPress={() =>
                  this.setState({
                    showInvestmentSourceModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userRiskID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.RiskPreference',
                )}
                placeholder={userRiskStatus}
                onPress={() =>
                  this.setState({
                    showRiskModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                // disabled={selectedCity === null ? true : false}
                colorControl={userInvesmentPurposeID !== null}
                label={Localization.t(
                  'CustomerRecognitionFormFix.YourTransactionPreference',
                )}
                placeholder={userInvesmentPurposeStatus}
                onPress={() =>
                  this.setState({
                    showInvestmentPurposeModal: true,
                  })
                }
              />
              <View style={{marginTop: 10}} />
              <View style={{flexDirection: 'row'}}>
                <FACheckbox.Circle
                  rule={'notEmptyAndNil'}
                  checked={ownBehalf}
                  onPress={() =>
                    this.setState({
                      ownBehalf: !ownBehalf,
                      otherBehalf: false,
                    })
                  }
                />
                <Text
                  style={{
                    marginLeft: 10,
                    flex: 1,
                    color: theme?.colors?.darkBlue,
                  }}>
                  {Localization.t('CustomerRecognitionFormFix.IveRead')}
                </Text>
              </View>
              <View style={{marginTop: 10}} />
              <View style={{flexDirection: 'row'}}>
                <FACheckbox.Circle
                  rule={'notEmptyAndNil'}
                  checked={otherBehalf}
                  onPress={() =>
                    this.setState({
                      otherBehalf: !otherBehalf,
                      ownBehalf: false,
                    })
                  }
                />
                <Text
                  style={{
                    marginLeft: 10,
                    flex: 1,
                    color: theme?.colors?.darkBlue,
                  }}>
                  {Localization.t('CustomerRecognitionFormFix.SomeoneElse')}
                </Text>
              </View>
              <View style={{marginTop: 10}} />

              {otherBehalf ? (
                <>
                  <FAForm
                    formValidSituationChanged={value =>
                      this.setState({
                        FormIsValid: value,
                      })
                    }>
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonName',
                      )}
                      name={'Name'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonName',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfName: value,
                        })
                      }
                      value={otherBehalfName}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonSurname',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonSurname',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfSurname: value,
                        })
                      }
                      value={otherBehalfSurname}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonIdentity',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonIdentity',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfIdentityNumber: value,
                        })
                      }
                      value={otherBehalfIdentityNumber}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonsFatherName',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonsFatherName',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfFatherName: value,
                        })
                      }
                      value={otherBehalfFatherName}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonsMotherName',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.OtherPersonsMotherName',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfMotherName: value,
                        })
                      }
                      value={otherBehalfMotherName}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.PlaceofBirthofRelevant',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.PlaceofBirthofRelevant',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfBirthdateArea: value,
                        })
                      }
                      value={otherBehalfBirthdateArea}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FADatePicker
                      minumumDate={
                        new Date(moment().subtract(72, 'years').toDate())
                      }
                      maximumDate={new Date()}
                      birthDate
                      label={Localization.t(
                        'CustomerRecognitionFormFix.DateofBirthofRelevant',
                      )}
                      rule={'notEmptyAndNil'}
                      value={otherBehalfBirthdate}
                      onChange={value =>
                        this.setState({
                          otherBehalfBirthdate: value,
                        })
                      }
                      placeHolder={Localization.t(
                        'CustomerRecognitionFormFix.DateofBirthofRelevant',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.EmailOfRelevant',
                      )}
                      name={'surname'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.EmailOfRelevant',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfEmail: value,
                        })
                      }
                      value={otherBehalfEmail}
                      rule={'isEmail'}
                      errorMessage={Localization.t(
                        'LoginScreenFix.correctEmailFormat',
                      )}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput.PhoneNumber
                      keyboardType={'phone-pad'}
                      rule={
                        phoneCode === '+90'
                          ? 'purePhoneNumberValue'
                          : 'notEmptyAndNil'
                      }
                      phonePrefix={phoneCode}
                      errorMessage={Localization.t(
                        'LoginScreenFix.CorrectFormatPhone',
                      )}
                      selectedItem={{
                        id: 1,
                        phoneCode: phoneCode,
                        flag: flag,
                      }}
                      placeholder={'555 555 55 55'}
                      label={Localization.t(
                        'CustomerRecognitionFormFix.PhoneOfRelevant',
                      )}
                      onChangeValue={(formatted, extracted) =>
                        this.onChangePhoneNumber(formatted, extracted)
                      }
                      value={formattedValue}
                      notFormattedValue={fullPhoneNumber}
                      mask={
                        phoneCode === '+90'
                          ? '[000] [000] [00] [00]'
                          : '[00000000000000]'
                      }
                      onPress={() => this.navigate()}
                    />
                    <View style={{marginTop: 10}} />
                    <FATextInput
                      label={Localization.t(
                        'CustomerRecognitionFormFix.AdressOfRelevant',
                      )}
                      name={'adress'}
                      placeholder={Localization.t(
                        'CustomerRecognitionFormFix.AdressOfRelevant',
                      )}
                      disable={false}
                      onChangeValue={value =>
                        this.setState({
                          otherBehalfAdress: value,
                        })
                      }
                      value={otherBehalfAdress}
                      rule={'notEmptyAndNil'}
                      errorMessage={Localization.t(
                        'CustomerRecognitionFormFix.NotEmpty',
                      )}
                    />
                  </FAForm>
                </>
              ) : null}
              <FAButton
                onPress={() => this.serviceCall()}
                text={Localization.t('CommonsFix.Send')}
                containerStyle={{
                  backgroundColor: FAOrange,
                  marginVertical: 10,
                  borderRadius: 4,
                }}
                textStyle={{marginVertical: 14}}
              />
            </KeyboardAwareScrollView>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
