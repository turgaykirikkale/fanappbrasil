import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import FAStandardHeader from '../../../Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FATextInput from '../../../Components/Composite/FATextInput';
import FAButton from '../../../Components/Composite/FAButton';
import FAColor from '../../../Commons/FAColor';
import {AccountStatus} from '../../../Commons/FAEnums';
import FAService from '../../../Services';
import {connect} from 'react-redux';
import Localization from '@Localization';

const AccountSettings = props => {
  // const CustomerStatusEnumId = props?.UserState?.UserInfo?.CustomerStatusEnumId;
  const [CustomerStatusEnumId, setCustomerStatusEnumId] = useState(null);

  const [showIdentityForm, setShowIdentityForm] = useState(true);
  const [approveIdentityButtonText, setApproveIdentityButtonText] = useState(
    Localization.t('AccountSettingsFix.Notverified'),
  );
  const [approveIdentityButtonDisabaled, setApproveIdentityButtonDisabled] =
    useState(false);
  const [approveIdentityColorControl, setApproveIdentityColorControl] =
    useState(0);

  const [
    customerIdentificationFormButtonText,
    setCustomerIdentificationFormButtonText,
  ] = useState(Localization.t('AccountSettingsFix.Notverified'));
  const [
    customerIdentificationFormButtonDisabled,
    setCustomerIdentificationFormButtonDisabled,
  ] = useState(true);
  const [customerIdentificationColorControl, setCustomerIdentificationControl] =
    useState(0);

  const [documentButtonText, setDocumentButtonText] = useState(
    Localization.t('AccountSettingsFix.Notverified'),
  );
  const [documentButtonDisabled, setDocumentButtonDisabled] = useState(true);
  const [documentColorControl, setDocumentColorControl] = useState(0);
  const [showDocument, setShowDocument] = useState(true);

  useEffect(() => {
    setCustomerStatusEnumId(props?.UserState?.UserInfo?.CustomerStatusEnumId);
    FAService.GetExchangeRegisterSetting()
      .then(res => {
        if (res?.data?.IsCustomerIdentification === true) {
          if (9 <= CustomerStatusEnumId && CustomerStatusEnumId <= 12) {
            setShowIdentityForm(false);
            setShowDocument(true);
          } else {
            setShowDocument(false);
          }
        }
      })
      .catch(err => {
        console.log('errACC', err);
      });
  });

  useEffect(() => {
    if (
      CustomerStatusEnumId > AccountStatus.MailConfirmed &&
      CustomerStatusEnumId === AccountStatus.PendingCredentials
    ) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Waitingforapproval'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(1);
    } else if (
      CustomerStatusEnumId > AccountStatus.PendingCredentials &&
      CustomerStatusEnumId === AccountStatus.CredentialComplete
    ) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(2);
      setCustomerIdentificationFormButtonText(
        Localization.t('AccountSettingsFix.Notverified'),
      );
      setCustomerIdentificationFormButtonDisabled(false);
      setCustomerIdentificationControl(1);
    } else if (
      CustomerStatusEnumId > AccountStatus.CredentialComplete &&
      CustomerStatusEnumId === AccountStatus.CustomerIdentificationFormWaiting
    ) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(2);
      setCustomerIdentificationFormButtonText(
        Localization.t('AccountSettingsFix.Waitingforapproval'),
      );
      setCustomerIdentificationFormButtonDisabled(true);
      setCustomerIdentificationControl(2);
    } else if (
      CustomerStatusEnumId > AccountStatus.CustomerIdentificationFormWaiting &&
      CustomerStatusEnumId === AccountStatus.CustomerIdentificationFormApproved
    ) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(2);
      setCustomerIdentificationFormButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setCustomerIdentificationFormButtonDisabled(true);
      setCustomerIdentificationControl(3);
      setDocumentButtonText(Localization.t('AccountSettingsFix.Notverified'));
      setDocumentButtonDisabled(false);
      setDocumentColorControl(1);
    } else if (
      CustomerStatusEnumId > AccountStatus.CustomerIdentificationFormApproved &&
      CustomerStatusEnumId === AccountStatus.DocumentApprovalWaiting
    ) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(2);
      setCustomerIdentificationFormButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setCustomerIdentificationFormButtonDisabled(true);
      setCustomerIdentificationControl(3);
      setDocumentButtonText(
        Localization.t('AccountSettingsFix.PleaseComplete'),
      );
      setDocumentButtonDisabled(false);
      setDocumentColorControl(2);
    } else if (CustomerStatusEnumId >= AccountStatus.DocumentApproved) {
      setApproveIdentityButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setApproveIdentityButtonDisabled(true);
      setApproveIdentityColorControl(2);
      setCustomerIdentificationFormButtonText(
        Localization.t('AccountSettingsFix.Verified'),
      );
      setCustomerIdentificationFormButtonDisabled(true);
      setCustomerIdentificationControl(3);
      setDocumentButtonText(Localization.t('AccountSettingsFix.Verified'));
      setDocumentButtonDisabled(true);
      setDocumentColorControl(3);
    }
  });
  const {navigation} = props;
  const UserInfo = props?.UserState?.UserInfo;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <FAStandardHeader
            title={Localization.t('SettingScreenFix.AccountSetting')}
            navigation={navigation}
          />
          <ScrollView style={{marginHorizontal: 10, marginTop: 10}}>
            <FATextInput
              label={Localization.t('CommonsFix.Name')}
              name={'Name'}
              placeholder={UserInfo.Name || null}
              disable={true}
            />
            <View style={{marginTop: 10}} />
            <FATextInput
              label={Localization.t('CommonsFix.Surname')}
              name={'surname'}
              placeholder={UserInfo.Surname || null}
              disable={true}
            />
            <View style={{marginTop: 10}} />
            <FATextInput
              label={Localization.t('CommonsFix.Phone')}
              name={'phone'}
              placeholder={`${UserInfo.CountryCode}${UserInfo.Phone}`}
              disable={true}
            />
            <View style={{marginTop: 10}} />
            <FATextInput
              label={Localization.t('CommonsFix.EmailAdress')}
              name={'eposta'}
              placeholder={UserInfo.Mail}
              disable={true}
            />
            <View style={{marginTop: 10}}>
              <Text style={{color: theme?.colors?.darkBlue}}>
                {Localization.t('AccountSettingsFix.IdentityConfirmation')}
              </Text>
              <FAButton
                onPress={() => navigation.navigate('ValidateIdentity')}
                disabled={approveIdentityButtonDisabaled}
                text={approveIdentityButtonText}
                containerStyle={{
                  backgroundColor:
                    approveIdentityColorControl === 0
                      ? theme?.colors?.white
                      : approveIdentityColorControl === 1
                      ? FAColor.Orange
                      : FAColor.Green,

                  borderRadius: 4,
                  marginTop: 8,
                  borderWidth: 2,
                  borderColor:
                    approveIdentityColorControl === 0
                      ? '#A50D47'
                      : approveIdentityColorControl === 1
                      ? FAColor.Orange
                      : FAColor.Green,
                }}
                textStyle={{
                  marginVertical: 8,
                  color:
                    approveIdentityColorControl === 0
                      ? '#A50D47'
                      : approveIdentityColorControl === 1
                      ? 'white'
                      : 'white',
                }}
              />
            </View>

            {/* {showIdentityForm === false ? null : (
              <View style={{marginTop: 10}}>
                <Text style={{color: theme?.colors?.darkBlue}}>
                  {Localization.t('AccountSettings.CustomerIdentificationForm')}
                </Text>
                <FAButton
                  onPress={() =>
                    navigation.navigate('CustomerRecognitionFormScreen')
                  }
                  disabled={customerIdentificationFormButtonDisabled}
                  text={customerIdentificationFormButtonText}
                  containerStyle={{
                    backgroundColor:
                      customerIdentificationColorControl === 0
                        ? theme?.colors?.white
                        : customerIdentificationColorControl === 1
                        ? theme?.colors?.white
                        : customerIdentificationColorControl === 2
                        ? FAColor.Orange
                        : FAColor.Green,
                    borderRadius: 4,
                    marginTop: 8,
                    borderWidth: 1,
                    borderColor:
                      customerIdentificationColorControl === 0
                        ? '#A50D47'
                        : customerIdentificationColorControl === 1
                        ? '#A50D47'
                        : customerIdentificationColorControl === 2
                        ? FAColor.Orange
                        : FAColor.Green,
                  }}
                  textStyle={{
                    marginVertical: 8,
                    color:
                      customerIdentificationColorControl === 0 ||
                      customerIdentificationColorControl === 1
                        ? '#A50D47'
                        : 'white',
                  }}
                />
              </View>
            )} */}

            {showDocument ? (
              <View style={{marginTop: 10, marginBottom: 10}}>
                <Text style={{color: theme?.colors?.darkBlue}}>
                  {Localization.t('AccountSettingsFix.Documents')}
                </Text>
                <FAButton
                  onPress={() => navigation.navigate('DocumentationPage')}
                  disabled={documentButtonDisabled}
                  text={documentButtonText}
                  containerStyle={{
                    backgroundColor:
                      documentColorControl === 0
                        ? theme?.colors?.white
                        : documentColorControl === 1
                        ? theme?.colors?.white
                        : documentColorControl === 2
                        ? FAColor.Orange
                        : FAColor.Green,
                    borderRadius: 4,
                    marginTop: 8,
                    borderWidth: 1,
                    borderColor:
                      documentColorControl === 0
                        ? '#A50D47'
                        : documentColorControl === 1
                        ? '#A50D47'
                        : documentColorControl === 2
                        ? FAColor.Orange
                        : FAColor.Green,
                  }}
                  textStyle={{
                    marginVertical: 8,
                    color:
                      documentColorControl === 0 || documentColorControl === 1
                        ? '#A50D47'
                        : 'white',
                  }}
                />
              </View>
            ) : null}
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, null)(AccountSettings);
