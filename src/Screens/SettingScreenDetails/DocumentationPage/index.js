import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import FAButton from '../../../Components/Composite/FAButton';
import FAStandardHeader from '../../../Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import FAColor from '../../../Commons/FAColor';
import FAService from '../../../Services';
import Localization from '@Localization';
import _ from 'lodash';

const DocumentationPage = props => {
  const [approved, setApproved] = useState(false);
  // const [documentTypes, setdocumentTypes] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [citizenDocumentStatus, setCitizenDocumentStatus] = useState(null);
  const [selfieDocumentStatus, setSelfieDocumentStatus] = useState(null);
  const [citizenDocumentConditionId, setCitizenDocumentConditionId] =
    useState(null);
  const [selfieDocumentConditionId, setSelfieDocumentConditionId] =
    useState(null);
  const {navigation} = props;

  useEffect(() => {
    FAService.getUserDocumentRejection()
      .then(res => console.log('DocumentREj', res))
      .catch(err => console.log('DocumentRejERRO', err));
  }, []);
  useEffect(() => {
    FAService.getUserDocumentationType()
      .then(res => {
        if (res?.data?.DocumentTypes) {
          const documentTypes = res?.data?.DocumentTypes;
          _.each(documentTypes, item => {
            if (item.Name === 'CitizenshipCard') {
              setCitizenDocumentConditionId(item.ConditionId);

              if (item.DocumentStatus !== null) {
                setCitizenDocumentStatus(
                  item.DocumentStatus.DocumentStatusEnumId,
                );
              }
            } else if (item.Name === 'Selfie') {
              setSelfieDocumentConditionId(item.ConditionId);
              if (item.DocumentStatus !== null) {
                setSelfieDocumentStatus(
                  item.DocumentStatus.DocumentStatusEnumId,
                );
              }
            }
          });
        } else {
          setIsLoading(false);
          setErrorMessage(Localization.t('DocumenationPageFix.Pleasetrylater'));
        }
      })
      .catch(err => console.log('DocumentTYOEERROr', err));
  }, []);

  const controlCitizenStatus = () => {
    if (citizenDocumentStatus === 0) {
      return Localization.t('AccountSettingsFix.Waitingforapproval');
    } else if (citizenDocumentStatus === 1) {
      return Localization.t('AccountSettingsFix.Verified');
    } else if (citizenDocumentStatus === 2) {
      return 'Error';
    } else {
      return Localization.t('AccountSettingsFix.Notverified');
    }
  };

  const controlSelfieStatus = () => {
    if (selfieDocumentStatus === 0) {
      return Localization.t('AccountSettingsFix.Waitingforapproval');
    } else if (selfieDocumentStatus === 1) {
      return Localization.t('AccountSettingsFix.Verified');
    } else if (selfieDocumentStatus === 2) {
      return 'Error';
    } else {
      return Localization.t('AccountSettingsFix.Notverified');
    }
  };

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <FAStandardHeader
            title={Localization.t('AccountSettingsFix.Documents')}
            navigation={navigation}
          />
          <View style={{marginHorizontal: 10, marginVertical: 10}}>
            <Text style={{color: theme?.colors?.black}}>
              {Localization.t('DocumenationPageFix.CitizenShip')}
            </Text>
            <FAButton
              disabled={
                citizenDocumentStatus === null || citizenDocumentStatus === 2
                  ? false
                  : true
              }
              onPress={() =>
                navigation.navigate('CitizenshipVerificationDocument', {
                  ConditionId: citizenDocumentConditionId,
                  DocumentStatus: citizenDocumentStatus,
                })
              }
              text={controlCitizenStatus()}
              containerStyle={{
                backgroundColor:
                  citizenDocumentStatus === 1
                    ? FAColor.Green
                    : theme?.colors?.white,
                borderColor:
                  citizenDocumentStatus === null || citizenDocumentStatus === 2
                    ? '#A50D47'
                    : citizenDocumentStatus === 0
                    ? FAColor.Orange
                    : FAColor.Green,
                borderWidth: 1,
                marginTop: 8,
                borderRadius: 4,
              }}
              textStyle={{
                color:
                  citizenDocumentStatus === null || citizenDocumentStatus === 2
                    ? '#A50D47'
                    : citizenDocumentStatus === 0
                    ? FAColor.Orange
                    : FAColor.White,
                marginVertical: 10,
              }}
            />
          </View>

          <View style={{marginHorizontal: 10, marginVertical: 10}}>
            <Text style={{color: theme?.colors?.black}}>
              {Localization.t('DocumenationPageFix.Selfie')}
            </Text>
            <FAButton
              disabled={
                selfieDocumentStatus === null || selfieDocumentStatus === 2
                  ? false
                  : true
              }
              onPress={() =>
                navigation.navigate('SelfieVerification', {
                  ConditionId: selfieDocumentConditionId,
                  DocumentStatus: selfieDocumentStatus,
                })
              }
              text={controlSelfieStatus()}
              containerStyle={{
                backgroundColor:
                  selfieDocumentStatus === 1
                    ? FAColor.Green
                    : theme?.colors?.white,
                borderColor:
                  selfieDocumentStatus === null || selfieDocumentStatus === 2
                    ? '#A50D47'
                    : selfieDocumentStatus === 0
                    ? FAColor.Orange
                    : FAColor.Green,
                borderWidth: 1,
                marginTop: 8,
                borderRadius: 4,
              }}
              textStyle={{
                color:
                  selfieDocumentStatus === null || selfieDocumentStatus === 2
                    ? '#A50D47'
                    : selfieDocumentStatus === 0
                    ? FAColor.Orange
                    : FAColor.White,

                marginVertical: 10,
              }}
            />
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default DocumentationPage;
