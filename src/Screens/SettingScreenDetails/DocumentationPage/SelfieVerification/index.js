import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {ThemeContext} from '../../../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../../../Components/Composite/FAStandardHeader';
import FAButton from '../../../../Components/Composite/FAButton';
import FAAccountConfirmationFlow from '@Components/Composite/FAAccountConfirmationFlow';
import FACamera from '@Components/Composite/FACamera';
import {FAOrange} from '../../../../Commons/FAColor';
import Localization from '@Localization';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Toast from 'react-native-toast-message';
import FAAlertModal from '../../../../Components/Composite/FAAlertModal';
import FAService from '../../../../Services';
import _ from 'lodash';

const SelfieVerification = props => {
  console.log(props);
  const {navigation} = props;
  const [showCamera, setShowCamera] = useState(false);
  const [selfie, setSelfie] = useState(null);
  const [uploadedSelfie, setUploadedSelfie] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const DocumentStatus = props?.route?.params?.DocumentStatus;

  // useEffect(() => {
  //   FAService.contents()
  //     .then(res => {
  //       if (res?.data && res?.data?.length > 0) {
  //         console.log('contents', res);
  //       } else {
  //         console.log('ContentSElseError');
  //       }
  //     })
  //     .catch(err => {
  //       console.log('contents', err);
  //     });
  // }, []);

  const openCamera = () => {
    setShowCamera(true);
  };
  const onShotCamera = data => {
    console.log(data);
    setSelfie(data);
    setShowCamera(false);
  };

  if (showCamera) {
    return (
      <FACamera
        showCamera={showCamera}
        onShotCamera={image => onShotCamera(image)}
        closeCamera={() => setShowCamera(false)}
      />
    );
  }

  const serviceCall = () => {
    setIsLoading(true);
    const ConditionId = props?.route?.params?.ConditionId;

    let ImageNameForFront = '';
    if (selfie.fileName) {
      ImageNameForFront = selfie.fileName;
    } else {
      const myArray = selfie.uri.split('/');
      ImageNameForFront = myArray[myArray.length - 1];
    }

    const dataForFront = selfie.type ? selfie.type : 'image/jpg';
    const base64ForFront = selfie.base64;
    const FixedmageNameForFront = ImageNameForFront;

    const requestBody = {};
    requestBody.DocumentTypeConditionId = ConditionId;
    requestBody.Pages = [
      {
        FrontContent: `data:${dataForFront};base64,${base64ForFront}`,
        FrontName: FixedmageNameForFront,
      },
    ];

    console.log(requestBody, 'requesBody');

    FAService.addCustomerDocumentation(requestBody)
      .then(res => {
        console.log(res, 'res');
        if (res?.data && res?.status === 200) {
          if (res.data.IsSuccess === true) {
            setIsLoading(false);
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('CommonsFix.Succces'),
              text2: res.data.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => navigation.navigate('AccountSettings'),
              onPress: () => {},
            });
          } else {
            setIsLoading(false);
            setShowErrorMessage(true);
            setErrorMessage(res?.data?.Message);
          }
        }
      })
      .catch(err => {
        console.log(err, 'err');
      });
  };

  console.log(selfie, 'self');
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            flex: 1,
            backgroundColor: theme?.colors?.white,
          }}>
          <FAStandardHeader
            title={Localization.t('DocumenationPageFix.SelfieHeader')}
            navigation={navigation}
          />
          <FAFullScreenLoader isLoading={isLoading} />
          <FAAlertModal
            visible={showErrorMessage}
            text={errorMessage}
            iconName={'times-circle'}
            header={Localization.t('CommonsFix.Error')}
            onPress={() => setShowErrorMessage(false)}
          />
          <ScrollView>
            <View style={{marginHorizontal: 10}}>
              {DocumentStatus === 2 ? (
                <View
                  style={{
                    backgroundColor: theme?.colors?.danger,
                    paddingVertical: 20,
                    paddingHorizontal: 10,
                    marginTop: 10,
                    borderRadius: 4,
                  }}>
                  <Text style={{color: 'white'}}>
                    {Localization.t('DocumenationPageFix.SomethingWentWrong')}
                  </Text>
                </View>
              ) : null}
              <View
                style={{
                  marginVertical: 10,
                  borderWidth: 1,
                  borderColor: theme?.colors?.darkGray,
                  paddingHorizontal: 6,
                  paddingVertical: 10,
                  borderRadius: 4,
                }}>
                <Text style={{color: theme?.colors?.black}}>
                  {Localization.t('DocumenationPageFix.SelfTextFirst')}
                </Text>
                <Text style={{color: theme?.colors?.black, marginTop: 8}}>
                  {Localization.t('DocumenationPageFix.SelfTextSecond')}
                </Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    marginTop: 8,
                    fontWeight: 'bold',
                  }}>
                  {Localization.t('DocumenationPageFix.SelfTextThird')}
                </Text>
                <Text style={{color: theme?.colors?.black, marginTop: 8}}>
                  {Localization.t('DocumenationPageFix.SelfTextFourth')}
                </Text>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    marginTop: 8,
                    fontWeight: 'bold',
                  }}>
                  {Localization.t('DocumenationPageFix.SelfTextFifth')}
                </Text>
              </View>
              <FAAccountConfirmationFlow
                value={selfie}
                rule={'notEmptyAndNil'}
                iconName="FAfronID"
                firstText={Localization.t(
                  'DocumenationPageFix.YourSelfieImage',
                )}
                image={selfie}
                openCamera={() => openCamera()}
                onPress={() => setUploadedSelfie(true)}
                onPressDelete={() => setSelfie(null)}
                upload={uploadedSelfie}
              />
              <FAButton
                disabled={selfie === null ? true : false}
                onPress={() => serviceCall()}
                text={Localization.t('CommonsFix.Submit')}
                containerStyle={{
                  backgroundColor: FAOrange,
                  marginVertical: 10,
                  borderRadius: 4,
                }}
                textStyle={{color: 'white', marginVertical: 10}}
              />
            </View>
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default SelfieVerification;
