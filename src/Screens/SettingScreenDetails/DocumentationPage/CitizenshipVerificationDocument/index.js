import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {ThemeContext} from '../../../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../../../Components/Composite/FAStandardHeader';
import FAButton from '../../../../Components/Composite/FAButton';
import FAAccountConfirmationFlow from '@Components/Composite/FAAccountConfirmationFlow';
import FACamera from '@Components/Composite/FACamera';
import {FAOrange} from '../../../../Commons/FAColor';
import _ from 'lodash';
import {ScrollView} from 'react-native-gesture-handler';
import Localization from '@Localization';
import FAService from '../../../../Services';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Toast from 'react-native-toast-message';
import FAAlertModal from '../../../../Components/Composite/FAAlertModal';

const CitizenshipVerificationDocument = props => {
  const [showCamera, setShowCamera] = useState(false);
  const [order, setOrder] = useState(null);
  const [ImageOne, setImageOne] = useState([]);
  const [ImageTwo, setImageTwo] = useState([]);
  const [uploadIdentityfrontend, setuploadIdentityfrontend] = useState(false);
  const [uploadIdentitybackend, setuploadIdentitybackend] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const {navigation} = props;
  const DocumentStatus = props?.route?.params?.DocumentStatus;


  const openCamera = incomingOrder => {
    setOrder(incomingOrder);
    setShowCamera(true);
  };

  const onShotCamera = data => {
    if (order === 1) {
      console.log('data1', data);
      setImageOne(data);
      console.log('ImageOne', ImageOne);
      setShowCamera(false);
    } else if (order === 2) {
      console.log('data2', data);

      setImageTwo(data);
      console.log('ImageTwo', ImageTwo);
      setShowCamera(false);
    }
  };

  if (showCamera) {
    return (
      <FACamera
        showCamera={showCamera}
        onShotCamera={image => onShotCamera(image)}
        closeCamera={() => setShowCamera(false)}
      />
    );
  }
  const isReadyForServiciCall = () => {
    if (_.isNull(ImageOne) || _.isNull(ImageTwo)) {
      return true;
    }
    return false;
  };

  const serviceCall = () => {
    setIsLoading(true);
    const ConditionId = props?.route?.params?.ConditionId;

    let ImageNameForFront = '';
    if (ImageOne.fileName) {
      ImageNameForFront = ImageOne.fileName;
    } else {
      const myArray = ImageOne.uri.split('/');
      ImageNameForFront = myArray[myArray.length - 1];
    }
    let ImageNameForBack = '';
    if (ImageTwo.fileName) {
      ImageNameForFront = ImageTwo.fileName;
    } else {
      const myArray = ImageTwo.uri.split('/');

      ImageNameForBack = myArray[myArray.length - 1];
    }
    const dataForFront = ImageOne.type ? ImageOne.type : 'image/jpg';
    const base64ForFront = ImageOne.base64;
    const FixedmageNameForFront = ImageNameForFront;

    const dataForBack = ImageTwo.type ? ImageOne.type : 'image/jpg';
    const base6ForBack = ImageTwo.base64;
    const fixedImageNameForBack = ImageNameForBack;

    const requestBody = {};
    requestBody.DocumentTypeConditionId = ConditionId;
    requestBody.Pages = [
      {
        BackContent: `data:${dataForBack};base64,${base6ForBack}`,
        BackName: fixedImageNameForBack,
        FrontContent: `data:${dataForFront};base64,${base64ForFront}`,
        FrontName: FixedmageNameForFront,
      },
    ];
    console.log(requestBody, 'requesBody');

    FAService.addCustomerDocumentation(requestBody)
      .then(res => {
        console.log(res, 'res');
        if (res?.data && res?.status === 200) {
          if (res.data.IsSuccess === true) {
            setIsLoading(false);
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('CommonsFix.Succces'),
              text2: res.data.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => navigation.navigate('AccountSettings'),
              onPress: () => {},
            });
          } else {
            setIsLoading(false);
            setShowErrorMessage(true);
            setErrorMessage(res?.data?.Message);
          }
        }
      })
      .catch(err => {
        console.log(err, 'err');
      });
  };

  console.log('1', ImageOne);
  console.log('2', ImageTwo);

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: theme?.colors?.white,
          }}>
          <FAFullScreenLoader isLoading={isLoading} />
          <FAAlertModal
            visible={showErrorMessage}
            text={errorMessage}
            iconName={'times-circle'}
            header={Localization.t('CommonsFix.Error')}
            onPress={() => setShowErrorMessage(false)}
          />
          <FAStandardHeader
            title={Localization.t('DocumenationPageFix.CitizenHeader')}
            navigation={navigation}
          />
          <View style={{marginHorizontal: 10}}>
            {DocumentStatus === 2 ? (
              <View
                style={{
                  backgroundColor: theme?.colors?.danger,
                  paddingVertical: 20,
                  paddingHorizontal: 10,
                  marginTop: 10,
                  borderRadius: 4,
                }}>
                <Text style={{color: 'white'}}>
                  {Localization.t('DocumenationPageFix.SomethingWentWrong')}
                </Text>
              </View>
            ) : null}
            <View
              style={{
                marginVertical: 10,
                borderWidth: 1,
                borderColor: theme?.colors?.darkGray,
                paddingHorizontal: 6,
                paddingVertical: 10,
                borderRadius: 4,
              }}>
              <Text style={{color: theme?.colors?.black}}>
                {Localization.t('DocumenationPageFix.textFirst')}
              </Text>
              <Text style={{marginVertical: 8, color: theme?.colors?.black}}>
                {Localization.t('DocumenationPageFix.textSecond')}
              </Text>
              <Text style={{color: theme?.colors?.black, fontWeight: 'bold'}}>
                {Localization.t('DocumenationPageFix.textThird')}
              </Text>
            </View>
            <View>
              <FAAccountConfirmationFlow
                value={ImageOne}
                rule={'notEmptyAndNil'}
                iconName="FAfronID"
                firstText={Localization.t('DocumenationPageFix.IDFronSide')}
                image={ImageOne}
                openCamera={() => openCamera(1)}
                onPress={() => setuploadIdentityfrontend(true)}
                onPressDelete={() => setImageOne(null)}
                upload={uploadIdentityfrontend}
              />

              <FAAccountConfirmationFlow
                value={ImageTwo}
                rule={'notEmptyAndNil'}
                iconName="FAfronID"
                firstText={Localization.t('DocumenationPageFix.IDBackSide')}
                image={ImageTwo}
                openCamera={() => openCamera(2)}
                onPress={() => setuploadIdentitybackend(true)}
                onPressDelete={() => setImageTwo(null)}
                upload={uploadIdentitybackend}
              />
              <FAButton
                onPress={() => serviceCall()}
                disabled={isReadyForServiciCall()}
                text={Localization.t('CommonsFix.Submit')}
                containerStyle={{
                  backgroundColor: FAOrange,
                  marginVertical: 10,
                  borderRadius: 4,
                }}
                textStyle={{color: 'white', marginVertical: 10}}
              />
            </View>
          </View>
        </ScrollView>
      )}
    </ThemeContext.Consumer>
  );
};

export default CitizenshipVerificationDocument;
