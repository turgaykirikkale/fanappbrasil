import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {ThemeContext} from '../../../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../../../Components/Composite/FAStandardHeader';
import FARegisterSelectableModal from '../../../../Components/Composite/FARegisterSelectableModal';
import FAPickerComponent from '../../../../Components/Composite/FAPickerComponent';
import FAService from '../../../../Services/index';
import {ScrollView} from 'react-native-gesture-handler';
import FAAccountConfirmationFlow from '@Components/Composite/FAAccountConfirmationFlow';
import {FAOrange} from '../../../../Commons/FAColor';
import FAButton from '../../../../Components/Composite/FAButton';
import DocumentPicker, {
  DirectoryPickerResponse,
  DocumentPickerResponse,
  isInProgress,
  types,
} from 'react-native-document-picker';

const AdressVerification = props => {
  const countryDAta = [
    {Id: 79, Name: 'Germany', Code: 'DE', DialCode: '+49'},
    {Id: 223, Name: 'Turkey', Code: 'TR', DialCode: '+90'},
  ];
  const [showCountryModal, setShowCountryModal] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState(countryDAta[1]);
  const [cityList, setCityList] = useState([]);
  const [showCityModal, setShowCityModal] = useState(false);
  const [selectedCity, setSelectedCity] = useState(null);
  const [selectidCityPlaceHolder, setSelectedCityPlaceHolder] =
    useState('Şehir Seçini');
  const [districtList, setDistrictList] = useState([]);
  const [showDistrictModal, setShowDistrictModal] = useState(false);
  const [selectedDistrict, setSelectedDistrict] = useState(null);
  const [selectdDistrictPlaceHolder, setSelectedDistrictPlacheHolder] =
    useState('İlçe Seçiniz');
  const {navigation} = props;

  const [file, setFile] = useState(null);
  const [uploadFile, setUploadedFile] = useState(false);

  const controlSelectedCountry = value => {
    setSelectedCountry(value);
    setShowCountryModal(false);
    FAService.GetCityList(value.Id)
      .then(res => {
        if (res?.data) {
          setCityList(res.data);
        }
      })
      .catch(err => {
        console.log('errCityList', err);
      });
  };

  const controlSelectedCity = value => {
    setSelectedCity(value);
    setSelectedCityPlaceHolder(value.Name);
    setShowCityModal(false);

    GetDistrictList(value);
  };

  const GetDistrictList = value => {
    FAService.GetDistrictList(value.Id)
      .then(res => {
        if (res.data && res.status === 200) {
          console.log('districtListERR', res.data);
          setDistrictList(res.data);
        }
      })
      .catch(err => {
        console.log('districtListERR', err);
      });
  };

  const controlSelectedDistrict = value => {
    setSelectedDistrict(value);
    setSelectedDistrictPlacheHolder(value.Name);
    setShowDistrictModal(false);
  };

  useEffect(() => {
    console.log(selectedCountry);
    const countryId = selectedCountry.Id;
    FAService.GetCityList(countryId)
      .then(res => {
        if (res?.data) {
          console.log('a');
          setCityList(res.data);
        }
      })
      .catch(err => {
        console.log('errCityList', err);
      });
  }, []);

  const selectFile = async () => {
    console.log('ABACLAK');
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      console.log('res', results);
      setFile(results);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log('Canceled');
      } else {
        console.log(err);
        console.log('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  console.log(selectedCountry);
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            flex: 1,
            backgroundColor: theme?.colors?.white,
          }}>
          {showCountryModal ? (
            <FARegisterSelectableModal
              data={countryDAta}
              closeModal={() => setShowCountryModal(false)}
              controlSelectedItem={value => controlSelectedCountry(value)}
            />
          ) : null}

          {showCityModal ? (
            <FARegisterSelectableModal
              data={cityList}
              closeModal={() => setShowCityModal(false)}
              controlSelectedItem={value => controlSelectedCity(value)}
            />
          ) : null}
          {showDistrictModal ? (
            <FARegisterSelectableModal
              data={districtList}
              closeModal={() => setShowDistrictModal(false)}
              controlSelectedItem={value => controlSelectedDistrict(value)}
            />
          ) : null}
          <FAStandardHeader title={'Adres Doğrulama'} navigation={navigation} />
          <View style={{marginHorizontal: 10, flex: 1}}>
            <View
              style={{
                marginVertical: 10,
                borderWidth: 1,
                borderColor: '#ffebb7',
                paddingHorizontal: 6,
                paddingVertical: 10,
                borderRadius: 4,
                backgroundColor: '#ffebb7',
              }}>
              <Text style={{color: 'black'}}>
                Türkiye'de ikamet eden kullanıcılarımız, sadece adınıza
                düzenlenmiş ve son 1 ayda e-devlet üzerinden alınmış Barkodlu
                Yerleşim Yeri Belgesi(İkametgah) geçerli belge olarak kabul
                edilmektedir. E-devlet üzerinden alınmamış olan diğer adress
                belgeleri (fatura vb.) onaylanmayacaktır. Aşağıdaki formda
                belirttiğiniz adresle yükleyeceğinz belgedeki adres aynı
                olmalıdır.
              </Text>
              <Text style={{color: 'black', marginTop: 8}}>
                .jpg, .png, .jpeg, .pdf türünde sadece 1 adet en fazla 15 MB
                boyutunda dosya yukleyebilirsiniz.
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginTop: 8,
                  fontWeight: 'bold',
                }}>
                Yoğunluk nedeniyle belge onay süreciniz 48 saat içerisinde
                tamamlanacağı bildirmek isteriz.
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginTop: 8,
                  fontWeight: 'bold',
                }}>
                E-devlet üzerinde oluşturacağınız adres belgesinde 'Belgenin
                Neden Verileceği' seçeneğine KURUMA İBRAZ seçilmeli, 'Kurum Adı'
                alanına 'Bitci' yazılmalıdır. Bu şartlara uymayan dökümanlar
                geçerli kabul edilmeyerek tarafınıza iade edilecektir.
              </Text>
            </View>
            <ScrollView styke={{flex: 1}} showsVerticalScrollIndicator={false}>
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                colorControl={selectedCountry !== null}
                label={'Doğum Ülkesi'}
                placeholder={selectedCountry.Name || null}
                onPress={() => setShowCountryModal(true)}
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                colorControl={selectedCity !== null}
                label={'Şehir'}
                placeholder={selectidCityPlaceHolder}
                onPress={() => setShowCityModal(true)}
              />
              <View style={{marginTop: 10}} />
              <FAPickerComponent
                colorControl={selectedDistrict !== null}
                label={'İlçe'}
                placeholder={selectdDistrictPlaceHolder}
                onPress={() => setShowDistrictModal(true)}
              />

              <FAAccountConfirmationFlow
                value={file}
                rule={'notEmptyAndNil'}
                iconName="FAfronID"
                firstText={file !== null ? file[0].name : 'Selfie görüntünüz'}
                image={file !== null ? file[0].uri : file}
                openCamera={() => selectFile()}
                onPress={() => selectFile()}
                onPressDelete={() => setFile(null)}
                upload={uploadFile}
              />
              <FAButton
                text={'Gönder'}
                containerStyle={{
                  backgroundColor: FAOrange,
                  marginVertical: 10,
                  borderRadius: 4,
                }}
                textStyle={{color: 'white', marginVertical: 10}}
              />
            </ScrollView>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default AdressVerification;
