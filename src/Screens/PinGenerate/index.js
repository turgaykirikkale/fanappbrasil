import React, {Component} from 'react';
import {View} from 'react-native';
import FATextInput from '@Components/Composite/FATextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FAButton from '@Components/Composite/FAButton';
import FAForm from '@ValidationEngine/Form';
import FAService from '@Services';
import Toast from 'react-native-toast-message';
import {connect} from 'react-redux';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {styles} from './assets/styles';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class PinGenerate extends Component {
  constructor(props) {
    const UserState = props && props.UserState;
    super(props);
    this.state = {
      currentPIN: '',
      newPIN: '',
      repeatnewPIN: '',
      formIsValid: false,
      alreadyHasPin:
        UserState && UserState.UserInfo && UserState.UserInfo.HasPinState,
      showDialogBox: false,
      serviceMessage: null,
      serviceLoading: false,
      currentPinShow: false,
      newPinShow: false,
      repeatNewPinShow: false,
    };
  }

  generatePINServiceCall() {
    const {currentPIN, newPIN, repeatnewPIN, alreadyHasPin} = this.state;
    let requestBody = null;
    if (currentPIN === newPIN) {
      this.setState({
        showDialogBox: true,
        serviceMessage: Localization.t('PinGenerate.NotSame'),
      });
    } else {
      if (alreadyHasPin) {
        requestBody = {
          CurrentPinNumber: currentPIN,
          PinNumber: newPIN,
          RepeatPinNumber: repeatnewPIN,
        };
      } else {
        requestBody = {
          PinNumber: newPIN,
          RepeatPinNumber: repeatnewPIN,
        };
      }
      this.setState({serviceLoading: true}, () => {
        FAService.DefinePin(requestBody)
          .then(response => {
            if (response && response.data && response.data.IsSuccess) {
              this.setState({
                serviceLoading: false,
              });

              Toast.show({
                type: 'success',
                position: 'top',
                text1: Localization.t('Commons.Succes'),
                text2: response.data.Message,
                visibilityTime: 750,
                autoHide: true,
                topOffset: 70,
                bottomOffset: 40,
                onShow: () => {},
                onHide: () => {},
                onPress: () => {},
              });
              if (
                this.props &&
                this.props.navigation &&
                this.props.navigation.canGoBack()
              ) {
                this.props.navigation.goBack();
              }
            } else {
              this.setState({
                serviceLoading: false,
                showDialogBox: true,
                serviceMessage: response.data.Message,
              });
            }
          })
          .catch(error => {
            this.setState({
              serviceLoading: false,
              showDialogBox: true,
              serviceMessage: Localization.t('PinGenerate.EnterAgain'),
            });
          });
      });
    }
  }

  render() {
    const {
      currentPIN,
      newPIN,
      repeatnewPIN,
      alreadyHasPin,
      formIsValid,
      serviceLoading,
      serviceMessage,
      showDialogBox,
      currentPinShow,
      newPinShow,
      repeatNewPinShow,
    } = this.state;
    const {navigation} = this.props;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <KeyboardAwareScrollView
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={
                alreadyHasPin
                  ? Localization.t('PinGenerate.ChangePINCode')
                  : Localization.t('PinGenerate.CreatePINCode')
              }
              navigation={navigation}
            />

            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showDialogBox}
              text={serviceMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <View style={styles.childContainer}>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    formIsValid: value,
                  })
                }>
                {alreadyHasPin ? (
                  <FATextInput.Password
                    isPasswordShowing={!currentPinShow}
                    onPressRightIcon={() =>
                      this.setState({
                        currentPinShow: !currentPinShow,
                      })
                    }
                    keyboardType={'number-pad'}
                    label={Localization.t('PinGenerate.CurrentPINCode')}
                    rule={'pinLenght'}
                    errorMessage={Localization.t(
                      'PinGenerate.PINCodeErrorMessage',
                    )}
                    name={'currenPin'}
                    value={currentPIN}
                    onChangeValue={value =>
                      this.setState({
                        currentPIN: value,
                      })
                    }
                    placeholder={Localization.t('PinGenerate.CurrentPINCode')}
                  />
                ) : null}

                <View style={styles.marginTop16} />
                <FATextInput.Password
                  isPasswordShowing={!newPinShow}
                  onPressRightIcon={() =>
                    this.setState({
                      newPinShow: !newPinShow,
                    })
                  }
                  keyboardType={'number-pad'}
                  label={Localization.t('PinGenerate.NewPINCode')}
                  rule={'pinLenght'}
                  errorMessage={Localization.t(
                    'PinGenerate.PINCodeErrorMessage',
                  )}
                  name={'newPinCode'}
                  value={newPIN}
                  onChangeValue={value =>
                    this.setState({
                      newPIN: value,
                    })
                  }
                  placeholder={Localization.t('PinGenerate.NewPINCode')}
                />
                <View style={styles.marginTop16} />
                <FATextInput.Password
                  isPasswordShowing={!repeatNewPinShow}
                  onPressRightIcon={() =>
                    this.setState({
                      repeatNewPinShow: !repeatNewPinShow,
                    })
                  }
                  keyboardType={'number-pad'}
                  label={Localization.t('PinGenerate.RepeatNewPINCode')}
                  rule={'pinLenght'}
                  errorMessage={Localization.t(
                    'PinGenerate.PINCodeErrorMessage',
                  )}
                  name={'repeatNewPinCode'}
                  value={repeatnewPIN}
                  onChangeValue={value =>
                    this.setState({
                      repeatnewPIN: value,
                    })
                  }
                  placeholder={Localization.t('PinGenerate.RepeatNewPINCode')}
                />
                <View style={styles.marginTop16} />
                <FAButton
                  disabled={!formIsValid}
                  onPress={() => this.generatePINServiceCall()}
                  containerStyle={[
                    styles.buttonContainer,
                    {backgroundColor: theme?.colors?.green},
                  ]}
                  textStyle={[
                    styles.buttonTextStyle,
                    {color: theme?.colors?.white2},
                  ]}
                  text={
                    alreadyHasPin
                      ? Localization.t('PinGenerate.ChangePINCode')
                      : Localization.t('PinGenerate.CreatePINCode')
                  }
                />
              </FAForm>
            </View>
          </KeyboardAwareScrollView>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, null)(PinGenerate);
