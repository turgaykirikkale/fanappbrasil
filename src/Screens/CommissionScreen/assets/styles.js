import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  headerLeftIconStyle: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  marginLeft2: {marginLeft: 2},
  renderheaderContainerStyle: {
    flexDirection: 'row',
    backgroundColor: FAColor.VGray,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  renderHeaderTextStyle: {color: FAColor.Black},
  renderHeaderValueTextStyle: {
    flex: 1,
    textAlign: 'right',
    color: FAColor.Gray,
  },
  mainContainerStyle: {flex: 1, backgroundColor: FAColor.White},
  LevelContainerStyle: {
    backgroundColor: '#F8F9FB',
    alignItems: 'center',
    paddingVertical: 30,
  },
  levelTextStyle: {color: FAColor.DarkBlue, fontSize: 12},
  fiatAccountLevelContainer: {
    width: 70,
    height: 70,
    backgroundColor: FAColor.Orange,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 25,
  },
  fiatAccountLevelTextStyle: {
    fontSize: 40,
    color: FAColor.White,
    fontWeight: 'bold',
  },
  flexDirectionRow: {flexDirection: 'row', alignItems: 'center'},
});
