import React, {Component} from 'react';
import {View, Text} from 'react-native';
import FAColor from '@Commons/FAColor';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import FACommissionRatio from '@Components/UI/FACommissionRatio';
import FAService from '@Services';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {styles} from './assets/styles';
import Localization from '@Localization';
import {AppConstants} from '@Commons/Contants';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
//{Localization.t('Commons.Error')}
export default class CommissionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      crytoCryptoMakerRate: null,
      cryptoCryptoTakerRate: null,
      fiatAccountLevel: null,
      crytoAccountLevel: null,
    };
  }

  componentWillMount() {
    this.setState({
      isLoading: true,
    });

    const requestBody2 = {
      CoinId: 86,
      CurrencyId: 2007,
    };

    FAService.GetMarketInformation(requestBody2)
      .then(response => {
        if (response && response.data && response.status === 200) {
          this.setState({
            fiatAccountLevel: response.data.CommissionRate.AccountLevel,
            isLoading: false,
            crytoCryptoMakerRate: response.data.CommissionRate.MakerRate,
            cryptoCryptoTakerRate: response.data.CommissionRate.TakerRate,
            crytoAccountLevel: response.data.CommissionRate.AccountLevel,
          });
        }
      })
      .catch(error => {});
  }

  renderStars = (i, percentage) => {
    return (
      <View style={styles.marginLeft2}>
        <FAVectorIcon
          group={'FontAwesome'}
          iconName={'star'}
          size={16}
          color={percentage > i ? FAColor.Orange : FAColor.VBlueGray}
        />
      </View>
    );
  };
  renderStarsArray = percentage => {
    const starsArray = [];
    for (let i = 0; i <= 5; i++) {
      starsArray.push(this.renderStars(i, percentage));
    }
    return starsArray;
  };

  renderheader({header, value}, theme) {
    return (
      <View
        style={[
          styles.renderheaderContainerStyle,
          {backgroundColor: theme?.colors?.vGray},
        ]}>
        <Text
          numberOfLines={3}
          style={[
            styles.renderHeaderTextStyle,
            {color: theme?.colors?.black, flex: 3},
          ]}>
          {header}
        </Text>
        <Text
          style={[
            styles.renderHeaderValueTextStyle,
            {color: theme?.colors?.gray},
          ]}>
          {value}
        </Text>
      </View>
    );
  }

  render() {
    const {
      isLoading,
      crytoCryptoMakerRate,
      cryptoCryptoTakerRate,
      fiatAccountLevel,
    } = this.state;
    const {navigation} = this.props;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainerStyle,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('CommissionScreen.title')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={isLoading} />
            <View
              style={[
                styles.LevelContainerStyle,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <Text
                style={[
                  styles.levelTextStyle,
                  {color: theme?.colors?.darkBlue},
                ]}>
                {Localization.t('CommissionScreen.level')}
              </Text>
              <View
                style={[
                  styles.fiatAccountLevelContainer,
                  {backgroundColor: theme?.colors?.orange},
                ]}>
                <Text
                  style={[
                    styles.fiatAccountLevelTextStyle,
                    {color: theme?.colors?.white2},
                  ]}>
                  {fiatAccountLevel}
                </Text>
              </View>
              <View style={styles.flexDirectionRow}>
                {this.renderStarsArray(fiatAccountLevel)}
              </View>
            </View>

            {this.renderheader(
              {
                header: Localization.t(
                  'CommissionScreen.CommissionTableHeaderCrypto',
                ),
                value: `${AppConstants.CoinCode}/BITCI`,
              },
              theme,
            )}
            <FACommissionRatio
              makerRatio={crytoCryptoMakerRate}
              takerRatio={cryptoCryptoTakerRate}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
