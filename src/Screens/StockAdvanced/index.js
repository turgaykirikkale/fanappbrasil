import FAExchangeOrderTypePicker from '@Components/Composite/FAExchangeOrderTypePicker';
import FAModeSelectorBox from '@Components/Composite/FAModeSelectorBox';
import {getCommissionRate} from '@Helpers/Calculation';
import FABalanceLine from '@Components/UI/FABalanceLine';
import FAStockHeader from '@Components/Composite/FAStockHeader';
import FATextInput from '@Components/Composite/FATextInput';
import {orderBookDataRenderer} from '@Commons/FAStreamLogic';
import FARangeSlider from '@Components/UI/FARangeSlider';
import FAOrderLine from '@Components/UI/FAOrderLine';
import {TabBar, TabView} from 'react-native-tab-view';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import FAButton from '@Components/Composite/FAButton';
import Toast from 'react-native-toast-message';
import {OrderType} from '@Commons/FAEnums';
import Localization from '@Localization';
import autobind from 'autobind-decorator';
import FAEnums from '@Commons/FAEnums';
import {styles} from './assets/styles';
import {connect} from 'react-redux';
import FAService from '@Services';
import React from 'react';
import _ from 'lodash';
import {stockCalculationHelper} from '@Helpers/StockCalculationHelper';
import FAActiveOrderList from '@Components/Composite/FAActiveOrderList';
import {
  View,
  FlatList,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  AppState,
  RefreshControl,
} from 'react-native';
import {
  subscribeCustomerOrderTickerChannel,
  subscribeMarketHistoryChannel,
  subscribeOrderBookTickerChannel,
  unSubscribeMarketHistoryChannel,
  unSubscribeOrderBookTickerChannel,
  unSubscribeCustomerOrderTickerChannel,
  subscribeBitciTryTickerChannel,
  unSubscribeBitciTryTickerChannel,
} from '@Stream';
import {
  BlueGray,
  DarkBlue,
  Green,
  Orange,
  Red,
  VBlueGray,
  White,
  FAOrange,
} from '@Commons/FAColor';
import {StockMode} from '@Commons/FAEnums';
import {toFixedNoRounding} from '@Commons/FAMath';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {setStockModeAction} from '@GlobalStore/Actions/MarketActions';
import {setSelectedPair} from '@GlobalStore/Actions/MarketActions';
import {setSelectedCoinAndCurrencyCode} from '@GlobalStore/Actions/MarketActions';

import {getUserWalletsAction} from '@GlobalStore/Actions/WalletActions';
import {setDataToStorage} from '@Commons/FAStorage';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import FAStockTransactionHistoryList from '@Components/Composite/FAStockTransactionHistoryList';
import FAMarketHistoryList from '@Components/Composite/FAMarketHistoryList';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAScrollingFilterList from '../../Components/UI/FAScrollingFilterList';
import FAPairSelectedBox from '../../Components/Composite/FAPairSelectedBox';
import UploadUserInformationPage from '../../Screens/UploadUserInformationPage';

const marketTypes = [
  {
    label: Localization.t('MarketScreenFix.Limit'),
    value: StockMarketTypes.Limit,
  },
  {
    label: Localization.t('MarketScreenFix.Market'),
    value: StockMarketTypes.Market,
  },
  {
    label: Localization.t('MarketScreenFix.Stop'),
    value: StockMarketTypes.Stop,
  },
];

class StockAdvanced extends React.Component {
  constructor(props) {
    super(props);
    this.initialInputValues = {
      amount: null,
      totalPrice: null,
      limitPrice: null,
      stopPrice: null,
      percent: null,
    };
    this.state = {
      isAnonymousFlow: props?.route?.params?.isAnonymous,
      pairDetail:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.pairDetail
          ? props.route.params.pairDetail
          : {},
      input: this.initialInputValues,
      MarketQueueName: null,
      isScreenRefreshing: false,
      createOrderLoading: false,
      coinBalanceDetail: {},
      coinBalanceLoading: true,
      currencyBalanceDetail: 0.0,
      currencyBalanceLoading: true,
      buyOrderBook: [],
      sellOrderBook: [],
      orderBookLoading: true,
      customerActiveBuyOrders: [],
      customerActiveSellOrders: [],
      activeOrdersLoading: false,
      customerTransactionHistoryList: [],
      customerTransactionHistoryLoading: false,
      marketHistory: [],
      marketHistoryLoading: false,
      flow:
        props && props.route && props.route.params && props.route.params.flow
          ? props.route.params.flow
          : FAEnums.BUYFLOW,
      percent: 0,
      marketFlowTypeList: [
        {
          label: Localization.t('MarketScreenFix.Limit'),
          value: StockMarketTypes.Limit,
        },
        // {
        //   label: Localization.t('MarketScreenFix.Market'),
        //   value: StockMarketTypes.Market,
        // },
        // {
        //   label: Localization.t('MarketScreenFix.Stop'),
        //   value: StockMarketTypes.Stop,
        // },
      ],
      currentMarketFlowType: StockMarketTypes.Limit,
      activeOrdersSelectedOrderType: FAEnums.BUYFLOW,
      leftSideMaxHeight: 600,
      index: 0,
      routes: [
        {
          key: 'activeOrders',
          title: Localization.t('WalletFix.ActiveOrders'),
          accessibilityLabel: 'activeOrders',
        },
        {
          key: 'transactionHistory',
          title: Localization.t('CommonsFix.TransactionHistory'),
          accessibilityLabel: 'transactionHistory',
        },

        {
          key: 'marketHistory',
          title: Localization.t('MarketScreenFix.MarketHistory'),
          accessibilityLabel: 'marketHistory',
        },
      ],
      filterLogic: {
        date: true,
        amount: false,
        price: false,
      },
      selectedMode: null,
      isModeBoxOpen: false,
      appState: AppState.currentState,
      showDialogBox: false,
      errorMessage: null,
      bitciTryPrice: null,
      isPairSelectedBox: false,
    };
    this.customerTransactionHistoryListSize = 8;
    this.orderChangeHandler = this.orderChangeHandler.bind(this);
    this.rightSideOnLayoutChange = this.rightSideOnLayoutChange.bind(this);
  }

  _unsubscribeFocusListener = null;
  _unsubscribeBlurListener = null;

  customerOrderType = [
    {label: Localization.t('CommonsFix.BuyOrder'), value: BUYFLOW},
    {label: Localization.t('CommonsFix.SellOrder'), value: SELLFLOW},
  ];

  componentWillMount() {
    const {isAnonymousFlow, routes} = this.state;
    const {navigation} = this.props;
    this._unsubscribeFocusListener = navigation.addListener('focus', () => {
      let anonymousTabRoutes = routes;
      if (isAnonymousFlow) {
        _.remove(anonymousTabRoutes, {key: 'activeOrders'});
        _.remove(anonymousTabRoutes, {key: 'transactionHistory'});
        const marketHistoryTabIndex = _.findIndex(anonymousTabRoutes, {
          key: 'marketHistory',
        });
        if (marketHistoryTabIndex > -1) {
          this.setState({
            routes: anonymousTabRoutes,
            index: marketHistoryTabIndex,
          });
        }
      }
      this.getMarketQueueNameAndFillScreenData(null, true);
    });
    this._unsubscribeBlurListener = navigation.addListener('blur', () => {
      // unSubscribeOrderBookTickerChannel();
      // unSubscribeCustomerOrderTickerChannel();
      // unSubscribeMarketHistoryChannel();
      // unSubscribeBitciTryTickerChannel();
    });
  }

  @autobind
  handleBitciTryPrice(incomingData) {
    if (incomingData && incomingData.p) {
      this.setState({
        bitciTryPrice: incomingData.p,
      });
    }
  }

  componentDidMount() {
    this.getMarketQueueNameAndFillScreenData(null, true);
    // unSubscribeMarketHistoryChannel();
  }

  componentWillReceiveProps(nextProps) {
    const {MarketState} = this.props;

    if (
      MarketState &&
      MarketState.SelectedPair &&
      nextProps &&
      nextProps.MarketState &&
      nextProps.MarketState.SelectedPair &&
      !_.isEqual(MarketState.SelectedPair, nextProps.MarketState.SelectedPair)
    ) {
      this.getMarketQueueNameAndFillScreenData(nextProps.MarketState, true);
      setTimeout(() => {
        this.getMarketQueueNameAndFillScreenData(null, true);
      }, 800);
    }
  }

  componentWillUnmount() {
    unSubscribeOrderBookTickerChannel();
    unSubscribeMarketHistoryChannel();
    unSubscribeCustomerOrderTickerChannel();
  }

  @autobind
  onRefresh() {
    const {MarketState} = this.props;
    this.setState({isScreenRefreshing: true});
    const requestBody = {
      selectedCoinCode: MarketState.SelectedCoinCode,
      selectedCurrencyCode: MarketState.SelectedCurrencyCode,
    };
    this.fillOrderBookByPair(requestBody, true);
    this.setState({isScreenRefreshing: false});
  }

  @autobind
  getMarketQueueNameAndFillScreenData(incomingMarketState, noOpenChannel) {
    const {MarketState} = this.props;
    let marketState = MarketState;
    if (incomingMarketState && incomingMarketState.SelectedPair) {
      marketState = incomingMarketState;
    }

    if (marketState && marketState.SelectedPair) {
      const {SelectedPair} = marketState;
      const {CoinId, CurrencyId} = SelectedPair;
      const requestBody = {
        CurrencyId: CurrencyId,
        CoinId: CoinId,
      };
      FAService.GetMarketInformation(requestBody)
        .then(response => {
          if (response && response.data) {
            this.setState(
              {
                MarketQueueName: response.data.MarketQueueName,
              },
              () => {
                if (SelectedPair?.CurrencyCode === 'BITCI') {
                  console.log(SelectedPair);
                  subscribeBitciTryTickerChannel(this.handleBitciTryPrice);
                }
                this.fillScreenData(incomingMarketState, noOpenChannel);
              },
            );
          }
        })
        .catch(error => {});
    }
  }

  @autobind
  fillScreenData(incomingMarketState, noOpenChannel) {
    const {MarketState, isAnonymousFlow} = this.props;

    let marketState = MarketState;
    const requestBody = {
      selectedCoinCode: marketState.SelectedCoinCode,
      selectedCurrencyCode: marketState.SelectedCurrencyCode,
    };
    if (incomingMarketState && incomingMarketState.SelectedPair) {
      marketState = incomingMarketState;
      // unSubscribeOrderBookTickerChannel();
      // unSubscribeMarketHistoryChannel();
      // unSubscribeCustomerOrderTickerChannel();
    }
    if (isAnonymousFlow) {
      this.fillOrderBookByPair(requestBody);
      this.getMarketHistoryServiceCall(requestBody, noOpenChannel);
      this.setState({
        pairDetail: marketState.SelectedPair,
      });
    }
    this.fillOrderBookByPair(requestBody);
    this.fillCustomerActiveOrders(requestBody, incomingMarketState);
    this.getMarketHistoryServiceCall(requestBody, noOpenChannel);
    this.getCurrencyAndCoinBalanceDetailAndSetToState(true);
    this.setState({
      pairDetail: marketState.SelectedPair,
    });
  }

  @autobind
  fillOrderBookByPair(incomingData, noSocketSubs) {
    this.setState({
      orderBookLoading: !noSocketSubs,
    });
    const {MarketQueueName} = this.state;
    FAService.GetActiveOrders(incomingData)
      .then(response => {
        if (response && response.data) {
          if (response && response.data && response.data.length > 0) {
            this.setState({
              lastPrice: response.data[0].LastCoinPrice,
              change24: response.data[0].Change24H,
            });
          }

          let buyOrders = _.sortBy(
            _.filter(response.data, {Type: FAEnums.BUYFLOW}),
            'Price',
          ).reverse();
          let sellOrders = _.sortBy(
            _.filter(response.data, {Type: FAEnums.SELLFLOW}),
            'Price',
          );
          this.setState(
            {
              buyOrderBook: percentExecutor(buyOrders, 'Price'),
              sellOrderBook: percentExecutor(sellOrders, 'Price'),
            },
            () => {
              if (!noSocketSubs) {
                subscribeOrderBookTickerChannel(
                  MarketQueueName,
                  this.orderChangeHandler,
                );
              } else {
                this.setState({isScreenRefreshing: false});
              }
            },
          );
        }
        this.setState({
          orderBookLoading: false,
          isScreenRefreshing: false,
        });
      })
      .catch(error => {
        this.setState({
          orderBookLoading: false,
        });
      });
  }

  handleMarketTypeChange(newMarketTypeValue) {
    this.setState({
      currentMarketFlowType: newMarketTypeValue,
    });
    this.clearInputs();
  }

  changeActiveOrdersSelectedOrderType(newValue) {
    this.setState({
      activeOrdersSelectedOrderType: newValue,
    });
  }

  orderChangeHandler(data) {
    let newBuyOrderBook = this.state.buyOrderBook;
    let newSellOrderBook = this.state.sellOrderBook;
    let newChange24 = this.state.change24;
    let newLastPrice = this.state.lastPrice;

    if (data && data.b && data.s) {
      const orderBook = orderBookDataRenderer(data);
      newBuyOrderBook = orderBook.BuyOrderBook;
      newSellOrderBook = orderBook.SellOrderBook;
    }

    if (data && data.p) {
      newLastPrice = data.p;
    }

    if (data && data.c) {
      newChange24 = data.c;
    }

    this.setState({
      buyOrderBook: newBuyOrderBook,
      sellOrderBook: newSellOrderBook,
      lastPrice: newLastPrice,
      change24: newChange24,
    });
  }

  @autobind
  fillCustomerActiveOrders(requestBody, incomingMarketState) {
    const {MarketState} = this.props;
    let marketState = MarketState;
    if (incomingMarketState) {
      marketState = incomingMarketState;
    }
    const {isAnonymousFlow} = marketState;
    if (!isAnonymousFlow) {
      const {MarketQueueName} = this.state;
      FAService.GetCustomerActiveOrders(requestBody)
        .then(response => {
          if (response && response.data && response.status === 200) {
            const customerActiveBuyOrders = _.filter(response.data, {
              Type: FAEnums.BUYFLOW,
            });
            const customerActiveSellOrders = _.filter(response.data, {
              Type: FAEnums.SELLFLOW,
            });
            this.setState({
              customerActiveBuyOrders: customerActiveBuyOrders,
              customerActiveSellOrders: customerActiveSellOrders,
            });
            subscribeCustomerOrderTickerChannel(
              MarketQueueName,
              this.handleCustomerActiveOrders,
            );
          }
          this.setState({
            activeOrdersLoading: false,
          });
        })
        .catch(error => {
          this.setState({
            activeOrdersLoading: false,
          });
          // console.log(error);
        });
    }
  }

  @autobind
  handleCustomerActiveOrders(data) {
    const {customerActiveBuyOrders, customerActiveSellOrders} = this.state;
    if (data.CoinId && data.CurrencyId) {
      let searchItemIndex = null;
      let orderArray = [];

      if (data.CancelledAfterProcess) {
        return false;
      }

      if (data.Type === OrderType.Buy) {
        orderArray = customerActiveBuyOrders;
      } else {
        orderArray = customerActiveSellOrders;
      }
      searchItemIndex = _.findIndex(orderArray, item => {
        return (item.OrderId || item.Id) === data.Id;
      });

      if (data.State === 0 && searchItemIndex < 0) {
        orderArray.push(data);
      } else if (searchItemIndex !== -1 && data.State === 1) {
        orderArray[searchItemIndex] = data;
      } else if (searchItemIndex !== -1 && data.State === 2) {
        let item = orderArray[searchItemIndex];
        if (data.ProcessCoinValue === data.CoinValue) {
          orderArray.splice(searchItemIndex, 1);
        } else {
          item.CoinValue = item.CoinValue - data.ProcessCoinValue;
          orderArray[searchItemIndex] = item;
        }
      } else if (searchItemIndex !== -1 && data.State === 4) {
        orderArray.splice(searchItemIndex, 1);
      }
      if (data.Type === OrderType.Buy) {
        this.setState({customerActiveBuyOrders: orderArray});
      } else {
        this.setState({customerActiveSellOrders: orderArray});
      }

      this.fillCustomerTransactionHistoryList(true);
      setTimeout(() => {
        this.getCurrencyAndCoinBalanceDetailAndSetToState();
      }, 800);
    }
  }

  fillCustomerTransactionHistoryList(fromSocket = false) {
    const {buyOrderBook, pairDetail} = this.state;
    if (buyOrderBook && buyOrderBook.length && buyOrderBook.length > 0) {
      const requestBody = {
        PagerDto: {
          Page: 1,
          Size: this.customerTransactionHistoryListSize,
          Total: 0,
        },
        coinId: pairDetail && pairDetail.CoinId ? pairDetail.CoinId : null,
        currencyId:
          pairDetail && pairDetail.CurrencyId ? pairDetail.CurrencyId : null,
      };
      if (!fromSocket) {
        this.setState({
          customerTransactionHistoryLoading: true,
        });
      }
      FAService.GetCustomerTransactionHistory(requestBody)

        .then(response => {
          if (response && response.data && response.status === 200) {
            this.setState({
              customerTransactionHistoryList: response.data.Orders || [],
            });
          }
          if (!fromSocket) {
            this.setState({customerTransactionHistoryLoading: false});
          }
        })
        .catch(error => {
          if (!fromSocket) {
            this.setState({customerTransactionHistoryLoading: false});
          }
        });
    }
  }

  getMarketHistoryServiceCall(incomingData, noOpenChannel) {
    const {MarketQueueName} = this.state;
    const requestBody = {
      selectedCoinCode: incomingData.selectedCoinCode,
      selectedCurrencyCode: incomingData.selectedCurrencyCode,
    };
    this.setState({marketHistoryLoading: true});
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response && response.status === 200 && response.data) {
          const marketHistoryList = _.slice(response.data, 0, 10);
          this.setState({marketHistory: marketHistoryList}, () => {
            if (!noOpenChannel) {
              subscribeMarketHistoryChannel(
                MarketQueueName,
                this.marketHistoryHandler,
              );
            }
          });
        }
        this.setState({marketHistoryLoading: false});
      })
      .catch(error => this.setState({marketHistoryLoading: false}));
  }

  @autobind
  marketHistoryHandler(data) {
    let tradeList = [];
    const socketHistoryData = data.split('|');
    if (socketHistoryData) {
      _.map(socketHistoryData, item => {
        const tradeItem = item.split(',');
        const coinValue = Number(tradeItem[2]);
        const price = Number(tradeItem[1]);
        let trade = {
          CreatedOn: String(tradeItem[0]),
          Price: Number(tradeItem[1]),
          CoinValue: Number(tradeItem[2]),
          MatchType: Number(tradeItem[3]),
          Total: coinValue * price,
          Id: tradeItem[4],
        };
        tradeList.push(trade);
      });
    }
    tradeList = _.slice(tradeList, 0, 10);
    console.log(tradeList, 'TradeList');
    this.setState({
      marketHistory: tradeList,
    });
  }

  @autobind
  findMatchedAssetBalance(balanceList, assetId) {
    return _.find(balanceList, {CoinId: assetId})?.CoinBalance;
  }

  getCurrencyAndCoinBalanceDetailAndSetToState(initial) {
    const {UserState, MarketState} = this.props;

    const {pairDetail} = this.state;
    if (pairDetail) {
      const coinBalanceDetailRequestBody = {
        CurrencyId: pairDetail.CurrencyId,
        CoinId: pairDetail.CoinId,
      };
      if (initial) {
        this.setState({coinBalanceLoading: true});
        this.setState({currencyBalanceLoading: true});
      }
      FAService.GetCoinBalanceDetail(coinBalanceDetailRequestBody)
        .then(response => {
          if (response && response.status === 200 && response.data) {
            this.setState({
              coinBalanceDetail: response.data,
            });
          }
          if (initial) {
            this.setState({coinBalanceLoading: false});
          }
        })
        .catch(error => {
          if (initial) {
            this.setState({coinBalanceLoading: false});
          }
        });

      const currencyBalanceDetailRequestBody = {
        CurrencyId: pairDetail.CurrencyId,
      };
      FAService.GetCurrencyBalanceDetail(currencyBalanceDetailRequestBody)
        .then(response => {
          console.log('CurrencyBalance', response);
          if (response && response.status === 200 && response.data) {
            this.setState({
              currencyBalanceDetail: response.data,
            });
          }
          if (initial) {
            this.setState({currencyBalanceLoading: false});
          }
        })
        .catch(error => {
          if (initial) {
            this.setState({currencyBalanceLoading: false});
          }
        });
    }
  }

  getCommissionRate(isTaker) {
    const {pairDetail} = this.state;
    const {CurrencyCode, CurrencyId, CoinId} = pairDetail;
    if (isTaker) {
      return getCommissionRate(
        {CurrencyCode: CurrencyCode, CurrencyId: CurrencyId},
        CoinId,
        true,
      ).taker;
    } else {
      return getCommissionRate(
        {CurrencyCode: CurrencyCode, CurrencyId: CurrencyId},
        CoinId,
        true,
      ).maker;
    }
  }

  commissionRateCalculate(limit) {
    try {
      let calculatedLimit = limit || this.state.limitPrice;
      const {flow, currentMarketFlowType, buyOrderBook, sellOrderBook} =
        this.state;

      if (currentMarketFlowType === 'piyasa') {
        return this.getCommissionRate(true);
      }
      let ActiveOrders = null;
      let commissionRate = 0;
      if (flow === 1) {
        if (sellOrderBook && sellOrderBook.length && sellOrderBook.length > 0) {
          ActiveOrders = sellOrderBook;
          if (calculatedLimit >= ActiveOrders[0].Price) {
            commissionRate = this.getCommissionRate(true);
          } else {
            commissionRate = this.getCommissionRate(false);
          }
        }
      } else {
        if (buyOrderBook && buyOrderBook.length && buyOrderBook.length > 0) {
          ActiveOrders = buyOrderBook;
          if (calculatedLimit > ActiveOrders[0].Price) {
            commissionRate = this.getCommissionRate(false);
          } else {
            commissionRate = this.getCommissionRate(true);
          }
        }
      }
      return commissionRate / 100;
    } catch (error) {}
  }

  calculation(
    incomingCurrencyValue,
    incomingCoinValue,
    incomingLimitValue,
    triggeredByPercentValueExecutor,
  ) {
    const {currentMarketFlowType, input, flow, buyOrderBook, sellOrderBook} =
      this.state;
    const state = {
      currentMarketFlowType: currentMarketFlowType,
      percent: input.percent,
      flow: flow,
      limitPrice: input.limitPrice,
      amount: input.amount,
      totalPrice: input.totalPrice,
      buyOrderBook: buyOrderBook,
      sellOrderBook: sellOrderBook,
    };

    const calculationResponse = stockCalculationHelper(
      state,
      incomingCurrencyValue,
      incomingCoinValue,
      incomingLimitValue,
      triggeredByPercentValueExecutor,
    );
    if (calculationResponse) {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          percent:
            calculationResponse.percent || calculationResponse.percent === null
              ? calculationResponse.percent
              : prevState.input.percent,
          amount:
            calculationResponse.amount || calculationResponse.amount === null
              ? calculationResponse.amount
              : prevState.input.amount,
          limitPrice:
            calculationResponse.limitPrice ||
            calculationResponse.limitPrice === null
              ? calculationResponse.limitPrice
              : prevState.input.limitPrice,
          totalPrice:
            calculationResponse.totalPrice ||
            calculationResponse.totalPrice === null
              ? calculationResponse.totalPrice
              : prevState.input.totalPrice,
        },
      }));
    }
  }

  @autobind
  createOrder() {
    const {
      buyOrderBook,
      sellOrderBook,
      flow,
      currentMarketFlowType,
      input,
      pairDetail,
    } = this.state;

    if (this.props?.UserState?.UserInfo?.CustomerStatusEnumId < 10) {
      this.setState({
        showUserUploadInformModal: true,
      });
    } else {
      const ActiveBuyOrders = buyOrderBook;
      const ActiveSellOrders = sellOrderBook;

      let generatedPrice = 0;
      let stopValue = null;
      if (currentMarketFlowType === StockMarketTypes.Market) {
        if (flow === 1) {
          generatedPrice = ActiveSellOrders[0].Price;
        } else {
          generatedPrice = ActiveBuyOrders[0].Price;
        }
      } else if (currentMarketFlowType === StockMarketTypes.Limit) {
        generatedPrice = input.limitPrice;
      } else if (currentMarketFlowType === StockMarketTypes.Stop) {
        stopValue = input.stopPrice;
        generatedPrice = input.limitPrice;
      }
      const requestBody = {
        CoinId: pairDetail?.CoinId,
        CurrencyId: pairDetail?.CurrencyId,
        Price: generatedPrice,
        Amount: toFixedNoRounding(Number(input.amount), 8),
        StopLossPrice: stopValue,
        OrderType: flow,
      };
      this.setState({createOrderLoading: true});
      FAService.CreateOrder(requestBody)
        .then(response => {
          if (response && response.data && !response.data.IsSuccess) {
            this.setState({
              errorMessage: response.data.Message,
            });
          } else {
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('CommonsFix.Succces'),
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
          }
          this.setState({
            createOrderLoading: false,
          });
        })
        .catch(error => {
          this.setState({
            createOrderLoading: false,
          });
        });
    }
  }

  handlePercentExecutor(newPercentValue) {
    const {flow, currencyBalanceDetail, coinBalanceDetail} = this.state;
    let currencyBalance = currencyBalanceDetail?.Balance;
    let coinBalance = coinBalanceDetail?.CoinBalance;
    if (flow === BUYFLOW) {
      if (currencyBalance) {
        const calculatedBalance = (
          currencyBalance * newPercentValue
        ).toString();
        this.calculation(
          calculatedBalance,
          null,
          null,
          newPercentValue.toString(),
        );
      }
    } else {
      if (coinBalance) {
        const calculatedBalance = (coinBalance * newPercentValue).toString();
        this.calculation(
          null,
          calculatedBalance,
          null,
          newPercentValue.toString(),
        );
      }
    }

    this.setState(prevState => ({
      input: {
        ...prevState.input,
        percent: newPercentValue,
      },
    }));
  }

  customerOrderHandlersAndBalanceHandler() {
    const {MarketState, getUserWalletsAction} = this.props;

    const requestBody = {
      selectedCoinCode: MarketState.SelectedCoinCode,
      selectedCurrencyCode: MarketState.SelectedCurrencyCode,
    };

    setTimeout(() => {
      this.fillCustomerActiveOrders(requestBody, MarketState);
    }, 800);

    setTimeout(() => {
      this.getCurrencyAndCoinBalanceDetailAndSetToState(true);
    }, 800);
    getUserWalletsAction();
  }

  @autobind
  orderListOnPressHandler(incomingPrice) {
    const {currentMarketFlowType} = this.state;

    if (currentMarketFlowType === StockMarketTypes.Market) {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          totalPrice: currentMarketFlowType,
        },
      }));
      this.calculation(incomingPrice);
    } else {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          limitPrice: incomingPrice,
        },
      }));
      this.calculation(null, null, incomingPrice);
    }
  }

  @autobind
  renderOrderRowItem(item, reverse = false) {
    const {selectedMode} = this.state;
    let decimalCurrencyCount = 8;
    let decimalCoinCount = 8;

    const {coinBalanceDetail} = this.state;
    if (coinBalanceDetail) {
      const {CoinDecimal, PriceDecimal} = coinBalanceDetail;
      decimalCoinCount = CoinDecimal;
      decimalCurrencyCount = PriceDecimal;
    }
    return (
      <FAOrderLine
        volumeBarDirection={
          selectedMode === StockMode.Standard
            ? item.Type === BUYFLOW
              ? 'right'
              : 'left'
            : null
        }
        linePercent={item.DifferencePercent}
        reverse={reverse}
        onPress={price => this.orderListOnPressHandler(price.toString())}
        price={item.Price}
        amount={item.CoinValue}
        flowType={item.Type}
        size={FAEnums.SMALL}
        decimalCurrencyCount={3}
        decimalCoinCount={3}
      />
    );
  }

  rightSideOnLayoutChange(event) {
    if (event && event.nativeEvent && event.nativeEvent.layout) {
      this.setState({
        leftSideMaxHeight: event.nativeEvent.layout.height,
      });
    }
  }

  @autobind
  activeOrderRender(theme) {
    const {
      activeOrdersSelectedOrderType,
      customerActiveBuyOrders,
      customerActiveSellOrders,
      isOrderCancelLoading,
      coinBalanceDetail,
      activeOrdersLoading,
    } = this.state;

    let currencyDecimalCount = 8;
    let coinDecimalCount = 8;
    if (!_.isNil(coinBalanceDetail)) {
      const {PriceDecimal, CoinDecimal} = coinBalanceDetail;
      currencyDecimalCount = PriceDecimal || currencyDecimalCount;
      coinDecimalCount = CoinDecimal || coinDecimalCount;
    }

    if (activeOrdersLoading) {
      return (
        <View
          style={[
            styles.loaderContainer,
            {backgroundColor: theme?.colors?.white},
          ]}>
          <ActivityIndicator color={theme?.colors?.orange} size={'small'} />
        </View>
      );
    }

    return (
      <View style={{marginHorizontal: 10, paddingVertical: 8}}>
        <FAScrollingFilterList
          data={this.customerOrderType}
          selected={activeOrdersSelectedOrderType}
          onSelect={incomingValue =>
            this.setState({
              activeOrdersSelectedOrderType: incomingValue,
            })
          }
        />
        {!isOrderCancelLoading &&
        activeOrdersSelectedOrderType === OrderType.Buy &&
        customerActiveBuyOrders &&
        customerActiveBuyOrders.length &&
        customerActiveBuyOrders.length > 0 ? (
          <FAActiveOrderList
            data={customerActiveBuyOrders}
            onOrderCancelSuccess={() => this.handleCustomerActiveOrders()}
          />
        ) : !isOrderCancelLoading &&
          activeOrdersSelectedOrderType === OrderType.Sell &&
          customerActiveSellOrders &&
          customerActiveSellOrders.length &&
          customerActiveSellOrders.length > 0 ? (
          <FAActiveOrderList
            data={customerActiveSellOrders}
            onOrderCancelSuccess={() => this.handleCustomerActiveOrders()}
          />
        ) : isOrderCancelLoading ? (
          <ActivityIndicator size={'large'} color={'orange'} />
        ) : null}
      </View>
    );
  }

  @autobind
  transactionHistoryRender() {
    const {MarketState} = this.props;
    const {customerTransactionHistoryList, customerTransactionHistoryLoading} =
      this.state;
    if (customerTransactionHistoryLoading) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size={'small'} color={Orange} />
        </View>
      );
    }

    if (MarketState && MarketState.isAnonymousFlow) {
      return null;
    }

    return (
      <FAStockTransactionHistoryList data={customerTransactionHistoryList} />
    );
  }

  @autobind
  marketHistoryRender() {
    const {marketHistory, marketHistoryLoading} = this.state;
    if (marketHistoryLoading) {
      return (
        <View style={{paddingVertical: 30}}>
          <ActivityIndicator size={'small'} color={FAOrange} />
        </View>
      );
    }

    return <FAMarketHistoryList data={marketHistory} />;
  }

  @autobind
  renderTabBar(props, theme) {
    const {isAnonymousFlow} = this.state;
    return (
      <TabBar
        scrollEnabled={true}
        {...props}
        renderLabel={({route, focused, color}) => (
          <Text
            style={[
              styles.tabBarLabel,
              {
                color: focused
                  ? theme?.colors?.orange
                  : theme?.colors?.darkBlue,
              },
            ]}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={[
          styles.tabBarContainerStyle,
          isAnonymousFlow
            ? {
                flex: 1,
                justifyContent: 'center',
              }
            : null,
          {borderColor: theme?.colors?.gray},
        ]}
        indicatorStyle={[
          styles.tabBarIndicatorStyle,
          {backgroundColor: theme?.colors?.orange},
        ]}
        style={[styles.tabBarStyle, {backgroundColor: theme?.colors?.white}]}
        tabStyle={
          isAnonymousFlow
            ? {
                width: '100%',
              }
            : null
        }
      />
    );
  }

  onIndexChangeHandler(indexValue) {
    const {MarketState} = this.props;
    let selectedCoinCode,
      selectedCurrencyCode = null;
    if (
      MarketState &&
      MarketState.SelectedCoinCode &&
      MarketState.SelectedCurrencyCode
    ) {
      selectedCoinCode = MarketState.SelectedCoinCode;
      selectedCurrencyCode = MarketState.SelectedCurrencyCode;
    }

    const data = {
      selectedCoinCode: selectedCoinCode,
      selectedCurrencyCode: selectedCurrencyCode,
    };

    if (indexValue === 0) {
      this.fillCustomerActiveOrders(data);
      unSubscribeMarketHistoryChannel();
    } else if (indexValue === 1) {
      if (MarketState && !MarketState.isAnonymousFlow) {
        this.fillCustomerTransactionHistoryList();
        unSubscribeMarketHistoryChannel();
      }
    } else if (indexValue === 2) {
      this.getMarketHistoryServiceCall(data);
    }
    this.setState({index: indexValue});
  }

  clearInputs() {
    this.setState({
      input: this.initialInputValues,
    });
  }

  handleFlowChange(newFlowValue) {
    this.setState({
      flow: newFlowValue,
    });
    this.clearInputs();
  }

  navigateToMarketScreen() {
    const {navigation} = this.props;
    navigation.navigate('MarketTabStackNavigator', {params: {from: 'stock'}});
  }

  async dispatchSelectedStockMode(newStockMode) {
    if (newStockMode && newStockMode !== StockMode.Advanced) {
      const {navigation, setStockModeAction} = this.props;
      const {flow} = this.state;
      setStockModeAction(newStockMode);
      this.setState({isModeBoxOpen: false});
      await setDataToStorage('StockMode', newStockMode);
      if (StockMode.EasyBuySell === newStockMode) {
        navigation?.navigate('StockEasyBuy', {flow});
      } else {
        navigation?.navigate('StockStandardScreen', {flow});
      }
    }
  }

  async dispatchSelectedPairDetails(value) {
    const {MarketState, setSelectedPair, setSelectedCoinAndCurrencyCode} =
      this.props;
    let selectedCoinAndSelectedCurrencyCode = {
      SelectedCoinCode: value.CoinCode,
      SelectedCurrencyCode: value.CurrencyCode,
    };
    console.log('BEFOREMarketState', MarketState);
    setSelectedPair(value);
    setSelectedCoinAndCurrencyCode(selectedCoinAndSelectedCurrencyCode);
    console.log('AFTERMarketState', MarketState);
    this.setState({
      isPairSelectedBox: false,
    });
  }

  @autobind
  navigateToLoginRegister() {
    const {navigation} = this.props;
    navigation.navigate('FanTabStackNavigator', {
      screen: 'FanScreen',
    });
  }

  lastMarketOrderPriceExecutor(marketHistoryLastOrder) {
    const {currentMarketFlowType} = this.state;
    if (currentMarketFlowType === 'piyasa') {
      this.calculation(marketHistoryLastOrder.Price.toString());
    } else {
      this.calculation(null, null, marketHistoryLastOrder.Price.toString());
    }
  }

  @autobind
  priceOnPressIncrement() {
    const {buyOrderBook, sellOrderBook, flow} = this.state;
    let lastOrderValue = null;
    if (flow === FAEnums.BUYFLOW) {
      lastOrderValue = sellOrderBook[0].Price;
    } else {
      lastOrderValue = buyOrderBook[0].Price;
    }

    if (lastOrderValue) {
      this.setState(
        {
          limitPrice: String(lastOrderValue),
        },
        () => {
          this.calculation(null, null, String(lastOrderValue));
        },
      );
    }
  }

  navigateToChartScreen() {
    const {pairDetail} = this.state;
    const {navigation} = this.props;
    if (!_.isEmpty(pairDetail)) {
      const {CoinCode, CurrencyCode} = pairDetail;
      const chartUrl = `https://chart.bitci.com/exchange/advanced/${CoinCode}_${CurrencyCode}`;
      navigation.navigate('WebviewScreen', {
        title: Localization.t('WebviewScreen.HeaderTitle', {
          coinCode: CoinCode,
          currencyCode: CurrencyCode,
        }),
        url: chartUrl,
      });
    }
  }

  tabRenderByIndex(theme) {
    const {isAnonymousFlow, index} = this.state;
    if (isAnonymousFlow) {
      if (index === 0) {
        return this.marketHistoryRender();
      }
    } else {
      if (index === 0) {
        return this.activeOrderRender(theme);
      } else if (index === 1) {
        return this.transactionHistoryRender();
      } else if (index === 2) {
        return this.marketHistoryRender();
      }
    }
  }

  render() {
    const {MarketState, navigation} = this.props;
    const {isAnonymousFlow} = MarketState;
    const {
      input,
      sellOrderBook,
      buyOrderBook,
      flow,
      marketFlowTypeList,
      currentMarketFlowType,
      leftSideMaxHeight,
      index,
      routes,
      isModeBoxOpen,
      coinBalanceDetail,
      currencyBalanceDetail,
      pairDetail,
      marketHistory,
      createOrderLoading,
      orderBookLoading,
      errorMessage,
      isScreenRefreshing,
      lastPrice,
      change24,
      bitciTryPrice,
      isPairSelectedBox,
      MarketQueueName,
      showUserUploadInformModal,
    } = this.state;
    const initialLayout = {width: Dimensions.get('window').width};
    let marketHistoryLastOrder = null;
    const currencyDecimal = 2;
    const coinDecimal = 4;
    const currencyCode = pairDetail?.CurrencyCode || '';
    const coinCode = pairDetail?.CoinCode || '';

    if (marketHistory && marketHistory.length && marketHistory.length > 0) {
      marketHistoryLastOrder = marketHistory[0];
    }

    let buyButtonText = `${coinCode} ${Localization.t('CommonsFix.Buy')}`;
    let sellButtonText = `${coinCode} ${Localization.t(
      'MarketScreenFix.Sell',
    )}`;

    if (isAnonymousFlow) {
      buyButtonText = Localization.t('Drawer.Login');
      sellButtonText = Localization.t('Drawer.Login');
    }

    const formattedCurrencyBalance = FACurrencyAndCoinFormatter(
      Number(currencyBalanceDetail.Balance).toFixed(9),
      currencyDecimal,
    );
    const formattedCoinBalance = FACurrencyAndCoinFormatter(
      Number(coinBalanceDetail?.CoinBalance).toFixed(9),
      coinDecimal,
    );

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            {showUserUploadInformModal ? (
              <View style={{flex: 1, backgroundColor: theme?.colors?.vGray}}>
                <UploadUserInformationPage
                  closeModal={() =>
                    this.setState({
                      showUserUploadInformModal: false,
                    })
                  }
                  navigateSettingScreen={() =>
                    navigation.navigate('AccountSettings')
                  }
                />
              </View>
            ) : (
              <SafeAreaView
                style={{flex: 1, backgroundColor: theme?.colors?.white}}>
                <FAStockHeader
                  navigation={navigation}
                  pair={pairDetail}
                  favoriteDisable={isAnonymousFlow}
                  coinCode={MarketState.SelectedCoinCode}
                  currencyCode={MarketState.SelectedCurrencyCode}
                  dailyChange={change24}
                  onPressPair={() => this.setState({isPairSelectedBox: true})}
                  onPressChart={() => this.navigateToChartScreen()}
                  onPressMode={() => this.setState({isModeBoxOpen: true})}
                />

                <FAModeSelectorBox
                  onPressClose={() => this.setState({isModeBoxOpen: false})}
                  selectedMode={StockMode.Advanced}
                  onChangeMode={newStockMode =>
                    this.dispatchSelectedStockMode(newStockMode)
                  }
                  visible={isModeBoxOpen}
                />
                <FAPairSelectedBox
                  // onPressClose={() => this.setState({isModeBoxOpen: false})}
                  // selectedMode={StockMode.Advanced}
                  // onChangeMode={newStockMode =>
                  //   this.dispatchSelectedStockMode(newStockMode)
                  // }
                  onChangeSelectedPair={value =>
                    this.dispatchSelectedPairDetails(value)
                  }
                  selectedMarket={pairDetail}
                  onPressClose={() => this.setState({isPairSelectedBox: false})}
                  visible={isPairSelectedBox}
                />
                <FAAlertModal
                  visible={!_.isEmpty(errorMessage)}
                  text={errorMessage}
                  iconName={'times-circle'}
                  header={Localization.t('CommonsFix.Error')}
                  onPress={() => this.setState({errorMessage: false})}
                />
                <ScrollView
                  nestedScrollEnabled
                  style={[
                    styles.mainScrollView,
                    {backgroundColor: theme?.colors?.white},
                  ]}
                  refreshControl={
                    <RefreshControl
                      refreshing={isScreenRefreshing}
                      onRefresh={() => this.onRefresh()}
                    />
                  }>
                  <View style={styles.mainContainer}>
                    {leftSideMaxHeight ? (
                      <View
                        style={[
                          styles.leftSectionMainContainer,
                          {maxHeight: leftSideMaxHeight},
                        ]}>
                        {orderBookLoading ? (
                          <View
                            style={[
                              styles.loaderContainer,
                              styles.flex1,
                              {backgroundColor: theme?.colors?.white},
                            ]}>
                            <ActivityIndicator size={'small'} color={Orange} />
                          </View>
                        ) : (
                          <>
                            <View style={styles.orderLineTopTextContainer}>
                              <Text
                                style={[
                                  styles.orderLineTopPriceText,
                                  {color: theme?.colors?.black},
                                ]}>
                                {Localization.t('CommonsFix.Price')}
                              </Text>
                              <Text
                                style={[
                                  styles.orderLineTopAmountText,
                                  {color: theme?.colors?.black},
                                ]}>
                                {Localization.t('WalletFix.Amount')}
                              </Text>
                            </View>
                            <View style={styles.sellOrderBookContainer}>
                              <FlatList
                                nestedScrollEnabled
                                inverted
                                keyExtractor={(item, index) => index}
                                data={
                                  sellOrderBook
                                    ? sellOrderBook.slice(0, 15)
                                    : []
                                }
                                renderItem={({item, index}) =>
                                  this.renderOrderRowItem(item, true)
                                }
                              />
                            </View>

                            <TouchableOpacity
                              activeOpacity={0.7}
                              onPress={() =>
                                this.lastMarketOrderPriceExecutor(
                                  marketHistoryLastOrder,
                                )
                              }
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={[
                                  styles.orderBookLastOrderText,
                                  {
                                    color:
                                      marketHistoryLastOrder &&
                                      marketHistoryLastOrder.MatchType
                                        ? marketHistoryLastOrder.MatchType ===
                                          FAEnums.BUYFLOW
                                          ? theme?.colors?.green
                                          : theme?.colors?.red
                                        : 'black',
                                  },
                                ]}>
                                {lastPrice
                                  ? FACurrencyAndCoinFormatter(
                                      toFixedNoRounding(lastPrice, 3),
                                      3,
                                    )
                                  : null}
                              </Text>
                            </TouchableOpacity>

                            <View />
                            <View style={styles.buyOrderBookContainer}>
                              <FlatList
                                nestedScrollEnabled
                                keyExtractor={(item, index) => index}
                                data={
                                  buyOrderBook ? buyOrderBook.slice(0, 15) : []
                                }
                                renderItem={({item, index}) =>
                                  this.renderOrderRowItem(item, true)
                                }
                              />
                            </View>
                          </>
                        )}
                      </View>
                    ) : null}
                    <View
                      style={styles.rightSectionMainContainer}
                      onLayout={this.rightSideOnLayoutChange}>
                      <View style={styles.buttonContainer}>
                        <FAButton
                          onPress={() => this.handleFlowChange(BUYFLOW)}
                          text={Localization.t('MarketScreenFix.Buy')}
                          containerStyle={[
                            styles.buyFlowButtonContainer,
                            {
                              backgroundColor:
                                flow === FAEnums.BUYFLOW
                                  ? theme?.colors?.green
                                  : theme?.colors?.blueGray,
                            },
                          ]}
                          textStyle={[
                            styles.buyFlowButtonText,
                            {
                              color:
                                flow === FAEnums.BUYFLOW
                                  ? theme?.colors?.white2
                                  : theme?.colors?.darkBlue,
                            },
                          ]}
                        />
                        <FAButton
                          onPress={() => this.handleFlowChange(SELLFLOW)}
                          text={Localization.t('MarketScreenFix.Sell')}
                          containerStyle={[
                            styles.sellFlowButtonContainer,
                            {
                              backgroundColor:
                                flow === FAEnums.SELLFLOW
                                  ? theme?.colors?.red
                                  : theme?.colors?.blueGray,
                            },
                          ]}
                          textStyle={[
                            styles.sellFlowButtonText,
                            {
                              color:
                                flow === FAEnums.SELLFLOW
                                  ? theme?.colors?.white2
                                  : theme?.colors?.darkBlue,
                            },
                          ]}
                        />
                      </View>
                      <View style={styles.marginTop8}>
                        <FAExchangeOrderTypePicker
                          onValueChange={newMarketFlowType =>
                            this.handleMarketTypeChange(newMarketFlowType)
                          }
                          selectedFlow={currentMarketFlowType}
                          flowList={marketFlowTypeList}
                        />
                      </View>
                      {currentMarketFlowType !== StockMarketTypes.Market ? (
                        <View style={styles.marginTop8}>
                          <FATextInput.ExchangeCounter
                            decimalCurrencyCount={3}
                            onPressIncrement={() => {}}
                            placeholder={Localization.t(
                              'MarketScreenFix.Price',
                            )}
                            value={input.limitPrice}
                            onChangeValue={limitValue =>
                              this.calculation(null, null, limitValue)
                            }
                          />
                        </View>
                      ) : null}

                      {currentMarketFlowType === StockMarketTypes.Market ? (
                        <View style={styles.marginTop8}>
                          <FATextInput.ExchangeCounter
                            onPressIncrement={
                              flow === BUYFLOW ? () => {} : null
                            }
                            decimalCurrencyCount={3}
                            placeholder={Localization.t(
                              'MarketScreenFix.TotalPrice',
                            )}
                            value={input.totalPrice}
                            onChangeValue={incomingTotalPrice =>
                              this.calculation(incomingTotalPrice)
                            }
                          />
                        </View>
                      ) : null}
                      <View style={styles.marginTop8}>
                        <FATextInput.ExchangeCounter
                          decimalCoinCount={coinDecimal}
                          onPressIncrement={flow === BUYFLOW ? () => {} : null}
                          placeholder={Localization.t('WalletFix.Amount')}
                          value={input.amount}
                          onChangeValue={amountValue =>
                            this.calculation(null, amountValue)
                          }
                        />
                      </View>
                      {currentMarketFlowType === StockMarketTypes.Stop ? (
                        <View style={styles.marginTop8}>
                          <FATextInput.ExchangeCounter
                            decimalCurrencyCount={currencyDecimal}
                            placeholder={Localization.t(
                              'MarketScreenFix.StopPrice',
                            )}
                            value={input.stopPrice}
                            onChangeValue={stopPriceValue =>
                              this.setState({
                                input: {...input, stopPrice: stopPriceValue},
                              })
                            }
                          />
                        </View>
                      ) : null}
                      {currentMarketFlowType !== StockMarketTypes.Market ? (
                        <View style={styles.marginTop8}>
                          <FATextInput.ExchangeCounter
                            onPressIncrement={
                              flow === BUYFLOW ? () => {} : null
                            }
                            placeholder={Localization.t(
                              'MarketScreenFix.TotalPrice',
                            )}
                            value={input.totalPrice}
                            decimalCurrencyCount={currencyDecimal}
                            onChangeValue={incomingTotalPrice =>
                              this.calculation(incomingTotalPrice)
                            }
                          />
                        </View>
                      ) : null}
                      <View style={styles.rangeSliderContainer}>
                        <FARangeSlider
                          flowType={flow}
                          onSelectPercent={percentValue =>
                            this.handlePercentExecutor(percentValue)
                          }
                          currentPercent={input.percent}
                        />
                      </View>
                      <View style={styles.balanceLineContainer}>
                        {isAnonymousFlow ? null : (
                          <FABalanceLine
                            column
                            currency={
                              flow === BUYFLOW ? currencyCode : coinCode
                            }
                            balance={
                              flow === BUYFLOW
                                ? formattedCurrencyBalance
                                : formattedCoinBalance
                            }
                          />
                        )}
                      </View>
                      <View style={styles.buyButtonContainer}>
                        <FAButton
                          isLoading={createOrderLoading}
                          disabled={createOrderLoading}
                          onPress={() =>
                            isAnonymousFlow
                              ? this.navigateToLoginRegister()
                              : this.createOrder()
                          }
                          text={
                            flow === FAEnums.BUYFLOW
                              ? buyButtonText
                              : sellButtonText
                          }
                          containerStyle={[
                            styles.buyCoinButtonContainer,
                            {
                              backgroundColor:
                                flow === FAEnums.BUYFLOW
                                  ? theme?.colors?.green
                                  : theme?.colors?.red,
                            },
                          ]}
                          textStyle={[
                            styles.buyCoinButtonText,
                            {color: theme?.colors?.white2},
                          ]}
                        />
                      </View>
                    </View>
                  </View>
                  <TabView
                    style={styles.tabViewStyle}
                    renderTabBar={props => this.renderTabBar(props, theme)}
                    navigationState={{index, routes}}
                    renderScene={() => null}
                    onIndexChange={(indexValue: number) =>
                      this.onIndexChangeHandler(indexValue)
                    }
                    initialLayout={initialLayout}
                  />
                  {this.tabRenderByIndex(theme)}
                </ScrollView>
              </SafeAreaView>
            )}
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
    UserState: state.UserState,
  };
};
export default connect(mapStateToProps, {
  setStockModeAction,
  getUserWalletsAction,
  setSelectedPair,
  setSelectedCoinAndCurrencyCode,
})(StockAdvanced);
