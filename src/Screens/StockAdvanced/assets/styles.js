import {StyleSheet} from 'react-native';
import {
  Black,
  DarkBlueO,
  DarkGray,
  Orange,
  White,
  BlueGray,
  DarkBlue,
  VGray,
} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    paddingVertical: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flex1: {flex: 1},
  marginTop8: {marginTop: 8},
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 14,
  },
  leftSectionMainContainer: {flex: 0.4, marginLeft: 9},
  orderLineTopTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 8,
  },
  orderLineTopAmountText: {marginRight: 4, fontSize: 14, color: Black},
  orderLineTopPriceText: {marginLeft: 4, fontSize: 14, color: Black},
  sellOrderBookContainer: {flex: 1},
  buyOrderBookContainer: {flex: 1},
  orderBookLastOrderText: {
    textAlign: 'center',
    paddingVertical: 8,
    fontSize: 14,
  },
  rightSectionMainContainer: {flex: 0.6, marginHorizontal: 8},
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  activeOrderHeaderScrollView: {
    marginHorizontal: 10,
    marginVertical: 16,
  },
  activeOrderHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  activeOrderHeaderBuyButtonContainer: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 4,
  },
  activeOrderHeaderButtonText: {
    fontSize: 12,
    color: Black,
  },
  activeOrderHeaderSellButtonContainer: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 4,
    marginLeft: 8,
  },
  marketHistorySortingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: VGray,
  },
  marketHistoryDateSortingContainer: {
    flex: 1,
    paddingLeft: 10,
    backgroundColor: VGray,
    paddingVertical: 10,
  },
  marketHistoryAmountSortingContainer: {
    flex: 1,
    backgroundColor: VGray,
    paddingVertical: 10,
    alignItems: 'flex-end',
    marginLeft: 12,
  },
  marketHistoryPriceSortingContainer: {
    flex: 1,
    backgroundColor: VGray,
    paddingVertical: 10,
    alignItems: 'flex-end',
    paddingRight: 12,
  },
  marketHistoryFlatListSeperator: {
    borderWidth: 1,
    //TODO Renk statik verildi. Değiştirilecek
    borderColor: '#F5F6FA',
  },
  marketHistoryMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 12,
  },
  marketHistoryDateContainer: {flex: 1},
  marketHistoryDateText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryHourText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryCoinValue: {
    fontSize: 12,
    color: DarkGray,
    flex: 1,
    textAlign: 'right',
    marginLeft: 12,
  },
  marketHistoryCoinPrice: {
    fontSize: 12,
    flex: 1,
    textAlign: 'right',
    marginRight: 10,
  },
  tabBarLabel: {
    margin: -8,
    fontSize: 12,
  },
  tabBarContainerStyle: {
    borderColor: 'gray',
  },
  tabBarIndicatorStyle: {
    backgroundColor: Orange,
  },
  tabBarStyle: {
    backgroundColor: 'white',
  },
  mainScrollView: {flex: 1, backgroundColor: White},
  balanceLineContainer: {marginTop: 16},
  rangeSliderContainer: {marginTop: 10},
  buyButtonContainer: {marginTop: 17},
  tabViewStyle: {
    marginTop: 12,
  },
  moreAllContainerStyle: {
    marginTop: 10,
    paddingVertical: 10,
    backgroundColor: DarkBlueO,
  },
  moreAllText: {color: White},
  buyFlowButtonContainer: {
    paddingVertical: 10,
    flex: 1,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  buyFlowButtonText: {
    fontSize: 14,
  },
  sellFlowButtonContainer: {
    paddingVertical: 10,
    flex: 1,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
  sellFlowButtonText: {
    fontSize: 14,
  },
  buyCoinButtonContainer: {
    paddingVertical: 17,
    borderRadius: 4,
  },
  buyCoinButtonText: {
    fontSize: 14,
    color: White,
  },

  standardMainContainer: {flex: 1, backgroundColor: White},
  standardFlowButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  standardBuyFlowButtonContainer: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    paddingVertical: 9,
    flex: 1,
  },
  standardBuyFlowButtonText: {
    fontSize: 14,
  },
  standardSellFlowButtonContainer: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    paddingVertical: 9,
    flex: 1,
  },
  standardSellFlowButtonText: {
    fontSize: 14,
  },
  standardMainWrapper: {
    marginHorizontal: 10,
  },
  standardTabViewStyle: {
    marginTop: 10,
  },
  standardTabBarContainerStyle: {
    borderColor: 'gray',
  },
  standardTabBarIndicatorStyle: {
    backgroundColor: Orange,
  },
  standardTabBarStyle: {
    backgroundColor: 'transparent',
  },
  standardTabStyle: {width: 'auto'},
  standardTabBarLabel: {
    fontSize: 14,
    color: DarkBlue,
  },
  standardCurrentMarketFlowTypeButton: {
    borderBottomWidth: 2,
    paddingVertical: 5,
    paddingHorizontal: 13,
  },
  standardCurrentMarketFlowTypeButtonText: {fontSize: 14, color: DarkBlue},
  standardFormElementContainer: {marginTop: 16},
  standardCreateOrderButton: {
    marginBottom: 16,
  },
  standardOrderBookTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: BlueGray,
    paddingVertical: 7,
    justifyContent: 'space-between',
    marginHorizontal: -10,
  },
  standardOrderBookBuyTitle: {
    fontSize: 14,
    color: DarkBlue,
    textAlign: 'center',
    flex: 1,
    marginLeft: 10,
  },
  standardOrderBookSellTitle: {
    fontSize: 14,
    color: DarkBlue,
    textAlign: 'center',
    flex: 1,
    marginLeft: 10,
  },
  standardOrderBookHeadContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  orderBookHeadTitleContainer: {
    alignItems: 'center',
    marginBottom: 5,
  },
  standardOrderBookHeadTitle: {
    fontSize: 14,
    //TODO Statik verildi
    color: '#3B3B3B',
  },
  standardOrderBookHeadCoinOrCurrencyCode: {
    fontSize: 12,
    color: '#9B9B9B',
  },
  standardOrderBookContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
