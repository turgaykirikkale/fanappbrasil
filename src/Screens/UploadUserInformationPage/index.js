import React from 'react';
import {View, Text} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import _ from 'lodash';
import FAButton from '../../Components/Composite/FAButton';
import FAColor from '../../Commons/FAColor';
import {CommonActions} from '@react-navigation/native';

const UploadUserInformationPage = props => {
  const TextList = [
    Localization.t('UploadUserInformationPage.IdentityConfirmation'),
    // Localization.t('UploadUserInformationPage.AddressConfirmation'),
    Localization.t('UploadUserInformationPage.CustomerIdentificationForm'),
    Localization.t('UploadUserInformationPage.MyDocuments'),
  ];

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View
          style={{
            // borderTopLeftRadius: 50,
            // borderTopRightRadius: 50,
            position: 'absolute',
            bottom: 0,
            height: '100%',
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)',
            // alignItems: 'center',
            // justifyContent: 'center'
            paddingHorizontal: 20,
            zIndex: 999,
          }}>
          <View
            style={{
              backgroundColor: theme?.colors?.blueGray,
              marginHorizontal: 10,
              flex: 1,
              borderRadius: 4,
              justifyContent: 'center',
              paddingHorizontal: 10,
              marginBottom: 100,
              marginTop: 120,
            }}>
            <Text style={{color: theme?.colors?.black, letterSpacing: 0.6}}>
              {Localization.t('UploadUserInformationPage.DearUser')}
            </Text>
            <Text
              style={{
                color: theme?.colors?.black,
                letterSpacing: 0.6,
                marginVertical: 10,
              }}>
              {Localization.t('UploadUserInformationPage.explainTextOne')}
            </Text>
            <Text style={{color: theme?.colors?.black, letterSpacing: 0.6}}>
              {Localization.t('UploadUserInformationPage.explainTextSecond')}
            </Text>
            <View style={{marginVertical: 14}}>
              {_.map(TextList, item => {
                return (
                  <Text
                    style={{color: theme?.colors?.black, letterSpacing: 0.6}}>
                    {`- ${item}`}
                  </Text>
                );
              })}
            </View>
            <View style={{flexDirection: 'row'}}>
              <FAButton
                onPress={() =>
                  props.navigateSettingScreen && props.navigateSettingScreen()
                }
                text={Localization.t('UploadUserInformationPage.GotoSetting')}
                textStyle={{color: 'white', marginVertical: 6}}
                containerStyle={{
                  backgroundColor: FAColor.Orange,
                  borderRadius: 4,
                  flex: 1,
                  marginRight: 8,
                }}
              />
              <FAButton
                onPress={() => props.closeModal && props.closeModal()}
                text={Localization.t('UploadUserInformationPage.Cancel')}
                textStyle={{color: 'black', marginVertical: 6}}
                containerStyle={{
                  backgroundColor: FAColor.Gray,
                  borderRadius: 4,
                  flex: 1,
                  marginLeft: 8,
                }}
              />
            </View>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default UploadUserInformationPage;
