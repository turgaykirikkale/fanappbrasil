import {Black, DarkGray, SoftBlack, White} from '@Commons/FAColor';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  flexRow: {flexDirection: 'row', alignItems: 'center'},
  flex1: {flex: 1},
  scrollView: {flex: 1, backgroundColor: White},
  mainContainer: {
    paddingHorizontal: 10,
  },
  marketInformationContainer: {alignItems: 'center'},
  coinCodeAndCoinNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  coinCode: {color: Black, fontSize: 16, fontWeight: 'bold'},
  coinName: {color: DarkGray, fontSize: 16, marginLeft: 3},
  lastPrice: {
    color: Black,
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 4,
    paddingBottom: 25,
  },
  buyButtonContainer: {
    paddingVertical: 10,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  buyButtonText: {
    fontSize: 14,
    fontWeight: '600',
  },
  sellButtonContainer: {
    paddingVertical: 10,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
  sellButtonText: {
    fontSize: 14,
    fontWeight: '600',
  },
  transactionInformationContainer: {marginTop: 16, alignItems: 'center'},
  transactionInformation: {fontSize: 12, color: SoftBlack},
  inputContainer: {marginVertical: 24},
  createOrderButtonContainer: {
    paddingVertical: 15,
    marginTop: 17,
    borderRadius: 4,
  },
  createOrderButtonText: {fontSize: 16, fontWeight: '500', color: White},
});
