import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import FAModeSelectorBox from '@Components/Composite/FAModeSelectorBox';
import {BlueGray, DarkBlue, Green, Red, White} from '@Commons/FAColor';
import {stockCalculationHelper} from '@Helpers/StockCalculationHelper';
import {useMarketInformation} from '@Utils/Hooks/useMarketInformation';
import {setStockModeAction} from '@GlobalStore/Actions/MarketActions';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import FAStockHeader from '@Components/Composite/FAStockHeader';
import {orderBookDataRenderer} from '@Helpers/StockStreamLogic';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import React, {useCallback, useEffect, useState} from 'react';
import FATextInput from '@Components/Composite/FATextInput';
import FABalanceLine from '@Components/UI/FABalanceLine';
import FAButton from '@Components/Composite/FAButton';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import {toFixedNoRounding} from '@Commons/FAMath';
import Toast from 'react-native-toast-message';
import {AppConstants} from '@Commons/Contants';
import {StockMode} from '@Commons/FAEnums';
import {styles} from './assets/styles';
import {connect} from 'react-redux';
import FAService from '@Services';
import _ from 'lodash';
import {
  subscribeOrderBookTickerChannel,
  unSubscribeOrderBookTickerChannel,
  subscribeMarketHistoryChannel,
  unSubscribeMarketHistoryChannel,
  subscribeCustomerOrderTickerChannel,
  unSubscribeCustomerOrderTickerChannel,
} from '@Stream';
import {setDataToStorage} from '@Commons/FAStorage';

const StockEasyBuySellScreen = props => {
  const {navigation} = props;
  // Input States
  const [flow, setFlow] = useState(BUYFLOW);
  const [amount, setAmount] = useState(null);
  const [totalPrice, setTotalPrice] = useState(null);

  // Order Service states
  const [marketQueueName, setMarketQueueName] = useState(null);
  const [buyOrderBook, setBuyOrderBook] = useState([]);
  const [sellOrderBook, setSellOrderBook] = useState([]);
  const [lastOrderPrice, setLastOrderPrice] = useState(0.0);
  const [errorMessage, setErrorMessage] = useState(null);

  // Customer service states
  const [coinBalance, setCoinBalance] = useState(0.0);
  const [currencyBalance, setCurrencyBalance] = useState(0.0);

  // Stock mode states
  const [isModeBoxOpen, setIsModeBoxOpen] = useState(false);

  // Loading states
  const [orderBookLoading, setOrderBookLoading] = useState(true);
  const [createOrderLoading, setCreateOrderLoading] = useState(false);

  // Hook datas
  const {MarketQueueName} = useMarketInformation();

  useEffect(() => {
    const requestBody = {
      selectedCoinCode: AppConstants.CoinCode,
      selectedCurrencyCode: AppConstants.CurrencyCode,
    };
    if (MarketQueueName && marketQueueName) {
      setMarketQueueName(MarketQueueName);
      fillOrderBook(requestBody);
      fillMarketHistoryLastOrder(requestBody);
    } else {
      fillMarketQueueName();
    }
  }, [marketQueueName]);

  const findMatchedAssetBalance = (balanceList, assetId) => {
    return _.find(balanceList, {CoinId: assetId})?.CoinBalance;
  };

  useEffect(() => {
    const {UserState} = props;
    const userFinanceInfo = UserState?.FinanceInfo;
    if (userFinanceInfo?.CustomerCoinBalanceDetailList) {
      const customerCoinBalanceDetailList =
        userFinanceInfo.CustomerCoinBalanceDetailList;
      const matchedCurrencyBalance = findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        29,
      );
      const matchedCoinBalance = findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        AppConstants.CoinId,
      );
      setCoinBalance(
        FACurrencyAndCoinFormatter(
          matchedCoinBalance,
          AppConstants.CoinDecimalCount,
        ),
      );
      setCurrencyBalance(
        FACurrencyAndCoinFormatter(
          matchedCurrencyBalance,
          AppConstants.CurrencyDecimalCount,
        ),
      );
    }
  }, [props.UserState]);

  useEffect(() => {
    const {route} = props;
    const incomingFlow = route?.params?.flow;
    if (incomingFlow) {
      setFlow(incomingFlow);
    }
    return () => {
      unSubscribeOrderBookTickerChannel();
      unSubscribeMarketHistoryChannel();
      unSubscribeCustomerOrderTickerChannel();
    };
  }, []);

  const fillMarketQueueName = () => {
    const requestBody = {
      CurrencyId: AppConstants.CurrencyId,
      CoinId: AppConstants.CoinId,
    };
    FAService.GetMarketInformation(requestBody)
      .then(response => {
        if (response?.data && response.status === 200) {
          const responseData = response.data;
          setMarketQueueName(responseData?.MarketQueueName);
        }
      })
      .catch(err => {});
  };

  const fillOrderBook = requestBody => {
    setOrderBookLoading(true);
    FAService.GetActiveOrders(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const responseData = res.data;
          let buyOrders = _.sortBy(
            _.filter(responseData, {Type: BUYFLOW}),
            'Price',
          ).reverse();
          let sellOrders = _.sortBy(
            _.filter(responseData, {Type: SELLFLOW}),
            'Price',
          );
          setBuyOrderBook(percentExecutor(_.slice(buyOrders, 0, 15)));
          setSellOrderBook(percentExecutor(_.slice(sellOrders, 0, 15)));
          setOrderBookLoading(false);
          subscribeOrderBookTickerChannel(
            marketQueueName,
            orderBookChangeHandler,
          );
          subscribeCustomerOrderTickerChannel(
            marketQueueName,
            customerOrderChangeHandler,
          );
        }
      })
      .catch(err => console.log('err = ', err));
  };

  const orderBookChangeHandler = data => {
    const orderBook = orderBookDataRenderer(data);
    const generatedBuyOrderBook = _.sortBy(
      orderBook.BuyOrderBook,
      'Price',
    ).reverse();
    const generatedSellOrderBook = _.sortBy(orderBook.SellOrderBook, 'Price');
    if (generatedBuyOrderBook && generatedSellOrderBook) {
      setBuyOrderBook(_.slice(generatedBuyOrderBook, 0, 15));
      setSellOrderBook(_.slice(generatedSellOrderBook, 0, 15));
    }
  };

  const customerOrderChangeHandler = data => {
    const {getUserFinanceStateAction} = props;
    if (data) {
      setTimeout(() => {
        getUserFinanceStateAction();
      }, 1000);
    }
  };

  const fillMarketHistoryLastOrder = requestBody => {
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response?.data && response.status === 200) {
          const incomingLastOrderPrice = response.data[0]?.Price;
          if (incomingLastOrderPrice) {
            setLastOrderPrice(incomingLastOrderPrice);
            subscribeMarketHistoryChannel(
              marketQueueName,
              marketHistoryChangeHandler,
            );
          }
        }
      })
      .catch(err => console.log('Errr = ', err));
  };

  const marketHistoryChangeHandler = data => {
    if (data?.Price && lastOrderPrice !== data.Price) {
      setLastOrderPrice(data.Price);
    }
  };

  const calculation = incomingValue => {
    if (incomingValue) {
      const state = {
        currentMarketFlowType: StockMarketTypes.Market,
        percent: null,
        flow: flow,
        limitPrice: null,
        amount: amount,
        totalPrice: totalPrice,
        buyOrderBook: buyOrderBook,
        sellOrderBook: sellOrderBook,
      };
      let response = {};
      if (flow === BUYFLOW) {
        response = stockCalculationHelper(state, incomingValue);
        if (response?.amount) {
          setAmount(response.amount);
        }
      } else {
        response = stockCalculationHelper(state, null, incomingValue);
        if (response?.totalPrice) {
          setTotalPrice(response.totalPrice);
        }
      }
    }
  };

  const createOrder = () => {
    const {getUserFinanceStateAction} = props;
    let generatedPrice = 0;
    if (flow === BUYFLOW) {
      generatedPrice = sellOrderBook[0]?.Price;
    } else {
      generatedPrice = buyOrderBook[0]?.Price;
    }
    const requestBody = {
      CoinId: AppConstants.CoinId,
      CurrencyId: AppConstants.CurrencyId,
      Price: generatedPrice,
      Amount: toFixedNoRounding(Number(amount), 8),
      StopLossPrice: null,
      OrderType: flow,
    };
    setCreateOrderLoading(true);
    FAService.CreateOrder(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const resData = res.data;
          if (resData.IsSuccess) {
            getUserFinanceStateAction();
            Toast.show({
              type: 'success',
              position: 'top',
              text1: 'İşleminiz Başarılı',
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
            setCreateOrderLoading(false);
          } else {
            // IsSuccess değilse
            setErrorMessage(resData.Message);
          }
        } else {
          // Response 200 değilse
          setCreateOrderLoading(false);
        }
      })
      .catch(err => {
        setCreateOrderLoading(false);
        console.log('Err = ', err);
      });
  };

  const handleFlowChange = newFlow => {
    setFlow(newFlow);
    setAmount(null);
    setTotalPrice(null);
  };

  const navigateToChartScreen = () => {
    const coinCode = AppConstants.CoinCode;
    const currencyCode = AppConstants.CurrencyCode;
    const chartUrl = `https://chart.bitci.com/exchange/advanced/${coinCode}_${currencyCode}`;
    console.log('Chart url = ', chartUrl);
    navigation.navigate('WebviewScreen', {
      url: chartUrl,
      title: `${coinCode}/${currencyCode} Grafik`,
    });
  };

  const dispatchSelectedStockMode = async newStockMode => {
    if (newStockMode && newStockMode !== StockMode.EasyBuySell) {
      const {setStockModeAction} = props;
      setStockModeAction(newStockMode);
      setIsModeBoxOpen(false);
      await setDataToStorage('StockMode', newStockMode);
      navigation?.navigate('StockStandardScreen', {flow});
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: White}}>
      <FAModeSelectorBox
        selectedMode={StockMode.EasyBuySell}
        onChangeMode={newMode => dispatchSelectedStockMode(newMode)}
        visible={isModeBoxOpen}
      />
      <FAStockHeader
        onPressChart={() => navigateToChartScreen()}
        onPressMode={() => setIsModeBoxOpen(true)}
      />
      <FAAlertModal
        visible={!_.isEmpty(errorMessage)}
        text={errorMessage}
        iconName={'times-circle'}
        header={'hata'}
        onPress={() => setErrorMessage(null)}
      />
      <FAFullScreenLoader isLoading={orderBookLoading || createOrderLoading} />
      <ScrollView style={styles.scrollView}>
        <View style={styles.mainContainer}>
          <View style={styles.marketInformationContainer}>
            <View style={styles.coinCodeAndCoinNameContainer}>
              <Text style={styles.coinCode}>{AppConstants.CoinCode}</Text>
              <Text style={styles.coinName}>{AppConstants.CoinName}</Text>
            </View>
            <Text style={styles.lastPrice}>
              {FACurrencyAndCoinFormatter(lastOrderPrice, 4)}{' '}
              {AppConstants.CurrencyCode}
            </Text>
          </View>
          <View style={styles.flexRow}>
            <View style={styles.flex1}>
              <FAButton
                onPress={() => handleFlowChange(BUYFLOW)}
                text={`${AppConstants.CoinCode} Al`}
                containerStyle={[
                  styles.buyButtonContainer,
                  {backgroundColor: flow === BUYFLOW ? Green : BlueGray},
                ]}
                textStyle={[
                  styles.buyButtonText,
                  {color: flow === BUYFLOW ? White : DarkBlue},
                ]}
              />
            </View>
            <View style={styles.flex1}>
              <FAButton
                onPress={() => handleFlowChange(SELLFLOW)}
                text={`${AppConstants.CoinCode} Sat`}
                containerStyle={[
                  styles.sellButtonContainer,
                  {backgroundColor: flow === SELLFLOW ? Red : BlueGray},
                ]}
                textStyle={[
                  styles.sellButtonText,
                  {color: flow === SELLFLOW ? White : DarkBlue},
                ]}
              />
            </View>
          </View>
          <View style={styles.transactionInformationContainer}>
            {flow === BUYFLOW && (
              <Text style={styles.transactionInformation}>
                {AppConstants.CoinCode} almak istediğiniz{' '}
                {AppConstants.CurrencyCode} miktarını girin
              </Text>
            )}
            {flow === SELLFLOW && (
              <Text style={styles.transactionInformation}>
                Satmak istediğiniz {AppConstants.CoinCode} miktarını girin
              </Text>
            )}
          </View>
          <View style={styles.inputContainer}>
            {flow === BUYFLOW && (
              <FATextInput.BigLine
                onChangeValue={newValue => {
                  calculation(newValue);
                }}
                value={totalPrice}
                decimalCurrencyCount={4}
                placeholder={'1.200'}
                currencyOrCoinCode={AppConstants.CurrencyCode}
              />
            )}
            {flow === SELLFLOW && (
              <FATextInput.BigLine
                onChangeValue={newValue => {
                  calculation(newValue);
                }}
                value={amount}
                decimalCoinCount={2}
                placeholder={'1.200'}
                currencyOrCoinCode={AppConstants.CoinCode}
              />
            )}
          </View>
          <FABalanceLine
            currency={
              flow === BUYFLOW
                ? AppConstants.CurrencyCode
                : AppConstants.CoinCode
            }
            balance={flow === BUYFLOW ? currencyBalance : coinBalance}
          />
          <FAButton
            // onPress={() => createOrder()}
            text={`${AppConstants.CoinCode} ${
              flow === BUYFLOW ? 'Satın Al' : 'Sat'
            }`}
            containerStyle={[
              styles.createOrderButtonContainer,
              {backgroundColor: flow === BUYFLOW ? Green : Red},
            ]}
            textStyle={styles.createOrderButtonText}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {
  getUserFinanceStateAction,
  setStockModeAction,
})(StockEasyBuySellScreen);
