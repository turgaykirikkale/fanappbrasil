import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import FADrawerHeader from '@Components/Composite/FADrawerHeader';
import {VBlueGray} from '@Commons/FAColor';
import FANewsShower from '@Components/Composite/FANewsShower';
import _ from 'lodash';
import FAService from '@Services';
import FACustomTabView from '@Components/Composite/FACustomTabView';
import {DarkBlue} from '@Commons/FAColor';
import {sentenceShortener} from '@Helpers/StringHelper';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {connect} from 'react-redux';
import Localization from '@Localization';
import {getNewsAction} from '@GlobalStore/Actions/NewsActions';
import FAFanCard from '@Components/UI/FAFanCard';
import FAButton from '@Components/Composite/FAButton';
import {White} from '@Commons/FAColor';
import FATokenCard from '../../Components/UI/FATokenCard';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import rssParser from 'react-native-rss-parser';
import {getDataFromStorage} from '../../Commons/FAStorage';
import WebView from 'react-native-webview';

const HomeScreen = props => {
  const {navigation} = props;

  const mockData = props?.route?.params?.mockData;
  //Screen states
  const [news, setNews] = useState([]);
  const [sliderNews, setSliderNews] = useState([]);
  const [totalNewsCount, setTotalNewsCount] = useState(0);
  const [branches, setBranches] = useState([]);
  const [newsLoading, setNewsLoading] = useState(false);

  //Tab view states
  const [routes, setRoutes] = useState([
    {key: 'futbol', title: Localization.t('MockNewsItems.football')},
    {key: 'basketbol', title: Localization.t('MockNewsItems.basketball')},
    {key: 'motorspots', title: Localization.t('MockNewsItems.motoSports')},
    {key: 'e-sport', title: Localization.t('MockNewsItems.eSpor')},
    {key: 'social', title: Localization.t('MockNewsItems.social')},
  ]);
  const [tabIndex, setTabIndex] = useState(props?.route?.params?.index || 0);

  const [refreshing, setRefreshing] = useState(false);

  const {UserState} = props;
  const limit = 4;

  useEffect(() => {
    // fillNewsBranches(); //mock datadan sonra düzeltilecek
  }, []);

  useEffect(() => {
    // fillNewsBranches(); //mock datadan sonra düzeltilecek
    props.getNewsAction();
  }, [UserState.Language]);

  useEffect(() => {
    fillNews();
  }, [props.NewsState, tabIndex]);

  const fillNews = async triggeredByLoadMore => {
    const currentBranchId = branches[tabIndex]?.BranchId;
    const currentLanguageCode = await getDataFromStorage('Language');
    if (
      currentBranchId &&
      currentBranchId === 22 &&
      currentLanguageCode === 'en'
    ) {
      fetch('https://wp.bitcitech.com/west/feed/')
        .then(response => response.text())
        .then(responseData => rssParser.parse(responseData))
        .then(rss => {
          const rssItems = rss.items;
          let newItem = [];
          if (rssItems) {
            _.each(rssItems, item => {
              newItem.push({
                Branch: 'Motor Sports',
                BranchId: 22,
                Content: item.content,
                ContentUrl: item.links[0]?.url || null,
                CreatedOn: item.published,
                // Description: item.description,
                // Id: 9999,
                MainBranch: 'Motor Sports',
                Title: item.title,
                IsWebview: _.isEmpty(item.content),
                WebUrl: item.id,
                ContentType: 'PhotoNews',
              });
            });
            setNews(newItem);
            setSliderNews([]);
          }
        })
        .catch(err => {
          console.log('err', err);
        });
    } else if (!_.isEmpty(props.NewsState) && branches && branches[tabIndex]) {
      const matchedBranchName = branches[tabIndex].Name || null;
      if (!triggeredByLoadMore) {
        setNewsLoading(true);
      }
      if (matchedBranchName !== null) {
        let matchedNewsList = _.filter(props.NewsState, {
          MainBranch: matchedBranchName,
        });
        if (matchedNewsList?.length > 0) {
          const sliderNewsArray = _.filter(matchedNewsList, {InCarousel: true});
          const filteredNews = _.filter(matchedNewsList, {InCarousel: false});
          let start = news.length;
          let end = start + limit;
          if (!triggeredByLoadMore) {
            start = 0;
            end = limit;
          }
          const slicedNewsList = _.slice(filteredNews, start, end);
          if (triggeredByLoadMore) {
            setNews(prevState => [...prevState, ...slicedNewsList]);
          } else {
            setNews(slicedNewsList);
          }
          setSliderNews(sliderNewsArray || []);
          setTotalNewsCount(filteredNews?.length || 0);
        } else {
          setNews([]);
          setSliderNews([]);
        }
        if (!triggeredByLoadMore) {
          setNewsLoading(false);
        }
      } else {
        if (!triggeredByLoadMore) {
          setNewsLoading(false);
        }
      }
    }
  };

  const fillNewsBranches = () => {
    FAService.GetNewsBranches()
      .then(res => {
        if (res?.data && res.status === 200) {
          const resData = res.data;
          setBranches(resData);
          let newRoutes = [];
          _.each(resData, item => {
            newRoutes.push({
              key: item.Name.toLocaleLowerCase(),
              title: item.Name,
            });
          });
          setRoutes(newRoutes);
        }
      })
      .catch(err => console.log('Err => ', err));
  };
  const onChangeTabIndex = tabIndex => {
    setTabIndex(tabIndex);
  };

  const onRefresh = () => {
    setRefreshing(true);
    // fillNewsBranches();
    props.getNewsAction();
    setRefreshing(false);
  };

  const screenWidth = Dimensions.get('screen').width;
  const tokenInfo = {
    CoinId: 111,
    CoinCode: 'TMFT',
    CoinName: 'Türkiye Motosiklet Federasyonu Token',
    Price: 10.23,
    CurrencyCode: 'BITCI',
  };

  const navigationToWebSite = item => {
    // console.log('URL', `${item}`);
    // const siteUrl = `${item.Navigation}`;
    // navigation.navigate('WebviewScreen', {
    //   url: siteUrl,
    //   title: `${item.LongName}`,
    // });
    return (
      <View>
        <Text>Some Label</Text>

        <View style={{flex: 1}}>
          <WebView
            javaScriptEnabled={true}
            source={{html: '<h1>Some Heading</h1>'}}></WebView>
        </View>
      </View>
    );
  };

  let filteredData = [];
  // eslint-disable-next-line no-lone-blocks
  {
    _.map(mockData, item => {
      if (item.CategoryID === tabIndex) {
        filteredData.push(item);
      }
    });
  }
  const sorttedList = _.sortBy(filteredData, 'LongName');
  return (
    <View style={{flex: 1}}>
      <FAStandardHeader
        title={Localization.t('News')}
        navigation={navigation}
      />
      <FAFullScreenLoader isLoading={newsLoading} />
      <ThemeContext.Consumer>
        {({theme}) => (
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            nestedScrollEnabled
            scrollEnabled
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 20}}
            style={{
              flex: 1,
              backgroundColor: theme?.colors?.blueGray,
              position: 'relative',
            }}>
            <View
              style={{
                flex: 1,
              }}>
              <FACustomTabView
                currentIndex={tabIndex}
                routes={routes}
                onIndexChange={indexValue => onChangeTabIndex(indexValue)}
                itemCountPerPage={3}
              />

              <ScrollView>
                {_.map(sorttedList, item => {
                  // eslint-disable-next-line no-lone-blocks
                  {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          navigation.navigate('NewsWebView', {item: item})
                        }
                        activeOpacity={0.7}
                        style={{
                          borderRadius: 4,
                          marginTop: 10,
                          flex: 1,
                          marginHorizontal: 10,
                          backgroundColor: theme?.colors?.white,
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingVertical: 8,
                          paddingHorizontal: 8,
                        }}>
                        <Image
                          style={{
                            width: 50,
                            height: 50,
                            borderRadius: 25,
                          }}
                          source={{
                            uri: `https://borsa.bitci.com/img/coin/${item.Code}.png`,
                            headers: {
                              'User-Agent': item.Code.UserAgent,
                            },
                          }}
                        />
                        <Text
                          style={{
                            marginLeft: 6,
                            color: theme?.colors?.black,
                            marginVertical: 1,
                          }}>
                          {item.LongName}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={{
                            marginLeft: 6,
                            color: theme?.colors?.black,
                            marginVertical: 1,
                            flex: 1,
                            textAlign: 'right',
                            fontSize: 12,
                          }}>
                          >
                        </Text>
                      </TouchableOpacity>
                    );
                  }
                })}
              </ScrollView>
              {/* <View
                style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginHorizontal: 11,
                }}>
                <FATokenCard
                  onPress={() =>
                    navigation.navigate('TokenBuyScreen', {tokenInfo})
                  }
                  tokenCode={tokenInfo.CoinCode}
                  tokenName={tokenInfo.CoinName}
                  value={tokenInfo.Price}
                  currencyCode={tokenInfo.CurrencyCode}
                  // isCompleted
                />
              </View> */}

              {/* <View style={{marginTop: 10, marginHorizontal: 11}}>
                <Image
                  style={{
                    height: screenWidth / 2,
                    borderRadius: 6,
                    marginVertical: 0,
                  }}
                  source={{
                    uri: 'https://dapi.bitcitech.com/assets/2661b5ca-702e-44a9-9a8f-1cd5889430ab?format=webp&quality=50',
                  }}
                />
                {!_.isEmpty(sliderNews) && (
                  <View style={{marginTop: 20}}>
                    <FlatList
                      showsHorizontalScrollIndicator={false}
                      ItemSeparatorComponent={() => (
                        <View style={{marginLeft: 6}} />
                      )}
                      horizontal
                      data={sliderNews}
                      renderItem={({item, index}) => (
                        <FAFanCard
                          text={item.Title}
                          branchName={item.MainBranch || item.Branch || null}
                          image={item.ContentUrl}
                          onPress={() =>
                            props.navigation.navigate('NewsDetailScreen', {
                              news: item,
                            })
                          }
                          cardRatio={3}
                        />
                      )}
                    />
                  </View>
                )}
              </View> */}

              {/* <View
                style={{
                  paddingHorizontal: 11,
                  paddingVertical: 20,
                }}>
                {news?.length > 0 && (
                  <FlatList
                    ListFooterComponent={
                      news.length !== totalNewsCount
                        ? () => (
                            <FAButton
                              onPress={() => fillNews(true)}
                              text={'Devamını Gör'}
                              containerStyle={{
                                flex: 1,
                                backgroundColor: theme?.colors?.darkBlue,
                                marginTop: 20,
                                paddingVertical: 8,
                                borderRadius: 4,
                              }}
                              textStyle={{color: theme?.colors?.white2}}
                            />
                          )
                        : null
                    }
                    ItemSeparatorComponent={() => (
                      <View style={{marginTop: 20}} />
                    )}
                    keyExtractor={(item, index) => index}
                    data={news}
                    renderItem={({item}) => {
                      const isVideo =
                        item.ContentType !== 'PhotoNews' &&
                        item.ContentType !== 'Photo';
                      return (
                        <FANewsShower
                          video={isVideo}
                          onPress={() => {
                            props.navigation.navigate('NewsDetailScreen', {
                              news: item,
                            });
                          }}
                          publishDate={item.CreatedOn}
                          headerText={item.MainBranch || item.Branch || null}
                          explainedText={sentenceShortener(item.Title, 55)}
                          content={sentenceShortener(item.Description, 150)}
                          image={item.ContentUrl}
                          language={UserState?.Language || 'tr'}
                        />
                      );
                    }}
                  />
                )}
              </View> */}
            </View>
            {_.isEmpty(filteredData) && ( //filtered data news olarak değiştirilecek
              <View
                style={{
                  flex: 1,
                  paddingHorizontal: 10,
                  // backgroundColor: theme?.colors?.white,
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 12,
                    color: theme?.colors?.darkBlue,
                  }}>
                  {/*{branches[tabIndex]?.Name} KATEGORİSİNDE HABER BULUNAMADI*/}
                  {branches[tabIndex]?.Name
                    ? Localization.t('NotFoundNews', {
                        category: branches[tabIndex].Name,
                      })
                    : Localization.t('NotFoundContent')}
                </Text>
              </View>
            )}
          </ScrollView>
        )}
      </ThemeContext.Consumer>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    NewsState: state.NewsState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {getNewsAction})(HomeScreen);
