import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAMarketItem from '@Components/Composite/FAMarketItem';
import FASearchBar from '@Components/Composite/FASearchBar';
import {FlatList, RefreshControl, View} from 'react-native';
import * as Analytics from 'appcenter-analytics';
import React, {useEffect, useState} from 'react';
import DapiService from '@Services/DapiService';
import _ from 'lodash';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const FanTokenInfoScreen = props => {
  const {navigation} = props;
  const [tokenList, setTokenList] = useState([]);
  const [searchValue, setSearchValue] = useState(null);
  const [isRefreshing, setIsRefreshing] = useState(false);

  useEffect(() => {
    getFanTokens();
  }, []);

  const getFanTokens = (onRefresh = false) => {
    if (onRefresh) {
      setIsRefreshing(true);
    }
    DapiService.GetFanTokens()
      .then(res => {
        if (res?.data?.data) {
          let resData = res.data.data;
          if (resData) {
            resData = _.filter(resData, item => item.contract_address);
            resData = _.sortBy(resData, 'coin_code');
            setTokenList(resData);
            if (onRefresh) {
              setIsRefreshing(false);
            }
          }
        }
      })
      .catch(err => console.log(err));
  };

  const onRefresh = () => {
    getFanTokens(true);
  };

  const navigateToWebview = (token, theme) => {
    Analytics.trackEvent('Fan Token Info Display', {
      Coin: `${token?.coin_code || null} - ${token?.coin_name || null}`,
    });
    navigation.navigate('WebviewScreen', {
      title: `${token.coin_code} Bilgi`,
      url: `https://bitcicom.vercel.app/tr/projects/${token.coin_code}?mode=${
        theme?.name || null
      }&custom=fanapp`,
    });
  };

  const filterTokenList = () => {
    if (tokenList?.length > 0 && searchValue?.length > 0) {
      let filteredList = [];
      const lowerCasedSearchText = searchValue.toLowerCase();
      _.each(tokenList, item => {
        if (
          item.coin_code.toLowerCase().indexOf(lowerCasedSearchText) >= 0 ||
          item.coin_name.toLowerCase().indexOf(lowerCasedSearchText) >= 0
        ) {
          filteredList.push(item);
        }
      });
      return filteredList;
    }
    return tokenList;
  };

  const fanTokenList = filterTokenList();
  console.log('fanTokenList', fanTokenList);
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <>
          <FAStandardHeader title={'Fan Token Info'} navigation={navigation} />
          <FASearchBar
            value={searchValue}
            onChangeText={newValue => setSearchValue(newValue)}
            placeholder={'Koin kodu veya adı ile ara'}
          />
          <FlatList
            style={{backgroundColor: theme?.colors?.white}}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
            }
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: theme?.colors?.blueGray,
                }}
              />
            )}
            data={fanTokenList}
            renderItem={({item, index}) => (
              <FAMarketItem
                key={index}
                imageUri={`https://bitci.com/page/assets/coin?code=${item.coin_code}`}
                onPress={() => navigateToWebview(item, theme)}
                imageName={item.coin_code}
                coinName={item.coin_code}
                coinLongName={item.coin_name}
              />
            )}
          />
        </>
      )}
    </ThemeContext.Consumer>
  );
};

export default FanTokenInfoScreen;
