import React, {Component} from 'react';
import {View} from 'react-native';
import FATextInput from '@Components/Composite/FATextInput';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import autobind from 'autobind-decorator';
import FAForm from '@ValidationEngine/Form';
import FAService from '@Services';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class ResetPasswordStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneCode:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.item &&
        this.props.route.params.item.code
          ? this.props &&
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.item &&
            this.props.route.params.item.code
          : '+55',
      flag:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.item &&
        this.props.route.params.item.flag
          ? this.props &&
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.flag
          : 'Brazil',
      purePhoneNumber: '',
      email: '',
      FormIsValid: false,
      showDialog: false,
      serviceMessage: null,
    };
  }

  @autobind
  onPhoneSelect({code, flag}) {
    this.setState({
      phoneCode: code,
      flag: flag,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.route.params.item.code !== this.state.phoneCode) {
      this.setState({
        purePhoneNumber: '',
        phonePrefix: nextProps.route.params.item.code,
        flag: nextProps.route.params.item.flag,
      });
    }
  }

  passwordResetServiceCall() {
    const {purePhoneNumber, email} = this.state;
    const requestBody = {Mail: email, Phone: purePhoneNumber};
    FAService.PasswordReset(requestBody)
      .then(response => {
        if (
          response &&
          response.data &&
          response.status === 200 &&
          response.data.Success
        ) {
          this.props.navigation.navigate('ResetPasswordStep2', {
            userContactInfo: requestBody,
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {});
  }

  @autobind
  onChangePhoneNumber(formatted, extracted) {
    const newPhonePrefix = this.state.phoneCode;
    let generatedPhoneNumberValue = extracted;
    if (extracted.indexOf(newPhonePrefix) > -1) {
      generatedPhoneNumberValue = extracted.replace(newPhonePrefix, '');
    }
    this.setState({
      purePhoneNumber: generatedPhoneNumberValue,
      fullPhoneNumber: extracted,
      phonePrefix: newPhonePrefix,
      formattedValue: formatted,
    });
  }

  @autobind
  navigate() {
    const {navigation} = this.props;
    navigation.navigate('CountrySelectScreen', {
      onPhoneSelect: this.onPhoneSelect,
    });
  }

  render() {
    const {
      phoneCode,
      flag,
      purePhoneNumber,
      email,
      showDialog,
      serviceMessage,
      FormIsValid,
      formattedValue,
      fullPhoneNumber,
    } = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <View
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <FAAlertModal
                visible={showDialog}
                text={serviceMessage}
                iconName={'times-circle'}
                header={Localization.t('Commons.Error')}
                onPress={() => this.setState({showDialog: false})}
              />
              <FAStandardHeader
                title={Localization.t('ResetPasswordScreen.HeaderTitle')}
                navigation={navigation}
              />

              <KeyboardAwareScrollView style={styles.childContainer}>
                <FAForm
                  formValidSituationChanged={value =>
                    this.setState({
                      FormIsValid: value,
                    })
                  }>
                  <FATextInput.PhoneNumber
                    keyboardType={'phone-pad'}
                    rule={
                      phoneCode === '+90'
                        ? 'purePhoneNumberValue'
                        : 'notEmptyAndNil'
                    }
                    phonePrefix={phoneCode}
                    errorMessage={Localization.t(
                      'CreateAccountScreen.CorrectFormatPhone',
                    )}
                    selectedItem={{
                      id: 1,
                      phoneCode: phoneCode,
                      flag: flag,
                    }}
                    placeholder={'555 555 55 55'}
                    label={Localization.t('Commons.Phone')}
                    onChangeValue={(formatted, extracted) =>
                      this.onChangePhoneNumber(formatted, extracted)
                    }
                    value={formattedValue}
                    notFormattedValue={fullPhoneNumber}
                    mask={
                      phoneCode === '+90'
                        ? '[000] [000] [00] [00]'
                        : '[00000000000000]'
                    }
                    onPress={() => this.navigate()}
                  />
                  <View style={styles.marginTop16} />
                  <FATextInput
                    label={Localization.t(
                      'ResetPasswordScreen.EmailAddressLabel',
                    )}
                    placeholder={Localization.t(
                      'ResetPasswordScreen.EmailAddressLabel',
                    )}
                    rule={'isEmail'}
                    errorMessage={Localization.t(
                      'LoginScreen.correctEmailFormat',
                    )}
                    onChangeValue={newWalue =>
                      this.setState({
                        email: newWalue,
                      })
                    }
                    value={email}
                  />
                </FAForm>
                <FAButton
                  disabled={!FormIsValid}
                  onPress={() => this.passwordResetServiceCall()}
                  containerStyle={[
                    styles.buttonContainer,
                    {backgroundColor: theme?.colors?.success},
                  ]}
                  textStyle={[
                    styles.buttonTextStyle,
                    {color: theme?.colors?.white2},
                  ]}
                  text={Localization.t(
                    'ResetPasswordScreen.ResetMyPasswordText',
                  )}
                />
              </KeyboardAwareScrollView>
            </View>
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}
