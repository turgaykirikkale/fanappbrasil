import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  childContainer: {marginTop: 16, marginHorizontal: 10},
  headerTextStyle: {fontSize: 20, color: '#3B3B3B', marginBottom: 16}, //TO DO Color
  marginTop16: {marginTop: 16},
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 16,
  },
  buttonTextStyle: {
    marginVertical: 15,
    color: FAColor.White,
  },
  smsApproveText: {
    fontSize: 16,
    color: FAColor.DarkGray,
    marginBottom: 8,
    textAlign: 'center',
    letterSpacing: -0.5,
  },
  smsTimerText: {
    fontSize: 14,
    color: FAColor.Gray,
    marginBottom: 16,
    textAlign: 'center',
    letterSpacing: -0.5,
  },
  emailApproveText: {
    fontSize: 16,
    color: FAColor.DarkGray,
    marginBottom: 8,
    textAlign: 'center',
    marginTop: 16,
    letterSpacing: -0.5,
  },
  emailTimerText: {
    fontSize: 14,
    color: FAColor.Gray,
    marginBottom: 16,
    textAlign: 'center',
    letterSpacing: -0.5,
  },
});
