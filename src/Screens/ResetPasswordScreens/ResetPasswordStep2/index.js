import React, {Component} from 'react';
import {View, Text, AppState} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FATextInput from '@Components/Composite/FATextInput';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import autobind from 'autobind-decorator';
import _ from 'lodash';
import FAForm from '@ValidationEngine/Form';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import moment from 'moment';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';

export default class ResetPasswordStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userContactInfo:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.userContactInfo,
      SMSCode: '',
      EmailCode: '',
      countDownTime: 79,
      FormIsValid: false,
      appState: AppState.currentState,
      backgroundTime: null,
      foregroundTime: null,
    };
  }

  onFinishcountDownTimer() {
    return console.log('finishFunction');
  }
  @autobind
  countDownLogic() {
    const {countDownTime} = this.state;
    if (typeof countDownTime === 'number') {
      const incomingTimeObject = this.calculateMinAndSec();
      if (
        incomingTimeObject &&
        !_.isNil(incomingTimeObject.min) &&
        !_.isNil(incomingTimeObject.sec)
      ) {
        const {min, sec} = incomingTimeObject;
        this.setState({
          countDownTime: countDownTime - 1,
          min,
          sec,
        });
        if (countDownTime === 0) {
          this.onFinishcountDownTimer();
          clearInterval(this.timerInterval);
        }
      }
    }
  }

  componentWillMount() {
    const {countDownTime} = this.state;
    if (countDownTime) {
      this.timerInterval = setInterval(this.countDownLogic, 1000);
    }
  }
  componentDidMount() {
    this.appStateSubscription = AppState.addEventListener(
      'change',
      nextAppState => {
        if (
          this.state.appState.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          const {countDownTime, backgroundTime} = this.state;

          if (backgroundTime) {
            let now = moment();
            let different = moment.duration(now.diff(backgroundTime));
            let newCountDownTime = countDownTime;
            different = different.asSeconds();
            // eslint-disable-next-line no-const-assign
            newCountDownTime -= different;
            newCountDownTime = parseInt(newCountDownTime);
            if (newCountDownTime < 0) {
              newCountDownTime = 0;
            }
            this.setState({
              backgroundTime: null,
              countDownTime: newCountDownTime,
            });
          }
        } else {
          this.setState({
            backgroundTime: moment(),
          });
          console.log('App has come to the background!');
        }
        this.setState({appState: nextAppState});
      },
    );
  }

  @autobind
  calculateMinAndSec() {
    const {countDownTime} = this.state;
    if (typeof countDownTime === 'number') {
      const min = parseInt(countDownTime / 60, 10);
      const sec = countDownTime - min * 60;
      return {min, sec};
    }
  }
  @autobind
  displayTimeLogic(value) {
    if (value / 10 >= 1) {
      return String(value);
    }
    return '0' + value;
  }
  @autobind
  displayCountDownTime() {
    const {countDownTime} = this.state;
    const {min, sec} = this.state;
    if (!_.isNil(countDownTime) && !_.isNil(min) && !_.isNil(sec)) {
      return (
        <Text style={styles.countDownTimerStyle}>
          {this.displayTimeLogic(min)}:{this.displayTimeLogic(sec)}
        </Text>
      );
    }
  }

  render() {
    const {SMSCode, EmailCode, userContactInfo, FormIsValid} = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              title={Localization.t('ResetPasswordScreen.HeaderTitle')}
              navigation={navigation}
            />
            <KeyboardAwareScrollView
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <View style={styles.childContainer}>
                <FAForm
                  formValidSituationChanged={value =>
                    this.setState({
                      FormIsValid: value,
                    })
                  }>
                  <Text
                    style={[
                      styles.headerTextStyle,
                      {color: theme?.colors?.black},
                    ]}>
                    {Localization.t('ResetPasswordScreen.Step2InformationText')}
                  </Text>

                  <FATextInput
                    keyboardType={'numeric'}
                    label={Localization.t(
                      'ResetPasswordScreen.SmsVerificationCodeLabel',
                    )}
                    placeholder={Localization.t(
                      'ResetPasswordScreen.SmsVerificationCodeLabel',
                    )}
                    onChangeValue={newWalue =>
                      this.setState({
                        SMSCode: newWalue,
                      })
                    }
                    value={SMSCode}
                    rule={'isOtp'}
                    errorMessage={Localization.t('OtpScreen.ErrorMessage')}
                    mask={'[000000]'}
                  />
                  <View style={styles.marginTop16} />

                  <FATextInput
                    label={Localization.t(
                      'ResetPasswordScreen.EmailVerificationCodeLabel',
                    )}
                    placeholder={Localization.t(
                      'ResetPasswordScreen.EmailVerificationCodeLabel',
                    )}
                    onChangeValue={newWalue =>
                      this.setState({
                        EmailCode: newWalue,
                      })
                    }
                    value={EmailCode}
                    rule={'notEmptyAndNil'}
                    errorMessage={Localization.t('OtpScreen.ErrorMessage')}
                  />
                  <FAButton
                    disabled={!FormIsValid}
                    onPress={() =>
                      navigation.navigate('ResetPasswordStep3', {
                        userContactInfoAndCodes: {
                          ...userContactInfo,
                          VerifiCodeFactor: SMSCode,
                          VerifiCodeMail: EmailCode,
                        },
                      })
                    }
                    containerStyle={styles.buttonContainer}
                    textStyle={styles.buttonTextStyle}
                    text={Localization.t('OtpScreen.VerifyButtonText')}
                  />
                  {/* <Text style={styles.smsApproveText}>
              {BMALocalization.t('ResetPasswordScreen.notreceived')}
            </Text>
            <Text style={styles.smsTimerText}>
              {this.displayCountDownTime()
                ? this.displayCountDownTime()
                : '01.20'}{' '}
              {BMALocalization.t('OtpScreen.SubInformationContent')}
            </Text>
            <FADivider text={'VEYA'} />
            <Text style={styles.emailApproveText}>
              {BMALocalization.t('ResetPasswordScreen.notreceivedEmail')}
            </Text>
            <Text style={styles.emailTimerText}>
              {this.displayCountDownTime()
                ? this.displayCountDownTime()
                : '01.20'}{' '}
              {BMALocalization.t('OtpScreen.SubInformationContentEmail')}
            </Text> */}
                </FAForm>
              </View>
            </KeyboardAwareScrollView>
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}
