import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  childContainer: {marginTop: 16, marginHorizontal: 10},
  marginTop16: {marginTop: 16},
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginTop: 16,
  },
  buttonTextStyle: {
    marginVertical: 15,
    color: FAColor.White,
  },
});
