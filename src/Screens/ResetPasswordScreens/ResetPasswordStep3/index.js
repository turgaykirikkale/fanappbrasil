import React, {Component} from 'react';
import {SafeAreaView, View, TouchableOpacity} from 'react-native';
import FATextInput from '@Components/Composite/FATextInput';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import autobind from 'autobind-decorator';
import FAForm from '@ValidationEngine/Form';
import Toast from 'react-native-toast-message';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import _ from 'lodash';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAService from '@Services';
import {CommonActions} from '@react-navigation/native';
import Localization from '@Localization';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class ResetPasswordStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      showNewPassword: false,
      repeatNewPassword: '',
      showRepeatNewPassword: false,
      userContactInfoAndCodes:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.userContactInfoAndCodes,
      FormIsValid: false,
      showDialog: false,
      serviceMessage: null,
      showPassword: false,
      showRepeatPassword: false,
    };
  }
  @autobind
  passwordResetServiceCall() {
    const {userContactInfoAndCodes, newPassword} = this.state;
    const {navigation} = this.props;
    const requestBody = {
      ...userContactInfoAndCodes,
      NewPassword: newPassword,
    };

    this.setState({isLoading: true});
    FAService.PasswordReset(requestBody)
      .then(response => {
        if (
          response &&
          response.data &&
          response.status === 200 &&
          response.data.Success
        ) {
          Toast.show({
            type: 'success',
            position: 'top',
            text1: Localization.t('Commons.Succes'),
            text2: Localization.t('Commons.SuccessfulTransaction'),
            visibilityTime: 750,
            autoHide: true,
            topOffset: 50,
            bottomOffset: 40,
            onShow: () => {},
            onHide: () => {},
            onPress: () => {},
          });
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: 'FanTabStackNavigator', screen: 'LoginScreen'}],
            }),
          );
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false,
        });
        console.log(error);
      });
  }

  render() {
    const {
      newPassword,
      repeatNewPassword,
      showDialog,
      serviceMessage,
      FormIsValid,
      showPassword,
      showRepeatPassword,
    } = this.state;
    const {navigation} = this.props;
    let disableButton = true;

    if (
      FormIsValid &&
      !_.isEqual(newPassword, '') &&
      !_.isEqual(repeatNewPassword, '') &&
      _.isEqual(repeatNewPassword, newPassword)
    ) {
      disableButton = false;
    }
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              title={Localization.t('ResetPasswordScreen.HeaderTitle')}
              navigation={navigation}
            />
            <KeyboardAwareScrollView
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <FAAlertModal
                visible={showDialog}
                text={serviceMessage}
                iconName={'times-circle'}
                header={Localization.t('Commons.Error')}
                onPress={() => this.setState({showDialog: false})}
              />

              <View style={styles.childContainer}>
                <FAForm
                  formValidSituationChanged={value =>
                    this.setState({
                      FormIsValid: value,
                    })
                  }>
                  <FATextInput.Password
                    isPasswordShowing={!showPassword}
                    onPressRightIcon={() =>
                      this.setState({
                        showPassword: !showPassword,
                      })
                    }
                    label={Localization.t(
                      'ResetPasswordScreen.NewPasswordLabel',
                    )}
                    rule={'userPasswordRule'}
                    errorMessage={Localization.t('Commons.PasswordRule')}
                    name={'Şifre Input'}
                    value={newPassword}
                    onChangeValue={value =>
                      this.setState({
                        newPassword: value,
                      })
                    }
                    placeholder={Localization.t(
                      'ResetPasswordScreen.NewPasswordLabel',
                    )}
                  />

                  <View style={styles.marginTop16} />

                  <FATextInput.Password
                    isPasswordShowing={!showRepeatPassword}
                    onPressRightIcon={() =>
                      this.setState({
                        showRepeatPassword: !showRepeatPassword,
                      })
                    }
                    label={Localization.t(
                      'ResetPasswordScreen.repeatNewPassword',
                    )}
                    rule={'userPasswordRule'}
                    errorMessage={Localization.t('Commons.PasswordRule')}
                    name={'Şifre Input'}
                    value={repeatNewPassword}
                    onChangeValue={value =>
                      this.setState({
                        repeatNewPassword: value,
                      })
                    }
                    placeholder={Localization.t(
                      'ResetPasswordScreen.repeatNewPassword',
                    )}
                  />
                </FAForm>
                <FAButton
                  disabled={disableButton}
                  onPress={() => this.passwordResetServiceCall()}
                  containerStyle={styles.buttonContainer}
                  textStyle={styles.buttonTextStyle}
                  text={Localization.t('ResetPasswordScreen.changePassoword')}
                />
              </View>
            </KeyboardAwareScrollView>
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}
