import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {getDataFromStorage} from '@Commons/FAStorage';
import React, {useEffect, useState} from 'react';
import {StockMode} from '@Commons/FAEnums';
import {connect} from 'react-redux';
import {ActivityIndicator, SafeAreaView, View} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import {FAOrange, White} from '../../Commons/FAColor';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const StockFlowExecutor = props => {
  const [isLoading] = useState(false);

  const executor = async () => {
    let stockMode = await getDataFromStorage('StockMode');
    if (!stockMode) {
      const {MarketState} = props;
      stockMode = MarketState?.StockMode;
    }
    navigateToSelectedStockMode(stockMode);
  };

  const navigateToSelectedStockMode = mode => {
    if (mode) {
      if (mode === StockMode.EasyBuySell) {
        resetStackAndNavigate('StockEasyBuySellScreen', 0);
      } else if (mode === StockMode.Standard) {
        resetStackAndNavigate('StockStandardScreen', 1);
      } else {
        resetStackAndNavigate('StockAdvanced', 2);
      }
    }
  };

  const resetStackAndNavigate = (screenName, index) => {
    const {navigation} = props;
    navigation.dispatch(
      CommonActions.reset({
        index: index,
        routes: [{name: screenName}],
      }),
    );
  };

  useEffect(() => {
    executor();
  }, []);

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <SafeAreaView
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme?.colors?.white,
          }}>
          <ActivityIndicator color={FAOrange} size={'large'} />
        </SafeAreaView>
      )}
    </ThemeContext.Consumer>
  );
};

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps)(StockFlowExecutor);
