import {StyleSheet} from 'react-native';
import {DarkBlue, FAOrange, SoftBlack, White} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  scrollView: {flex: 1},
  contentContainer: {marginVertical: 12, marginHorizontal: 16},
  branch: {
    fontSize: 10,
    color: DarkBlue,
  },
  title: {fontSize: 14, color: FAOrange, marginVertical: 14},
  publishDateContainer: {
    paddingVertical: 6,
    backgroundColor: White,
    paddingHorizontal: 16,
    marginTop: 6,
  },
  publishDate: {fontSize: 10, color: DarkBlue, textAlign: 'center'},
  sameNewsContainer: {marginHorizontal: 12, marginVertical: 12},
  sameNewsTitle: {fontSize: 12, fontWeight: 'bold', color: FAOrange},
  sameNewsContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingRight: 12,
    marginVertical: 8,
  },
  sameNewsImage: {height: 75, width: 75},
  sameNewsContent: {marginLeft: 12},
});
