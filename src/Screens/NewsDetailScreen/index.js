import {
  Dimensions,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {RenderHTML} from 'react-native-render-html';

import Localization from '@Localization';
import {styles} from './assets/styles';
import moment from 'moment';
import React, {useRef, useState} from 'react';
import _ from 'lodash';
import {useIsFocused} from '@react-navigation/core';
import {sentenceShortener} from '@Helpers/StringHelper';

import {ThemeContext} from '../../Utils/Theme/ThemeProvider';


const NewsDetailScreen = props => {
  const newsDetail = props?.route?.params?.news;

  if (_.isUndefined(newsDetail) || _.isEmpty(newsDetail)) {
    return null;
  }
  const contentSource = {
    html: `${newsDetail.content || ''}`,
  };

  const renderersProps = {
    img: {
      enableExperimentalPercentWidth: true,
    },
  };
  const screenWidth = Dimensions.get('screen').width;
  const language = Localization.language || 'en';
  const isFocused = useIsFocused();
  if (!isFocused) {
    return null;
  }

  const [similarNews, setSimilarNews] = useState([]);
  const scrollRef = useRef();

  return (
    <>
      <FAStandardHeader
        title={`${newsDetail.title?.toLocaleUpperCase() || ''}`}
        navigation={props.navigation}
      />
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <ScrollView
              style={[
                styles.scrollView,
                {backgroundColor: theme?.colors?.blueGray},
              ]}
              ref={scrollRef}>
              {/* <Image
              source={{uri: newsDetail.ContentUrl}}
              style={{height: screenWidth / 1.75}}
            /> */}
              <View style={styles.contentContainer}>
                <Text
                  style={[styles.branch, {color: theme?.colors?.darkBlue}]}>{`${
                  newsDetail.MainBranch || ''
                }`}</Text>
                <Text style={[styles.title, {color: theme?.colors?.faOrange}]}>
                  {newsDetail.title}
                </Text>
                <RenderHTML
                  contentWidth={screenWidth - 50}
                  renderersProps={renderersProps}
                  source={contentSource}
                  tagsStyles={{
                    body: {
                      color: theme?.colors?.darkBlue,
                    },
                  }}
                />
              </View>
              <View
                style={[
                  styles.publishDateContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <Text
                  style={[
                    styles.publishDate,
                    {color: theme?.colors?.faOrange},
                  ]}>
                  {moment(newsDetail.published).locale(language).fromNow() ||
                    ''}
                </Text>
              </View>
              {similarNews?.length > 0 && (
                <View style={styles.sameNewsContainer}>
                  <Text
                    style={[
                      styles.sameNewsTitle,
                      {color: theme?.colors?.faOrange},
                    ]}>
                    İlgili Haberler
                  </Text>
                  {similarNews?.map((item, index) => (
                    <TouchableOpacity
                      key={index}
                      onPress={() => {
                        props.navigation.navigate('NewsDetailScreen', {
                          news: item,
                        });
                        if (scrollRef !== null && scrollRef.current) {
                          scrollRef.current?.scrollTo({
                            y: 0,
                            animated: true,
                          });
                        }
                      }}
                      activeOpacity={1}
                      style={[
                        styles.sameNewsContentContainer,
                        {
                          marginTop: index === 0 ? 8 : null,
                          backgroundColor: theme?.colors?.white,
                        },
                      ]}>
                      <Image
                        source={{
                          uri: item.ContentUrl || '',
                        }}
                        style={styles.sameNewsImage}
                        resizeMode={'cover'}
                      />
                      <View style={{width: screenWidth - (36 + 75)}}>
                        <Text
                          style={[
                            styles.sameNewsContent,
                            {color: theme?.colors?.black},
                          ]}>
                          {sentenceShortener(item.Title || '', 50)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  ))}
                </View>
              )}
            </ScrollView>
          </>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default NewsDetailScreen;
