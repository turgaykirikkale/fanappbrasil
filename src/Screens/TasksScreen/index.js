import React from 'react';
import {View, Text} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {ScrollView} from 'react-native-gesture-handler';
import TaskItems from '../../Components/UI/TasksItems';
import TasksHeader from '../../Components/Composite/TasksHeader';
import Localization from '@Localization';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const TasksScreen = props => {
  const {navigation} = props;
  const inset = useSafeAreaInsets();
  const passive = true;

  const mockData = [
    {
      header: 'Günlük Görevler',
      DaysLeft: '16:59',
      data: [
        {
          header: '5 Video izle',
          completed: 3,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
        {
          header: '100 Bitci Satın Al',
          completed: 100,
          Total: 100,
          completedEarn: 100,
          totalEarn: 100,
          Code: 'BITCI',
        },
        {
          header: '3 Haber Oku',
          completed: 2,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
      ],
    },
    {
      header: 'Haftalık Görevler',
      DaysLeft: '1G 16:59',
      data: [
        {
          header: '5 Video izle',
          completed: 3,
          Total: 5,
          completedEarn: 500,
          totalEarn: 500,
          Code: 'BITCI',
        },
        {
          header: '100 Bitci Satın Al',
          completed: 100,
          Total: 100,
          completedEarn: 100,
          totalEarn: 100,
          Code: 'BITCI',
        },
        {
          header: '3 Haber Oku',
          completed: 2,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
      ],
    },
    {
      header: 'Aylık Görevler',
      DaysLeft: '25G 16:59',
      data: [
        {
          header: '5 Video izle',
          completed: 3,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
        {
          header: '5 Video izle',
          completed: 3,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
        {
          header: '3 Haber Oku',
          completed: 2,
          Total: 5,
          completedEarn: 217,
          totalEarn: 500,
          Code: 'BITCI',
        },
      ],
    },
  ];

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
          <FAStandardHeader title={'GÖREVLER'} navigation={navigation} />
          {passive ? (
            <View
              style={{
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0, 0.6)',
                top: 98,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 99,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white'}}>
                {Localization.t('FanScreen.Soon')}
              </Text>
            </View>
          ) : null}
          <ScrollView>
            <TasksHeader headerUp={'Genel Durum'} headerBottom={'Tamamlanan'} />

            {mockData.map((item, index) => {
              return (
                <TaskItems
                  header={item.header}
                  data={item.data}
                  DaysLeft={item.DaysLeft}
                />
              );
            })}
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default TasksScreen;
