import React, {useEffect, useState} from 'react';
import FADrawerHeader from '../../Components/Composite/FADrawerHeader';
import {ScrollView, Text, View, FlatList} from 'react-native';
import Localization from '@Localization';
import {connect} from 'react-redux';
import _, {result} from 'lodash';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAFanCard from '@Components/UI/FAFanCard';
import rssParser from 'react-native-rss-parser';
import NewsService from '../../Services/NewsService';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import UploadUserInformationPage from '../UploadUserInformationPage';

const NewsScreen = props => {
  const [showUserUploadInformModal, setshowUserUploadInformModal] = useState(
    props?.UserState?.UserInfo?.CustomerStatusEnumId < 10 ? true : false,
  );
  const [news, setNews] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoading, setIsloading] = useState(true);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [serviceErrorMessage, setserviceErrorMessage] = useState('');
  const {navigation} = props;

  useEffect(() => {
    fetchNewsData();
    setTimeout(() => {
      setIsloading(false);
    }, 1000);
  }, []);

  const fetchNewsData = () => {
    fetch(`https://wp.bitcitech.com/west/feed/?paged=${pageNumber}`)
      .then(response => response.text())
      .then(responseData => rssParser.parse(responseData))
      .then(rss => {
        const rssItems = rss?.items;

        const concatData = _.concat(news, rssItems);

        setNews(concatData);
        setPageNumber(pageNumber + 1);
      })
      .catch(err => {
        setserviceErrorMessage(Localization.t('Commons.UnexpectedError'));
        setErrorModalVisible(true);
        setIsloading(false);
      });
  };

  const photoUrl = item => {
    let re = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i;
    let text = item?.content;
    let imageUrls;
    if (text.match(re)) {
      imageUrls = text.match(re);
    } else {
      imageUrls = null;
    }

    let PhotoUrl = imageUrls[0].toString().split(' ');
    console.log(PhotoUrl[0]);
    return PhotoUrl[0];
  };

  return (
    <FADrawerHeader
      title={Localization.t('CommonsFix.News')}
      navigation={navigation}
      routeMain={props.route.name}>
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            {showUserUploadInformModal ? (
              <UploadUserInformationPage
                closeModal={() => setshowUserUploadInformModal(false)}
                navigateSettingScreen={() =>
                  navigation.navigate('AccountSettings')
                }
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  paddingBottom: 10,
                  paddingHorizontal: 10,
                }}>
                <FAFullScreenLoader isLoading={isLoading} />
                <FAAlertModal
                  visible={errorModalVisible}
                  text={serviceErrorMessage}
                  iconName={'times-circle'}
                  header={Localization.t('CommonsFix.Error')}
                  onPress={() => setErrorModalVisible(false)}
                />
                <View style={{marginTop: 10}}>
                  <FlatList
                    onEndReached={({distanceFromEnd}) => {
                      fetchNewsData();
                    }}
                    onEndReachedThreshold={0.2}
                    ItemSeparatorComponent={() => (
                      <View style={{marginLeft: 10}} />
                    )}
                    keyExtractor={(item, index) => index}
                    showsHorizontalScrollIndicator={false}
                    data={news}
                    renderItem={({index, item}) => (
                      <View style={{marginBottom: 10}}>
                        <FAFanCard
                          height
                          theme={theme}
                          text={item.title}
                          image={photoUrl(item)}
                          branchName={item.MainBranch || null}
                          onPress={() =>
                            navigation.navigate('NewsDetailScreen', {
                              news: item,
                            })
                          }
                        />
                      </View>
                    )}
                  />
                </View>
              </View>
            )}
          </>
        )}
      </ThemeContext.Consumer>
    </FADrawerHeader>
  );
};

const mapStateToProps = state => {
  return {
    NewsState: state.NewsState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(NewsScreen);
