import React, {Component} from 'react';
import {Text, View} from 'react-native';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {resetState} from '@GlobalStore/Actions/FlowActions/index';

import autobind from 'autobind-decorator';
import {deleteAccessTokenFromStorage} from '@Commons/FAStorage';
import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';
import {deleteAuthorizationHeader} from '@Services';

class ResetCredentialExecutor extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.logout();
  }

  @autobind
  navigateToPreLogin() {
    const {navigation, resetState} = this.props;
    resetState();
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'AuthLoaderScreen'}],
      }),
    );
  }

  @autobind
  logout() {
    deleteAuthorizationHeader();
    deleteAccessTokenFromStorage(this.navigateToPreLogin());
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <FAFullScreenLoader isLoading={true} />
      </View>
    );
  }
}

export default connect(null, {resetState})(ResetCredentialExecutor);
