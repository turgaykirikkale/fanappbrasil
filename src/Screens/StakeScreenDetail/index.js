import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAColor from '@Commons/FAColor';
import FAStake from '@Components/UI/FAStake';
import FAStakeLimits from '@Components/UI/FAStakeLimits';
import FATextInput from '@Components/Composite/FATextInput';
import FAStakeCustomerInfos from '@Components/UI/FAStakeCustomerInfos';
import FACheckbox from '@Components/Composite/FACheckbox';
import FATextRendered from '@Components/UI/FATextRendered';
import autobind from 'autobind-decorator';
import FAButton from '@Components/Composite/FAButton';
import moment from 'moment';
import {toFixedNoRounding} from '@Commons/FAMath';
import FAService from '@Services';
import Toast from 'react-native-toast-message';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';
import {connect} from 'react-redux';
import _ from 'lodash';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import FAAlertModal from '@Components/Composite/FAAlertModal';

class StakeScreenDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.type,
      interestStakeData:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.interestStakeData,
      interestStakeRate:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.interestStakeRate,
      currentPercent:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.currentPercent,
      stakeValue: '',
      acceptedStake: false,
      startDate: null,
      finishDate: null,
      takeBackDate: null,
      interestStakeDay:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.interestStakeDay,
      estimatedStake: '0.00',
      totalValue: '0.00',
      InterestStakeTypeEnumId:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.InterestStakeTypeEnumId,
      errorMessage: '',
      showDialogBox: false,
    };
    this.currentLangCode =
      (props && props.CommonState && props.CommonState.language) || 'tr';
    this.userAgreementUri = 'https://www.bitci.com/yasal/kullanici-sozlesmesi';
    this.StakeAgreementUri = 'https://www.bitci.com/yasal/stake-hizmetleri';
  }

  componentDidMount() {
    const {interestStakeDay, interestStakeData} = this.state;
    const {UserState} = this.props;
    if (UserState?.FinanceInfo) {
      let userBalances = UserState.FinanceInfo.CustomerCoinBalanceDetailList;
      let filteredBalance = _.filter(userBalances, {
        CoinId: interestStakeData.CoinId,
      });

      if (filteredBalance.length > 0) {
        this.setState({
          balance: filteredBalance[0].CoinBalance,
        });
      } else {
        this.setState({
          balance: 0,
        });
      }
    }
    const startDate = moment().format('DD.MM.YYYY HH:mm');
    const finishDate = moment()
      .add(interestStakeDay, 'days')
      .format('DD.MM.YYYY HH:mm');
    const takeBackDate = moment()
      .add(interestStakeDay + 1, 'days')
      .format('DD.MM.YYYY HH:mm');

    this.setState({
      startDate,
      finishDate,
      takeBackDate,
    });
  }

  componentWillReceiveProps(nextProps) {
    const {interestStakeData} = this.state;
    const {UserState} = this.props;

    if (nextProps?.UserState && nextProps?.UserState !== UserState) {
      let userBalances =
        nextProps.UserState.FinanceInfo.CustomerCoinBalanceDetailList;
      let filteredBalance = _.filter(userBalances, {
        CoinId: interestStakeData.CoinId,
      });
      this.setState({
        balance: filteredBalance[0].CoinBalance,
      });
    }
  }
  @autobind
  handlePercentExecutor(percentValue, LockedListDetail) {
    this.setState({
      currentPercent: percentValue,
      interestStakeRate: LockedListDetail.InterestRate,
      interestStakeDay: LockedListDetail.Day,
    });
  }

  @autobind
  openProtectionofpersonaldataWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.userAgreementUri}?lang=${this.currentLangCode}`,
      title: Localization.t('CreateAccountScreen.UserAgreement'),
    });
  }
  @autobind
  stakeAgreementDataWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.StakeAgreementUri}?lang=${this.currentLangCode}`,
      title: Localization.t('StakeScreen.StakingServices'),
    });
  }
  @autobind
  readyForService() {
    const {stakeValue, acceptedStake, interestStakeData} = this.state;

    if (
      stakeValue >= interestStakeData?.MinAmount &&
      stakeValue <= interestStakeData?.MaxAmount &&
      acceptedStake !== false
    ) {
      return false;
    }
    return true;
  }
  @autobind
  stakeValueHandler(value) {
    const {interestStakeDay, interestStakeRate, stakeValue} = this.state;
    let estimatedStake = toFixedNoRounding(
      (value * interestStakeRate * interestStakeDay) / 36500,
      2,
    );
    let TotalValue = parseFloat(stakeValue) + parseFloat(estimatedStake);
    let totalValue = toFixedNoRounding(TotalValue, 2);
    this.setState({
      estimatedStake: estimatedStake,
      totalValue: totalValue,
    });
  }
  @autobind
  stakeServiceCall() {
    const {getUserFinanceStateAction} = this.props;
    const {
      stakeValue,
      interestStakeData,
      acceptedStake,
      interestStakeDay,
      InterestStakeTypeEnumId,
    } = this.state;
    const requestBody = {
      Amount: stakeValue,
      CoinId: interestStakeData.CoinId,
      ContractApproval: acceptedStake,
      Day: InterestStakeTypeEnumId === 2 ? null : interestStakeDay,
      InterestStakeTypeEnumId: InterestStakeTypeEnumId,
    };

    FAService.InterestStakeProcess(requestBody)
      .then(response => {
        if (response?.data?.IsSuccess) {
          Toast.show({
            type: 'success',
            position: 'top',
            text1: Localization.t('Commons.Succes'),
            text2: Localization.t('Commons.SuccessfulTransaction'),
            visibilityTime: 750,
            autoHide: true,
            topOffset: 100,
            bottomOffset: 40,
            onShow: () => {},
            onHide: () => {
              getUserFinanceStateAction();
            },
            onPress: () => {},
          });
        } else {
          this.setState({
            errorMessage: response.data.Message,
            showDialogBox: true,
          });
        }
      })
      .catch(error => {
        this.setState({
          errorMessage: Localization.t('Commons.UnexpectedError'),
        });
      });
  }

  render() {
    const {navigation} = this.props;
    const {
      currentPercent,
      stakeValue,
      acceptedStake,
      type,
      interestStakeData,
      interestStakeRate,
      interestStakeDay,
      startDate,
      finishDate,
      takeBackDate,
      estimatedStake,
      totalValue,
      placeHolder,
      balance,
      showDialogBox,
      errorMessage,
    } = this.state;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.vGray}}>
            <FAStandardHeader
              title={'Staking Details'}
              navigation={navigation}
            />
            <FAAlertModal
              visible={showDialogBox}
              text={errorMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <FAStake
              LockedListDetail={interestStakeData.LockedListDetail}
              minAmount={interestStakeData.MinAmount}
              interestStakeRate={interestStakeRate}
              details
              type={type}
              onSelectPercent={(percentValue, LockedListDetail) =>
                this.handlePercentExecutor(percentValue, LockedListDetail)
              }
              currentPercent={currentPercent}
            />
            <ScrollView
              style={{
                flex: 1,
                backgroundColor: theme?.colors?.blueGray,
                marginBottom: 20,
              }}>
              <View style={{marginVertical: 20, marginHorizontal: 10}}>
                <FAStakeLimits
                  interestStakeData={interestStakeData}
                  balance={balance}
                />
              </View>
              <View style={{marginHorizontal: 10, marginBottom: 10}}>
                <Text
                  style={{
                    marginBottom: 6,
                    color: theme?.colors?.black,
                    fontWeight: '400',
                  }}>
                  {Localization.t('StakeScreen.StakeAmount')}
                </Text>
                <FATextInput.Money
                  placeholder={placeHolder}
                  decimalCount={2}
                  onChangeValue={Value =>
                    this.setState(
                      {
                        stakeValue: Value,
                      },
                      () => this.stakeValueHandler(Value),
                    )
                  }
                  value={stakeValue}
                  currencyOrCoinCode={'BITCI'}
                />
              </View>
              <View style={{marginVertical: 20, marginHorizontal: 10}}>
                {type === 1 ? (
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 6,
                      }}>
                      <Text
                        style={{
                          fontWeight: '400',
                          color: theme?.colors?.black,
                        }}>
                        {Localization.t('StakeScreen.StakeDate')}
                      </Text>
                      <Text
                        style={{
                          fontWeight: '400',
                          color: theme?.colors?.black,
                        }}>
                        {startDate}
                      </Text>
                    </View>
                    <Text
                      style={{
                        marginTop: 30,
                        marginBottom: 10,
                        textAlign: 'center',
                        color: 'red',
                        fontWeight: '600',
                      }}>
                      {Localization.t('StakeScreen.Earn')}
                    </Text>
                  </View>
                ) : (
                  <FAStakeCustomerInfos
                    interestStakeDay={interestStakeDay}
                    startDate={startDate}
                    finishDate={finishDate}
                    takeBackDate={takeBackDate}
                    estimatedStake={estimatedStake || '0.00'}
                    totalValue={totalValue || '0.00'}
                  />
                )}
              </View>
              <View
                style={{
                  marginHorizontal: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <FACheckbox.Circle
                  fillColor={FAColor.Orange}
                  checked={acceptedStake}
                  onPress={() =>
                    this.setState({
                      acceptedStake: !acceptedStake,
                    })
                  }
                />
                <FATextRendered
                  stake
                  rightText={Localization.t('StakeScreen.readAndAgree')}
                  boldSentences={[
                    Localization.t('CreateAccountScreen.UserAgreement'),
                    Localization.t('StakeScreen.StakingServices1'),
                  ]}
                  onPressBoldSentences={[
                    () => this.openProtectionofpersonaldataWebView(),
                    () => this.stakeAgreementDataWebView(),
                  ]}
                />
              </View>
            </ScrollView>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 20,
                marginHorizontal: 10,
              }}>
              <FAButton
                disabled={this.readyForService()}
                onPress={() => this.stakeServiceCall()}
                containerStyle={{
                  backgroundColor: FAColor.Orange,
                  flex: 1,
                  marginRight: 5,
                  borderRadius: 4,
                }}
                textStyle={{
                  color: theme?.colors?.black,
                  marginVertical: 14,
                  fontWeight: '600',
                }}
                text={Localization.t('StakeScreen.Staking')}
              />
              <FAButton
                onPress={() => navigation.navigate('StakingScreen')}
                containerStyle={{
                  flex: 1,
                  backgroundColor: theme?.colors?.white,
                  marginLeft: 5,
                  borderRadius: 4,
                }}
                textStyle={{
                  marginVertical: 14,
                  fontWeight: '600',
                  color: theme?.colors?.black,
                }}
                text={Localization.t('StakeScreen.giveUp')}
              />
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {getUserFinanceStateAction})(
  StakeScreenDetail,
);
