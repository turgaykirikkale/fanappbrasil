import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  renderItemContainer: {borderBottomWidth: 1, borderColor: FAColor.BlueGray},
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  searchBarContainer: {marginHorizontal: 10, marginVertical: 16},
  searchButtonTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    marginVertical: 12,
  },
});
