import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import FAColor from '@Commons/FAColor';
import FACountriesForLanguage from '@Commons/FACountriesForLanguage';
import {styles} from './assets/styles';
import FASearchBar from '@Components/Composite/FASearchBar';
import FAAccountMenuItems from '@Components/Composite/FAAccountMenuItems';
import autobind from 'autobind-decorator';
import FAButton from '@Components/Composite/FAButton';
import _ from 'lodash';
import {connect} from 'react-redux';
import {changeLanguage} from '@GlobalStore/Actions/UserActions';
import {setDataToStorage} from '@Commons/FAStorage';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {setLanguage} from '@Services';
import {CommonActions} from '@react-navigation/native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class SelectLanguageScreen extends Component {
  constructor(props) {
    super(props);
    this.data = FACountriesForLanguage;
    this.state = {
      data: this.data,
      selectedLanguage:
        props && props.UserState && props.UserState.Language
          ? props.UserState.Language
          : 'en',
    };
  }

  @autobind
  onSelectLanguage(incomingLanguage) {
    const {changeLanguage} = this.props;
    try {
      setDataToStorage('Language', incomingLanguage);
    } catch (error) {}
    changeLanguage(incomingLanguage);
    setLanguage(incomingLanguage);
    Localization.changeLanguage(incomingLanguage);
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'AuthLoaderScreen'}],
      }),
    );
  }

  @autobind
  onValueChangeCheckBox(item) {
    this.setState({
      selectedLanguage: item.id,
    });
  }

  @autobind
  searchEngineOnChange(value) {
    if (value.length > 0) {
      let newSearchedArr = [];
      _.filter(this.data, item => {
        if (
          (value &&
            typeof value === 'string' &&
            item &&
            item.name &&
            typeof item.name === 'string' &&
            item.name.toLowerCase().indexOf(value.toLowerCase()) > -1) ||
          (item.id &&
            typeof item.id === 'string' &&
            item.id.toLowerCase().indexOf(value.toLowerCase()) > -1)
        ) {
          newSearchedArr.push(item);
        }
      });
      this.setState({
        data: newSearchedArr,
      });
    } else {
      this.setState({
        data: this.data,
      });
    }
  }

  renderItem(item) {
    return (
      <FAAccountMenuItems
        disabled={item.disabled}
        iconName={item.flag}
        CountryName={item.name}
        checked={item.id === this.state.selectedLanguage ? true : false}
        navigation={this.props.navigation}
        onPress={() => this.onValueChangeCheckBox(item)}
      />
    );
  }

  render() {
    const {data, selectedLanguage} = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('SettingScreenFix.SelectLanguage')}
              navigation={navigation}
            />
            <View style={styles.searchBarContainer}>
              <FASearchBar
                placeholder={Localization.t('SettingScreenFix.SearchLanguage')}
                onChangeText={value => this.searchEngineOnChange(value)}
              />
            </View>
            <FlatList
              ItemSeparatorComponent={() => (
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: theme?.colors?.blueGray,
                  }}
                />
              )}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              data={data}
              renderItem={({item}) => this.renderItem(item)}
              keyExtractor={(item, index) => index}
            />
            <FAButton
              onPress={() => this.onSelectLanguage(selectedLanguage)}
              containerStyle={{backgroundColor: theme?.colors?.green}}
              textStyle={[
                styles.searchButtonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={Localization.t('SettingScreenFix.Save')}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {changeLanguage})(SelectLanguageScreen);
