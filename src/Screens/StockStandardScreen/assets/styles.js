import {StyleSheet} from 'react-native';
import {
  DarkBlack,
  LightGray,
  BlueGray,
  DarkBlue,
  White,
} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  marginTop16: {marginTop: 16},
  safeAreaView: {flex: 1, backgroundColor: White},
  scrollView: {flex: 1},
  mainContainer: {paddingHorizontal: 10, paddingTop: 25},
  flowButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buyFlowContainer: {
    paddingVertical: 10,
    flex: 1,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  buyFlowText: {
    fontSize: 14,
    fontWeight: '600',
  },
  sellFlowContainer: {
    paddingVertical: 10,
    flex: 1,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
  sellFlowText: {
    fontSize: 14,
    fontWeight: '600',
  },
  marketTypeContainer: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  marketTypeText: {fontSize: 14, fontWeight: '600', color: DarkBlue},
  marketTypeButton: {
    paddingVertical: 5,
    paddingHorizontal: 18,
    borderBottomWidth: 2,
  },
  createOrderButtonContainer: {
    paddingVertical: 15,
    borderRadius: 4,
  },
  createOrderButtonText: {fontSize: 16, fontWeight: '500', color: White},
  orderBookHeadContainer: {
    paddingVertical: 12,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: BlueGray,
  },
  orderBookHeadTitle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '600',
    color: DarkBlue,
  },
  orderBookHeadAmountAndPriceContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderBookHeadFirstAmount: {flexDirection: 'row', alignItems: 'center'},
  orderBookHeadPrice: {flexDirection: 'row', alignItems: 'center'},
  orderBookHeadLastAmount: {flexDirection: 'row', alignItems: 'center'},
  orderBookHeadAmountAndPriceText: {
    fontSize: 12,
    fontWeight: '600',
    color: DarkBlack,
  },
  orderBookHeadCurrencyOrCoinCode: {
    fontSize: 10,
    fontWeight: '600',
    color: LightGray,
  },
});
