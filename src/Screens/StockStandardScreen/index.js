import FAStockTransactionHistoryList from '@Components/Composite/FAStockTransactionHistoryList';
import FAMarketHistoryList from '@Components/Composite/FAMarketHistoryList';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import {BUYFLOW, OrderType, SELLFLOW, StockMode} from '@Commons/FAEnums';
import FAScrollingFilterList from '@Components/UI/FAScrollingFilterList';
import FAActiveOrderList from '@Components/Composite/FAActiveOrderList';
import FAModeSelectorBox from '@Components/Composite/FAModeSelectorBox';
import {stockCalculationHelper} from '@Helpers/StockCalculationHelper';
import {setStockModeAction} from '@GlobalStore/Actions/MarketActions';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import {orderBookDataRenderer} from '@Helpers/StockStreamLogic';
import FAStockHeader from '@Components/Composite/FAStockHeader';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FAOrderBook from '@Components/Composite/FAOrderBook';
import FATextInput from '@Components/Composite/FATextInput';
import FABalanceLine from '@Components/UI/FABalanceLine';
import FARangeSlider from '@Components/UI/FARangeSlider';
import FAButton from '@Components/Composite/FAButton';
import {TabBar, TabView} from 'react-native-tab-view';
import {styles} from './assets/styles';
import {setDataToStorage} from '@Commons/FAStorage';
import autobind from 'autobind-decorator';
import {connect} from 'react-redux';
import FAService from '@Services';
import {FAOrange} from '@Commons/FAColor';
import React from 'react';
import _ from 'lodash';
import {
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {
  subscribeCustomerOrderTickerChannel,
  subscribeMarketHistoryChannel,
  subscribeOrderBookTickerChannel,
  unSubscribeCustomerOrderTickerChannel,
  unSubscribeMarketHistoryChannel,
  unSubscribeOrderBookTickerChannel,
} from '@Stream';
import Toast from 'react-native-toast-message';
import {toFixedNoRounding} from '@Commons/FAFormatter/StatusFormatter';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAEnums from '@Commons/FAEnums';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const marketTypes = [
  {
    label: Localization.t('StockStandartScreen.Limit'),
    value: StockMarketTypes.Limit,
  },
  {
    label: Localization.t('StockStandartScreen.Market'),
    value: StockMarketTypes.Market,
  },
  {
    label: Localization.t('StockStandartScreen.Stop'),
    value: StockMarketTypes.Stop,
  },
];

class StockStandardScreen extends React.Component {
  constructor(props) {
    super(props);
    this.initialInputValues = {
      amount: null,
      totalPrice: null,
      limitPrice: null,
      stopPrice: null,
      percent: null,
    };
    this.customerTransactionHistoryListSize = 6;
    this.state = {
      selectedPair: props?.route?.params?.params?.pairDetail || {},
      isAnonymousFlow: props?.route?.params?.isAnonymous,
      flow: props?.route?.params?.params?.flow || BUYFLOW,
      currentMarketType: StockMarketTypes.Limit,
      input: this.initialInputValues,
      orderBook: {
        Buy: [],
        Sell: [],
        Change24H: '0.0',
      },
      marketQueueName: null,
      balance: {
        coinBalance: '0.0',
        currencyBalance: '0.0',
      },
      customerOrders: {
        Buy: [],
        Sell: [],
      },
      selectedCustomerOrderType: BUYFLOW,
      customerTransactionHistory: [],
      marketHistory: [],
      index: 0,
      routes: [
        {
          key: 'openOrders',
          title: Localization.t('StockStandartScreen.OpenOrders'),
        },
        {
          key: 'transactionHistory',
          title: Localization.t('StockStandartScreen.TransactionHistory'),
        },
        {
          key: 'marketHistory',
          title: Localization.t('StockStandartScreen.MarketHistory'),
        },
      ],

      errorMessage: null,
      isModeBoxOpen: false,
      orderBookLoading: false,
      customerOrderLoading: false,
      marketHistoryLoading: false,
      customerTransactionHistoryLoading: false,
      createOrderLoading: false,
    };
  }

  customerOrderType = [
    {label: Localization.t('StockStandartScreen.BuyOrder'), value: BUYFLOW},
    {label: Localization.t('StockStandartScreen.SellOrder'), value: SELLFLOW},
  ];

  _unsubscribeFocusListener = null;
  _unsubscribeBlurListener = null;

  componentWillMount() {
    const {navigation} = this.props;
    this._unsubscribeFocusListener = navigation.addListener('focus', () => {
      const {isAnonymousFlow, routes} = this.state;
      let anonymousTabRoutes = routes;
      if (isAnonymousFlow) {
        _.remove(anonymousTabRoutes, {key: 'openOrders'});
        _.remove(anonymousTabRoutes, {key: 'transactionHistory'});
        const marketHistoryTabIndex = _.findIndex(anonymousTabRoutes, {
          key: 'marketHistory',
        });
        if (marketHistoryTabIndex > -1) {
          this.setState({
            routes: anonymousTabRoutes,
            index: marketHistoryTabIndex,
          });
        }
      }
      this.getMarketQueueNameAndFillAllData();
    });
    this._unsubscribeBlurListener = navigation.addListener('blur', () => {
      unSubscribeOrderBookTickerChannel();
      unSubscribeCustomerOrderTickerChannel();
      unSubscribeMarketHistoryChannel();
    });
  }

  componentWillUnmount() {
    this._unsubscribeFocusListener();
    this._unsubscribeBlurListener();
    unSubscribeOrderBookTickerChannel();
    unSubscribeCustomerOrderTickerChannel();
    unSubscribeMarketHistoryChannel();
  }

  componentWillReceiveProps(nextProps) {
    const {UserState} = this.props;
    const {marketQueueName} = this.state;
    if (nextProps?.UserState && nextProps?.UserState !== UserState) {
      this.fillCustomerBalance(nextProps.UserState);
    }

    if (
      nextProps?.MarketState?.MarketQueueName &&
      nextProps.MarketState.MarketQueueName !== marketQueueName
    ) {
      this.getMarketQueueNameAndFillAllData(nextProps.MarketState);
    }
  }

  fillAllData() {
    const {isAnonymousFlow} = this.state;
    if (!isAnonymousFlow) {
      this.fillCustomerBalance();
      this.fillCustomerActiveOrders();
    }
    this.fillMarketHistory();
    this.fillOrderBook();
  }

  getMarketQueueNameAndFillAllData(incomingMarketState) {
    const {MarketState} = this.props;
    let marketState = MarketState;
    if (incomingMarketState) {
      marketState = incomingMarketState;
    }
    if (marketState?.MarketQueueName && marketState?.SelectedPair) {
      const selectedPair = marketState.SelectedPair;
      this.setState(
        {
          marketQueueName: marketState.MarketQueueName,
          selectedPair: selectedPair || {},
        },
        () => {
          this.fillAllData();
        },
      );
    } else {
      this.fillMarketQueueName();
    }
  }

  fillMarketQueueName() {
    const {MarketState} = this.props;
    if (MarketState?.SelectedPair) {
      const selectedPair = MarketState.SelectedPair;
      const requestBody = {
        CurrencyId: selectedPair.CurrencyId,
        CoinId: selectedPair.CoinId,
      };
      FAService.GetMarketInformation(requestBody)
        .then(response => {
          if (response?.data && response.status === 200) {
            const responseData = response.data;
            this.setState(
              {
                marketQueueName: responseData.MarketQueueName,
              },
              () => this.fillAllData(),
            );
          }
        })
        .catch(err => {
          console.log('fillMarketQueueName error = ', err);
        });
    }
  }

  @autobind
  fillOrderBook() {
    const {marketQueueName, selectedPair} = this.state;
    const requestBody = {
      selectedCoinCode: selectedPair?.CoinCode || null,
      selectedCurrencyCode: selectedPair?.CurrencyCode || null,
    };
    this.setState({orderBookLoading: true});
    FAService.GetActiveOrders(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const responseData = res.data;
          let buyOrders = _.sortBy(
            _.filter(responseData, {Type: BUYFLOW}),
            'Price',
          ).reverse();
          let sellOrders = _.sortBy(
            _.filter(responseData, {Type: SELLFLOW}),
            'Price',
          );
          this.setState({
            orderBook: {
              Buy: percentExecutor(_.slice(buyOrders, 0, 15)),
              Sell: percentExecutor(_.slice(sellOrders, 0, 15)),
              Change24H: buyOrders[0]?.Change24H,
            },
            orderBookLoading: false,
          });
          subscribeOrderBookTickerChannel(
            marketQueueName,
            this.orderBookChangeHandler,
          );
        } else {
          this.setState({
            orderBookLoading: false,
          });
        }
      })
      .catch(err => {
        console.log('fillOrderBook err = ', err);
        this.setState({
          orderBookLoading: false,
        });
      });
  }

  @autobind
  orderBookChangeHandler(data) {
    const {orderBook} = this.state;
    const {BuyOrderBook, SellOrderBook, Change24} = orderBookDataRenderer(data);
    let newBuyOrderBook,
      newSellOrderBook,
      newChange24 = null;

    if (BuyOrderBook) {
      newBuyOrderBook = _.sortBy(BuyOrderBook, 'Price').reverse();
    } else {
      newBuyOrderBook = orderBook.Buy;
    }

    if (SellOrderBook) {
      newSellOrderBook = _.sortBy(SellOrderBook, 'Price');
    } else {
      newSellOrderBook = orderBook.Sell;
    }

    if (Change24) {
      newChange24 = Change24;
    } else {
      newChange24 = orderBook.Change24H;
    }

    this.setState({
      orderBook: {
        Buy: newBuyOrderBook,
        Sell: newSellOrderBook,
        Change24H: newChange24,
      },
    });
  }

  findMatchedAssetBalance(balanceList, assetId) {
    return _.find(balanceList, {CoinId: assetId})?.CoinBalance;
  }

  fillCustomerBalance(userState) {
    const {MarketState} = this.props;
    let {UserState, getUserFinanceStateAction} = this.props;
    if (userState) {
      UserState = userState;
    }

    const userFinanceInfo = UserState?.FinanceInfo;
    if (
      userFinanceInfo?.CustomerCoinBalanceDetailList &&
      MarketState?.SelectedPair
    ) {
      const selectedPair = MarketState.SelectedPair;
      const customerCoinBalanceDetailList =
        userFinanceInfo.CustomerCoinBalanceDetailList;
      const matchedCurrencyBalance = this.findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        29,
      );

      const matchedCoinBalance = this.findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        selectedPair.CoinId,
      );

      this.setState({
        balance: {
          coinBalance: matchedCoinBalance || '0.0',
          currencyBalance: matchedCurrencyBalance || '0.0',
        },
      });
    } else {
      getUserFinanceStateAction();
    }
  }

  @autobind
  fillCustomerActiveOrders() {
    const {marketQueueName, selectedPair} = this.state;
    const requestBody = {
      selectedCoinCode: selectedPair?.CoinCode || null,
      selectedCurrencyCode: selectedPair?.CurrencyCode || null,
    };
    this.setState({customerOrderLoading: true});
    FAService.GetCustomerActiveOrders(requestBody)
      .then(response => {
        if (response && response.data && response.status === 200) {
          const customerActiveBuyOrders = _.filter(response.data, {
            Type: BUYFLOW,
          });
          const customerActiveSellOrders = _.filter(response.data, {
            Type: SELLFLOW,
          });
          this.setState({
            customerOrders: {
              Buy: customerActiveBuyOrders,
              Sell: customerActiveSellOrders,
            },
            customerOrderLoading: false,
          });
          subscribeCustomerOrderTickerChannel(
            marketQueueName,
            this.customerActiveOrderHandler,
          );
        }
      })
      .catch(error => {
        this.setState({customerOrderLoading: false});
        console.log('fillCustomerActiveOrders error = ', error);
      });
  }

  @autobind
  customerActiveOrderHandler(data) {
    const {customerOrders} = this.state;

    if (data?.CoinId && data?.CurrencyId) {
      let searchItemIndex = null;
      let orderArray = [];
      if (data.CancelledAfterProcess) {
        return false;
      }

      if (data.Type === OrderType.Buy) {
        orderArray = customerOrders.Buy || [];
      } else {
        orderArray = customerOrders.Sell || [];
      }

      searchItemIndex = _.findIndex(orderArray, item => {
        return (item.OrderId || item.Id) === data.Id;
      });

      if (data.State === 0 && searchItemIndex < 0) {
        orderArray.push(data);
      } else if (searchItemIndex !== -1 && data.State === 1) {
        orderArray[searchItemIndex] = data;
      } else if (searchItemIndex !== -1 && data.State === 2) {
        let item = orderArray[searchItemIndex];
        if (data.ProcessCoinValue === data.CoinValue) {
          orderArray.splice(searchItemIndex, 1);
        } else {
          item.CoinValue = item.CoinValue - data.ProcessCoinValue;
          orderArray[searchItemIndex] = item;
        }
      } else if (searchItemIndex !== -1 && data.State === 4) {
        orderArray.splice(searchItemIndex, 1);
      }

      if (data.Type === OrderType.Buy) {
        this.setState(prevState => ({
          customerOrders: {
            ...prevState.customerOrders,
            Buy: [...orderArray],
          },
        }));
      } else {
        this.setState(prevState => ({
          customerOrders: {
            ...prevState.customerOrders,
            Sell: [...orderArray],
          },
        }));
      }

      const {getUserFinanceStateAction} = this.props;
      setTimeout(() => {
        getUserFinanceStateAction();
        this.fillCustomerTransactionHistory(true);
      }, 1000);
      //   // this.getCurrencyAndCoinBalanceDetailAndSetToState();
    }
  }

  fillCustomerTransactionHistory(fromSocket) {
    const {selectedPair} = this.state;
    const requestBody = {
      PagerDto: {
        Page: 1,
        Size: this.customerTransactionHistoryListSize,
        Total: 0,
      },
      coinId: selectedPair?.CoinId,
      currencyId: selectedPair?.CurrencyId,
    };
    if (!fromSocket) {
      this.setState({
        customerTransactionHistoryLoading: true,
      });
    }
    FAService.GetCustomerTransactionHistory(requestBody)
      .then(response => {
        if (response && response.data && response.status === 200) {
          this.setState({
            customerTransactionHistory: response.data.Orders || [],
          });
        }
        if (!fromSocket) {
          this.setState({customerTransactionHistoryLoading: false});
        }
      })
      .catch(error => {
        if (!fromSocket) {
          this.setState({customerTransactionHistoryLoading: false});
        }
      });
  }

  @autobind
  fillMarketHistory() {
    const {marketQueueName, selectedPair} = this.state;
    const requestBody = {
      selectedCoinCode: selectedPair?.CoinCode || null,
      selectedCurrencyCode: selectedPair?.CurrencyCode || null,
    };
    this.setState({marketHistoryLoading: true});
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response && response.status === 200 && response.data) {
          const marketHistoryList = _.slice(response.data, 0, 15);
          this.setState({marketHistory: marketHistoryList}, () => {
            subscribeMarketHistoryChannel(
              marketQueueName,
              this.marketHistoryHandler,
            );
          });
        }
        this.setState({marketHistoryLoading: false});
      })
      .catch(error => this.setState({marketHistoryLoading: false}));
  }

  @autobind
  marketHistoryHandler(data) {
    const {marketHistory} = this.state;
    let newMarketHistoryList = marketHistory;
    if (data && data.Price) {
      newMarketHistoryList.unshift(data);
      newMarketHistoryList = _.slice(newMarketHistoryList, 0, 15);
      this.setState({
        marketHistory: newMarketHistoryList,
      });
    }
  }

  calculation(
    incomingCurrencyValue,
    incomingCoinValue,
    incomingLimitValue,
    triggeredByPercentValueExecutor,
  ) {
    const {currentMarketType, input, flow, orderBook} = this.state;
    const state = {
      currentMarketFlowType: currentMarketType,
      percent: input.percent,
      flow: flow,
      limitPrice: input.limitPrice,
      amount: input.amount,
      totalPrice: input.totalPrice,
      buyOrderBook: orderBook.Buy,
      sellOrderBook: orderBook.Sell,
    };

    const calculationResponse = stockCalculationHelper(
      state,
      incomingCurrencyValue,
      incomingCoinValue,
      incomingLimitValue,
      triggeredByPercentValueExecutor,
    );
    if (calculationResponse) {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          percent:
            calculationResponse.percent || calculationResponse.percent === null
              ? calculationResponse.percent
              : prevState.input.percent,
          amount:
            calculationResponse.amount || calculationResponse.amount === null
              ? calculationResponse.amount
              : prevState.input.amount,
          limitPrice:
            calculationResponse.limitPrice ||
            calculationResponse.limitPrice === null
              ? calculationResponse.limitPrice
              : prevState.input.limitPrice,
          totalPrice:
            calculationResponse.totalPrice ||
            calculationResponse.totalPrice === null
              ? calculationResponse.totalPrice
              : prevState.input.totalPrice,
        },
      }));
    }
  }

  createOrder() {
    const {orderBook, flow, currentMarketType, input, selectedPair} =
      this.state;
    const ActiveBuyOrders = orderBook.Buy;
    const ActiveSellOrders = orderBook.Sell;

    let generatedPrice = 0;
    let stopValue = null;
    if (currentMarketType === StockMarketTypes.Market) {
      if (flow === 1) {
        generatedPrice = ActiveSellOrders[0].Price;
      } else {
        generatedPrice = ActiveBuyOrders[0].Price;
      }
    } else if (currentMarketType === 'limit') {
      generatedPrice = input.limitPrice;
    } else if (currentMarketType === 'stop') {
      stopValue = input.stopPrice;
      generatedPrice = input.limitPrice;
    }
    const requestBody = {
      CoinId: selectedPair?.CoinId,
      CurrencyId: selectedPair?.CurrencyId,
      Price: generatedPrice,
      Amount: toFixedNoRounding(Number(input.amount), 8),
      StopLossPrice: stopValue,
      OrderType: flow,
    };
    this.setState({createOrderLoading: true});
    FAService.CreateOrder(requestBody)
      .then(response => {
        if (response && response.data && !response.data.IsSuccess) {
          this.setState({
            errorMessage: response.data.Message,
          });
        } else {
          Toast.show({
            type: 'success',
            position: 'top',
            text1: Localization.t('Commons.Succes'),
            visibilityTime: 750,
            autoHide: true,
            topOffset: 50,
            bottomOffset: 40,
            onShow: () => {},
            onHide: () => {},
            onPress: () => {},
          });
        }
        this.setState({
          createOrderLoading: false,
        });
      })
      .catch(error => {
        this.setState({
          createOrderLoading: false,
        });
      });
  }

  handleMarketTypeChange(newMarketTypeValue) {
    this.setState({
      currentMarketType: newMarketTypeValue,
    });
    this.clearInputs();
  }

  handleFlowChange(newFlowValue) {
    this.setState({
      flow: newFlowValue,
    });
    this.clearInputs();
  }

  handlePercentExecutor(newPercentValue) {
    const {flow, balance} = this.state;
    const {coinBalance, currencyBalance} = balance;
    if (flow === BUYFLOW) {
      if (currencyBalance) {
        const calculatedBalance = (
          currencyBalance * newPercentValue
        ).toString();
        this.calculation(
          calculatedBalance,
          null,
          null,
          newPercentValue.toString(),
        );
      }
    } else {
      if (coinBalance) {
        const calculatedBalance = (coinBalance * newPercentValue).toString();
        this.calculation(
          null,
          calculatedBalance,
          null,
          newPercentValue.toString(),
        );
      }
    }

    this.setState(prevState => ({
      input: {
        ...prevState.input,
        percent: newPercentValue,
      },
    }));
  }

  handleTabIndexChange(newTabIndexValue) {
    if (newTabIndexValue === 0) {
      this.fillCustomerActiveOrders();
      unSubscribeMarketHistoryChannel();
    } else if (newTabIndexValue === 1) {
      this.fillCustomerTransactionHistory();
      unSubscribeCustomerOrderTickerChannel();
      unSubscribeMarketHistoryChannel();
    } else if (newTabIndexValue === 2) {
      this.fillMarketHistory();
      unSubscribeCustomerOrderTickerChannel();
    }
    this.setState({
      index: newTabIndexValue,
    });
  }

  clearInputs() {
    this.setState({
      input: this.initialInputValues,
    });
  }

  onPressOrderPrice(orderPrice) {
    const {currentMarketType} = this.state;
    if (currentMarketType === StockMarketTypes.Market) {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          totalPrice: orderPrice,
        },
      }));
      this.calculation(orderPrice);
    } else {
      this.setState(prevState => ({
        input: {
          ...prevState.input,
          limitPrice: orderPrice,
        },
      }));
      this.calculation(null, null, orderPrice);
    }
  }

  async dispatchSelectedStockMode(newStockMode) {
    if (newStockMode && newStockMode !== StockMode.Standard) {
      const {navigation, setStockModeAction} = this.props;
      const {flow} = this.state;
      setStockModeAction(newStockMode);
      this.setState({isModeBoxOpen: false});
      await setDataToStorage('StockMode', newStockMode);
      if (newStockMode === StockMode.EasyBuySell) {
        navigation.navigate('StockEasyBuy', {flow});
      } else {
        navigation.navigate('StockAdvanced', {flow});
      }
    }
  }

  navigateToMarketScreen() {
    const {navigation} = this.props;
    navigation.navigate('MarketTabStackNavigator', {params: {from: 'stock'}});
  }

  navigateToChartScreen() {
    const {selectedPair} = this.state;
    const {navigation} = this.props;
    const coinCode = selectedPair?.CoinCode || null;
    const currencyCode = selectedPair?.CurrencyCode || null;
    const chartUrl = `https://chart.bitci.com/exchange/advanced/${coinCode}_${currencyCode}`;
    navigation.navigate('WebviewScreen', {
      url: chartUrl,
      title: `${coinCode}/${currencyCode} ${Localization.t(
        'StockEasyBuy.Chart',
      )}`,
    });
  }

  navigateToLoginRegisterStack() {
    const {navigation} = this.props;
    if (navigation) {
      navigation.navigate('FanTabStackNavigator');
    }
  }

  @autobind
  renderTabBar(props, theme) {
    const {isAnonymousFlow} = this.state;
    return (
      <TabBar
        scrollEnabled
        {...props}
        renderLabel={({route, focused, color}) => (
          <Text
            style={{
              fontSize: 12,
              color: focused
                ? theme?.colors?.faOrange
                : theme?.colors?.darkBlue,
              textAlign: 'center',
            }}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={[
          {
            borderColor: theme?.colors?.gray,
          },
          isAnonymousFlow ? {flex: 1, justifyContent: 'center'} : null,
        ]}
        indicatorStyle={{
          backgroundColor: theme?.colors?.faOrange,
        }}
        style={{
          backgroundColor: theme?.colors?.white,
        }}
        tabStyle={
          isAnonymousFlow
            ? {
                width: '100%',
              }
            : null
        }
      />
    );
  }

  tabRenderByIndex(theme) {
    const {isAnonymousFlow, index} = this.state;
    if (isAnonymousFlow) {
      if (index === 0) {
        return this.tabRenderMarketHistory(theme);
      }
    } else {
      if (index === 0) {
        return this.tabRenderOpenOrders(theme);
      } else if (index === 1) {
        return this.tabRenderTransactionHistory(theme);
      } else if (index === 2) {
        return this.tabRenderMarketHistory(theme);
      }
    }
  }

  tabRenderOpenOrders() {
    const {customerOrders, customerOrderLoading, selectedCustomerOrderType} =
      this.state;
    if (customerOrderLoading) {
      return (
        <View
          style={{
            paddingVertical: 30,
          }}>
          <ActivityIndicator size={'small'} color={FAOrange} />
        </View>
      );
    }
    let customerOrderBySelectedType = customerOrders;
    if (selectedCustomerOrderType === BUYFLOW) {
      customerOrderBySelectedType = customerOrders.Buy;
    } else {
      customerOrderBySelectedType = customerOrders.Sell;
    }
    return (
      <View style={{marginTop: 12, marginHorizontal: 10}}>
        <FAScrollingFilterList
          data={this.customerOrderType}
          selected={selectedCustomerOrderType}
          onSelect={incomingValue =>
            this.setState({
              selectedCustomerOrderType: incomingValue,
            })
          }
        />
        <View style={{marginTop: 12}}>
          <FAActiveOrderList data={customerOrderBySelectedType} />
        </View>
      </View>
    );
  }

  tabRenderTransactionHistory(theme) {
    const {customerTransactionHistory, customerTransactionHistoryLoading} =
      this.state;
    if (customerTransactionHistoryLoading) {
      return (
        <View style={{paddingVertical: 30}}>
          <ActivityIndicator size={'small'} color={theme?.colors?.faOrange} />
        </View>
      );
    }
    return <FAStockTransactionHistoryList data={customerTransactionHistory} />;
  }

  tabRenderMarketHistory(theme) {
    const {marketHistory, marketHistoryLoading} = this.state;
    if (marketHistoryLoading) {
      return (
        <View style={{paddingVertical: 30}}>
          <ActivityIndicator size={'small'} color={theme?.colors?.faOrange} />
        </View>
      );
    }

    return <FAMarketHistoryList data={marketHistory} />;
  }

  render() {
    const {
      flow,
      input,
      isModeBoxOpen,
      orderBook,
      errorMessage,
      currentMarketType,
      balance,
      orderBookLoading,
      index,
      routes,
      isAnonymousFlow,
      createOrderLoading,
      selectedPair,
    } = this.state;
    const currencyDecimal = selectedPair?.PriceDecimal || 2;
    const coinDecimal = selectedPair?.CoinDecimal || 4;
    const currencyCode = selectedPair?.CurrencyCode || '';
    const coinCode = selectedPair?.CoinCode || '';
    const formattedCurrencyBalance = FACurrencyAndCoinFormatter(
      Number(balance?.currencyBalance).toFixed(9),
      currencyDecimal,
    );
    const formattedCoinBalance = FACurrencyAndCoinFormatter(
      Number(balance?.coinBalance).toFixed(9),
      coinDecimal,
    );
    let buyButtonText = `${coinCode} ${Localization.t('StockEasyBuy.Buy')}`;
    let sellButtonText = `${coinCode} ${Localization.t('StockEasyBuy.Sell')}`;
    if (isAnonymousFlow) {
      buyButtonText = Localization.t('LoginOrRegister');
      sellButtonText = Localization.t('LoginOrRegister');
    }
    const initialLayout = {width: Dimensions.get('window').width};
    console.log('fromStandarSScreen', selectedPair);
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <SafeAreaView
            style={[
              styles.safeAreaView,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAFullScreenLoader isLoading={createOrderLoading} />
            <FAModeSelectorBox
              onPressClose={() => this.setState({isModeBoxOpen: false})}
              visible={isModeBoxOpen}
              selectedMode={StockMode.Standard}
              onChangeMode={newStockMode =>
                this.dispatchSelectedStockMode(newStockMode)
              }
            />
            <FAStockHeader
              pair={selectedPair}
              coinCode={coinCode}
              currencyCode={currencyCode}
              favoriteDisable={isAnonymousFlow}
              dailyChange={orderBook?.Change24H}
              onPressPair={() => this.navigateToMarketScreen()}
              onPressChart={() => this.navigateToChartScreen()}
              onPressMode={() => this.setState({isModeBoxOpen: true})}
            />
            <FAAlertModal
              text={errorMessage}
              iconName={'times-circle'}
              visible={!_.isEmpty(errorMessage)}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({errorMessage: null})}
            />
            <ScrollView
              style={styles.scrollView}
              showsVerticalScrollIndicator={false}>
              <View style={styles.mainContainer}>
                <View style={styles.flowButtonContainer}>
                  <FAButton
                    onPress={() => this.handleFlowChange(BUYFLOW)}
                    text={Localization.t('StockStandartScreen.Buying')}
                    containerStyle={[
                      styles.buyFlowContainer,
                      {
                        backgroundColor:
                          flow === BUYFLOW
                            ? theme?.colors?.green
                            : theme?.colors?.blueGray,
                      },
                    ]}
                    textStyle={[
                      styles.buyFlowText,
                      {
                        color:
                          flow === BUYFLOW
                            ? theme?.colors?.white2
                            : theme?.colors?.darkBlue,
                      },
                    ]}
                  />
                  <FAButton
                    onPress={() => this.handleFlowChange(SELLFLOW)}
                    text={Localization.t('StockStandartScreen.Selling')}
                    containerStyle={[
                      styles.sellFlowContainer,
                      {
                        backgroundColor:
                          flow === SELLFLOW
                            ? theme?.colors?.red
                            : theme?.colors?.blueGray,
                      },
                    ]}
                    textStyle={[
                      styles.sellFlowText,
                      {
                        color:
                          flow === SELLFLOW
                            ? theme?.colors?.white2
                            : theme?.colors?.darkBlue,
                      },
                    ]}
                  />
                </View>
                <View style={styles.marketTypeContainer}>
                  {marketTypes.map((item, itemIndex) => (
                    <FAButton
                      key={itemIndex}
                      text={item.label}
                      textStyle={[
                        styles.marketTypeText,
                        {color: theme?.colors?.darkBlue},
                      ]}
                      onPress={() => this.handleMarketTypeChange(item.value)}
                      containerStyle={[
                        styles.marketTypeButton,
                        {
                          borderBottomColor:
                            item.value === currentMarketType
                              ? theme?.colors?.orange
                              : theme?.colors?.vGray,
                        },
                      ]}
                    />
                  ))}
                </View>
                {currentMarketType !== StockMarketTypes.Market && (
                  <View style={styles.marginTop16}>
                    <FATextInput.Money
                      value={input.limitPrice}
                      onPressIncrement={() => {}}
                      decimalCount={currencyDecimal}
                      currencyOrCoinCode={currencyCode}
                      placeholder={Localization.t('StockStandartScreen.Price')}
                      onChangeValue={limitValue =>
                        this.calculation(null, null, limitValue)
                      }
                    />
                  </View>
                )}
                {currentMarketType === StockMarketTypes.Market && (
                  <View style={styles.marginTop16}>
                    <FATextInput.Money
                      decimalCount={currencyDecimal}
                      onChangeValue={totalPriceValue =>
                        this.calculation(totalPriceValue, null)
                      }
                      value={input.totalPrice}
                      placeholder={Localization.t(
                        'StockStandartScreen.TotalPrice',
                      )}
                      currencyOrCoinCode={currencyCode}
                      onPressIncrement={flow === BUYFLOW ? () => {} : null}
                    />
                  </View>
                )}
                <View style={styles.marginTop16}>
                  <FATextInput.Money
                    decimalCount={coinDecimal}
                    onChangeValue={amountValue =>
                      this.calculation(null, amountValue)
                    }
                    value={input.amount}
                    placeholder={Localization.t('StockStandartScreen.Amount')}
                    currencyOrCoinCode={coinCode}
                    onPressIncrement={flow === BUYFLOW ? null : () => {}}
                  />
                </View>
                {currentMarketType === StockMarketTypes.Stop && (
                  <View style={styles.marginTop16}>
                    <FATextInput.Money
                      decimalCount={currencyDecimal}
                      onChangeValue={stopPriceValue =>
                        this.setState({
                          input: {...input, stopPrice: stopPriceValue},
                        })
                      }
                      value={input.stopPrice}
                      placeholder={Localization.t(
                        'StockStandartScreen.StopPrice',
                      )}
                    />
                  </View>
                )}
                {currentMarketType !== StockMarketTypes.Market && (
                  <View style={styles.marginTop16}>
                    <FATextInput.Money
                      decimalCount={currencyDecimal}
                      onChangeValue={totalPriceValue =>
                        this.calculation(totalPriceValue)
                      }
                      value={input.totalPrice}
                      placeholder={Localization.t(
                        'StockStandartScreen.TotalPrice',
                      )}
                      currencyOrCoinCode={currencyCode}
                      onPressIncrement={flow === BUYFLOW ? () => {} : null}
                    />
                  </View>
                )}
                <View style={styles.marginTop16}>
                  <FARangeSlider
                    onSelectPercent={percentValue =>
                      this.handlePercentExecutor(percentValue)
                    }
                    currentPercent={input.percent}
                    flowType={flow}
                  />
                </View>
                <View style={styles.marginTop16}>
                  <FABalanceLine
                    currency={flow === BUYFLOW ? currencyCode : coinCode}
                    balance={
                      flow === BUYFLOW
                        ? formattedCurrencyBalance
                        : formattedCoinBalance
                    }
                  />
                </View>
                <View style={styles.marginTop16}>
                  <FAButton
                    onPress={() =>
                      isAnonymousFlow
                        ? this.navigateToLoginRegisterStack()
                        : this.createOrder()
                    }
                    text={flow === BUYFLOW ? buyButtonText : sellButtonText}
                    containerStyle={[
                      styles.createOrderButtonContainer,
                      {
                        backgroundColor:
                          flow === BUYFLOW
                            ? theme?.colors?.green
                            : theme?.colors?.red,
                      },
                    ]}
                    textStyle={[
                      styles.createOrderButtonText,
                      {color: theme?.colors?.white2},
                    ]}
                  />
                </View>
              </View>
              <View style={styles.marginTop16}>
                <View
                  style={[
                    styles.orderBookHeadContainer,
                    {backgroundColor: theme?.colors?.blueGray},
                  ]}>
                  <Text style={styles.orderBookHeadTitle}>
                    {Localization.t('StockStandartScreen.BuyOrders')}
                  </Text>
                  <Text style={styles.orderBookHeadTitle}>
                    {Localization.t('StockStandartScreen.SellOrders')}
                  </Text>
                </View>
                <View
                  style={[
                    styles.orderBookHeadAmountAndPriceContainer,
                    {backgroundColor: theme?.colors?.white},
                  ]}>
                  <View style={styles.orderBookHeadFirstAmount}>
                    <Text
                      style={[
                        styles.orderBookHeadAmountAndPriceText,
                        {color: theme?.colors?.darkBlack},
                      ]}>
                      {Localization.t('StockStandartScreen.Amount')}
                    </Text>
                    <Text
                      style={[
                        styles.orderBookHeadCurrencyOrCoinCode,
                        {color: theme?.colors?.lightGray},
                      ]}>
                      ({coinCode})
                    </Text>
                  </View>
                  <View style={styles.orderBookHeadPrice}>
                    <Text
                      style={[
                        styles.orderBookHeadAmountAndPriceText,
                        {color: theme?.colors?.darkBlack},
                      ]}>
                      {Localization.t('StockStandartScreen.Price')}
                    </Text>
                    <Text
                      style={[
                        styles.orderBookHeadCurrencyOrCoinCode,
                        {color: theme?.colors?.lightGray},
                      ]}>
                      ({currencyCode})
                    </Text>
                  </View>
                  <View style={styles.orderBookHeadLastAmount}>
                    <Text
                      style={[
                        styles.orderBookHeadAmountAndPriceText,
                        {color: theme?.colors?.darkBlack},
                      ]}>
                      {Localization.t('StockStandartScreen.Amount')}
                    </Text>
                    <Text
                      style={[
                        styles.orderBookHeadCurrencyOrCoinCode,
                        {color: theme?.colors?.lightGray},
                      ]}>
                      ({coinCode})
                    </Text>
                  </View>
                </View>
              </View>
              {orderBookLoading ? (
                <View style={{flex: 1, paddingVertical: 40}}>
                  <ActivityIndicator
                    color={theme?.colors?.orange}
                    size={'small'}
                  />
                </View>
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    backgroundColor: theme?.colors?.vGray,
                  }}>
                  <View
                    style={{
                      flex: 1,
                    }}>
                    <FAOrderBook
                      theme={theme}
                      size={FAEnums.SMALL}
                      data={orderBook.Buy}
                      onPress={price =>
                        this.onPressOrderPrice(price.toString())
                      }
                      volumeBarDirection={'right'}
                    />
                  </View>
                  <View
                    style={{
                      flex: 1,
                    }}>
                    <FAOrderBook
                      theme={theme}
                      size={FAEnums.SMALL}
                      data={orderBook.Sell}
                      onPress={price =>
                        this.onPressOrderPrice(price.toString())
                      }
                      volumeBarDirection={'left'}
                      reverse
                    />
                  </View>
                </View>
              )}
              <TabView
                renderTabBar={props => this.renderTabBar(props, theme)}
                renderScene={() => null}
                navigationState={{index, routes}}
                onIndexChange={indexValue =>
                  this.handleTabIndexChange(indexValue)
                }
                initialLayout={initialLayout}
              />
              {this.tabRenderByIndex(theme)}
            </ScrollView>
          </SafeAreaView>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {
  setStockModeAction,
  getUserFinanceStateAction,
})(StockStandardScreen);
