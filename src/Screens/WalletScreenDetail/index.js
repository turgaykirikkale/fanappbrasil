import FAWalletAdressInformation from '@Components/Composite/FAWalletAdressInformation';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {getUserWalletsAction} from '@GlobalStore/Actions/WalletActions';
import FAWalletHeaderDetail from '@Components/UI/FAWalletHeaderDetail';
import FACryptoWithdrawInfo from '@Components/UI/FACryptoWithdrawInfo';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {coinProcess, coinProcessWithdrawUnsubscribe} from '@Stream';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FATextInput from '@Components/Composite/FATextInput';
import CameraRoll from '@react-native-community/cameraroll';
import FAQrScanner from '@Components/Composite/FAQrScanner';
import Clipboard from '@react-native-community/clipboard';
import FAButton from '@Components/Composite/FAButton';
import {TabView, TabBar} from 'react-native-tab-view';
import FAModal from '@Components/Composite/FAModal';
import {toFixedNoRounding} from '@Commons/FAMath';
import Toast from 'react-native-toast-message';
import ViewShot from 'react-native-view-shot';
import QRCode from 'react-native-qrcode-svg';
import FAForm from '@ValidationEngine/Form';
import Svg, {Path} from 'react-native-svg';
import autobind from 'autobind-decorator';
import React, {Component} from 'react';
import FAColor from '@Commons/FAColor';
import {styles} from './assets/styles';
import Share from 'react-native-share';
import {connect} from 'react-redux';
import FAService from '@Services';
import {
  View,
  Text,
  Dimensions,
  Platform,
  PermissionsAndroid,
  ScrollView,
} from 'react-native';
import Localization from '@Localization';
import _ from 'lodash';
import FABalanceLine from '@Components/UI/FABalanceLine';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import {stockCalculationHelper} from '@Helpers/StockCalculationHelper';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import {
  subscribeMarketHistoryChannel,
  subscribeOrderBookTickerChannel,
  unSubscribeMarketHistoryChannel,
  unSubscribeOrderBookTickerChannel,
} from '@Stream';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import {orderBookDataRenderer} from '@Helpers/StockStreamLogic';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import FACustomTabView from '../../Components/Composite/FACustomTabView';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class WalletScreenDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.data,
      FormIsValid: false,
      sendingCoinAdress: '',
      sendingCoinAmount: '',
      index:
        (this.props &&
          this.props.route &&
          this.props.route.params &&
          this.props.route.params.index) ||
        0,
      routes: [
        {
          key: 'deposit',
          title: Localization.t('WalletFix.Deposit'),
          accessibilityLabel: 'test',
        },
        {
          key: 'withdraw',
          title: Localization.t('WalletFix.Withdraw'),
          accessibilityLabel: 'deneme',
        },
      ],
      coinAddress: '',
      showCamera: false,
      showApproveModal: false,
      emailAuth: false,
      smsAuth: false,
      googleAuth: false,
      passwordAuth: false,
      pinAuth: false,
      isLoading: false,
      showDialog: false,
      serviceMessage: null,
      requestCode: null,
      smsAuthValue: '',
      emailAuthValue: '',
      googleAuthValue: '',
      pinAuthValue: '',
      passwordAuthValue: '',
      isLoadingRender: false,

      //Swap States
      errorMessage: null,
      lastOrderPrice: null,
      totalPrice: null,
      amount: null,
      sellOrderBook: [],
      marketQueueName: null,
      currencyData: props?.route?.params?.data || {},
      mainCurrencyData: props?.route?.params?.mainWallet || {},

      //chainNetworkInfos
      ChainNetworkCode: null,
      ChainNetworkName: null,
      CoinChainNetworkId: null,
    };
  }

  componentWillMount() {
    const {route} = this.props;
    const {routes} = this.state;
    const hideWithdraw = route?.params?.hideWithdraw || false;
    const showSwap = route?.params?.showSwap || false;
    let newTabRoutes = routes;
    if (hideWithdraw) {
      _.remove(newTabRoutes, {key: 'withdraw'});
      this.setState({
        routes: newTabRoutes,
      });
    }

    if (showSwap) {
      newTabRoutes.push({
        key: 'swap',
        title: Localization.t('Swap'),
      });
      this.setState({
        routes: newTabRoutes,
      });
      this.getMarketQueueNameAndFillData();
    }

    coinProcess(this.callBackWithdrawStream);
    this.getCoinChainNetwork();
  }

  componentWillUnmount() {
    coinProcessWithdrawUnsubscribe();
    unSubscribeMarketHistoryChannel();
    unSubscribeOrderBookTickerChannel();
  }

  getCoinChainNetwork() {
    console.log('getCoinChainNetwor', this.state.data);
    const {data} = this.state;
    const requestBody = {
      coinId: data.AssetId,
    };
    if (data) {
      FAService.GetCoinChainNetwork(requestBody)
        .then(res => {
          if (res?.data && res?.status === 200) {
            console.log('AKKKA', res);
            FAService.GetCoinAdress({
              CoinChainNetworkId: res.data[0].CoinChainNetworkId,
            })
              .then(response => {
                console.log('responseWallet123', response);
                if (response && response.data && response.status === 200) {
                  const {Address, HasTag, Tag} = response.data;
                  this.setState({
                    coinAddress: Address,
                    destinationID: HasTag ? Tag : null,
                    isLoadingRender: false,
                  });
                } else {
                  this.setState({
                    isLoadingRender: false,
                    showDialog: true,
                    serviceMessage: 'Error',
                  });
                }
              })
              .catch(error => {
                console.log('GetCoinAdressError');
              });
          } else {
            this.setState({
              isLoadingRender: false,
              errorMessage: 'Error',
            });
          }
        })
        .catch(err => console.log('getCoinChainNetworRes err', err));
    }
  }
  getMarketQueueNameAndFillData() {
    const requestBody = {
      CoinId: 29, //Bitci coin id
      CurrencyId: 2008, // Usdt currency id
    };
    FAService.GetMarketInformation(requestBody)
      .then(res => {
        if (res?.data?.MarketQueueName) {
          this.setState(
            {
              marketQueueName: res.data.MarketQueueName,
            },
            () => this.fillAllData(),
          );
        }
      })
      .catch(err => console.log('getMarketQueueNameAndFillData err', err));
  }

  fillAllData() {
    this.fillMarketLastOrder();
    this.fillOrderBook();
  }

  fillMarketLastOrder() {
    const {marketQueueName, currencyData, mainCurrencyData} = this.state;
    const requestBody = {
      selectedCoinCode: mainCurrencyData.Code || null, //Bitci coin id
      selectedCurrencyCode: currencyData.Code || null, //Usdt currency id
    };
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response?.data) {
          const incomingLastOrderPrice = response.data[0]?.Price || null;
          if (incomingLastOrderPrice) {
            this.setState(
              {
                lastOrderPrice: incomingLastOrderPrice,
              },
              () => {
                subscribeMarketHistoryChannel(
                  marketQueueName,
                  this.marketHistoryChangeHandler,
                );
              },
            );
          }
        }
      })
      .catch(err => console.log('fillMarketLastOrder err', err));
  }

  @autobind
  marketHistoryChangeHandler(data) {
    const {lastOrderPrice} = this.state;
    if (data?.Price && lastOrderPrice !== data.Price) {
      this.setState({
        lastOrderPrice: data?.Price,
      });
    }
  }

  fillOrderBook() {
    const {marketQueueName, currencyData, mainCurrencyData} = this.state;
    const requestBody = {
      selectedCoinCode: mainCurrencyData?.Code || null,
      selectedCurrencyCode: currencyData?.Code || null,
    };
    FAService.GetActiveOrders(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const responseData = res.data;
          let sellOrders = _.sortBy(
            _.filter(responseData, {Type: SELLFLOW}),
            'Price',
          );
          sellOrders = _.slice(sellOrders, 0, 30);
          this.setState(
            {
              sellOrderBook: percentExecutor(sellOrders),
            },
            () => {
              subscribeOrderBookTickerChannel(
                marketQueueName,
                this.orderBookChangeHandler,
              );
            },
          );
        }
      })
      .catch(err => console.log('fillOrderBook err', err));
  }

  @autobind
  orderBookChangeHandler(data) {
    const {sellOrderBook} = this.state;
    const {SellOrderBook} = orderBookDataRenderer(data);
    let newSellOrderBook = null;

    if (SellOrderBook) {
      newSellOrderBook = _.sortBy(SellOrderBook, 'Price');
    } else {
      newSellOrderBook = sellOrderBook;
    }
    this.setState({
      sellOrderBook: newSellOrderBook,
    });
  }

  capture() {
    if (Platform.OS === 'android') {
      this.hasAndroidPermission();
    }
    this.viewshotref.capture().then(uri => {
      CameraRoll.save(uri, 'photo').then(onfulfilled => {
        Toast.show({
          type: 'success',
          position: 'top',
          text1: Localization.t('CommonsFix.Succces'),
          text2: Localization.t('CommonsFix.SuccessfulTransaction'),
          visibilityTime: 750,
          autoHide: true,
          topOffset: 100,
          bottomOffset: 40,
          onShow: () => {},
          onHide: () => {},
          onPress: () => {},
        });
      });
    });
  }

  async hasAndroidPermission() {
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    } else {
      const status = await PermissionsAndroid.request(permission);
      if (status === 'denied') {
        return false;
      } else if (status === 'never_ask_again') {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: Localization.t('ScreenshotMessages.PermissionRequiredTitle'),
          text2: Localization.t('ScreenshotMessages.PermissionRequiredText'),
          visibilityTime: 2000,
          autoHide: true,
          topOffset: 100,
          bottomOffset: 40,
          onShow: () => {},
          onHide: () => {},
          onPress: () => {},
        });
        return false;
      } else if (status === 'granted') {
        this.capture();
      }
    }
  }

  qrCodeShare() {
    try {
      this.viewshotref.capture().then(uri => {
        const options = {
          url: uri,
        };
        Share.open(options)
          .then(res => {})
          .catch(err => {});
      });
    } catch (error) {
      console.log('Error');
    }
  }

  @autobind
  WalletAddressCodeCoppy() {
    const {coinAddress} = this.state;
    try {
      Clipboard.setString(coinAddress);
      Toast.show({
        type: 'success',
        position: 'top',
        text1: Localization.t('CommonsFix.Succces'),
        text2: Localization.t('CommonsFix.SuccessfulTransaction'),
        visibilityTime: 750,
        autoHide: true,
        topOffset: 50,
        bottomOffset: 40,
        onShow: () => {},
        onHide: () => {},
        onPress: () => {},
      });
    } catch (e) {}
  }

  componentDidMount() {
    const {data, CoinChainNetworkId} = this.state;
    this.setState({isLoadingRender: true});
    if (data) {
    }
  }

  @autobind
  renderDepositScreen(theme) {
    const {coinAddress, data, isLoadingRender} = this.state;
    console.log(coinAddress, data, 'asdasdasdasdasdas');
    if (isLoadingRender) {
      return <FAFullScreenLoader isLoading={isLoadingRender} />;
    } else {
      return (
        <View style={styles.renderDepositScreenContainer}>
          {coinAddress ? (
            <View style={styles.renderDepositScreenQrContainer}>
              <ViewShot
                style={{backgroundColor: theme?.colors?.white2, padding: 10}}
                ref={ref => {
                  this.viewshotref = ref;
                }}
                options={{format: 'jpg', quality: 0.9}}>
                <QRCode value={coinAddress} size={132} />
              </ViewShot>
            </View>
          ) : null}
          <FAWalletAdressInformation
            theme={theme}
            coinCode={data.Code}
            adress={coinAddress}
            network={data.Network}
            onPressCopy={() => this.WalletAddressCodeCoppy()}
          />
          <View style={styles.renderDepositScreenChildContainer}>
            <View style={styles.flexDirectionRow}>
              <FAButton
                text={Localization.t('WalletFix.SaveToGallery')}
                containerStyle={[
                  styles.renderDepositScreenSaveToGalleryButtonContainer,
                  {backgroundColor: theme?.colors?.darkBlue},
                ]}
                textStyle={[
                  styles.renderDepositScreenSaveToGalleryButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                onPress={() => this.capture()}
              />
              <FAButton
                text={Localization.t('WalletFix.Share')}
                containerStyle={[
                  styles.renderDepositScreenShareButtonContainer,
                  {backgroundColor: theme?.colors?.darkBlue},
                ]}
                textStyle={[
                  styles.renderDepositScreenShareButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                onPress={() => this.qrCodeShare()}
              />
            </View>
          </View>
        </View>
      );
    }
  }

  @autobind
  calculateTotalCoinAmount(data) {
    const calculatedAmount = toFixedNoRounding(data.Balance, 5);
    this.setState({
      sendingCoinAmount: String(calculatedAmount),
    });
  }

  callCoinWithdrawService() {
    this.setState({isLoading: true});
    const {sendingCoinAdress, sendingCoinAmount, sendingDestinationID, data} =
      this.state;

    if (data) {
      const requestBody = {
        CoinId: data.AssetId,
        Address: sendingCoinAdress,
        Tag: sendingDestinationID,
        Amount:
          parseFloat(sendingCoinAmount) - parseFloat(data.WithdrawalCommission),
        ValidationCode: '',
        PinNumber: '',
        Password: '',
        IsCompleteProcess: false,
        RequestCode: '',
        LanguageCode: 'en',
      };

      FAService.CoinWithdraw(requestBody)
        .then(response => {
          if (response && response.data && response.data.Success) {
            this.setState({
              isLoading: false,
            });

            Toast.show({
              type: 'success',
              text1: Localization.t('CommonsFix.Succces'),
              text2: response.data.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 75,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
          } else {
            this.setState({
              isLoading: false,
              showDialog: true,
              serviceMessage: response.data.Message,
            });
          }
        })
        .catch(error => {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
          });
        });
    }
  }

  @autobind
  callBackWithdrawStream(response) {
    const {getUserWalletsAction} = this.props;
    const viewRef = this;
    if (response && response.Data) {
      const data = response.Data;
      if (data.IsCompleted) {
        this.setState(
          {
            showApproveModal: false,
            emailAuth: false,
            smsAuth: false,
            googleAuth: false,
            pinAuth: false,
            requestCode: null,
            isLoading: false,
            ibanNo: '',
            currencyAmount: '',
          },
          () => {
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('CommonsFix.Succces'),
              text2: response.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 100,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
            setTimeout(() => {
              getUserWalletsAction();
              viewRef &&
                viewRef.props &&
                viewRef.props.navigation &&
                viewRef.props.navigation.goBack();
            }, 750);
          },
        );
      } else if (
        data &&
        !data.IsCompleted &&
        data.Errors &&
        data.Errors.length > 0
      ) {
        this.setState({
          showApproveModal: false,
          emailAuth: false,
          smsAuth: false,
          googleAuth: false,
          pinAuth: false,
          requestCode: null,
          isLoading: false,
          ibanNo: '',
          currencyAmount: '',
          showDialog: true,
          serviceMessage: data.Errors[0],
        });
      } else {
        this.setState({
          isLoading: false,
          showApproveModal: true,
          emailAuth: data.IsEmailAuthSet,
          smsAuth: data.IsSmsAuthSet,
          googleAuth: data.IsTwoFactorAuthSet,
          pinAuth: data.IsPinRequired,
          passwordAuth: data.IsPasswordRequired,
          requestCode: data.RequestCode,
        });
      }
    }
  }

  callApproveWithdrawService() {
    const {
      smsAuthValue,
      emailAuthValue,
      googleAuthValue,
      pinAuthValue,
      passwordAuthValue,
      requestCode,
      sendingCoinAdress,
      sendingCoinAmount,
      sendingDestinationID,
      data,
    } = this.state;
    this.setState({
      isLoading: true,
      showApproveModal: false,
    });

    const requestBody = {
      CoinId: data.AssetId,
      IsCompleteProcess: true,
      PinNumber: pinAuthValue,
      ValidationCode: smsAuthValue || googleAuthValue,
      MailValidationCode: emailAuthValue,
      RequestCode: requestCode,
      Address: sendingCoinAdress,
      Tag: sendingDestinationID,
      Amount:
        parseFloat(sendingCoinAmount) - parseFloat(data.WithdrawalCommission),
      Password: passwordAuthValue || null,
      LanguageCode: 'tr',
    };
    FAService.CoinWithdraw(requestBody)
      .then(response => {
        if (response && response.data && response.data.Success) {
          this.setState({
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          showDialog: true,
          serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
        });
      });
  }

  @autobind
  isReadyForServiceCall() {
    try {
      const {sendingCoinAmount, FormIsValid, data} = this.state;

      if (data) {
        const {WithdrawalCommission, WithdrawalLowerLimit} = data;
        const calculatedCoinAmountWithCom =
          parseFloat(sendingCoinAmount) - parseFloat(WithdrawalCommission);
        if (
          FormIsValid &&
          calculatedCoinAmountWithCom >= parseFloat(WithdrawalLowerLimit) &&
          calculatedCoinAmountWithCom <= data.Balance
        ) {
          return false;
        }
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  @autobind
  renderWithdrawScreen(theme) {
    const {
      sendingCoinAdress,
      sendingCoinAmount,
      showCamera,
      data,
      showApproveModal,
      smsAuth,
      emailAuth,
      googleAuth,
      pinAuth,
      passwordAuth,
      showDialog,
      serviceMessage,
    } = this.state;
    const {navigation} = this.props;
    if (showCamera) {
      return (
        <FAQrScanner
          showCamera={showCamera}
          onShotCamera={image => this.onShotCamera(image)}
          closeCamera={() => this.setState({showCamera: false})}
          onQrReaded={inComingReferenceCode =>
            this.setState({
              showCamera: false,
              sendingCoinAdress: inComingReferenceCode,
            })
          }
        />
      );
    }
    const calculatedCoinAmountWithCom =
      parseFloat(sendingCoinAmount) - parseFloat(data.WithdrawalCommission);
    return (
      <KeyboardAwareScrollView>
        <FAWalletHeaderDetail theme={theme} data={data} />
        <FAAlertModal
          theme={theme}
          visible={showDialog}
          text={serviceMessage}
          iconName={'times-circle'}
          header={Localization.t('CommonsFix.Error')}
          onPress={() => this.setState({showDialog: false})}
        />
        <FAModal.WithSecurityInput
          theme={theme}
          visible={showApproveModal}
          smsCode={smsAuth}
          onChangeSmsCode={value => this.setState({smsAuthValue: value})}
          emailCode={emailAuth}
          onChangeEmailCode={value => this.setState({emailAuthValue: value})}
          googleCode={googleAuth}
          onChangeGoogleCode={value => this.setState({googleAuthValue: value})}
          pinCode={pinAuth}
          onChangePinCode={value => this.setState({pinAuthValue: value})}
          passwordCode={passwordAuth}
          onChangePasswordCode={value =>
            this.setState({passwordAuthValue: value})
          }
          onPressSendButton={() => this.callApproveWithdrawService()}
          onPressCancelButton={() =>
            this.setState({
              showApproveModal: false,
            })
          }
        />
        <View style={styles.marginHorizontal10}>
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.TotalBalance')}
            value={toFixedNoRounding(data.Balance + data.InOrderBalance, 5)}
            coinName={data.Code}
          />
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.BuySellBalance')}
            value={toFixedNoRounding(data.InOrderBalance, 5)}
            coinName={data.Code}
          />
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.AvailableBalance')}
            value={toFixedNoRounding(data.Balance, 5)}
            coinName={data.Code}
          />
          <View style={{marginTop: 25}} />
          <FAForm
            formValidSituationChanged={value =>
              this.setState({
                FormIsValid: value,
              })
            }>
            <FATextInput.WithRightIcon
              theme={theme}
              rule={'notEmptyAndNil'}
              errorMessage={Localization.t('WalletFix.errorMessage')}
              label={Localization.t('WalletFix.RecieverBitciAdress', {
                Code: data.Code,
              })}
              placeholder={Localization.t('WalletFix.RecieverBitciAdress', {
                Code: data.Code,
              })}
              value={sendingCoinAdress}
              onChangeValue={value =>
                this.setState({
                  sendingCoinAdress: value,
                })
              }
              rightIconComponent={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24">
                  <Path d="M0 0h24v24H0z" fill="none" />
                  <Path
                    d="M15 3h6v5h-2V5h-4zM9 3v2H5v3H3V3zm6 18v-2h4v-3h2v5zm-6 0H3v-5h2v3h4zM3 11h18v2H3z"
                    fill="#767d8d"
                  />
                </Svg>
              }
              onPressRightIcon={() =>
                this.setState({
                  showCamera: true,
                })
              }
            />
          </FAForm>
          <View style={styles.marginTop20} />
          <FATextInput.Money
            theme={theme}
            fontSize={14}
            placeholder={Localization.t('WalletFix.AmountToBeSend')}
            currencyOrCoinCode={data.Code}
            onPressIncrement={() => this.calculateTotalCoinAmount(data)}
            label={Localization.t('WalletFix.Amount')}
            onChangeValue={value =>
              this.setState({
                sendingCoinAmount: value,
              })
            }
            value={sendingCoinAmount}
          />
          <View style={styles.marginTop20} />
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.MinumumWithdrawAmount')}
            value={data.WithdrawalLowerLimit}
            coinName={data.Code}
          />
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.TransactionFee')}
            value={data.WithdrawalCommission}
            coinName={data.Code}
          />
          <FACryptoWithdrawInfo
            theme={theme}
            text={Localization.t('WalletFix.AmountToBeSend')}
            value={
              calculatedCoinAmountWithCom > 0
                ? toFixedNoRounding(calculatedCoinAmountWithCom, 5)
                : '0'
            }
            coinName={data.Code}
          />
          <FAButton
            disabled={this.isReadyForServiceCall()}
            onPress={() => this.callCoinWithdrawService()}
            containerStyle={[
              styles.withdrawButtonContainer,
              {backgroundColor: theme?.colors?.green},
            ]}
            textStyle={[
              styles.withdrawButtontextStle,
              {color: theme?.colors?.white2},
            ]}
            text={Localization.t('WalletFix.ConfirmOrder')}
          />
          <View style={styles.HistoryButtonContainer}>
            <FAButton
              onPress={() =>
                navigation.navigate('CoinWithdrawTransactionHistoryScreen', {
                  type: 1,
                  CoinData: data,
                })
              }
              containerStyle={[
                styles.ActiveOrdersButtonContainer,
                {backgroundColor: theme?.colors?.darkBlue},
              ]}
              textStyle={[
                styles.ActiveOrderButtonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={Localization.t('WalletFix.ActiveOrders')}
            />
            <FAButton
              onPress={() =>
                navigation.navigate('CoinWithdrawTransactionHistoryScreen', {
                  type: 2,
                  CoinData: data,
                })
              }
              containerStyle={[
                styles.TransactionHistoryButtonContainer,
                {backgroundColor: theme?.colors?.darkBlue},
              ]}
              textStyle={[
                styles.TransactionHistoryButtonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={Localization.t('CommonsFix.TransactionHistory')}
            />
          </View>
          <Text style={styles.firstInftoTextStyle}>
            {Localization.t('WalletFix.InformatitonText1')}
          </Text>
          <Text style={styles.secondInfoTextStyle}>
            {Localization.t('WalletFix.InformatitonText2')}
          </Text>
        </View>
      </KeyboardAwareScrollView>
    );
  }

  @autobind
  renderScene({route}, theme) {
    switch (route.key) {
      case 'deposit':
        return this.renderDepositScreen(theme);
      case 'withdraw':
        return this.renderWithdrawScreen(theme);
      case 'swap':
        return this.tabRenderSwap(theme);
      default:
        return null;
    }
  }

  swapCalculation(incomingTotalPrice = null, incomingAmount = null) {
    const {amount, totalPrice, buyOrderBook, sellOrderBook} = this.state;
    const state = {
      currentMarketFlowType: StockMarketTypes.Market,
      percent: null,
      flow: BUYFLOW,
      limitPrice: null,
      amount: amount,
      totalPrice: totalPrice,
      buyOrderBook: buyOrderBook,
      sellOrderBook: sellOrderBook,
    };
    let response = {};
    response = stockCalculationHelper(
      state,
      incomingTotalPrice,
      incomingAmount,
    );
    this.setState(prevState => ({
      amount:
        response.amount || response.amount === null
          ? response.amount
          : prevState.input.amount,
      totalPrice:
        response.totalPrice || response.totalPrice === null
          ? response.totalPrice
          : prevState.input.totalPrice,
    }));
  }

  createOrder() {
    const {sellOrderBook, amount} = this.state;
    const generatedPrice = sellOrderBook[0]?.Price;
    const {navigation, getUserFinanceStateAction} = this.props;

    const requestBody = {
      CoinId: 29, //Bitci coin id
      CurrencyId: 2008, //Usdt currency id
      Price: generatedPrice,
      Amount: toFixedNoRounding(Number(amount), 8),
      StopLossPrice: null,
      OrderType: BUYFLOW,
    };
    this.setState({isLoading: true});
    FAService.CreateOrder(requestBody)
      .then(response => {
        if (response?.data) {
          const resData = response.data;
          if (resData.IsSuccess) {
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('CommonsFix.Succces'),
              visibilityTime: 500,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {
                getUserFinanceStateAction();
                navigation.goBack();
              },
              onPress: () => {},
            });
          } else {
            this.setState({
              errorMessage: resData.Message,
            });
          }
        }
        this.setState({isLoading: false});
      })
      .catch(error => {
        console.log(error);
        this.setState({isLoading: false});
      });
  }

  tabRenderSwap(theme) {
    const {totalPrice, amount, lastOrderPrice, currencyData, mainCurrencyData} =
      this.state;

    if (!currencyData) {
      return null;
    }
    return (
      <ScrollView style={styles.scrollView}>
        {lastOrderPrice && (
          <Text style={[styles.lastPrice, {color: theme?.colors?.black}]}>
            {FACurrencyAndCoinFormatter(lastOrderPrice, 4)}{' '}
            {currencyData.Code || ''}
          </Text>
        )}
        <FATextInput.Money
          theme={theme}
          value={totalPrice}
          placeholder={Localization.t('CommonsFix.TotalPrice')}
          currencyOrCoinCode={currencyData.Code || null}
          decimalCount={4}
          onPressIncrement={() =>
            this.swapCalculation(String(currencyData.Balance || 0.0))
          }
          onChangeValue={value => this.swapCalculation(value)}
        />

        <View style={styles.marginTop8}>
          <FATextInput.Money
            theme={theme}
            value={amount}
            placeholder={Localization.t('WalletFix.Amount')}
            currencyOrCoinCode={mainCurrencyData.Code || null}
            decimalCount={4}
            onPressIncrement={() =>
              this.swapCalculation(
                null,
                String(mainCurrencyData.Balance || 0.0),
              )
            }
            onChangeValue={value => this.swapCalculation(null, value)}
          />
        </View>
        <View style={styles.marginTop8}>
          <FABalanceLine
            theme={theme}
            currency={currencyData.Code || ''}
            balance={
              FACurrencyAndCoinFormatter(currencyData.Balance, 2) || '0.0'
            }
          />
        </View>
        <View style={styles.marginTop8}>
          <FABalanceLine
            theme={theme}
            currency={mainCurrencyData.Code || ''}
            balance={
              FACurrencyAndCoinFormatter(mainCurrencyData.Balance, 2) || '0.0'
            }
          />
        </View>
        <FAButton
          onPress={() => this.createOrder()}
          text={Localization.t('CommonsFix.Buy')}
          containerStyle={[
            styles.createOrderContainer,
            {backgroundColor: theme?.colors?.success},
          ]}
          textStyle={[styles.createOrderText, {color: theme?.colors?.white2}]}
        />
      </ScrollView>
    );
  }

  @autobind
  renderTabBar(props, theme) {
    const {index} = this.state;
    return (
      <TabBar
        {...props}
        renderLabel={({route, focused, color}) => (
          <Text
            style={{
              color: focused
                ? route.key === 'deposit'
                  ? theme?.colors?.success
                  : theme?.colors?.danger
                : theme?.colors?.darkBlue,
            }}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={{
          borderColor: theme?.colors?.darkBlue,
        }}
        indicatorStyle={{
          backgroundColor:
            index === 0 ? theme?.colors?.success : theme?.colors?.danger,
        }}
        style={{
          backgroundColor: theme?.colors?.white,
        }}
        labelStyle={{
          color: theme?.colors?.success,
        }}
      />
    );
  }

  render() {
    const {navigation} = this.props;
    const {index, routes, isLoading, errorMessage} = this.state;
    const initialLayout = {width: Dimensions.get('window').width};
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAFullScreenLoader isLoading={false} />
            <FAAlertModal
              theme={theme}
              iconName={'times-circle'}
              visible={!_.isNil(errorMessage)}
              header={Localization.t('CommonsFix.Error')}
              text={errorMessage}
              onPress={() => this.setState({errorMessage: null})}
            />
            <FAStandardHeader
              title={Localization.t('CommonsFix.Wallet')}
              navigation={navigation}
            />
            <TabView
              renderTabBar={props => this.renderTabBar(props, theme)}
              navigationState={{index, routes}}
              renderScene={props => this.renderScene(props, theme)}
              onIndexChange={(indexValue: number) =>
                this.setState({index: indexValue})
              }
              initialLayout={initialLayout}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default connect(null, {
  getUserWalletsAction,
  getUserFinanceStateAction,
})(WalletScreenDetail);
