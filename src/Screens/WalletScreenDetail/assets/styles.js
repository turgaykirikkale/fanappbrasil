import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';
import {Success, White} from '../../../Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {backgroundColor: FAColor.White, flex: 1},
  depositScreenContainer: {flex: 1},
  merginHorizontal5: {marginHorizontal: 5},
  qrConrtainer: {alignItems: 'center', marginVertical: 18},
  qrCodeContainer: {
    flexDirection: 'row',
    backgroundColor: FAColor.BlueGray,
    paddingHorizontal: 32,
    paddingVertical: 12,
    marginVertical: 5,
  },
  flex1: {flex: 1},
  qrCodeHeaderTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    marginBottom: 8,
  },
  coinAddressTextStyle: {color: FAColor.Black},
  coppyCodeButtonStyle: {alignSelf: 'center', marginLeft: 30, marginTop: 5},
  buttonsContainer: {
    marginHorizontal: 10,
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 5,
  },
  flexDirectionRow: {flexDirection: 'row'},
  saveToGalleryButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    flex: 1,
    marginRight: 8,
    borderRadius: 4,
  },
  saveToGalleryButtonTextStyle: {color: FAColor.Black, marginVertical: 12},
  shareQrCodeButtonContainer: {
    backgroundColor: FAColor.Orange,
    flex: 1,
    marginLeft: 8,
    borderRadius: 4,
  },
  shareQrCodeButtonTextStyle: {color: FAColor.White, marginVertical: 12},
  withdrawScreenContainer: {marginTop: 16, marginHorizontal: 10},
  headerItemCodeTextStyle: {fontSize: 16, color: FAColor.Black},
  headerItemNameTextStyle: {
    marginLeft: 4,
    fontSize: 16,
    color: FAColor.DarkGray,
  },
  headerItemBalanceContainerStyle: {flexDirection: 'row', marginTop: 2},
  headerItemBalanceTextStyle: {fontSize: 14, color: FAColor.Black},
  headerItemBalanceCodeTextStyle: {
    marginLeft: 4,
    fontSize: 14,
    color: FAColor.DarkGray,
  },
  headerDividerStyle: {
    borderWidth: 2,
    marginVertical: 14,
    borderColor: FAColor.VBlueGray,
  },
  marginHorizontal10: {marginHorizontal: 10},
  marginTop20: {marginTop: 20},

  withdrawButtonContainer: {
    backgroundColor: '#03A66D',
    borderRadius: 4,
    marginVertical: 15,
  },
  withdrawButtontextStle: {
    marginVertical: 12,
    color: FAColor.White,
  },
  firstInftoTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    letterSpacing: -0.5,
  },
  secondInfoTextStyle: {
    fontSize: 12,
    color: FAColor.DarkBlue,
    letterSpacing: -0.5,
    marginVertical: 15,
  },
  HistoryButtonContainer: {flexDirection: 'row', marginBottom: 8},
  ActiveOrdersButtonContainer: {
    marginRight: 3,
    flex: 1,
    borderRadius: 4,
  },
  ActiveOrderButtonTextStyle: {color: FAColor.DarkBlue, marginVertical: 12},
  TransactionHistoryButtonContainer: {
    marginLeft: 3,
    backgroundColor: FAColor.VBlueGray,
    flex: 1,
    borderRadius: 4,
  },
  TransactionHistoryButtonTextStyle: {
    color: FAColor.DarkBlue,
    marginVertical: 12,
  },
  renderDepositScreenContainer: {flex: 1},
  renderDepositScreenQrContainer: {alignItems: 'center', marginVertical: 18},
  renderDepositScreenChildContainer: {
    marginHorizontal: 10,
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 5,
  },
  renderDepositScreenSaveToGalleryButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    flex: 1,
    marginRight: 8,
    borderRadius: 4,
  },
  renderDepositScreenSaveToGalleryButtonTextStyle: {
    color: FAColor.Black,
    marginVertical: 12,
  },
  renderDepositScreenShareButtonContainer: {
    backgroundColor: FAColor.Orange,
    flex: 1,
    marginLeft: 8,
    borderRadius: 4,
  },
  renderDepositScreenShareButtonTextStyle: {fontSize: 15, marginVertical: 15},

  //Swap tab styles
  scrollView: {
    paddingHorizontal: 14,
    paddingVertical: 16,
    flex: 1,
  },
  lastPrice: {
    fontSize: 24,
    fontWeight: 'bold',
    paddingBottom: 8,
    textAlign: 'center',
  },
  marginTop8: {marginTop: 8},
  createOrderContainer: {
    paddingVertical: 12,
    backgroundColor: Success,
    borderRadius: 4,
    marginTop: 12,
  },
  createOrderText: {color: White},
});
