import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  headerLeftIconStyle: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  renderItemContainer: {borderBottomWidth: 1},
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  searchBarContainer: {marginHorizontal: 10, marginVertical: 16},
});
