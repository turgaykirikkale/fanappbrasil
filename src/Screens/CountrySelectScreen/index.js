import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import FACountries from '@Commons/FACountries';
import FAAccountMenuItems from '@Components/Composite/FAAccountMenuItems';
import {Success, White} from '@Commons/FAColor';
import autobind from 'autobind-decorator';
import FASearchBar from '@Components/Composite/FASearchBar';
import {styles} from './assets/styles';
import _ from 'lodash';
import Localization from '@Localization';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

export default class CountrySelectScreen extends Component {
  constructor(props) {
    super(props);
    this.data = FACountries;
    this.state = {
      data: this.data,
      selectedCountry:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.phoneCode,
      type:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.type,
    };
  }

  @autobind
  searchEngineOnChange(value) {
    if (value.length > 0) {
      let newSearchedArr = [];
      _.filter(this.data, item => {
        if (
          item.name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
          item.code.indexOf(value) > -1
        ) {
          newSearchedArr.push(item);
        }
      });
      this.setState({
        data: newSearchedArr,
      });
    } else {
      this.setState({
        data: _.sortBy(this.data, 'name'),
      });
    }
  }

  @autobind
  selectCountry(item) {
    const {type} = this.state;
    this.setState(
      {
        selectedCountry: item.code,
      },
      () => {
        this.props.route.params.onPhoneSelect(item);
        this.props.navigation.goBack();
      },
    );
  }

  renderItem(item, theme) {
    return (
      <View
        style={[
          styles.renderItemContainer,
          {borderColor: theme?.colors?.softGray},
        ]}>
        <FAAccountMenuItems.WithCountryCode
          iconName={item.flag}
          countryCode={item.code}
          CountryName={item.name}
          checked={item.code === this.state.selectedCountry ? true : false}
          fillColor={Success}
          tickColor={White}
          navigation={this.props.navigation}
          onPress={() => this.selectCountry(item)}
        />
      </View>
    );
  }

  render() {
    const {data} = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('CountrySelectScreen.SelectCountry')}
              navigation={navigation}
            />
            <FASearchBar
              placeholder={Localization.t('CountrySelectScreen.SearchCountry')}
              onChangeText={value => this.searchEngineOnChange(value)}
            />

            <FlatList
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              data={data}
              renderItem={({item}) => this.renderItem(item, theme)}
              keyExtractor={(item, index) => index}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
