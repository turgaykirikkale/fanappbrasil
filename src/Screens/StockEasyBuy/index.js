import React, {Component} from 'react';
import {SafeAreaView, View, ScrollView, Text, StatusBar} from 'react-native';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import {setStockModeAction} from '@GlobalStore/Actions/MarketActions';
import {connect} from 'react-redux';
import FAModeSelectorBox from '@Components/Composite/FAModeSelectorBox';
import {StockMode} from '@Commons/FAEnums';
import autobind from 'autobind-decorator';
import {setDataToStorage} from '@Commons/FAStorage';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import FAStockHeader from '@Components/Composite/FAStockHeader';
import {AppConstants} from '@Commons/Contants';
import {styles} from './assets/styles';
import {BlueGray, DarkBlue, Green, Red, White} from '@Commons/FAColor';
import _ from 'lodash';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {
  subscribeOrderBookTickerChannel,
  unSubscribeOrderBookTickerChannel,
  subscribeMarketHistoryChannel,
  unSubscribeMarketHistoryChannel,
  subscribeCustomerOrderTickerChannel,
  unSubscribeCustomerOrderTickerChannel,
  subscribeBitciTryTickerChannel,
  unSubscribeBitciTryTickerChannel,
} from '@Stream';
import FAService from '@Services';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import {orderBookDataRenderer} from '@Helpers/StockStreamLogic';
import FAButton from '@Components/Composite/FAButton';
import FATextInput from '@Components/Composite/FATextInput';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import {stockCalculationHelper} from '@Helpers/StockCalculationHelper';
import FABalanceLine from '@Components/UI/FABalanceLine';
import Toast from 'react-native-toast-message';
import {toFixedNoRounding} from '@Commons/FAMath';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class StockEasyBuySellScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPair: props?.route?.params?.params?.pairDetail || {},
      flow: props?.route?.params?.params?.flow || BUYFLOW,
      isAnonymousFlow: props?.route?.params?.isAnonymous,
      isModeBoxOpen: false,
      amount: null,
      totalPrice: null,
      orderBookLoading: false,
      createOrderLoading: false,
      lastOrderPrice: 0.0,
      marketQueueName: null,
      buyOrderBook: [],
      sellOrderBook: [],
      coinBalance: 0.0,
      currencyBalance: 0.0,
      errorMessage: null,
      Change24H: '0.0',
      bitciTryPrice: null,
    };
  }

  _unsubscribeFocusListener = null;
  _unsubscribeBlurListener = null;

  componentWillMount() {
    const {navigation} = this.props;
    this._unsubscribeFocusListener = navigation.addListener('focus', () => {
      this.getMarketQueueNameAndFillAllData();
    });

    this._unsubscribeBlurListener = navigation.addListener('blur', () => {
      unSubscribeOrderBookTickerChannel();
      unSubscribeCustomerOrderTickerChannel();
      unSubscribeMarketHistoryChannel();
      unSubscribeBitciTryTickerChannel();
    });
  }

  componentWillUnmount() {
    this._unsubscribeFocusListener();
    this._unsubscribeBlurListener();
    unSubscribeOrderBookTickerChannel();
    unSubscribeCustomerOrderTickerChannel();
    unSubscribeMarketHistoryChannel();
    unSubscribeBitciTryTickerChannel();
  }

  @autobind
  handleBitciTryTicker(data) {
    if (data?.p) {
      this.setState({
        bitciTryPrice: data.p,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const {UserState} = this.props;
    const {marketQueueName} = this.state;
    if (nextProps?.UserState && nextProps?.UserState !== UserState) {
      this.fillCustomerBalance(nextProps.UserState);
    }

    if (
      nextProps?.MarketState?.MarketQueueName &&
      nextProps.MarketState.MarketQueueName !== marketQueueName
    ) {
      this.getMarketQueueNameAndFillAllData(nextProps.MarketState);
    }
  }

  @autobind
  getMarketQueueNameAndFillAllData(incomingMarketState) {
    const {MarketState} = this.props;
    let marketState = MarketState;
    if (incomingMarketState) {
      marketState = incomingMarketState;
    }
    if (marketState?.MarketQueueName && marketState?.SelectedPair) {
      const selectedPair = marketState.SelectedPair;
      this.setState(
        {
          marketQueueName: marketState.MarketQueueName,
          selectedPair: selectedPair || {},
        },
        () => {
          subscribeBitciTryTickerChannel(this.handleBitciTryTicker);
          this.fillAllData();
        },
      );
    } else {
      this.fillMarketQueueName();
    }
  }

  @autobind
  fillAllData() {
    const {isAnonymousFlow} = this.state;
    if (!isAnonymousFlow) {
      this.fillCustomerBalance();
    }
    this.fillMarketHistoryLastOrder();
    this.fillOrderBook();
  }

  @autobind
  findMatchedAssetBalance(balanceList, assetId) {
    return _.find(balanceList, {CoinId: assetId})?.CoinBalance;
  }

  @autobind
  fillCustomerBalance(userState) {
    const {MarketState} = this.props;
    let {UserState, getUserFinanceStateAction} = this.props;
    if (userState) {
      UserState = userState;
    }

    const userFinanceInfo = UserState?.FinanceInfo;
    if (
      userFinanceInfo?.CustomerCoinBalanceDetailList &&
      MarketState?.SelectedPair
    ) {
      const selectedPair = MarketState.SelectedPair;
      const customerCoinBalanceDetailList =
        userFinanceInfo.CustomerCoinBalanceDetailList;
      const matchedCurrencyBalance = this.findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        29,
      );
      const matchedCoinBalance = this.findMatchedAssetBalance(
        customerCoinBalanceDetailList,
        selectedPair.CoinId,
      );
      this.setState({
        coinBalance: matchedCoinBalance || '0.0',
        currencyBalance: matchedCurrencyBalance || '0.0',
      });
    } else {
      getUserFinanceStateAction();
    }
  }

  @autobind
  fillOrderBook() {
    const {marketQueueName, selectedPair} = this.state;
    const requestBody = {
      selectedCoinCode: selectedPair?.CoinCode || null,
      selectedCurrencyCode: selectedPair?.CurrencyCode || null,
    };
    this.setState({orderBookLoading: true});
    FAService.GetActiveOrders(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const responseData = res.data;
          let buyOrders = _.sortBy(
            _.filter(responseData, {Type: BUYFLOW}),
            'Price',
          ).reverse();
          let sellOrders = _.sortBy(
            _.filter(responseData, {Type: SELLFLOW}),
            'Price',
          );
          let sliceBuyOrders = percentExecutor(_.slice(buyOrders, 0, 15));
          let sliceSellOrders = percentExecutor(_.slice(sellOrders, 0, 15));

          this.setState(
            {
              buyOrderBook: sliceBuyOrders,
              sellOrderBook: sliceSellOrders,
              orderBookLoading: false,
              Change24H: buyOrders[0]?.Change24H,
            },
            () => {
              subscribeOrderBookTickerChannel(
                marketQueueName,
                this.orderBookChangeHandler,
              );
              subscribeCustomerOrderTickerChannel(
                marketQueueName,
                this.customerOrderChangeHandler,
              );
            },
          );
        }
      })
      .catch(err => {
        console.log('err = ', err);
        this.setState({orderBookLoading: false});
      });
  }

  @autobind
  orderBookChangeHandler(data) {
    const {buyOrderBook, sellOrderBook, Change24H} = this.state;
    const {BuyOrderBook, SellOrderBook, Change24} = orderBookDataRenderer(data);
    let newBuyOrderBook,
      newSellOrderBook,
      newChange24 = null;

    if (BuyOrderBook) {
      newBuyOrderBook = _.sortBy(BuyOrderBook, 'Price').reverse();
    } else {
      newBuyOrderBook = buyOrderBook;
    }
    if (SellOrderBook) {
      newSellOrderBook = _.sortBy(SellOrderBook, 'Price');
    } else {
      newSellOrderBook = sellOrderBook;
    }

    if (Change24) {
      newChange24 = Change24;
    } else {
      newChange24 = Change24H;
    }
    this.setState({
      buyOrderBook: newBuyOrderBook,
      sellOrderBook: newSellOrderBook,
      Change24H: newChange24,
    });
  }

  @autobind
  customerOrderChangeHandler(data) {
    const {getUserFinanceStateAction} = this.props;
    if (data) {
      setTimeout(() => {
        getUserFinanceStateAction();
      }, 1000);
    }
  }

  @autobind
  fillMarketHistoryLastOrder() {
    const {marketQueueName, selectedPair} = this.state;
    const requestBody = {
      selectedCoinCode: selectedPair?.CoinCode || null,
      selectedCurrencyCode: selectedPair?.CurrencyCode || null,
    };
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response?.data && response.status === 200) {
          const incomingLastOrderPrice = response.data[0]?.Price;
          if (incomingLastOrderPrice) {
            this.setState(
              {
                lastOrderPrice: incomingLastOrderPrice,
              },
              () => {
                subscribeMarketHistoryChannel(
                  marketQueueName,
                  this.marketHistoryChangeHandler,
                );
              },
            );
          }
        }
      })
      .catch(err => console.log('Errr = ', err));
  }

  @autobind
  marketHistoryChangeHandler(data) {
    const {lastOrderPrice} = this.state;
    if (data?.Price && lastOrderPrice !== data.Price) {
      this.setState({
        lastOrderPrice: data?.Price,
      });
    }
  }

  @autobind
  fillMarketQueueName() {
    const {selectedPair} = this.state;
    const requestBody = {
      CurrencyId: selectedPair?.CurrencyId || null,
      CoinId: selectedPair?.CoinId || null,
    };
    FAService.GetMarketInformation(requestBody)
      .then(response => {
        if (response?.data && response.status === 200) {
          const responseData = response.data;
          this.setState({marketQueueName: responseData?.MarketQueueName}, () =>
            this.fillAllData(),
          );
        }
      })
      .catch(err => {});
  }

  navigateToMarketScreen() {
    const {navigation} = this.props;
    navigation.navigate('MarketTabStackNavigator', {params: {from: 'stock'}});
  }

  @autobind
  navigateToChartScreen() {
    const {navigation} = this.props;
    const {selectedPair} = this.state;
    const coinCode = selectedPair?.CoinCode || null;
    const currencyCode = selectedPair?.CurrencyCode || null;
    const chartUrl = `https://chart.bitci.com/exchange/advanced/${coinCode}_${currencyCode}`;
    navigation.navigate('WebviewScreen', {
      url: chartUrl,
      title: `${coinCode}/${currencyCode} ${Localization.t(
        'StockEasyBuy.Chart',
      )}`,
    });
  }

  @autobind
  navigateToLoginRegisterStack() {
    const {navigation} = this.props;
    if (navigation) {
      navigation.navigate('FanTabStackNavigator');
    }
  }

  @autobind
  dispatchSelectedStockMode(newStockMode) {
    const {flow} = this.state;
    const {navigation} = this.props;
    if (newStockMode && newStockMode !== StockMode.EasyBuySell) {
      const {setStockModeAction} = this.props;
      setStockModeAction(newStockMode);
      this.setState({
        isModeBoxOpen: false,
      });
      setDataToStorage('StockMode', newStockMode);
      if (newStockMode === StockMode.Standard) {
        navigation.navigate('StockStandardScreen', {flow});
      } else {
        navigation.navigate('StockAdvanced', {flow});
      }
    }
  }

  handleFlowChange(newFlow) {
    this.setState({
      flow: newFlow,
      amount: null,
      totalPrice: null,
    });
  }

  calculation(incomingValue) {
    const {flow, amount, totalPrice, buyOrderBook, sellOrderBook} = this.state;
    if (incomingValue) {
      const state = {
        currentMarketFlowType: StockMarketTypes.Market,
        percent: null,
        flow: flow,
        limitPrice: null,
        amount: amount,
        totalPrice: totalPrice,
        buyOrderBook: buyOrderBook,
        sellOrderBook: sellOrderBook,
      };
      let response = {};
      if (flow === BUYFLOW) {
        response = stockCalculationHelper(state, incomingValue);
        if (response?.amount) {
          this.setState({
            amount: response.amount,
          });
        }
      } else {
        response = stockCalculationHelper(state, null, incomingValue);
        if (response?.totalPrice) {
          this.setState({
            totalPrice: response.totalPrice,
          });
        }
      }
    }
  }

  createOrder() {
    const {getUserFinanceStateAction} = this.props;
    const {flow, sellOrderBook, buyOrderBook, amount, selectedPair} =
      this.state;
    let generatedPrice = 0;
    if (flow === BUYFLOW) {
      generatedPrice = sellOrderBook[0]?.Price;
    } else {
      generatedPrice = buyOrderBook[0]?.Price;
    }
    const requestBody = {
      CoinId: selectedPair?.CoinId || null,
      CurrencyId: selectedPair?.CurrencyId || null,
      Price: generatedPrice,
      Amount: toFixedNoRounding(Number(amount), 8),
      StopLossPrice: null,
      OrderType: flow,
    };
    this.setState({
      orderBookLoading: true,
    });
    FAService.CreateOrder(requestBody)
      .then(res => {
        if (res?.data && res.status === 200) {
          const resData = res.data;
          if (resData.IsSuccess) {
            getUserFinanceStateAction();
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('Commons.Succes'),
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
            this.setState({
              orderBookLoading: false,
              createOrderLoading: false,
            });
          } else {
            // IsSuccess değilse
            this.setState({
              errorMessage: resData.Message,
              orderBookLoading: false,
              createOrderLoading: false,
            });
          }
        } else {
          // Response 200 değilse
          this.setState({
            orderBookLoading: false,
            createOrderLoading: false,
          });
        }
      })
      .catch(err => {
        this.setState({
          orderBookLoading: false,
          createOrderLoading: false,
        });
        console.log('Err = ', err);
      });
  }

  render() {
    const {
      isModeBoxOpen,
      errorMessage,
      orderBookLoading,
      createOrderLoading,
      lastOrderPrice,
      flow,
      amount,
      totalPrice,
      currencyBalance,
      coinBalance,
      Change24H,
      bitciTryPrice,
      selectedPair,
    } = this.state;
    const isAnonymousFlow = this.props?.route?.params?.isAnonymous;
    const currencyDecimal = selectedPair?.PriceDecimal || 2;
    const coinDecimal = selectedPair?.CoinDecimal || 4;
    const currencyCode = selectedPair?.CurrencyCode || '';
    const coinCode = selectedPair?.CoinCode || '';

    const formattedCurrencyBalance = FACurrencyAndCoinFormatter(
      currencyBalance,
      currencyDecimal,
    );
    const formattedCoinBalance = FACurrencyAndCoinFormatter(
      coinBalance,
      coinDecimal,
    );

    const formattedLastPrice = FACurrencyAndCoinFormatter(
      lastOrderPrice,
      coinDecimal,
    );

    let buyButtonText = `${coinCode} ${Localization.t('StockEasyBuy.Buy')}`;
    let sellButtonText = `${coinCode} ${Localization.t('StockEasyBuy.Sell')}`;
    if (isAnonymousFlow) {
      buyButtonText = Localization.t('LoginOrRegister');
      sellButtonText = Localization.t('LoginOrRegister');
    }
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <SafeAreaView
            style={{flex: 1, backgroundColor: theme?.colors?.white}}>
            <FAFullScreenLoader
              isLoading={orderBookLoading || createOrderLoading}
            />
            <FAModeSelectorBox
              onPressClose={() => this.setState({isModeBoxOpen: false})}
              selectedMode={StockMode.EasyBuySell}
              onChangeMode={newMode => this.dispatchSelectedStockMode(newMode)}
              visible={isModeBoxOpen}
            />
            <FAStockHeader
              pair={selectedPair}
              favoriteDisable={isAnonymousFlow}
              coinCode={coinCode}
              currencyCode={currencyCode}
              dailyChange={Change24H}
              onPressPair={() => this.navigateToMarketScreen()}
              onPressChart={() => this.navigateToChartScreen()}
              onPressMode={() =>
                this.setState({
                  isModeBoxOpen: true,
                })
              }
            />
            <FAAlertModal
              visible={!_.isEmpty(errorMessage)}
              text={errorMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() =>
                this.setState({
                  errorMessage: null,
                })
              }
            />
            <ScrollView
              style={[
                styles.scrollView,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <View style={styles.mainContainer}>
                <View style={styles.marketInformationContainer}>
                  <View style={styles.coinCodeAndCoinNameContainer}>
                    <Text
                      style={[styles.coinCode, {color: theme?.colors?.black}]}>
                      {coinCode}
                    </Text>
                    <Text
                      style={[
                        styles.coinName,
                        {color: theme?.colors?.darkGray},
                      ]}>
                      {selectedPair?.CoinName || ''}
                    </Text>
                  </View>
                  <View style={styles.flexRow}>
                    <Text
                      style={[styles.lastPrice, {color: theme?.colors?.black}]}>
                      {formattedLastPrice} {selectedPair?.CurrencyCode || ''}
                    </Text>
                    {bitciTryPrice && (
                      <Text
                        style={{
                          paddingBottom: 20,
                          marginLeft: 6,
                          color: theme?.colors?.darkBlue,
                          fontSize: 18,
                        }}>
                        ≈{' '}
                        {FACurrencyAndCoinFormatter(
                          parseFloat(lastOrderPrice) *
                            parseFloat(bitciTryPrice),
                          currencyDecimal,
                        )}{' '}
                        TRY
                      </Text>
                    )}
                  </View>
                </View>
                <View style={styles.flexRow}>
                  <View style={styles.flex1}>
                    <FAButton
                      onPress={() => this.handleFlowChange(BUYFLOW)}
                      text={`${coinCode} ${Localization.t('StockEasyBuy.Buy')}`}
                      containerStyle={[
                        styles.buyButtonContainer,
                        {
                          backgroundColor:
                            flow === BUYFLOW
                              ? theme?.colors?.green
                              : theme?.colors?.blueGray,
                        },
                      ]}
                      textStyle={[
                        styles.buyButtonText,
                        {
                          color:
                            flow === BUYFLOW
                              ? theme?.colors?.white2
                              : theme?.colors?.darkBlue,
                        },
                      ]}
                    />
                  </View>
                  <View style={styles.flex1}>
                    <FAButton
                      onPress={() => this.handleFlowChange(SELLFLOW)}
                      text={`${coinCode} ${Localization.t(
                        'StockEasyBuy.Sell',
                      )}`}
                      containerStyle={[
                        styles.sellButtonContainer,
                        {
                          backgroundColor:
                            flow === SELLFLOW
                              ? theme?.colors?.red
                              : theme?.colors?.blueGray,
                        },
                      ]}
                      textStyle={[
                        styles.sellButtonText,
                        {
                          color:
                            flow === SELLFLOW
                              ? theme?.colors?.white2
                              : theme?.colors?.darkBlue,
                        },
                      ]}
                    />
                  </View>
                </View>
                <View style={styles.transactionInformationContainer}>
                  {flow === BUYFLOW && (
                    <Text
                      style={[
                        styles.transactionInformation,
                        {color: theme?.colors?.black},
                      ]}>
                      {Localization.t('StockEasyBuy.BuyingAmount', {
                        CoinCode: coinCode,
                        CurrencyCode: currencyCode,
                      })}
                    </Text>
                  )}
                  {flow === SELLFLOW && (
                    <Text
                      style={[
                        styles.transactionInformation,
                        {color: theme?.colors?.black},
                      ]}>
                      {Localization.t('StockEasyBuy.SellingAmount', {
                        CoinCode: coinCode,
                      })}
                    </Text>
                  )}
                </View>
                <View style={styles.inputContainer}>
                  {flow === BUYFLOW && (
                    <FATextInput.BigLine
                      onChangeValue={newValue => {
                        this.calculation(newValue);
                      }}
                      value={totalPrice}
                      decimalCurrencyCount={4}
                      placeholder={'1.200'}
                      currencyOrCoinCode={currencyCode}
                    />
                  )}
                  {flow === SELLFLOW && (
                    <FATextInput.BigLine
                      onChangeValue={newValue => {
                        this.calculation(newValue);
                      }}
                      value={amount}
                      decimalCoinCount={2}
                      placeholder={'1.200'}
                      currencyOrCoinCode={coinCode}
                    />
                  )}
                </View>
                <FABalanceLine
                  currency={flow === BUYFLOW ? currencyCode : coinCode}
                  balance={
                    flow === BUYFLOW
                      ? formattedCurrencyBalance
                      : formattedCoinBalance
                  }
                />
                <FAButton
                  onPress={() =>
                    isAnonymousFlow
                      ? this.navigateToLoginRegisterStack()
                      : this.createOrder()
                  }
                  text={flow === BUYFLOW ? buyButtonText : sellButtonText}
                  containerStyle={[
                    styles.createOrderButtonContainer,
                    {
                      backgroundColor:
                        flow === BUYFLOW
                          ? theme?.colors?.green
                          : theme?.colors?.red,
                    },
                  ]}
                  textStyle={[
                    styles.createOrderButtonText,
                    {color: theme?.colors?.white2},
                  ]}
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {
  getUserFinanceStateAction,
  setStockModeAction,
})(StockEasyBuySellScreen);
