import React, {useEffect, useState} from 'react';
import {View, ScrollView, Text} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import BroadageService from '../../Services/BroadgeService/BroadageService';
import _ from 'lodash';
import FAMatchCenterHeaderSelect from '@Components/Composite/FAMatchCenterHeaderSelect';
import {colors} from 'react-native-elements';
import FAStandingsComponent from '../../Components/UI/FAStandingsComponent';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Localization from '@Localization';

const StandingScreen = props => {
  const {navigation} = props;

  const [tournaments, setTournaments] = useState([]);
  const [seasons, setseasons] = useState([]);

  const [selectedTournament, setSelectedTournament] = useState([]);
  const [selectedSeason, setSelectedSeason] = useState(null);

  const [serviceLoading, setServiceLoading] = useState(true);
  const [standingList, setstandingList] = useState(null);

  useEffect(() => {
    BroadageService.GetTournamentsList()
      .then(res => {
        if (res?.data?.data) {
          const responseData = res?.data?.data;

          let FilteredData = [];
          _.each(responseData, item => {
            if (item.externalId === 35) {
              console.log('itemStanding', item);
              let fixedItem = {
                label: Localization.t('TeamScreen.BrasiSeriaA'),
                value: item.externalId,
                id: item.id,
              };
              FilteredData.push(fixedItem);
            }
          });
          setTournaments(FilteredData);
        }
      })
      .catch(err => console.log('GetTournamentsList', err));
  }, []);

  const SelectedTournaments = value => {
    setSelectedTournament(value);
    let filteredSeason = _.filter(tournaments, {value});
    BroadageService.GetSoccerSeaosonId(filteredSeason[0].id)
      .then(res => {
        if (res?.data?.data?.seasons) {
          const responseData = res?.data?.data?.seasons[0];
          let fixedItem = {
            label: responseData.name,
            value: responseData.id,
            stages: responseData.stages,
            externalId: res?.data?.data?.externalId,
          };
          setseasons(fixedItem);
        }
      })
      .catch(err => console.log('GetSoccerSeaosonId', err));
  };
  const SelectedSeaoson = value => {
    setSelectedSeason(value);
    BroadageService.GetSoccerSeaosonStandings(value)
      .then(res => {
        if (res?.status === 200) {
          setServiceLoading(false);
          setstandingList(res?.data);
        }
      })
      .catch(err => console.log('GetSoccerSeaosonStandings', err));
  };

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{backgroundColor: theme?.colors?.blueGray, flex: 1}}>
          <FAStandardHeader
            title={Localization.t('TeamScreen.Standings')}
            navigation={navigation}
          />
          <FAFullScreenLoader isLoading={serviceLoading} />
          <View style={{marginTop: 5}}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal
              style={{
                backgroundColor: theme?.colors?.softGray,
              }}>
              <FAMatchCenterHeaderSelect
                onValueChange={value => SelectedTournaments(value)}
                list={tournaments}
                value={selectedTournament}
              />
              <FAMatchCenterHeaderSelect
                onValueChange={value => SelectedSeaoson(value)}
                list={seasons}
                value={selectedSeason}
              />
            </ScrollView>
          </View>
          <>
            {_.isEmpty(standingList) ? (
              <View style={{marginTop: 20}}>
                <Text
                  style={{
                    color: theme?.colors?.black,
                    textAlign: 'center',
                    alignSelf: 'center',
                  }}>
                  {Localization.t('TeamScreen.ThereIsNothing')}
                </Text>
              </View>
            ) : (
              <FAStandingsComponent data={standingList} />
            )}
          </>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default StandingScreen;
