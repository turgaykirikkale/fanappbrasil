import React, {Component} from 'react';
import {View, Text, ScrollView, FlatList} from 'react-native';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAColor from '@Commons/FAColor';
import FAMatchCenterHeaderSelect from '@Components/Composite/FAMatchCenterHeaderSelect';
import FAIcon from '@Components/UI/FAIcon';
import FACustomTabView from '@Components/Composite/FACustomTabView';
import _ from 'lodash';
import FALiveMatchStaticItems from '@Components/UI/FALiveMatchStaticItems';
import FATeamLogos from '@Components/UI/FATeamLogos';
import FALiveMatchTeamsLiveLineUp from '@Components/Composite/FALiveMatchTeamsLiveLineUp';

export default class LivMatchStatics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      league: '',
      teams: '',
      season: '',
      tabIndex: 0,
      routes: [
        {
          key: 'live',
          title: 'Canlı Anlatım',
        },
        {
          title: 'Maç Kadrosu',
        },
        {
          title: 'İstatislikler',
        },
      ],
    };
  }
  onChangeTabIndex(value) {
    this.setState({
      tabIndex: value,
    });
  }

  render() {
    const {league, teams, routes, tabIndex} = this.state;
    const {navigation} = this.props;
    const liveCenter = [];
    const teamSquad = [];
    const statics = [
      {homeTeam: '14', subject: 'Toplam Şut', awayTeam: '6'},
      {homeTeam: '4', subject: 'İsabetli Şut', awayTeam: '3'},
      {homeTeam: '%48', subject: 'Topla Oynama', awayTeam: '%52'},
      {homeTeam: '12', subject: 'Faul', awayTeam: '6'},
      {homeTeam: '6', subject: 'Sarı Kart', awayTeam: '3'},
      {homeTeam: '0', subject: 'Kırmızı Kart', awayTeam: '0'},
      {homeTeam: '5', subject: 'Ofsayy', awayTeam: '1'},
      {homeTeam: '7', subject: 'Korner', awayTeam: '2'},
    ];
    const bjkLiveLineUp = [
      {time: `23'`, name: 'Caner Erkin'},
      {time: `56'`, name: 'Gökhan Gönül'},
      {time: `23'`, name: 'Burak Yılmaz'},
    ];
    return (
      <View style={{flex: 1, backgroundColor: FAColor.White}}>
        <FAStandardHeader title={'Maç Merkezi'} navigation={navigation} />
        <View
          style={{flexDirection: 'row', backgroundColor: FAColor.VBlueGray}}>
          <FAMatchCenterHeaderSelect
            onValueChange={value =>
              this.setState({
                league: value,
              })
            }
            list={[
              {
                label: 'Türkiye Süper Lig',
                value: 1,
              },
              {
                label: 'Premier Lig',
                value: 2,
              },
              {
                label: 'Bundesliga',
                value: 3,
              },
            ]}
            league={league}
          />
          <FAMatchCenterHeaderSelect
            onValueChange={value =>
              this.setState({
                teams: value,
              })
            }
            list={[
              {
                label: 'Beşiktaş-Fenerbahçe',
                value: 1,
              },
              {
                label: 'Galatasaray-Trabzonspor',
                value: 2,
              },
              {
                label: 'Konyaspor-Başakşehir',
                value: 3,
              },
            ]}
            league={teams}
          />
        </View>
        <ScrollView>
          <View style={{marginVertical: 10, marginHorizontal: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text>Süper Lig </Text>
              <Text
                style={{flex: 1, textAlign: 'center', color: FAColor.Green}}>
                88'
              </Text>
              <Text>19/07/2020</Text>
            </View>
            <View style={{flexDirection: 'row', marginVertical: 10}}>
              <View>
                <View
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    backgroundColor: 'black',
                    alignSelf: 'center',
                    marginBottom: 8,
                  }}></View>
                <Text style={{textAlign: 'center'}}>Beşiktaş J.K</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginLeft: 4,
                  marginVertical: 8,
                }}>
                <Text style={{fontSize: 36}}>3 - 2</Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: FAColor.DarkBlueO,
                  }}>
                  Seyirci 28.000
                </Text>
              </View>
              <View>
                <View
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    backgroundColor: 'yellow',
                    alignSelf: 'center',
                    marginBottom: 8,
                  }}></View>
                <Text style={{textAlign: 'center'}}>Fenerbahçe S.K</Text>
              </View>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{flex: 1}}>
                <FlatList
                  nestedScrollEnabled
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index}
                  data={bjkLiveLineUp}
                  renderItem={({item, index}) => (
                    <FALiveMatchTeamsLiveLineUp left item={item} />
                  )}
                />
              </View>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <FlatList
                  nestedScrollEnabled
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index}
                  data={bjkLiveLineUp}
                  renderItem={({item, index}) => (
                    <FALiveMatchTeamsLiveLineUp item={item} />
                  )}
                />
              </View>
            </View>
            <View style={{marginVertical: 10}}>
              <Text
                style={{
                  fontSize: 12,
                  color: FAColor.DarkBlueO,
                  marginBottom: 5,
                }}>
                Stadyum : Vodafone Park
              </Text>
              <Text style={{fontSize: 10, color: FAColor.DarkBlueO}}>
                *Tüm maçlar Türkiye saati ile gösterilmektedir
              </Text>
            </View>
          </View>
          <FACustomTabView
            currentIndex={tabIndex}
            routes={routes}
            onIndexChange={indexValue => this.onChangeTabIndex(indexValue)}
            itemCountPerPage={3}
          />
          {tabIndex === 0 ? (
            !_.isEmpty(liveCenter) ? (
              <Text>Canlı Anlatım Dizaynı Gelecek </Text>
            ) : (
              //liveCenterDesign Will Come
              <Text
                style={{
                  textAlign: 'center',
                  marginVertical: 10,
                  color: FAColor.SoftBlack,
                  fontSize: 12,
                }}>
                Canlı anlatım bulunmamaktadır.
              </Text>
            )
          ) : null}
          {tabIndex === 1 ? (
            !_.isEmpty(teamSquad) ? (
              <Text>Maç Kadrousu Dizaynı Gelecek</Text>
            ) : (
              <Text
                style={{
                  textAlign: 'center',
                  marginVertical: 10,
                  color: FAColor.SoftBlack,
                  fontSize: 12,
                }}>
                Maç kadrosu mevcut değildir.
              </Text>
            )
          ) : null}
          {tabIndex === 2 ? (
            !_.isEmpty(statics) ? (
              <View style={{marginHorizontal: 10, marginVertical: 20}}>
                <FATeamLogos header={'Takım İstatislikleri'} />
                <FlatList
                  nestedScrollEnabled
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index}
                  data={statics}
                  renderItem={({item, index}) => (
                    <FALiveMatchStaticItems item={item} />
                  )}
                />
              </View>
            ) : (
              <Text
                style={{
                  textAlign: 'center',
                  marginVertical: 10,
                  color: FAColor.SoftBlack,
                  fontSize: 12,
                }}>
                İstatislikler mevcut değildir.
              </Text>
            )
          ) : null}
        </ScrollView>
      </View>
    );
  }
}
