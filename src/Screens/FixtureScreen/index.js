import React, {useEffect, useState} from 'react';
import {View, ScrollView} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAMatchCenterHeaderSelect from '@Components/Composite/FAMatchCenterHeaderSelect';
import BroadageService from '../../Services/BroadgeService/BroadageService';
import _ from 'lodash';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FATeamResults from '../../Components/Composite/FATeamResults';
import Localization from '@Localization';

const FixtureScreen = props => {
  const {navigation} = props;

  const [tournaments, setTournaments] = useState([]);
  const [seasons, setseasons] = useState([]);
  const [stages, setStages] = useState([]);
  const [selectedTournament, setSelectedTournament] = useState([]);
  const [selectedSeason, setSelectedSeason] = useState(null);
  const [selectedStages, setSelectedStages] = useState(null);
  const [serviceLoading, setServiceLoading] = useState(true);
  const [matchList, setMatchlist] = useState([]);
  useEffect(() => {
    BroadageService.GetTournamentsList()
      .then(res => {
        if (res?.data?.data) {
          const responseData = res?.data?.data;
          let FilteredData = [];
          _.each(responseData, item => {
            if (item.externalId === 35 || item.externalId === 66) {
              let fixedItem = {
                label:
                  item.externalId === 66
                    ? Localization.t('TeamScreen.BrasilCup')
                    : Localization.t('TeamScreen.BrasiSeriaA'),
                value: item.externalId,
                id: item.id,
              };
              FilteredData.push(fixedItem);
            }
          });
          setTournaments(FilteredData);
        }
      })
      .catch(err => console.log('GetTournamentsList', err));
  }, []);

  const SelectedTournaments = value => {
    setSelectedTournament(value);
    let filteredSeason = _.filter(tournaments, {value});
    BroadageService.GetSoccerSeaosonId(filteredSeason[0].id)
      .then(res => {
        if (res?.data?.data?.seasons) {
          const responseData = res?.data?.data?.seasons[0];
          let fixedItem = {
            label: responseData.name,
            value: responseData.id,
            stages: responseData.stages,
            externalId: res?.data?.data?.externalId,
          };
          setseasons(fixedItem);
        }
      })
      .catch(err => console.log('GetSoccerSeaosonId', err));
  };

  const SelectedSeaoson = value => {
    setSelectedSeason(value);
    let selectedStage = _.filter(seasons.stages, {active: true});
    let setRounds = [];
    if (selectedStage[0]?.totalRound === 1) {
      let name = selectedStage[0]?.name.split('.');
      let fixedRounds = {
        label: `${name[0]}. ${Localization.t('TeamScreen.Tour')}`,
        value: selectedStage[0]?.activeRound,
        activeRound: true,
      };
      setRounds.push(fixedRounds);
    } else {
      for (let i = 0; i < selectedStage[0]?.totalRound; i++) {
        let item = {
          label: `${i + 1}. ${Localization.t('TeamScreen.Week')}`,
          value: i + 1,
          activeRound: selectedStage[0]?.activeRound === i + 1 ? true : false,
        };
        setRounds.push(item);
      }
    }
    let selectedForİnitialize = _.find(setRounds, {activeRound: true});
    setStages(setRounds);
    setSelectedStages(selectedForİnitialize?.value);
  };

  const SelectedRound = value => {
    setSelectedStages(value);
    BroadageService.GetSoccerSeaosonFixture(selectedSeason, value)
      .then(res => {
        if (res?.data) {
          setServiceLoading(false);
          setMatchlist(res?.data);
        }
      })
      .catch(err => console.log('GetSoccerSeaosonFixture', err));
  };

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{backgroundColor: theme?.colors?.blueGray, flex: 1}}>
          <FAStandardHeader title={'Fixture'} navigation={navigation} />
          <FAFullScreenLoader isLoading={serviceLoading} />
          <View style={{marginTop: 5}}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal
              style={{
                backgroundColor: theme?.colors?.softGray,
              }}>
              <FAMatchCenterHeaderSelect
                onValueChange={value => SelectedTournaments(value)}
                list={tournaments}
                value={selectedTournament}
              />
              <FAMatchCenterHeaderSelect
                onValueChange={value => SelectedSeaoson(value)}
                list={seasons}
                value={selectedSeason}
              />

              <FAMatchCenterHeaderSelect
                onValueChange={value => SelectedRound(value)}
                list={stages}
                value={selectedStages}
              />
            </ScrollView>
          </View>
          <ScrollView style={{flex: 1, marginBottom: 10}}>
            {matchList !== [] ? <FATeamResults data={matchList} /> : null}
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default FixtureScreen;
