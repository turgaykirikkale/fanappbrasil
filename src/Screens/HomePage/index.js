import React, {Component} from 'react';
import {
  View,
  FlatList,
  ScrollView,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import FADrawerHeader from '@Components/Composite/FADrawerHeader';
import {connect} from 'react-redux';
import FASlider from '@Components/UI/FASlider';
import FAService from '@Services';
import _ from 'lodash';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import FAListHeader from '@Components/Composite/FAListHeader';
import autobind from 'autobind-decorator';
import FAFanCard from '@Components/UI/FAFanCard';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import UploadUserInformationPage from '../UploadUserInformationPage';
import rssParser from 'react-native-rss-parser';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showUserUploadInformModal:
        this.props?.UserState?.UserInfo?.CustomerStatusEnumId < 10
          ? true
          : false,
      sliderList: [],
      NewsData: [],
      Surveys: [],
      Events: [],
      videos: [],
      categoryList: [],
      seasons: [],
      standingList: [],
      isLoading: true,
    };
  }

  componentWillMount() {
    this.setDataToScreen();
    fetch('https://wp.bitcitech.com/west/feed')
      .then(response => response.text())
      .then(responseData => rssParser.parse(responseData))
      .then(rss => {
        const rssItems = rss?.items;
        this.setState({
          isLoading: false,
          NewsData: _.slice(rssItems, 0, 6),
        });
      })
      .catch(err => {
        console.log('err1', err);
        // setserviceErrorMessage(Localization.t('Commons.UnexpectedError'));
        // setErrorModalVisible(true);
        // setIsloading(false);
      });
  }

  setDataToScreen() {
    this.fillVideoList();
    // this.fillSlider();
  }

  @autobind
  fillVideoList() {
    const {onRefresh} = this.state;
    let Videos = [];
    let VideosDetail = [];
    if (onRefresh) {
      this.setState({onRefresh: true});
    }
    FAService.GetMainPageVideos()
      .then(res => {
        if (res?.data) {
          const resData = res.data;
          const categoryList = _.filter(resData, category => {
            return !_.isEmpty(category?.Videos);
          });
          this.setState({categoryList: categoryList});

          if (categoryList) {
            _.each(categoryList, item => {
              Videos.push(item.Videos);
            });
          }
          if (Videos.length > 0) {
            _.each(Videos, item => {
              if (item.length > 0) {
                _.each(item, Video => {
                  VideosDetail.push(Video);
                });
              }
            });
          }

          this.setState({
            videos: VideosDetail,
          });
        }
        if (onRefresh) {
          this.setState({
            onRefresh: false,
          });
        }
      })
      .catch(err => console.log(err));
  }

  photoUrl(item) {
    let re = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i;
    let text = item?.content;
    let imageUrls;
    if (text.match(re)) {
      imageUrls = text.match(re);
    } else {
      imageUrls = null;
    }

    let PhotoUrl = imageUrls[0].toString().split(' ');
    return PhotoUrl[0];
  }

  render() {
    const {navigation, EventState, SurveyState, NewsState} = this.props;
    const {
      sliderList,
      videos,
      NewsData,
      showUserUploadInformModal,
      standingList,
      isLoading,
    } = this.state;
    const screenWidth = Dimensions.get('screen').width;
    const routeParams = this.props?.route?.params;
    let Events = [];
    let Surveys = [];
    let NewsDetail = [];
    let News = [];
    if (!routeParams?.isAnonymous) {
      if (
        !_.isEmpty(EventState?.ActiveEvents?.All) ||
        !_.isEmpty(EventState?.JoinedEvents?.All) ||
        !_.isEmpty(EventState?.PassedEvents?.All)
      ) {
        const activeEvents = EventState?.ActiveEvents?.All;
        const JoinedEvents = EventState?.JoinedEvents?.All;
        const PassedEvents = EventState?.PassedEvents?.All;
        _.each(activeEvents, item => {
          Events.push(item);
        });
        _.each(JoinedEvents, item => {
          Events.push(item);
        });
        _.each(PassedEvents, item => {
          Events.push(item);
        });
      }
      if (
        !_.isEmpty(SurveyState?.ActiveSurveys?.All) ||
        !_.isEmpty(SurveyState?.PassedSurveys?.All) ||
        !_.isEmpty(SurveyState?.VotedSurveys?.All)
      ) {
        const activeSurveys = SurveyState?.ActiveSurveys?.All;
        const passedSurveys = SurveyState?.PassedSurveys?.All;
        const votedSurveys = SurveyState?.VotedSurveys?.All;
        _.each(activeSurveys, item => {
          Surveys.push(item);
        });
        _.each(passedSurveys, item => {
          Surveys.push(item);
        });
        _.each(votedSurveys, item => {
          Surveys.push(item);
        });
      }
    }

    const UserBalance =
      this.props?.UserState?.FinanceInfo?.CustomerCoinBalanceDetailList;

    return (
      <FADrawerHeader
        title={Localization.t('CommonsFix.Home')}
        navigation={navigation}
        routeMain={this.props.route.name}>
        <ThemeContext.Consumer>
          {({theme}) => (
            <>
              {showUserUploadInformModal ? (
                <UploadUserInformationPage
                  closeModal={() =>
                    this.setState({
                      showUserUploadInformModal: false,
                    })
                  }
                  navigateSettingScreen={() =>
                    navigation.navigate('AccountSettings')
                  }
                />
              ) : (
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  style={{backgroundColor: theme?.colors?.blueGray}}>
                  <FAFullScreenLoader isLoading={isLoading} />

                  <TouchableOpacity
                    disabled={false}
                    onPress={() =>
                      navigation.navigate('TradeTabStackNavigator')
                    }
                    style={{marginHorizontal: 10, marginVertical: 10}}>
                    <Image
                      resizeMode="stretch"
                      style={{
                        height: screenWidth / 1.8,
                        borderRadius: 4,
                        width: '100%',
                      }}
                      source={require('./assets/cbfFanApp.jpeg')}
                    />
                  </TouchableOpacity>
                  <View style={{marginHorizontal: 10}}>
                    {!_.isEmpty(NewsData) && (
                      <>
                        <View
                          style={{
                            backgroundColor: theme?.colors?.white,
                            paddingVertical: 10,
                            borderRadius: 5,
                            paddingHorizontal: 10,
                          }}>
                          <FAListHeader
                            theme={theme}
                            resetPadding
                            iconInLocal
                            title={Localization.t('CommonsFix.News')}
                            iconName={'FAPages'}
                            fontColor={generateRandomColor()}
                            onPress={() =>
                              navigation.navigate('NewsTabStackNavigator', {})
                            }
                          />
                        </View>
                        <View style={{marginTop: 10}}>
                          <FlatList
                            ItemSeparatorComponent={() => (
                              <View style={{marginLeft: 10}} />
                            )}
                            keyExtractor={(item, index) => index}
                            showsHorizontalScrollIndicator={false}
                            data={NewsData}
                            renderItem={({index, item}) => (
                              <FAFanCard
                                theme={theme}
                                text={item.title}
                                image={this.photoUrl(item)}
                                branchName={item.MainBranch || null}
                                onPress={() =>
                                  navigation.navigate('NewsDetailScreen', {
                                    news: item,
                                  })
                                }
                                cardRatio={3}
                              />
                            )}
                            horizontal
                          />
                        </View>
                      </>
                    )}
                  </View>
                  {!_.isEmpty(videos) && (
                    <View style={{marginHorizontal: 10}}>
                      <View
                        style={{
                          backgroundColor: theme?.colors?.white,
                          paddingVertical: 10,
                          borderRadius: 5,
                          paddingHorizontal: 10,
                          marginTop: 10,
                        }}>
                        <FAListHeader
                          theme={theme}
                          resetPadding
                          iconInLocal
                          title={Localization.t(
                            'CommonsFix.RecentlyAddedVideos',
                          )}
                          iconName={'FAPages'}
                          fontColor={generateRandomColor()}
                          onPress={() =>
                            navigation.navigate(
                              'VideoGalleryTabStackNavigator',
                              {
                                screen: 'FanAppTvScreen',
                              },
                            )
                          }
                        />
                      </View>
                      <View style={{marginVertical: 10}}>
                        <FlatList
                          ItemSeparatorComponent={() => (
                            <View style={{marginLeft: 10}} />
                          )}
                          keyExtractor={(item, index) => index}
                          showsHorizontalScrollIndicator={false}
                          data={videos}
                          renderItem={({index, item}) => (
                            <FAFanCard
                              onPress={() =>
                                navigation.navigate('VideoScreen', {
                                  video: item,
                                  // category: category,
                                })
                              }
                              image={item.ImageUrl || null}
                              text={item.Title}
                              cardRatio={3}
                            />
                          )}
                          horizontal
                        />
                      </View>
                    </View>
                  )}

                  {!routeParams?.isAnonymous && (
                    <>
                      {!_.isEmpty(Surveys) && (
                        <View
                          style={{
                            backgroundColor: theme?.colors?.white,
                            marginHorizontal: 10,
                            borderRadius: 4,
                            paddingHorizontal: 15,
                            paddingVertical: 13,
                          }}>
                          <View style={{marginBottom: 13}}>
                            <FAListHeader
                              theme={theme}
                              resetPadding
                              iconInLocal
                              title={Localization.t('CommonsFix.Polls')}
                              iconName={'FASurvey'}
                              fontColor={generateRandomColor()}
                              onPress={() =>
                                navigation.navigate('SurveysScreen', {
                                  routeMain: this.props.route.name,
                                })
                              }
                            />
                          </View>
                          <FASlider.Alternative
                            UserWallets={UserBalance}
                            data={Surveys}
                            buttonText={Localization.t('CommonsFix.Reply')}
                            buttonContainerStyle={{
                              backgroundColor: theme?.colors?.faOrange,
                              borderRadius: 5,
                            }}
                            buttonTextStyle={{
                              marginVertical: 8,
                              marginHorizontal: 16,
                              fontSize: 12,
                            }}
                            textStyle={{color: theme?.colors?.black}}
                            onPress={item =>
                              navigation.navigate('SurveyScreenDetail', {
                                UserBalance,
                                event: item,
                                isAnonymousFlow: routeParams?.isAnonymous,
                                routeMain: this.props.route.name,
                              })
                            }
                          />
                        </View>
                      )}
                      {!_.isEmpty(Events) && (
                        <View
                          style={{
                            backgroundColor: theme?.colors?.white,
                            marginHorizontal: 10,
                            borderRadius: 4,
                            paddingHorizontal: 15,
                            paddingVertical: 13,
                            marginTop: 10,
                          }}>
                          <View style={{marginBottom: 13}}>
                            <FAListHeader
                              theme={theme}
                              fontColor={generateRandomColor()}
                              resetPadding
                              title={Localization.t('CommonsFix.Events')}
                              iconName={'star'}
                              onPress={() =>
                                navigation.navigate('EventsScreen', {
                                  routeMain: this.props.route.name,
                                })
                              }
                            />
                          </View>
                          <FASlider.Alternative
                            data={Events}
                            buttonText={Localization.t('CommonsFix.Join')}
                            buttonContainerStyle={{
                              backgroundColor: theme?.colors?.faOrange,
                              borderRadius: 5,
                            }}
                            buttonTextStyle={{
                              marginVertical: 8,
                              marginHorizontal: 16,
                              fontSize: 12,
                            }}
                            textStyle={{color: theme?.colors?.black}}
                            onPress={inComingItem =>
                              navigation.navigate('EventDetailScreen', {
                                UserBalance,
                                event: inComingItem,
                                routeMain: this.props.route.name,
                              })
                            }
                          />
                        </View>
                      )}
                    </>
                  )}

                  <View style={{marginBottom: 10}} />
                </ScrollView>
              )}
            </>
          )}
        </ThemeContext.Consumer>
      </FADrawerHeader>
    );
  }
}

const mapStateToProps = state => {
  return {
    NewsState: state.NewsState,
    SurveyState: state.SurveyState,
    EventState: state.EventState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(HomeScreen);
