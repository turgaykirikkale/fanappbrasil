import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAService from '../../Services';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FARenderVolumeCompetitionsListItems from '../../Components/UI/FAVolumeCompetitionListItems';
import FAVolumeCompetitionItemAwardRank from '../../Components/UI/FAVolumeCompetitionItemAwardRank';
import {FlatList} from 'react-native-gesture-handler';
import _ from 'lodash';
import moment from 'moment';
import FASorting from '../../Components/Composite/FASorting';
import FARenderUsersCompetitionRanks from '../../Components/UI/FARenderUsersCompetitionRanks';
import Localization from '@Localization';
import {setSelectedCoinAndCurrencyCode} from '@GlobalStore/Actions/MarketActions';
import {connect} from 'react-redux';
import {BUYFLOW} from '@Commons/FAEnums';

const VolumeCompetetitionDetailScreen = props => {
  const {navigation} = props;
  const item = props?.route?.params?.item;
  const [VolumeCompetitionItemDetail, setVolumeCompetitionItemDetail] =
    useState([]);
  const [LuckuyNumbers, setLuckyNumbers] = useState([]);
  const [competitionRanks, setCompetitionsRanks] = useState([]);
  const [FilterLogic, setFilterLogic] = useState({
    volume: undefined,
    rank: undefined,
    email: undefined,
  });
  const [Loader, setLoader] = useState(false);

  useEffect(() => {
    getCompetitionsItemDetail();
    getUserCompetitionsRanks();
  }, []);

  const getUserCompetitionsRanks = () => {
    setLoader(true);
    FAService.getUserCompetitionRanks(item.Id)
      .then(res => {
        if (
          res?.data &&
          res?.data?.Data &&
          res?.data?.Data.Data &&
          res.status === 200
        ) {
          setCompetitionsRanks(res?.data?.Data?.Data);
          setLoader(false);
        }
      })
      .catch(err => console.log('Err SAS=> ', err));
  };
  const getCompetitionsItemDetail = () => {
    const requestBody = {};
    requestBody.eventId = item.Id;
    FAService.getVolumeCompetitionItemDetail(requestBody)
      .then(res => {
        if (res?.data && res?.data?.Details && res.status === 200) {
          setVolumeCompetitionItemDetail(res.data.Details);
          let LuckuyNumberArr = _.filter(res.data.Details, {
            IsSpecial: true,
          });
          setLuckyNumbers(LuckuyNumberArr);
        }
      })
      .catch(err => console.log('Err => ', err));
  };

  const filterAction = () => {
    if (competitionRanks && competitionRanks.length > 0) {
      let filteredList = competitionRanks;

      const {rank, email, volume} = FilterLogic;

      if (rank !== undefined) {
        if (rank) {
          filteredList = _.sortBy(filteredList, 'Rank').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'Rank');
        }
      } else if (email !== undefined) {
        if (email) {
          filteredList = _.sortBy(filteredList, 'Mail').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'Mail');
        }
      } else if (volume !== undefined) {
        if (volume) {
          filteredList = _.sortBy(filteredList, 'Volume').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'Volume');
        }
      }

      return setCompetitionsRanks(filteredList);
    }
    return setCompetitionsRanks(competitionRanks);
  };

  const controlNavitagationToMarket = inComigItem => {
    const {navigation, setSelectedCoinAndCurrencyCode} = props;
    let params = {
      SelectedCoinCode: inComigItem.CoinCode,
      SelectedCurrencyCode: inComigItem.CurrencyCode,
      SelectedPair: inComigItem,
      MarketQueueName: `${inComigItem.CoinCode}${inComigItem.CurrencyCode}`,
    };
    console.log('paramsasdasdasda', params);
    setSelectedCoinAndCurrencyCode(params);

    navigation.navigate('TradeTabStackNavigator', {
      params: {
        ...params,
        flow: BUYFLOW,
      },
    });
  };

  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              // type={2}
              title={Localization.t('VolumeCompetitions.header')}
              navigation={navigation}
            />
            <ScrollView style={{backgroundColor: theme?.colors?.blueGray}}>
              <View style={{flexDirection: 'row'}}>
                <FARenderVolumeCompetitionsListItems
                  item={item}
                  onPress={inComigItem =>
                    controlNavitagationToMarket(inComigItem)
                  }
                />

                <View
                  style={{
                    flex: 1,
                    marginVertical: 10,
                    borderRadius: 4,
                    paddingVertical: 10,
                    marginHorizontal: 10,
                    paddingHorizontal: 8,
                    backgroundColor: theme?.colors?.white,
                  }}>
                  <FAVolumeCompetitionItemAwardRank
                    data={VolumeCompetitionItemDetail}
                    Code={item.CoinCode || item.CurrencyCode}
                  />
                </View>
              </View>

              <View
                style={{
                  backgroundColor: theme?.colors?.white,
                  flexDirection: 'row',
                  paddingHorizontal: 10,
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={{
                      color: theme?.colors?.black,
                      marginBottom: 3,
                      fontWeight: 'bold',
                    }}>
                    {Localization.t('VolumeCompetitions.LuckyNumbers')}
                  </Text>
                  <FlatList
                    horizontal
                    style={{width: 150}}
                    data={LuckuyNumbers}
                    renderItem={({item, index}) => {
                      return (
                        <View
                          style={{
                            borderRadius: 4,
                            width: 25,
                            justifyContent: 'center',
                            backgroundColor: item.IsLucky
                              ? theme?.colors?.green
                              : theme?.colors?.red,
                            alignItems: 'center',
                            marginRight: 5,
                          }}>
                          <Text
                            style={{
                              color: theme?.colors?.black,
                              fontSize: 12,
                            }}>
                            {item.Order}
                          </Text>
                        </View>
                      );
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingLeft: 20,
                  }}>
                  <Text
                    style={{
                      color: theme?.colors?.black,
                      textAlign: 'left',
                      fontWeight: 'bold',
                    }}>
                    {Localization.t('VolumeCompetitions.Dates')}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View>
                      <Text
                        style={{
                          color: theme?.colors?.black,
                          fontWeight: 'bold',
                          fontSize: 12,
                        }}>
                        {Localization.t('VolumeCompetitions.StartDate')}
                      </Text>
                      <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                        {moment(item.StartDate).format('DD.MM.YYYY')}
                      </Text>
                    </View>
                    <View style={{marginLeft: 2}}>
                      <Text
                        style={{
                          color: theme?.colors?.black,
                          fontWeight: 'bold',
                          fontSize: 12,
                        }}>
                        {Localization.t('VolumeCompetitions.EndDate')}
                      </Text>
                      <Text style={{color: theme?.colors?.black, fontSize: 12}}>
                        {moment(item.EndDate).format('DD.MM.YYYY')}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>

              <View
                style={{
                  marginTop: 2,
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: theme?.colors.white,
                  paddingVertical: 3,
                  paddingHorizontal: 10,
                }}>
                <FASorting
                  type={5}
                  theme={theme}
                  onPress={flag =>
                    setFilterLogic(
                      {
                        rank: flag,
                        email: undefined,
                        volume: undefined,
                      },
                      filterAction(),
                    )
                  }
                  value={FilterLogic.rank}
                  containerStyle={{
                    flex: 1,
                    backgroundColor: theme?.colors?.white,
                  }}
                  text={Localization.t('VolumeCompetitions.Rank')}
                />
                <FASorting
                  type={5}
                  theme={theme}
                  onPress={flag =>
                    setFilterLogic(
                      {
                        rank: undefined,
                        email: flag,
                        volume: undefined,
                      },
                      filterAction(),
                    )
                  }
                  value={FilterLogic.email}
                  containerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    backgroundColor: theme?.colors?.white,
                  }}
                  text={Localization.t('VolumeCompetitions.email')}
                />
                <FASorting
                  type={5}
                  theme={theme}
                  onPress={flag =>
                    setFilterLogic(
                      {
                        rank: undefined,
                        email: undefined,
                        volume: flag,
                      },
                      filterAction(),
                    )
                  }
                  value={FilterLogic.volume}
                  containerStyle={{
                    alignItems: 'flex-end',
                    flex: 1,
                    backgroundColor: theme?.colors?.white,
                  }}
                  text={Localization.t('VolumeCompetitions.Volume')}
                />
              </View>
              <View>
                {Loader ? (
                  <ActivityIndicator
                    size={'large'}
                    color={theme?.colors?.orange}
                    style={{marginTop: 30}}
                  />
                ) : (
                  <FARenderUsersCompetitionRanks data={competitionRanks} />
                )}
              </View>
            </ScrollView>
          </>
        )}
      </ThemeContext.Consumer>
    </>
  );
};
const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {
  setSelectedCoinAndCurrencyCode,
})(VolumeCompetetitionDetailScreen);
