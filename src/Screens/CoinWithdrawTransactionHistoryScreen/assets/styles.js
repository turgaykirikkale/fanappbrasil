import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},

  renderItemStyle: {
    borderBottomWidth: 2,
    paddingTop: 10,
  },
  sortingContainer: {flexDirection: 'row', alignItems: 'center'},
  dateSortingContainer: {
    flex: 1,
    paddingLeft: 10,
    backgroundColor: FAColor.VGray,
    paddingVertical: 10,
  },
  AmountContainerStyle: {
    flex: 1,
    backgroundColor: FAColor.VGray,
    paddingVertical: 10,
    alignItems: 'center',
  },
  StateContainerStyle: {
    flex: 1,
    paddingRight: 10,
    backgroundColor: FAColor.VGray,
    paddingVertical: 10,
    alignItems: 'flex-end',
  },
  thereIsNoOrderContainerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  thereIsNoOrderTextStyle: {color: FAColor.Gray},
});
