import React, {Component} from 'react';
import {FlatList, View, Text} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import FAService from '@Services';
import FACoinTransactionHistoryItem from '@Components/Composite/FACoinTransactionHistoryItem';
import autobind from 'autobind-decorator';
import FASorting from '@Components/Composite/FASorting';
import {styles} from './assets/styles';
import Toast from 'react-native-toast-message';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

export default class CoinWithdrawTransactionHistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTXID: false,
      filterLogic: {
        date: true,
        amount: undefined,
        state: undefined,
      },
      transactionData: [],
      generatedData: [],
      currentPage: 1,
      isLoading: false,
      showDialog: false,
      serviceMessage: null,
      modalControl: false,
    };
  }

  generateFormattedDate(nonFormattedDate) {
    const splittedDate = _.split(nonFormattedDate, '-');
    if (splittedDate.length === 3) {
      return `${splittedDate[2]}-${splittedDate[1]}-${splittedDate[0]}`;
    }
  }

  componentDidMount() {
    this.fetchAllList();
  }

  @autobind
  onEndReached() {
    this.fetchAllList();
  }

  @autobind
  fetchAllList() {
    const {currentPage} = this.state;
    const data =
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.CoinData;
    const type =
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.type;
    const generatedStartDate = this.generateFormattedDate(
      moment().subtract(5, 'years').format('DD-MM-YYYY'),
    );
    const generatedEndDate = this.generateFormattedDate(
      moment().format('DD-MM-YYYY'),
    );
    if (type === 2) {
      const requestBody = {
        StartDate: generatedStartDate,
        Filters: [{FilterType: 11, FilterValue: data.AssetId.toString()}],
        IsActive: false,
        PageNumber: currentPage,
        Size: 25,
        EndDate: generatedEndDate,
      };
      this.setState({
        isLoading: true,
      });
      FAService.GetCoinWithdrawList(requestBody)
        .then(response => {
          if (response && response.data && response.status === 200) {
            const concatData = _.concat(
              this.state.transactionData,
              response.data.Data.CustomerCoinWithdrawalDto,
            );
            this.setState({
              transactionData: concatData,
              generatedData: concatData,
              isLoading: false,
              currentPage: currentPage + 1,
            });
          } else {
            this.setState({
              isLoading: false,
              showDialog: true,
              serviceMessage: response.data.Message,
            });
          }
        })

        .catch(error => {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
          });
        });
    } else {
      const requestBodyForActive = {
        StartDate: generatedStartDate,
        Filters: [{FilterType: 11, FilterValue: data.AssetId}],
        IsActive: true,
        PageNumber: currentPage,
        Size: 25,
        EndDate: generatedEndDate,
      };
      FAService.GetCoinWithdrawList(requestBodyForActive)
        .then(response => {
          if (response && response.data && response.status === 200) {
            const concatData = _.concat(
              this.state.transactionData,
              response.data.Data.CustomerCoinWithdrawalDto,
            );

            this.setState({
              transactionData: concatData,
              generatedData: concatData,
              ID: response.data.Data.CustomerCoinWithdrawalDto.Id,
              currentPage: currentPage + 1,
            });
          } else {
            this.setState({
              serviceMessage: response.data.Message,
            });
          }
        })

        .catch(error => {});
    }
  }

  @autobind
  cancelServisCall(id) {
    const requestBody = {
      Id: id.toString(),
    };
    FAService.deleteActiveCoinWithdraw(requestBody)
      .then(response => {
        if (response && response.data && response.status === 200) {
          this.setState({
            serviceMessage: response.data.Message,
          });
          Toast.show({
            type: 'success',
            position: 'top',
            text1: Localization.t('CommonsFix.Succces'),
            text2: response.data.Message,
            visibilityTime: 750,
            autoHide: true,
            topOffset: 100,
            bottomOffset: 40,
            onShow: () => {},
            onHide: () => {},
            onPress: () => {},
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })

      .catch(error => {});
    this.cancelActiveOrdersItems(id);
  }

  cancelActiveOrdersItems(id) {
    const {generatedData} = this.state;
    let newValues = generatedData;
    const index = generatedData.indexOf(_.find(generatedData, {Id: id}));
    if (index > -1) {
      newValues.splice(index, 1);
      this.setState(
        {
          generatedData: newValues,
        },
        () => this.fetchAllList(),
      );
    }
  }

  renderItem(item, theme) {
    const type =
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.type;
    return (
      <View
        style={[styles.renderItemStyle, {borderColor: theme?.colors?.vGray}]}>
        <FACoinTransactionHistoryItem
          theme={theme}
          item={item}
          onPressCancel={id => this.cancelServisCall(id)}
          type={type}
        />
      </View>
    );
  }

  filterAction() {
    const {transactionData} = this.state;
    if (transactionData && transactionData.length > 0) {
      const {filterLogic} = this.state;
      const {date, amount, state} = filterLogic;
      let filteredCoinList = transactionData;
      if (date === undefined && amount === undefined && state === undefined) {
        filteredCoinList = _.sortBy(filteredCoinList, 'CreateDate');
      }

      if (date !== undefined) {
        if (date) {
          filteredCoinList = _.sortBy(filteredCoinList, 'CreateDate');
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'CreateDate').reverse();
        }
      } else if (amount !== undefined) {
        if (amount) {
          filteredCoinList = _.sortBy(filteredCoinList, 'Amount').reverse();
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'Amount');
        }
      } else if (state !== undefined) {
        if (state) {
          filteredCoinList = _.sortBy(filteredCoinList, 'State').reverse();
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'State');
        }
      }

      this.setState({
        generatedData: filteredCoinList,
      });
    }
  }

  render() {
    const data =
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.CoinData;
    const type =
      this.props &&
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.type;
    const {navigation} = this.props;
    const {generatedData, filterLogic, isLoading} = this.state;

    const headerTitle =
      type === 2
        ? Localization.t('CommonsFix.TransactionHistory')
        : Localization.t('WalletFix.ActiveOrders');

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAFullScreenLoader isLoading={isLoading} />
            <FAStandardHeader title={headerTitle} navigation={navigation} />
            <View style={styles.sortingContainer}>
              <FASorting
                theme={theme}
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: flag,
                        amount: undefined,
                        price: undefined,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                value={filterLogic.date}
                containerStyle={[
                  styles.dateSortingContainer,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                text={Localization.t('CommonsFix.Date')}
              />
              <FASorting
                theme={theme}
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: undefined,
                        amount: flag,
                        price: undefined,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                value={filterLogic.amount}
                containerStyle={[
                  styles.AmountContainerStyle,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                text={Localization.t('WalletFix.Amount')}
              />
              <FASorting
                theme={theme}
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: undefined,
                        amount: undefined,
                        state: flag,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                value={filterLogic.state}
                containerStyle={[
                  styles.StateContainerStyle,
                  {backgroundColor: theme?.colors?.vGray},
                ]}
                text={Localization.t('WalletFix.Situation')}
              />
            </View>

            {generatedData.length === 0 ? (
              <View style={styles.thereIsNoOrderContainerStyle}>
                <Text
                  style={[
                    styles.thereIsNoOrderTextStyle,
                    {color: theme?.colors?.black},
                  ]}>
                  {type === 2
                    ? Localization.t('FanScreenFix.ThereisNoHistory')
                    : Localization.t('FanScreenFix.ThereisNoOrder')}
                </Text>
              </View>
            ) : (
              <FlatList
                data={generatedData}
                renderItem={({item}) => this.renderItem(item, theme)}
                keyExtractor={(item, index) => index}
                onEndReached={this.onEndReached}
                onEndReachedThreshold={0.8}
              />
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
