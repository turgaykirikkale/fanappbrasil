import React, {Component} from 'react';
import {ScrollView, View, Text, RefreshControl} from 'react-native';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FACustomTabView from '@Components/Composite/FACustomTabView';
import FAStake from '@Components/UI/FAStake';
import FAService from '@Services';
import autobind from 'autobind-decorator';
import FAActiveAndHistoryStake from '@Components/Composite/FAActiveAndHistoryStake';
import moment from 'moment';
import _ from 'lodash';
import Toast from 'react-native-toast-message';
import FAStopStakeModal from '@Components/UI/FAStopStakeModal';
import FAPickerSelect from '@Components/Composite/FAPickerSelect';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';

class StakingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      interestStakeInfodata: [],
      interestStakedata: [],
      tabIndex: 0,
      minAmount: null,
      LockedListDetail: [],
      interestStakeRate: null,
      FlexInterestRate: null,
      currentPercent: 1,
      ActiveStake: [],
      HistoryStake: [],
      errorMessage: null,
      modalShow: false,
      isScreenRefreshing: false,
      modalItem: [],
      listForActiveStake: [
        {
          label: Localization.t('StakeScreen.All'),
          value: 'all',
        },
        {
          label: Localization.t('StakeScreen.Locked'),
          value: 'locked',
        },
        {
          label: Localization.t('StakeScreen.Flexible'),
          value: 'flexible',
        },
      ],
      listForActiveStakeValue: 'all',
      shortActiveStake: [
        {
          label: Localization.t('StakeScreen.Arrangement'),
          value: 'sorting',
        },
        {
          label: Localization.t('StakeScreen.StakeRate'),
          value: 'stakerate',
        },
        {
          label: Localization.t('StakeScreen.LockedAmount'),
          value: 'lockedAmount',
        },
        {
          label: Localization.t('StakeScreen.CurrentStake'),
          value: 'currentStake',
        },
        {
          label: Localization.t('StakeScreen.StartDate'),
          value: 'startDate',
        },
        {
          label: Localization.t('StakeScreen.EndDate'),
          value: 'endDate',
        },
      ],
      shortForStake: 'sorting',
      routes: [
        {
          key: 'kilitli',
          title: Localization.t('StakeScreen.Locked'),
        },
        {
          key: 'esnek',
          title: Localization.t('StakeScreen.Flexible'),
        },
        {
          key: 'aktif',
          title: Localization.t('StakeScreen.Active'),
        },
        {
          key: 'geçmiş',
          title: Localization.t('StakeScreen.History'),
        },
      ],
    };
  }

  componentWillMount() {
    this.interestStakeInfo();
    this.interestStake();
    this.getStakeDetails();
    this.getHistoryStakeDetails();
  }

  @autobind
  interestStakeInfo() {
    FAService.GetInterestStakeInfo()
      .then(response => {
        if (response?.response.data) {
          this.setState({
            interestStakeInfodata: response.data,
          });
        } else {
          this.setState({
            errorMessage: Localization.t('Commons.UnexpectedError'),
          });
        }
      })
      .catch(error => {
        this.setState({
          errorMessage: Localization.t('Commons.UnexpectedError'),
        });
      });
  }

  @autobind
  interestStake() {
    FAService.GetInterestStake()
      .then(response => {
        if (response && response.data) {
          const data = response.data[0];
          this.setState({
            interestStakedata: data,
            minAmount: data.MinAmount,
            interestStakeRate: data.LockedListDetail[0].InterestRate || null,
            LockedListDetail: data.LockedListDetail,
            FlexInterestRate: data.FlexInterestRate,
            interestStakeDay: data.LockedListDetail[0].Day || null,
          });
        } else {
          this.setState({
            errorMessage: Localization.t('Commons.UnexpectedError'),
          });
        }
      })
      .catch(error => {
        this.setState({
          errorMessage: Localization.t('Commons.UnexpectedError'),
        });
      });
  }

  @autobind
  getStakeDetails() {
    const startDate = moment();
    const finishDate = moment().add(200);
    const requestBody = {
      PageNumber: 1,
      Size: 100000,
      StartDate: startDate,
      EndDate: finishDate,
      Sort: {
        SortType: 'Id',
        SortValue: 'desc',
      },
      Filter: '{}',
    };

    FAService.GetStakeDetails(requestBody)
      .then(response => {
        if (response?.data?.Data) {
          const data = response?.data?.Data?.Data;

          this.setState({
            ActiveStake: data,
          });
        } else {
        }
      })
      .catch(error => {});
  }

  @autobind
  getHistoryStakeDetails() {
    const finishDate = moment().format('YYYY-MM-DD');
    const startDate = moment().subtract(1, 'years').format('YYYY-MM-DD');
    const requestBody = {
      PageNumber: 1,
      Size: 25,
      StartDate: startDate,
      EndDate: finishDate,
      Sort: {
        SortType: 'Id',
        SortValue: 'desc',
      },
      Filter: '{"InterestStakeStatusEnumId":3}',
    };
    FAService.GetStakeDetails(requestBody)
      .then(response => {
        if (response?.data?.Data) {
          const data = response?.data?.Data?.Data;
          this.setState({
            HistoryStake: data,
          });
        } else {
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChangeTabIndex(value) {
    this.setState({
      tabIndex: value,
    });
  }

  handlePercentExecutor(percentValue, LockedListDetail) {
    this.setState({
      currentPercent: percentValue,
      interestStakeRate: LockedListDetail.InterestRate,
      interestStakeDay: LockedListDetail.Day,
    });
  }

  onPressShowStakeModal(item) {
    this.setState({
      modalShow: true,
      modalItem: item,
    });
  }
  @autobind
  onRefresh() {
    const {MarketState} = this.props;
    this.setState({isScreenRefreshing: true});
    this.interestStakeInfo();
    this.interestStake();
    this.getStakeDetails();
    this.getHistoryStakeDetails();
    this.setState({isScreenRefreshing: false});
  }

  onPressStakeQuite(item) {
    const requestBody = {
      InterestStakeTransactionId: item.InterestStakeTransactionId,
    };
    FAService.CompleteFlexibleInterestStakeTransaction(requestBody)
      .then(response => {
        if (response?.response.data?.IsSuccess) {
          this.setState(
            {
              modalShow: false,
            },
            () => {
              setTimeout(() => {
                Toast.show({
                  type: 'success',
                  position: 'top',
                  text1: Localization.t('Commons.Succes'),
                  text2: Localization.t('Commons.SuccessfulTransaction'),
                  visibilityTime: 750,
                  autoHide: true,
                  topOffset: 100,
                  bottomOffset: 40,
                  onShow: () => {},
                  onHide: () => {
                    this.getStakeDetails();
                  },
                  onPress: () => {},
                });
              }, 1000);
            },
          );
        } else {
          this.setState({
            modalShow: false,
            errorMessage: Localization.t('Commons.UnexpectedError'),
          });
        }
      })
      .catch(error => {
        this.setState({
          modalShow: false,
          errorMessage: Localization.t('Commons.UnexpectedError'),
        });
      });
  }

  render() {
    const {navigation} = this.props;
    const {
      tabIndex,
      routes,
      currentPercent,
      interestStakedata,
      interestStakeInfodata,
      interestStakeRate,
      minAmount,
      LockedListDetail,
      FlexInterestRate,
      interestStakeDay,
      ActiveStake,
      modalShow,
      modalItem,
      listForActiveStake,
      shortActiveStake,
      listForActiveStakeValue,
      shortForStake,
      HistoryStake,
      isScreenRefreshing,
    } = this.state;
    let activeStake = [];
    let historyStake = [];
    let sortingActiveStake = [];
    let sortingHistoryStake = [];
    if (listForActiveStakeValue === 'all') {
      if (tabIndex === 2) {
        activeStake = ActiveStake;
      } else if (tabIndex === 3) {
        historyStake = HistoryStake;
      }
    }
    if (listForActiveStakeValue === 'locked') {
      if (tabIndex === 2) {
        activeStake = _.filter(ActiveStake, {InterestStakeType: 'Kilitli'});
      } else if (tabIndex === 3) {
        historyStake = _.filter(HistoryStake, {
          InterestStakeType: 'Kilitli',
        });
      }
    }
    if (listForActiveStakeValue === 'flexible') {
      if (tabIndex === 2) {
        activeStake = _.filter(ActiveStake, {
          InterestStakeType: 'Esnek',
        });
      } else if (tabIndex === 3) {
        historyStake = _.filter(HistoryStake, {
          InterestStakeType: 'Esnek',
        });
      }
    }
    if (shortForStake === 'sorting') {
      if (tabIndex === 2) {
        sortingActiveStake = activeStake;
      } else if (tabIndex === 3) {
        sortingHistoryStake = historyStake;
      }
    }
    if (shortForStake === 'stakerate') {
      if (tabIndex === 2) {
        sortingActiveStake = _.sortBy(activeStake, 'InterestRate').reverse();
      } else if (tabIndex === 3) {
        sortingHistoryStake = _.sortBy(historyStake, 'InterestRate').reverse();
      }
    }
    if (shortForStake === 'lockedAmount') {
      if (tabIndex === 2) {
        sortingActiveStake = _.sortBy(activeStake, 'Amount').reverse();
      } else if (tabIndex === 3) {
        sortingHistoryStake = _.sortBy(historyStake, 'Amount').reverse();
      }
    }
    if (shortForStake === 'currentStake') {
      if (tabIndex === 2) {
        sortingActiveStake = _.sortBy(activeStake, 'Gain').reverse();
      } else if (tabIndex === 3) {
        sortingHistoryStake = _.sortBy(historyStake, 'Gain').reverse();
      }
    }
    if (shortForStake === 'startDate') {
      if (tabIndex === 2) {
        sortingActiveStake = _.sortBy(activeStake, 'StartDate').reverse();
      } else if (tabIndex === 3) {
        sortingHistoryStake = _.sortBy(historyStake, 'StartDate').reverse();
      }
    }
    if (shortForStake === 'endDate') {
      if (tabIndex === 2) {
        sortingActiveStake = _.sortBy(activeStake, 'EndDate').reverse();
      } else if (tabIndex === 3) {
        sortingHistoryStake = _.sortBy(historyStake, 'EndDate').reverse();
      }
    }

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1}}>
            <FAStopStakeModal
              visible={modalShow}
              item={modalItem}
              onPressStakeQuite={item => this.onPressStakeQuite(item)}
              onPressCancel={() => this.setState({modalShow: false})}
            />
            <FAStandardHeader title={'Staking'} navigation={navigation} />
            <View>
              <FACustomTabView
                currentIndex={tabIndex}
                routes={routes}
                onIndexChange={indexValue => this.onChangeTabIndex(indexValue)}
                itemCountPerPage={4}
              />
            </View>

            {tabIndex === 0 ? (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    tintColor={'orange'}
                    refreshing={isScreenRefreshing}
                    onRefresh={() => this.onRefresh()}
                  />
                }
                style={{flex: 1, backgroundColor: theme?.colors?.white}}>
                <FAStake
                  LockedListDetail={LockedListDetail}
                  minAmount={minAmount}
                  interestStakeRate={interestStakeRate}
                  type={0}
                  onSelectPercent={(percentValue, LockedListDetail) =>
                    this.handlePercentExecutor(percentValue, LockedListDetail)
                  }
                  currentPercent={currentPercent}
                  onPress={() =>
                    navigation.navigate('StakeScreenDetail', {
                      type: 0,
                      interestStakeData: interestStakedata,
                      interestStakeRate: interestStakeRate,
                      currentPercent: currentPercent,
                      interestStakeDay: interestStakeDay,
                      InterestStakeTypeEnumId: 1,
                    })
                  }
                />
              </ScrollView>
            ) : null}
            {tabIndex === 1 ? (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    tintColor={'orange'}
                    refreshing={isScreenRefreshing}
                    onRefresh={() => this.onRefresh()}
                  />
                }
                style={{flex: 1, backgroundColor: theme?.colors?.white}}>
                <FAStake
                  interestStakeRate={FlexInterestRate}
                  minAmount={minAmount}
                  onPress={() =>
                    navigation.navigate('StakeScreenDetail', {
                      type: 1,
                      interestStakeData: interestStakedata,
                      interestStakeRate: FlexInterestRate,
                      InterestStakeTypeEnumId: 2,
                    })
                  }
                />
              </ScrollView>
            ) : null}
            {tabIndex === 2 ? (
              <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
                <View
                  style={{
                    marginHorizontal: 10,
                    flexDirection: 'row',
                    marginVertical: 6,
                  }}>
                  <View style={{flex: 1, marginRight: 5}}>
                    <FAPickerSelect
                      theme={theme}
                      onValueChange={value =>
                        this.setState({
                          listForActiveStakeValue: value,
                        })
                      }
                      list={listForActiveStake}
                      value={listForActiveStakeValue}
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <FAPickerSelect
                      theme={theme}
                      list={shortActiveStake}
                      onValueChange={value =>
                        this.setState({
                          shortForStake: value,
                        })
                      }
                      value={shortForStake}
                    />
                  </View>
                </View>
                <FAActiveAndHistoryStake
                  isScreenRefreshing={isScreenRefreshing}
                  onRefreshControl={() => this.onRefresh()}
                  data={sortingActiveStake}
                  onPressStakeQuite={item => this.onPressShowStakeModal(item)}
                />
              </View>
            ) : null}
            {tabIndex === 3 ? (
              <View
                style={{
                  flex: 1,
                  backgroundColor: theme?.colors?.white,
                }}>
                <View
                  style={{
                    marginHorizontal: 10,
                    flexDirection: 'row',
                    marginVertical: 6,
                  }}>
                  <View style={{flex: 1, marginRight: 5}}>
                    <FAPickerSelect
                      theme={theme}
                      onValueChange={value =>
                        this.setState({
                          listForActiveStakeValue: value,
                        })
                      }
                      list={listForActiveStake}
                      value={listForActiveStakeValue}
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 5}}>
                    <FAPickerSelect
                      theme={theme}
                      list={shortActiveStake}
                      onValueChange={value =>
                        this.setState({
                          shortForStake: value,
                        })
                      }
                      value={shortForStake}
                    />
                  </View>
                </View>
                <FAActiveAndHistoryStake
                  isScreenRefreshing={isScreenRefreshing}
                  onRefreshControl={() => this.onRefresh()}
                  data={sortingHistoryStake}
                  onPressStakeQuite={item => this.onPressShowStakeModal(item)}
                />
              </View>
            ) : null}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default StakingScreen;
