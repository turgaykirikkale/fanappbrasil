import {setSelectedCoinAndCurrencyCode} from '@GlobalStore/Actions/MarketActions';
import {getMarketInformationAction} from '@GlobalStore/Actions/MarketActions';
import FACustomTabView from '@Components/Composite/FACustomTabView';
import FADrawerHeader from '@Components/Composite/FADrawerHeader';
import {ActivityIndicator, FlatList, View} from 'react-native';
import FAMarketItem from '@Components/Composite/FAMarketItem';
import FASearchBar from '@Components/Composite/FASearchBar';
import FASorting from '@Components/Composite/FASorting';
import {Orange} from '@Commons/FAColor';
import {AppConstants} from '@Commons/Contants';
import autobind from 'autobind-decorator';
import {BUYFLOW} from '@Commons/FAEnums';
import {connect} from 'react-redux';
import FAService from '@Services';
import React from 'react';
import _ from 'lodash';
import {
  subscribeCoinMarketPriceTicker,
  unSubscribeCoinMarketPriceTicker,
} from '@Stream';
import * as Analytics from 'appcenter-analytics';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';
import {MockTokenList} from '../../Commons/MockDatas/TokenList';

class MarketScreen extends React.Component {
  constructor(props) {
    super(props);
    const {route} = props;
    this.state = {
      from: route?.params?.params?.from || 'init',
      searchValue: null,
      coinList: [],

      favoriteCoinList: [],
      marketMatch: [],
      CurrenctList: [],
      selectedCurrency: AppConstants.CurrencyId,
      marketListLoading: false,
      coinListLoading: false,
      filterLogic: {
        name: true,
        lastPrice: undefined,
        volume: undefined,
        dailyChange: undefined,
      },
      showOnlyFavoriteCoinList: false,
      routes: [
        {key: 'bitci', title: 'BITCI'},
        {key: 'brl', title: 'BRL'},
        {key: 'try', title: 'CHFT'},
        {key: 'usdt', title: 'USDT'},
        {key: 'favorites', title: Localization.t('StakeScreen.Favorites')},
      ],
      tabIndex: 0,
    };
  }

  componentWillMount() {
    const {tabIndex, routes} = this.state;
    const {navigation, MarketState, route} = this.props;

    let incomingSelectedCurrencyIdFromStore = null;
    if (MarketState && MarketState.SelectedPair) {
      const {SelectedPair} = MarketState;
      incomingSelectedCurrencyIdFromStore =
        (SelectedPair && SelectedPair.CurrencyId) || null;
    }
    this._unsubscribe = navigation.addListener('focus', () => {
      const isAnonymousFlow = route?.params?.isAnonymous || false;
      let anonymousTabRoutes = routes;

      if (isAnonymousFlow) {
        _.remove(anonymousTabRoutes, {key: 'favorites'});
        const allCoinListTabIndex = _.findIndex(anonymousTabRoutes, {
          key: 'all',
        });
        if (allCoinListTabIndex > -1) {
          this.setState({
            routes: anonymousTabRoutes,
            index: allCoinListTabIndex,
          });
        }
      }

      if (tabIndex === 0) {
        this.getCoinRatesServiceCall(incomingSelectedCurrencyIdFromStore);
      } else if (tabIndex === 1) {
        this.getCoinRatesServiceCall(incomingSelectedCurrencyIdFromStore);
      } else if (tabIndex === 2) {
        this.getCoinRatesServiceCall(incomingSelectedCurrencyIdFromStore);
      } else if (tabIndex === 3) {
        this.getCoinRatesServiceCall(incomingSelectedCurrencyIdFromStore);
      } else {
        this.getFavoriteCoinList();
      }
    });
  }
  componentWillReceiveProps(nextProps) {}

  componentWillUnmount() {
    this._unsubscribe();
    unSubscribeCoinMarketPriceTicker();
  }

  getFavoriteCoinList() {
    const {route} = this.props;
    if (route?.params?.isAnonymous) {
      return null;
    }
    const classRef = this;
    this.setState({coinListLoading: true});
    FAService.GetFavoriteCoinList()
      .then(res => {
        if (res?.data) {
          const coinListOfRelevantCurrency = _.sortBy(
            _.filter(res.data, {
              CurrencyCode: 'BITCI',
            }),
            'CoinName',
          );
          this.setState(
            {
              favoriteCoinList: coinListOfRelevantCurrency,
            },
            () => subscribeCoinMarketPriceTicker(classRef.marketListHandler),
          );
        }
        this.setState({coinListLoading: false});
      })
      .catch(err => {
        console.log(err);
        this.setState({coinListLoading: false});
      });
  }

  getCoinRatesServiceCall() {
    const classRef = this;
    this.setState(
      {
        marketListLoading: true,
      },
      () => {
        FAService.GetMarketList()
          .then(res => {
            if (
              res &&
              res.status === 200 &&
              res.data &&
              res.data.CurrencyList
            ) {
              const CurrenctList = _.filter(res.data.CurrencyList, {
                IsMarketOpen: true,
              });
              const marketMatch = res.data.MarketMatch;

              this.setCoinList();
              this.setState(
                {
                  marketMatch,
                  CurrenctList: CurrenctList,
                },
                () => {
                  subscribeCoinMarketPriceTicker(classRef.marketListHandler);
                },
              );
            }
            this.setState({
              marketListLoading: false,
            });
          })
          .catch(err => {
            this.setState({
              marketListLoading: false,
            });
            console.log(err);
          });
      },
    );
  }

  setCoinList() {
    const {selectedCurrency: currencyId} = this.state;
    this.setState(
      {
        coinListLoading: true,
      },
      () => {
        FAService.GetCoinRates(currencyId)
          .then(response => {
            if (response && response.status === 200 && response.data) {
              const coinList = _.sortBy(
                _.filter(response.data, {
                  CurrencyId: currencyId,
                }),
                ['CoinName'],
              );

              // let filteredCoinList = [];

              // _.map(coinList, item => {
              //   if (MockTokenList.includes(item.CoinCode))
              //     filteredCoinList.push(item);
              // });

              this.setState({
                coinList: coinList,
              });
            }
            this.setState({
              coinListLoading: false,
            });
          })
          .catch(error => {
            this.setState({
              coinListLoading: false,
            });
          });
      },
    );
  }

  @autobind
  marketListHandler(data) {
    const classRef = this;

    if (data) {
      const {coinList, favoriteCoinList} = classRef.state;
      const newAllCoinList = classRef.getMatchedCoinIndexAndReturnNewCoinList(
        coinList,
        data,
      );

      const newFavoriteCoinList =
        classRef.getMatchedCoinIndexAndReturnNewCoinList(
          favoriteCoinList,
          data,
        );

      classRef.setState({
        coinList: newAllCoinList,
        favoriteCoinList: newFavoriteCoinList,
      });
    }
  }

  @autobind
  getMatchedCoinIndexAndReturnNewCoinList(array, data) {
    let newCoinList = array;
    const matchedCoinIndex = _.findIndex(array, {
      CurrencyId: data.CurrencyId,
      CoinId: data.CoinId,
    });

    if (matchedCoinIndex > -1) {
      newCoinList[matchedCoinIndex] = data;
    }

    return newCoinList;
  }

  onChangeSearchBarText(newValue) {
    this.setState({
      searchValue: newValue,
    });
  }

  onChangeTabIndex(tabIndex) {
    this.setState({tabIndex});
    if (tabIndex === 0) {
      // this.getCoinRatesServiceCall(2007);
      // unSubscribeCoinMarketPriceTicker();
      this.setState({
        selectedCurrency: 2007,
      });
      this.getCoinRatesServiceCall();
      unSubscribeCoinMarketPriceTicker();
    }
    if (tabIndex === 1) {
      this.setState({
        selectedCurrency: 6,
      });
      this.getCoinRatesServiceCall();
      unSubscribeCoinMarketPriceTicker();
    }
    if (tabIndex === 2) {
      this.setState({
        selectedCurrency: 2,
      });
      this.getCoinRatesServiceCall();
      unSubscribeCoinMarketPriceTicker();
    }
    if (tabIndex === 3) {
      this.setState({
        selectedCurrency: 2008,
      });
      this.getCoinRatesServiceCall();
      unSubscribeCoinMarketPriceTicker();
    }
    if (tabIndex === 4) {
      this.getFavoriteCoinList();
      unSubscribeCoinMarketPriceTicker();
    }
  }

  @autobind
  onSelectMarketItemHandler(incomingData) {
    const {from} = this.state;
    const {
      navigation,
      setSelectedCoinAndCurrencyCode,
      getMarketInformationAction,
    } = this.props;
    let params = {
      SelectedCoinCode: incomingData.CoinCode,
      SelectedCurrencyCode: incomingData.CurrencyCode,
      SelectedPair: incomingData,
    };
    setSelectedCoinAndCurrencyCode(params);
    getMarketInformationAction({
      CurrencyId: incomingData.CurrencyId,
      CoinId: incomingData.CoinId,
    });
    Analytics.trackEvent('Market Display', {
      CoinCode: incomingData?.CoinCode || null,
    });
    if (from && from === 'stock') {
      navigation.navigate('TradeTabStackNavigator', {
        params: {
          ...params,
          flow: BUYFLOW,
        },
      });
    } else {
      navigation.navigate('MarketDetailScreen', params);
    }
  }

  renderCoinList(coin, index) {
    const {marketMatch, selectedCurrency} = this.state;
    let currencyDecimalCount = 6;
    let coinDecimalCount = 6;
    let generatedCoin = coin;
    if (!_.isNil(marketMatch)) {
      //TODO(Ali Berk Yurtoğlu) Favorilerde selectedCurrencyId olmadığından
      // dolayı marketMatch içerisinden coin bilgisi alınamıyor. Düzeltilecek
      const coinDecimalLimitResponse = _.find(marketMatch[selectedCurrency], {
        CoinId: coin.CoinId,
      });
      if (!_.isNil(coinDecimalLimitResponse)) {
        const {PriceDecimal, CoinDecimal} = coinDecimalLimitResponse;
        generatedCoin = {
          ...coin,
          PriceDecimal,
          CoinDecimal,
        };
        currencyDecimalCount = PriceDecimal;
        coinDecimalCount = CoinDecimal;
      }
    }

    return (
      <View key={index} style={{}}>
        <FAMarketItem
          currencyDecimalCount={currencyDecimalCount}
          coinDecimalCount={coinDecimalCount}
          imageName={generatedCoin.CoinCode || ''}
          status={true}
          coinName={generatedCoin.CoinCode}
          coinLongName={generatedCoin.CoinName || ''}
          coinPrice={generatedCoin.Price || ''}
          coinVolume={generatedCoin.Volume24H}
          // currencySymbol={generatedCoin.CoinCode || ''}
          currencyCode={generatedCoin.CurrencyCode}
          change24H={generatedCoin.Change24H || '0'}
          onPress={() => this.onSelectMarketItemHandler(generatedCoin)}
        />
      </View>
    );
  }

  filterCoinList() {
    const {
      coinList,
      favoriteCoinList,
      searchValue,
      selectedCurrency,
      tabIndex,
    } = this.state;
    if (
      (tabIndex === 0 && !_.isNil(coinList)) ||
      (tabIndex === 1 && !_.isNil(coinList)) ||
      (tabIndex === 2 && !_.isNil(coinList)) ||
      (tabIndex === 3 && !_.isNil(coinList)) ||
      (tabIndex === 4 && !_.isNil(favoriteCoinList))
    ) {
      let filteredList = [];
      if (searchValue && searchValue.length && searchValue.length > 0) {
        if (tabIndex === 3) {
          _.each(favoriteCoinList, item => {
            const lowerCasedSearchText = searchValue.toLowerCase();
            if (
              item.CurrencyId === selectedCurrency &&
              (item.CoinCode.toLowerCase().indexOf(lowerCasedSearchText) >= 0 ||
                item.CoinName.toLowerCase().indexOf(lowerCasedSearchText) >= 0)
            ) {
              filteredList.push(item);
            }
          });
          return filteredList;
        } else {
          _.each(coinList, coin => {
            const lowerCasedSearchText = searchValue.toLowerCase();
            if (
              coin.CoinCode.toLowerCase().indexOf(lowerCasedSearchText) >= 0 ||
              coin.CoinName.toLowerCase().indexOf(lowerCasedSearchText) >= 0
            ) {
              filteredList.push(coin);
            }
          });
        }
        return filteredList;
      }
      if (tabIndex === 4 && !_.isNil(favoriteCoinList)) {
        return favoriteCoinList;
      }
      return coinList;
    }
  }

  filterAction(coinList) {
    if (coinList && coinList.length > 0) {
      let filteredCoinList = coinList;
      const {filterLogic} = this.state;
      const {name, lastPrice, volume, dailyChange} = filterLogic;
      if (name !== undefined) {
        if (name) {
          filteredCoinList = _.sortBy(filteredCoinList, 'CoinName');
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'CoinName').reverse();
        }
      } else if (lastPrice !== undefined) {
        if (lastPrice) {
          filteredCoinList = _.sortBy(filteredCoinList, 'Price').reverse();
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'Price');
        }
      } else if (volume !== undefined) {
        if (volume) {
          filteredCoinList = _.sortBy(filteredCoinList, 'Volume24H').reverse();
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'Volume24H');
        }
      } else if (dailyChange !== undefined) {
        if (dailyChange) {
          filteredCoinList = _.sortBy(filteredCoinList, 'Change24H').reverse();
        } else {
          filteredCoinList = _.sortBy(filteredCoinList, 'Change24H');
        }
      }

      return filteredCoinList;
    }
    return coinList;
  }

  render() {
    const {navigation} = this.props;
    const {
      searchValue,
      coinListLoading,
      marketListLoading,
      filterLogic,
      routes,
      tabIndex,
    } = this.state;
    let filteredCoinList = this.filterCoinList();
    filteredCoinList = this.filterAction(filteredCoinList);

    return (
      <FADrawerHeader
        title={'Market'}
        navigation={navigation}
        routeMain={this.props.route.name}>
        <ThemeContext.Consumer>
          {({theme}) => (
            <>
              <View>
                <View style={{marginVertical: 8, marginHorizontal: 8}}>
                  <FASearchBar
                    theme={theme}
                    placeholder={Localization.t('SearchInput.Placeholder')}
                    value={searchValue}
                    onChangeText={newValue =>
                      this.onChangeSearchBarText(newValue)
                    }
                  />
                </View>
                <View>
                  <FACustomTabView
                    theme={theme}
                    scrollEnabled={false}
                    currentIndex={tabIndex}
                    routes={routes}
                    onIndexChange={newTabIndex =>
                      this.onChangeTabIndex(newTabIndex)
                    }
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: theme?.colors?.vGray,
                  }}>
                  <FASorting
                    theme={theme}
                    onPress={flag =>
                      this.setState(
                        {
                          filterLogic: {
                            name: flag,
                            lastPrice: undefined,
                            volume: undefined,
                            dailyChange: undefined,
                          },
                        },
                        () => this.filterAction(),
                      )
                    }
                    initialValue={true}
                    value={filterLogic.name}
                    containerStyle={{
                      flex: 1,
                      paddingLeft: 10,
                      backgroundColor: theme?.colors?.vGray,
                      paddingVertical: 10,
                    }}
                    text={Localization.t('sorting.Name')}
                  />
                  <FASorting
                    theme={theme}
                    onPress={flag =>
                      this.setState(
                        {
                          filterLogic: {
                            name: undefined,
                            lastPrice: flag,
                            volume: undefined,
                            dailyChange: undefined,
                          },
                        },
                        () => this.filterAction(),
                      )
                    }
                    value={filterLogic.lastPrice}
                    containerStyle={{
                      flex: 1,
                      paddingLeft: 10,
                      backgroundColor: theme?.colors?.vGray,
                      paddingVertical: 10,
                      marginLeft: 25,
                    }}
                    text={Localization.t('sorting.Price')}
                  />
                  <FASorting
                    theme={theme}
                    onPress={flag =>
                      this.setState(
                        {
                          filterLogic: {
                            name: undefined,
                            lastPrice: undefined,
                            volume: flag,
                            dailyChange: undefined,
                          },
                        },
                        () => this.filterAction(),
                      )
                    }
                    value={filterLogic.volume}
                    containerStyle={{
                      flex: 1,
                      paddingLeft: 10,
                      backgroundColor: theme?.colors?.vGray,
                      paddingVertical: 10,
                    }}
                    text={Localization.t('sorting.Volume')}
                  />
                  <FASorting
                    theme={theme}
                    onPress={flag =>
                      this.setState(
                        {
                          filterLogic: {
                            name: undefined,
                            lastPrice: undefined,
                            volume: undefined,
                            dailyChange: flag,
                          },
                        },
                        () => this.filterAction(),
                      )
                    }
                    value={filterLogic.dailyChange}
                    containerStyle={{
                      flex: 1,
                      paddingRight: 12,
                      backgroundColor: theme?.colors?.vGray,
                      paddingVertical: 10,
                      alignItems: 'flex-end',
                    }}
                    text={Localization.t('sorting.Change')}
                  />
                </View>
              </View>
              {coinListLoading || marketListLoading ? (
                <View style={{justifyContent: 'center', flex: 1}}>
                  <ActivityIndicator size={'large'} color={Orange} />
                </View>
              ) : filteredCoinList &&
                filteredCoinList.length &&
                filteredCoinList.length > 0 ? (
                <View style={{flex: 1}}>
                  <FlatList
                    ItemSeparatorComponent={() => (
                      <View
                        style={{
                          borderBottomWidth: 1,
                          borderBottomColor: theme?.colors?.blueGray,
                        }}
                      />
                    )}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index}
                    data={filteredCoinList}
                    renderItem={({item, index}) =>
                      this.renderCoinList(item, index)
                    }
                  />
                </View>
              ) : null}
            </>
          )}
        </ThemeContext.Consumer>
      </FADrawerHeader>
    );
  }
}

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {
  setSelectedCoinAndCurrencyCode,
  getMarketInformationAction,
})(MarketScreen);
