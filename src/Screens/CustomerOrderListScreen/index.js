import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import FAScrollingFilterList from '@Components/UI/FAScrollingFilterList';
import FAActiveOrderList from '@Components/Composite/FAActiveOrderList';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';
import autobind from 'autobind-decorator';
import Localization from '@Localization';
import {styles} from './assets/styles';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import FAService from '@Services';
import React from 'react';
import _ from 'lodash';
import {
  customerAllOrderTickerChannel,
  unSubscribeCustomerAllOrderTickerChannel,
} from '@Stream';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import FAPickerSelect from '@Components/Composite/FAPickerSelect';

class CustomerOrderListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customerOrders: [],
      selectedOrderType: BUYFLOW,
      isLoading: false,
      filter: null,
    };
  }

  customerOrderType = [
    {label: Localization.t('CommonsFix.BuyOrder'), value: BUYFLOW},
    {label: Localization.t('CommonsFix.SellOrder'), value: SELLFLOW},
  ];

  componentWillMount() {
    this.fillCustomerAllActiveOrders();
  }

  componentWillUnmount() {
    unSubscribeCustomerAllOrderTickerChannel();
  }

  @autobind
  fillCustomerAllActiveOrders() {
    this.setState({isLoading: true});
    FAService.GetCustomerAllActiveOrders()
      .then(response => {
        if (response && response.status === 200 && response.data) {
          this.setState(
            {
              customerOrders: response.data,
            },
            () =>
              customerAllOrderTickerChannel(this.handleCustomerAllActiveOrders),
          );
        }
        this.setState({
          isLoading: false,
        });
      })
      .catch(error => {
        this.setState({
          isLoading: false,
        });
      });
  }

  @autobind
  handleCustomerAllActiveOrders(data) {
    const {customerOrders, isAnonymousFlow} = this.state;
    if (!isAnonymousFlow) {
      if (data.CoinId && data.CurrencyId) {
        let orderArray = customerOrders;
        let incomingData = data;
        if (incomingData.CancelledAfterProcess) {
          return false;
        }
        const searchItemIndex = _.findIndex(orderArray, item => {
          return (item.OrderId || item.Id) === incomingData.Id;
        });

        const searchItem =
          _.find(orderArray, item => item.CoinId === incomingData.CoinId) ||
          null;

        if (searchItem) {
          incomingData = {
            ...incomingData,
            CoinCode: searchItem.CoinCode,
            CurrencyCode: searchItem.CurrencyCode,
          };
        }

        if (orderArray.length <= 0 || incomingData.State === 0) {
          orderArray.push(incomingData);
        } else if (searchItemIndex !== -1 && incomingData.State === 1) {
          orderArray[searchItemIndex] = incomingData;
        } else if (searchItemIndex !== -1 && incomingData.State === 2) {
          let item = orderArray[searchItemIndex];
          if (incomingData.ProcessCoinValue === incomingData.CoinValue) {
            orderArray.splice(searchItemIndex, 1);
          } else {
            item.CoinValue = item.CoinValue - incomingData.ProcessCoinValue;
            orderArray[searchItemIndex] = item;
          }
        } else if (searchItemIndex !== -1 && incomingData.State === 4) {
          orderArray.splice(searchItemIndex, 1);
        }

        this.setState({customerOrders: orderArray}, () => {
          const {getUserFinanceStateAction} = this.props;
          getUserFinanceStateAction();
        });
      }
    }
  }

  cancelCustomerActiveOrder() {
    const {getUserFinanceStateAction} = this.props;
    getUserFinanceStateAction();
  }

  render() {
    const {navigation} = this.props;
    const {customerOrders, selectedOrderType, isLoading, filter} = this.state;
    let customerOrderByFlow = customerOrders;
    let filterListByCoinList = [
      {
        label: Localization.t('CommonsFix.All'),
        value: null,
      },
      {
        label: Localization.t('CommonsFix.BuyOrder'),
        value: 'buyorder',
      },
      {
        label: Localization.t('CommonsFix.SellOrder'),
        value: 'sellorder',
      },
    ];
    if (selectedOrderType === BUYFLOW) {
      customerOrderByFlow = _.filter(customerOrders, {Type: BUYFLOW});
    } else {
      customerOrderByFlow = _.filter(customerOrders, {Type: SELLFLOW});
    }

    let customerOrdersByFilter = customerOrderByFlow;
    if (!_.isEmpty(customerOrderByFlow)) {
      _.each(customerOrderByFlow, order => {
        const filterIndex = _.findIndex(filterListByCoinList, {
          value: order.CoinCode,
        });
        if (filterIndex < 0 && order.CoinCode) {
          filterListByCoinList.push({
            label: order.CoinCode || null,
            value: order.CoinCode || null,
          });
        }
      });
      if (filter !== null) {
        customerOrdersByFilter = _.filter(customerOrderByFlow, {
          CoinCode: filter,
        });
      } else {
        customerOrdersByFilter = customerOrderByFlow;
      }
    }
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('FanScreenFix.ThereisNoOrder')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={isLoading} />
            <View style={styles.filterContainer}>
              <FAScrollingFilterList
                selected={selectedOrderType}
                data={this.customerOrderType}
                onSelect={newValue =>
                  this.setState({selectedOrderType: newValue})
                }
              />
              {!_.isEmpty(filterListByCoinList) && (
                <TouchableOpacity
                  style={{borderWidth: 2, borderColor: theme?.colors?.vGray}}>
                  <FAPickerSelect
                    nonBorder
                    paddingVertical={5}
                    paddingHorizontal={30}
                    onValueChange={filterValue =>
                      this.setState({filter: filterValue})
                    }
                    value={filter}
                    list={filterListByCoinList}
                    theme={theme}
                  />
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.listMainContainer}>
              <View style={styles.orderListContainer}>
                <FAActiveOrderList
                  showCodes={true}
                  data={customerOrdersByFilter}
                  onOrderCancelSuccess={this.cancelCustomerActiveOrder}
                />
              </View>
              {!(customerOrdersByFilter?.length > 0) && (
                <Text
                  style={[
                    styles.notFoundOrderText,
                    {color: theme?.colors?.darkBlue},
                  ]}>
                  {Localization.t('CoinWithdrawHistoryScreen.NoOrder')}
                </Text>
              )}
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default connect(null, {getUserFinanceStateAction})(
  CustomerOrderListScreen,
);
