import {StyleSheet} from 'react-native';
import {White, DarkBlue} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {backgroundColor: White, flex: 1},
  filterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 6,
    marginHorizontal: 8,
  },
  listMainContainer: {flex: 1, position: 'relative'},
  orderListContainer: {paddingHorizontal: 10, flex: 1},
  notFoundOrderText: {
    position: 'absolute',
    top: 20,
    alignSelf: 'center',
    color: DarkBlue,
  },
});
