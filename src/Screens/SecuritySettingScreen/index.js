import React, {Component} from 'react';
import {View} from 'react-native';
import FAColor from '@Commons/FAColor';
import FAAccountMenuItems from '@Components/Composite/FAAccountMenuItems';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import FAEnums from '@Commons/FAEnums';
import FAModal from '@Components/Composite/FAModal';
import Clipboard from '@react-native-community/clipboard';
import Toast from 'react-native-toast-message';
import FAService from '@Services';
import {getUserInformationAction} from '@GlobalStore/Actions/UserActions';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {styles} from './assets/styles';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class SecuritySettingScreen extends Component {
  constructor(props) {
    const UserState = props && props.UserState;
    super(props);
    this.state = {
      alreadyHasPin:
        UserState && UserState.UserInfo && UserState.UserInfo.HasPinState,
      isSmsAuth:
        UserState && UserState.UserInfo && UserState.UserInfo.IsSmsAuth,
      isTwoFactorAuth:
        UserState && UserState.UserInfo && UserState.UserInfo.IsTwoFactorAuth,
      flow: null,
      showApproveModal: false,
      hasEmailAuthCode: false,
      EmailAuthCodeValue: '',
      hasSmsAuthCode: false,
      SmsAuthCodeValue: '',
      hasGoogleAuthCode: false,
      GoogleAuthCodeValue: '',
      googleAuthQrUrl: true,
      googleAuthQrManuelKey: null,
      serviceLoading: false,
      serviceMessage: null,
      showDialogBox: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    const {UserState} = this.props;
    if (
      nextProps &&
      nextProps.UserState &&
      nextProps.UserState.UserInfo &&
      UserState &&
      UserState.UserInfo
    ) {
      this.setState(
        {
          isSmsAuth: nextProps.UserState.UserInfo.IsSmsAuth,
          isTwoFactorAuth: nextProps.UserState.UserInfo.IsTwoFactorAuth,
        },
        () => {},
      );
    }
  }

  callSmsProvider() {
    const {flow, isSmsAuth, isTwoFactorAuth} = this.state;
    const requestBody = {
      AuthCode: '',
      AuthType:
        flow === FAEnums.SmsProvider.sms
          ? 1
          : flow === FAEnums.SmsProvider.googleAuth
          ? 2
          : null,
      Status: flow === FAEnums.SmsProvider.sms ? !isSmsAuth : !isTwoFactorAuth,
    };

    FAService.SetCustomerAuthenticationType(requestBody)
      .then(res => {
        if (
          res &&
          res.data &&
          (res.data.QrCodeImageUrl || res.data.ManeulKeyForTwoFa)
        ) {
          this.setState({
            googleAuthQrUrl: res.data.QrCodeImageUrl,
            googleAuthQrManuelKey: res.data.ManeulKeyForTwoFa || null,
            showApproveModal: true,
          });
        } else if (res && res.data && res.data.CodeGenerated) {
          this.setState({
            showApproveModal: true,
          });
        } else if (res && res.data && flow === FAEnums.SmsProvider.sms) {
          this.setState({
            showApproveModal: true,
          });
        }
      })
      .catch(err => {});
  }

  callAuthenticationTypeService() {
    const {
      flow,
      isSmsAuth,
      isTwoFactorAuth,
      EmailAuthCodeValue,
      SmsAuthCodeValue,
      GoogleAuthCodeValue,
    } = this.state;
    let requestBody = {};
    if (flow === FAEnums.SmsProvider.googleAuth) {
      requestBody = {
        AuthCode: GoogleAuthCodeValue,
        SmsCode: SmsAuthCodeValue,
        EmailCode: EmailAuthCodeValue,
        AuthType:
          flow === FAEnums.SmsProvider.sms
            ? 1
            : flow === FAEnums.SmsProvider.googleAuth
            ? 2
            : null,
        Status:
          flow === FAEnums.SmsProvider.sms ? !isSmsAuth : !isTwoFactorAuth,
      };
    } else {
      requestBody = {
        AuthCode: SmsAuthCodeValue,
        EmailCode: EmailAuthCodeValue,
        AuthType:
          flow === FAEnums.SmsProvider.sms
            ? 1
            : flow === FAEnums.SmsProvider.googleAuth
            ? 2
            : null,
        Status:
          flow === FAEnums.SmsProvider.sms ? !isSmsAuth : !isTwoFactorAuth,
      };
    }
    this.setState({
      serviceLoading: true,
    });
    const {getUserInformationAction} = this.props;

    FAService.SetCustomerAuthenticationType(requestBody)
      .then(res => {
        if (res && res.data && res.data.IsCompleted) {
          this.setState(
            {
              showApproveModal: false,
              serviceLoading: false,
            },
            () => {
              Toast.show({
                type: 'success',
                position: 'top',
                text1: Localization.t('Commons.Succes'),
                text2: Localization.t('Commons.SuccessfulTransaction'),
                visibilityTime: 750,
                autoHide: true,
                topOffset: 75,
                bottomOffset: 40,
                onShow: () => {},
                onHide: () => {},
                onPress: () => {},
              });
              getUserInformationAction();
            },
          );
        } else if (res && res.data && res.data.ErrorMessage) {
          this.setState({
            showApproveModal: false,
            serviceLoading: false,
            showDialogBox: true,
            serviceMessage: res.data.ErrorMessage,
          });
        }
      })
      .catch(err => {});
  }

  @autobind
  smsAuthSection() {
    this.setState(
      {
        hasEmailAuthCode: true,
        hasSmsAuthCode: true,
        hasGoogleAuthCode: false,
        flow: FAEnums.SmsProvider.sms,
      },
      () => this.callSmsProvider(),
    );
  }

  twoFactorAuthSection() {
    this.setState(
      {
        hasEmailAuthCode: true,
        hasSmsAuthCode: true,
        hasGoogleAuthCode: true,
        flow: FAEnums.SmsProvider.googleAuth,
      },
      () => this.callSmsProvider(),
    );
  }

  copyQrCodeManuelKey() {
    const {googleAuthQrManuelKey} = this.state;
    Clipboard.setString(googleAuthQrManuelKey);
    Toast.show({
      type: 'success',
      position: 'top',
      text1: Localization.t('Commons.Succes'),
      text2: Localization.t('Commons.SuccessfulTransaction'),
      visibilityTime: 750,
      autoHide: true,
      topOffset: 75,
      bottomOffset: 40,
      onShow: () => {},
      onHide: () => {},
      onPress: () => {},
    });
  }

  render() {
    const {navigation} = this.props;
    const {
      isOnPin,
      isSmsAuth,
      isTwoFactorAuth,
      showApproveModal,
      hasEmailAuthCode,
      hasSmsAuthCode,
      hasGoogleAuthCode,
      googleAuthQrManuelKey,
      serviceLoading,
      serviceMessage,
      showDialogBox,
      alreadyHasPin,
    } = this.state;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('SecuritySettings.header')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showDialogBox}
              text={serviceMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <FAModal.WithSecurityInput
              qrCodeUrl={hasGoogleAuthCode}
              qrCodeManuelKey={googleAuthQrManuelKey}
              visible={showApproveModal}
              smsCode={hasSmsAuthCode}
              onChangeSmsCode={value =>
                this.setState({SmsAuthCodeValue: value})
              }
              emailCode={hasEmailAuthCode}
              onChangeEmailCode={value =>
                this.setState({EmailAuthCodeValue: value})
              }
              googleCode={hasGoogleAuthCode}
              onChangeGoogleCode={value =>
                this.setState({GoogleAuthCodeValue: value})
              }
              onPressSendButton={() => this.callAuthenticationTypeService()}
              onPressCancelButton={() =>
                this.setState({
                  showApproveModal: false,
                })
              }
              copyQrCodeManuelKey={() => this.copyQrCodeManuelKey()}
            />
            {alreadyHasPin ? (
              <FAAccountMenuItems.AccountParentItems
                text={Localization.t('SecuritySettings.ChangePIN')}
                onPress={() => navigation.navigate('PinGenerate')}
              />
            ) : (
              <FAAccountMenuItems.WithSwitch
                size={'small'}
                text={Localization.t('SecuritySettings.DefinePIN')}
                isOn={alreadyHasPin}
                onToggle={() =>
                  this.setState(
                    {
                      alreadyHasPin: !alreadyHasPin,
                    },
                    () => navigation.navigate('PinGenerate'),
                  )
                }
              />
            )}

            <FAAccountMenuItems.WithSwitch
              disabled={isSmsAuth}
              size={'small'}
              text={Localization.t('SecuritySettings.SmsLoginConfirmation')}
              isOn={isSmsAuth}
              onToggle={() => this.smsAuthSection()}
            />
            <FAAccountMenuItems.WithSwitch
              disabled={isTwoFactorAuth}
              size={'small'}
              text={Localization.t('SecuritySettings.GoogleAuthenticator')}
              isOn={isTwoFactorAuth}
              onToggle={() => this.twoFactorAuthSection()}
            />
            <FAAccountMenuItems.AccountParentItems
              text={Localization.t('SecuritySettings.ChangePassword')}
              onPress={() => navigation.navigate('PasswordGenerate')}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {getUserInformationAction})(
  SecuritySettingScreen,
);
