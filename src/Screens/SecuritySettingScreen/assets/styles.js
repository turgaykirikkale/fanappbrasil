import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  headerLeftIconContainer: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  mainContainer: {backgroundColor: FAColor.White, flex: 1},
});
