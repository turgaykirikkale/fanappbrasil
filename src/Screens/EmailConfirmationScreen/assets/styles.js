import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  childContainer: {marginHorizontal: 10, marginTop: 16},
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 16,
  },
  buttonText: {
    marginVertical: 16,
    color: FAColor.White,
  },
});
