import React, {Component} from 'react';
import {View, Text} from 'react-native';
import FATextInput from '@Components/Composite/FATextInput';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import FAService from '@Services';
import autobind from 'autobind-decorator';
import {CommonActions} from '@react-navigation/native';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAModal from '@Components/Composite/FAModal';
import FATextRendered from '@Components/UI/FATextRendered';
import FAForm from '@ValidationEngine/Form';
import FAEmailConfirmModal from '@Components/Composite/FAEmailConfirmModal';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAColor from '@Commons/FAColor';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAEnums from '../../Commons/FAEnums';
import FAAlertModal from '@Components/Composite/FAAlertModal';

export default class EmailConfirmationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      type: 1,
      Phone:
        (props &&
          props.route &&
          props.route.params &&
          props.route.params.phone) ||
        null,
      flow:
        (props &&
          props.route &&
          props.route.params &&
          props.route.params.flow) ||
        FAEnums.REGISTERFLOW,
      formIsValid: false,
      modalShow: false,
      serviceLoading: false,
      showDialogBox: false,
      serviceMessage: null,
      showApproveEmailModal: false,
      FormIsValid: false,
      serviceErrorMessage: null,
    };
  }

  @autobind
  sendMailVerificationServiceCall() {
    const {email, Phone} = this.state;
    const {navigation} = this.props;

    let requestBody = {
      Phone: Phone,
      Email: email,
    };

    this.setState({serviceLoading: true}, () => {
      FAService.UserRegisterSendMailVerification(requestBody)
        .then(response => {
          if (
            response &&
            response.data &&
            response.status === 200 &&
            response.data.IsSuccess
          ) {
            this.setState({
              serviceLoading: false,
              modalShow: true,
              showApproveEmailModal: false,
            });
          } else {
            this.setState({
              showApproveEmailModal: false,
              serviceLoading: false,
              showDialogBox: true,
              serviceErrorMessage:
                response.data.Message || response.data.ErrorMessage,
            });
          }
        })
        .catch(error => {
          this.setState({
            showApproveEmailModal: false,
            serviceLoading: false,
            showDialogBox: true,
            serviceErrorMessage: Localization.t('CommonsFix.UnexpectedError'),
          });
        });
    });
  }
  @autobind
  navigateToPreLogin() {
    const {navigation} = this.props;

    this.setState({modalShow: false}, () => {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'AuthLoaderScreen'}],
        }),
      );
    });
  }

  @autobind
  textLogic() {
    const {email} = this.state;
    return (
      <FATextRendered
        rightText={Localization.t(
          'EmailConfirmationScreenFix.EmailVerification',
        )}
        boldSentences={[`${email}`]}
        disabled={true}
      />
    );
  }

  render() {
    const {
      email,
      type,
      serviceLoading,
      showApproveEmailModal,
      FormIsValid,
      serviceErrorMessage,
      showDialogBox,
    } = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.blueGray},
            ]}>
            <FAAlertModal
              visible={showDialogBox}
              text={serviceErrorMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <FAStandardHeader
              title={Localization.t('EmailConfirmationScreenFix.HeaderTitle')}
              navigation={navigation}
              hideBackButton={true}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAEmailConfirmModal
              visible={this.state.modalShow}
              onPress={() => this.navigateToPreLogin()}
            />
            <FAModal.Alert
              visible={showApproveEmailModal}
              buttons={[
                {
                  text: Localization.t('CommonsFix.Cancel'),
                  containerStyle: {
                    backgroundColor: FAColor.VBlueGray,
                    borderRadius: 4,
                  },
                  textStyle: {marginVertical: 15, color: FAColor.DarkBlue},
                  onPress: () =>
                    this.setState({
                      showApproveEmailModal: false,
                    }),
                },
                {
                  text: Localization.t('CommonsFix.Send'),
                  containerStyle: {
                    backgroundColor: FAColor.Green,
                    borderRadius: 4,
                  },
                  textStyle: {marginVertical: 15, color: FAColor.White},
                  onPress: () => this.sendMailVerificationServiceCall(),
                  // onPress: () =>
                  //   this.setState({
                  //     serviceLoading: false,
                  //     modalShow: true,
                  //     showApproveEmailModal: false,
                  //   }),
                },
              ]}
              text={this.textLogic()}
            />

            <View style={styles.childContainer}>
              <Text style={{color: theme?.colors?.darkBlue}}>
                {Localization.t(
                  'EmailConfirmationScreenFix.CompleteEmailAdress',
                )}
              </Text>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    FormIsValid: value,
                  })
                }>
                <View style={{marginTop: 8}} />
                <FATextInput
                  rule={'isEmail'}
                  name={'email'}
                  label={Localization.t('Commons.EmailAdress')}
                  placeholder={Localization.t('Commons.EmailAdress')}
                  onChangeValue={newWalue =>
                    this.setState({
                      email: newWalue,
                    })
                  }
                  value={email}
                />
              </FAForm>
              <FAButton
                disabled={!FormIsValid}
                onPress={() =>
                  this.setState({
                    showApproveEmailModal: true,
                  })
                }
                containerStyle={styles.buttonContainer}
                textStyle={styles.buttonText}
                text={
                  type === 1
                    ? Localization.t('Commons.Okay')
                    : Localization.t('Commons.Update')
                }
              />
              <View
                style={{
                  backgroundColor: '#FDFAE5',
                  borderRadius: 4,
                  borderColor: '#C7AC67',
                  borderWidth: 0.5,
                }}>
                <Text
                  style={{
                    color: '#C7AC67',
                    marginVertical: 15,
                    marginHorizontal: 10,
                  }}>
                  {Localization.t('EmailConfirmationScreenFix.Proceed')}
                </Text>
              </View>
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
