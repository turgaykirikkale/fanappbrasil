import React, {useEffect, useState} from 'react';
import {Dimensions, Text, TouchableOpacity, View} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAAssetShower from '@Components/Composite/FAAssetShower';
import {DarkGray} from '@Commons/FAColor';
import {TabBar, TabView} from 'react-native-tab-view';
import FAPollCardList from '@Components/Composite/FAPollCardList';
import FAIcon from '@Components/UI/FAIcon';
import {connect} from 'react-redux';
import {getSurveyAction} from '@GlobalStore/Actions/SurveyActions';
import {AppConstants} from '../../Commons/Contants';
import _ from 'lodash';
import {toFixedNoRounding} from '../../Commons/FAMath';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const SurveysScreen = props => {
  const {navigation} = props;
  const route = props?.route?.params?.routeMain;
  const {SurveyState, UserState} = props;
  const [index, setIndex] = useState(0);
  const [activeSurveys, setactiveSurveys] = useState([]);
  const [passedSurveys, setpassedSurveys] = useState([]);
  const [votedSurveys, setvotedSurveys] = useState([]);
  const [showFreeActiveSurveys, setShowFreeActiveSurveys] = useState(false);
  const [showFreePassedSurveys, setShowFreePassedSurveys] = useState(false);
  const [showFreeVotedSurveys, setShowFreeVotedSurveys] = useState(false);
  const [coinBalance, setCoinBalance] = useState(0.0);
  const [currencyBalance, setCurrencyBalance] = useState(0.0);
  const [isLoading, setIsLoading] = useState(true);

  const [routes, setRoutes] = useState([
    {
      key: 'active',
      title: Localization.t('Commons.Active'),
    },
    {
      key: 'voted',
      title: Localization.t('Commons.Joined'),
    },
    {
      key: 'passed',
      title: Localization.t('Commons.Passed'),
    },
  ]);

  useEffect(() => {
    const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;
    const {getSurveyAction} = props;
    if (!isAnonymousFlow) {
      getSurveyAction();
    }
  }, []);

  useEffect(() => {
    if (SurveyState) {
      setactiveSurveys(SurveyState?.ActiveSurveys);
      setpassedSurveys(SurveyState?.PassedSurveys);
      setvotedSurveys(SurveyState?.VotedSurveys);
    }
    setIsLoading(false);
  }, [SurveyState]);

  useEffect(() => {
    if (UserState?.FinanceInfo) {
      const {CustomerCoinBalanceDetailList} = UserState?.FinanceInfo;
      const currencyBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        CoinId: 29, // TODO Appconstants currencyID 2007 olarak alınmış static verildi
      });
      const coinBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        CoinId: AppConstants.CoinId,
      });
      setCurrencyBalance(
        toFixedNoRounding(currencyBalanceDetail?.CoinBalance, 4) || '0.00',
        AppConstants.CurrencyDecimalCount,
      );

      setCoinBalance(
        toFixedNoRounding(coinBalanceDetail?.CoinBalance, 2) || '0.00',
        AppConstants.CoinDecimalCount,
      );
    }
  }, [UserState]);

  const renderTabBar = (tabProps, theme) => {
    return (
      <TabBar
        {...tabProps}
        renderLabel={({route, focused, color}) => (
          <Text
            style={{
              fontSize: 12,
              color: focused
                ? theme?.colors?.faOrange
                : theme?.colors?.darkBlue,
            }}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={{
          borderColor: theme?.colors?.gray,
        }}
        indicatorStyle={{
          backgroundColor: theme?.colors?.faOrange,
        }}
        style={{
          backgroundColor: theme?.colors?.white,
        }}
      />
    );
  };

  const renderScene = ({route}, theme) => {
    switch (route.key) {
      case 'active':
        return tabRenderActiveSurvey(theme);
      case 'voted':
        return tabRenderVotedSurvey(theme);
      case 'passed':
        return tabRenderPassedSurvey(theme);
      default:
        return null;
    }
  };

  const renderFilterButtonText = (showFreeEvents: Boolean) => {
    let buttonText = `%s ${Localization.t('SurveyScreen.showSurveys')}`;
    if (showFreeEvents) {
      buttonText = buttonText.replace(/%s/g, Localization.t('Commons.All'));
    } else {
      buttonText = buttonText.replace(/%s/g, Localization.t('Commons.Free'));
    }
    return buttonText;
  };

  const renderFilterEventListButton = (
    eventList: Array,
    eventType: 'active' | 'voted' | 'passed',
  ) => {
    let buttonText = Localization.t('SurveyScreen.noJoined');
    buttonText = renderFilterButtonText(
      eventType === 'active'
        ? showFreeActiveSurveys
        : eventType === 'voted'
        ? showFreeVotedSurveys
        : eventType === 'passed'
        ? showFreePassedSurveys
        : null,
    );
    return (
      <TouchableOpacity
        onPress={() => filterSurveyList(eventList, eventType)}
        activeOpacity={0.8}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 15,
          paddingHorizontal: 13,
        }}>
        <View style={{width: 14, height: 14}}>
          <FAIcon iconName={'FAEye'} color={'#AAAAAA'} />
        </View>
        <Text style={{fontSize: 12, color: '#AAAAAA', marginLeft: 9}}>
          {buttonText}
        </Text>
      </TouchableOpacity>
    );
  };

  const filterSurveyList = (
    eventList: Array,
    eventType: 'active' | 'voted' | 'passed',
  ) => {
    if (eventType === 'active') {
      setShowFreeActiveSurveys(!showFreeActiveSurveys);
    } else if (eventType === 'voted') {
      setShowFreeVotedSurveys(!showFreeVotedSurveys);
    } else if (eventType === 'passed') {
      setShowFreePassedSurveys(!showFreePassedSurveys);
    }
  };

  const navigateToSurveyDetailScreen = item => {
    const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;
    const UserBalance =
      props?.UserState?.FinanceInfo?.CustomerCoinBalanceDetailList;

    navigation.navigate('SurveyScreenDetail', {
      UserBalance,
      event: item,
      isAnonymousFlow: isAnonymousFlow,
      routeMain: route,
    });
  };

  const tabRenderActiveSurvey = theme => {
    return (
      <View style={{marginHorizontal: 5, flex: 1}}>
        {renderFilterEventListButton(activeSurveys, 'active')}
        <FAPollCardList
          data={
            showFreeActiveSurveys ? activeSurveys?.Free : activeSurveys?.All
          }
          spaceBetweenItems={10}
          onPress={item => navigateToSurveyDetailScreen(item)}
        />
      </View>
    );
  };

  const tabRenderVotedSurvey = () => {
    if (!_.isEmpty(votedSurveys) && (votedSurveys.All || votedSurveys.Free)) {
      const votedSurveysData = showFreeVotedSurveys
        ? votedSurveys?.Free
        : votedSurveys?.All;
      return (
        <View style={{marginHorizontal: 5, flex: 1}}>
          {renderFilterEventListButton(votedSurveys, 'voted')}
          <FAPollCardList
            data={votedSurveysData}
            spaceBetweenItems={10}
            onPress={item => navigateToSurveyDetailScreen(item)}
          />
        </View>
      );
    }
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Text
          style={{
            textAlign: 'center',
            color: DarkGray,
            fontSize: 14,
          }}>
          {Localization.t('SurveyScreen.noJoined')}
        </Text>
      </View>
    );
  };

  const tabRenderPassedSurvey = () => {
    return (
      <View style={{marginHorizontal: 5, flex: 1}}>
        {renderFilterEventListButton(passedSurveys, 'passed')}
        <FAPollCardList
          data={
            showFreePassedSurveys ? passedSurveys?.Free : passedSurveys?.All
          }
          spaceBetweenItems={10}
          onPress={item => navigateToSurveyDetailScreen(item)}
        />
      </View>
    );
  };
  const isAnonymousFlow = props?.MarketState?.isAnonymousFlow;
  const initialLayout = {width: Dimensions.get('window').width};

  if (isLoading) {
    return <FAFullScreenLoader isLoading={isLoading} />;
  }
  return (
    <>
      <FAStandardHeader
        title={Localization.t('SurveyScreenDetail.header')}
        navigation={navigation}
      />
     
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
            <FAAssetShower
              onPress={flow => {
                flow === 1
                  ? navigation.navigate('LoginScreen')
                  : flow === 2
                  ? navigation.navigate('RegisterStepOne')
                  : navigation.navigate('WalletTabStackNavigator');
              }}
              fanTokenBalance={coinBalance}
              currencyBalance={currencyBalance}
            />
            {isAnonymousFlow ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: theme?.colors?.darkGray, fontSize: 16}}>
                  {Localization.t('SurveyScreen.JoinAnonymous')}
                </Text>
              </View>
            ) : (
              <TabView
                renderTabBar={tabProps => renderTabBar(tabProps, theme)}
                renderScene={sceneProps => renderScene(sceneProps, theme)}
                navigationState={{index, routes}}
                onIndexChange={indexValue => setIndex(indexValue)}
                initialLayout={initialLayout}
              />
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

const mapStateToProps = state => {
  return {
    SurveyState: state.SurveyState,
    UserState: state.UserState,
    MarketState: state.MarketState,
  };
};

export default connect(mapStateToProps, {
  getSurveyAction,
})(SurveysScreen);
