import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import React, {useEffect, useRef, useState} from 'react';
import {sentenceShortener} from '@Helpers/StringHelper';
import {RenderHTML} from 'react-native-render-html';
import WebView from 'react-native-webview';
import Localization from '@Localization';
import {styles} from './assets/styles';
import FAService from '@Services';
import moment from 'moment';
import _ from 'lodash';
import {
  Dimensions,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Analytics from 'appcenter-analytics';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const VideoScreen = props => {
  const [similarVideos, setSimilarVideos] = useState([]);
  const routeParams = props?.route?.params;
  const incomingVideoItem = routeParams?.video;
  const incomingCategory = routeParams?.category;
  const contentSource = {
    html: `${incomingVideoItem?.Content || ''}`,
  };
  const language = Localization.language || 'en';

  useEffect(() => {
    Analytics.trackEvent('Video Display', {
      Category: incomingCategory?.CategoryName || null,
      Video: incomingVideoItem?.Title || null,
    });

    const categoryId = incomingCategory?.CategoryId;
    if (categoryId) {
      FAService.GetMainPageVideosByCategory(categoryId)
        .then(response => {
          if (response?.data) {
            let allVideosWithoutCurrent = response.data;
            _.remove(allVideosWithoutCurrent, {Id: incomingVideoItem?.Id});
            allVideosWithoutCurrent = _.slice(allVideosWithoutCurrent, 0, 6);
            if (!_.isEmpty(allVideosWithoutCurrent)) {
              setSimilarVideos(allVideosWithoutCurrent);
            }
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }, [incomingVideoItem]);

  const scrollRef = useRef();
  const screenWidth = Dimensions.get('screen').width;
  return (
    <>
      <FAStandardHeader title={'Video'} navigation={props.navigation} />
      <ThemeContext.Consumer>
        {({theme}) => (
          <ScrollView
            style={[
              styles.scrollView,
              {backgroundColor: theme?.colors?.blueGray},
            ]}>
            <WebView
              source={{uri: incomingVideoItem?.MediaUrl}}
              style={[styles.video, {backgroundColor: theme?.colors?.white}]}
            />
            <View style={styles.contentContainer}>
              <Text style={[styles.title, {color: theme?.colors?.faOrange}]}>
                {incomingVideoItem?.Title || ''}
              </Text>
              <RenderHTML
                source={contentSource}
                tagsStyles={{
                  body: {
                    color: theme?.colors?.black,
                  },
                }}
              />
            </View>
            {incomingVideoItem.CreatedOn && (
              <View
                style={[
                  styles.publishDateContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <Text
                  style={[styles.publishDate, {color: theme?.colors?.black}]}>
                  {moment(incomingVideoItem.CreatedOn)
                    .locale(language)
                    .fromNow()}
                </Text>
              </View>
            )}
            {!_.isEmpty(similarVideos) && (
              <View style={styles.contentContainer}>
                <Text
                  style={[
                    styles.similarTitle,
                    {color: theme?.colors?.faOrange},
                  ]}>
                  İlgili Videolar
                </Text>
                {similarVideos?.map((item, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      props.navigation.navigate('VideoScreen', {
                        video: item,
                        category: incomingCategory,
                      });
                      if (scrollRef !== null && scrollRef.current) {
                        scrollRef.current?.scrollTo({
                          y: 0,
                          animated: true,
                        });
                      }
                    }}
                    activeOpacity={1}
                    style={[
                      styles.similarVideoContainer,
                      {backgroundColor: theme?.colors?.white},
                    ]}>
                    <Image
                      source={{
                        uri: item.ImageUrl || '',
                      }}
                      style={{height: 75, width: 75}}
                      resizeMode={'cover'}
                    />
                    <View style={{width: screenWidth - (36 + 75)}}>
                      <Text
                        style={[
                          styles.marginLeft12,
                          {color: theme?.colors?.black},
                        ]}>
                        {sentenceShortener(item.Title || '', 50)}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </View>
            )}
          </ScrollView>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default VideoScreen;
