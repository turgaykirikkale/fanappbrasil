import {Dimensions, StyleSheet} from 'react-native';
import {DarkBlue, FAOrange, White} from '@Commons/FAColor';

const screenWidth = Dimensions.get('screen').width;
export const styles = StyleSheet.create({
  scrollView: {flex: 1},
  video: {height: screenWidth / 1.75},
  contentContainer: {paddingHorizontal: 12, paddingVertical: 20},
  title: {fontSize: 16, color: DarkBlue, paddingBottom: 6},
  publishDateContainer: {
    paddingVertical: 6,
    backgroundColor: White,
    paddingHorizontal: 16,
    marginTop: 12,
  },
  publishDate: {fontSize: 10, color: DarkBlue, textAlign: 'center'},
  similarTitle: {fontSize: 12, fontWeight: 'bold', color: FAOrange},
  similarVideoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingRight: 12,
    marginVertical: 8,
  },
  marginLeft12: {marginLeft: 12},
});
