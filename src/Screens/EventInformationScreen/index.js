import React, {Component} from 'react';
import {SafeAreaView, View, Text, Image, ScrollView} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import {AppConstants} from '@Commons/Contants';
import moment from 'moment';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import {CommonActions} from '@react-navigation/native';

export default class EventInformationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventDetail: props?.route?.params?.event,
      CoinCode: props?.route?.params?.CoinCode,
      route: props?.route?.params?.routeMain,
    };
  }

  render() {
    const {eventDetail, CoinCode, route} = this.state;
    const {navigation} = this.props;
    const formattedEndDate =
      eventDetail?.ActivityEndDate &&
      moment(eventDetail.ActivityEndDate).format('DD MMMM YYYY HH:mm');
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <SafeAreaView
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <ScrollView>
              <View
                style={[
                  styles.approveTextContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <Image
                  style={styles.imageStyle}
                  // source={require('../../../Assets/images/korfez.jpg')}
                  source={{
                    uri: `https://borsa.bitci.com/img/coin/${CoinCode}.png`,
                  }}
                  //propstan alıncak
                />
                <Text
                  style={[
                    styles.congratulationsTextStyle,
                    {color: theme?.colors?.success},
                  ]}>
                  {Localization.t('EventInformationScreen.CONG')}
                </Text>
                <Text
                  style={[
                    styles.headerInformationText,
                    {color: theme?.colors?.black},
                  ]}>
                  {eventDetail?.Title?.toLocaleUpperCase(
                    AppConstants.DefaultLanguage,
                  )}{' '}
                  {Localization.t('EventInformationScreen.JoinedEvent')}
                </Text>
              </View>
              <View
                style={[
                  styles.informationTextContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <Text
                  style={[
                    styles.informationTextStyle,
                    {color: theme?.colors?.black},
                  ]}>
                  *{' '}
                  {Localization.t('EventInformationScreen.JoinedEvent', {
                    formattedEndDate: formattedEndDate,
                  })}
                </Text>
              </View>
            </ScrollView>
            <View style={styles.buttonMainContainer}>
              <FAButton
                onPress={() =>
                  navigation.dispatch(
                    CommonActions.reset({
                      index: 1,
                      routes: [
                        {name: route},
                        {
                          name: 'EventsScreen',
                          params: {routeMain: route},
                        },
                      ],
                    }),
                  )
                }
                text={Localization.t('EventInformationScreen.Events')}
                textStyle={[
                  styles.buttonTextStyle,
                  {
                    color: theme?.colors?.white,
                  },
                ]}
                rightIconSize={10}
                containerStyle={[
                  styles.buttonContainer,
                  {backgroundColor: theme?.colors?.black},
                ]}
              />
            </View>
          </SafeAreaView>
        )}
      </ThemeContext.Consumer>
    );
  }
}
