import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1},
  approveTextContainer: {
    paddingHorizontal: 30,
    paddingVertical: 36,
    backgroundColor: FAColor.White,
    borderRadius: 4,
    marginHorizontal: 8,
    marginVertical: 15,
  },
  imageStyle: {width: 100, height: 100, alignSelf: 'center'},
  congratulationsTextStyle: {
    color: FAColor.Success,
    textAlign: 'center',
    fontSize: 20,
    marginTop: 13,
  },
  headerInformationText: {
    color: FAColor.Black,
    fontSize: 12,
    textAlign: 'center',
    lineHeight: 20,
    marginTop: 20,
  },
  informationTextContainer: {marginHorizontal: 9},
  informationTextStyle: {
    fontSize: 11,
    color: FAColor.DarkGray,
    lineHeight: 20,
  },
  buttonMainContainer: {flex: 1, justifyContent: 'flex-end'},
  buttonContainer: {backgroundColor: FAColor.Black},
  buttonTextStyle: {
    marginVertical: 16,
    color: FAColor.White,
    fontWeight: '700',
  },
});
