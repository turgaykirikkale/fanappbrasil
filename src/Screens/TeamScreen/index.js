import React, {useEffect, useState} from 'react';
import FADrawerHeader from '../../Components/Composite/FADrawerHeader';
import {ScrollView, Text, View, FlatList, TouchableOpacity} from 'react-native';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FATeamResults from '../../Components/Composite/FATeamResults';
import {connect} from 'react-redux';
import _, {each} from 'lodash';
import FAListHeader from '@Components/Composite/FAListHeader';
import FASlider from '@Components/UI/FASlider';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import FAService from '@Services';
import FAFanCard from '@Components/UI/FAFanCard';
import BroadageService from '../../Services/BroadgeService/BroadageService';
import FAMatchCenterHeaderSelect from '@Components/Composite/FAMatchCenterHeaderSelect';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAStandingsComponent from '../../Components/UI/FAStandingsComponent';
import FAFixtureItem from '../../Components/UI/FAFixtureItem';

const TeamScreen = props => {
  const [videos, setVideos] = useState([]);
  const [tournaments, setTournaments] = useState([]);
  const [matchList, setMatchlist] = useState([]);

  const [serviceLoading, setServiceLoading] = useState(true);

  //modalsControl
  const [showTournametsModal, setShowTournamentsModal] = useState(false);
  const [showSeasonModal, setShowSeasonModal] = useState(false);
  const [showWeekModal, setShowWeekModal] = useState(false);

  const [standingList, setStandinList] = useState([]);

  useEffect(() => {
    BroadageService.GetTournamentsList()
      .then(res => {
        debugger;
        if (res?.data?.data) {
          const responseData = res?.data?.data;
          let FilteredData = _.find(responseData, {externalId: 35});
          debugger;
          if (FilteredData?.id) {
            debugger;
            BroadageService.GetSoccerSeaosonId(FilteredData?.id)
              .then(responseSeaosonId => {
                debugger;
                if (responseSeaosonId?.data?.data?.seasons) {
                  debugger;
                  const responseSeaosonIdData =
                    responseSeaosonId?.data?.data?.seasons[0];
                  debugger;
                  console.log('responseSeaosonId', responseSeaosonIdData);
                  if (responseSeaosonIdData?.id) {
                    debugger;
                    BroadageService.GetSoccerSeaosonStandings(
                      responseSeaosonIdData?.id,
                    )
                      .then(responseStanding => {
                        debugger;
                        if (responseStanding?.status === 200) {
                          debugger;
                          let standing = [];
                          console.log('responseStanding', responseStanding);
                          standing = responseStanding?.data;
                          setStandinList(standing);
                          setServiceLoading(false);
                          debugger;
                        }
                      })
                      .catch(err =>
                        console.log('GetSoccerSeaosonStandings', err),
                      );
                    BroadageService.GetSoccerSeaosonFixture(
                      responseSeaosonIdData?.id,
                      null,
                    )
                      .then(responseFixtrue => {
                        debugger;
                        console.log('GetSoccerSeaosonFixture', responseFixtrue);
                        if (responseFixtrue?.data) {
                          // setServiceLoading(false);
                          let sliceData = responseFixtrue?.data.slice(0, 5);
                          setMatchlist(sliceData);
                        }
                      })
                      .catch(err =>
                        console.log('GetSoccerSeaosonFixture', err),
                      );
                  }
                }
              })
              .catch(err => console.log('GetSoccerSeaosonId', err));
          }
        }
      })
      .catch(err => console.log('GetTournamentsList', err));
    // BroadageService.GetTournamentsList()
    //   .then(res => {
    //     console.log('GetTournamentsList', res);
    //     if (res?.data?.data) {
    //       let filteredTournamenst = [];

    //       _.each(res.data.data, item => {
    //         if (item.externalId === 35) {
    //           const itemFixed = {
    //             label: item.name,
    //             value: item,
    //           };
    //           filteredTournamenst.push(itemFixed);
    //         }
    //       });
    //       setTournaments(filteredTournamenst);
    //       getSeaonsId(filteredTournamenst);
    //     }
    //   })
    //   .catch(err => console.log('GetTournamentsList', err));
  }, []);

  // const getSeaonsId = data => {
  //   if (data) {
  //     _.map(data, item => {
  //       BroadageService.GetSoccerSeaosonId(item.value.id)
  //         .then(res => {
  //           const seaosonInfo = res?.data?.data;
  //           console.log('GetSoccerSeaosonId', res);
  //           getMatchList(seaosonInfo);
  //         })
  //         .catch(err => console.log('GetSoccerSeaosonId', err));
  //     });
  //   }
  // };

  // const getMatchList = seasonId => {
  //   if (seasonId) {
  //     BroadageService.GetSoccerSeaosonFixture(seasonId.seasons[0].id)
  //       .then(res => {
  //         console.log('GetSoccerSeaosonFixture', res);
  //         setMatchlist(res.data);
  //         setServiceLoading(false);
  //       })
  //       .catch(err => console.log('GetSoccerSeaosonFixture', err));
  //   }
  // };
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonId()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonId', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonId', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetDailyMatchList()
  //     .then(res => {
  //       console.log('GetDailyMatchList', res);
  //     })
  //     .catch(err => console.log('GetDailyMatchList', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonStandings()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonStandings', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonStandings', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonFixture()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonFixture', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonFixture', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonTeams()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonTeams', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonTeams', err));
  // }, []);

  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonLeaderBoardGoal()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonLeaderBoardGoal', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonLeaderBoardGoal', err));
  // }, []);

  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonLeaderBoardAssist()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonLeaderBoardAssist', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonLeaderBoardAssist', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonLeaderBoardRedCard()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonLeaderBoardRedCard', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonLeaderBoardRedCard', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerSeaosonLeaderBoardYellowCard()
  //     .then(res => {
  //       console.log('GetSoccerSeaosonLeaderBoardYellowCard', res);
  //     })
  //     .catch(err => console.log('GetSoccerSeaosonLeaderBoardYellowCard', err));
  // }, []);
  // useEffect(() => {
  //   BroadageService.GetSoccerTeams()
  //     .then(res => {
  //       console.log('GetSoccerTeams', res);
  //     })
  //     .catch(err => console.log('GetSoccerTeams', err));
  // }, []);

  useEffect(() => {
    let Videos = [];
    let VideosDetail = [];

    FAService.GetMainPageVideos()
      .then(res => {
        if (res?.data) {
          const resData = res.data;
          const categoryFiterList = _.filter(resData, category => {
            return !_.isEmpty(category?.Videos);
          });

          if (categoryFiterList) {
            _.each(categoryFiterList, item => {
              Videos.push(item.Videos);
            });
          }
          if (Videos.length > 0) {
            _.each(Videos, item => {
              if (item.length > 0) {
                _.each(item, Video => {
                  VideosDetail.push(Video);
                });
              }
            });
          }
          setVideos(VideosDetail);
        }
      })
      .catch(err => console.log(err));
  }, []);
  const {navigation, EventState, SurveyState} = props;
  const cardButtonData = [
    // {
    //   title: 'Standings',
    //   active: true,
    //   navigation: 'StandingScreen',
    //   webviewUrl: null,
    // },
    // {
    //   title: 'Fixture',
    //   active: true,
    //   navigation: 'FixtureScreen',
    //   webviewUrl: null,
    // },

    {
      title: 'History',
      active: true,
      // icon: 'trophy',
      // navigation: 'AchievementsScreen',
      webviewUrl: null,
    },
    {
      title: 'Brazil Fan Token(BFT)',
      active: true,
      // icon: 'camera',
      // navigation: 'AchievementsAndCollections',
      webviewUrl: null,
    },
  ];

  const routeParams = props?.route?.params;
  let Events = [];
  let Surveys = [];

  if (!routeParams?.isAnonymous) {
    if (
      !_.isEmpty(EventState?.ActiveEvents?.All) ||
      !_.isEmpty(EventState?.JoinedEvents?.All) ||
      !_.isEmpty(EventState?.PassedEvents?.All)
    ) {
      const activeEvents = EventState?.ActiveEvents?.All;
      const JoinedEvents = EventState?.JoinedEvents?.All;
      const PassedEvents = EventState?.PassedEvents?.All;
      _.each(activeEvents, item => {
        Events.push(item);
      });
      _.each(JoinedEvents, item => {
        Events.push(item);
      });
      _.each(PassedEvents, item => {
        Events.push(item);
      });
    }
    if (
      !_.isEmpty(SurveyState?.ActiveSurveys?.All) ||
      !_.isEmpty(SurveyState?.PassedSurveys?.All) ||
      !_.isEmpty(SurveyState?.VotedSurveys?.All)
    ) {
      const activeSurveys = SurveyState?.ActiveSurveys?.All;
      const passedSurveys = SurveyState?.PassedSurveys?.All;
      const votedSurveys = SurveyState?.VotedSurveys?.All;
      _.each(activeSurveys, item => {
        Surveys.push(item);
      });
      _.each(passedSurveys, item => {
        Surveys.push(item);
      });
      _.each(votedSurveys, item => {
        Surveys.push(item);
      });
    }
  }
  const UserBalance =
    props?.UserState?.FinanceInfo?.CustomerCoinBalanceDetailList;

  console.log(matchList);
  return (
    <FADrawerHeader
      title={Localization.t('TeamScreen.Team')}
      navigation={navigation}
      routeMain={props.route.name}>
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAFullScreenLoader isLoading={serviceLoading} />
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{
                flex: 1,
                backgroundColor: theme?.colors?.blueGray,
              }}>
              <View style={{marginHorizontal: 10}}></View>
              {/* {matchList !== []
                ? _.map(matchList, item => {
                    return (
                      <>
                        <FATeamResults item={item} />
                      </>
                    );
                  })
                : null} */}
              <View style={{marginTop: 10}}>
                <View
                  style={{
                    backgroundColor: theme?.colors?.white,
                    paddingVertical: 10,
                    borderRadius: 5,
                    paddingHorizontal: 10,
                    marginBottom: 4,
                    marginHorizontal: 10,
                  }}>
                  <FAListHeader
                    theme={theme}
                    resetPadding
                    iconInLocal
                    title={Localization.t('TeamScreen.Fixture')}
                    iconName={'FAPages'}
                    fontColor={generateRandomColor()}
                    onPress={() => navigation.navigate('FixtureScreen', {})}
                  />
                </View>

                {_.map(matchList, item => {
                  return <FAFixtureItem item={item} />;
                })}
              </View>
              <View style={{marginHorizontal: 10, marginVertical: 10}}>
                <View
                  style={{
                    backgroundColor: theme?.colors?.white,
                    paddingVertical: 10,
                    borderRadius: 5,
                    paddingHorizontal: 10,
                  }}>
                  <FAListHeader
                    theme={theme}
                    resetPadding
                    iconInLocal
                    title={Localization.t('TeamScreen.Standings')}
                    iconName={'FAPages'}
                    fontColor={generateRandomColor()}
                    onPress={() => navigation.navigate('StandingScreen', {})}
                  />
                </View>

                <FAStandingsComponent data={standingList} type={0} />
              </View>

              {/* 
              <View
                style={{
                  marginTop: 18,
                  marginBottom: 10,
                  marginHorizontal: 10,
                }}>
                <FlatList
                  numColumns={2}
                  data={cardButtonData}
                  ItemSeparatorComponent={() => (
                    <View style={{marginVertical: 4}} />
                  )}
                  renderItem={({item, index}) => (
                    <View
                      style={{
                        marginLeft: index % 2 === 1 ? 8 : null,
                        flex: 1,
                      }}>
                      <TouchableOpacity
                        // disabled={item.webviewUrl === null ? true : false}
                        onPress={() =>
                          item.webviewUrl === null
                            ? navigation.navigate(item.navigation, {
                                tournaments: tournaments,
                              })
                            : navigation.navigate('WebviewScreen', {
                                url: `${item.webviewUrl}`,
                                title: item.title,
                              })
                        }
                        activeOpacity={0.7}
                        style={{
                          backgroundColor: theme?.colors?.darkBlue,
                          borderRadius: 4,
                        }}>
                        <Text
                          style={{
                            marginLeft: 8,
                            color: 'white',
                            fontSize: 12,
                            marginVertical: 10,
                            letterSpacing: 0.7,
                          }}>
                          {item.title}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                />
              </View> */}
              {!routeParams?.isAnonymous && (
                <>
                  {!_.isEmpty(Surveys) && (
                    <View
                      style={{
                        backgroundColor: theme?.colors?.white,
                        marginHorizontal: 10,
                        borderRadius: 4,
                        paddingHorizontal: 15,
                        paddingVertical: 13,
                        marginTop: 10,
                      }}>
                      <View style={{marginBottom: 13}}>
                        <FAListHeader
                          theme={theme}
                          resetPadding
                          iconInLocal
                          title={Localization.t('CommonsFix.Polls')}
                          iconName={'FASurvey'}
                          fontColor={generateRandomColor()}
                          onPress={() =>
                            navigation.navigate('SurveysScreen', {
                              routeMain: props.route.name,
                            })
                          }
                        />
                      </View>
                      <FASlider.Alternative
                        UserWallets={UserBalance}
                        data={Surveys}
                        buttonText={Localization.t('CommonsFix.Reply')}
                        buttonContainerStyle={{
                          backgroundColor: theme?.colors?.faOrange,
                          borderRadius: 5,
                        }}
                        buttonTextStyle={{
                          marginVertical: 8,
                          marginHorizontal: 16,
                          fontSize: 12,
                        }}
                        textStyle={{color: theme?.colors?.black}}
                        onPress={item =>
                          navigation.navigate('SurveyScreenDetail', {
                            UserBalance,
                            event: item,
                            isAnonymousFlow: routeParams?.isAnonymous,
                            routeMain: props.route.name,
                          })
                        }
                      />
                    </View>
                  )}
                  {!_.isEmpty(Events) && (
                    <View
                      style={{
                        backgroundColor: theme?.colors?.white,
                        marginHorizontal: 10,
                        borderRadius: 4,
                        paddingHorizontal: 15,
                        paddingVertical: 13,
                        marginTop: 10,
                      }}>
                      <View style={{marginBottom: 13}}>
                        <FAListHeader
                          theme={theme}
                          fontColor={generateRandomColor()}
                          resetPadding
                          title={Localization.t('CommonsFix.Events')}
                          iconName={'star'}
                          onPress={() =>
                            navigation.navigate('EventsScreen', {
                              routeMain: props.route.name,
                            })
                          }
                        />
                      </View>
                      <FASlider.Alternative
                        data={Events}
                        buttonText={Localization.t('CommonsFix.Join')}
                        buttonContainerStyle={{
                          backgroundColor: theme?.colors?.faOrange,
                          borderRadius: 5,
                        }}
                        buttonTextStyle={{
                          marginVertical: 8,
                          marginHorizontal: 16,
                          fontSize: 12,
                        }}
                        textStyle={{color: theme?.colors?.black}}
                        onPress={inComingItem =>
                          navigation.navigate('EventDetailScreen', {
                            UserBalance,
                            event: inComingItem,
                            routeMain: props.route.name,
                          })
                        }
                      />
                    </View>
                  )}
                </>
              )}
              {!_.isEmpty(videos) && (
                <View style={{marginHorizontal: 10}}>
                  <View
                    style={{
                      backgroundColor: theme?.colors?.white,
                      paddingVertical: 10,
                      borderRadius: 5,
                      paddingHorizontal: 10,
                      marginTop: 10,
                    }}>
                    <FAListHeader
                      theme={theme}
                      resetPadding
                      iconInLocal
                      title={Localization.t('CommonsFix.RecentlyAddedVideos')}
                      iconName={'FAPages'}
                      fontColor={generateRandomColor()}
                      onPress={() =>
                        navigation.navigate('VideoGalleryTabStackNavigator', {
                          screen: 'FanAppTvScreen',
                        })
                      }
                    />
                  </View>
                  <View style={{marginVertical: 10}}>
                    <FlatList
                      ItemSeparatorComponent={() => (
                        <View style={{marginLeft: 10}} />
                      )}
                      keyExtractor={(item, index) => index}
                      showsHorizontalScrollIndicator={false}
                      data={videos}
                      renderItem={({index, item}) => (
                        <FAFanCard
                          onPress={() =>
                            navigation.navigate('VideoScreen', {
                              video: item,
                              // category: category,
                            })
                          }
                          image={item.ImageUrl || null}
                          text={item.Title}
                          cardRatio={3}
                        />
                      )}
                      horizontal
                    />
                  </View>
                </View>
              )}
            </ScrollView>
          </>
        )}
      </ThemeContext.Consumer>
    </FADrawerHeader>
  );
};

const mapStateToProps = state => {
  return {
    SurveyState: state.SurveyState,
    EventState: state.EventState,
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(TeamScreen);
