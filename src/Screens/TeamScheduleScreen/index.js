import React, {Component} from 'react';
import {View, Text, Dimensions, FlatList} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAColor from '@Commons/FAColor';
import FAPickerSelect from '@Components/Composite/FAPickerSelect';
import {TabBar, TabView} from 'react-native-tab-view';
import autobind from 'autobind-decorator';
import FAShareBox from '@Components/Composite/FAShareBox';
import FAMatchScheduleItems from '@Components/UI/FAMatchScheduleItems';

export default class TeamScheduleScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTournament: 'Tüm Turnuvalar',
      Tournaments: [
        {
          label: 'Tüm Turnuvalar',
          value: 'Tüm Turnuvalar',
        },
        {
          label: 'Türkiye Süper Lig',
          value: 'Türkiye Süper Lig',
        },
        {
          label: 'Şampiyonlar Ligi',
          value: 'Şampiyonlar Ligi',
        },
        {
          label: 'Hazırlık Maçları',
          value: 'Hazırlık Maçları',
        },
      ],

      routes: [
        {
          key: 'General',
          title: 'Genel',
        },
        {
          key: 'Home',
          title: 'İç',
        },
        {
          key: 'Outside',
          title: 'Dış',
        },
      ],
      index: 0,
    };
  }
  @autobind
  renderTabBar(tabProps) {
    return (
      <TabBar
        {...tabProps}
        renderLabel={({route, focused, color}) => (
          <Text
            style={{
              fontSize: 12,
              color: focused ? FAColor.FAOrange : FAColor.DarkBlue,
            }}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={{
          borderColor: FAColor.Gray,
        }}
        indicatorStyle={{
          backgroundColor: FAColor.FAOrange,
        }}
        style={{
          backgroundColor: FAColor.White,
        }}
      />
    );
  }
  @autobind
  renderScene({route}) {
    switch (route.key) {
      case 'General':
        return this.GeneralMatches();
      case 'Home':
        return this.HomeMatches();
      case 'Outside':
        return this.OutsideMatches();
      default:
        return null;
    }
  }
  @autobind
  GeneralMatches() {
    const {selectedTournament} = this.state;
    const {navigation} = this.props;
    const data = [{}, {}, {}, {}, {}, {}];
    return (
      <View style={{flex: 1}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          data={data}
          renderItem={({item, index}) => (
            <FAMatchScheduleItems
              selectedTournament={selectedTournament}
              onPress={() => navigation.navigate('LiveMatchStatics')}
            />
          )}
        />
      </View>
    );
  }
  @autobind
  HomeMatches() {
    const {selectedTournament} = this.state;
    const data = [{}, {}];
    return (
      <View style={{flex: 1}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          data={data}
          renderItem={({item, index}) => (
            <FAMatchScheduleItems selectedTournament={selectedTournament} />
          )}
        />
      </View>
    );
  }
  @autobind
  OutsideMatches() {
    const {selectedTournament} = this.state;
    const data = [{}, {}, {}];
    return (
      <View style={{flex: 1}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          data={data}
          renderItem={({item, index}) => (
            <FAMatchScheduleItems selectedTournament={selectedTournament} />
          )}
        />
      </View>
    );
  }
  render() {
    const initialLayout = {width: Dimensions.get('window').width}.width;
    const {Tournaments, selectedTournament, index, routes} = this.state;
    const {navigation} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: FAColor.White}}>
        <FAStandardHeader title={'Maç Takvimi'} navigation={navigation} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: 8,
            marginVertical: 8,
          }}>
          <View
            style={{
              backgroundColor: FAColor.Orange,
              height: 60,
              width: 60,
              borderRadius: 30,
            }}></View>
          <Text style={{marginLeft: 12}}>Sporting</Text>
        </View>

        <View
          style={{
            backgroundColor: FAColor.SoftGray,
            borderRadius: 4,
            marginHorizontal: 8,
          }}>
          <FAPickerSelect
            nonBorder
            onValueChange={incomingValue =>
              this.setState({
                selectedTournament: incomingValue,
              })
            }
            value={selectedTournament}
            list={Tournaments}
            paddingVertical={9.5}
          />
        </View>

        <TabView
          renderTabBar={this.renderTabBar}
          renderScene={this.renderScene}
          navigationState={{index, routes}}
          onIndexChange={indexValue =>
            this.setState({
              index: indexValue,
            })
          }
          initialLayout={initialLayout}
        />
      </View>
    );
  }
}
