import React from 'react';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {
  Black,
  DarkBlue,
  FAOrange,
  Gray,
  VBlueGray,
  White,
} from '@Commons/FAColor';
import FAButton from '@Components/Composite/FAButton';
import FAService from '@Services';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import FATextInput from '@Components/Composite/FATextInput';
import autobind from 'autobind-decorator';
import _ from 'lodash';
import FABalanceLine from '@Components/UI/FABalanceLine';
import {connect} from 'react-redux';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import {toFixedNoRounding} from '@Commons/FAMath';
import Toast from 'react-native-toast-message';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class TokenBuyScreen extends React.Component {
  constructor(props) {
    super(props);
    this.initialInputValues = {
      amount: null,
      totalPrice: null,
    };
    this.state = {
      isAnonymous: props?.route?.params?.isAnonymous || false,
      tokenInfo: props?.route?.params?.tokenInfo || {},
      selectedCurrency: {},
      isLoading: false,
      input: this.initialInputValues,
      coinBalance: 0.0,
    };
    this.maximumToken = 5000;
  }

  componentWillMount() {
    this.getIeoAssetDetail();
  }

  componentWillReceiveProps(nextProps) {
    const {UserState} = nextProps;
    const {props} = this;
    const {isAnonymous} = this.state;
    if (!isAnonymous) {
      if (
        UserState?.FinanceInfo &&
        props?.UserState?.FinanceInfo &&
        UserState.FinanceInfo !== props?.UserState?.FinanceInfo
      ) {
        this.fillCustomerBalance();
      }
    }
  }

  fillCustomerBalance() {
    const {tokenInfo} = this.state;
    const requestBody = {
      CoinId: tokenInfo?.CoinId || null,
      CurrencyId: 1,
    };
    FAService.GetCoinBalanceDetail(requestBody)
      .then(res => {
        if (res?.data) {
          const resData = res.data;
          this.setState({
            coinBalance: resData.TotalBalance || '',
          });
        }
      })
      .catch(err => console.log('FillCustomerBalance error', err));
  }

  getIeoAssetDetail() {
    const {tokenInfo, isAnonymous} = this.state;
    const {getUserFinanceStateAction} = this.props;
    this.setState({isLoading: true});
    FAService.GetIeoAssetDetail(tokenInfo.CoinId)
      .then(res => {
        if (res?.data) {
          const incomingTokenInfo = res.data;
          const currencyList = incomingTokenInfo?.AssetPriceList || [];
          if (currencyList) {
            const defaultCurrency = _.find(currencyList, {AssetCode: 'BITCI'});
            this.setState({
              selectedCurrency: defaultCurrency || currencyList[0] || {},
            });
          }
          this.setState(
            {
              tokenInfo: incomingTokenInfo,
            },
            () => {
              if (!isAnonymous) {
                getUserFinanceStateAction();
                this.fillCustomerBalance();
              }
            },
          );
        }
        this.setState({isLoading: false});
      })
      .catch(err => {
        console.log('GetIeoAssetDetail error', err);
        this.setState({isLoading: false});
      });
  }

  plusButtonCalculator() {
    const {selectedCurrency, coinBalance} = this.state;
    let maximumBuyableAmount =
      this.maximumToken * parseFloat(selectedCurrency.Price).toFixed(4);
    if (coinBalance > 0) {
      maximumBuyableAmount =
        (this.maximumToken - coinBalance) *
        parseFloat(selectedCurrency.Price).toFixed(4);
    }
    if (selectedCurrency.CustomerBalance >= maximumBuyableAmount) {
      this.calculation(String(maximumBuyableAmount) || '0', null, true);
    } else {
      this.calculation(
        toFixedNoRounding(selectedCurrency.CustomerBalance, 2) || '0',
        null,
        true,
      );
    }
  }

  @autobind
  calculation(incomingTotalPrice, incomingAmount) {
    if (
      (_.isEmpty(incomingAmount) ||
        _.isNil(incomingAmount) ||
        incomingAmount === null) &&
      (_.isEmpty(incomingTotalPrice) ||
        _.isNil(incomingTotalPrice) ||
        incomingTotalPrice === null)
    ) {
      this.setState({
        input: {
          totalPrice: '',
          amount: '',
        },
      });
    } else {
      const {selectedCurrency} = this.state;
      if (
        (_.isEmpty(incomingAmount) || _.isNil(incomingAmount)) &&
        incomingTotalPrice
      ) {
        let calculatedTokenAmount =
          incomingTotalPrice / parseFloat(selectedCurrency?.Price).toFixed(4) ||
          0.0;
        let calculatedTotalPrice = incomingTotalPrice;
        if (calculatedTokenAmount >= this.maximumToken) {
          calculatedTokenAmount = this.maximumToken;
          calculatedTotalPrice =
            this.maximumToken *
              parseFloat(selectedCurrency?.Price).toFixed(4) || 0.0;
        }
        this.setState({
          input: {
            amount: String(calculatedTokenAmount),
            totalPrice: String(calculatedTotalPrice),
          },
        });
      } else if (
        (_.isEmpty(incomingTotalPrice) || _.isNil(incomingTotalPrice)) &&
        incomingAmount
      ) {
        let amount = incomingAmount;
        if (incomingAmount >= this.maximumToken) {
          amount = this.maximumToken;
        }
        let calculatedTotalCost =
          amount * parseFloat(selectedCurrency?.Price).toFixed(4) || 0.0;
        this.setState({
          input: {
            amount: String(amount),
            totalPrice: String(calculatedTotalCost),
          },
        });
      }
    }
  }

  createOrder() {
    const {tokenInfo, selectedCurrency} = this.state;
    let requestBody = {
      CoinId: tokenInfo?.CoinId || null,
      AssetId: selectedCurrency?.AssetId || '',
      AssetTypeId: selectedCurrency?.AssetTypeId || '',
      Amount: this.state.input.amount,
    };

    if (requestBody.Amount) {
      requestBody.Amount = toFixedNoRounding(requestBody.Amount, 2);
    }

    this.setState({isLoading: true});
    FAService.BuyIeoToken(requestBody)
      .then(res => {
        if (res?.data) {
          const resData = res.data;
          if (_.isEmpty(resData.Message) && _.isEmpty(resData.Data)) {
            this.calculation();
            this.getIeoAssetDetail();
          } else {
            Toast.show({
              type: 'error',
              position: 'top',
              text1: 'İşlem Başarısız',
              text2: resData.Message,
              visibilityTime: 750,
              autoHide: true,
              topOffset: 50,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
            this.calculation();
          }
        }
        this.setState({isLoading: false});
      })
      .catch(err => {
        console.log('BuyIeoToken error', err);
        this.setState({isLoading: false});
      });
  }

  generateCurrencyListContainerStyle(item, index, theme) {
    const {selectedCurrency, tokenInfo} = this.state;
    const currencies = tokenInfo?.AssetPriceList || null;
    return {
      backgroundColor:
        item.AssetCode === selectedCurrency?.AssetCode
          ? theme?.colors?.faOrange
          : theme?.colors?.blueGray,
      borderTopLeftRadius: index === 0 ? 4 : 0,
      borderBottomLeftRadius: index === 0 ? 4 : 0,
      borderTopRightRadius: index === currencies?.length - 1 ? 4 : 0,
      borderBottomRightRadius: index === currencies?.length - 1 ? 4 : 0,
    };
  }

  render() {
    const {navigation} = this.props;
    const {tokenInfo, selectedCurrency, isLoading, coinBalance} = this.state;
    return (
      <>
        <FAStandardHeader title={'Token Satın Al'} navigation={navigation} />
        <FAFullScreenLoader isLoading={isLoading} />
        <ThemeContext.Consumer>
          {({theme}) => (
            <ScrollView
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <Image
                source={{
                  uri: `https://borsa.bitci.com/img/coin/${tokenInfo?.CoinCode}.png?v=1`,
                }}
                style={styles.tokenLogo}
              />
              <View style={styles.coinNameAndCoinCodeContainer}>
                <Text style={[styles.coinName, {color: theme?.colors?.black}]}>
                  {tokenInfo?.CoinName}
                </Text>
                <Text
                  style={([styles.coinCode], {color: theme?.colors?.black})}>
                  {tokenInfo?.CoinCode}
                </Text>
              </View>
              <Text
                style={[
                  styles.coinPrice,
                  {color: theme?.colors?.black},
                ]}>{`${FACurrencyAndCoinFormatter(
                selectedCurrency?.Price || '',
                4,
              )} ${selectedCurrency?.AssetCode || ''}`}</Text>
              <View style={styles.currencyListContainer}>
                {tokenInfo?.AssetPriceList?.map((currency, index) => {
                  let containerStyle = this.generateCurrencyListContainerStyle(
                    currency,
                    index,
                    theme,
                  );
                  let textStyle = {
                    color:
                      currency?.AssetCode === selectedCurrency?.AssetCode
                        ? theme?.colors?.black
                        : theme?.colors?.darkBlue,
                  };
                  return (
                    <FAButton
                      key={index}
                      onPress={() =>
                        this.setState({
                          input: this.initialInputValues,
                          selectedCurrency: currency,
                        })
                      }
                      containerStyle={[
                        styles.currencyButtonContainer,
                        {...containerStyle},
                      ]}
                      textStyle={[styles.currencyButtonText, textStyle]}
                      text={currency.AssetCode || ''}
                    />
                  );
                })}
              </View>
              <Text
                style={[styles.saleInformation, {color: theme?.colors?.black}]}>
                {tokenInfo?.CoinCode} almak istediğiniz{' '}
                {selectedCurrency?.AssetCode} miktarını girin
              </Text>

              <View style={styles.totalInputContainer}>
                <FATextInput.Money
                  decimalCount={2}
                  value={this.state.input.totalPrice}
                  placeholder={'Toplam'}
                  currencyOrCoinCode={selectedCurrency?.AssetCode || ''}
                  onPressIncrement={() => this.plusButtonCalculator()}
                  onChangeValue={value => this.calculation(value, null)}
                />
              </View>
              <View style={styles.marginTop10}>
                <FATextInput.Money
                  decimalCount={4}
                  value={this.state.input.amount}
                  placeholder={'Miktar'}
                  currencyOrCoinCode={tokenInfo?.CoinCode || ''}
                  onPressIncrement={() => this.plusButtonCalculator()}
                  onChangeValue={value => this.calculation(null, value)}
                />
              </View>
              <View style={styles.marginTop10}>
                <FABalanceLine
                  onPress={() => this.plusButtonCalculator()}
                  currency={selectedCurrency?.AssetCode || ''}
                  balance={FACurrencyAndCoinFormatter(
                    selectedCurrency?.CustomerBalance || 0.0,
                    2,
                  )}
                />
              </View>
              <View style={styles.marginTop10}>
                <FABalanceLine
                  currency={tokenInfo?.CoinCode || ''}
                  balance={FACurrencyAndCoinFormatter(coinBalance || 0.0, 4)}
                />
              </View>
              <FAButton
                onPress={() => this.createOrder()}
                disabled={this.state.input.totalPrice <= 0.0}
                text={'Satın Al'}
                containerStyle={styles.buyButtonContainer}
                textStyle={styles.buyButtonText}
              />
            </ScrollView>
          )}
        </ThemeContext.Consumer>
      </>
    );
  }
}

export const styles = StyleSheet.create({
  marginTop10: {marginTop: 10},
  mainContainer: {
    flex: 1,
    paddingVertical: 24,
    paddingHorizontal: 10,
  },
  tokenLogo: {
    width: 64,
    height: 64,
    alignSelf: 'center',
  },
  coinNameAndCoinCodeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    justifyContent: 'center',
  },
  coinName: {
    fontSize: 14,
    fontWeight: '500',
    color: Black,
    textAlign: 'center',
    marginRight: 4,
  },
  coinCode: {
    fontSize: 14,
    color: Gray,
    marginLeft: 6,
    textAlign: 'center',
  },
  coinPrice: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Black,
    marginTop: 8,
    textAlign: 'center',
  },
  currencyListContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 16,
    justifyContent: 'center',
  },
  currencyButtonContainer: {
    paddingVertical: 9,
    paddingHorizontal: 21,
  },
  currencyButtonText: {fontSize: 14, fontWeight: '500'},
  saleInformation: {
    fontSize: 14,
    color: Black,
    textAlign: 'center',
    marginTop: 16,
  },
  totalInputContainer: {marginTop: 16},
  buyButtonContainer: {
    backgroundColor: FAOrange,
    width: '100%',
    borderRadius: 4,
    paddingVertical: 12,
    marginTop: 10,
  },
  buyButtonText: {fontSize: 14, fontWeight: '500', color: White},
});

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {getUserFinanceStateAction})(
  TokenBuyScreen,
);
