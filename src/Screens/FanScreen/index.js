import React, {Component} from 'react';
import {ScrollView, View, Text, Image, FlatList, Alert} from 'react-native';
import {AppConstants} from '@Commons/Contants';
import autobind from 'autobind-decorator';
import FAAssetsHeader from '@Components/Composite/FAAssetsHeader';
import FADrawerHeader from '@Components/Composite/FADrawerHeader';
import FASlider from '@Components/UI/FASlider';
import FAListHeader from '@Components/Composite/FAListHeader';
import {styles} from './assets/styles';
import FAAccountScreenIsAnonymousFlow from '@Components/Composite/FAAccountScreenIsAnonymousFlow';
import {getUserFinanceStateAction} from '@GlobalStore/Actions/UserActions';
import {connect} from 'react-redux';
import _ from 'lodash';
import {toFixedNoRounding} from '@Commons/FAMath';
import FAEnums from '@Commons/FAEnums';
import Localization from '@Localization';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import FACustomTabView from '@Components/Composite/FACustomTabView';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAAccountMenuItems from '@Components/Composite/FAAccountMenuItems';
import codePush from 'react-native-code-push';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {deleteAuthorizationHeader} from '@Services';
import {deleteAccessTokenFromStorage} from '@Commons/FAStorage';
import {CommonActions} from '@react-navigation/native';
import {resetState} from '@GlobalStore/Actions/FlowActions/index';
class FanScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getCustomerAllActiveOrdersServiceCallLoading: false,
      activeOrdersSelectedOrderType: FAEnums.BUYFLOW,
      customerAllActiveOrders: [],
      index: 0,
      checkUpdate: false,
      routes: [
        {
          key: 'assets',
          title: Localization.t('FanScreenFix.Assets'),
        },
        {
          key: 'orders',
          title: Localization.t('FanScreenFix.Orders'),
        },
      ],
      isAnonymousFlow: props.MarketState?.isAnonymousFlow,
    };
  }

  componentWillMount() {
    const {isAnonymousFlow} = this.state;
    const {getUserFinanceStateAction} = this.props;

    if (!isAnonymousFlow) {
      getUserFinanceStateAction();
    }
  }

  @autobind
  assetsRender() {
    const {navigation, UserState} = this.props;
    let TotalBalanceValueList;
    if (
      UserState &&
      UserState.FinanceInfo &&
      UserState.FinanceInfo.TotalBalanceValueList &&
      UserState.FinanceInfo.CustomerCoinBalanceDetailList
    ) {
      TotalBalanceValueList = UserState.FinanceInfo.TotalBalanceValueList[0];
    }

    return (
      <View style={{flex: 1}}>
        <FAAssetsHeader
          buttonText={Localization.t('CommonsFix.SeeAll')}
          TotalBalanceValueList={TotalBalanceValueList}
          onPressWalletButton={() =>
            navigation.navigate('WalletTabStackNavigator', {
              screen: 'WalletScreen',
              params: {hideZeroWallets: true},
            })
          }
        />
      </View>
    );
  }

  @autobind
  onIndexChangeHandler(indexValue) {
    if (indexValue === 0 && indexValue !== this.state.index) {
      this.setState({index: indexValue});
    } else if (indexValue === 1) {
      this.setState({index: 0});
      const {navigation} = this.props;
      navigation.navigate('CustomerOrderListScreen');
    }
  }
  checkAppUpdate() {
    this.setState({
      checkAppUpdate: true,
    });
    codePush.checkForUpdate().then(update => {
      if (!update) {
        this.setState({
          checkAppUpdate: false,
        });
        Alert.alert(
          Localization.t('SettingScreenFix.Uptade'),
          Localization.t('SettingScreenFix.AppIsAlreadyUpdate'),
        );
      } else {
        Alert.alert(
          Localization.t('SettingScreenFix.Uptade'),
          Localization.t('SettingScreenFix.UpdateAvailable'),
          [
            {
              text: Localization.t('SettingScreenFix.No'),
              onPress: () => null,
            },
            {
              text: Localization.t('SettingScreenFix.Yes'),
              onPress: () => {
                codePush.sync({
                  installMode: codePush.InstallMode.IMMEDIATE,
                });
              },
            },
          ],
        );
        this.setState({
          checkAppUpdate: false,
        });
      }
    });
  }

  logout() {
    const {resetState: resetStoreState} = this.props;
    resetStoreState();
    deleteAccessTokenFromStorage(this.resetStack());
    deleteAuthorizationHeader();
  }

  resetStack() {
    const {navigation} = this.props;
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'AuthLoaderScreen'}],
      }),
    );
  }

  render() {
    const {isAnonymousFlow, checkUpdate} = this.state;
    const {navigation, UserState, EventState, SurveyState} = this.props;
    let userName;
    let Events = [];
    let Surveys = [];
    let currencyBalance;
    let coinBalance;

    if (!isAnonymousFlow) {
      if (
        UserState &&
        UserState.UserInfo &&
        UserState.UserInfo.NameSurname &&
        UserState.FinanceInfo &&
        UserState.FinanceInfo.TotalBalanceValueList &&
        UserState.FinanceInfo.CustomerCoinBalanceDetailList
      ) {
        userName = UserState.UserInfo.NameSurname;
        // const {CustomerCoinBalanceDetailList} = UserState?.FinanceInfo;
        // const currencyBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        //   CoinId: AppConstants.CurrencyId,
        // });
        // const coinBalanceDetail = _.find(CustomerCoinBalanceDetailList, {
        //   CoinId: AppConstants.CoinId,
        // });
        // currencyBalance =
        //   toFixedNoRounding(currencyBalanceDetail?.TotalBalance, 4) || 0.0;
        // coinBalance =
        //   toFixedNoRounding(coinBalanceDetail?.TotalBalance, 2) || 0.0;
      }
      // if (
      //   !_.isEmpty(EventState?.ActiveEvents?.All) ||
      //   !_.isEmpty(EventState?.JoinedEvents?.All) ||
      //   !_.isEmpty(EventState?.PassedEvents?.All)
      // ) {
      //   const activeEvents = EventState?.ActiveEvents?.All;
      //   const JoinedEvents = EventState?.JoinedEvents?.All;
      //   const PassedEvents = EventState?.PassedEvents?.All;
      //   _.each(activeEvents, item => {
      //     Events.push(item);
      //   });
      //   _.each(JoinedEvents, item => {
      //     Events.push(item);
      //   });
      //   _.each(PassedEvents, item => {
      //     Events.push(item);
      //   });
      // }
      // if (
      //   !_.isEmpty(SurveyState?.ActiveSurveys?.All) ||
      //   !_.isEmpty(SurveyState?.PassedSurveys?.All) ||
      //   !_.isEmpty(SurveyState?.VotedSurveys?.All)
      // ) {
      //   const activeSurveys = SurveyState?.ActiveSurveys?.All;
      //   const passedSurveys = SurveyState?.PassedSurveys?.All;
      //   const votedSurveys = SurveyState?.VotedSurveys?.All;
      //   _.each(activeSurveys, item => {
      //     Surveys.push(item);
      //   });
      //   _.each(passedSurveys, item => {
      //     Surveys.push(item);
      //   });
      //   _.each(votedSurveys, item => {
      //     Surveys.push(item);
      //   });
      // }
    }
    if (isAnonymousFlow) {
      console.log('THOSS', this.props);
      return (
        <ThemeContext.Consumer>
          {({theme}) => (
            <FADrawerHeader
              title={Localization.t('FanScreenFix.Fan')}
              navigation={navigation}
              routeMain={this.props.route.name}>
              <FAAccountScreenIsAnonymousFlow
                onPressLogin={() => navigation.navigate('LoginScreen')}
                onPressSignIn={() => navigation.navigate('RegisterStepOne')}
              />
              <FAAccountMenuItems.AccountParentItems
                leftIconName={'BMAEarthLine'}
                text={Localization.t('SettingScreenFix.SelectLanguage')}
                onPress={() => navigation.navigate('SelectLanguageScreen')}
              />
              <FAAccountMenuItems.AccountParentItems
                disable={checkUpdate}
                leftIconName={'sync'}
                text={
                  checkUpdate
                    ? Localization.t('SettingScreenFix.CheckingForUpdate')
                    : Localization.t('SettingScreenFix.CheckforUpdates')
                }
                onPress={() => this.checkAppUpdate()}
              />
              <Text
                style={{
                  alignSelf: 'center',
                  // marginTop: ,
                  color: theme?.colors?.darkBlue,
                  fontSize: 12,
                }}>
                V1.1.0
              </Text>
            </FADrawerHeader>
          )}
        </ThemeContext.Consumer>
      );
    }

    const randomColor = generateRandomColor();
    const randomColor2 = generateRandomColor();

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <FADrawerHeader
            title={Localization.t('FanScreenFix.Fan')}
            navigation={navigation}
            routeMain={this.props.route.name}>
            <View style={{backgroundColor: theme?.colors?.white, flex: 1}}>
              <ScrollView
                nestedScrollEnabled
                style={[
                  styles.scrollViewBackgrounColor,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <View>
                  <Image
                    style={styles.imageStyle}
                    source={require('./assets/bg.jpg')}
                  />

                  <View style={styles.personalİnformationContainerStyle}>
                    <Text style={styles.helloTextStyle}>
                      {Localization.t('Drawer.Hello')}
                    </Text>
                    <Text
                      style={[
                        styles.nameAndSurnameTextStyle,
                        {textTransform: 'uppercase'},
                      ]}>
                      {userName ? userName : null}
                    </Text>
                  </View>
                </View>

                {/* <View style={styles.flex1}>
                <FACustomTabView
                  theme={theme}
                  scrollEnabled={false}
                  currentIndex={index}
                  onIndexChange={this.onIndexChangeHandler}
                  itemCountPerPage={2}
                  routes={routes}
                  customContainerStyle={{paddingVertical: 16}}
                />
                {index === 0 ? this.assetsRender() : null}
              </View> */}

                {/* <View
                style={{
                  marginTop: 16,
                  marginHorizontal: 10,
                }}>
                <FlatList
                  numColumns={2}
                  data={cardButtonData}
                  ItemSeparatorComponent={() => (
                    <View style={{marginVertical: 4}} />
                  )}
                  renderItem={({item, index}) => (
                    <View
                      style={{marginLeft: index % 2 === 1 ? 8 : null, flex: 1}}>
                      <FACardButton
                        passive={!item.active}
                        text={item.title}
                        iconName={item.icon}
                        onPress={() => navigation.navigate(item.navigation)}
                      />
                    </View>
                  )}
                />
              </View> */}

                {/* <View
                style={[
                  styles.sliderMainContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <View style={{marginBottom: 13}}>
                  <FAListHeader
                    theme={theme}
                    fontColor={randomColor}
                    resetPadding
                    title={Localization.t('CommonsFix.Events')}
                    iconName={'star'}
                    onPress={() => navigation.navigate('EventsScreen')}
                  />
                </View>
                <FASlider.Alternative
                  data={Events}
                  buttonText={Localization.t('CommonsFix.Reply')}
                  buttonContainerStyle={[
                    styles.sliderButtonContainer,
                    {backgroundColor: theme?.colors?.orange},
                  ]}
                  buttonTextStyle={styles.buttonTextStyle}
                  textStyle={{color: theme?.colors?.black}}
                  onPress={inComingItem =>
                    navigation.navigate('EventDetailScreen', {
                      // UserBalance,
                      event: {item: inComingItem},
                    })
                  }
                />
              </View> */}
                {/* <View
                style={[
                  styles.sliderMainContainer,
                  {backgroundColor: theme?.colors?.white},
                ]}>
                <View style={{marginBottom: 13}}>
                  <FAListHeader
                    theme={theme}
                    resetPadding
                    iconInLocal
                    title={Localization.t('CommonsFix.Polls')}
                    iconName={'FASurvey'}
                    fontColor={randomColor2}
                    onPress={() => navigation.navigate('SurveysScreen')}
                  />
                </View>
                <FASlider.Alternative
                  data={Surveys}
                  buttonText={Localization.t('CommonsFix.Reply')}
                  buttonContainerStyle={[
                    styles.sliderButtonContainer,
                    {backgroundColor: theme?.colors?.orange},
                  ]}
                  buttonTextStyle={styles.buttonTextStyle}
                  textStyle={{color: theme?.colors?.black}}
                  onPress={inComingItem =>
                    navigation.navigate('SurveyScreenDetail', {
                      event: {
                        item: inComingItem,
                      },
                      isAnonymousFlow: isAnonymousFlow,
                    })
                  }
                />
              </View> */}
                <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
                  <FAFullScreenLoader
                    isLoading={checkUpdate}
                    text={Localization.t('SettingScreenFix.CheckforUpdates')}
                  />
                  <FAAccountMenuItems.AccountParentItems
                    leftIconName={'BMASetting2Line'}
                    text={Localization.t('SettingScreenFix.AccountSetting')}
                    onPress={() => navigation.navigate('AccountSettings')}
                  />

                  <FAAccountMenuItems.AccountParentItems
                    leftIconName={'BMAEarthLine'}
                    text={Localization.t('SettingScreenFix.SelectLanguage')}
                    onPress={() => navigation.navigate('SelectLanguageScreen')}
                  />
                  <FAAccountMenuItems.AccountParentItems
                    leftIconName={'BMAUsers'}
                    text={Localization.t('SettingScreenFix.References')}
                    onPress={() => navigation.navigate('ReferenceScreen')}
                  />
                  <FAAccountMenuItems.AccountParentItems
                    disable={checkUpdate}
                    leftIconName={'sync'}
                    text={
                      checkUpdate
                        ? Localization.t('SettingScreenFix.CheckingForUpdate')
                        : Localization.t('SettingScreenFix.CheckforUpdates')
                    }
                    onPress={() => this.checkAppUpdate()}
                  />
                  {/* <FAAccountMenuItems.AccountParentItems
                    disable={true}
                    leftIconName={'FAShare'}
                    text={Localization.t('AccountSettingsFix.ShareWithFriend')}
                    // onPress={() => this.checkAppUpdate()}
                  /> */}
                  <FAAccountMenuItems.AccountParentItems
                    disable={false}
                    leftIconName={'BMALogoutBoxLine'}
                    text={Localization.t('Drawer.Logout')}
                    onPress={() => this.logout()}
                  />
                  <Text
                    style={{
                      alignSelf: 'center',
                      marginTop: 10,
                      color: theme?.colors?.darkBlue,
                      fontSize: 12,
                    }}>
                    V1.1.0
                  </Text>
                </View>
              </ScrollView>
            </View>
          </FADrawerHeader>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
    MarketState: state.MarketState,
    // EventState: state.EventState,
    // SurveyState: state.SurveyState,
  };
};

export default connect(mapStateToProps, {
  getUserFinanceStateAction,
  resetState,
})(FanScreen);
