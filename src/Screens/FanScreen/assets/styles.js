import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';
import {VBlueGray, VGray} from '../../../Commons/FAColor';

export const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
    height: 100,
    backgroundColor: 'black',
  },
  personalİnformationContainerStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  helloTextStyle: {fontWeight: '700', color: 'white', fontSize: 12},
  nameAndSurnameTextStyle: {fontWeight: '700', color: 'white'},
  flex1: {flex: 1},
  sliderMainContainer: {
    backgroundColor: 'white',
    marginHorizontal: 10,
    borderRadius: 4,
    paddingHorizontal: 15,
    paddingVertical: 13,
    marginTop: 20,
  },
  sliderButtonContainer: {
    backgroundColor: '#EA5615',
    borderRadius: 5,
  },
  buttonTextStyle: {
    marginVertical: 8,
    marginHorizontal: 16,
    fontSize: 12,
  },
  scrollViewBackgrounColor: {backgroundColor: FAColor.VBlueGray},
  shareBoxContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 4,
  },

  activeOrderScrollView: {marginHorizontal: 10, marginVertical: 16},
  activeOrderHeaderContainer: {flexDirection: 'row', alignItems: 'center'},
  activeBuyOrderTextContainer: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 4,
  },
  activeBuyOrderText: {fontSize: 12, color: FAColor.Black},
  activeSellOrderTextContainer: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 4,
    marginLeft: 8,
  },
  activeSellOrderText: {fontSize: 12, color: FAColor.Black},
  activeOrderLoaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: FAColor.White,
    paddingVertical: 20,
  },
  activeOrderListContainer: {
    backgroundColor: FAColor.White,
    flex: 1,
  },
});
