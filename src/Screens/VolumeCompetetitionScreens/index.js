import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAService from '../../Services';
import FARenderVolumeCompetitionsList from '../../Components/UI/FARenderVolumeCompetitionsList';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import Localization from '@Localization';
import FACustomTabView from '../../Components/Composite/FACustomTabView';
import _ from 'lodash';
const VolumeCompetitions = props => {
  const {navigation} = props;
  const [VolumeCompetitionEvents, setVolumeCompetitionEvents] = useState([]);
  const [tabIndex, settabIndex] = useState(0);
  const routes = [
    {key: 'all', title: Localization.t('VolumeCompetitions.All')},
    {key: 'active', title: Localization.t('VolumeCompetitions.Active')},
    {key: 'Near', title: Localization.t('VolumeCompetitions.Soon')},
  ];
  useEffect(() => {
    getCompetitionsEvents();
  }, []);

  const getCompetitionsEvents = () => {
    FAService.getVolumeCompetitionEvents()
      .then(res => {
        if (res?.data && res.status === 200) {
          setVolumeCompetitionEvents(res.data);
        }
      })
      .catch(err => console.log('Err => ', err));
  };
  console.log('asdasdasdasd', VolumeCompetitionEvents);
  let filteredData;
  if (tabIndex === 0) {
    filteredData = VolumeCompetitionEvents;
  }
  if (tabIndex === 1) {
    filteredData = _.filter(VolumeCompetitionEvents, {
      IsActive: false,
    });
  }
  if (tabIndex === 2) {
    filteredData = null;
  }
  return (
    <>
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
            <FAStandardHeader
              title={Localization.t('VolumeCompetitions.header')}
              navigation={navigation}
            />
            <View style={{flex: 1}}>
              <View>
                <FACustomTabView
                  theme={theme}
                  scrollEnabled={false}
                  currentIndex={tabIndex}
                  onIndexChange={indexValue => settabIndex(indexValue)}
                  itemCountPerPage={3}
                  routes={routes}
                  customContainerStyle={{paddingVertical: 16}}
                />
              </View>
              <FARenderVolumeCompetitionsList
                type={1}
                data={filteredData}
                theme={theme}
                onPress={item =>
                  navigation.navigate('VolumeCompetetitionDetailScreen', {
                    item: item,
                  })
                }
              />
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    </>
  );
};

export default VolumeCompetitions;
