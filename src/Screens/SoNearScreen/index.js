import React from 'react';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import {Text} from 'react-native';
import {DarkBlue} from '@Commons/FAColor';

const StakingScreen = props => {
  return (
    <>
      <FAStandardHeader title={'ÇOK YAKINDA'} navigation={props.navigation} />
      <Text style={{marginVertical: 80, textAlign: 'center', color: DarkBlue}}>
        ÇOK YAKINDA
      </Text>
    </>
  );
};
export default StakingScreen;
