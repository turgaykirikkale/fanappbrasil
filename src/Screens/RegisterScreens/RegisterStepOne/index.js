import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FATextInput from '@Components/Composite/FATextInput';
import FAEnums from '@Commons/FAEnums';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {ThemeContext} from '../../../Utils/Theme/ThemeProvider';
import {styles} from '../RegisterStepOne/assets/styles';
import Localization from '@Localization';
import autobind from 'autobind-decorator';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import {getDataFromStorage} from '../../../Commons/FAStorage';
import FATextRendered from '@Components/UI/FATextRendered';
import FACheckbox from '@Components/Composite/FACheckbox';
import FADivider from '@Components/UI/FADivider';
import FAForm from '@ValidationEngine/Form';
import FAService from '@Services';
import FAButton from '@Components/Composite/FAButton';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';

class RegisterStepOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneCode:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.code
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.countryCode
          : '+55',
      flag:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.flag
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.flag
          : 'Brazil',
      userName: '',
      userSurname: '',
      purePhoneNumber: '',
      fullPhoneNumber: '',
      formattedValue: '',
      userPassword: '',
      userRepeatPassword: '',
      showReferenceInput: false,
      referenceCode: '',
      currentLangCode: null,
      IsUserAgreement: false,
      IsIlluminationText: false,
      IsElectronicCommercialMessage: false,
      FormIsValid: false,
      serviceLoading: false,
      serviceMessage: null,
      showDialogBox: false,
      showPassword: false,
      showRepeatPassword: false,
    };
    this.userAgreementUri = 'https://www.bitci.com/legals/user-agreement/';
    this.privacyPolicyUri = 'https://www.bitci.com/legals/privacy-policy/';
    this.protectionofpersonaldataUri = 'https://www.bitci.com/legals/kvkk/';
    this.explicitConsentStatementUri =
      'https://www.bitci.com/legals/express-consent-statement/';
    this.commercialelectronicmessageUri = 'https://www.bitci.com/legals/iys/';
  }

  async componentWillMount() {
    const currentLangCode = await getDataFromStorage('Language');
    this.setState({
      currentLangCode,
    });
  }

  @autobind
  navigate() {
    this.props.navigation.navigate('CountrySelectScreen', {
      onPhoneSelect: this.onPhoneSelect,
    });
  }
  @autobind
  onPhoneSelect({code, flag}) {
    this.setState({
      phoneCode: code,
      flag: flag,
    });
  }

  @autobind
  onChangePhoneNumber(formatted, extracted) {
    const newPhonePrefix = this.state.phoneCode;
    let generatedPhoneNumberValue = extracted;
    if (extracted.indexOf(newPhonePrefix) > -1) {
      generatedPhoneNumberValue = extracted.replace(newPhonePrefix, '');
    }
    this.setState({
      purePhoneNumber: generatedPhoneNumberValue,
      fullPhoneNumber: extracted,
      phoneCode: newPhonePrefix,
      formattedValue: formatted,
    });
  }
  @autobind
  openProtectionofpersonaldataWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.protectionofpersonaldataUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('LoginScreenFix.KVVKIllumination'),
    });
  }

  @autobind
  openUserAgreementWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.userAgreementUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('LoginScreenFix.UserAgreement'),
    });
  }

  @autobind
  openexplicitConsentStatemenWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.explicitConsentStatementUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('LoginScreenFix.ExplicitConsentText'),
    });
  }

  @autobind
  opencommercialelectronicmessageWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.commercialelectronicmessageUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('LoginScreenFix.CommercialElectronicMessage'),
    });
  }
  @autobind
  readyForService() {
    const {IsUserAgreement, FormIsValid, userPassword, userRepeatPassword} =
      this.state;

    if (IsUserAgreement && FormIsValid) {
      return false;
    }
    return true;
  }
  @autobind
  userRegisterServiceCall() {
    this.setState({serviceLoading: true});
    const {navigation} = this.props;
    const {
      phoneCode,
      userName,
      userSurname,
      purePhoneNumber,
      userPassword,
      referenceCode,
      IsUserAgreement,
      IsIlluminationText,
      IsElectronicCommercialMessage,
    } = this.state;

    const requestBody = {
      CountryCode: phoneCode,
      IsElectronicCommercialMessage: IsElectronicCommercialMessage,
      IsIlluminationText: IsIlluminationText,
      IsUserAgreement: IsUserAgreement,
      Name: userName,
      Password: userPassword,
      Phone: purePhoneNumber,
      Surname: userSurname,
      UtmUrl: null,
      ReffererCode: referenceCode || null,
    };

    FAService.UserRegister(requestBody)
      .then(response => {
        if (
          response &&
          response.data &&
          response.status === 200 &&
          response.data.IsSuccess
        ) {
          this.setState(
            {serviceLoading: false, showApprovePhoneModal: false},
            () => {
              // navigation.navigate('OTPScreen', {
              //   registerUserCridentials: requestBody,
              //   flow: FAEnums.REGISTERFLOW,
              // });

              navigation.navigate('OTPScreen', {
                phone: requestBody.Phone,
                flow: FAEnums.REGISTERFLOW,
                hideBackButton: true,
              });
            },
          );
        } else {
          this.setState({
            showApprovePhoneModal: false,
            serviceLoading: false,
            showDialogBox: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {
        console.log('erro', error);
        this.setState({
          serviceLoading: false,
          showApprovePhoneModal: false,
        });
      });
  }

  render() {
    const {navigation} = this.props;
    const {
      phoneCode,
      flag,
      formattedValue,
      fullPhoneNumber,
      userName,
      userSurname,
      userPassword,
      userRepeatPassword,
      showReferenceInput,
      referenceCode,
      IsUserAgreement,
      IsIlluminationText,
      IsElectronicCommercialMessage,
      serviceLoading,
      serviceMessage,
      showDialogBox,
      purePhoneNumber,
      showPassword,
      showRepeatPassword,
    } = this.state;
    console.log(this.state);
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('LoginScreenFix.CreateAccount')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showDialogBox}
              text={serviceMessage}
              iconName={'times-circle'}
              header={Localization.t('CommonsFix.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <KeyboardAwareScrollView
              showsVerticalScrollIndicator={false}
              style={[
                styles.childContainer,
                {backgroundColor: theme?.colors?.white},
              ]}
              showVerticalScroll>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    FormIsValid: value,
                  })
                }>
                <FATextInput
                  label={Localization.t('CommonsFix.Name')}
                  rule={'notEmptyAndNil'}
                  errorMessage={Localization.t(
                    'LoginScreenFix.nameErrorMessage',
                  )}
                  name={'Name'}
                  value={userName}
                  onChangeValue={value =>
                    this.setState({
                      userName: value,
                    })
                  }
                  placeholder={Localization.t('CommonsFix.Name')}
                />
                <View style={{marginTop: 8}} />
                <FATextInput
                  label={Localization.t('CommonsFix.Surname')}
                  rule={'notEmptyAndNil'}
                  errorMessage={Localization.t(
                    'LoginScreenFix.surnameErrorMessage',
                  )}
                  name={'Surname'}
                  value={userSurname}
                  onChangeValue={value =>
                    this.setState({
                      userSurname: value,
                    })
                  }
                  placeholder={Localization.t('CommonsFix.Surname')}
                />
                <View style={{marginTop: 8}} />
                <FATextInput.PhoneNumber
                  keyboardType={'phone-pad'}
                  rule={
                    phoneCode === '+90'
                      ? 'purePhoneNumberValue'
                      : 'notEmptyAndNil'
                  }
                  phonePrefix={phoneCode}
                  errorMessage={Localization.t(
                    'CreateAccountScreen.CorrectFormatPhone',
                  )}
                  selectedItem={{
                    id: 1,
                    phoneCode: phoneCode,
                    flag: flag,
                  }}
                  placeholder={'555 555 55 55'}
                  label={Localization.t('CommonsFix.Phone')}
                  onChangeValue={(formatted, extracted) =>
                    this.onChangePhoneNumber(formatted, extracted)
                  }
                  value={formattedValue}
                  notFormattedValue={fullPhoneNumber}
                  mask={
                    phoneCode === '+90'
                      ? '[000] [000] [00] [00]'
                      : '[00000000000000]'
                  }
                  onPress={() => this.navigate()}
                />
                <View style={{marginTop: 8}} />
                <FATextInput.Password
                  isPasswordShowing={!showPassword}
                  onPressRightIcon={() =>
                    this.setState({
                      showPassword: !showPassword,
                    })
                  }
                  label={Localization.t('CommonsFix.Password')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t('Commons.PasswordRule')}
                  name={'Şifre Input'}
                  value={userPassword}
                  onChangeValue={value =>
                    this.setState({
                      userPassword: value,
                    })
                  }
                  placeholder={Localization.t('CommonsFix.Password')}
                />
                <View style={{marginTop: 8}} />
                <FATextInput.Password
                  isPasswordShowing={!showRepeatPassword}
                  onPressRightIcon={() =>
                    this.setState({
                      showRepeatPassword: !showRepeatPassword,
                    })
                  }
                  label={Localization.t('CommonsFix.RepeatPassword')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t('CommonsFix.RepeatPassword')}
                  name={'Şifre Input'}
                  value={userRepeatPassword}
                  onChangeValue={value =>
                    this.setState({
                      userRepeatPassword: value,
                    })
                  }
                  placeholder={Localization.t('CommonsFix.RepeatPassword')}
                />
              </FAForm>
              <View style={{marginTop: 12}} />

              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    showReferenceInput: !showReferenceInput,
                  })
                }>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      flexDirection: 'row',
                      borderBottomColor: theme?.colors?.darkBlue,
                    }}>
                    <Text
                      style={{
                        color: theme?.colors?.darkBlue,
                        marginRight: 5,
                      }}>
                      {Localization.t('ReferenceScreenFix.ReferenceCode')}
                    </Text>
                    <FAVectorIcon
                      iconName={'angle-down'}
                      size={18}
                      color={theme?.colors?.darkBlue}
                    />
                  </View>
                </View>
              </TouchableOpacity>
              {showReferenceInput ? (
                <>
                  <View style={{marginTop: 8}} />
                  <FATextInput
                    name={'ReferenceCode'}
                    value={referenceCode}
                    onChangeValue={value =>
                      this.setState({
                        referenceCode: value,
                      })
                    }
                    placeholder={Localization.t(
                      'ReferenceScreenFix.ReferenceCode',
                    )}
                  />
                </>
              ) : null}
              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsUserAgreement}
                  onPress={() =>
                    this.setState({
                      IsUserAgreement: !IsUserAgreement,
                    })
                  }
                />
                <FATextRendered
                  checked={IsUserAgreement}
                  rightText={Localization.t('LoginScreenFix.kvkkTexts')}
                  boldSentences={[
                    Localization.t('LoginScreenFix.KVVKIllumination'),
                    Localization.t('LoginScreenFix.UserAgreement'),
                  ]}
                  onPressBoldSentences={[
                    () => this.openProtectionofpersonaldataWebView(),
                    () => this.openUserAgreementWebView(),
                  ]}
                />
              </View>
              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsIlluminationText}
                  onPress={() =>
                    this.setState({
                      IsIlluminationText: !IsIlluminationText,
                    })
                  }
                />
                <View style={{marginTop: 16}} />

                <FATextRendered
                  checked
                  rightText={Localization.t(
                    'LoginScreenFix.explicitConsentText',
                  )}
                  boldSentences={[
                    Localization.t('LoginScreenFix.ExplicitConsentText'),
                  ]}
                  onPressBoldSentences={[
                    () => this.openexplicitConsentStatemenWebView(),
                  ]}
                />
              </View>
              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsElectronicCommercialMessage}
                  onPress={() =>
                    this.setState({
                      IsElectronicCommercialMessage:
                        !IsElectronicCommercialMessage,
                    })
                  }
                />
                <View style={styles.marginTop16} />

                <FATextRendered
                  checked
                  rightText={Localization.t(
                    'LoginScreenFix.commercialelectronicText',
                  )}
                  boldSentences={[
                    Localization.t(
                      'LoginScreenFix.CommercialElectronicMessage',
                    ),
                  ]}
                  onPressBoldSentences={[
                    () => this.opencommercialelectronicmessageWebView(),
                  ]}
                />
              </View>
              <View style={{marginTop: 8}} />
              <FAButton
                disabled={this.readyForService()}
                onPress={() => this.userRegisterServiceCall()}
                // onPress={() => {
                //   navigation.navigate('OTPScreen', {
                //     phone: purePhoneNumber,
                //     flow: FAEnums.REGISTERFLOW,
                //     hideBackButton: true,
                //   });
                // }}
                containerStyle={[
                  styles.createAccountButtonContainer,
                  {backgroundColor: theme?.colors?.success},
                ]}
                textStyle={[
                  styles.createAccountButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('LoginScreenFix.CreateAccount')}
              />

              <FADivider text={Localization.t('CommonsFix.Or')} />
              <FAButton
                disabled={false}
                onPress={() => navigation.navigate('LoginScreen')}
                containerStyle={[
                  styles.loginButtonContainer,
                  {backgroundColor: theme?.colors?.gray},
                ]}
                textStyle={[
                  styles.loginButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('CommonsFix.Login')}
              />
            </KeyboardAwareScrollView>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default RegisterStepOne;
