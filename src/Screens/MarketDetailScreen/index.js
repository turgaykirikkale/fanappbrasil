import FACoinHeadDetail from '@Components/Composite/FACoinHeadDetail';
import {DarkBlue, Orange, Green, Red} from '@Commons/FAColor';
import {orderBookDataRenderer} from '@Commons/FAStreamLogic';
import FASorting from '@Components/Composite/FASorting';
import FAOrderLine from '@Components/UI/FAOrderLine';
import {TabBar, TabView} from 'react-native-tab-view';
import FAButton from '@Components/Composite/FAButton';
import autobind from 'autobind-decorator';
import FAEnums from '@Commons/FAEnums';
import {styles} from './assets/styles';
import {connect} from 'react-redux';
import FAService from '@Services';
import moment from 'moment';
import React from 'react';
import _ from 'lodash';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {
  subscribeOrderBookTickerChannel,
  subscribeMarketHistoryChannel,
  unSubscribeMarketHistoryChannel,
  subscribeCoinMarketPriceTicker,
  unSubscribeCoinMarketPriceTicker,
} from '@Stream';
import {toFixedNoRounding} from '@Commons/FAMath';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
// import FAHeader from '../../Components/Composite/BMAHeader';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAOrderBook from '@Components/Composite/FAOrderBook';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FACustomTabView from '../../Components/Composite/FACustomTabView';
import Localization from '@Localization';

//LOCALİZATİON YAPILACAK

class MarketDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ordersLoading: false,
      marketHistoryLoading: false,
      coinRatesLoading: true,
      pairDetail: props?.route?.params?.SelectedPair || {},
      buyOrderBook: [],
      sellOrderBook: [],
      marketHistoryList: [],
      selectedCoinCode: null,
      selectedCurrencyCode: null,
      MarketQueueName: null,
      currentCoinData: {},
      filterLogic: {
        date: false,
        amount: false,
        price: false,
      },
      index: 0,
      routes: [
        {
          key: 'orders',
          title: Localization.t('MarketDetail.Orders'),
        },
        {
          key: 'marketHistory',
          title: Localization.t('MarketDetail.MarketHistory'),
        },
        // {
        //   key: 'moreInformation',
        //   title: BMALocalization.t('MarketDetail.MoreInformation'),
        // },
      ],
    };
  }

  @autobind
  getMarketQueueNameAndFillScreenData() {
    const {MarketState} = this.props;
    if (MarketState && MarketState.SelectedPair) {
      const {SelectedPair} = MarketState;
      const {CoinId, CurrencyId} = SelectedPair;
      const requestBody = {
        CurrencyId: CurrencyId,
        CoinId: CoinId,
      };
      FAService.GetMarketInformation(requestBody)
        .then(response => {
          if (response && response.data) {
            this.setState(
              {
                MarketQueueName: response.data.MarketQueueName,
              },
              () => {
                this.fillScreenData();
              },
            );
          }
        })
        .catch(error => {});
    }
  }

  @autobind
  fillScreenData() {
    this.callGetCoinRatesService();
    this.fillOrderBookByPair();
    this.fillMarketHistory();
  }

  componentWillMount() {
    const {MarketState, navigation} = this.props;
    if (
      MarketState &&
      MarketState.SelectedCoinCode &&
      MarketState.SelectedCurrencyCode
    ) {
      const selectedCoinCode = MarketState.SelectedCoinCode,
        selectedCurrencyCode = MarketState.SelectedCurrencyCode;
      this.setState(
        {
          selectedCoinCode,
          selectedCurrencyCode,
        },
        () => {
          this.getMarketQueueNameAndFillScreenData();
        },
      );
    }
    this._unsubscribe = navigation.addListener('blur', () => {
      unSubscribeCoinMarketPriceTicker();
    });
  }

  componentWillReceiveProps(nextProps) {
    const {MarketState} = nextProps;
    const {selectedCoinCode, selectedCurrencyCode} = this.state;
    if (
      MarketState &&
      MarketState.SelectedCoinCode &&
      MarketState.SelectedCurrencyCode &&
      (MarketState.SelectedCoinCode !== selectedCoinCode ||
        MarketState.SelectedCurrencyCode !== selectedCurrencyCode)
    ) {
      const selectedCoinCode = MarketState.SelectedCoinCode,
        selectedCurrencyCode = MarketState.SelectedCurrencyCode;
      this.setState(
        {
          selectedCoinCode,
          selectedCurrencyCode,
        },
        () => {
          this.getMarketQueueNameAndFillScreenData();
        },
      );
    }
  }

  componentWillUnmount() {
    this._unsubscribe();
    unSubscribeMarketHistoryChannel();
  }

  callGetCoinRatesService() {
    const {selectedCoinCode, selectedCurrencyCode} = this.state;
    this.setState({
      coinRatesLoading: true,
    });
    FAService.GetCoinRates()
      .then(res => {
        if (res && res.status === 200 && res.data) {
          const currentCoinData = _.find(res.data, {
            CoinCode: selectedCoinCode,
            CurrencyCode: selectedCurrencyCode,
          });
          if (!_.isNil(currentCoinData)) {
            this.setState(
              {
                currentCoinData: currentCoinData,
              },
              () => {
                subscribeCoinMarketPriceTicker(this.handleCoinInfo);
              },
            );
          }
        }
        this.setState({
          coinRatesLoading: false,
        });
      })
      .catch(err =>
        this.setState({
          coinRatesLoading: false,
        }),
      );
  }

  @autobind
  handleCoinInfo(data) {
    const {selectedCoinCode, selectedCurrencyCode, currentCoinData} =
      this.state;
    if (data && data.CoinCode) {
      let currentCoinInfo = currentCoinData;
      if (
        data.CoinCode === selectedCoinCode &&
        data.CurrencyCode === selectedCurrencyCode
      ) {
        currentCoinInfo = data;
      }
      this.setState({
        currentCoinData: currentCoinInfo,
      });
    }
  }

  fillOrderBookByPair() {
    const {selectedCoinCode, selectedCurrencyCode, MarketQueueName} =
      this.state;
    const requestBody = {
      selectedCoinCode: selectedCoinCode,
      selectedCurrencyCode: selectedCurrencyCode,
    };
    this.setState({
      ordersLoading: true,
    });
    FAService.GetActiveOrders(requestBody)
      .then(response => {
        if (response && response.data) {
          let buyOrders = _.sortBy(
            _.filter(response.data, {Type: FAEnums.BUYFLOW}),
            'Price',
          ).reverse();
          let sellOrders = _.sortBy(
            _.filter(response.data, {Type: FAEnums.SELLFLOW}),
            'Price',
          );

          this.setState(
            {
              buyOrderBook: percentExecutor(buyOrders),
              sellOrderBook: percentExecutor(sellOrders),
            },
            () => {
              subscribeOrderBookTickerChannel(
                MarketQueueName,
                this.orderChangeHandler,
              );
            },
          );
        }

        this.setState({
          ordersLoading: false,
        });
      })
      .catch(error => {
        this.setState({
          ordersLoading: false,
        });
      });
  }

  @autobind
  orderChangeHandler(data) {
    let newBuyOrderBook = this.state.buyOrderBook;
    let newSellOrderBook = this.state.sellOrderBook;
    if (data && data.b && data.s) {
      const orderBook = orderBookDataRenderer(data);
      newBuyOrderBook = orderBook.BuyOrderBook;
      newSellOrderBook = orderBook.SellOrderBook;
    }

    this.setState({
      buyOrderBook: newBuyOrderBook,
      sellOrderBook: newSellOrderBook,
    });
  }

  fillMarketHistory() {
    const {selectedCoinCode, selectedCurrencyCode, MarketQueueName} =
      this.state;
    const requestBody = {
      selectedCoinCode: selectedCoinCode,
      selectedCurrencyCode: selectedCurrencyCode,
    };
    this.setState({
      marketHistoryLoading: true,
    });
    FAService.GetMarketHistory(requestBody)
      .then(response => {
        if (response && response.status === 200 && response.data) {
          const marketHistoryList = _.slice(response.data, 0, 10);

          this.setState({marketHistoryList: marketHistoryList}, () => {
            subscribeMarketHistoryChannel(
              MarketQueueName,
              this.marketHistoryHandler,
            );
          });
        }
        this.setState({
          marketHistoryLoading: false,
        });
      })
      .catch(error =>
        this.setState({
          marketHistoryLoading: false,
        }),
      );
  }
  @autobind
  marketHistoryHandler(data) {
    let tradeList = [];
    const socketHistoryData = data.split('|');
    console.log('socketHistoryDataaaaaaa', socketHistoryData);
    if (socketHistoryData) {
      _.map(socketHistoryData, item => {
        const tradeItem = item.split(',');
        const coinValue = Number(tradeItem[2]);
        const price = Number(tradeItem[1]);
        let trade = {
          CreatedOn: String(tradeItem[0]),
          Price: Number(tradeItem[1]),
          CoinValue: Number(tradeItem[2]),
          MatchType: Number(tradeItem[3]),
          Total: coinValue * price,
          Id: tradeItem[4],
        };
        tradeList.push(trade);
      });
    }
    tradeList = _.slice(tradeList, 0, 10);
    console.log(tradeList, 'AAATradeList');
    this.setState({
      marketHistoryList: tradeList,
    });
  }

  @autobind
  renderTabBar(props) {
    return (
      <TabBar
        // scrollEnabled={true}
        {...props}
        renderLabel={({route, focused, color}) => (
          <Text
            style={[styles.tabBarLabel, {color: focused ? Orange : DarkBlue}]}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={styles.tabBarContentContainer}
        indicatorStyle={styles.tabBarIndicator}
        style={styles.tabBarStyle}
      />
    );
  }

  @autobind
  onIndexChangeHandler(incomingIndex) {
    this.setState({index: incomingIndex});
    if (incomingIndex === 0) {
      unSubscribeMarketHistoryChannel();
      this.fillOrderBookByPair();
    } else if (incomingIndex === 1) {
      this.fillMarketHistory();
    } else if (incomingIndex === 2) {
      unSubscribeMarketHistoryChannel();
    }
  }

  @autobind
  filterAction(marketHistoryList) {
    if (marketHistoryList && marketHistoryList.length > 0) {
      let filteredMarketHistoryList = marketHistoryList;
      const {filterLogic} = this.state;
      const {date, amount, price} = filterLogic;

      if (date !== undefined) {
        if (date) {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'CreatedOn',
          );
        } else {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'CreatedOn',
          ).reverse();
        }
      } else if (amount !== undefined) {
        if (amount) {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'CoinValue',
          );
        } else {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'CoinValue',
          ).reverse();
        }
      } else if (price !== undefined) {
        if (price) {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'Price',
          );
        } else {
          filteredMarketHistoryList = _.sortBy(
            filteredMarketHistoryList,
            'Price',
          ).reverse();
        }
      }

      return filteredMarketHistoryList;
    }
    return marketHistoryList;
  }

  @autobind
  renderOrdersTab(theme) {
    const {
      selectedCoinCode,
      selectedCurrencyCode,
      buyOrderBook,
      sellOrderBook,
      pairDetail,
      ordersLoading,
    } = this.state;
    let currencyDecimalCount = 4;
    let coinDecimalCount = 8;
    if (!_.isNil(pairDetail)) {
      const {PriceDecimal, CoinDecimal} = pairDetail;
      currencyDecimalCount = PriceDecimal || currencyDecimalCount;
      coinDecimalCount = CoinDecimal || coinDecimalCount;
    }

    if (ordersLoading) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator color={theme?.colors?.orange} size={'small'} />
        </View>
      );
    }

    if (!buyOrderBook || !buyOrderBook.length || buyOrderBook.length <= 0) {
      return null;
    }

    return (
      <View style={{marginBottom: 72, backgroundColor: theme?.colors?.white}}>
        <View
          style={[
            styles.orderBookHead,
            {backgroundColor: theme?.colors?.blueGray},
          ]}>
          <Text
            style={[
              styles.orderBookHeadTitle,
              {color: theme?.colors?.darkBlue},
            ]}>
            {Localization.t('MarketDetail.BuyOrders')}
          </Text>
          <Text
            style={[
              styles.orderBookHeadTitle,
              {color: theme?.colors?.darkBlue},
            ]}>
            {Localization.t('MarketDetail.SellOrders')}
          </Text>
        </View>
        <View style={styles.orderBookAmountAndPriceTitle}>
          <View style={styles.orderBookAmountAndPriceTitleContainer}>
            <Text
              style={[
                styles.orderBookAmountAndPriceText,
                {color: theme?.colors?.darkBlack},
              ]}>
              {Localization.t('MarketDetail.Amount')}
            </Text>
            <Text
              style={[
                styles.orderBookCurrencyAndCoinCodeText,
                {color: theme?.colors?.lightGray},
              ]}>
              ({selectedCoinCode || ''})
            </Text>
          </View>
          <View style={styles.orderBookAmountAndPriceTitleContainer}>
            <Text
              style={[
                styles.orderBookAmountAndPriceText,
                {color: theme?.colors?.darkBlack},
              ]}>
              {Localization.t('MarketDetail.Price')}
            </Text>
            <Text
              style={[
                styles.orderBookCurrencyAndCoinCodeText,
                {color: theme?.colors?.lightGray},
              ]}>
              ({selectedCurrencyCode || ''})
            </Text>
          </View>
          <View style={styles.orderBookAmountAndPriceTitleContainer}>
            <Text
              style={[
                styles.orderBookAmountAndPriceText,
                {color: theme?.colors?.darkBlack},
              ]}>
              {Localization.t('MarketDetail.Amount')}
            </Text>
            <Text
              style={[
                styles.orderBookCurrencyAndCoinCodeText,
                {color: theme?.colors?.lightGray},
              ]}>
              ({selectedCoinCode || ''})
            </Text>
          </View>
        </View>
        <View
          style={[
            styles.orderBookContainerWrapper,
            {backgroundColor: theme?.colors?.vGray},
          ]}>
          <View style={styles.flex1}>
            <FAOrderBook
              theme={theme}
              data={buyOrderBook && buyOrderBook.slice(0, 15)}
              onPress={() => {}}
              volumeBarDirection={'right'}
            />
          </View>
          <View style={styles.flex1}>
            <FAOrderBook
              theme={theme}
              data={sellOrderBook && sellOrderBook.slice(0, 15)}
              reverse
              volumeBarDirection={'left'}
              onPress={() => {}}
            />
          </View>
        </View>
      </View>
    );
  }

  @autobind
  renderMarketHistoryTab(theme) {
    const {marketHistoryList, filterLogic, pairDetail, marketHistoryLoading} =
      this.state;

    if (marketHistoryLoading) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator color={theme?.colors?.orange} size={'small'} />
        </View>
      );
    }

    if (
      !marketHistoryList ||
      !marketHistoryList.length ||
      marketHistoryList.length <= 0
    ) {
      return null;
    }

    const filteredMarketHistoryList = this.filterAction(marketHistoryList);
    let currencyDecimalCount = 4;
    let coinDecimalCount = 8;
    if (!_.isNil(pairDetail)) {
      const {PriceDecimal, CoinDecimal} = pairDetail;
      currencyDecimalCount = PriceDecimal || currencyDecimalCount;
      coinDecimalCount = CoinDecimal || coinDecimalCount;
    }
    return (
      <View style={{marginBottom: 60}}>
        <View style={styles.marketHistorySortingContainer}>
          <FASorting
            theme={theme}
            onPress={flag =>
              this.setState(
                {
                  filterLogic: {
                    date: flag,
                    amount: undefined,
                    price: undefined,
                  },
                },
                () => this.filterAction(),
              )
            }
            initialValue={true}
            value={filterLogic.date}
            containerStyle={[
              styles.dateSorting,
              {backgroundColor: theme?.colors?.vGray},
            ]}
            text={Localization.t('FilterLogic.DateText')}
          />
          <FASorting
            theme={theme}
            onPress={flag =>
              this.setState(
                {
                  filterLogic: {
                    date: undefined,
                    amount: flag,
                    price: undefined,
                  },
                },
                () => this.filterAction(),
              )
            }
            value={filterLogic.amount}
            containerStyle={[
              styles.amountSorting,
              {backgroundColor: theme?.colors?.vGray},
            ]}
            text={Localization.t('FilterLogic.AmountText')}
          />
          <FASorting
            theme={theme}
            onPress={flag =>
              this.setState(
                {
                  filterLogic: {
                    date: undefined,
                    amount: undefined,
                    price: flag,
                  },
                },
                () => this.filterAction(),
              )
            }
            value={filterLogic.price}
            containerStyle={[
              styles.priceSorting,
              {backgroundColor: theme?.colors?.vGray},
            ]}
            text={Localization.t('FilterLogic.PriceText')}
          />
        </View>
        <FlatList
          nestedScrollEnabled
          ItemSeparatorComponent={() => (
            <View
              style={[
                styles.marketHistoryFlatListSeperator,
                {borderColor: theme?.colors?.vGray},
              ]}
            />
          )}
          keyExtractor={({item, index}) => index}
          data={filteredMarketHistoryList}
          renderItem={({item, index}) => (
            <View key={index} style={styles.marketHistoryMainContainer}>
              <View style={styles.marketHistoryDateContainer}>
                <Text style={styles.marketHistoryDateText}>
                  {moment(item.CreatedOn).format('DD.MM.YYYY')}
                </Text>
                <Text style={styles.marketHistoryHourText}>
                  {moment(item.CreatedOn).format('HH:mm')}
                </Text>
              </View>
              <Text style={styles.marketHistoryCoinValue}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(item.CoinValue || '', coinDecimalCount),
                  coinDecimalCount,
                )}
              </Text>
              <Text
                style={[
                  styles.marketHistoryCoinPrice,
                  {color: item.MatchType === FAEnums.BUYFLOW ? Green : Red},
                ]}>
                {FACurrencyAndCoinFormatter(
                  toFixedNoRounding(item.Price, currencyDecimalCount),
                  currencyDecimalCount,
                )}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }

  renderMoreInformationTab() {}

  navigateToStockScreen(flow) {
    const {navigation} = this.props;
    const {pairDetail} = this.state;
    navigation.navigate('TradeTabStackNavigator', {params: {pairDetail, flow}});
  }

  render() {
    const {navigation} = this.props;
    const {currentCoinData, index, routes, pairDetail, coinRatesLoading} =
      this.state;

    const initialLayout = {width: Dimensions.get('window').width};
    let currencyDecimalCount = 4;
    if (!_.isNil(pairDetail)) {
      currencyDecimalCount = pairDetail.PriceDecimal || currencyDecimalCount;
    }

    let coinCode = '',
      currencyCode = '';
    if (!_.isEmpty(pairDetail)) {
      coinCode = pairDetail.CoinCode;
      currencyCode = pairDetail.CurrencyCode;
    }
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.generalSafeAreaView,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={`${coinCode}/${currencyCode}`}
              navigation={navigation}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
              <FACoinHeadDetail
                theme={theme}
                isLoading={coinRatesLoading}
                currencyDecimalCount={currencyDecimalCount}
                Price={
                  currentCoinData && currentCoinData.Price
                    ? currentCoinData.Price
                    : '0.0'
                }
                Change24H={
                  currentCoinData && currentCoinData.Change24H
                    ? currentCoinData.Change24H
                    : '0.0'
                }
                Highest={
                  currentCoinData && currentCoinData.High24H
                    ? currentCoinData.High24H
                    : '0.0'
                }
                Lowest={
                  currentCoinData && currentCoinData.Low24H
                    ? currentCoinData.Low24H
                    : '0.0'
                }
                BTCVolume={
                  currentCoinData && currentCoinData.Volume24H
                    ? currentCoinData.Volume24H
                    : '0.0'
                }
                coinCode={
                  currentCoinData && currentCoinData.CoinCode
                    ? currentCoinData.CoinCode
                    : '0.0'
                }
              />

              <FACustomTabView
                scrollEnabled={false}
                theme={theme}
                currentIndex={index}
                routes={routes}
                onIndexChange={indexValue =>
                  this.onIndexChangeHandler(indexValue)
                }
                customContainerStyle={{paddingVertical: 14}}
              />
              {index === 0 ? this.renderOrdersTab(theme) : null}
              {index === 1 ? this.renderMarketHistoryTab(theme) : null}
              {index === 2 ? this.renderMoreInformationTab(theme) : null}
            </ScrollView>
            <View style={{position: 'relative'}}>
              <View
                style={{
                  backgroundColor: theme?.colors?.vGray,
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingVertical: 9,
                  flex: 1,
                }}>
                <FAButton
                  onPress={() => this.navigateToStockScreen(1)}
                  text={Localization.t('Commons.BuyText')}
                  containerStyle={{
                    backgroundColor: theme?.colors?.green,
                    paddingVertical: 15,
                    flex: 1,
                    marginHorizontal: 10,
                    borderRadius: 4,
                  }}
                  textStyle={{color: theme?.colors?.white2, fontSize: 14}}
                />
                <FAButton
                  onPress={() => this.navigateToStockScreen(2)}
                  text={Localization.t('Commons.SellText')}
                  containerStyle={{
                    backgroundColor: theme?.colors?.red,
                    paddingVertical: 15,
                    flex: 1,
                    marginHorizontal: 10,
                    borderRadius: 4,
                  }}
                  textStyle={{color: theme?.colors?.white2, fontSize: 14}}
                />
              </View>
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const mapStateToProps = state => {
  return {
    MarketState: state.MarketState,
  };
};
export default connect(mapStateToProps)(MarketDetailScreen);
