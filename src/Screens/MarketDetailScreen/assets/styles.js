import {StyleSheet} from 'react-native';
import {
  BlueGray,
  DarkBlue,
  DarkGray,
  Gray,
  Orange,
  VGray,
  White,
} from '../../../Commons/FAColor';

export const styles = StyleSheet.create({
  flex1: {flex: 1},
  loaderContainer: {
    paddingVertical: 50,
    flex: 1,
    justifyContent: 'center',
    marginTop: 16,
    alignItems: 'center',
  },
  orderBookHead: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingVertical: 15,
    backgroundColor: BlueGray,
  },
  orderBookHeadTitle: {
    flex: 1,
    color: DarkBlue,
    fontSize: 12,
    textAlign: 'center',
  },
  orderBookAmountAndPriceTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 6,
  },
  orderBookAmountAndPriceTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  orderBookAmountAndPriceText: {color: '#3B3B3B', fontSize: 12},
  orderBookCurrencyAndCoinCodeText: {
    color: '#9B9B9B',
    fontSize: 10,
    marginLeft: 3,
  },
  orderBookContainerWrapper: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  marketHistoryFlatListSeperator: {
    borderWidth: 1,
    //TODO Renk statik verildi. Değiştirilecek
    borderColor: '#F5F6FA',
  },
  marketHistoryMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 12,
  },
  marketHistoryDateContainer: {flex: 1},
  marketHistoryDateText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryHourText: {
    fontSize: 12,
    color: DarkGray,
    marginRight: 15,
  },
  marketHistoryCoinValue: {
    fontSize: 12,
    color: DarkGray,
    flex: 1,
    textAlign: 'right',
  },
  marketHistoryCoinPrice: {
    fontSize: 12,
    flex: 1,
    textAlign: 'right',
    marginRight: 8,
  },
  marketHistorySortingContainer: {flexDirection: 'row', alignItems: 'center'},
  generalSafeAreaView: {flex: 1, backgroundColor: White},
  tabBarStyle: {backgroundColor: White},
  tabBarLabel: {
    margin: 0,
    fontSize: 12,
  },
  tabBarContentContainer: {borderColor: Gray},
  tabBarIndicator: {backgroundColor: Orange},
  dateSorting: {
    flex: 1,
    paddingLeft: 10,
    backgroundColor: VGray,
    paddingVertical: 10,
  },
  amountSorting: {
    flex: 1,
    backgroundColor: VGray,
    paddingVertical: 10,
    alignItems: 'flex-end',
  },
  priceSorting: {
    flex: 1,
    backgroundColor: VGray,
    paddingVertical: 10,
    alignItems: 'flex-end',
    paddingRight: 12,
  },
});
