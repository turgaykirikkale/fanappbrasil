import React, {Component} from 'react';
import {Text, View} from 'react-native';
import FATextInput from '@Components/Composite/FATextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FAButton from '@Components/Composite/FAButton';
import FAForm from '@ValidationEngine/Form';
import FAService from '@Services';
import Toast from 'react-native-toast-message';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import {styles} from './assets/styles';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import {deleteAuthorizationHeader} from '@Services';
import _ from 'lodash';
import FAExchangeOrderTypePicker from '../../Components/Composite/FAExchangeOrderTypePicker';

export default class PasswordGenerate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPassord: '',
      newPassword: '',
      repeatNewPassword: '',
      formIsValid: false,
      showDialogBox: false,
      serviceMessage: null,
      serviceLoading: false,

      passwordChangeDurationOptions: [],
      selectedPasswordChangeDuration: null,
      currentPassordShow: false,
      newPasswordShow: false,
      repeatNewPassworShow: false,
    };

    this.CustomerInfo = props?.route?.params?.CustomerInfo;
  }

  componentWillMount() {
    FAService.PasswordChangeDuration()
      .then(response => {
        if (response && response.data) {
          let passwordChangeDurationOptions = [];
          _.each(response.data, item => {
            const generatedItem = {
              label:
                String(item) +
                ' ' +
                Localization.t('PasswordChangeDuration.month'),
              value: String(item),
            };
            passwordChangeDurationOptions.push(generatedItem);
          });
          this.setState({
            passwordChangeDurationOptions: passwordChangeDurationOptions,
            selectedPasswordChangeDuration: '3',
          });
        }
      })
      .catch(error => {});
  }

  componentDidMount() {
    const {navigation} = this.props;
    this._unsubscribe = navigation.addListener('beforeRemove', () => {
      if (this.CustomerInfo) {
        deleteAuthorizationHeader();
      }
    });
  }

  componentWillUnmount() {
    if (this.CustomerInfo) {
      deleteAuthorizationHeader();
    }
    this._unsubscribe();
  }

  callChangePasswordService() {
    const {
      currentPassord,
      newPassword,
      repeatNewPassword,
      selectedPasswordChangeDuration,
    } = this.state;
    this.setState({serviceLoading: true});

    if (currentPassord === newPassword) {
      this.setState({
        showDialogBox: true,
        serviceMessage: Localization.t('PasswordGenerate.NotSame'),
        serviceLoading: false,
      });
    } else {
      let requestBody = {
        Password: currentPassord,
        NewPassword: newPassword,
        RepeatNewPassword: repeatNewPassword,
      };

      if (!_.isNil(selectedPasswordChangeDuration)) {
        requestBody.PasswordChangeDuration = parseInt(
          selectedPasswordChangeDuration,
        );
      }
      FAService.ChangePassword(requestBody)
        .then(response => {
          if (response && response.data && !response.data.IsSuccess) {
            this.setState({
              serviceLoading: false,
              showDialogBox: true,
              serviceMessage: response.data.Message,
            });
          } else {
            this.setState({
              serviceLoading: false,
            });
            Toast.show({
              type: 'success',
              position: 'top',
              text1: Localization.t('Commons.Succes'),
              text2: Localization.t('Commons.SuccessfulTransaction'),
              visibilityTime: 750,
              autoHide: true,
              topOffset: 70,
              bottomOffset: 40,
              onShow: () => {},
              onHide: () => {},
              onPress: () => {},
            });
            setTimeout(() => {
              this.props?.navigation?.navigate('ResetCredentialExecutor');
            }, 350);
          }
        })
        .catch(error => {
          console.log('ChangePassword Error', error);
          this.setState({
            showDialogBox: true,
            serviceLoading: false,
            serviceMessage: Localization.t('Commons.UnexpectedError'),
          });
        });
    }
  }

  render() {
    const {
      currentPassord,
      newPassword,
      repeatNewPassword,
      formIsValid,
      serviceLoading,
      showDialogBox,
      serviceMessage,

      selectedPasswordChangeDuration,
      passwordChangeDurationOptions,
      currentPassordShow,
      newPasswordShow,
      repeatNewPassworShow,
    } = this.state;
    const {navigation} = this.props;
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <KeyboardAwareScrollView
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader
              title={Localization.t('PasswordGenerate.header')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showDialogBox}
              text={serviceMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <View style={styles.childContainer}>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    formIsValid: value,
                  })
                }>
                <FATextInput.Password
                  isPasswordShowing={!currentPassordShow}
                  onPressRightIcon={() =>
                    this.setState({
                      currentPassordShow: !currentPassordShow,
                    })
                  }
                  label={Localization.t('PasswordGenerate.CurrentPassword')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t(
                    'PasswordGenerate.PasswordRules',
                  )}
                  name={'Mevcutpin'}
                  value={currentPassord}
                  onChangeValue={value =>
                    this.setState({
                      currentPassord: value,
                    })
                  }
                  placeholder={Localization.t(
                    'PasswordGenerate.CurrentPassword',
                  )}
                />

                <View style={styles.marginTop16} />
                <FATextInput.Password
                  isPasswordShowing={!newPasswordShow}
                  onPressRightIcon={() =>
                    this.setState({
                      newPasswordShow: !newPasswordShow,
                    })
                  }
                  label={Localization.t('PasswordGenerate.NewPassword')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t(
                    'PasswordGenerate.PasswordRules',
                  )}
                  name={'newPIN'}
                  value={newPassword}
                  onChangeValue={value =>
                    this.setState({
                      newPassword: value,
                    })
                  }
                  placeholder={Localization.t('PasswordGenerate.NewPassword')}
                />
                <View style={styles.marginTop16} />
                <FATextInput.Password
                  isPasswordShowing={!repeatNewPassworShow}
                  onPressRightIcon={() =>
                    this.setState({
                      repeatNewPassworShow: !repeatNewPassworShow,
                    })
                  }
                  label={Localization.t('PasswordGenerate.RepeatNewPassword')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t(
                    'PasswordGenerate.PasswordRules',
                  )}
                  name={'repeatNewPIN'}
                  value={repeatNewPassword}
                  onChangeValue={value =>
                    this.setState({
                      repeatNewPassword: value,
                    })
                  }
                  placeholder={Localization.t(
                    'PasswordGenerate.RepeatNewPassword',
                  )}
                />
                {passwordChangeDurationOptions?.length > 0 ? (
                  <View style={{marginTop: 20}}>
                    <Text
                      style={{
                        marginBottom: 8,
                        fontSize: 14,
                        color: '#717171',
                      }}>
                      {Localization.t('PasswordChangeDuration.info')}
                    </Text>
                    <FAExchangeOrderTypePicker
                      onValueChange={newDurationValue =>
                        this.setState({
                          selectedPasswordChangeDuration: newDurationValue,
                        })
                      }
                      selectedFlow={selectedPasswordChangeDuration}
                      flowList={passwordChangeDurationOptions}
                    />
                  </View>
                ) : null}
                <View style={styles.marginTop16} />
                <FAButton
                  disabled={!formIsValid}
                  onPress={() => this.callChangePasswordService()}
                  containerStyle={[
                    styles.buttonContainer,
                    {backgroundColor: theme?.colors?.green},
                  ]}
                  textStyle={[
                    styles.buttonTextStyle,
                    {text: theme?.colors?.white2},
                  ]}
                  text={Localization.t('PasswordGenerate.ChangePassword')}
                />
              </FAForm>
            </View>
          </KeyboardAwareScrollView>
        )}
      </ThemeContext.Consumer>
    );
  }
}
