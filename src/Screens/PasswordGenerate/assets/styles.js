import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  headerContainer: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  mainContainer: {backgroundColor: FAColor.White, flex: 1},
  childContainer: {marginVertical: 16, marginHorizontal: 10},
  marginTop16: {marginTop: 16},
  buttonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
  },
  buttonTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    marginVertical: 14,
  },
});
