import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  childContainer: {
    flex: 1,
    backgroundColor: FAColor.White,
    marginHorizontal: 10,
    marginTop: 16,
    marginBottom: 10,
  },
  marginTop16: {marginTop: 16},
  firstCheckBoxContainer: {flexDirection: 'row', marginTop: 16},
  firstCheckBoxTextStyle: {
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 8,
    alignItems: 'center',
    color: '#696969', // TO DO color
  },
  secondCheckBoxContainer: {
    flexDirection: 'row',
    marginTop: 16,
    alignItems: 'center',
  },
  secondCheckBoxTextStyle: {
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 8,
    color: '#696969', // TO DO Color
  },
  createAccountButtonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 16,
  },
  createAccountButtonTextStyle: {
    marginVertical: 16,
    color: FAColor.White,
  },
  loginButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    borderRadius: 4,
    marginTop: 16,
  },
  loginButtonTextStyle: {
    marginVertical: 16,
    color: FAColor.DarkBlue,
  },
});
