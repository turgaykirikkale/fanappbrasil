import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FATextInput from '@Components/Composite/FATextInput';
import FATextRendered from '@Components/UI/FATextRendered';
import FACheckbox from '@Components/Composite/FACheckbox';
import FAButton from '@Components/Composite/FAButton';
import FAModal from '@Components/Composite/FAModal';
import FADivider from '@Components/UI/FADivider';
import FAForm from '@ValidationEngine/Form';
import Svg, {Path} from 'react-native-svg';
import autobind from 'autobind-decorator';
import React, {Component} from 'react';
import {styles} from './assets/styles';
import FAColor from '@Commons/FAColor';
import FAEnums from '@Commons/FAEnums';
import {View} from 'react-native';
import FAService from '@Services';
import Localization from '@Localization';
import FAQrScanner from '@Components/Composite/FAQrScanner';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import {getDataFromStorage} from '../../Commons/FAStorage';

export default class CreateAccountScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phoneCode:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.code
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.countryCode
          : '+90',
      flag:
        props &&
        props.route &&
        props.route.params &&
        props.route.params.item &&
        props.route.params.item.flag
          ? props &&
            props.route &&
            props.route.params &&
            props.route.params.flag
          : 'Turkey',
      userFullName: '',
      purePhoneNumber: '',
      fullPhoneNumber: '',
      formattedValue: '',
      password: '',
      repeatPassword: '',
      referenceCode: null,
      IsElectronicCommercialMessage: false,
      IsIlluminationText: false,
      IsUserAgreement: false,
      showDialogBox: false,
      serviceMessage: null,
      serviceLoading: false,
      FormIsValid: false,
      showApprovePhoneModal: false,
      showCamera: false,

      currentLangCode: null,
    };

    this.userAgreementUri = 'https://www.bitci.com/user-agreement';
    this.privacyPolicyUri = 'https://www.bitci.com/privacy-policy';
    this.protectionofpersonaldataUri =
      'https://www.bitci.com/protection-of-personal-data';
    this.explicitConsentStatementUri = 'https://www.bitci.com/express-consent';
    this.commercialelectronicmessageUri =
      'https://www.bitci.com/commercial-electronic-message';
  }

  async componentWillMount() {
    const currentLangCode = await getDataFromStorage('Language');
    this.setState({
      currentLangCode,
    });
  }

  @autobind
  onPhoneSelect({code, flag}) {
    this.setState({
      phoneCode: code,
      flag: flag,
    });
  }

  @autobind
  registerServiceCall() {
    const {navigation} = this.props;
    const {
      purePhoneNumber,
      password,
      phoneCode,
      referenceCode,
      IsElectronicCommercialMessage,
      IsIlluminationText,
      IsUserAgreement,
    } = this.state;
    const requestBody = {
      CountryCode: phoneCode,
      IsElectronicCommercialMessage: IsElectronicCommercialMessage,
      IsIlluminationText: IsIlluminationText,
      IsUserAgreement: IsUserAgreement,
      Password: password,
      Phone: purePhoneNumber,
      ReffererCode: referenceCode || null,
    };

    FAService.UserRegister(requestBody)
      .then(response => {
        if (
          response &&
          response.data &&
          response.status === 200 &&
          response.data.IsSuccess
        ) {
          this.setState(
            {serviceLoading: false, showApprovePhoneModal: false},
            () => {
              navigation.navigate('OTPScreen', {
                registerUserCridentials: requestBody,
                flow: FAEnums.REGISTERFLOW,
              });
            },
          );
        } else {
          this.setState({
            showApprovePhoneModal: false,
            serviceLoading: false,
            showDialogBox: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {
        this.setState({
          serviceLoading: false,
          showApprovePhoneModal: false,
        });
      });
  }

  @autobind
  openProtectionofpersonaldataWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.protectionofpersonaldataUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('CreateAccountScreen.KVVKIllumination'),
    });
  }

  @autobind
  openUserAgreementWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.userAgreementUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('CreateAccountScreen.UserAgreement'),
    });
  }

  @autobind
  openexplicitConsentStatemenWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.explicitConsentStatementUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('CreateAccountScreen.ExplicitConsentText'),
    });
  }

  @autobind
  opencommercialelectronicmessageWebView() {
    this.props.navigation.navigate('WebviewScreen', {
      url: `${this.commercialelectronicmessageUri}?lang=${this.state.currentLangCode}`,
      title: Localization.t('CreateAccountScreen.CommercialElectronicMessage'),
    });
  }

  @autobind
  onChangePhoneNumber(formatted, extracted) {
    const newPhonePrefix = this.state.phoneCode;
    let generatedPhoneNumberValue = extracted;
    if (extracted.indexOf(newPhonePrefix) > -1) {
      generatedPhoneNumberValue = extracted.replace(newPhonePrefix, '');
    }
    this.setState({
      purePhoneNumber: generatedPhoneNumberValue,
      fullPhoneNumber: extracted,
      phonePrefix: newPhonePrefix,
      formattedValue: formatted,
    });
  }

  @autobind
  navigate() {
    this.props.navigation.navigate('CountrySelectScreen', {
      onPhoneSelect: this.onPhoneSelect,
    });
  }

  @autobind
  readyForService() {
    const {
      IsIlluminationText,
      IsUserAgreement,
      FormIsValid,
      password,
      repeatPassword,
    } = this.state;

    if (
      IsIlluminationText &&
      IsUserAgreement &&
      FormIsValid &&
      password === repeatPassword
    ) {
      return false;
    }
    return true;
  }

  @autobind
  textLogic() {
    const {phoneCode, purePhoneNumber} = this.state;
    return (
      <FATextRendered
        rightText={Localization.t('CreateAccountScreen.smsModal')}
        boldSentences={[`${phoneCode + purePhoneNumber}`]}
        disabled={true}
      />
    );
  }

  @autobind
  closeApproveModalAndInvogeRegisterService() {
    const {navigation} = this.props;
    this.setState(
      {
        showApprovePhoneModal: false,
      },
      () => {
        this.registerServiceCall();
        // navigation.navigate('IdentityEnterScreen');
      },
    );
  }

  render() {
    const {
      flag,
      password,
      referenceCode,
      repeatPassword,
      IsElectronicCommercialMessage,
      IsIlluminationText,
      IsUserAgreement,
      phoneCode,
      serviceLoading,
      showApprovePhoneModal,
      showDialogBox,
      serviceMessage,
      formattedValue,
      fullPhoneNumber,
      showCamera,
      FormIsValid,
    } = this.state;

    console.log(FormIsValid, IsIlluminationText, IsUserAgreement);
    const {navigation} = this.props;
    if (showCamera) {
      return (
        <FAQrScanner
          showCamera={showCamera}
          onShotCamera={image => this.onShotCamera(image)}
          closeCamera={() => this.setState({showCamera: false})}
          onQrReaded={inComingReferenceCode =>
            this.setState({
              showCamera: false,
              referenceCode: inComingReferenceCode,
            })
          }
        />
      );
    }

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showDialogBox}
              text={serviceMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showDialogBox: false})}
            />
            <FAStandardHeader
              title={Localization.t('CreateAccountScreen.title')}
              navigation={navigation}
            />
            <FAModal.Alert
              visible={showApprovePhoneModal}
              buttons={[
                {
                  text: Localization.t('Commons.Cancel'),
                  containerStyle: {
                    backgroundColor: theme?.colors?.gray,
                    borderRadius: 4,
                  },
                  textStyle: {marginVertical: 15, color: theme?.colors?.white2},
                  onPress: () =>
                    this.setState({
                      showApprovePhoneModal: false,
                    }),
                },
                {
                  text: Localization.t('Commons.Send'),
                  containerStyle: {
                    backgroundColor: theme?.colors?.green,
                    borderRadius: 4,
                  },
                  textStyle: {marginVertical: 15, color: theme?.colors?.white2},
                  onPress: () =>
                    this.closeApproveModalAndInvogeRegisterService(),
                },
              ]}
              text={this.textLogic()}
            />
            <KeyboardAwareScrollView
              showsVerticalScrollIndicator={false}
              style={[
                styles.childContainer,
                {backgroundColor: theme?.colors?.white},
              ]}
              showVerticalScroll>
              <FAForm
                formValidSituationChanged={value =>
                  this.setState({
                    FormIsValid: value,
                  })
                }>
                <FATextInput.PhoneNumber
                  keyboardType={'phone-pad'}
                  rule={
                    phoneCode === '+90'
                      ? 'purePhoneNumberValue'
                      : 'notEmptyAndNil'
                  }
                  phonePrefix={phoneCode}
                  errorMessage={Localization.t(
                    'CreateAccountScreen.CorrectFormatPhone',
                  )}
                  selectedItem={{
                    id: 1,
                    phoneCode: phoneCode,
                    flag: flag,
                  }}
                  placeholder={'555 555 55 55'}
                  label={Localization.t('Commons.Phone')}
                  onChangeValue={(formatted, extracted) =>
                    this.onChangePhoneNumber(formatted, extracted)
                  }
                  value={formattedValue}
                  notFormattedValue={fullPhoneNumber}
                  mask={
                    phoneCode === '+90'
                      ? '[000] [000] [00] [00]'
                      : '[00000000000000]'
                  }
                  onPress={() => this.navigate()}
                />
                <View style={{marginTop: 16}} />
                <FATextInput.Password
                  label={Localization.t('Commons.Password')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t('Commons.PasswordRule')}
                  name={'Şifre Input'}
                  value={password}
                  onChangeValue={value =>
                    this.setState({
                      password: value,
                    })
                  }
                  placeholder={Localization.t('Commons.Password')}
                />
                <View style={{marginTop: 16}} />

                <FATextInput.Password
                  label={Localization.t('Commons.RepeatPassword')}
                  rule={'userPasswordRule'}
                  errorMessage={Localization.t('Commons.RepeatPassword')}
                  name={'Şifre Input'}
                  value={repeatPassword}
                  onChangeValue={value =>
                    this.setState({
                      repeatPassword: value,
                    })
                  }
                  placeholder={Localization.t('Commons.RepeatPassword')}
                />
                <View style={styles.marginTop16} />
              </FAForm>
              <FATextInput.WithRightIcon
                label={Localization.t('CreateAccountScreen.ReferenceCode')}
                value={referenceCode}
                onChangeValue={value =>
                  this.setState({
                    referenceCode: value,
                  })
                }
                placeholder={Localization.t(
                  'CreateAccountScreen.ReferenceCode',
                )}
                //TODO CAMERA WİLL ADD
                onPressRightIcon={() =>
                  this.setState({
                    showCamera: true,
                  })
                }
                rightIconComponent={
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24">
                    <Path d="M0 0h24v24H0z" fill="none" />
                    <Path
                      d="M15 3h6v5h-2V5h-4zM9 3v2H5v3H3V3zm6 18v-2h4v-3h2v5zm-6 0H3v-5h2v3h4zM3 11h18v2H3z"
                      fill="#767d8d"
                    />
                  </Svg>
                }
              />

              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsUserAgreement}
                  onPress={() =>
                    this.setState({
                      IsUserAgreement: !IsUserAgreement,
                    })
                  }
                />
                <FATextRendered
                  rightText={Localization.t('CreateAccountScreen.kvkkTexts')}
                  boldSentences={[
                    Localization.t('CreateAccountScreen.KVVKIllumination'),
                    Localization.t('CreateAccountScreen.UserAgreement'),
                  ]}
                  onPressBoldSentences={[
                    () => this.openProtectionofpersonaldataWebView(),
                    () => this.openUserAgreementWebView(),
                  ]}
                />
              </View>
              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsIlluminationText}
                  onPress={() =>
                    this.setState({
                      IsIlluminationText: !IsIlluminationText,
                    })
                  }
                />
                <View style={{marginTop: 16}} />

                <FATextRendered
                  rightText={Localization.t(
                    'CreateAccountScreen.explicitConsentText',
                  )}
                  boldSentences={[
                    Localization.t('CreateAccountScreen.ExplicitConsentText'),
                  ]}
                  onPressBoldSentences={[
                    () => this.openexplicitConsentStatemenWebView(),
                  ]}
                />
              </View>
              <View style={styles.firstCheckBoxContainer}>
                <FACheckbox.Circle
                  checked={IsElectronicCommercialMessage}
                  onPress={() =>
                    this.setState({
                      IsElectronicCommercialMessage:
                        !IsElectronicCommercialMessage,
                    })
                  }
                />
                <View style={styles.marginTop16} />

                <FATextRendered
                  rightText={Localization.t(
                    'CreateAccountScreen.commercialelectronicText',
                  )}
                  boldSentences={[
                    Localization.t(
                      'CreateAccountScreen.CommercialElectronicMessage',
                    ),
                  ]}
                  onPressBoldSentences={[
                    () => this.opencommercialelectronicmessageWebView(),
                  ]}
                />
              </View>
              <FAButton
                disabled={this.readyForService()}
                onPress={() =>
                  this.setState({
                    showApprovePhoneModal: true,
                  })
                }
                containerStyle={[
                  styles.createAccountButtonContainer,
                  {backgroundColor: theme?.colors?.success},
                ]}
                textStyle={[
                  styles.createAccountButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('CreateAccountScreen.title')}
              />

              <FADivider text={Localization.t('Commons.Or')} />
              <FAButton
                disabled={false}
                onPress={() => navigation.navigate('LoginScreen')}
                containerStyle={[
                  styles.loginButtonContainer,
                  {backgroundColor: theme?.colors?.gray},
                ]}
                textStyle={[
                  styles.loginButtonTextStyle,
                  {color: theme?.colors?.white2},
                ]}
                text={Localization.t('Commons.Login')}
              />
            </KeyboardAwareScrollView>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
