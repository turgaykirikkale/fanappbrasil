import React, {Component} from 'react';
import {View, Text, ScrollView, useWindowDimensions} from 'react-native';
import FAAssetShower from '@Components/Composite/FAAssetShower';
import FADrawImage from '@Components/Composite/FADrawImage';
import FAButton from '@Components/Composite/FAButton';
import {AppConstants} from '@Commons/Contants';
import {styles} from './assets/styles';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import _ from 'lodash';
import moment from 'moment';
import {Black, FAOrange, Green} from '@Commons/FAColor';
import {EventPriceType} from '@Commons/Enums/EventPriceType';
import Localization from '@Localization';
import {RenderHTML} from 'react-native-render-html';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';

export default class EventDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventDetail: props?.route?.params?.event,
      route: props?.route?.params?.routeMain,
    };
  }

  render() {
    const {eventDetail, route} = this.state;
    const {UserBalance} = this.props?.route?.params;
    const isAnonymousFlow = this.props?.route?.params.isAnonymousFlow;

    let CoinBalance;
    let currencyBalance;
    let CoinCode;
    if (UserBalance && eventDetail) {
      UserBalance.map(item => {
        if (item.CoinId === eventDetail?.CoinId) {
          if (item.CoinBalance < 1) {
            CoinBalance = '0.00';
            CoinCode = item.CoinCode;
          } else {
            CoinCode = item.CoinCode;
            CoinBalance = FACurrencyAndCoinFormatter(
              item.CoinBalance || 0.0,
              AppConstants.CoinDecimalCount,
            );
          }
        }
        if (item.CoinCode === AppConstants.CurrencyCode) {
          CoinCode = item.CoinCode;
          currencyBalance = FACurrencyAndCoinFormatter(
            item.CoinBalance || 0.0,
            AppConstants.CoinDecimalCount,
          );
        }
      });
    }

    const {navigation} = this.props;
    // if (_.isEmpty(eventDetail)) {
    //   if (navigation.canGoBack()) {
    //     navigation.goBack();
    //   }
    // }
    const dayAndMonth = moment(eventDetail?.ActivityEndDate).format('DD MMMM');
    const hour = moment(eventDetail?.ActivityEndDate).format('HH:mm');
    const isJoined = eventDetail?.Joined;
    const isActive = eventDetail?.Active;
    const eventPriceType =
      eventDetail?.Price <= 0 ? EventPriceType.Free : EventPriceType.Paid;

    let buttonText = `${Localization.t('EventDetailScreen.JoinEvent')} (${
      eventDetail?.Price
    } ${CoinCode})`;
    if (isJoined) {
      buttonText = Localization.t('EventDetailScreen.JoinedEvent');
    } else if (!isActive) {
      buttonText = Localization.t('EventDetailScreen.PassedEvent');
    } else if (eventPriceType === EventPriceType.Free) {
      buttonText = Localization.t('EventDetailScreen.JoinFree');
    }

    const contentSource = {
      html: `
        ${eventDetail?.Description}`,
    };
    debugger;
    return (
      <>
        <FAStandardHeader
          title={Localization.t('EventConfirmationScreen.Events')}
          navigation={navigation}
        />

        <ThemeContext.Consumer>
          {({theme}) => (
            <View
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.blueGray},
              ]}>
              <FAAssetShower
                onPress={flow => {
                  flow === 1
                    ? navigation.navigate('LoginScreen')
                    : flow === 2
                    ? navigation.navigate('RegisterStepOne')
                    : navigation.navigate('WalletTabStackNavigator');
                }}
                CoinCode={CoinCode}
                fanTokenBalance={CoinBalance || '0.00'}
                currencyBalance={currencyBalance || '0.00'}
              />
              <FADrawImage
                CoinCode={CoinCode}
                price={String(eventDetail?.Price)}
                date={dayAndMonth}
                time={hour}
                explainedText={eventDetail?.Title}
                imageUrl={eventDetail?.ImageUrl}
              />
              <ScrollView
                style={[
                  styles.textsContainer,
                  {backgroundColor: theme?.colors?.blueGray},
                ]}
                showsVerticalScrollIndicator={false}>
                <View
                  style={[
                    styles.textsChildContainer,
                    {backgroundColor: theme?.colors?.blueGray},
                  ]}>
                  <RenderHTML
                    source={contentSource}
                    tagsStyles={{
                      body: {
                        color: theme?.colors?.black,
                      },
                    }}
                  />
                </View>
              </ScrollView>

              <FAButton
                disabled={!isActive || isJoined}
                onPress={() =>
                  navigation.navigate('EventConfirmationScreen', {
                    event: eventDetail,
                    CoinCode: CoinCode,
                    routeMain: route,
                  })
                }
                containerStyle={{
                  backgroundColor: !isActive
                    ? theme?.colors?.white
                    : isJoined
                    ? theme?.colors?.green
                    : theme?.colors?.faOrange,
                }}
                textStyle={styles.buttonTextStyle}
                text={buttonText}
              />
            </View>
          )}
        </ThemeContext.Consumer>
      </>
    );
  }
}
