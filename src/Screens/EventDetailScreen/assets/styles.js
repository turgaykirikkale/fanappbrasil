import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1},
  textsContainer: {backgroundColor: '#F8F9FB', flex: 1},
  textsChildContainer: {
    backgroundColor: FAColor.White,
    flex: 1,
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 4,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  description: {
    fontSize: 14,
    color: '#404040',
    textAlign: 'center',
    marginVertical: 20,
    marginHorizontal: 10,
    flex: 1,
  },
  buttonTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    letterSpacing: -0.5,
    marginVertical: 15,
  },
});
