import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  approveTextContainer: {
    paddingHorizontal: 30,
    paddingVertical: 36,
    // backgroundColor: FAColor.White,
    borderRadius: 4,
    marginHorizontal: 20,
    marginVertical: 15,
  },
  approvedTextStyle: {
    textAlign: 'center',
    lineHeight: 30,
    // color: FAColor.Success,
    fontWeight: '600',
  },
  informationTextContainer: {marginHorizontal: 9},
  informationTextStyle: {
    fontSize: 11,
    color: FAColor.DarkGray,
    lineHeight: 20,
  },
  buttonMainContainer: {flex: 1, justifyContent: 'flex-end'},
  buttonContainer: {backgroundColor: FAColor.Success},
  buttonTextStyle: {
    marginVertical: 16,
    color: FAColor.White,
    fontWeight: '700',
  },
});
