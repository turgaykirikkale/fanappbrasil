import React, {Component} from 'react';
import {View, Text, ScrollView, BackHandler} from 'react-native';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import {AppConstants} from '@Commons/Contants';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import moment from 'moment';
import FAService from '@Services';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import Toast from 'react-native-toast-message';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {
  subscribeJoinActivityProcess,
  unSubscribeJoinActivityProcess,
} from '@Stream';
import {connect} from 'react-redux';
import {EventPriceType} from '@Commons/Enums/EventPriceType';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

class EventConfirmationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventDetail: props?.route?.params?.event,
      errorMessage: null,
      showErrorMessage: false,
      serviceLoading: false,
      CoinCode: props?.route?.params?.CoinCode,
      route: props?.route?.params?.routeMain,
    };
  }

  componentWillMount() {
    const {CoinCode, route} = this.state;
    subscribeJoinActivityProcess(streamData => {
      const {eventDetail} = this.state;
      const {navigation} = this.props;
      if (streamData && streamData.Success) {
        Toast.show({
          type: 'success',
          position: 'top',
          text1: Localization.t('CommonsFix.Succces'),
          text2: Localization.t('CommonsFix.SuccessfulTransaction'),
          visibilityTime: 2000,
          autoHide: true,
          topOffset: 100,
          bottomOffset: 40,
          onShow: () => {},
          onHide: () => {},
          onPress: () => {},
        });
        navigation.navigate('EventInformationScreen', {
          event: eventDetail,
          CoinCode: CoinCode,
          routeMain: route,
        });
      } else {
        this.setState({
          serviceLoading: false,
          errorMessage: streamData.Message,
          showErrorMessage: true,
        });
      }
    });
  }

  joinEvent(navigationParams: Object) {
    const {eventDetail} = this.state;

    this.setState({serviceLoading: true});
    if (eventDetail?.ActivityId) {
      const requestBody = {
        ActivityId: eventDetail?.ActivityId,
      };
      FAService.JoinEvent(requestBody)
        .then(res => {
          if (res && res.status === 200 && res.data) {
            const resData = res.data;
            this.setState({serviceLoading: false});
            if (!resData.IsSuccess) {
              this.setState({
                errorMessage: resData.Message,
                showErrorMessage: true,
              });
            }
          }
        })
        .catch(err => {
          this.setState({serviceLoading: true});
          console.log('Error => ', err);
        });
    }
  }

  componentWillUnmount() {
    unSubscribeJoinActivityProcess();
  }

  render() {
    const {
      eventDetail,
      showErrorMessage,
      errorMessage,
      serviceLoading,
      CoinCode,
    } = this.state;
    const {navigation} = this.props;

    const eventPriceType =
      eventDetail?.Price <= 0 ? EventPriceType.Free : EventPriceType.Paid;
    let confirmationText = Localization.t('EventsScreenFix.JoinApprove', {
      Title: eventDetail.Title,
    });

    if (eventPriceType === EventPriceType.Free) {
      confirmationText = Localization.t('EventsScreenFix.FreeJoin', {
        confirmationText: confirmationText,
      });
    } else {
      confirmationText = Localization.t('EventsScreenFix.PayForjoin', {
        Price: eventDetail?.Price,
        CoinCode: CoinCode,
        confirmationText: confirmationText,
      });
    }

    const eventStringEndDate = moment(eventDetail?.ActivityEndDate).format(
      'DD MMMM YYYY HH:mm',
    );

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              title={Localization.t('CommonsFix.Events')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showErrorMessage}
              text={errorMessage}
              iconName={'times-circle'}
              header={Localization.t('CommonsFix.Error')}
              onPress={() => this.setState({showErrorMessage: false})}
            />
            <ScrollView style={{backgroundColor: theme?.colors?.white}}>
              <View
                style={[
                  styles.approveTextContainer,
                  {backgroundColor: theme?.colors?.blueGray},
                ]}>
                <Text
                  style={[
                    styles.approvedTextStyle,
                    {color: theme?.colors?.success},
                  ]}>
                  {confirmationText.toLocaleUpperCase(
                    AppConstants.DefaultLanguage,
                  )}
                </Text>
              </View>

              <View
                style={[
                  styles.informationTextContainer,
                  {color: theme?.colors?.blueGray},
                ]}>
                <Text
                  style={[
                    styles.informationTextStyle,
                    {color: theme?.colors?.black},
                  ]}>
                  * {Localization.t('EventsScreenFix.EventWillEnd')}
                  <Text
                    style={{fontWeight: '600', color: theme?.colors?.black}}>
                    {eventStringEndDate}
                  </Text>
                  {Localization.t('EventsScreenFix.EventEnd')}
                </Text>
              </View>
            </ScrollView>
            <View
              style={[
                styles.buttonMainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <FAButton
                onPress={() =>
                  this.joinEvent({formattedEndDate: eventStringEndDate})
                }
                text={Localization.t('EventsScreenFix.Approve')}
                textStyle={styles.buttonTextStyle}
                rightIconSize={10}
                containerStyle={[
                  styles.buttonContainer,
                  {backgroundColor: theme?.colors?.green},
                ]}
              />
            </View>
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default connect(null, {})(EventConfirmationScreen);
