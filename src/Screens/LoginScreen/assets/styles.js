import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: FAColor.White},
  RouteRenderContainer: {marginHorizontal: 10, marginTop: 16},
  marginTop16: {marginTop: 16},
  forgotPasswordContainer: {alignSelf: 'flex-end', marginTop: 16},
  forgotButtonTextStyle: {
    color: FAColor.DarkBlue,
    fontSize: 14,
  },
  loginButtonContainer: {
    backgroundColor: FAColor.Green,
    borderRadius: 4,
    marginVertical: 16,
  },
  loginButtonTextStyle: {
    marginVertical: 16,
    color: FAColor.White,
  },
  createAccountButtonContainer: {
    backgroundColor: FAColor.VBlueGray,
    borderRadius: 4,
    marginTop: 16,
  },
  createAccountButtonTextStyle: {
    marginVertical: 16,
    color: FAColor.DarkBlue,
  },
  tabbarTextStyle: {color: FAColor.Green, margin: 8},
});
