import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {View, Text, Dimensions, SafeAreaView} from 'react-native';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FATextInput from '@Components/Composite/FATextInput';
import {CommonActions} from '@react-navigation/native';
import {TabView, TabBar} from 'react-native-tab-view';
import FAButton from '@Components/Composite/FAButton';
import {setDataToStorage} from '@Commons/FAStorage';
import FADivider from '@Components/UI/FADivider';
import FAForm from '@ValidationEngine/Form';
import autobind from 'autobind-decorator';
import React, {Component} from 'react';
import FAColor from '@Commons/FAColor';
import {styles} from './assets/styles';
import FAEnums from '@Commons/FAEnums';
import FAService from '@Services';
import _ from 'lodash';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneCode:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.item &&
        this.props.route.params.item.code
          ? this.props &&
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.countryCode
          : '+55',
      flag:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.item &&
        this.props.route.params.item.flag
          ? this.props &&
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.flag
          : 'Brazil',
      emailFormIsValid: false,
      phoneFormIsValid: false,
      purePhoneNumber: '',
      fullPhoneNumber: '',
      formattedValue: '',
      phonePrefix: '',
      email: '',
      password: '',
      index: 0,
      routes: [
        {
          key: 'phone',
          title: Localization.t('CommonsFix.Phone'),
          accessibilityLabel: 'test',
        },
        {
          key: 'email',
          title: Localization.t('CommonsFix.Email'),
          accessibilityLabel: 'deneme',
        },
      ],
      serviceErrorMessage: null,
      errorModalVisible: false,
      serviceLoading: false,
      showPassword: false,
    };
  }

  @autobind
  loginServiceCall(loginFlow) {
    this.setState({
      serviceLoading: true,
    });
    const {email, password, purePhoneNumber} = this.state;
    const {navigation} = this.props;
    let requestBody = {};
    requestBody.EmailOrPhone =
      loginFlow === 'withEmail' ? email : purePhoneNumber;
    requestBody.Password = password;
    requestBody.LanguageCode = 'en';
    requestBody.SmsMobileKey = null;
    requestBody.AuthenticationCode = null;
    FAService.Login(requestBody)
      .then(async response => {
        if (response && response.data.Token) {
          await setDataToStorage('Token', response.data.Token);
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: 'AuthLoaderScreen'}],
            }),
          );
        } else if (
          response &&
          response.status === 200 &&
          response.data.Success &&
          !response.data.ConfirmRegisterRequired
        ) {
          navigation.navigate('OTPScreen', {
            loginUserCridentials: requestBody,
            flow: FAEnums.LOGINFLOW,
            isGoogleAuth: response.data.AuthType === 2 ? true : false,
          });
        } else if (
          response.data.AuthType ===
            FAEnums.CustomerRegisterRedirect.ToSmsValidation ||
          response.data.AuthType === 2
        ) {
          navigation.navigate('OTPScreen', {
            loginUserCridentials: requestBody,
            flow: FAEnums.LOGINFLOW,
          });
        } else if (
          response &&
          response.data.RegisterationRedirectTo ===
            FAEnums.CustomerRegisterRedirect.ToSmsValidation
        ) {
          // navigation.navigate('OTPScreen', {
          //   loginUserCridentials: requestBody,
          //   flow: FAEnums.REGISTERFLOW,
          // });
          navigation.navigate('OTPScreen', {
            phone: purePhoneNumber,
            flow: FAEnums.REGISTERFLOW,
            hideBackButton: true,
          });
        } else if (
          response &&
          response.data.RegisterationRedirectTo ===
            FAEnums.CustomerRegisterRedirect.ToEmailValidation
        ) {
          navigation.navigate('EmailConfirmationScreen', {
            phone: purePhoneNumber,
          });
        } else if (
          response &&
          !response.data.Success &&
          !_.isNil(response.data.ErrorMessage)
        ) {
          this.setState({
            serviceErrorMessage: response.data.ErrorMessage,
            errorModalVisible: true,
            serviceLoading: false,
          });
        }
        this.setState({
          serviceLoading: false,
        });
      })
      .catch(error => {
        debugger;
        this.setState({
          serviceErrorMessage: Localization.t('Commons.UnexpectedError'),
          errorModalVisible: true,
          serviceLoading: false,
        });
        console.log('LOGİNERR', error);
      });
  }

  @autobind
  emailRouteRender(theme) {
    const {email, password, emailFormIsValid, showPassword} = this.state;
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView style={styles.RouteRenderContainer}>
        <FAForm
          formValidSituationChanged={value =>
            this.setState({
              emailFormIsValid: value,
            })
          }>
          <FATextInput
            rule={'isEmail'}
            errorMessage={Localization.t('LoginScreenFix.correctEmailFormat')}
            label={Localization.t('CommonsFix.Email')}
            placeholder={Localization.t('CommonsFix.EmailAdress')}
            onChangeValue={newWalue =>
              this.setState({
                email: newWalue,
              })
            }
            value={email}
          />
          <View style={styles.marginTop16} />
          <FATextInput.Password
            isPasswordShowing={!showPassword}
            onPressRightIcon={() =>
              this.setState({
                showPassword: !showPassword,
              })
            }
            label={Localization.t('CommonsFix.Password')}
            rule={'userPasswordRule'}
            errorMessage={Localization.t('CommonsFix.PasswordRule')}
            name={'Şifre Input'}
            value={password}
            onChangeValue={value =>
              this.setState({
                password: value,
              })
            }
            placeholder={Localization.t('CommonsFix.Password')}
          />
          <View style={styles.forgotPasswordContainer}>
            <FAButton
              onPress={() => navigation.navigate('ResetPasswordStep1')}
              text={Localization.t('LoginScreenFix.ForgotMyPassword')}
              textStyle={[
                styles.forgotButtonTextStyle,
                {color: theme?.colors?.darkBlue},
              ]}
            />
          </View>

          <FAButton
            disabled={!emailFormIsValid}
            onPress={() => this.loginServiceCall('withEmail')}
            containerStyle={[
              styles.loginButtonContainer,
              {backgroundColor: theme?.colors?.success},
            ]}
            textStyle={[
              styles.loginButtonTextStyle,
              {color: theme?.colors?.white2},
            ]}
            text={Localization.t('CommonsFix.Login')}
          />

          <FADivider text={Localization.t('CommonsFix.Or')} />
          <FAButton
            onPress={() => this.props.navigation.navigate('RegisterStepOne')}
            disabled={false}
            containerStyle={[
              styles.createAccountButtonContainer,
              {backgroundColor: theme?.colors?.gray},
            ]}
            textStyle={[
              styles.createAccountButtonTextStyle,
              {color: theme?.colors?.white2},
            ]}
            text={Localization.t('LoginScreenFix.CreateAccount')}
          />
        </FAForm>
      </KeyboardAwareScrollView>
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.route.params.item.code !== this.state.phoneCode) {
      this.setState({
        purePhoneNumber: '',
        fullPhoneNumber: '',
        phoneCode: nextProps.route.params.item.code,
        flag: nextProps.route.params.item.flag,
      });
    }
  }

  @autobind
  onChangePhoneNumber(formatted, extracted) {
    const newPhonePrefix = this.state.phoneCode;
    let generatedPhoneNumberValue = extracted;
    if (extracted.indexOf(newPhonePrefix) > -1) {
      generatedPhoneNumberValue = extracted.replace(newPhonePrefix, '');
    }
    this.setState({
      purePhoneNumber: generatedPhoneNumberValue,
      fullPhoneNumber: extracted,
      phonePrefix: newPhonePrefix,
      formattedValue: formatted,
    });
  }

  @autobind
  navigate() {
    const {navigation} = this.props;
    navigation.navigate('CountrySelectScreen', {
      onPhoneSelect: this.onPhoneSelect,
    });
  }

  @autobind
  onPhoneSelect({code, flag}) {
    this.setState({
      phoneCode: code,
      flag: flag,
    });
  }

  phoneRouteRender(theme) {
    const {navigation} = this.props;
    const {
      phonePrefix,
      purePhoneNumber,
      password,
      phoneCode,
      flag,
      phoneFormIsValid,
      formattedValue,
      fullPhoneNumber,
      showPassword,
    } = this.state;

    return (
      <KeyboardAwareScrollView style={styles.RouteRenderContainer}>
        <FAForm
          formValidSituationChanged={value =>
            this.setState({
              phoneFormIsValid: value,
            })
          }>
          <FATextInput.PhoneNumber
            keyboardType={'phone-pad'}
            rule={
              phoneCode === '+90' ? 'purePhoneNumberValue' : 'notEmptyAndNil'
            }
            phonePrefix={phoneCode}
            errorMessage={Localization.t('LoginScreenFix.CorrectFormatPhone')}
            selectedItem={{
              id: 1,
              phoneCode: phoneCode,
              flag: flag,
            }}
            placeholder={'555 555 55 55'}
            label={Localization.t('CommonsFix.Phone')}
            onChangeValue={(formatted, extracted) =>
              this.onChangePhoneNumber(formatted, extracted)
            }
            value={formattedValue}
            notFormattedValue={fullPhoneNumber}
            mask={
              phoneCode === '+90' ? '[000] [000] [00] [00]' : '[00000000000000]'
            }
            onPress={() => this.navigate()}
          />
          <View style={{marginTop: 16}} />
          <FATextInput.Password
            isPasswordShowing={!showPassword}
            onPressRightIcon={() =>
              this.setState({
                showPassword: !showPassword,
              })
            }
            label={Localization.t('CommonsFix.Password')}
            rule={'userPasswordRule'}
            errorMessage={Localization.t('CommonsFix.PasswordRule')}
            name={'Şifre Input'}
            value={password}
            onChangeValue={value =>
              this.setState({
                password: value,
              })
            }
            placeholder={Localization.t('CommonsFix.Password')}
          />
          <View style={styles.forgotPasswordContainer}>
            <FAButton
              onPress={() => navigation.navigate('ResetPasswordStep1')}
              text={Localization.t('LoginScreenFix.ForgotMyPassword')}
              textStyle={[
                styles.forgotButtonTextStyle,
                {color: theme?.colors?.darkBlue},
              ]}
            />
          </View>

          <FAButton
            disabled={!phoneFormIsValid}
            onPress={() => this.loginServiceCall()}
            containerStyle={[
              styles.loginButtonContainer,
              {backgroundColor: theme?.colors?.success},
            ]}
            textStyle={[
              styles.loginButtonTextStyle,
              {color: theme?.colors?.white2},
            ]}
            text={Localization.t('CommonsFix.Login')}
          />

          <FADivider text={Localization.t('CommonsFix.Or')} />
          <FAButton
            onPress={() => this.props.navigation.navigate('RegisterStepOne')}
            disabled={false}
            containerStyle={[
              styles.createAccountButtonContainer,
              {backgroundColor: theme?.colors?.gray},
            ]}
            textStyle={[
              styles.createAccountButtonTextStyle,
              {color: theme?.colors?.white2},
            ]}
            text={Localization.t('LoginScreenFix.CreateAccount')}
          />
        </FAForm>
      </KeyboardAwareScrollView>
    );
  }

  @autobind
  renderScene({route}, theme) {
    switch (route.key) {
      case 'email':
        return this.emailRouteRender(theme);
      case 'phone':
        return this.phoneRouteRender(theme);
      default:
        return null;
    }
  }

  @autobind
  renderTabBar(props, theme) {
    return (
      <TabBar
        {...props}
        renderLabel={({route, focused, color}) => (
          <Text
            style={[styles.tabbarTextStyle, {color: theme?.colors?.success}]}>
            {route.title}
          </Text>
        )}
        contentContainerStyle={{
          borderColor: theme?.colors?.gray,
        }}
        indicatorStyle={{
          backgroundColor: theme?.colors?.success,
        }}
        style={{
          backgroundColor: theme?.colors?.white,
        }}
      />
    );
  }

  render() {
    const {navigation} = this.props;
    const {
      index,
      routes,
      serviceLoading,
      errorModalVisible,
      serviceErrorMessage,
    } = this.state;
    const initialLayout = {width: Dimensions.get('window').width};
    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <>
            <FAStandardHeader
              title={Localization.t('CommonsFix.Login')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={errorModalVisible}
              text={serviceErrorMessage}
              iconName={'times-circle'}
              header={Localization.t('CommonsFix.Error')}
              onPress={() => this.setState({errorModalVisible: false})}
            />
            <SafeAreaView
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <TabView
                renderTabBar={props => this.renderTabBar(props, theme)}
                navigationState={{index, routes}}
                renderScene={props => this.renderScene(props, theme)}
                onIndexChange={(indexValue: number) =>
                  this.setState({index: indexValue})
                }
                initialLayout={initialLayout}
              />
            </SafeAreaView>
          </>
        )}
      </ThemeContext.Consumer>
    );
  }
}
