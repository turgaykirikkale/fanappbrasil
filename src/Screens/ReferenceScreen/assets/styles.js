import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  pageheaderStyle: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  mainContainer: {backgroundColor: FAColor.White},
  headerContainer: {marginHorizontal: 10, marginVertical: 23},
  h1TextStyle: {fontSize: 20, color: FAColor.White, fontWeight: 'bold'},
  h2TextStyle: {fontSize: 14, color: '#FFFFFFE6', marginTop: 8}, // TO DO COLOR
  childContainer: {
    backgroundColor: FAColor.White,
    paddingHorizontal: 26,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  marginVertical16: {marginVertical: 16},
  earnTextStyle: {color: FAColor.Orange, fontWeight: '400'},
  earnValueTextStyle: {
    fontSize: 24,
    color: FAColor.Black,
    fontWeight: 'bold',
    marginTop: 4,
  },
  marginBottom16: {marginBottom: 16},
  referenceCodeStyle: {color: FAColor.Orange, fontWeight: '400'},
  RefferalProgramCodeContainerStyle: {flexDirection: 'row', marginTop: 4},
  RefferalProgramCodeTextStyle: {fontSize: 12, flex: 1, letterSpacing: -0.6},
  referenceConnectionTextStyle: {color: FAColor.Orange, fontWeight: '400'},
  referenceLinkContainer: {
    flexDirection: 'row',
    marginTop: 4,
  },
  referenceLinkTextStyle: {fontSize: 12, flex: 1, letterSpacing: -0.6},
  CommissionInformationContainer: {
    flexDirection: 'row',
    backgroundColor: FAColor.VGray,
    paddingVertical: 20,
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 8,
  },
  CommissionRatioContainer: {marginRight: 20},
  CommissionRatioHeaderStyle: {
    color: FAColor.DarkBlue,
    letterSpacing: -0.7,
    textAlign: 'center',
  },
  CommissionRatioTextStyle: {
    fontSize: 30,
    textAlign: 'center',
    marginTop: 7,
    fontWeight: 'bold',
  },
  TotalReferenceContainer: {marginLeft: 20},
  TotalReferenceHeaderStyle: {
    color: FAColor.DarkBlue,
    letterSpacing: -0.7,
    textAlign: 'center',
  },
  TotalReferenceTextStyle: {
    fontSize: 30,
    textAlign: 'center',
    marginTop: 7,
    fontWeight: 'bold',
  },
  ButtonsContainer: {
    flexDirection: 'row',
    backgroundColor: FAColor.VBlueGray,
    paddingVertical: 8,
    paddingHorizontal: 45,
    justifyContent: 'center',
  },
  shareButtonContainer: {marginRight: 42},
  shareButtonTextStyle: {color: FAColor.DarkBlue},
  QrCodeButtonContainer: {marginLeft: 42},
  QrCodeTextStyle: {color: FAColor.DarkBlue},
  marginBottom10: {marginBottom: 10},
});
