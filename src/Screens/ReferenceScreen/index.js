import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import FAVectorIcon from '@Components/UI/FAVectorIcon';
import FAColor from '@Commons/FAColor';
import FAButton from '@Components/Composite/FAButton';
import FAReferencesList from '@Components/Composite/FAReferencesList';
import autobind from 'autobind-decorator';
import FAService from '@Services';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-community/clipboard';
import Share from 'react-native-share';
import _ from 'lodash';
import FAQRViewer from '@Components/Composite/FAQRViewer';
import {styles} from './assets/styles';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

export default class ReferenceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RefferalProgramCode: '',
      ReferralCommissionPercantage: null,
      isLoading: false,
      CommssionsData: [],
      TotalWinningCommission: null,
      FriendsData: [],
      type: 1,
      showModal: false,
      showDialog: false,
    };
  }

  componentWillMount() {
    this.setState({isLoading: true});
    this.ReferralProgramStateInfo();
    this.ReferralCommisionInfo();
    this.ReferralProgramFriendsInfo();
  }

  @autobind
  ReferralProgramStateInfo() {
    this.setState({isLoading: true});
    FAService.ReferralProgramState()
      .then(response => {
        if (response && response.data && response.status === 200) {
          this.setState({
            RefferalProgramCode: response.data.RefferalProgramCode,
            ReferralCommissionPercantage:
              response.data.ReferralCommissionPercantage,
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })

      .catch(error => {
        this.setState({
          isLoading: false,
          showDialog: true,
          serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
        });
      });
  }

  @autobind
  ReferralCommisionInfo() {
    const requestBody = {
      StartDate: moment().subtract(1, 'years').format('YYYY-MM-DD'),
      Filters: [],
      IsActive: true,
      PageNumber: 1,
      Size: 25,
      EndDate: moment().format('YYYY-MM-DD'),
    };
    this.setState({isLoading: true});
    FAService.ReferralCommision(requestBody)
      .then(response => {
        if (response && response.data && response.status === 200) {
          this.setState({
            TotalWinningCommission: response.data.Data.TotalWinningCommission,
            CommssionsData: response.data.Data.ReferralCommissions,
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })

      .catch(error => {
        this.setState({
          isLoading: false,
          showDialog: true,
          serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
        });
      });
  }

  @autobind
  ReferralProgramFriendsInfo() {
    const requestBodyForFriends = {
      StartDate: moment().subtract(1, 'years').format('YYYY-MM-DD'),
      Filters: [],
      IsActive: true,
      PageNumber: 1,
      Size: 25,
      EndDate: moment().format('YYYY-MM-DD'),
    };
    this.setState({isLoading: true});
    FAService.ReferralProgramFriends(requestBodyForFriends)
      .then(response => {
        if (
          response &&
          response.data &&
          response.data.IsActive &&
          response.status === 200
        ) {
          this.setState({
            TotalFriendsCount: response.data.DataCount,
            FriendsData: response.data.Data.ReferralFriends,
            isLoading: false,
          });
        } else {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: response.data.Message,
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          showDialog: true,
          serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
        });
      });
  }

  refenceNumberCopy() {
    const {RefferalProgramCode} = this.state;
    Clipboard.setString(RefferalProgramCode);
    Toast.show({
      type: 'success',
      position: 'top',
      text1: Localization.t('CommonsFix.Succces'),
      text2: Localization.t('CommonsFix.SuccessfulTransaction'),
      visibilityTime: 750,
      autoHide: true,
      topOffset: 50,
      bottomOffset: 40,
      onShow: () => {},
      onHide: () => {},
      onPress: () => {},
    });
  }

  referenceLinkCopy() {
    const {RefferalProgramCode} = this.state;
    Clipboard.setString(
      `https://borsa.bitci.com/register?ref=${RefferalProgramCode}`,
    );
    Toast.show({
      type: 'success',
      position: 'top',
      text1: Localization.t('CommonsFix.Succces'),
      text2: Localization.t('CommonsFix.SuccessfulTransaction'),
      visibilityTime: 750,
      autoHide: true,
      topOffset: 50,
      bottomOffset: 40,
      onShow: () => {},
      onHide: () => {},
      onPress: () => {},
    });
  }

  @autobind
  shareOptions() {
    const {RefferalProgramCode} = this.state;

    if (!_.isNil(RefferalProgramCode) && !_.isEmpty(RefferalProgramCode)) {
      const options = {
        message: RefferalProgramCode,
      };
      Share.open(options)
        .then(res => {})
        .catch(err => {});
    }
  }

  render() {
    const {
      RefferalProgramCode,
      TotalWinningCommission,
      ReferralCommissionPercantage,
      TotalFriendsCount,
      FriendsData,
      CommssionsData,
      showModal,
      isLoading,
      showDialog,
      serviceMessage,
    } = this.state;
    const {navigation} = this.props;

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View style={{flex: 1}}>
            <FAStandardHeader
              title={Localization.t('ReferenceScreenFix.title')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={isLoading} />
            <ScrollView
              style={[
                styles.mainContainer,
                {backgroundColor: theme?.colors?.white},
              ]}>
              <FAAlertModal
                visible={showDialog}
                text={serviceMessage}
                iconName={'times-circle'}
                header={Localization.t('CommonsFix.Error')}
                onPress={() => this.setState({showDialog: false})}
              />
              <FAQRViewer
                visible={showModal}
                onPressCancel={() =>
                  this.setState({
                    showModal: false,
                  })
                }
                RefferalProgramCode={RefferalProgramCode}
                RefferalProgramLink={`https://borsa.bitci.com/register?ref=${RefferalProgramCode}`}
              />
              <View style={{backgroundColor: theme?.colors?.orange}}>
                <View style={styles.headerContainer}>
                  <Text
                    style={[
                      styles.h1TextStyle,
                      {color: theme?.colors?.white2},
                    ]}>
                    {Localization.t('ReferenceScreenFix.PassiveIncome')}
                  </Text>
                  <Text
                    style={[
                      styles.h2TextStyle,
                      {color: theme?.colors?.white2},
                    ]}>
                    {Localization.t('ReferenceScreenFix.EarnText')}
                  </Text>
                </View>
                <View
                  style={[
                    styles.childContainer,
                    {backgroundColor: theme?.colors?.white},
                  ]}>
                  <View style={styles.marginVertical16}>
                    <Text
                      style={[
                        styles.earnTextStyle,
                        {color: theme?.colors?.faOrange},
                      ]}>
                      {Localization.t('ReferenceScreenFix.Profit')}
                    </Text>
                    <Text
                      style={[
                        styles.earnValueTextStyle,
                        {color: theme?.colors?.black},
                      ]}>
                      {`${TotalWinningCommission || '0'} CHFT`}
                    </Text>
                  </View>
                  <View style={styles.marginBottom16}>
                    <Text
                      style={[
                        styles.referenceCodeStyle,
                        {color: theme?.colors?.faOrange},
                      ]}>
                      {Localization.t('ReferenceScreenFix.ReferenceCode')}
                    </Text>
                    <View style={styles.RefferalProgramCodeContainerStyle}>
                      <Text
                        style={[
                          styles.RefferalProgramCodeTextStyle,
                          {color: theme?.colors?.black},
                        ]}>
                        {RefferalProgramCode || ''}
                      </Text>
                      <TouchableOpacity
                        onPress={() => this.refenceNumberCopy()}>
                        <FAVectorIcon
                          group={'FontAwesome'}
                          iconName={'copy'}
                          size={16}
                          color={theme?.colors?.black}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.marginBottom16}>
                    <Text
                      style={[
                        styles.referenceConnectionTextStyle,
                        {color: theme?.colors?.faOrange},
                      ]}>
                      {Localization.t('ReferenceScreenFix.ReferenceLink')}
                    </Text>
                    <View style={styles.referenceLinkContainer}>
                      <Text
                        style={[
                          styles.referenceLinkTextStyle,
                          {color: theme?.colors?.black},
                        ]}
                        numberOfLines={1}>
                        {`https://borsa.bitci.com/register?ref=${RefferalProgramCode}`}
                      </Text>
                      <TouchableOpacity
                        onPress={() => this.referenceLinkCopy()}>
                        <FAVectorIcon
                          group={'FontAwesome'}
                          iconName={'copy'}
                          size={16}
                          color={theme?.colors?.black}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={[
                      styles.CommissionInformationContainer,
                      {backgroundColor: theme?.colors?.vGray},
                    ]}>
                    <View style={styles.CommissionRatioContainer}>
                      <Text
                        style={[
                          styles.CommissionRatioHeaderStyle,
                          {color: theme?.colors?.darkBlue},
                        ]}>
                        {Localization.t('ReferenceScreenFix.CommissionRatio')}
                      </Text>
                      <Text
                        style={[
                          styles.CommissionRatioTextStyle,
                          {color: theme?.colors?.black},
                        ]}>
                        {` %${ReferralCommissionPercantage || '0'}`}
                      </Text>
                    </View>
                    <View style={styles.TotalReferenceContainer}>
                      <Text
                        style={[
                          styles.TotalReferenceHeaderStyle,
                          {color: theme?.colors?.darkBlue},
                        ]}>
                        {Localization.t('ReferenceScreenFix.TotalReference')}
                      </Text>
                      <Text
                        style={[
                          styles.TotalReferenceTextStyle,
                          {color: theme?.colors?.black},
                        ]}>
                        {TotalFriendsCount || '0'}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={[
                  styles.ButtonsContainer,
                  {backgroundColor: theme?.colors?.blueGray},
                ]}>
                <FAButton
                  leftIconColor={'#767D8D'}
                  leftIconName={'share-square'}
                  leftIconSize={22}
                  disabled={false}
                  onPress={() => this.shareOptions()}
                  containerStyle={styles.shareButtonContainer}
                  textStyle={[
                    styles.shareButtonTextStyle,
                    {color: theme?.colors?.darkBlue},
                  ]}
                  text={Localization.t('ReferenceScreenFix.Share')}
                />
                <FAButton
                  leftIconColor={'#767D8D'}
                  leftIconName={'qrcode'}
                  leftIconSize={22}
                  disabled={false}
                  onPress={() =>
                    this.setState({
                      showModal: true,
                    })
                  }
                  containerStyle={styles.QrCodeButtonContainer}
                  textStyle={[
                    styles.QrCodeTextStyle,
                    {color: theme?.colors?.darkBlue},
                  ]}
                  text={Localization.t('ReferenceScreenFix.QrCode')}
                />
              </View>

              <FAReferencesList
                data={FriendsData}
                onPress={inComingData =>
                  navigation.navigate('ReferenceScreenAllList', {
                    data: inComingData,
                  })
                }
              />

              <FAReferencesList
                data={CommssionsData}
                save
                onPress={(inComingData, inComingSave) =>
                  navigation.navigate('ReferenceScreenAllList', {
                    data: inComingData,
                    save: inComingSave,
                  })
                }
              />
              <View style={styles.marginBottom10} />
            </ScrollView>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
