import React from 'react';
import {Text, View} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import {connect} from 'react-redux';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
const NFTScreen = props => {
  const {navigation} = props;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <FAStandardHeader title={'NFT'} navigation={navigation} />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                letterSpacing: 4,
                color: theme?.colors?.black,
              }}>
              SOON...
            </Text>
          </View>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(NFTScreen);
