import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  pageheaderStyle: {
    height: '100%',
    paddingLeft: 16,
    paddingRight: 20,
    justifyContent: 'center',
  },
  renderItemContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#F7F7F7', // TO DO COLOR
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignContent: 'center',
  },
  dateTextStyle: {fontSize: 12, color: '#3B3B3B', flex: 1}, // TO DO COLOR
  nameAndSurname: {
    fontSize: 12,
    color: '#3B3B3B', // TO DO COLOR
    flex: 1,
  },
  WinningCommissionTextStyle: {
    fontSize: 12,
    color: '#3B3B3B', // TO DO COLOR
    flex: 1,
    textAlign: 'right',
  },
  mainContainer: {backgroundColor: FAColor.White, flex: 1},
  sortingContainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
    backgroundColor: FAColor.VGray,
  },
  flex1: {flex: 1},
  sortingNameAndSurnameContainerStyle: {
    backgroundColor: FAColor.VGray,
    flex: 1,
  },
  noReferenceTextStyle: {textAlign: 'center', marginTop: 5},
});
