import React, {Component} from 'react';
import {View, Text, SafeAreaView, FlatList} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import FASorting from '@Components/Composite/FASorting';
import FAColor from '@Commons/FAColor';
import FAService from '@Services';
import autobind from 'autobind-decorator';
import {styles} from './assets/styles';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

export default class ReferencesScreenAllList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      save:
        (this.props.route &&
          this.props.route.params &&
          this.props.route.params.save) ||
        null,
      currentPage: 1,
      data: [],
      filterLogic: {
        date: true,
        nameAndSurname: undefined,
        earn: undefined,
      },
    };
  }

  componentWillMount() {
    this.setState({isLoading: true});
    this.fetchAllList();
  }

  @autobind
  onEndReached() {
    this.fetchAllList();
  }

  @autobind
  fetchAllList() {
    const {save} = this.state;
    const {currentPage} = this.state;
    if (save) {
      const requestBody = {
        StartDate: moment().subtract(1, 'years').format('YYYY-MM-DD'),
        Filters: [],
        IsActive: true,
        PageNumber: currentPage,
        Size: 25,
        EndDate: moment().format('YYYY-MM-DD'),
      };

      FAService.ReferralCommision(requestBody)
        .then(response => {
          if (response && response.data && response.status === 200) {
            this.setState({
              data: _.concat(
                this.state.data,
                response.data.Data.ReferralCommissions,
              ),

              currentPage: currentPage + 1,
              isLoading: false,
            });
          } else {
            this.setState({
              isLoading: false,
              showDialog: true,
              serviceMessage: response.data.Message,
            });
          }
        })

        .catch(error => {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
          });
        });
    } else {
      const requestBodyForFriends = {
        StartDate: moment().subtract(1, 'years').format('YYYY-MM-DD'),
        Filters: [],
        IsActive: true,
        PageNumber: currentPage,
        Size: 25,
        EndDate: moment().format('YYYY-MM-DD'),
      };
      FAService.ReferralProgramFriends(requestBodyForFriends)
        .then(response => {
          if (
            response &&
            response.data &&
            response.data.IsActive &&
            response.status === 200
          ) {
            this.setState({
              data: _.concat(
                this.state.data,
                response.data.Data.ReferralFriends,
              ),
              currentPage: currentPage + 1,
              isLoading: false,
            });
          } else {
            this.setState({
              isLoading: false,
              showDialog: true,
              serviceMessage: response.data.Message,
            });
          }
        })
        .catch(error => {
          this.setState({
            isLoading: false,
            showDialog: true,
            serviceMessage: Localization.t('CommonsFix.UnexpectedError'),
          });
        });
    }
  }

  filterAction(List) {
    if (List && List.length > 0) {
      let filteredList = List;
      const {filterLogic} = this.state;
      const {date, nameAndSurname, earn} = filterLogic;

      if (date !== undefined) {
        if (date) {
          filteredList = _.sortBy(filteredList, 'date');
        } else {
          filteredList = _.sortBy(filteredList, 'date').reverse();
        }
      } else if (nameAndSurname !== undefined) {
        if (nameAndSurname) {
          filteredList = _.sortBy(filteredList, 'nameAndSurname').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'nameAndSurname');
        }
      } else if (earn !== undefined) {
        if (earn) {
          filteredList = _.sortBy(filteredList, 'earn').reverse();
        } else {
          filteredList = _.sortBy(filteredList, 'earn');
        }
      }

      return filteredList;
    }
    return List;
  }

  renderItem(item, theme) {
    const {save} = this.state;
    return (
      <View
        style={[
          styles.renderItemContainer,
          {borderColor: theme?.colors?.blueGray},
        ]}>
        <Text style={[styles.dateTextStyle, {color: theme?.colors?.darkBlack}]}>
          {moment(item.CreatedOn).format('YYYY-MM-DD')}
        </Text>
        <Text
          style={[
            styles.nameAndSurname,
            {
              textAlign: save ? 'center' : 'right',
              color: theme?.colors?.darkBlack,
            },
          ]}>
          {item.NameSurname}
        </Text>
        {save ? (
          <Text
            style={[
              styles.WinningCommissionTextStyle,
              {color: theme?.colors?.darkBlack},
            ]}>
            {item.WinningCommission}
          </Text>
        ) : null}
      </View>
    );
  }

  render() {
    const {filterLogic, save, data} = this.state;
    const {navigation} = this.props;
    let filteredList;
    filteredList = this.filterAction(data);
    filteredList.reverse();

    const headerTitle = this.state.save
      ? Localization.t('ReferenceScreenFix.MyProfit')
      : Localization.t('ReferenceScreenFix.Myreferences');

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.white},
            ]}>
            <FAStandardHeader title={headerTitle} navigation={navigation} />
            <View
              style={[
                styles.sortingContainer,
                {backgroundColor: theme?.colors?.vGray},
              ]}>
              <FASorting
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: flag,
                        nameAndSurname: undefined,
                        earn: undefined,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                initialValue={true}
                value={filterLogic.date}
                containerStyle={styles.flex1}
                text={Localization.t('CommonsFix.Date')}
              />
              <FASorting
                onPress={flag =>
                  this.setState(
                    {
                      filterLogic: {
                        date: undefined,
                        nameAndSurname: flag,
                        earn: undefined,
                      },
                    },
                    () => this.filterAction(),
                  )
                }
                value={filterLogic.nameAndSurname}
                containerStyle={[
                  styles.sortingNameAndSurnameContainerStyle,
                  {
                    marginRight: save ? 20 : null,
                    alignItems: save ? null : 'flex-end',
                    backgroundColor: theme?.colors?.vGray,
                  },
                ]}
                text={Localization.t('CommonsFix.NameAndSurname')}
              />
              {save ? (
                <FASorting
                  onPress={flag =>
                    this.setState(
                      {
                        filterLogic: {
                          date: undefined,
                          nameAndSurname: undefined,
                          earn: flag,
                        },
                      },
                      () => this.filterAction(),
                    )
                  }
                  containerStyle={{
                    backgroundColor: theme?.colors?.vGray,
                  }}
                  value={filterLogic.earn}
                  text={Localization.t('ReferenceScreenFix.Profit')}
                />
              ) : null}
            </View>
            {!_.isEmpty(filteredList) &&
            !_.isNil(filteredList) &&
            !_.isNull(filteredList) ? (
              <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal={false}
                showsVerticalScrollIndicator={false}
                data={filteredList}
                renderItem={({item}) => this.renderItem(item, theme)}
                keyExtractor={(item, index) => index}
                onEndReached={this.onEndReached}
                onEndReachedThreshold={0.2}
              />
            ) : (
              <Text style={styles.noReferenceTextStyle}>
                {Localization.t('ReferenceScreenFix.ThereIsNoReference')}
              </Text>
            )}
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
