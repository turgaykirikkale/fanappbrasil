import React, {Component} from 'react';
import {View, FlatList, Text} from 'react-native';
import FAAssetShower from '@Components/Composite/FAAssetShower';
import FAPollSelection from '@Components/Composite/FAPollSelection';
import autobind from 'autobind-decorator';
import FAPollImage from '@Components/Composite/FAPollImage';
import FAButton from '@Components/Composite/FAButton';
import {styles} from './assets/styles';
import FAService from '@Services';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import {subscribeVoteQuizProcess, unSubscribeVoteQuizProcess} from '@Stream';
import FAAlertModal from '@Components/Composite/FAAlertModal';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Toast from 'react-native-toast-message';
import Localization from '@Localization';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FACurrencyAndCoinFormatter from '@Commons/FACurrencyFormat';
import {AppConstants} from '@Commons/Contants';
import {CommonActions} from '@react-navigation/native';
import FADrawImage from '@Components/Composite/FADrawImage';

export default class SurveyScreenDetail extends Component {
  constructor(props) {
    console.log(props, 'THİSPROPS');
    super(props);
    this.state = {
      selectedItems: [],
      selected: null,
      voted:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.event &&
        this.props.route.params.event.Voted,
      route:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.routeMain,

      totalVotedNumber: 100,
      iconActive: false,
      data:
        this.props &&
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.event,
      errorMessage: null,
      showErrorMessage: false,
      serviceLoading: false,
    };
  }

  @autobind
  selectedItems(inComingId) {
    const {selectedItems, data} = this.state;

    if (data.MultipleSelectionEnabled) {
      let VotedArray = selectedItems;
      if (!VotedArray.includes(inComingId)) {
        VotedArray.push(inComingId);
      } else {
        const index = VotedArray.indexOf(inComingId);
        VotedArray.splice(index, 1);
      }

      this.setState({
        selectedItems: VotedArray,
        iconActive: true,
      });
    } else {
      this.setState({
        selected: inComingId,
        iconActive: true,
      });
    }
  }

  componentDidMount() {
    subscribeVoteQuizProcess(data => {
      if (data?.Success) {
        Toast.show({
          type: 'success',
          position: 'top',
          text1: Localization.t('CommonsFix.Succces'),
          text2: Localization.t('CommonsFix.SuccessfulTransaction'),
          visibilityTime: 2000,
          autoHide: true,
          topOffset: 60,
          bottomOffset: 40,
          onShow: () => {},
          onHide: () => {},
          onPress: () => {},
        });
        this.quizResults();
      } else {
        this.setState({
          serviceLoading: false,
          errorMessage: data.Message,
          showErrorMessage: true,
        });
      }
    });
  }

  @autobind
  quizResults() {
    const {data} = this.state;
    const requestBody = {
      quizId: data.QuizId,
      resource: 'en',
    };
    FAService.QuizResults(requestBody)
      .then(response => {
        if (response && response.data && response.status === 200) {
          console.log('response quizresult: ', response);
        } else {
          console.log('response quizresult', response);
        }
      })
      .catch(error => {});
  }

  @autobind
  voteServiceCall() {
    const {data, selected, selectedItems} = this.state;
    this.setState({serviceLoading: true});
    const QuizOptionVote = data.MultipleSelectionEnabled
      ? selectedItems
      : [selected];
    const requestBody = {
      QuizId: data.QuizId,
      QuizOptionIds: QuizOptionVote,
    };

    FAService.VoteQuiz(requestBody)
      .then(response => {
        if (
          response &&
          response.data &&
          response.status === 200 &&
          response.data.IsSuccess
        ) {
          this.setState({serviceLoading: false, voted: true});
        } else {
          this.setState({
            serviceLoading: false,
            errorMessage: response.data.Message,
            showErrorMessage: true,
          });
        }
      })
      .catch(error => {});
  }

  componentWillUnmount() {
    unSubscribeVoteQuizProcess();
  }

  @autobind
  renderItem(item) {
    const {selected, totalVotedNumber, voted, selectedItems, data} = this.state;
    return (
      <FAPollSelection
        isPassive={!data.Active}
        voted={!data.Active || data.DaysLeft === 0 || voted}
        totalVotedNumber={totalVotedNumber}
        selected={selected}
        selectedItems={selectedItems}
        MultipleSelectionEnabled={data.MultipleSelectionEnabled}
        item={item}
        onPressSelect={inComingId => this.selectedItems(inComingId)}
      />
    );
  }

  render() {
    const {
      voted,
      data,
      selectedItems,
      selected,
      showErrorMessage,
      errorMessage,
      serviceLoading,
      route,
    } = this.state;

    const {UserBalance} = this.props?.route?.params;
    const {navigation} = this.props;
    let buttonDisabled = true;
    let CoinBalance;
    let currencyBalance;
    let CoinCode;
    if (UserBalance && data) {
      UserBalance.map(item => {
        if (item.CoinId === data.CoinId) {
          if (item.CoinBalance < 1 || item.CoinBalance === 0) {
            CoinBalance = '0.00';
            CoinCode = item.CoinCode;
          } else {
            CoinCode = item.CoinCode;
            CoinBalance = FACurrencyAndCoinFormatter(
              item.CoinBalance || 0.0,
              AppConstants.CoinDecimalCount,
            );
          }
        } else if (item.CoinCode === AppConstants.CurrencyCode) {
          currencyBalance = FACurrencyAndCoinFormatter(
            item.CoinBalance || 0.0,
            AppConstants.CoinDecimalCount,
          );
        }
      });
    }

    if (selected !== null || selectedItems.length > 0 || voted) {
      if (!data.Active) {
        buttonDisabled = true;
      }
      buttonDisabled = false;
    }

    let buttonText = Localization.t('SurveyScreenDetail.SendVote');

    if (voted) {
      debugger;
      buttonText = Localization.t('SurveyScreenDetail.Surveys');
    } else if (!data.Active || data.DaysLeft === 0) {
      debugger;
      buttonText = Localization.t('SurveyScreenDetail.EndedSurvey');
    }

    return (
      <ThemeContext.Consumer>
        {({theme}) => (
          <View
            style={[
              styles.mainContainer,
              {backgroundColor: theme?.colors?.vGray},
            ]}>
            <FAStandardHeader
              title={Localization.t('SurveyScreenDetail.header')}
              navigation={navigation}
            />
            <FAFullScreenLoader isLoading={serviceLoading} />
            <FAAlertModal
              visible={showErrorMessage}
              text={errorMessage}
              iconName={'times-circle'}
              header={Localization.t('Commons.Error')}
              onPress={() => this.setState({showErrorMessage: false})}
            />
            <FAAssetShower
              fanTokenBalance={CoinBalance || '0.00'}
              CoinCode={CoinCode}
              currencyBalance={currencyBalance || '0.00'}
              onPress={flow => {
                flow === 1
                  ? navigation.navigate('LoginScreen')
                  : flow === 2
                  ? navigation.navigate('RegisterStepOne')
                  : navigation.navigate('WalletTabStackNavigator');
              }}
            />
            <FADrawImage
              type={0}
              CoinCode={CoinCode}
              price={String(data?.Price)}
              date={data.DaysLeft}
              // time={hour}
              explainedText={data.Title}
              imageUrl={data.ImageUrl}
            />
            <View
              style={{
                flex: 1,
                backgroundColor: theme?.colors?.white,
                marginHorizontal: 10,
                marginTop: 10,
                borderRadius: 4,
              }}>
              <FlatList
                style={{borderRadius: 4}}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={data.QuizOptionList}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={(item, index) => index}
              />
            </View>
            <FAButton
              disabled={buttonDisabled}
              onPress={() =>
                voted
                  ? navigation.dispatch(
                      CommonActions.reset({
                        index: 1,
                        routes: [
                          {name: route},
                          {
                            name: 'SurveysScreen',
                            params: {routeMain: route},
                          },
                        ],
                      }),
                    )
                  : this.voteServiceCall()
              }
              containerStyle={{
                backgroundColor:
                  !data.Active || voted || data.DaysLeft === 0
                    ? theme?.colors?.black2
                    : theme?.colors?.orange,
              }}
              textStyle={[
                styles.buttonTextStyle,
                {color: theme?.colors?.white2},
              ]}
              text={buttonText}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}
