import {StyleSheet} from 'react-native';
import FAColor from '@Commons/FAColor';

export const styles = StyleSheet.create({
  mainContainer: {flex: 1},
  buttonTextStyle: {
    color: FAColor.White,
    fontWeight: '600',
    letterSpacing: -0.8,
    marginVertical: 16,
  },
});
