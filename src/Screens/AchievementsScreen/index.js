import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import TasksHeader from '../../Components/Composite/TasksHeader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAStandardHeader from '@Components/Composite/FAStandardHeader';
import FACustomTabView from '../../Components/Composite/FACustomTabView';
import FAButton from '../../Components/Composite/FAButton';
import Localization from '@Localization';

const AchievementsScreen = props => {
  const screenWidth = {width: Dimensions.get('window').width}.width;

  const [tabIndex, settabIndex] = useState(0);
  const [categoriIndex, setCategoriIndex] = useState(0);
  const routes = [
    {key: 'all', title: 'Tamamlanan'},
    {key: 'favorites', title: 'Özel'},
  ];
  const CategoriesData = [
    {Name: 'Hepsi', index: 0},
    {Name: 'Futbol', index: 1},
    {Name: 'Basketbol', index: 2},
    {Name: 'Motor Sporları', index: 3},
    {Name: 'Oyun', index: 4},
  ];
  const data = [
    {
      image: require('../../Assets/images/QuestionSymbol.jpeg'),
      header: '1000 Gol Barajı',
      content: 'Tüm Brezilya Token görevlerini tamamla.',
    },
    {
      image: require('../../Assets/images/QuestionSymbol.jpeg'),
      header: '1000 Gol Barajı',
      content: '1000 kez Brezilya Token alış veya satış işlemi gerçekleştir.',
    },
    {
      image: require('../../Assets/images/QuestionSymbol.jpeg'),
      header: '1000 Gol Barajı',
      content: 'Tüm Brezilya Token görevlerini tamamla.',
    },
  ];
  const {navigation} = props;
  const passive = true;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
          <FAStandardHeader title={'GÖREVLER'} navigation={navigation} />
          {passive ? (
            <View
              style={{
                marginTop: 70,
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0, 0.6)',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 99,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white'}}>
                {Localization.t('FanScreen.Soon')}
              </Text>
            </View>
          ) : null}
          <ScrollView>
            <TasksHeader headerUp={'Standart'} headerBottom={'Özel'} />
            <View>
              <FACustomTabView
                currentIndex={tabIndex}
                routes={routes}
                onIndexChange={indexValue => settabIndex(indexValue)}
                itemCountPerPage={2}
              />
            </View>
            <ScrollView
              horizontal
              style={{backgroundColor: theme?.colors?.gray, paddingRight: 10}}>
              {CategoriesData.map((item, index) => {
                return (
                  <>
                    <TouchableOpacity
                      onPress={() => setCategoriIndex(item.index)}
                      style={{
                        marginLeft: 10,
                        borderRadius: 4,
                        backgroundColor: theme?.colors?.white,
                        paddingHorizontal: 8,
                        paddingVertical: 6,
                        marginVertical: 10,
                      }}>
                      <Text
                        style={{
                          color: theme?.colors?.black,
                          fontSize: 12,
                        }}>
                        {item.Name}
                      </Text>
                    </TouchableOpacity>
                  </>
                );
              })}
            </ScrollView>
            <View
              style={{
                marginVertical: 10,
                backgroundColor: theme?.colors?.white,
              }}>
              {data.map((item, index) => {
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 10,
                      borderBottomWidth: 2,
                      borderColor: theme?.colors?.blueGray,
                      paddingVertical: 10,
                      paddingHorizontal: 10,
                    }}>
                    <Image
                      resizeMode={'cover'}
                      style={{
                        borderRadius: 4,
                        width: screenWidth / 3,
                        height: screenWidth / 2.28,
                        borderWidth: 2,
                        borderColor: theme?.colors?.blueGray,
                      }}
                      source={item.image}
                    />

                    <View style={{marginLeft: 10, flex: 1}}>
                      <Text
                        style={{
                          fontSize: 16,
                          fontWeight: 'bold',
                          color: theme?.colors.black,
                        }}>
                        {item.header}
                      </Text>
                      <View
                        style={{
                          marginTop: 9,
                          borderWidth: 1,
                          borderColor: theme?.colors?.blueGray,
                        }}
                      />
                      <Text
                        style={{
                          marginTop: 10,
                          fontSize: 14,
                          color: theme?.colors.darkBlue,
                        }}>
                        {item.content}
                      </Text>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          alignSelf: 'flex-end',
                        }}>
                        <Text style={{color: theme?.colors?.green}}>
                          Daha Fazla >
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default AchievementsScreen;
