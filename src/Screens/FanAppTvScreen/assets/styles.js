import {StyleSheet} from 'react-native';
import {BlueGray} from '@Commons/FAColor';

export const styles = StyleSheet.create({
  fanCardListContainer: {marginTop: 10},
  mainScrollView: {flex: 1, backgroundColor: BlueGray},
  mainContainer: {marginTop: 12, marginHorizontal: 9},
  sliderContainer: {
    borderRadius: 5,
  },
  firstFanVideoListContainer: {marginTop: 12},
  marginTop20: {marginTop: 20},
  marginTop10: {marginTop: 10},
  marginBottom20: {marginBottom: 20},
});
