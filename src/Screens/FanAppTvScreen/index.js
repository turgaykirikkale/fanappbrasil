import FAListHeader from '@Components/Composite/FAListHeader';
import {
  Dimensions,
  Image,
  RefreshControl,
  ScrollView,
  View,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {styles} from './assets/styles';
import FAService from '@Services';
import _ from 'lodash';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FADrawerHeader from '../../Components/Composite/FADrawerHeader';
import {connect} from 'react-redux';
import UploadUserInformationPage from '../UploadUserInformationPage';
import FAFanCard from '@Components//UI/FAFanCard';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import Localization from '@Localization';

const FanAppTvScreen = props => {
  const [showUserUploadInformModal, setshowUserUploadInformModal] = useState(
    props?.UserState?.UserInfo?.CustomerStatusEnumId < 10 ? true : false,
  );
  const {navigation} = props;
  const [categories, setCategories] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [serviceLoading, setServiceLoading] = useState(true);

  useEffect(() => {
    fillVideoList();
  }, []);

  const onRefresh = () => {
    fillVideoList(true);
  };

  const fillVideoList = (onRefresh = false) => {
    if (onRefresh) {
      setRefreshing(true);
    }
    FAService.GetMainPageVideos()
      .then(res => {
        if (res?.data) {
          const resData = res.data;
          const categoryList = _.filter(resData, category => {
            return !_.isEmpty(category?.Videos);
          });
          setCategories(categoryList || []);
          setServiceLoading(false);
        }
        if (onRefresh) {
          setRefreshing(false);
        }
      })
      .catch(err => console.log(err));
  };

  const screenWidth = Dimensions.get('screen').width;
  console.log(categories);

  return (
    <>
      <FADrawerHeader
        title={'CBF TV'}
        navigation={navigation}
        routeMain={props.route.name}>
        <ThemeContext.Consumer>
          {({theme}) => (
            <>
              {showUserUploadInformModal ? (
                <UploadUserInformationPage
                  closeModal={() => setshowUserUploadInformModal(false)}
                  navigateSettingScreen={() =>
                    navigation.navigate('AccountSettings')
                  }
                />
              ) : (
                <>
                  <FAFullScreenLoader isLoading={serviceLoading} />
                  <ScrollView
                    refreshControl={
                      <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                      />
                    }
                    style={[
                      styles.mainScrollView,
                      {backgroundColor: theme?.colors?.blueGray},
                    ]}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.mainContainer}>
                      <View>
                        <Image
                          style={{
                            height: screenWidth / 2,
                            borderRadius: 6,
                            width: '100%',
                          }}
                          // resizeMode="cover"
                          source={require('./assets/BrasilTeam2.jpeg')}
                        />
                      </View>
                      {_.map(categories, category => {
                        const randomColor = generateRandomColor();
                        return (
                          <View style={{}}>
                            <View
                              style={{
                                backgroundColor: theme?.colors?.white,
                                paddingVertical: 10,
                                borderRadius: 5,
                                paddingHorizontal: 10,
                                marginTop: 10,
                              }}>
                              <FAListHeader
                                title={category?.CategoryName}
                                fontColor={randomColor}
                                onPress={() =>
                                  navigation.navigate('VideoGalleryScreen', {
                                    category,
                                  })
                                }
                              />
                            </View>
                            <View style={{marginVertical: 10}}>
                              <FlatList
                                ItemSeparatorComponent={() => (
                                  <View style={{marginLeft: 10}} />
                                )}
                                keyExtractor={(item, index) => index}
                                showsHorizontalScrollIndicator={false}
                                data={category?.Videos}
                                renderItem={({index, item}) => (
                                  <FAFanCard
                                    onPress={() =>
                                      navigation.navigate('VideoScreen', {
                                        video: item,
                                        // category: category,
                                      })
                                    }
                                    image={item.ImageUrl || null}
                                    text={item.Title}
                                    cardRatio={3}
                                  />
                                )}
                                horizontal
                              />
                            </View>
                          </View>
                        );
                      })}
                    </View>
                  </ScrollView>
                </>
              )}
            </>
          )}
        </ThemeContext.Consumer>
      </FADrawerHeader>
    </>
  );
};

const mapStateToProps = state => {
  return {
    UserState: state.UserState,
  };
};

export default connect(mapStateToProps, {})(FanAppTvScreen);
