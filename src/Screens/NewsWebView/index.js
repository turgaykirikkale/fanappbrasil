import React, {useState} from 'react';
import {View, ActivityIndicator, Dimensions} from 'react-native';
import {WebView} from 'react-native-webview';
import FAStandardHeader from '../../Components/Composite/FAStandardHeader';
import FAFullScreenLoader from '@Components/UI/FAFullScreenLoader';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FAColor from '../../Commons/FAColor';

const NewsWebView = props => {
  const [loading, setLoading] = useState(true);
  const screenWidth = Dimensions.get('window').width;
  const item = props?.route?.params?.item;
  setTimeout(() => {
    setLoading(false);
  }, 5000);
  const LoadingIndicatorView = () => {
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
          alignSelf: 'center',
          position: 'absolute',
          justifyContent: 'center',
          backgroundColor: 'rgba(0,0,0,0.6)',
        }}>
        <ActivityIndicator color={FAColor.Orange} size="large" style={{}} />
      </View>
    );
  };
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.blueGray}}>
          <FAStandardHeader
            title={item.LongName}
            navigation={props.navigation}
          />
          <WebView
            source={{uri: item.Navigation, 'Accept-Language': `en`}}
            renderLoading={LoadingIndicatorView}
            startInLoadingState={true}
            style={{backgroundColor: theme?.colors?.blueGray}}
            // onMessage={event => console.log(event.nativeEvent.data)}
          />
        </View>
      )}
    </ThemeContext.Consumer>
  );
};
export default NewsWebView;
