export default class CentrifugeChannel {
  /* INSTANCE PROPERTIES */

  constructor(channel_name, centrifuge) {
    this.channel_name = channel_name;
    const self = this;
    this.subscription = centrifuge.subscribe(channel_name, function (result) {
      const {event, data} = result.data;
      self.onEvent(event, data);
    });
  }

  channel_name = '';
  subscription = {};
  bindings = {};

  bind(eventName, callback) {
    this.bindings[eventName] = callback;
  }

  onEvent(eventName, data) {
    if (this.bindings[eventName]) {
      this.bindings[eventName](data);
    }
  }

  unsubscribe() {
    console.log(this.subscription);
    this.subscription.unsubscribe();
  }
}
