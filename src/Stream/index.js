import Pusher from 'pusher-js/react-native';
import store from '@GlobalStore/store';
import {createCentrifuge} from './centrifuge';

let joinActivityProcessChannelName = null;
let VoteQuizProcessChannelName = null;
let activeMarketHistoryChannelName = null;
let activeOrderTickerChannelName = null;
let privateCustomerActiveOrdersChannelName = null;
let privateCustomerAllActiveOrdersChannelName = null;
let moneyProcessWithdrawChannelName = null;
let coinProcessWithdrawChannelName = null;
let SocketConfigs = {};
let StreamFlowType = 1;
let centrifugeStream = null;

const StreamSubscribe = channelName => {
  if (StreamFlowType === 1) {
    return socket.subscribe(channelName);
  } else {
    debugger;
    return centrifugeStream.subscribe(channelName);
  }
};

const StreamUnSubscribe = channelName => {
  if (StreamFlowType === 1) {
    return socket.unsubscribe(channelName);
  } else {
    console.log('centrifugeStream = ', centrifugeStream);
    return centrifugeStream.unsubscribe(channelName);
  }
};

export const setSocketconfigs = async incomingConfigFromService => {
  SocketConfigs = incomingConfigFromService;
  if (SocketConfigs && typeof SocketConfigs.ProviderEnumTypeId === 'number') {
    if (SocketConfigs.ProviderEnumTypeId === 1) {
      //   StreamFlowType = Stream_Pusher; Todo Delete it !!!!!!!
      StreamFlowType = 2;
    } else {
      centrifugeStream = createCentrifuge(SocketConfigs);
      StreamFlowType = 2;
    }
  } else {
    StreamFlowType = 1;
  }
};

const sta = '87eb7f22133b429ca4c9';
const prod = 'c3215b90c3786a6650ff';
const socket = new Pusher(prod, {
  cluster: 'eu',
});
export const subscribeBitciTryTickerChannel = callbackFunc => {
  const channelName = 'bitci-ticker';
  const channel = StreamSubscribe(channelName);
  channel.bind('bitci-try-ticker', function (data) {
    callbackFunc(data);
  });
};

export const unSubscribeBitciTryTickerChannel = () => {
  StreamUnSubscribe('bitci-ticker');
};

export const subscribeOrderBookTickerChannel = (
  marketQueueName,
  callbackFunc,
) => {
  const channelName = `${marketQueueName}_tickerv4`;
  activeOrderTickerChannelName = channelName;
  const channel = StreamSubscribe(channelName);
  channel.bind('order-ticker', function (data) {
    callbackFunc(data);
  });
};
export const unSubscribeOrderBookTickerChannel = () => {
  if (activeOrderTickerChannelName) {
    StreamUnSubscribe(activeOrderTickerChannelName);
    activeOrderTickerChannelName = null;
  }
};

export const subscribeCustomerOrderTickerChannel = (
  marketQueueName,
  callbackFunc,
) => {
  debugger;
  const globalStore = store.getState();
  const PusherPrivateKey = globalStore?.UserState?.PusherPrivateKey;
  debugger;
  if (PusherPrivateKey && marketQueueName) {
    debugger;
    let newChannelName = `Private_${PusherPrivateKey}_${marketQueueName}_ticker`;
    privateCustomerActiveOrdersChannelName = newChannelName;
    const channel = StreamSubscribe(newChannelName);
    channel.bind('private-ticker', function (data) {
      callbackFunc(data);
    });
  }
};
export const unSubscribeCustomerOrderTickerChannel = () => {
  if (privateCustomerActiveOrdersChannelName) {
    StreamUnSubscribe(privateCustomerActiveOrdersChannelName);
  }
};

export const subscribeMarketHistoryChannel = (
  marketQueueName,
  callbackFunc,
) => {
  debugger;
  const channelName = `${marketQueueName}_tradev3`;
  debugger;
  activeMarketHistoryChannelName = channelName;
  debugger;
  const channel = StreamSubscribe(channelName);
  debugger;
  channel.bind('trade-ticker', function (data) {
    callbackFunc(data);
  });
};
export const unSubscribeMarketHistoryChannel = () => {
  if (activeMarketHistoryChannelName) {
    StreamUnSubscribe(activeMarketHistoryChannelName);
  }
};

export const subscribeJoinActivityProcess = callbackFunc => {
  const globalStore = store.getState();
  const PusherPrivateKey = globalStore?.UserState?.PusherPrivateKey;
  if (PusherPrivateKey) {
    const channelName = `Private_${PusherPrivateKey}_JoinActivityProcess`;
    const channel = StreamSubscribe(channelName);
    joinActivityProcessChannelName = channelName;
    channel.bind('JoinActivityProcess', function (data) {
      callbackFunc(data);
    });
  }
};
export const unSubscribeJoinActivityProcess = () => {
  if (joinActivityProcessChannelName) {
    StreamUnSubscribe(joinActivityProcessChannelName);
  }
};

export const subscribeVoteQuizProcess = callbackFunc => {
  const globalStore = store.getState();
  const PusherPrivateKey = globalStore?.UserState?.PusherPrivateKey;
  if (PusherPrivateKey) {
    const channelName = `Private_${PusherPrivateKey}_VoteQuizProcess`;
    const channel = StreamSubscribe(channelName);
    VoteQuizProcessChannelName = channelName;
    channel.bind('VoteQuizProcess', function (data) {
      callbackFunc(data);
    });
  }
};

export const unSubscribeVoteQuizProcess = () => {
  if (VoteQuizProcessChannelName) {
    StreamUnSubscribe(VoteQuizProcessChannelName);
  }
};
export const coinProcess = callbackFunc => {
  const globalStore = store.getState();
  const PusherPrivateKey = globalStore?.UserState?.PusherPrivateKey;
  if (PusherPrivateKey) {
    const channelName = `Private_${PusherPrivateKey}_CoinProcesses`;
    coinProcessWithdrawChannelName = channelName;
    const channel = StreamSubscribe(channelName);
    channel.bind('Withdraw', function (data) {
      callbackFunc(data);
    });
  }
};
export const coinProcessWithdrawUnsubscribe = () => {
  try {
    StreamUnSubscribe(coinProcessWithdrawChannelName);
  } catch (error) {}
};

export const customerAllOrderTickerChannel = callbackFunc => {
  const globalStore = store.getState();
  const PusherPrivateKey = globalStore?.UserState?.PusherPrivateKey;
  if (PusherPrivateKey) {
    let newChannelName = `Private_${PusherPrivateKey}_general_ticker`;
    privateCustomerAllActiveOrdersChannelName = newChannelName;
    const channel = StreamSubscribe(newChannelName);
    channel.bind('private-general-ticker', function (data) {
      callbackFunc(data);
    });
  }
};

export function unSubscribeCustomerAllOrderTickerChannel() {
  StreamUnSubscribe(privateCustomerAllActiveOrdersChannelName);
}

export const subscribeCoinMarketPriceTicker = callbackFunc => {
  const channel = StreamSubscribe('coin_rate');
  channel.bind('rate-ticker', function (data) {
    callbackFunc(data);
  });
};

export const unSubscribeCoinMarketPriceTicker = () => {
  StreamUnSubscribe('coin_rate');
};
