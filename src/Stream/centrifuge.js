import CentrifugeChannel from './CentrifugeChannel';
import Centrifuge from 'centrifuge';

export const createCentrifuge = function (config) {
  let options = {
    websocket: WebSocket,
  };
  console.log('config?.SocketUrl', config?.SocketUrl);
  let centrifuge = new Centrifuge(
    config?.SocketUrl || 'ws://35.207.163.151/connection/websocket',
    // options,
  );
  if (config?.AccessToken) {
    centrifuge.setToken(config.AccessToken);
  }
  centrifuge.connect();

  const appId = config?.AppId;
  const channels: {[channelName: string]: CentrifugeChannel} = {};

  return {
    subscribe: function (channelName: string) {
      channelName = `${appId}_${channelName}`;
      let channel = new CentrifugeChannel(channelName, centrifuge);
      channels[channelName] = channel;
      return channel;
    },
    unsubscribe: function (channelName: string) {
      if (channels && channels[channelName]) {
        channelName = appId + '_' + channelName;
        channels[channelName].unsubscribe();
      }
    },
  };
};
