import _ from 'lodash';

export const StatusControl = (enums, data, special) => {
  if (enums && data && special) {
    let filterEnumList = [];

    _.map(enums, item => {
      if (item[0].includes(special)) {
        filterEnumList.push(item);
      }
    });
    let fixedList = [];
    _.map(filterEnumList, item => {
      _.map(data, itemEd => {
        if (item[0].includes(itemEd[0])) {
          let FixedItem = {
            Name: item[1],
            Id: itemEd[1],
          };

          if (_.find(fixedList, {Name: FixedItem.Name})) {
          } else {
            fixedList.push(FixedItem);
          }
        }
      });
    });
    return fixedList;
  } else {
    return [];
  }
};
