import AsyncStorage from '@react-native-async-storage/async-storage';

export const setDataToStorage = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    return false;
  }
};

export const getDataFromStorage = async key => {
  try {
    const incomingData = await AsyncStorage.getItem(key);
    return incomingData;
  } catch (e) {
    return false;
  }
};

export const deleteAccessTokenFromStorage = async deleteCallBack => {
  try {
    // await AsyncStorage.removeItem();
    await AsyncStorage.removeItem('Token');
    deleteCallBack();
  } catch (e) {
    return false;
  }
};
