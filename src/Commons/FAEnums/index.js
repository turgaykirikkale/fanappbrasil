export const BUYFLOW = 1;
export const SELLFLOW = 2;
export const SMALL = 0;
export const MEDIUM = 1;
export const IDENTITYNUMBER = 1;
export const PASSPORT = 2;
export const OTHERS = 3;
export const WOMAN = 1;
export const MAN = 0;
export const LOGINFLOW = 1;
export const REGISTERFLOW = 2;

export const SmsProvider = {
  sms: 1,
  googleAuth: 2,
};

export const CustomerRegisterRedirect = {
  None: 0,
  ToSmsValidation: 1,
  ToEmailValidation: 2,
  ToIdentityInfoValidation: 3,
  ToFileUploadValidation: 4,
};

export const OrderType = {
  Buy: BUYFLOW,
  Sell: SELLFLOW,
};

export const StockMode = {
  EasyBuySell: 'easy-buy-sell',
  Standard: 'standard',
  Advanced: 'advanced',
};

export const AccountStatus = {
  Temp: 1,
  WaitingForSmsApproval: 2,
  SmsConfirmed: 3,
  WaitingForMailApproval: 4,
  MailConfirmed: 5,
  PendingCredentials: 6,
  CredentialComplete: 7,
  CustomerIdentificationFormWaiting: 8,
  CustomerIdentificationFormApproved: 9,
  DocumentApprovalWaiting: 10,
  DocumentApproved: 11,
  Active: 12,
  Locked: 90,
  BlackListed: 91,
  FinancialBlockage: 92,
  TradingBlockage: 93,
};
export const IdentityType = {
  New: 1,
  Old: 2,
  ResidencePermit: 3,
};

export default {
  BUYFLOW,
  SELLFLOW,
  SMALL,
  MEDIUM,
  IDENTITYNUMBER,
  PASSPORT,
  OTHERS,
  WOMAN,
  MAN,
  LOGINFLOW,
  REGISTERFLOW,
  CustomerRegisterRedirect,
  SmsProvider,
  StockMode,
  AccountStatus,
  IdentityType,
};
