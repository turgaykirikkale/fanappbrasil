export const ToFixed = x => {
  const castedValue = x && x.toString();
  if (
    castedValue &&
    castedValue.indexOf('e') &&
    castedValue.indexOf('e') > -1
  ) {
    try {
      if (Math.abs(x) < 1.0) {
        let e = parseInt(x.toString().split('e-')[1]);
        if (e) {
          x *= Math.pow(10, e - 1);
          x = '0.' + new Array(e).join('0') + x.toString().substring(2);
        }
      }
      return String(x);
    } catch (error) {
      return '';
    }
  }
  return x;
};

export const toFixedNoRounding = (incomingValue, n) => {
  try {
    if (incomingValue) {
      let value = ToFixed(incomingValue);
      const reg = new RegExp('^-?\\d+(?:\\.\\d{0,' + n + '})?', 'g');
      const a = value.toString().match(reg)[0];
      const dot = a.indexOf('.');
      if (dot === -1) {
        return a + '.00';
      } else {
        const b = n - (a.length - dot) + 1;
        return b > 0 ? a + '0' : a;
      }
    }
    return incomingValue;
  } catch (e) {
    return '0.00';
  }
};
