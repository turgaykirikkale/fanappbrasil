export const Orange = '#EA5607';
export const OHover = '#FF7B34';
export const SoftO = '#FFC4A5';
export const Black = '#000000';
export const DarkBlack = '#3B3B3B';
export const SoftBlack = '#333333';
export const DarkGray = '#717171';
export const LightGray = '#9B9B9B';
export const DarkBlue = '#767D8D';
export const DarkBlueO = '#BABBC0';
export const Gray = '#B1B0AF';
export const SoftGray = '#E3E2E2';
export const VBlueGray = '#EBEBF0';
export const VGray = '#F5F5F5';
export const Orangray = '#F8F5F4';
export const BlueGray = '#F8F9FB';
export const White = '#FFFFFF';
export const Green = '#03A66D';
export const Red = '#C90950';
export const ModeRed = '#F84960';
export const Warning = '#FFC12C';
export const Info = '#17A2B8';
export const Success = '#28A745';
export const SoftSuccess = '#9AE6AB';
export const Danger = '#DC3545';
export const FAOrange = '#EA5615';

export default {
  Orange,
  OHover,
  SoftO,
  Black,
  DarkBlack,
  DarkGray,
  DarkBlue,
  DarkBlueO,
  Gray,
  SoftGray,
  VBlueGray,
  VGray,
  Orangray,
  BlueGray,
  White,
  Green,
  Red,
  ModeRed,
  Warning,
  Info,
  Success,
  SoftSuccess,
  Danger,
  FAOrange,
  LightGray,
};
