import {SideMenuRoute} from '../Enums/SideMenuRoute';
import Localization from '@Localization';

//TODO(Ali Berk Yurtoğlu) Obje içlerindeki notlar
export const SideMenuRouteList = [
  {
    title: 'BFT TOKEN',
    stack: null,
    screen: SideMenuRoute.BitciCoinScreen,
    imageName: 'BFT',
    showAnonymous: true,
    //Token web sayfasına
    //Coin icon koyulacak
  },
  {
    title: Localization.t('SliderMenuRouteList.Store'),
    stack: null,
    screen: SideMenuRoute.FanTokenScreen,
    icon: 'FACooperCoinLine',
    showAnonymous: true,
  },
  // {
  //   title: 'HABERLER',
  //   stack: null,
  //   screen: SideMenuRoute.NewsScreen,
  //   icon: 'FANewsPaperLine',
  //   showAnonymous: true,
  // },
  {
    title: Localization.t('SliderMenuRouteList.Surveys'),
    stack: 'SurveyStack',
    screen: SideMenuRoute.SurveysScreen,
    icon: 'FAChatPollLine',
    showAnonymous: true,
  },
  {
    title: Localization.t('SliderMenuRouteList.Events'),
    stack: 'EventStack',
    screen: SideMenuRoute.EventsScreen,
    icon: 'FACalendarEventLine',
    showAnonymous: true,
  },
  {
    title: Localization.t('SliderMenuRouteList.FANNAPPTV'),
    stack: 'FanAppTvStack',
    screen: SideMenuRoute.FanAppTvScreen,
    icon: 'FATVLine',
    showAnonymous: true,
  },
  // {
  //   title: 'NFT STORE',
  //   stack: null,
  //   screen: SideMenuRoute.NftStoreScreen,
  //   icon: 'FAArtBoardLine',
  //   showAnonymous: true,
  // },
  // {
  //   title: 'FANAPP STORE',
  //   stack: null,
  //   screen: SideMenuRoute.FanAppStoreScreen,
  //   icon: 'FAStoreLine',
  //   showAnonymous: true,
  // },
  {
    title: Localization.t('SliderMenuRouteList.LiveBroadcast'),
    stack: null,
    screen: SideMenuRoute.MatchCenterScreen,
    icon: 'FAFootballLine',
    showAnonymous: true,
    //Icon değişecek. Brodage link
  },
  {
    title: Localization.t('SliderMenuRouteList.MatchCenter'),
    stack: null,
    screen: SideMenuRoute.MatchCenterScreen,
    icon: 'FAFootballLine',
    showAnonymous: true,
  },
  {
    title: Localization.t('SliderMenuRouteList.Club'),
    stack: null,
    screen: SideMenuRoute.TeamsScreen,
    icon: 'FATeamLine',
    showAnonymous: true,
    //Screen ve stack değiştirilecek
  },
  // {
  //   title: 'İKİNCİ EL PAZARI',
  //   stack: null,
  //   screen: SideMenuRoute.SecondHandMarketScreen,
  //   icon: 'FAAchieve',
  //   showAnonymous: true,
  // },
  // {
  //   title: 'TROLLBOX (Chat)',
  //   stack: null,
  //   screen: SideMenuRoute.TrollBoxChatScreen,
  //   icon: 'FAChatSmileLine',
  //   showAnonymous: true,
  // },
  {
    title: Localization.t('SliderMenuRouteList.Settings'),
    stack: 'SettingStack',
    screen: SideMenuRoute.SettingsScreen,
    icon: 'FASettingLine',
    showAnonymous: true,
    // Güvenlik, dil ayarları, tema, alarm, kripto çevirici, yardım, referans, komisyon
  },
];
