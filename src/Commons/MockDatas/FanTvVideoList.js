export const BftFanTvVideoList = [
  {
    Image:
      'https://p4.wallpaperbetter.com/wallpaper/612/331/706/soccer-neymar-brazil-national-football-team-hd-wallpaper-preview.jpg',
    Text: 'Futbol Takımımızın Deplasman Formasını Sen Belirle',
    Price: 5,
    CoinCode: 'BFT',
  },
  {
    Image:
      'https://p4.wallpaperbetter.com/wallpaper/612/331/706/soccer-neymar-brazil-national-football-team-hd-wallpaper-preview.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: null,
    CoinCode: 'BFT',
  },
  {
    Image:
      'https://p4.wallpaperbetter.com/wallpaper/612/331/706/soccer-neymar-brazil-national-football-team-hd-wallpaper-preview.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: 12,
    CoinCode: 'BFT',
  },
];

export const SnftFanTvVideoList = [
  {
    Image:
      'https://besthqwallpapers.com/Uploads/25-1-2019/78447/thumb2-4k-sergio-ramos-back-view-spain-national-team-football-stars.jpg',
    Text: 'Futbol Takımımızın Deplasman Formasını Sen Belirle',
    Price: null,
    CoinCode: 'SNFT',
  },
  {
    Image:
      'https://besthqwallpapers.com/Uploads/25-1-2019/78447/thumb2-4k-sergio-ramos-back-view-spain-national-team-football-stars.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: 15,
    CoinCode: 'SNFT',
  },
  {
    Image:
      'https://besthqwallpapers.com/Uploads/25-1-2019/78447/thumb2-4k-sergio-ramos-back-view-spain-national-team-football-stars.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: 2.5,
    CoinCode: 'SNFT',
  },
];

export const MclFanTvVideoList = [
  {
    Image: 'https://pbs.twimg.com/media/E1ltRmTXoAAgLkC.jpg',
    Text: 'Futbol Takımımızın Deplasman Formasını Sen Belirle',
    Price: null,
    CoinCode: 'MCL',
  },
  {
    Image: 'https://pbs.twimg.com/media/E1ltRmTXoAAgLkC.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: null,
    CoinCode: 'MCL',
  },
  {
    Image: 'https://pbs.twimg.com/media/E1ltRmTXoAAgLkC.jpg',
    Text: 'Neymar da Silva Santos Júnior ile fotoğraf çekilme şansı yakala',
    Price: 12,
    CoinCode: 'MCL',
  },
];

export const SliderList = [
  {
    UrlPath:
      'https://i.pinimg.com/originals/07/c8/60/07c86072de446e804ab30eaa44246ab5.jpg',
    Title: 'İspanya Milli Futbol Takımı',
    SubTitle: 'Resmi Token Sponsoru',
  },
  {
    UrlPath:
      'https://lh3.googleusercontent.com/proxy/1ynPCssujTY_RofWqsDwYH3FGY-1wJ5ksiaj9nKFmNK_5jkHYd0_q2sMHQrYrdNuV-_N5YbvR2NsJfKcnSTrwYBMZVdC1sw5jDxN24QKaEQuvtG_X2aINOXM-SQ',
    Title: 'Brezilya Milli Futbol Takımı',
    SubTitle: 'Resmi Token Sponsoru',
  },
  {
    UrlPath:
      'https://e0.365dm.com/20/02/2048x1152/skysports-wolves-molineux_4924623.jpg',
    Title: 'Wolves Futbol Takımı',
    SubTitle: 'Resmi Token Sponsoru',
  },
];
