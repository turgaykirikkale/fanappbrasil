export const AppConstants = {
  CoinId: 92,
  CoinCode: 'BFT',
  CoinName: 'Brezilya Fan Token',
  TeamName: 'Brasil Football Team',
  CoinDecimalCount: 2,

  CurrencyId: 6,
  CurrencyCode: 'BRL',
  CurrencyName: 'Brezilya Fan Token',
  CurrencyDecimalCount: 2,

  DefaultLanguage: 'en',
  UserAgent: 'BitciFanApp',
};
