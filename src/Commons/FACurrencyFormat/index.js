import _ from 'lodash';
import 'intl';
import 'intl/locale-data/jsonp/tr';

const trauncateFractionAndFormat = (parts, digits) => {
  return parts
    .map(({type, value}) => {
      if (type !== 'fraction' || !value || value.length < digits) {
        return value;
      }

      let retVal = '';
      for (
        let idx = 0, counter = 0;
        idx < value.length && counter < digits;
        idx++
      ) {
        if (value[idx] !== '0') {
          counter++;
        }
        retVal += value[idx];
      }
      return retVal;
    })
    .reduce((string, part) => string + part);
};
const formatter = new Intl.NumberFormat('tr-TR', {
  minimumFractionDigits: 0,
  maximumFractionDigits: 20,
});

const FACurrencyAndCoinFormatter = (
  incomingMoneyValue,
  incomingDecimalLimit,
) => {
  let seperatorIndex = 1;
  let limit = String(incomingDecimalLimit) ? incomingDecimalLimit : 2;
  let castedMoneyValue = String(incomingMoneyValue);
 
  if (castedMoneyValue && castedMoneyValue.indexOf('.') > -1) {
    let thousandsPart = castedMoneyValue.split('.')[0];
    let decimalPart = castedMoneyValue.split('.')[1];
    if (String(decimalPart)) {
      decimalPart = decimalPart.slice(0, limit);
    }
    let formattedThousandsPart = '';
    const thosudansPartLength = thousandsPart.length;
    _.forEachRight(thousandsPart, (item, index) => {
      formattedThousandsPart += String(item);
      if (seperatorIndex % 3 === 0 && index !== 0) {
        formattedThousandsPart += '.';
      }
      seperatorIndex += 1;
    });
    formattedThousandsPart = formattedThousandsPart
      .split('')
      .reverse()
      .join('');
    let result = String(formattedThousandsPart) + ',' + String(decimalPart);
    return result;
  } else {
    return incomingMoneyValue;
  }
};

export default FACurrencyAndCoinFormatter;
