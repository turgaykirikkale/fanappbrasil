// import BMALocalization from '@Localization';

const Countries = [
  // {
  //   id: 'tr',
  //   name: 'Türkçe',
  //   flag: 'Turkey',
  //   disabled: false,
  // },
  {
    id: 'en',
    name: 'English',
    flag: 'England',
    disabled: false,
  },
  {
    id: 'pt',
    name: 'Portuguese',
    flag: 'Portugal',
    disabled: false,
  },
  {
    id: 'es',
    name: 'Spanish',
    flag: 'Spain',
    disabled: true,
  },
  {
    id: 'ar',
    name: 'Arabic',
    flag: 'UnitedArabEmirates',
    disabled: true,
  },
  {
    id: 'az',
    name: 'Azerbaijani',
    flag: 'Azerbaijan',
    disabled: true,
  },
  {
    id: 'bg',
    name: 'Bulgarian',
    flag: 'Bulgaria',
    disabled: true,
  },
  {
    id: 'bs',
    name: 'Bosnaian',
    flag: 'BosniaandHerzegovina',
    disabled: true,
  },
  {
    id: 'zh',
    name: 'Chinese',
    flag: 'China',
    disabled: true,
  },
  {
    id: 'nl',
    name: 'Dutch',
    flag: 'Netherlands',
    disabled: true,
  },
  {
    id: 'fr',
    name: 'French',
    flag: 'France',
    disabled: true,
  },
  {
    id: 'de',
    name: 'German',
    flag: 'Germany',
    disabled: true,
  },
  {
    id: 'ja',
    name: 'Japanese',
    flag: 'Japan',
    disabled: true,
  },
  {
    id: 'ko',
    name: 'Korean',
    flag: 'SouthKorea',
    disabled: true,
  },
];

export default Countries;
