import _ from 'lodash';
import {toFixedWithoutRounding} from '@Commons/FAMath';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import {BUYFLOW, SELLFLOW} from '../FAEnums';

const mapIncomingOrderBookAndReturn = (orderBook, orderType) => {
  let generatedOrderBook = [];
  if (orderBook && orderBook.length && orderBook.length > 0) {
    _.each(orderBook, item => {
      if (item && item.indexOf(',') > -1) {
        let generatedNewOrderObject = {};
        const seperatedPrice = item.split(',');
        if (
          seperatedPrice &&
          seperatedPrice.length &&
          seperatedPrice.length > 0
        ) {
          let coinValue = seperatedPrice[1];
          generatedNewOrderObject.Type = orderType;
          generatedNewOrderObject.Price = seperatedPrice[0];
          generatedNewOrderObject.CoinValue = coinValue;
          generatedNewOrderObject.Total =
            generatedNewOrderObject.Price * generatedNewOrderObject.CoinValue;
          generatedOrderBook.push(generatedNewOrderObject);
        }
      }
    });
    return percentExecutor(generatedOrderBook);
  }
  return [];
};

export const orderBookDataRenderer = data => {
  const buyOrdersPureData = data && data.b ? data.b.split('|') : [];
  const sellOrdersPureData = data && data.s ? data.s.split('|') : [];

  let buyOrderBook = mapIncomingOrderBookAndReturn(buyOrdersPureData, BUYFLOW);
  let sellOrderBook = mapIncomingOrderBookAndReturn(
    sellOrdersPureData,
    SELLFLOW,
  );

  if (buyOrderBook && buyOrderBook.length && buyOrderBook.length > 0) {
    buyOrderBook = _.sortBy(buyOrderBook, [
      item => {
        return parseFloat(item.Price);
      },
    ]).reverse();
    buyOrderBook = buyOrderBook.slice(0, 15);
  }

  if (sellOrderBook && sellOrderBook.length && sellOrderBook.length > 0) {
    sellOrderBook = _.sortBy(sellOrderBook, [
      item => {
        return parseFloat(item.Price);
      },
    ]);
    sellOrderBook = sellOrderBook.slice(0, 15);
  }

  return {
    BuyOrderBook: buyOrderBook,
    SellOrderBook: sellOrderBook,
  };
};
