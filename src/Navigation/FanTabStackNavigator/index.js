import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import FanScreen from '@Screens/FanScreen';
import LoginScreen from '@Screens/LoginScreen';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';
import EventStackNavigator from '@Navigation/EventStackNavigator/index';
import SurveyStackNavigator from '@Navigation/SurveyStackNavigator/index';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
import EventsScreen from '@Screens/EventsScreen';
import SurveysScreen from '@Screens/SurveysScreen';
import SurveyScreenDetail from '@Screens/SurveyScreenDetail';
import SettingStackNavigator from '@Navigation/SettingStackNavigator/index';
import CustomerOrderListScreen from '@Screens/CustomerOrderListScreen';
import StakingScreen from '@Screens/StakingScreen';
import StakeScreenDetail from '@Screens/StakeScreenDetail';
import WebviewScreen from '@Screens/WebviewScreen';
import SoNearScreen from '@Screens/SoNearScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';
import ResetCredentialExecutor from '@Screens/ResetCredentialExecutor';
import NFTCollections from '@Screens/NFTCollections';
import TasksScreen from '@Screens/TasksScreen';
import AchievementsAndCollections from '../../Screens/AchievementsAndCollections';
import AchievementsScreen from '../../Screens/AchievementsScreen';
import FanTokenInfoScreen from '@Screens/FanTokenInfoScreen';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator/index';
import VolumeCompetitions from '../../Screens/VolumeCompetetitionScreens';
import VolumeCompetetitionDetailScreen from '../../Screens/VolumeCompetitionDetailScreen';
//
import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import SelectLanguageScreen from '@Screens/SelectLanguageScreen';
import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import UploadUserInformationPage from '../../Screens/UploadUserInformationPage';
import ReferenceScreen from '@Screens/ReferenceScreen';
import ReferenceScreenAllList from '@Screens/ReferenceScreenAllList';
import NFTTabStackNavigator from '../NFTTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';

const FanTabStack = createNativeStackNavigator();
const FanTabStackNavigator = props => {
  const routeParams = props?.route?.params;

  return (
    <FanTabStack.Navigator screenOptions={{headerShown: false}}>
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'FanScreen'}
        component={FanScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'StakingScreen'}
        component={StakingScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'SoNearScreen'}
        component={SoNearScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'VolumeCompetetitionDetailScreen'}
        component={VolumeCompetetitionDetailScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'VolumeCompetitions'}
        component={VolumeCompetitions}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'FanAppTvStack'}
        component={FanAppTvStackNavigator}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'ResetCredentialExecutor'}
        component={ResetCredentialExecutor}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'WebviewScreen'}
        component={WebviewScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'FanTokenInfoScreen'}
        component={FanTokenInfoScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'StakeScreenDetail'}
        component={StakeScreenDetail}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'CustomerOrderListScreen'}
        component={CustomerOrderListScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'SettingStack'}
        component={SettingStackNavigator}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'EventStack'}
        component={EventStackNavigator}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'SurveyStack'}
        component={SurveyStackNavigator}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'NFTTabStackNavigator'}
        component={NFTTabStackNavigator}
      />

      <FanTabStack.Screen
        name={'EmailConfirmationScreen'}
        component={EmailConfirmationScreen}
      />
      <FanTabStack.Screen name={'LoginScreen'} component={LoginScreen} />
      {/* <FanTabStack.Screen
        name={'CreateAccountScreen'}
        component={CreateAccountScreen}
      /> */}
      <FanTabStack.Screen
        name={'RegisterStepOne'}
        component={RegisterStepOne}
      />

      {/* <FanTabStack.Screen
        name={'EmailConfirmationSecondStep'}
        component={EmailConfirmationSecondStep}
      /> */}
      <FanTabStack.Screen name={'OTPScreen'} component={OTPScreen} />
      <FanTabStack.Screen
        name={'PasswordGenerate'}
        component={PasswordGenerate}
      />
      <FanTabStack.Screen
        name={'ResetPasswordStep1'}
        component={ResetPasswordStep1}
      />
      <FanTabStack.Screen
        name={'ResetPasswordStep2'}
        component={ResetPasswordStep2}
      />
      <FanTabStack.Screen
        name={'ResetPasswordStep3'}
        component={ResetPasswordStep3}
      />
      <FanTabStack.Screen
        name={'AccountConfirmation'}
        component={AccountConfirmation}
      />
      <FanTabStack.Screen name={'EventsScreen'} component={EventsScreen} />
      <FanTabStack.Screen
        name={'EventDetailScreen'}
        component={EventDetailScreen}
      />
      <FanTabStack.Screen
        name={'EventConfirmationScreen'}
        component={EventConfirmationScreen}
      />
      <FanTabStack.Screen
        options={{gestureEnabled: false}}
        name={'EventInformationScreen'}
        component={EventInformationScreen}
      />
      <FanTabStack.Screen name={'SurveysScreen'} component={SurveysScreen} />
      <FanTabStack.Screen
        name={'SurveyScreenDetail'}
        component={SurveyScreenDetail}
      />

      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'NFTCollections'}
        component={NFTCollections}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'TasksScreen'}
        component={TasksScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'AchievementsAndCollections'}
        component={AchievementsAndCollections}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'AchievementsScreen'}
        component={AchievementsScreen}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'TradeTabStackNavigator'}
        component={TradeTabStackNavigator}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'SelectLanguageScreen'}
        component={SelectLanguageScreen}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'AccountSettings'}
        component={AccountSettings}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ValidateIdentity'}
        component={ValidateIdentity}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'CustomerRecognitionFormScreen'}
        component={CustomerRecognitionFormScreen}
      />
      <FanTabStack.Screen
        name={'CountrySelectScreen'}
        component={CountrySelectScreen}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        name={'DocumentationPage'}
        component={DocumentationPage}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        name={'CitizenshipVerificationDocument'}
        component={CitizenshipVerificationDocument}
      />

      <FanTabStack.Screen
        options={{headerShown: false}}
        name={'SelfieVerification'}
        component={SelfieVerification}
      />

      <FanTabStack.Screen
        options={{headerShown: false}}
        name={'AdressVerification'}
        component={AdressVerification}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        name={'UploadUserInformationPage'}
        component={UploadUserInformationPage}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ReferenceScreen'}
        component={ReferenceScreen}
      />
      <FanTabStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ReferenceScreenAllList'}
        component={ReferenceScreenAllList}
      />
      <FanTabStack.Screen
        initialParams={{...routeParams}}
        name={'WalletTabStackNavigator'}
        component={WalletTabStackNavigator}
      />
    </FanTabStack.Navigator>
  );
};

export default FanTabStackNavigator;
