import React from 'react';
import AuthLoaderScreen from '../Screens/AuthLoaderScreen';
import UploadUserInformationPage from '../Screens/UploadUserInformationPage';
import AppStackTabNavigator from '@Navigation/AppStackNavigator';
import {NavigationContainer} from '@react-navigation/native';
import PreLoginStackTabNavigator from '@Navigation/PreLoginStackTabNavigator';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ThemeProvider} from '@Utils/Theme/ThemeProvider';
import AccountSettings from '../Screens/SettingScreenDetails/AccountSettings';
import SettingStackNavigator from '../Navigation/SettingStackNavigator';

const Stack = createNativeStackNavigator();

const Entrypoint = () => {
  return (
    <ThemeProvider>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen
            name={'AuthLoaderScreen'}
            component={AuthLoaderScreen}
          />
          <Stack.Screen
            name={'UploadUserInformationPage'}
            component={UploadUserInformationPage}
          />
          <Stack.Screen
            name={'SettingStack'}
            component={SettingStackNavigator}
          />
          <Stack.Screen name={'AccountSettings'} component={AccountSettings} />
          <Stack.Screen
            name={'AppStackTabNavigator'}
            component={AppStackTabNavigator}
          />
          <Stack.Screen
            name={'PreLoginStackTabNavigator'}
            component={PreLoginStackTabNavigator}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
};

export default Entrypoint;
