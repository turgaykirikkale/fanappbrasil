import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import EventsScreen from '@Screens/EventsScreen';
import {SideMenuRoute} from '@Commons/Enums/SideMenuRoute';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';
const EventStack = createNativeStackNavigator();

const EventStackNavigator = () => {
  return (
    <EventStack.Navigator screenOptions={{headerShown: false}}>
      <EventStack.Screen
        name={SideMenuRoute.EventsScreen}
        component={EventsScreen}
      />
      <EventStack.Screen
        name={'EventDetailScreen'}
        component={EventDetailScreen}
      />
      <EventStack.Screen
        name={'EventConfirmationScreen'}
        component={EventConfirmationScreen}
      />
      <EventStack.Screen
        options={{gestureEnabled: false}}
        name={'EventInformationScreen'}
        component={EventInformationScreen}
      />
      <EventStack.Screen
        name={'AccountConfirmation'}
        component={AccountConfirmation}
      />
    </EventStack.Navigator>
  );
};

export default EventStackNavigator;
