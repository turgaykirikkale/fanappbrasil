import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SettingsScreen from '@Screens/SettingsScreen';
import SecuritySettingScreen from '@Screens/SecuritySettingScreen';
import CommissionScreen from '@Screens/CommissionScreen';
import ReferenceScreen from '@Screens/ReferenceScreen';
import ReferenceScreenAllList from '@Screens/ReferenceScreenAllList';
import PasswordGenerate from '@Screens/PasswordGenerate';
import PinGenerate from '@Screens/PinGenerate';
import SelectLanguageScreen from '@Screens/SelectLanguageScreen';
import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import CountrySelectScreen from '../../Screens/CountrySelectScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import UploadUserInformationPage from '../../Screens/UploadUserInformationPage';
const SettingStack = createNativeStackNavigator();
const SettingStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'SettingsScreen'}
        component={SettingsScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'SecuritySettingScreen'}
        component={SecuritySettingScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'CommissionScreen'}
        component={CommissionScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ReferenceScreen'}
        component={ReferenceScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ReferenceScreenAllList'}
        component={ReferenceScreenAllList}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'PasswordGenerate'}
        component={PasswordGenerate}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'PinGenerate'}
        component={PinGenerate}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'SelectLanguageScreen'}
        component={SelectLanguageScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'AccountSettings'}
        component={AccountSettings}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ValidateIdentity'}
        component={ValidateIdentity}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'CustomerRecognitionFormScreen'}
        component={CustomerRecognitionFormScreen}
      />
      <SettingStack.Screen
        name={'CountrySelectScreen'}
        component={CountrySelectScreen}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        name={'DocumentationPage'}
        component={DocumentationPage}
      />

      <SettingStack.Screen
        options={{headerShown: false}}
        name={'CitizenshipVerificationDocument'}
        component={CitizenshipVerificationDocument}
      />

      <SettingStack.Screen
        options={{headerShown: false}}
        name={'SelfieVerification'}
        component={SelfieVerification}
      />

      <SettingStack.Screen
        options={{headerShown: false}}
        name={'AdressVerification'}
        component={AdressVerification}
      />
      <SettingStack.Screen
        options={{headerShown: false}}
        name={'UploadUserInformationPage'}
        component={UploadUserInformationPage}
      />
    </SettingStack.Navigator>
  );
};

export default SettingStackNavigator;
