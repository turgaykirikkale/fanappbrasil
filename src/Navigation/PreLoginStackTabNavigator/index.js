import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FanTabStackNavigator from '../FanTabStackNavigator';
import HomeTabStackNavigator from '../HomeTabStackNavigator';
import TeamTabStackNavigator from '../TeamTabStackNavigator';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import {Text, View} from 'react-native';
import {DarkBlue, FAOrange} from '../../Commons/FAColor';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Svg, {Path} from 'react-native-svg';
import Localization from '@Localization';
import MarketTabStackNavigator from '../MarketTabStackNavigator';
import {ThemeContext} from '@Utils/Theme/ThemeProvider';
import {generateRandomColor} from '@Helpers/RandomColorHelper';
import NewsTabStackNavigator from '../NewsTabStackNavigator';
import VideoGalleryTabStackNavigator from '../VideoGalleryTabStackNavigator';
import NFTStackTabStackNavigator from '../NFTTabStackNavigator';

const PreLoginStackTab = createBottomTabNavigator();
const PreLoginStackTabNavigator = props => {
  const routeParams = props?.route?.params;
  const inset = useSafeAreaInsets();
  const randomFocusColor = generateRandomColor();

  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <PreLoginStackTab.Navigator
          screenOptions={{
            headerShown: false,
            tabBarStyle: {
              paddingTop: 6,
              backgroundColor: theme?.colors?.white,
              borderTopColor: theme?.colors?.softGray,
            },
          }}
          safeAreaInsets={{
            bottom: inset.bottom,
            left: inset.left,
            right: inset.right,
            top: inset.top,
          }}>
          <PreLoginStackTab.Screen
            initialParams={{...routeParams}}
            name={'HomeTabStackNavigator'}
            component={HomeTabStackNavigator}
            options={{
              tabBarShowLabel: true,
              tabBarLabel: ({focused}) => {
                return (
                  <View style={{marginBottom: 4}}>
                    <Text
                      style={{
                        color: focused
                          ? randomFocusColor
                          : theme?.colors?.darkBlue,
                        fontSize: 10,
                      }}>
                      {Localization.t('CommonsFix.Home')}
                    </Text>
                  </View>
                );
              },
              tabBarIcon: ({focused}) => (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24">
                  <Path d="M0 0h24v24H0z" fill="none" />
                  <Path
                    d="M21 20a1 1 0 01-1 1H4a1 1 0 01-1-1V9.49a1 1 0 01.386-.79l8-6.222a1 1 0 011.228 0l8 6.222a1 1 0 01.386.79V20zm-2-1V9.978l-7-5.444-7 5.444V19z"
                    fill={focused ? randomFocusColor : theme?.colors?.darkBlue}
                  />
                </Svg>
              ),
              unmountOnBlur: true,
            }}
          />
          <PreLoginStackTab.Screen
            initialParams={{...routeParams}}
            name={'NewsTabStackNavigator'}
            component={NewsTabStackNavigator}
            options={{
              tabBarShowLabel: true,
              tabBarLabel: ({focused}) => {
                return (
                  <View style={{marginBottom: 4}}>
                    <Text
                      style={{
                        color: focused
                          ? randomFocusColor
                          : theme?.colors?.darkBlue,
                        fontSize: 10,
                      }}>
                      {Localization.t('CommonsFix.News')}
                    </Text>
                  </View>
                );
              },
              tabBarIcon: ({focused}) => (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24">
                  <Path
                    fill={focused ? randomFocusColor : theme?.colors?.darkBlue}
                    d="M16 20V4H4v15a1 1 0 0 0 1 1h11zm3 2H5a3 3 0 0 1-3-3V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v7h4v9a3 3 0 0 1-3 3zm-1-10v7a1 1 0 0 0 2 0v-7h-2zM6 6h6v6H6V6zm2 2v2h2V8H8zm-2 5h8v2H6v-2zm0 3h8v2H6v-2z"
                  />
                </Svg>
              ),
              unmountOnBlur: true,
            }}
            listeners={({navigation, route}) => ({
              tabPress: e => {
                if (route?.params?.params?.from) {
                  route.params.params.from = undefined;
                }
              },
            })}
          />
          <PreLoginStackTab.Screen
            initialParams={{...routeParams}}
            name={'TeamTabStackNavigator'}
            component={TeamTabStackNavigator}
            options={{
              tabBarLabel: ({focused}) => {
                return (
                  <View style={{marginBottom: 4}}>
                    <Text
                      style={{
                        color: focused
                          ? randomFocusColor
                          : theme?.colors?.darkBlue,
                        fontSize: 10,
                      }}>
                      {Localization.t('TeamScreen.Team')}
                    </Text>
                  </View>
                );
              },
              tabBarIcon: ({focused}) => (
                <Svg xmlns="http://www.w3.org/2000/svg" width={24} height={24}>
                  <Path fill="none" d="M0 0h24v24H0z" />
                  <Path
                    fill={focused ? randomFocusColor : theme?.colors?.darkBlue}
                    d="M12 11a5 5 0 0 1 5 5v6h-2v-6a3 3 0 0 0-2.824-2.995L12 13a3 3 0 0 0-2.995 2.824L9 16v6H7v-6a5 5 0 0 1 5-5zm-6.5 3c.279 0 .55.033.81.094a5.947 5.947 0 0 0-.301 1.575L6 16v.086a1.492 1.492 0 0 0-.356-.08L5.5 16a1.5 1.5 0 0 0-1.493 1.356L4 17.5V22H2v-4.5A3.5 3.5 0 0 1 5.5 14zm13 0a3.5 3.5 0 0 1 3.5 3.5V22h-2v-4.5a1.5 1.5 0 0 0-1.356-1.493L18.5 16c-.175 0-.343.03-.5.085V16c0-.666-.108-1.306-.309-1.904A3.42 3.42 0 0 1 18.5 14zm-13-6a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5zm13 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5zm-13 2a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1zm13 0a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1zM12 2a4 4 0 1 1 0 8 4 4 0 0 1 0-8zm0 2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"
                  />
                </Svg>
              ),
              unmountOnBlur: true,
            }}
          />
          <PreLoginStackTab.Screen
            initialParams={{...routeParams}}
            name={'VideoGalleryTabStackNavigator'}
            component={VideoGalleryTabStackNavigator}
            options={{
              tabBarLabel: ({focused}) => {
                return (
                  <View style={{marginBottom: 4}}>
                    <Text
                      style={{
                        color: focused
                          ? randomFocusColor
                          : theme?.colors?.darkBlue,
                        fontSize: 10,
                      }}>
                      {Localization.t('CommonsFix.VideoGallery')}
                    </Text>
                  </View>
                );
              },
              tabBarIcon: ({focused}) => (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  {...props}>
                  <Path fill="none" d="M0 0h24v24H0z" />
                  <Path
                    fill={focused ? randomFocusColor : theme?.colors?.darkBlue}
                    d="M2 3.993A1 1 0 0 1 2.992 3h18.016c.548 0 .992.445.992.993v16.014a1 1 0 0 1-.992.993H2.992A.993.993 0 0 1 2 20.007V3.993zM4 5v14h16V5H4zm6.622 3.415 4.879 3.252a.4.4 0 0 1 0 .666l-4.88 3.252a.4.4 0 0 1-.621-.332V8.747a.4.4 0 0 1 .622-.332z"
                  />
                </Svg>
              ),
              unmountOnBlur: true,
            }}
            listeners={({navigation, route}) => ({
              tabPress: e => {
                if (route?.params?.params?.hideZeroWallets) {
                  route.params.params.hideZeroWallets = undefined;
                }
              },
            })}
          />
          <PreLoginStackTab.Screen
            initialParams={{...routeParams}}
            name={'FanTabStackNavigator'}
            component={FanTabStackNavigator}
            options={{
              tabBarLabel: ({focused}) => {
                return (
                  <View style={{marginBottom: 4}}>
                    <Text
                      style={{
                        color: focused
                          ? randomFocusColor
                          : theme?.colors?.darkBlue,
                        fontSize: 10,
                      }}>
                      {Localization.t('FanScreenFix.Fan')}
                    </Text>
                  </View>
                );
              },
              tabBarIcon: ({focused}) => (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24">
                  <Path d="M0 0h24v24H0z" fill="none" />
                  <Path
                    d="M4 22a8 8 0 1116 0h-2a6 6 0 10-12 0zm8-9a6 6 0 116-6 6 6 0 01-6 6zm0-2a4 4 0 10-4-4 4 4 0 004 4z"
                    fill={focused ? randomFocusColor : theme?.colors?.darkBlue}
                  />
                </Svg>
              ),
              unmountOnBlur: true,
            }}
          />
        </PreLoginStackTab.Navigator>
      )}
    </ThemeContext.Consumer>
  );
};

export default PreLoginStackTabNavigator;
