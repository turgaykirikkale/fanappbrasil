import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SurveysScreen from '@Screens/SurveysScreen';
import SurveyScreenDetail from '@Screens/SurveyScreenDetail';
import {SideMenuRoute} from '@Commons/Enums/SideMenuRoute';
import WalletScreen from '../../Screens/WalletScreen';

const SurveyStack = createNativeStackNavigator();

const SurveyStackNavigator = () => {
  return (
    <SurveyStack.Navigator screenOptions={{headerShown: false}}>
      <SurveyStack.Screen
        name={SideMenuRoute.SurveysScreen}
        component={SurveysScreen}
      />
      <SurveyStack.Screen
        name={'SurveyScreenDetail'}
        component={SurveyScreenDetail}
      />
      <SurveyStack.Screen name={'WalletScreen'} component={WalletScreen} />
    </SurveyStack.Navigator>
  );
};

export default SurveyStackNavigator;
