import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MarketScreen from '@Screens/MarketScreen';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';
import MarketDetailScreen from '@Screens/MarketDetailScreen';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';

import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';
import SurveysScreen from '../../Screens/SurveysScreen';
import SurveyScreenDetail from '../../Screens/SurveyScreenDetail';
import EventsScreen from '@Screens/EventsScreen';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';

const MarketStack = createNativeStackNavigator();
const MarketTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <MarketStack.Navigator screenOptions={{headerShown: false}}>
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'MarketScreen'}
        component={MarketScreen}
      />

      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'MarketDetailScreen'}
        component={MarketDetailScreen}
      />
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'SettingStack'}
        component={SettingStackNavigator}
      />
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'LoginScreen'}
        component={LoginScreen}
      />
      {/* <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'CreateAccountScreen'}
        component={CreateAccountScreen}
      /> */}
      <MarketStack.Screen
        name={'AccountConfirmation'}
        component={AccountConfirmation}
      />

      <MarketStack.Screen
        name={'EmailConfirmationScreen'}
        component={EmailConfirmationScreen}
      />
      <MarketStack.Screen
        name={'CountrySelectScreen'}
        component={CountrySelectScreen}
      />
      <MarketStack.Screen name={'OTPScreen'} component={OTPScreen} />
      <MarketStack.Screen
        name={'ResetPasswordStep1'}
        component={ResetPasswordStep1}
      />
      <MarketStack.Screen
        name={'PasswordGenerate'}
        component={PasswordGenerate}
      />
      <MarketStack.Screen
        name={'ResetPasswordStep2'}
        component={ResetPasswordStep2}
      />
      <MarketStack.Screen
        name={'ResetPasswordStep3'}
        component={ResetPasswordStep3}
      />
      <MarketStack.Screen
        name={'RegisterStepOne'}
        component={RegisterStepOne}
      />
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'SurveysScreen'}
        component={SurveysScreen}
      />
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'SurveyScreenDetail'}
        component={SurveyScreenDetail}
      />
      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'EventsScreen'}
        component={EventsScreen}
      />

      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'EventDetailScreen'}
        component={EventDetailScreen}
      />

      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'EventConfirmationScreen'}
        component={EventConfirmationScreen}
      />

      <MarketStack.Screen
        initialParams={{...routeParams}}
        name={'EventInformationScreen'}
        component={EventInformationScreen}
      />
      <MarketStack.Screen
        name={'ValidateIdentity'}
        component={ValidateIdentity}
      />
      <MarketStack.Screen
        name={'CustomerRecognitionFormScreen'}
        component={CustomerRecognitionFormScreen}
      />
      <MarketStack.Screen
        name={'DocumentationPage'}
        component={DocumentationPage}
      />
      <MarketStack.Screen
        name={'CitizenshipVerificationDocument'}
        component={CitizenshipVerificationDocument}
      />
      <MarketStack.Screen
        name={'SelfieVerification'}
        component={SelfieVerification}
      />
      <MarketStack.Screen
        name={'AdressVerification'}
        component={AdressVerification}
      />
    </MarketStack.Navigator>
  );
};

export default MarketTabStackNavigator;
