import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import VideoGalleryScreen from '@Screens/VideoGalleryScreen';
import EventStackNavigator from '../EventStackNavigator/index';
import SurveyStackNavigator from '../SurveyStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import CreateAccountScreen from '@Screens/CreateAccountScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';

import MarketTabStackNavigator from '../MarketTabStackNavigator';
import FanTabStackNavigator from '../FanTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';
import VideoScreen from '@Screens/VideoScreen';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import WebviewScreen from '../../Screens/WebviewScreen';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator';

import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';

import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import {View} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import FanAppTvScreen from '../../Screens/FanAppTvScreen';
import NFTTabStackNavigator from '../NFTTabStackNavigator';
import SurveysScreen from '../../Screens/SurveysScreen';
import SurveyScreenDetail from '../../Screens/SurveyScreenDetail';
import EventsScreen from '@Screens/EventsScreen';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
const VideoGalleryStack = createNativeStackNavigator();
const VideoGalleryTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <VideoGalleryStack.Navigator screenOptions={{headerShown: false}}>
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvScreen'}
              component={FanAppTvScreen}
            />
            <VideoGalleryStack.Screen
              name={'RegisterStepOne'}
              component={RegisterStepOne}
            />
            <VideoGalleryStack.Screen
              name={'EmailConfirmationScreen'}
              component={EmailConfirmationScreen}
            />
            <VideoGalleryStack.Screen
              name={'CountrySelectScreen'}
              component={CountrySelectScreen}
            />
            <VideoGalleryStack.Screen
              name={'OTPScreen'}
              component={OTPScreen}
            />
            <VideoGalleryStack.Screen
              name={'ResetPasswordStep1'}
              component={ResetPasswordStep1}
            />
            <VideoGalleryStack.Screen
              name={'PasswordGenerate'}
              component={PasswordGenerate}
            />
            <VideoGalleryStack.Screen
              name={'ResetPasswordStep2'}
              component={ResetPasswordStep2}
            />
            <VideoGalleryStack.Screen
              name={'ResetPasswordStep3'}
              component={ResetPasswordStep3}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'VideoGalleryScreen'}
              component={VideoGalleryScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'EventStack'}
              component={EventStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'SurveyStack'}
              component={SurveyStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'SettingStack'}
              component={SettingStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'LoginScreen'}
              component={LoginScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'CreateAccountScreen'}
              component={CreateAccountScreen}
            />
            <VideoGalleryStack.Screen
              name={'AccountConfirmation'}
              component={AccountConfirmation}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'MarketTabStackNavigator'}
              component={MarketTabStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'FanTabStackNavigator'}
              component={FanTabStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'VideoScreen'}
              component={VideoScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'WalletTabStackNavigator'}
              component={WalletTabStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'TradeTabStackNavigator'}
              component={TradeTabStackNavigator}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'WebviewScreen'}
              component={WebviewScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvStack'}
              component={FanAppTvStackNavigator}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'AccountSettings'}
              component={AccountSettings}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'ValidateIdentity'}
              component={ValidateIdentity}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'CustomerRecognitionFormScreen'}
              component={CustomerRecognitionFormScreen}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              name={'DocumentationPage'}
              component={DocumentationPage}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              name={'CitizenshipVerificationDocument'}
              component={CitizenshipVerificationDocument}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              name={'SelfieVerification'}
              component={SelfieVerification}
            />
            <VideoGalleryStack.Screen
              options={{headerShown: false}}
              name={'AdressVerification'}
              component={AdressVerification}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'NFTTabStackNavigator'}
              component={NFTTabStackNavigator}
            />

            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'SurveysScreen'}
              component={SurveysScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'SurveyScreenDetail'}
              component={SurveyScreenDetail}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'EventsScreen'}
              component={EventsScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'EventDetailScreen'}
              component={EventDetailScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'EventConfirmationScreen'}
              component={EventConfirmationScreen}
            />
            <VideoGalleryStack.Screen
              initialParams={{...routeParams}}
              name={'EventInformationScreen'}
              component={EventInformationScreen}
            />
          </VideoGalleryStack.Navigator>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default VideoGalleryTabStackNavigator;
