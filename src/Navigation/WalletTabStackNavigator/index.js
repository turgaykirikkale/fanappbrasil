import React from 'react';
import WalletScreen from '@Screens/WalletScreen';
import WalletScreenDetail from '@Screens/WalletScreenDetail';
import CoinWithdrawTransactionHistoryScreen from '@Screens/CoinWithdrawTransactionHistoryScreen';
import EventStackNavigator from '../EventStackNavigator/index';
import SurveyStackNavigator from '../SurveyStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AccountConfirmation from '@Screens/AccountConfirmation';
import WebviewScreen from '../../Screens/WebviewScreen';
import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import SurveysScreen from '../../Screens/SurveysScreen';
import SurveyScreenDetail from '../../Screens/SurveyScreenDetail';
import EventsScreen from '@Screens/EventsScreen';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
const WalletStack = createNativeStackNavigator();
const WalletTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <WalletStack.Navigator screenOptions={{headerShown: false}}>
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'WalletScreen'}
        component={WalletScreen}
        headerShown={false}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'WalletScreenDetail'}
        component={WalletScreenDetail}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'CoinWithdrawTransactionHistoryScreen'}
        component={CoinWithdrawTransactionHistoryScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'EventStack'}
        component={EventStackNavigator}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'SurveyStack'}
        component={SurveyStackNavigator}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'SettingStack'}
        component={SettingStackNavigator}
      />
      <WalletStack.Screen
        name={'AccountConfirmation'}
        component={AccountConfirmation}
      />
      <WalletStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'AccountSettings'}
        component={AccountSettings}
      />
      <WalletStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'ValidateIdentity'}
        component={ValidateIdentity}
      />
      <WalletStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'CustomerRecognitionFormScreen'}
        component={CustomerRecognitionFormScreen}
      />

      <WalletStack.Screen
        options={{headerShown: false}}
        name={'DocumentationPage'}
        component={DocumentationPage}
      />

      <WalletStack.Screen
        options={{headerShown: false}}
        name={'CitizenshipVerificationDocument'}
        component={CitizenshipVerificationDocument}
      />

      <WalletStack.Screen
        options={{headerShown: false}}
        name={'SelfieVerification'}
        component={SelfieVerification}
      />

      <WalletStack.Screen
        options={{headerShown: false}}
        name={'AdressVerification'}
        component={AdressVerification}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'SurveysScreen'}
        component={SurveysScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'SurveyScreenDetail'}
        component={SurveyScreenDetail}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'EventsScreen'}
        component={EventsScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'EventDetailScreen'}
        component={EventDetailScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'EventConfirmationScreen'}
        component={EventConfirmationScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'EventInformationScreen'}
        component={EventInformationScreen}
      />
      <WalletStack.Screen
        initialParams={{...routeParams}}
        name={'WebviewScreen'}
        component={WebviewScreen}
      />
    </WalletStack.Navigator>
  );
};
export default WalletTabStackNavigator;
