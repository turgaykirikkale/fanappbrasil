import React from 'react';
import {View} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import NewsScreen from '@Screens/NewsScreen';
import EventStackNavigator from '../EventStackNavigator/index';
import SurveyStackNavigator from '../SurveyStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import CreateAccountScreen from '@Screens/CreateAccountScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';

import MarketTabStackNavigator from '../MarketTabStackNavigator';
import FanTabStackNavigator from '../FanTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';
import NewsDetailScreen from '../../Screens/NewsDetailScreen';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import WebviewScreen from '../../Screens/WebviewScreen';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator';

import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';

import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import UploadUserInformationPage from '../../Screens/UploadUserInformationPage';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import NFTTabStackNavigator from '../NFTTabStackNavigator';

import SurveysScreen from '../../Screens/SurveysScreen';
import SurveyScreenDetail from '../../Screens/SurveyScreenDetail';
import EventsScreen from '@Screens/EventsScreen';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';

const NewsStack = createNativeStackNavigator();
const NewsTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <NewsStack.Navigator screenOptions={{headerShown: false}}>
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'NewsScreen'}
              component={NewsScreen}
            />
            <NewsStack.Screen
              name={'RegisterStepOne'}
              component={RegisterStepOne}
            />
            <NewsStack.Screen
              name={'EmailConfirmationScreen'}
              component={EmailConfirmationScreen}
            />
            <NewsStack.Screen
              name={'CountrySelectScreen'}
              component={CountrySelectScreen}
            />
            <NewsStack.Screen name={'OTPScreen'} component={OTPScreen} />
            <NewsStack.Screen
              name={'ResetPasswordStep1'}
              component={ResetPasswordStep1}
            />
            <NewsStack.Screen
              name={'PasswordGenerate'}
              component={PasswordGenerate}
            />
            <NewsStack.Screen
              name={'ResetPasswordStep2'}
              component={ResetPasswordStep2}
            />
            <NewsStack.Screen
              name={'ResetPasswordStep3'}
              component={ResetPasswordStep3}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'EventStack'}
              component={EventStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'SurveyStack'}
              component={SurveyStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'SettingStack'}
              component={SettingStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'NFTTabStackNavigator'}
              component={NFTTabStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'LoginScreen'}
              component={LoginScreen}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'CreateAccountScreen'}
              component={CreateAccountScreen}
            />
            <NewsStack.Screen
              name={'AccountConfirmation'}
              component={AccountConfirmation}
            />

            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'MarketTabStackNavigator'}
              component={MarketTabStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'FanTabStackNavigator'}
              component={FanTabStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'WalletTabStackNavigator'}
              component={WalletTabStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'TradeTabStackNavigator'}
              component={TradeTabStackNavigator}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'NewsDetailScreen'}
              component={NewsDetailScreen}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'WebviewScreen'}
              component={WebviewScreen}
            />
            <NewsStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvStack'}
              component={FanAppTvStackNavigator}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'AccountSettings'}
              component={AccountSettings}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'ValidateIdentity'}
              component={ValidateIdentity}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'CustomerRecognitionFormScreen'}
              component={CustomerRecognitionFormScreen}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              name={'DocumentationPage'}
              component={DocumentationPage}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              name={'CitizenshipVerificationDocument'}
              component={CitizenshipVerificationDocument}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              name={'SelfieVerification'}
              component={SelfieVerification}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              name={'AdressVerification'}
              component={AdressVerification}
            />

            <NewsStack.Screen
              options={{headerShown: false}}
              name={'SurveysScreen'}
              component={SurveysScreen}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              name={'SurveyScreenDetail'}
              component={SurveyScreenDetail}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              name={'EventsScreen'}
              component={EventsScreen}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              name={'EventDetailScreen'}
              component={EventDetailScreen}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              name={'EventConfirmationScreen'}
              component={EventConfirmationScreen}
            />
            <NewsStack.Screen
              options={{headerShown: false}}
              name={'EventInformationScreen'}
              component={EventInformationScreen}
            />
          </NewsStack.Navigator>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default NewsTabStackNavigator;
