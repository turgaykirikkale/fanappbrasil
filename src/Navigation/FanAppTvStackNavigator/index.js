import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {SideMenuRoute} from '@Commons/Enums/SideMenuRoute';
import FanAppTvScreen from '@Screens/FanAppTvScreen';
import VideoScreen from '@Screens/VideoScreen';
import VideoListScreen from '@Screens/VideoListScreen';

const FanAppTvStack = createNativeStackNavigator();

const FanAppTvStackNavigator = () => {
  return (
    <FanAppTvStack.Navigator screenOptions={{headerShown: false}}>
      <FanAppTvStack.Screen
        name={SideMenuRoute.FanAppTvScreen}
        component={FanAppTvScreen}
      />
      <FanAppTvStack.Screen name={'VideoScreen'} component={VideoScreen} />
      <FanAppTvStack.Screen
        name={'VideoListScreen'}
        component={VideoListScreen}
      />
    </FanAppTvStack.Navigator>
  );
};

export default FanAppTvStackNavigator;
