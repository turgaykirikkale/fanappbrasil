import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {View} from 'react-native';
import TeamScreen from '@Screens/TeamScreen';
import EventStackNavigator from '../EventStackNavigator/index';
import SurveyStackNavigator from '../SurveyStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import CreateAccountScreen from '@Screens/CreateAccountScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';

import MarketTabStackNavigator from '../MarketTabStackNavigator';
import FanTabStackNavigator from '../FanTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';
import TradeTabStackNavigator from '../TradeTabStackNavigator';

import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';

import VideoScreen from '@Screens/VideoScreen';
import SurveysScreen from '@Screens/SurveysScreen';
import SurveyScreenDetail from '@Screens/SurveyScreenDetail';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
import EventsScreen from '@Screens/EventsScreen';
import VideoListScreen from '@Screens/VideoListScreen';
import FanAppTvScreen from '@Screens/FanAppTvScreen';
import WebviewScreen from '../../Screens/WebviewScreen';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator';
import StandingScreen from '../../Screens/StandingScreen';
import FixtureScreen from '../../Screens/FixtureScreen';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
import NFTTabStackNavigator from '../NFTTabStackNavigator';


const TeamStack = createNativeStackNavigator();
const TeamTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <TeamStack.Navigator screenOptions={{headerShown: false}}>
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'TeamScreen'}
              component={TeamScreen}
            />
            <TeamStack.Screen
              name={'RegisterStepOne'}
              component={RegisterStepOne}
            />
            <TeamStack.Screen
              name={'EmailConfirmationScreen'}
              component={EmailConfirmationScreen}
            />
            <TeamStack.Screen
              name={'CountrySelectScreen'}
              component={CountrySelectScreen}
            />
            <TeamStack.Screen name={'OTPScreen'} component={OTPScreen} />
            <TeamStack.Screen
              name={'ResetPasswordStep1'}
              component={ResetPasswordStep1}
            />
            <TeamStack.Screen
              name={'PasswordGenerate'}
              component={PasswordGenerate}
            />
            <TeamStack.Screen
              name={'ResetPasswordStep2'}
              component={ResetPasswordStep2}
            />
            <TeamStack.Screen
              name={'ResetPasswordStep3'}
              component={ResetPasswordStep3}
            />

            <TeamStack.Screen
              name={'SurveyScreenDetail'}
              component={SurveyScreenDetail}
            />
            <TeamStack.Screen
              name={'SurveysScreen'}
              component={SurveysScreen}
            />
            <TeamStack.Screen name={'EventsScreen'} component={EventsScreen} />
            <TeamStack.Screen
              name={'EventDetailScreen'}
              component={EventDetailScreen}
            />
            <TeamStack.Screen
              name={'EventConfirmationScreen'}
              component={EventConfirmationScreen}
            />
            <TeamStack.Screen
              options={{gestureEnabled: false}}
              name={'EventInformationScreen'}
              component={EventInformationScreen}
            />
            <TeamStack.Screen
              options={{gestureEnabled: false}}
              name={'VideoScreen'}
              component={VideoScreen}
            />

            <TeamStack.Screen
              options={{gestureEnabled: false}}
              name={'VideoListScreen'}
              component={VideoListScreen}
            />
            <TeamStack.Screen
              name={'FanAppTvScreen'}
              component={FanAppTvScreen}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'EventStack'}
              component={EventStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'SurveyStack'}
              component={SurveyStackNavigator}
            />
              <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'NFTTabStackNavigator'}
              component={NFTTabStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'SettingStack'}
              component={SettingStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'LoginScreen'}
              component={LoginScreen}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'CreateAccountScreen'}
              component={CreateAccountScreen}
            />
            <TeamStack.Screen
              name={'AccountConfirmation'}
              component={AccountConfirmation}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'MarketTabStackNavigator'}
              component={MarketTabStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'FanTabStackNavigator'}
              component={FanTabStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'WalletTabStackNavigator'}
              component={WalletTabStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'TradeTabStackNavigator'}
              component={TradeTabStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'WebviewScreen'}
              component={WebviewScreen}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvStack'}
              component={FanAppTvStackNavigator}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'StandingScreen'}
              component={StandingScreen}
            />
            <TeamStack.Screen
              initialParams={{...routeParams}}
              name={'FixtureScreen'}
              component={FixtureScreen}
            />
          </TeamStack.Navigator>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default TeamTabStackNavigator;
