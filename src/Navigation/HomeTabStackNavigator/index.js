import React from 'react';
import HomeScreen from '@Screens/HomeScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import CreateAccountScreen from '@Screens/CreateAccountScreen';
import NewsDetailScreen from '@Screens/NewsDetailScreen';
import WebviewScreen from '../../Screens/WebviewScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';
import FanTokenInfoScreen from '@Screens/FanTokenInfoScreen';
import TokenBuyScreen from '@Screens/TokenBuyScreen';
import HomePage from '@Screens/HomePage';
import VideoScreen from '@Screens/VideoScreen';
import SurveysScreen from '@Screens/SurveysScreen';
import SurveyScreenDetail from '@Screens/SurveyScreenDetail';
import EventDetailScreen from '@Screens/EventDetailScreen';
import EventConfirmationScreen from '@Screens/EventConfirmationScreen';
import EventInformationScreen from '@Screens/EventInformationScreen';
import EventsScreen from '@Screens/EventsScreen';
import VideoListScreen from '@Screens/VideoListScreen';
import FanAppTvScreen from '@Screens/FanAppTvScreen';
import {View} from 'react-native';
import NewsWebView from '../../Screens/NewsWebView';
import CountrySelectScreen from '@Screens/CountrySelectScreen';
import OTPScreen from '@Screens/OTPScreen';
import ResetPasswordStep1 from '@Screens/ResetPasswordScreens/ResetPasswordStep1';
import ResetPasswordStep2 from '@Screens/ResetPasswordScreens/ResetPasswordStep2';
import ResetPasswordStep3 from '@Screens/ResetPasswordScreens/ResetPasswordStep3';
import EmailConfirmationScreen from '@Screens/EmailConfirmationScreen';
import PasswordGenerate from '@Screens/PasswordGenerate';

import RegisterStepOne from '../../Screens/RegisterScreens/RegisterStepOne';

import MarketTabStackNavigator from '../MarketTabStackNavigator';
import FanTabStackNavigator from '../FanTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import NFTTabStackNavigator from '../NFTTabStackNavigator';

import StandingScreen from '../../Screens/StandingScreen';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';

const HomeStack = createNativeStackNavigator();
const HomeTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <HomeStack.Navigator screenOptions={{headerShown: false}}>
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'HomePage'}
              component={HomePage}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'HomeScreen'}
              component={HomeScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'NewsDetailScreen'}
              component={NewsDetailScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'VideoScreen'}
              component={VideoScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'TokenBuyScreen'}
              component={TokenBuyScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'WebviewScreen'}
              component={WebviewScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'FanTokenInfoScreen'}
              component={FanTokenInfoScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvStack'}
              component={FanAppTvStackNavigator}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'MarketTabStackNavigator'}
              component={MarketTabStackNavigator}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'FanTabStackNavigator'}
              component={FanTabStackNavigator}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'WalletTabStackNavigator'}
              component={WalletTabStackNavigator}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'TradeTabStackNavigator'}
              component={TradeTabStackNavigator}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'NFTTabStackNavigator'}
              component={NFTTabStackNavigator}
            />

            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'SettingStack'}
              component={SettingStackNavigator}
            />

            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'LoginScreen'}
              component={LoginScreen}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'CreateAccountScreen'}
              component={CreateAccountScreen}
            />
            <HomeStack.Screen
              name={'AccountConfirmation'}
              component={AccountConfirmation}
            />
            <HomeStack.Screen
              name={'EmailConfirmationScreen'}
              component={EmailConfirmationScreen}
            />
            <HomeStack.Screen
              name={'CountrySelectScreen'}
              component={CountrySelectScreen}
            />
            <HomeStack.Screen name={'OTPScreen'} component={OTPScreen} />
            <HomeStack.Screen
              name={'ResetPasswordStep1'}
              component={ResetPasswordStep1}
            />
            <HomeStack.Screen
              name={'PasswordGenerate'}
              component={PasswordGenerate}
            />
            <HomeStack.Screen
              name={'ResetPasswordStep2'}
              component={ResetPasswordStep2}
            />
            <HomeStack.Screen
              name={'ResetPasswordStep3'}
              component={ResetPasswordStep3}
            />
            <HomeStack.Screen
              name={'SurveyScreenDetail'}
              component={SurveyScreenDetail}
            />
            <HomeStack.Screen
              name={'SurveysScreen'}
              component={SurveysScreen}
            />
            <HomeStack.Screen name={'EventsScreen'} component={EventsScreen} />
            <HomeStack.Screen
              name={'EventDetailScreen'}
              component={EventDetailScreen}
            />
            <HomeStack.Screen
              name={'EventConfirmationScreen'}
              component={EventConfirmationScreen}
            />
            <HomeStack.Screen
              options={{gestureEnabled: false}}
              name={'EventInformationScreen'}
              component={EventInformationScreen}
            />
            <HomeStack.Screen
              options={{gestureEnabled: false}}
              name={'VideoListScreen'}
              component={VideoListScreen}
            />
            <HomeStack.Screen
              name={'FanAppTvScreen'}
              component={FanAppTvScreen}
            />
            <HomeStack.Screen name={'NewsWebView'} component={NewsWebView} />
            <HomeStack.Screen
              name={'RegisterStepOne'}
              component={RegisterStepOne}
            />
            <HomeStack.Screen
              initialParams={{...routeParams}}
              name={'StandingScreen'}
              component={StandingScreen}
            />
          </HomeStack.Navigator>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};
export default HomeTabStackNavigator;
