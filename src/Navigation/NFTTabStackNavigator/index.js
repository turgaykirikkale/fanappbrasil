import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import NFTScreen from '@Screens/NFTScreen';
import EventStackNavigator from '../EventStackNavigator/index';
import SurveyStackNavigator from '../SurveyStackNavigator/index';
import SettingStackNavigator from '../SettingStackNavigator/index';
import LoginScreen from '@Screens/LoginScreen';
import CreateAccountScreen from '@Screens/CreateAccountScreen';
import AccountConfirmation from '@Screens/AccountConfirmation';

import MarketTabStackNavigator from '../MarketTabStackNavigator';
import FanTabStackNavigator from '../FanTabStackNavigator';
import WalletTabStackNavigator from '../WalletTabStackNavigator';
import TradeTabStackNavigator from '../TradeTabStackNavigator';
import WebviewScreen from '../../Screens/WebviewScreen';
import FanAppTvStackNavigator from '../FanAppTvStackNavigator';

import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
import UploadUserInformationPage from '../../Screens/UploadUserInformationPage';
import {View} from 'react-native';
import {ThemeContext} from '../../Utils/Theme/ThemeProvider';
const NFTStack = createNativeStackNavigator();
const NFTStackTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <ThemeContext.Consumer>
      {({theme}) => (
        <View style={{flex: 1, backgroundColor: theme?.colors?.white}}>
          <NFTStack.Navigator screenOptions={{headerShown: false}}>
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'NFTScreen'}
              component={NFTScreen}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'EventStack'}
              component={EventStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'SurveyStack'}
              component={SurveyStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'SettingStack'}
              component={SettingStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'LoginScreen'}
              component={LoginScreen}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'CreateAccountScreen'}
              component={CreateAccountScreen}
            />
            <NFTStack.Screen
              name={'AccountConfirmation'}
              component={AccountConfirmation}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'MarketTabStackNavigator'}
              component={MarketTabStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'FanTabStackNavigator'}
              component={FanTabStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'WalletTabStackNavigator'}
              component={WalletTabStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'TradeTabStackNavigator'}
              component={TradeTabStackNavigator}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'WebviewScreen'}
              component={WebviewScreen}
            />
            <NFTStack.Screen
              initialParams={{...routeParams}}
              name={'FanAppTvStack'}
              component={FanAppTvStackNavigator}
            />

            <NFTStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'AccountSettings'}
              component={AccountSettings}
            />
            <NFTStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'ValidateIdentity'}
              component={ValidateIdentity}
            />
            <NFTStack.Screen
              options={{headerShown: false}}
              initialParams={{...routeParams}}
              name={'CustomerRecognitionFormScreen'}
              component={CustomerRecognitionFormScreen}
            />

            <NFTStack.Screen
              options={{headerShown: false}}
              name={'DocumentationPage'}
              component={DocumentationPage}
            />

            <NFTStack.Screen
              options={{headerShown: false}}
              name={'CitizenshipVerificationDocument'}
              component={CitizenshipVerificationDocument}
            />

            <NFTStack.Screen
              options={{headerShown: false}}
              name={'SelfieVerification'}
              component={SelfieVerification}
            />

            <NFTStack.Screen
              options={{headerShown: false}}
              name={'AdressVerification'}
              component={AdressVerification}
            />
          </NFTStack.Navigator>
        </View>
      )}
    </ThemeContext.Consumer>
  );
};

export default NFTStackTabStackNavigator;
