import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import StockFlowExecutor from '@Screens/StockFlowExecutor';
import WebviewScreen from '@Screens/WebviewScreen';
import StockStandardScreen from '@Screens/StockStandardScreen';
import StockEasyBuy from '@Screens/StockEasyBuy';
import StockEasyBuySellScreen from '@Screens/StockEasyBuy';
import AccountConfirmation from '@Screens/AccountConfirmation';
import StockAdvanced from '@Screens/StockAdvanced';
import AccountSettings from '../../Screens/SettingScreenDetails/AccountSettings';
import ValidateIdentity from '../../Screens/SettingScreenDetails/ValidateIdentityScreen';
import CustomerRecognitionFormScreen from '../../Screens/SettingScreenDetails/CustomerRecognitionFormScreen';
import DocumentationPage from '../../Screens/SettingScreenDetails/DocumentationPage';
import CitizenshipVerificationDocument from '../../Screens/SettingScreenDetails/DocumentationPage/CitizenshipVerificationDocument';
import SelfieVerification from '../../Screens/SettingScreenDetails/DocumentationPage/SelfieVerification';
import AdressVerification from '../../Screens/SettingScreenDetails/DocumentationPage/AdressVerification';
const TradeStack = createNativeStackNavigator();
const TradeTabStackNavigator = props => {
  const routeParams = props?.route?.params;
  return (
    <TradeStack.Navigator screenOptions={{headerShown: false}}>
      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'StockFlowExecutor'}
        component={StockFlowExecutor}
      />

      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'StockAdvanced'}
        component={StockAdvanced}
      />
      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'StockStandardScreen'}
        component={StockStandardScreen}
      />
      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'StockEasyBuySellScreen'}
        component={StockEasyBuySellScreen}
      />
      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'WebviewScreen'}
        component={WebviewScreen}
      />
      <TradeStack.Screen
        options={{headerShown: false}}
        initialParams={{...routeParams}}
        name={'StockEasyBuy'}
        component={StockEasyBuy}
      />
      <TradeStack.Screen
        name={'AccountConfirmation'}
        component={AccountConfirmation}
      />
      <TradeStack.Screen
        initialParams={{...routeParams}}
        name={'AccountSettings'}
        component={AccountSettings}
      />
      <TradeStack.Screen
        name={'ValidateIdentity'}
        component={ValidateIdentity}
      />
      <TradeStack.Screen
        name={'CustomerRecognitionFormScreen'}
        component={CustomerRecognitionFormScreen}
      />
      <TradeStack.Screen
        name={'DocumentationPage'}
        component={DocumentationPage}
      />
      <TradeStack.Screen
        name={'CitizenshipVerificationDocument'}
        component={CitizenshipVerificationDocument}
      />
      <TradeStack.Screen
        name={'SelfieVerification'}
        component={SelfieVerification}
      />
      <TradeStack.Screen
        name={'AdressVerification'}
        component={AdressVerification}
      />
    </TradeStack.Navigator>
  );
};

export default TradeTabStackNavigator;
