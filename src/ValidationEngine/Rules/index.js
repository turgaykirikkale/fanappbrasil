import _ from 'lodash';

export const isBoolean = value => {
  if (typeof value === 'boolean') {
    return true;
  }
  return false;
};

export const notEmptyAndNil = value => {
  if (!_.isEmpty(value) && !_.isNil(value)) {
    return true;
  }
  return false;
};

export const moneyOrCoinWithdrawEmailPin = value => {
  if (value && value.length === 6) {
    return true;
  }
  return false;
};

export const leastOneUppercaseLetter = value => {
  return /([A-Z]+)/.test(value);
};

export const leastOneLowercaseLetter = value => {
  return /([a-z]+)/.test(value);
};

export const leastOneSymbol = value => {
  return /[$-/:-?{-~!"^_`\[\]]/.test(value);
};
export const IbanNo = value => {
  const re = /TR[a-zA-Z0-9]{2}\s?([0-9]{4}\s?){1}([0-9]{1})([a-zA-Z0-9]{3}\s?)([a-zA-Z0-9]{4}\s?){3}([a-zA-Z0-9]{2})\s?/;
  return re.test(value);
};
export const leastOneInteger = value => {
  return /[0-9]/.test(value);
};
export const passwordLenght = value => {
  if (value && value.length && value.length >= 8 && value.length <= 15) {
    return /.{8,15}$/.test(value);
  }
};
export const pinLenght = value => {
  if (value && value.length && value.length >= 4 && value.length <= 4) {
    return /.{4,4}$/.test(value);
  }
};
export const checkAgainPassword = (value, nextValue) => {
  if (value === nextValue) {
    return true;
  }
  return false;
};

export const isEqual = (firstValue, secondValue) => {
  return firstValue === secondValue;
};

export const min = (minValue, value) => {
  if (value && value.length && value.length >= minValue) {
    return true;
  }

  return false;
};

export const checkedRule = value => {
  if (value === true) {
    return true;
  }
  return false;
};

export const phoneNumberRule = value => {
  if (value && !_.isNil(value) && !_.isEmpty(value) && value.length === 12) {
    return true;
  }
  return false;
};

export const purePhoneNumberValue = value => {
  if (value) {
    let incomingValue = value.replace(/\s+/g, '');

    if (
      incomingValue &&
      incomingValue.length &&
      incomingValue.indexOf('+90') > -1
    ) {
      incomingValue = incomingValue.slice(3, 20);
    }
    if (
      incomingValue &&
      !_.isNil(incomingValue) &&
      !_.isEmpty(incomingValue) &&
      incomingValue.length === 10
    ) {
      return true;
    }
    return false;
  }
  return false;
};
export const identityNumberRule = value => {
  if (value && !_.isNil(value) && !_.isEmpty(value) && value.length === 11) {
    return true;
  }
  return false;
};

export const userPasswordRule = value => {
  const passwordValidationRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$!~'%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,15}$/;
  return passwordValidationRegex.test(String(value));
};

export const isEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const isOtp = incomingOtp => {
  const otpRegex = /^(\s*\d{6}\s*)(,\s*\d{6}\s*)*,?\s*$/;
  try {
    return otpRegex.test(incomingOtp);
  } catch {
    return false;
  }
};
export const isEmailOtp = value => {
  if (value && value.length && value.length >= 6 && value.length <= 6) {
    return /.{6,6}$/.test(value);
  }
};

export const isPin = incomingOtp => {
  const otpRegex = /^(\s*\d{4}\s*)(,\s*\d{4}\s*)*,?\s*$/;
  try {
    return otpRegex.test(incomingOtp);
  } catch {
    return false;
  }
};

export default {
  phoneNumberRule,
  userPasswordRule,
  isEmail,
  isOtp,
};
