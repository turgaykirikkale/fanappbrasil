import React, {Component} from 'react';
import {View} from 'react-native';
import _ from 'lodash';
import * as Rules from '@ValidationEngine/Rules';
import {styles} from './assets/styles';
import autobind from 'autobind-decorator';

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.componentStackGenerator = this.componentStackGenerator.bind(this);
    this.checkValidation = this.checkValidation.bind(this);
  }
  componentStack = [];
  isFormValid = false;

  componentWillMount() {
    if (this.props && this.props.children) {
      this.componentStack = [];
      this.componentStackGenerator(this.props.children, this.componentStack);
    }
  }

  componentWillReceiveProps(nextProps) {
    let currentComponentStack = [];
    this.componentStackGenerator(nextProps.children, currentComponentStack);
    if (!_.isEqual(this.componentStack, currentComponentStack)) {
      this.componentStack = currentComponentStack;
      this.checkValidation();
    }
  }

  componentDidMount() {
    this.checkValidation();
  }

  checkValidation() {
    if (this.componentStack && this.componentStack.length > 0) {
      let currentComponentsStackIsValid = true;
      _.each(this.componentStack, item => {
        if (item && item.rules && !item.rule) {
          if (typeof item.rules === 'object') {
            _.each(item.rules, rule => {
              if (rule && rule.name && rule.values) {
                if (
                  Rules[rule.name] &&
                  !Rules[rule.name](rule.values[0], rule.values[1])
                ) {
                  currentComponentsStackIsValid = false;
                }
              } else if (rule && rule.name && !rule.values) {
                if (Rules[rule.name] && !Rules[rule.name](item.value)) {
                  currentComponentsStackIsValid = false;
                }
              }
            });
          }
        } else if (item && !item.rules) {
          if (item && item.rule === undefined) {
            currentComponentsStackIsValid = false;
          } else if (
            typeof item.rule === 'string' &&
            !Rules[item.rule](item.value)
          ) {
            currentComponentsStackIsValid = false;
          } else if (
            typeof item.rule === 'object' &&
            !Rules[item.rule.name](item.rule.value, item.value)
          ) {
            currentComponentsStackIsValid = false;
          }
        } else {
          currentComponentsStackIsValid = false;
        }
      });
      if (currentComponentsStackIsValid !== this.isFormValid) {
        this.isFormValid = currentComponentsStackIsValid;
        this.props &&
          this.props.formValidSituationChanged &&
          this.props.formValidSituationChanged(currentComponentsStackIsValid);
      }
    }
  }

  @autobind
  validationObjectGenerator(item) {
    return {
      rule: item.props.rule,
      value: item.props.value,
      name: item.props.name,
    };
  }

  @autobind
  multipleValidationObjectGenerator(item) {
    return {
      rules: item.props.rules,
      value: item.props.value,
    };
  }

  @autobind
  isHasValidationRule(item) {
    return item && item.props && item.props.rule;
  }

  @autobind
  isHasMultipleValidationRule(item) {
    return item && item.props && item.props.rules;
  }

  @autobind
  checkItemHasSubChild(item) {
    return item && item.props && item.props.children;
  }

  @autobind
  componentStackGenerator(componentList, stack) {
    if (componentList && componentList.length && componentList.length > 0) {
      _.each(componentList, item => {
        if (this.checkItemHasSubChild(item)) {
          if (this.isHasValidationRule(item)) {
            stack.push(this.validationObjectGenerator(item));
          } else if (this.isHasMultipleValidationRule(item)) {
            stack.push(this.multipleValidationObjectGenerator(item));
          }
          this.componentStackGenerator(item.props.children, stack);
        } else if (this.isHasValidationRule(item)) {
          stack.push(this.validationObjectGenerator(item));
        } else if (this.isHasMultipleValidationRule(item)) {
          stack.push(this.multipleValidationObjectGenerator(item));
        }
      });
    } else if (
      componentList &&
      componentList.props &&
      componentList.props.children &&
      componentList.props.children.props &&
      !componentList.props.children.props.children &&
      this.isHasValidationRule(componentList.props.children.props.children)
    ) {
      stack.push(this.validationObjectGenerator(componentList.props.children));
    } else if (
      componentList &&
      componentList.props &&
      componentList.props.children &&
      componentList.props.children.props &&
      componentList.props.children.props.children &&
      this.isHasMultipleValidationRule(
        componentList.props.children.props.children,
      )
    ) {
      stack.push(
        this.multipleValidationObjectGenerator(componentList.props.children),
      );
    } else if (
      componentList &&
      componentList.props &&
      componentList.props.children &&
      componentList.props.children.props &&
      componentList.props.children.props.children
    ) {
      this.componentStackGenerator(
        componentList.props.children.props.children,
        stack,
      );
    } else if (
      componentList &&
      !componentList.length &&
      this.isHasValidationRule(componentList)
    ) {
      stack.push(this.validationObjectGenerator(componentList));
    }
  }

  render() {
    return <View style={styles.container}>{this.props.children}</View>;
  }
}
