import Localization from '@Localization';

export const timeLocalization = (isCapital = false) => {
  const currentLangCode = Localization.language;
  if (currentLangCode === 'tr') {
    return {
      hour: isCapital ? 'Saat' : 'saat',
      hourShortcut: 'sa',
      minute: isCapital ? 'Dakika' : 'dakika',
      minuteShortcut: 'd',
      second: isCapital ? 'Saniye' : 'saniye',
      secondShortcut: 's',
      day: isCapital ? 'Gün' : 'gün',
      week: isCapital ? 'Hafta' : 'hafta',
      month: isCapital ? 'Ay' : 'ay',
    };
  } else if (currentLangCode === 'en') {
    return {
      hour: isCapital ? 'Hour' : 'hour',
      hourShortcut: 'h',
      minute: isCapital ? 'Minute' : 'minute',
      minuteShortcut: 'm',
      second: isCapital ? 'Second' : 'second',
      secondShortcut: 'sec',
      day: isCapital ? 'Day' : 'day',
      week: isCapital ? 'Week' : 'week',
      month: isCapital ? 'Month' : 'month',
    };
  }
};
