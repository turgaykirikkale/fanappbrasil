import _ from 'lodash';

export function percentExecutor(incomingList) {
  try {
    let newList = incomingList;
    if (incomingList && incomingList.length && incomingList.length > 0) {
      const highestOrder = _.maxBy(incomingList.slice(0, 15), 'Total').Total;

      _.each(incomingList, (item, i) => {
        const calculatedPercent =
          (parseFloat(item.Total) * 100) / parseFloat(highestOrder);
        newList[i] = {
          ...newList[i],
          DifferencePercent: calculatedPercent,
        };
      });

      return newList;
    }
  } catch (error) {
    console.log('PercentExecutor catch = ', error);
  }
}
