import _ from 'lodash';
import {percentExecutor} from '@Helpers/OrderPercentExecutor';
import {BUYFLOW, SELLFLOW} from '@Commons/FAEnums';

const mapIncomingOrderBookAndReturn = (orderBook, orderType) => {
  let generatedOrderBook = [];
  if (orderBook && orderBook.length && orderBook.length > 0) {
    _.each(orderBook, item => {
      if (item && item.indexOf(',') > -1) {
        let generatedNewOrderObject = {};
        const seperatedPrice = item.split(',');
        if (
          seperatedPrice &&
          seperatedPrice.length &&
          seperatedPrice.length > 0
        ) {
          let coinValue = seperatedPrice[1];
          generatedNewOrderObject.Type = orderType;
          generatedNewOrderObject.Price = seperatedPrice[0];
          generatedNewOrderObject.CoinValue = coinValue;
          generatedNewOrderObject.Total =
            generatedNewOrderObject.Price * generatedNewOrderObject.CoinValue;
          generatedOrderBook.push(generatedNewOrderObject);
        }
      }
    });
    return percentExecutor(_.slice(generatedOrderBook, 0, 15));
  }
  return [];
};

export const orderBookDataRenderer = data => {
  let buyOrderBook = null;
  let sellOrderBook = null;
  let marketPrice = null;
  let change24 = null;
  if (data.c) {
  }
  try {
    if (data && data.b) {
      buyOrderBook = data.b.split('|') || [];
      buyOrderBook = mapIncomingOrderBookAndReturn(buyOrderBook, BUYFLOW);
    } else {
      buyOrderBook = null;
    }
  } catch (e) {
    buyOrderBook = null;
  }

  try {
    if (data && data.s) {
      sellOrderBook = data.s.split('|') || [];
      sellOrderBook = mapIncomingOrderBookAndReturn(sellOrderBook, SELLFLOW);
    }
  } catch (e) {
    sellOrderBook = null;
  }

  try {
    marketPrice = data && data.p && String(data.p) ? String(data.p) : null;
  } catch (e) {
    marketPrice = null;
  }

  try {
    change24 = data && data.c && String(data.c) ? String(data.c) : null;
  } catch (e) {
    change24 = null;
  }

  return {
    BuyOrderBook: buyOrderBook,
    SellOrderBook: sellOrderBook,
    MarketPrice: marketPrice,
    Change24: change24,
  };
};
