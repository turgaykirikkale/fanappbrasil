import {FAToFixed} from '@Commons/FAMath';
import store from '@GlobalStore/store';
import _ from 'lodash';
import FAService from '@Services';

export const orderCalculation = (
  incomingCurrencyValue,
  incomingCoinValue,
  processType,
  incomingComRate,
  activeBuyOrders,
  activeSellOrders,
) => {
  const comRate = incomingComRate / 100;
  let activeOrders = null;
  if (processType === 1) {
    activeOrders = activeSellOrders;
  } else {
    activeOrders = activeBuyOrders;
  }
  let totalCost = null;
  let totalCoin = null;
  if (!_.isNil(incomingCurrencyValue)) {
    let finalCurrencyValue =
      incomingCurrencyValue - incomingCurrencyValue * comRate;

    _.each(activeOrders, item => {
      if (_.isNil(totalCost) && item.Total >= finalCurrencyValue) {
        totalCoin = finalCurrencyValue / item.Price;
        totalCost = totalCoin * item.Price;

        return false;
      } else if (_.isNil(totalCost) && item.Total <= finalCurrencyValue) {
        totalCoin = item.CoinValue;
        totalCost = item.Total;
      } else if (totalCost && item.Total >= finalCurrencyValue - totalCost) {
        const remainingValue = finalCurrencyValue - totalCost;
        totalCoin += remainingValue / item.Price;
        totalCost += totalCoin * remainingValue;

        return false;
      } else if (totalCost && item.Total < finalCurrencyValue - totalCost) {
        totalCoin += item.CoinValue;
        totalCost += item.Total;
      }
    });
    totalCost = incomingCurrencyValue;
  } else if (!_.isNil(incomingCoinValue)) {
    _.each(activeOrders, item => {
      if (_.isNil(totalCoin) && item.CoinValue >= incomingCoinValue) {
        totalCoin = incomingCoinValue;
        totalCost = totalCoin * item.Price;
        return false;
      } else if (totalCoin && item.CoinValue >= incomingCoinValue - totalCoin) {
        const remainingValue = incomingCoinValue - totalCoin;
        totalCoin += remainingValue;
        totalCost += remainingValue * item.Price;
        return false;
      } else if (totalCoin && item.CoinValue < incomingCoinValue - totalCoin) {
        totalCoin += item.CoinValue;
        totalCost += item.CoinValue * item.Price;
      } else if (_.isNil(totalCoin) && item.CoinValue < incomingCoinValue) {
        totalCoin = item.CoinValue;
        totalCost += item.CoinValue * item.Price;
      }
    });
    totalCoin = incomingCoinValue;
    if (processType === 1) {
      totalCost = totalCost + totalCost * comRate;
    } else {
      totalCost = totalCost - totalCost * comRate;
    }
  }

  return {
    totalCost: FAToFixed(totalCost),
    totalCoin: FAToFixed(totalCoin, true),
  };
};

export const anonymousCommissionRateHelper = (currencyCode, isTaker) => {
  if (currencyCode === 'BTC' || currencyCode === 'CHFT') {
    return isTaker ? 0.15 : 0.1;
  }
  return isTaker ? 0.2 : 0.15;
};

export const getCommissionRate = (
  currency: Object,
  coinId: Number,
  isTaker: Boolean,
) => {
  let takerCommissionRate = 0.2;
  let makerCommissionRate = 0.15;
  let isAnonymousFlow = false;

  const globalStore = store.getState();
  if (
    globalStore &&
    (!globalStore.UserState || !globalStore.UserState.UserInfo)
  ) {
    isAnonymousFlow = true;
  }
  if (currency && coinId) {
    const requestBody = {
      CurrencyId: currency.CurrencyId,
      CoinId: coinId,
    };
    FAService.GetMarketInformation(requestBody)
      .then(response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.CommissionRate
        ) {
          const commissionRateResponse = response.data.CommissionRate;
          if (isAnonymousFlow) {
            takerCommissionRate,
              (makerCommissionRate = anonymousCommissionRateHelper(
                currency,
                isTaker,
              ));
          } else {
            takerCommissionRate = commissionRateResponse.TakerRate;
            makerCommissionRate = commissionRateResponse.MakerRate;
          }
        }
      })
      .catch(error => console.log(error));
  }

  return {
    maker: makerCommissionRate,
    taker: takerCommissionRate,
    isAnonymous: isAnonymousFlow,
  };
};
