export const generateRandomColor = () => {
  const colors = [
    '#aa33d9',
    '#5353e7',
    '#00FF00',
    // '#FFFF00',
    '#FFC4A5',
    '#03A66D',
    '#17A2B8',
    '#EA5615',
  ];
  const randomColorIndex = Math.floor(Math.random() * colors.length);

  return '#437bff';
};
