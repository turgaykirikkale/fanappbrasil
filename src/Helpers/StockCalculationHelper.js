import {orderCalculation} from './Calculation';
import {FAToFixed} from '@Commons/FAMath';
import {StockMarketTypes} from '@Commons/Enums/StockMarketTypes';
import _ from 'lodash';
import store from '../GlobalStore/store';

const handleCommissionRate = isTaker => {
  const globalStore = store.getState();
  if (globalStore?.MarketState?.CommissionRate) {
    const CommissionRate = globalStore.MarketState.CommissionRate;
    if (isTaker) {
      return CommissionRate.TakerRate;
    }
    return CommissionRate.MakerRate;
  }
};

const calculateCommissionRate = (limitPrice, state) => {
  try {
    let calculatedLimit = limitPrice;
    const {flow, currentMarketFlowType, buyOrderBook, sellOrderBook} = state;

    if (currentMarketFlowType === StockMarketTypes.Market) {
      return handleCommissionRate(true);
    }
    let ActiveOrders = null;
    let commissionRate = 0;
    if (flow === 1) {
      if (sellOrderBook && sellOrderBook.length && sellOrderBook.length > 0) {
        ActiveOrders = sellOrderBook;
        if (calculatedLimit >= ActiveOrders[0].Price) {
          commissionRate = handleCommissionRate(true);
        } else {
          commissionRate = handleCommissionRate(false);
        }
      }
    } else {
      if (buyOrderBook && buyOrderBook.length && buyOrderBook.length > 0) {
        ActiveOrders = buyOrderBook;
        if (calculatedLimit > ActiveOrders[0].Price) {
          commissionRate = handleCommissionRate(false);
        } else {
          commissionRate = handleCommissionRate(true);
        }
      }
    }
    return commissionRate / 100;
  } catch (error) {}
};

export const stockCalculationHelper = (
  state: {
    currentMarketFlowType: string,
    percent: number,
    flow: number,
    limitPrice: number | string,
    amount: number | string,
    totalPrice: number | string,
    buyOrderBook: Array,
    sellOrderBook: Array,
  },
  incomingCurrencyValue,
  incomingCoinValue,
  incomingLimit,
  triggedByPercentValueExecutor,
): Object => {
  let response = {};
  if (
    triggedByPercentValueExecutor === null ||
    triggedByPercentValueExecutor === undefined ||
    triggedByPercentValueExecutor === ''
  ) {
    response = {
      ...response,
      percent: 0,
    };
  }

  const {currentMarketFlowType, percent, flow, buyOrderBook, sellOrderBook} =
    state;

  if (
    (_.isNil(incomingCurrencyValue) || _.isEmpty(incomingCurrencyValue)) &&
    (_.isNil(incomingCoinValue) || _.isEmpty(incomingCoinValue)) &&
    (_.isNil(incomingLimit) || _.isEmpty(incomingLimit)) &&
    (_.isNil(triggedByPercentValueExecutor) ||
      _.isEmpty(triggedByPercentValueExecutor))
  ) {
    if (currentMarketFlowType === StockMarketTypes.Market) {
      response = {
        ...response,
        totalPrice: null,
        amount: null,
      };
    } else {
      if (incomingCurrencyValue === '' || incomingCoinValue === '') {
        response = {
          ...response,
          totalPrice: null,
          amount: null,
        };
      } else if (incomingLimit === '') {
        response = {
          ...response,
          limitPrice: null,
          totalPrice: null,
        };
      }
    }
    return response;
  } else {
    if (currentMarketFlowType === StockMarketTypes.Market) {
      if (
        (_.isNil(incomingCoinValue) || _.isEmpty(incomingCoinValue)) &&
        incomingCurrencyValue
      ) {
        const {totalCost, totalCoin} = orderCalculation(
          parseFloat(incomingCurrencyValue),
          null,
          flow,
          calculateCommissionRate(state.limitPrice, state),
          buyOrderBook,
          sellOrderBook,
        );
        response = {
          ...response,
          totalPrice: !_.isNil(totalCost) ? String(FAToFixed(totalCost)) : null,
          amount: !_.isNil(totalCoin) ? String(FAToFixed(totalCoin)) : null,
          percent: triggedByPercentValueExecutor ? percent : null,
        };
      } else if (_.isNil(incomingCoinValue) || _.isEmpty(incomingCoinValue)) {
        response = {
          ...response,
          amount: null,
          totalPrice: null,
        };
      } else if (
        (_.isNil(incomingCurrencyValue) || _.isEmpty(incomingCurrencyValue)) &&
        incomingCoinValue
      ) {
        const {totalCost, totalCoin} = orderCalculation(
          null,
          parseFloat(incomingCoinValue),
          flow,
          calculateCommissionRate(state.limitPrice, state),
          buyOrderBook,
          sellOrderBook,
        );
        response = {
          ...response,
          totalPrice: String(FAToFixed(totalCost)),
          amount: String(FAToFixed(totalCoin)),
          percent: triggedByPercentValueExecutor ? percent : null,
        };
      } else if (
        _.isNil(incomingCurrencyValue) ||
        _.isEmpty(incomingCurrencyValue)
      ) {
        response = {
          ...response,
          totalPrice: null,
          percent: triggedByPercentValueExecutor ? percent : null,
        };
      }
      return response;
    } else if (
      currentMarketFlowType === StockMarketTypes.Limit ||
      currentMarketFlowType === StockMarketTypes.Stop
    ) {
      //---------------Limitin Girildiği --------------------//
      if (!_.isNil(incomingLimit) && !_.isEmpty(incomingLimit)) {
        const {amount} = state;
        if (!_.isNil(amount) && amount) {
          const calculatedCurrencyValue =
            parseFloat(incomingLimit) * parseFloat(amount);

          let calculatedCurrencyValueWithCom = '';

          if (flow === 1) {
            calculatedCurrencyValueWithCom =
              parseFloat(calculatedCurrencyValue) +
              parseFloat(calculatedCurrencyValue) *
                calculateCommissionRate(incomingLimit, state);
          } else {
            calculatedCurrencyValueWithCom =
              parseFloat(calculatedCurrencyValue) -
              parseFloat(calculatedCurrencyValue) *
                calculateCommissionRate(incomingLimit, state);
          }
          console.log('Calculation helper amount = ', amount);
          console.log('Calculation helper incomingLimit = ', incomingLimit);

          response = {
            ...response,
            totalPrice: String(FAToFixed(calculatedCurrencyValueWithCom)),
            limitPrice: String(FAToFixed(incomingLimit)),
          };
        } else if (!_.isNil(state.totalPrice) && state.totalPrice) {
          const calculatedCoinValue =
            parseFloat(state.totalPrice) / parseFloat(incomingLimit);
          const calculatedCoinValueWithoutCom =
            parseFloat(calculatedCoinValue) -
            parseFloat(calculatedCoinValue) *
              calculateCommissionRate(incomingLimit, state);
          response = {
            ...response,
            limitPrice: String(FAToFixed(incomingLimit)),
            amount: String(FAToFixed(calculatedCoinValueWithoutCom)),
          };
        } else {
          response = {
            ...response,
            limitPrice: String(FAToFixed(incomingLimit)),
          };
        }
        return response;
      }

      //---------------Miktarin Coinin Girildiği -----------------//
      if (!_.isNil(incomingCoinValue) && !_.isEmpty(incomingCoinValue)) {
        if (!_.isNil(state.limitPrice) && state.limitPrice) {
          const calculatedCurrencyValue =
            parseFloat(state.limitPrice) * parseFloat(incomingCoinValue);
          let calculatedCurrencyValueWithCom = '';
          if (flow === 1) {
            calculatedCurrencyValueWithCom =
              parseFloat(calculatedCurrencyValue) +
              parseFloat(calculatedCurrencyValue) *
                calculateCommissionRate(state.limitPrice, state);
          } else {
            calculatedCurrencyValueWithCom =
              parseFloat(calculatedCurrencyValue) -
              parseFloat(calculatedCurrencyValue) *
                calculateCommissionRate(state.limitPrice, state);
          }
          response = {
            ...response,
            amount: String(FAToFixed(incomingCoinValue)),
            totalPrice: String(FAToFixed(calculatedCurrencyValueWithCom)),
          };
        } else {
          response = {
            ...response,
            amount: String(FAToFixed(incomingCoinValue)),
          };
        }
        return response;
      }
      //---------------------------- Toplamın Girildiği ---------------- //
      if (
        !_.isNil(incomingCurrencyValue) &&
        !_.isEmpty(incomingCurrencyValue) &&
        incomingCurrencyValue !== '.'
      ) {
        if (!_.isNil(state.limitPrice) && !_.isEmpty(state.limitPrice)) {
          const commissionRate = calculateCommissionRate(
            state.limitPrice,
            state,
          );
          let droppedCom = 0;
          let parsedIncomingCurrencyValue = parseFloat(incomingCurrencyValue);
          if (flow === 1) {
            droppedCom =
              parsedIncomingCurrencyValue -
              parsedIncomingCurrencyValue * commissionRate;
          } else {
            droppedCom =
              parsedIncomingCurrencyValue +
              parsedIncomingCurrencyValue * commissionRate;
          }
          const coinAmount = droppedCom / state.limitPrice;
          let incomingCurrencyValueParsed = incomingCurrencyValue;
          if (
            triggedByPercentValueExecutor &&
            triggedByPercentValueExecutor !== '1'
          ) {
            incomingCurrencyValueParsed = droppedCom;
          }
          response = {
            ...response,
            totalPrice: String(FAToFixed(incomingCurrencyValue)),
            amount: String(FAToFixed(coinAmount)),
          };
        } else {
          response = {
            ...response,
            totalPrice: String(FAToFixed(incomingCurrencyValue)),
          };
        }
        return response;
      } else {
        if (incomingCurrencyValue === '' || incomingCurrencyValue === '.') {
          response = {
            ...response,
            amount: null,
            totalPrice: null,
          };
        }
        return response;
      }
    }
  }
};
