export const sentenceShortener = (sentence: String, limit: Number): String => {
  if (sentence && sentence.length && sentence.length > limit) {
    let generatedSentence = sentence.slice(0, limit);
    generatedSentence = generatedSentence.trim();
    generatedSentence += '...';
    return generatedSentence;
  }
  return sentence;
};
