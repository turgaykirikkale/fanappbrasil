import _ from 'lodash';

export const percentageLocalizationHelperForTRLanguage = incomingValue => {
  try {
    const castedValue = String(incomingValue);
    if (!_.isNil(castedValue)) {
      switch (castedValue) {
        case '10':
          return 'undan';
        case '20':
          return 'sinden';
        case '30':
          return 'undan';
        case '40':
          return 'ından';
        case '50':
          return 'sinden';
        case '60':
          return 'ından';
        case '70':
          return 'inden';
        case '80':
          return 'inden';
        case '90':
          return 'ından';
        case '100':
          return 'ünden';
        default:
          break;
      }

      const lastDigit = castedValue.slice(-1);
      switch (lastDigit) {
        case '0':
          return 'undan';
        case '1':
          return 'inden';
        case '2':
          return 'sinden';
        case '3':
          return 'ünden';
        case '4':
          return 'ünden';
        case '5':
          return 'inden';
        case '6':
          return 'sından';
        case '7':
          return 'sinden';
        case '8':
          return 'inden';
        case '9':
          return 'undan';
        default:
          return '';
      }
    }
  } catch (e) {}
};
